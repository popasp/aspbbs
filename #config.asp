<%
'如果没有调用popasp则跳转到首页

'显示页面Trace信息
POP_MVC.config("SHOW_PAGE_TRACE") = 0

'网站是否处于上线状态，开发中设为1，上线后设为0
POP_MVC.config("APP_DEBUG") = 0

'默认插件名
POP_MVC.config("DEFAULT_EXTEND") = "bbs"

'是否继承默认插件的配置
POP_MVC.config("INHERIT_DEFAULT_EXTEND") = 1

'动态默认网址的入口文件
POP_MVC.config("INDEX_PAGE") = "default.asp"

'配置文件中的配置项的格式为 POP_MVC.config(key) = value

''URL访问模式支持 0 (普通模式); 1 (PATHINFO 模式); 
POP_MVC.config( "URL_MODE" ) = 1

private action_

action_ = LCase(request.querystring("a"))

if action_ = "search" or action_ = "msearch" OR action_ = "tpl"  OR action_ = "ajax" OR action_ = "removeproductcart" then
	POP_MVC.config( "URL_MODE" ) = 0
end if

''PATHINFO模式下的参数分割符 
POP_MVC.config( "URL_DEPR" ) = "_"

''PATHINFO模式下的匹配规则
'第4个参数,有3种: get,post,ignore，默认为get
POP_MVC.config( "URL_RULE" ) = array( _
	Array( "^\s*$" , "" , "c=bbs--Index&a=Index") _	
	,Array( "^checkcode$" , "a" , "c=Top") _	
	,Array( "^checkcode_[\w\.]+$" , "a_id" , "c=Top" ) _
	,Array( "^upload4summernote_upload$" , "aspbbs_a" , "c=upload4summernote" ) _
	,Array( "^u_[0-9]+$" , "aspbbs_id" , "c=U" ) _
	,Array( "^u_[0-9]+_[1-9]\d*$" , "aspbbs_id_page" , "c=U" ) _
	,Array( "^v_[0-9]+$" , "aspbbs_id" , "c=U&a=timeline" ) _
	,Array( "^v_[0-9]+_[1-9]\d*$" , "aspbbs_id_page" , "c=U&a=timeline" ) _
	,Array( "^[1-9]\d*$" , "id" , "c=thread&a=detail" ) _
	,Array( "^[1-9]\d*_[1-9]\d*$" , "id_page" , "c=thread&a=detail" ) _
	,Array( "^Last_[1-9]\d*$" , "aspbbs_id" , "c=thread&a=detail&page=x" ) _
	,Array( "^Last_[1-9]\d*_[1-9]\d*$" , "aspbbs_id_rand" , "c=thread&a=detail&page=x" ) _
	,Array( "^only_[1-9]\d*_[1-9]\d*$" , "a_id_UserID" , "c=thread&page=1" ) _
	,Array( "^only_[1-9]\d*_[1-9]\d*_[1-9]\d*$" , "a_id_UserID_page" , "c=thread" ) _
	,Array( "^(p|post|thread)_[1-9]\d*$" , "aspbbs_id" , "c=thread&a=detail" ) _
	,Array( "^(p|post|thread)_[1-9]\d*_[1-9]\d*$" , "aspbbs_id_page" , "c=thread&a=detail" ) _
	,Array( "^thread_(index|orderId|orderReply|unsolved|solved|feature|UnsolvedOrderID|UnsolvedOrderReply|SolvedOrderID|SolvedOrderReply|FeatureOrderID|FeatureOrderReply)_[1-9]\d*$" , "c_a_id" , "page=1" ) _
	,Array( "^thread_(index|orderId|orderReply|unsolved|solved|feature|UnsolvedOrderID|UnsolvedOrderReply|SolvedOrderID|SolvedOrderReply|FeatureOrderID|FeatureOrderReply)_[1-9]\d*_[1-9]\d*$" , "aspbbs_a_id_page" , "c=thread" ) _
	,Array( "^unsolved|solved|feature|orderId|orderReply|UnsolvedOrderID|UnsolvedOrderReply|SolvedOrderID|SolvedOrderReply|FeatureOrderID|FeatureOrderReply$" , "a" , "c=thread&id=" ) _
	,Array( "^(unsolved|solved|feature|orderId|orderReply|UnsolvedOrderID|UnsolvedOrderReply|SolvedOrderID|SolvedOrderReply|FeatureOrderID|FeatureOrderReply)_[1-9]\d*$" , "a_page" , "c=thread&id=" ) _
	,Array( "^thread_[a-z][a-z0-9]*$" , "c_a" , "" ) _
	,Array( "^thread_[a-z][a-z0-9]*_[1-9]\d*$" , "aspbbs_a_page" , "c=thread" ) _	
	,Array( "^index_[1-9]\d*$" , "a_page" , "c=thread&a=index&id=0" ) _
	,Array( "^list_[1-9]\d*$" , "aspbbs_id" , "c=thread&a=index" ) _
	,Array( "^list_[1-9]\d*_[1-9]\d*$" , "aspbbs_id_page" , "c=thread&a=index" ) _
	,Array( "^reply_[1-9]\d*$" , "a_ReplyID" , "c=thread&a=reply" ) _
	,Array( "^recycle|drafts|ThreadCheck$" , "a" , "c=thread&id=0" ) _
	,Array( "^toplist$" , "" , "c=thread&a=search&isTop=1" ) _
	,Array( "^(recycle|drafts)_[1-9]\d*$" , "a_page" , "c=thread&id=0" ) _
	,Array( "^ThreadCheck_[1-9]\d*$" , "aspbbs_page" , "c=thread&a=ThreadCheck&id=0" ) _
	,Array( "^toplist_[1-9]\d*$" , "aspbbs_page" , "c=thread&a=search&isTop=1" ) _
	,Array( "^UserSet$" , "" , "c=user&a=settings" ) _
	,Array( "^message$" , "" , "c=user&a=message" ) _
	,Array( "^BbsConfig$" , "" , "c=manage&a=configs" ) _
	,Array( "^SiteConfig$" , "" , "c=manage&a=configs2" ) _
	,Array( "^System$" , "" , "c=system&a=settings" ) _
	,Array( "^UserList$" , "" , "c=user&a=list" ) _
	,Array( "^AddDonate$" , "" , "c=Donate&a=AddDonate" ) _	
	,Array( "^post$" , "" , "c=Topic&a=Add" ) _
	,Array( "^sitemap$" , "" , "c=sitemap" ) _
	,Array( "^changelog$" , "" , "c=tools&a=changelog" ) _
	,Array( "^diary$" , "" , "c=laydate--index&a=index" ) _
	,Array( "^case$" , "" , "c=cases--Index&a=index" ) _
	,Array( "^case_[1-9]\d*$" , "aspbbs_id" , "c=cases--Index&a=index" ) _
	,Array( "^case_[1-9]\d*_[1-9]\d*$" , "aspbbs_id_page" , "c=cases--index&a=index" ) _
	,Array( "^File_(redundancy|emptyFolder|safeTest)$" , "c_a") _	
	,Array( "^File_(redundancy|emptyFolder|safeTest)_\w+$" , "c_a_action") _
	,Array( "^Site*_Status*_[0-9]+$" , "c_a_MailAlert" ) _
	,Array( "^UploadLog$" , "" , "c=UploadLog--Index&a=Index&page=1" ) _
	,Array( "^UploadLog_[1-9]\d*$" , "aspbbs_page" , "c=UploadLog--Index&a=Index" ) _
	,Array( "^faq$" , "c" , "page=1" ) _
	,Array( "^faq_[1-9]\d*$" , "c_page" , "" ) _
	,Array( "^[a-z][a-z0-9]*_[0-9]+$" , "c_id" , "a=index" ) _
	,Array( "^[a-z][a-z0-9]*$" , "c" , "" ) _
	,Array( "^[a-z][a-z0-9]*_[0-9]+$" , "c_id" , "a=index" ) _
	,Array( "^[a-z][a-z0-9]*_[a-z][a-z0-9]*$" , "c_a" ) _	
	,Array( "^[a-z0-9]+--[a-z][a-z0-9]*$" , "c" , "a=index" ) _
	,Array( "^[a-z0-9]+--[a-z][a-z0-9]*_[a-z][a-z0-9]*$" , "c_a" ) _
	,Array( "^[a-z0-9]+--[a-z][a-z0-9]*_[a-z][a-z0-9]*_[1-9]\d*$" , "c_a_id" ) _
	,Array( "^[a-z0-9]+--[a-z][a-z0-9]*_[a-z][a-z0-9]*_[1-9]\d*_[0-9]\d*$" , "c_a_id_page" ) _
	,Array( "^[a-z0-9]+--[a-z][a-z0-9]*_[1-9]\d*$" , "c_type" , "a=index" ) _
	,Array( "^[a-z0-9]+--[a-z][a-z0-9]*_[1-9]\d*_[1-9]\d*$" , "c_type_id" , "a=index" ) _
	,Array( "^[a-z][a-z0-9]*_[a-z][a-z0-9]*_[0-9]+$" , "c_a_id" ) _
	,Array( "^[a-z][a-z0-9]*_[a-z][a-z0-9]*_[0-9]+_[1-9]\d*$" , "c_a_id_page" ) _
)

'数据库存放目录文件名
POP_MVC.config("DATA_NAME") = "data"

''仅适用于sqlserver、mysql
POP_MVC.config("DB_HOST") = "localhost"

'数据库名,mysql或sqlserver数据时设置有效
POP_MVC.config("DB_NAME") = "aspbbs"

''数据库用户名，仅适用于sqlserver、mysql
POP_MVC.config("DB_USER") = "root"

''数据库密码，数据库密码，仅适用于sqlserver、mysql
POP_MVC.config("DB_PWD") = "root"

'数据库类型，支持 access、sqlite3、mysql 三种
POP_MVC.config("DB_TYPE") = "access"

'sqlite3数据库安装好驱动后，这里填写对应的驱动名称
POP_MVC.config("SQLITE3_DRIVER_NAME") = "SQLite3 ODBC Driver"

if POP_MVC.config("DB_TYPE") = "access" then
	POP_MVC.config("DB_PATH") = POP_MVC.config("CORE_NAME") & "/" & POP_MVC.config("DATA_NAME") & "/#aspbbs#.mdb"
elseif POP_MVC.config("DB_TYPE") = "sqlite3" then
	POP_MVC.config("DB_PATH") = POP_MVC.config("CORE_NAME") & "/" & POP_MVC.config("DATA_NAME") & "/#aspbbs#.db"
end if

'幻灯片设置
'POP_MVC.config("slide_name_list") = Array("幻灯片A","幻灯片B","幻灯片C","幻灯片D")

POP_MVC.config("CMS_UPLOAD_NAME") = "Upload"

'上传文件路径，必须写成根目录，如果是二级目录不用更改，后面不用加/
POP_MVC.config("CMS_UPLOAD_PATH") =  POP_MVC.config("CMS_UPLOAD_NAME") & "/bbs"

''上传的文件限制
POP_MVC.config("UPLOAD_MAX_FILESIZE") = 10*1024*1024

''上传限制
POP_MVC.config("UPLOAD_MAX_SIZE") = 100*1024*1024

''图片上传类型
POP_MVC.config("UPLOAD_IMAGE_TYPES") = "jpg;jpeg;png;gif;bmp;pcx;svg"

''文件上传类型
POP_MVC.config("UPLOAD_DOWNLOAD_TYPES") = "flv;swf;mkv;avi;rm;rmvb;mpeg;mpg;ogg;ogv;mov;wmv;mp4;webm;mp3;wav;mid;rar;zip;tar;gz;7z;bz2;cab;iso;doc;docx;xls;xlsx;ppt;pptx;pdf;txt;md;xml;wma;ico;asp;html;htm"

POP_MVC.config("UPLOAD_ALLOW_TYPES") = POP_MVC.config("UPLOAD_IMAGE_TYPES") & ";" & POP_MVC.config("UPLOAD_DOWNLOAD_TYPES")

'按帖子回复内容搜索的特征前缀
POP_MVC.config("SO_REPLY_PREFIX") = "rr "

'不进行懒加载的图片，如表情图片
POP_MVC.config("NO_FLOW_STR") = "tam-emoji/img"

'回复删除之后的提示
POP_MVC.config("DEL_REPLY_TIP") = "<div class='fly-remove'><i class='iconfont icon-tishilian'></i><div class='notice'>该回复内容已经被删除</div></div>"

'回复删除之后的提示
POP_MVC.config("DEL_REPLY_TIP") = "<div class='fly-remove'><i class='iconfont icon-tishilian'></i><div class='notice'>该留言内容正在审核中…</div></div>"


'''''''''''''''以下内容由项目开发者设置，切勿改动'''''''''''''''''
POP_MVC.config("CMS_NAME") = "ASPBBS"
POP_MVC.config("CMS_POWER_NAME") = "ASPBBS"

'数据表名前缀
POP_MVC.config( "DB_PREFIX" ) = "iAspCms_"

'插件数据表名前缀
POP_MVC.config( "EXTEND_TBL_PREFIX" ) = "iAspCms_self_"

'数据库查询语句表名前缀
POP_MVC.config( "DB_INNER_PREFIX" ) = "{prefix}"

'CMS版本号
POP_MVC.config("CMS_VERSION") = "2.0"

'文章内容分页标识符
POP_MVC.config("content_page_lable") = "{aspbbs:page}"

'cms类名
POP_MVC.config("CMS_CLASS_NAME") = "aspbbs"

'标签标识符{aspbbs:item}
POP_MVC.config("CMS_TAG_LABEL") = "aspbbs"

'自定义标签标识符{label:item}
POP_MVC.config("CMS_SELF_TAG_LABEL") = "label"

'后台管理日志，1记录，0不记录
'POP_MVC.config("CMS_ADMIN_LOG") = 1

'共用静态资源存放目录文件名
POP_MVC.config("STATIC_NAME") = "static"

'动态链接后缀
POP_MVC.config( "URL_SUFFIX" ) = ".html"


'官方服务器邮件转/代发网址
POP_MVC.config( "SERVER_MAIL_URL" ) = "http://www.iaspcms.cn/default.asp?ForwardMail--ForwardMail_mail"

'去js代码
POP_MVC.Config("REQ_FILTER_JS") = 1

'保留js代码
POP_MVC.Config("REQ_IGNORE_JS") = ""

'去iframe代码
POP_MVC.Config("REQ_FILTER_IFRAME") = 1

'保留iframe代码
POP_MVC.Config("REQ_IGNORE_IFRAME") = ""

'去HTML代码
POP_MVC.Config("REQ_FILTER_HTML") = 1

'保留HTML代码
POP_MVC.Config("REQ_IGNORE_HTML") = "Content,ThreadForbidTip,ExperienceIcon,CopyRight"
''''''''''''''''''''''''''''''''''''''''''''''''
%>