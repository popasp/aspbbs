<!--#include file="config.asp"-->
<%
 '=============================================================
 ' ASP
 ' OAuth 2.0
 ' 作者：QQ 6692103
 ' 授权项：QQ_SCOPE = get_user_info,get_info
 ' 详细参数请查看腾讯互联 API 列表 
 ' 建议控制授权项的数量，如果授权项太多，很可能引起用户反感
 '=============================================================

 Class QqConnet
     Private QQ_OAUTH_CONSUMER_KEY
     Private QQ_OAUTH_CONSUMER_SECRET
     Private QQ_CALLBACK_URL
     Private QQ_SCOPE
         
     Private Sub Class_Initialize      
         QQ_OAUTH_CONSUMER_KEY = QQ_APP_ID			'<--- APP ID
         QQ_OAUTH_CONSUMER_SECRET = QQ_APP_KEY		'<--- APP KEY
         QQ_CALLBACK_URL = QQ_CALLBACL_URL			'<--- 回调地址（需要填写完整网址）
         QQ_SCOPE = "get_user_info"                 '授权项
     End Sub

     Property Get APP_ID()    
         APP_ID = QQ_OAUTH_CONSUMER_KEY    
     End Property
 
     '生成Session("State")数据
     Public Function MakeRandNum()
         Randomize
         Dim width : width = 6 '随机数长度
         width = 10 ^ (width - 1)
         MakeRandNum = Int((width*10 - width) * Rnd() + width)
     End Function
          
     'Get方法请求
     Private Function RequestUrl(url)
         Set XmlObj = Server.CreateObject("Microsoft.XMLHTTP")
         XmlObj.open "GET",url, false
         XmlObj.send
         RequestUrl = XmlObj.responseText
         Set XmlObj = nothing
     End Function
     
     'Post方法请求
     Private Function RequestUrl_post(url,data)
         Set XmlObj = Server.CreateObject("Microsoft.XMLHTTP")
         XmlObj.open "POST", url, false
         'XmlObj.setrequestheader "POST","/t/add_t HTTP/1.1"
         XmlObj.setrequestheader "Host"," graph.qq.com "
         XmlObj.setrequestheader "content-length ",len(data)   
         XmlObj.setrequestheader "content-type ", "application/x-www-form-urlencoded "
         XmlObj.setrequestheader "Connection"," Keep-Alive"
         XmlObj.setrequestheader "Cache-Control"," no-cache"
         XmlObj.send(data)
         RequestUrl_post = XmlObj.responseText
         Set XmlObj = nothing
     End Function
     
     '生成登录地址
     Public Function GetAuthorization_Code()
         Dim url, params
         url = "https://graph.qq.com/oauth2.0/authorize"
         params = "client_id=" & QQ_OAUTH_CONSUMER_KEY
         params = params & "&redirect_uri=" & QQ_CALLBACK_URL
         params = params & "&response_type=code"
         params = params & "&scope="&QQ_SCOPE
         params = params & "&state="&Session("State")
         url = url & "?" & params
         GetAuthorization_Code = (url)
     End Function     
     
     '获取 access_token
     Public Function GetAccess_Token()
         Dim url, params,Temp
         Url="https://graph.qq.com/oauth2.0/token"
         params = "client_id=" & QQ_OAUTH_CONSUMER_KEY
         params = params & "&client_secret=" & QQ_OAUTH_CONSUMER_SECRET
         params = params & "&redirect_uri=" & QQ_CALLBACK_URL
         params = params & "&grant_type=authorization_code"
         params = params & "&code="&Session("Code")
         params = params & "&state="&Session("State")
         url = Url & "?" & params
         Temp=RequestUrl(url)
         Temp=split(Temp,"&")(0)
         Temp=replace(Temp,"access_token=","")
         GetAccess_Token=Temp
     End Function
     
     '检测是否合法登录
     Public Function CheckLogin()
         Dim Code,mState
         Code=Trim(Request.QueryString("code"))
         mState=Trim(Request.QueryString("state"))
         If Code<>"" Then
             CheckLogin = True
             Session("Code")=Code
         Else
             CheckLogin = False
         End If
     End Function
        
     '获取 Openid
     Public Function Getopenid()
         Dim url, params,Temp
         url = "https://graph.qq.com/oauth2.0/me"
         params = "access_token="&Session("Access_Token")
         url = Url & "?" & params
         Temp=RequestUrl(url)
         Temp=split(Temp,"openid"":""")(1)
         Temp=split(Temp,"""}")(0)
         Getopenid=Temp
     End Function

     '获取用户信息
     Public Function GetUserInfo()
         Dim url, params, result
         url = "https://graph.qq.com/user/get_user_info"
         params = "oauth_consumer_key=" & QQ_OAUTH_CONSUMER_KEY
         params = params & "&access_token=" & Session("Access_Token")
         params = params & "&openid=" & Session("Openid")
         url = url & "?" & params
         GetUserInfo = RequestUrl(url)
     End Function 

     '格式化 Json
     Public Function FormatJson(fString) 
         If fString<>"" Then 
         fString = trim(fString) 
         fString = Replace(fString,"""","""")
         fString = Replace(fString," ","")
         FormatJson = fString
     End If 
     End Function

     '获取昵称
     Public Function GetUserName(json)
         json = Split(json, """nickname"":""")(1)
         GetUserName = Split(json, """")(0)
     End Function

 End Class
 %>