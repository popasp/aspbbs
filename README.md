### 概述
ASPBBS是一款基于ASP程序开发的微型论坛系统，前端采用了layui，代码精炼，样式美观。 \
如果想在留言板与大型论坛之间寻找一个替代品，那么ASPBBS是一个不错的选择。 \
目前支持三种数据库access、mysql、sqlite3，数据库均放在置在core/data/文件夹下。一般情况下IIS默认支持access的数据库驱动，mysql与sqlite3的数据库驱动需要额外安装，可以到 http://version.popasp.com/ 免费下载。mysql的数据库需要手动安装。 \
该系统遵循 GPL 协议，免费开源。本系统作为大家学习交流使用，任何使用该系统产生的问题与法律纠纷作者概不负责。

### 关于作者
- 作者：王兴东，山西运城河津人
- QQ：709701017

微信与QQ同号。
- QQ群1：124648143
- QQ群2：753640534

如果没有特殊紧急事情，请不要打扰作者。

### 手册与文档
手册内容挺多，请大家访问下面的网址获取详细内容: \
http://www.popasp.com/iaspcms/label_aspbbs/

### 关于版本升级
请大家关注网站: http://www.iaspcms.cn 以便下载最新版本 
- zip 文件下载： https://gitee.com/popasp/aspbbs/tree/release_files/
- 码云发行版下载：https://gitee.com/popasp/aspbbs/releases


### 关于捐赠
如果觉得ASPBBS不错，想给作者捐赠以示鼓励，可以通过
- <img src="https://gitee.com/popasp/aspbbs/tree/master/templates/bbs/res/images/donate.gif">
- QQ: 709701017
- 微信: 709701017

这几个账户来转账，在此表示特别地感谢！

### 关于使用协议
请您遵守使用地的法律法规合法使用。 \
本系统不采集使用网站的信息。

### 更新历史

- 2021.6.20 0.9.9版本发布
- 2021.6.20 1.0.0版本发布
  - 删除了文件中多余的bbs.asp
  - 添加了 /configs.class.asp
  - 修改了 /default.asp ，改 set config = T_( "configs" )
- 2021.6.20 1.0.1版本发布
  - 对 /configs.class.asp 中的各项参数进行了详细说明
  - 修改了 core/extend/bbs/topic.class.asp ，解决了错误提示无权限编辑自己的帖子
- 2021.6.20 1.0.2版本发布
  - 修复了一些bug
- 2021.6.23 1.0.3
  - 将栏目、幻灯片、广告、友链从配置文件移到了后台
  - 优化了邮件提示，提示更加友好
  - 论坛关闭后，会将开启网址发到管理员邮箱
- 2021.7.14 1.0.4
  - 添加、修改帖子时不显示添加帖子的固定bar
  - 手机版时添加、修改帖子时，图片最大显示30%
  - 添加了捐赠功能
  - 修复了已知的bug
- 2021.7.15 1.0.5
  - 修复了签到活跃榜NaN与undefined的取值错误bug
- 2021.7.24 1.0.7
  - 添加了系统管理模块，含初始化、压缩、备份、还原数据库，数据库转换、切换功能
  - 添加了安全策略功能
  - 完善了模板细节
- 2021.8.12 1.0.8
  - 纠正了1.0.7已知的几个bug
  - 添加了冗余文件检测与空文件检测功能
  - 添加了用户管理模块