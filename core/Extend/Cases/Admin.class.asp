<%
Class Admin  'Extends bbs--Common
	private tableName,orderStr,auditStatus
	private sqlfile,extendName
	sub initialize
		if S_("IsAdmin") <> 1 then
			that.error("无权操作!")
		end if	
	
		tableName = "self_GuestCases"
		extendName = "Cases"
		
		set bookConfig = XM_(  extendName & "--Configs" ).db.path("Configs/Config").field("ConfigName,ConfigValue").getObject
		that.d("bookConfig") = bookConfig		
		
		objCMS.cmsPrefix = "aspbbs"
		guestConfig.DefaultTemplate = extendName
		objCMS.htmlFilePath = "html"	
		isWap = checkWap()		
		sqlfile = POP_MVC.config("CORE_NAME") & "/Extend/" & extendName & "/" & C_("DB_TYPE") & ".sql"	
		
		if S_("adminId") <> "" then
			that.d("mycaseUrl") = "?" & extendName & "--User_Index_" & S_("adminId")
		else
			that.d("mycaseUrl") = getLoginUrl()
		end if
		
		that.d("ExtendName") = extendName
	end sub
	
	''''''以下是站长允许的操作''''''	
	'安装数据表
	sub install
		Call K_("bbs--Tools").InstallTable( extendName , tableName, sqlfile )
	end sub
	
	'缷载数据表
	sub unstall
		Call K_("bbs--Tools").UnstallTable( extendName , tableName )
	end sub
	
	'生成建表语句
	sub createSql
		Call K_("bbs--Tools").createSql( tableName , sqlfile )
	end sub
	
	'查看建表语句
	sub viewSql
		Call K_("bbs--Tools").viewSql( sqlfile )
	end sub
	
	'查看XML配置文件
	sub viewXML
		Call K_("bbs--Tools").viewSql( "./templates/" & extendName & "/#data/#book#.xml" )
	end sub
	''''''以上是站长允许的操作''''''
	
	''''''''''''''''''''下面这组方法可直接用在插件中'''''''''''''''''''
	sub safeTestTest
		dim str
		str = K_("bbs--FileTool").safeTest
	end sub
	
	sub safeTestClear
		Call K_("bbs--FileTool").redundancyClear
		that.redirect("?" & extendName & "--Admin#safeTest")
	end sub
	
	'冗余文件检测
	sub redundancyTest
		dim str
		K_("bbs--FileTool").soTableFields = Array( tableName , "IndexImage" )
		str = K_("bbs--FileTool").redundancy
	end sub
	
	sub redundancyClear
		Call K_("bbs--FileTool").redundancyClear
		that.redirect("?" & extendName & "--Admin#redundancy")
	end sub
	
	sub emptyTest
		dim str
		str = K_("bbs--FileTool").emptyFolder
	end sub
	
	sub emptyClear
		Call K_("bbs--FileTool").emptyFolderClear
		that.redirect("?" & extendName & "--Admin#emptyFolder")
	end sub
	''''''''''''''''''''上面这组方法可直接用在插件中'''''''''''''''''''		
	
	'首页，跳转到设置页面
	sub index
		call settings
	end sub
	
	'将<1.6版本数据导入
	sub transferDB
		if S_("GroupID") <> 1 then
			that.error( "仅超管才可以操作!" )
		end if
	
		dim rs,where,rs2,dict,IndexImage,ImagePath
		'set rs = B_("self_GuestTopic").where( "SortID = " & POP_MVC.config("BBS_CASE_SORTID") ).field("TopicID,Title,AddTime,UserID").select
		set rs = B_("self_GuestTopic").field("TopicID,Title,AddTime,UserID").select
		
		set where = D_ 
		do while not rs.eof
			where("Title") = rs("Title")
			where("AddTime") = rs("AddTime")
			where("UserID") = rs("UserID")
			
			'查找案例表中是否存在
			set rs2 = B_( tableName ).where( where ).field("TopicID").find
			
			'如果不存在则转移
			if rs2.eof then			
				'得到一条记录
				set dict = B_("self_GuestTopic").where( rs("TopicID") ).getRow
				
				'封面图片
				IndexImage = dict("IndexImage")
				
				'封面图片转移到Cases/下面
				if POP_MVC.file.isFile( IndexImage ) then
				
					'创建文件夹
					Call POP_MVC.CreateFolder( POP_MVC.config("CMS_UPLOAD_NAME") & "/" & extendName & "/" & rs("UserID") )
					
					'转移后的位置
					ImagePath = POP_MVC.config("CMS_UPLOAD_NAME") & "/" & extendName & "/" & rs("UserID") & "/" & POP_MVC.file.basename( IndexImage )
					
					'如果目标图片不存在，则转移
					if not POP_MVC.file.isFile( ImagePath ) then
						Call POP_MVC.file.rename( IndexImage , ImagePath )
					end if
				end if
				Call B_( tableName ).data( dict ).Add
			end if
			that.u(rs2)
			rs.MoveNext
		loop
		that.u(rs)
		that.success( "数据与图片导入完成" )
	end sub
	
	'<1.6版，案例在表Topic，1.6后转入至Cases表，实现插件分离
	'将未经转入Upload/Cases的IndexImag字段的图片转移至Upload/Cases
	sub transferImages
		if S_("GroupID") <> 1 then
			that.error( "仅超管才可以操作!" )
		end if	
	
		dim rs,IndexImage,ImagePath
		set rs = B_( tableName ).field("TopicID,IndexImage,UserID").select
		
		do while not rs.eof
			'封面图片
			IndexImage = rs("IndexImage")
			
			'封面图片转移到Cases/下面
			if POP_MVC.file.isFile( IndexImage ) then
				if POP_MVC.String.iStartsWith( IndexImage , POP_MVC.config("CMS_UPLOAD_PATH") & "/" ) then
					'创建文件夹
					Call POP_MVC.CreateFolder( POP_MVC.config("CMS_UPLOAD_NAME") & "/" & extendName & "/" & rs("UserID") )
					'转移后的位置
					ImagePath =  POP_MVC.config("CMS_UPLOAD_NAME") & "/" & extendName & "/" & rs("UserID") & "/" & POP_MVC.file.basename( IndexImage )
					
					'如果目标图片不存在，则转移
					if not POP_MVC.file.isFile( ImagePath ) then
						Call POP_MVC.file.rename( IndexImage , ImagePath )
						Call B_(tableName).where( rs("TopicID") ).setField( "IndexImage" , ImagePath )
					end if
				end if
			end if
			rs.MoveNext
		loop
		that.u(rs)
		that.success( "图片转移成功" )
	end sub
	
	'显示某个用户提交的案例
	'仅管理员与自已可以查看
	sub user
		dim where,rs,userId,theYear
		
		userId = that.get("id")
		
		if S_("IsAdmin") <> 1 AND UserID - S_("adminId") <> 0 then
			that.error("您无权查看该页面")
		end if
		
		theYear = year( now() )
	
		set where = D_

		where( "a.UserID = b.UserID" ) = null
		where( "a.UserID" ) = userId
		
		if orderStr = "" then
			orderStr = "TopicID DESC"
		end if
		
		set rs = B_("self_GuestConfig").from("{prefix}self_GuestCases as a,{prefix}User as b").onlysql(0).order(orderStr).field("TopicID,Title,Content,a.AddTime,a.UserID,ZanCount,CaiCount,a.IndexImage,a.ContentSource,b.LoginName,b.Nickname").page("null,20").where( where ).select
		
		that.u(where)

		that.d("case") = rs
		that.d("theYear") = theYear

		objCMS.show( "Admin/user" )
	end sub	

	
	'显示指定年份的所有案例
	'仅管理员有查看及操作权限
	'未指定年份取今年数据
	sub audit
		if S_("IsAdmin") <> 1 then
			that.error("您无权查看该页面")
		end if
	
		dim where,rs,theYear
		
		theYear = that.get("id")
		
		if not isDate( theYear & "-1-1" ) then
			theYear = ""
		end if
		
		if theYear = "" then
			theYear = year( now() )			
			POP_MVC.get("id") = theYear
		end if
		
		set where = D_
				
		if C_("DB_TYPE") = "sqlite3" then
			where( "strftime('%Y',AddTime) = '" & theYear & "'" ) = null
		else
			where( "year(AddTime)" ) = theYear
		end if
		where( "a.UserID = b.UserID" ) = null
		if not isEmpty( auditStatus ) then
			where( "a.ContentStatus" ) = auditStatus
		end if
		
		if orderStr = "" then
			orderStr = "ContentStatus ASC,TopicID DESC"
		end if
		
		set rs = B_("self_GuestConfig").from("{prefix}self_GuestCases as a,{prefix}User as b").onlysql(0).order(orderStr).field("a.TopicID,a.Title,Content,a.AddTime,a.ContentStatus,a.UserID,ZanCount,CaiCount,a.IndexImage,a.ContentSource,b.LoginName,b.Nickname").page("null,20").where( where ).select

		that.u(where)
		
		
		that.d("case") = rs		
		that.d("arrYear") = POP_MVC.arr.range( year( now() ) ,2016 )
		that.d("theYear") = theYear
		that.d("curYear") = year(now)
		objCMS.show("Admin/audit")
	end sub
	
	'显示未审核案例
	sub audit0
		auditStatus = 0
		Call audit
	end sub
	
	'显示通过审核案例
	sub audit1
		auditStatus = 1
		Call audit
	end sub
	
	'显示已拒绝案例
	sub audit2
		auditStatus = 2
		Call audit
	end sub
	
	'删除案例
	'作者只能软删除
	'管理员第一次操作是软删除，第二次操作则会永久删除
	Sub Remove
		dim id,rs,authInfo,sTip
		id = that.req("id")
		
		'来路不对
		if not that.isSelfOrigin then
			that.error("非法操作")
		end if			
		
		'不是有效的ID
		if not that.isId( id ) then
			that.error( "非法操作" )
		end if
		
		'根据ID取记录
		set rs = B_(tableName).where(id).find
		

		
		if rs.eof then
			that.error( "该案例不存在" )
		end if
	
		authInfo = K_("bbs--Public").AuthCompare( rs("UserID") , "的案例进行删除操作！" )
			
		if authInfo <> "" then
			Call K_( "bbs--Public" ).CtrlLog( 0, authInfo , authInfo )
		end if
		
		if S_("IsAdmin") <> 1 then
			if S_("adminId") = rs("UserID") then
				Call B_(tableName).where(id).setField( "ContentStatus" , 2 )
				objCMS.clearCache
				
				sTip = "删除成功"
				Call K_( "bbs--Public" ).CtrlLog( 1, "用户ID: " & S_("adminId") & " ，对案例ID: " & id & " 软" & sTip , sTip )
			else
				Call K_( "bbs--Public" ).CtrlLog( 0, "用户ID: " & S_("adminId") & " ，对案例ID: " & id & " 非作者非管理员，无权删除" , "您不是管理员，无权删除他人的案例！" )
			end if
		else
			if rs("ContentStatus") = 1 then
				Call B_(tableName).where(id).setField( "ContentStatus" , 2 )
				objCMS.clearCache
				
				sTip = "删除成功"
				Call K_( "bbs--Public" ).CtrlLog( 1, "用户ID: " & S_("adminId") & " ，对案例ID: " & id & " 软" & sTip , sTip )
			else
				Call CheckSuperAdmin
			
				'彻底删除案例的时候，还要彻底删除回帖
				B_(tableName).where(id).remove
				B_("self_GuestReply").where( that.dict( "TopicID" , id ) ).remove
				objCMS.clearCache
				sTip = "删除成功"
				Call K_( "bbs--Public" ).CtrlLog( 1, "用户ID: " & S_("adminId") & " ，对案例ID: " & id & " " & sTip , Array(sTip ,"" ) )
			end if
		end if		
	End Sub		
	
	'设置字段的值，加精/置顶
	'这儿的操作进行了权限验证
	Sub setField
		dim field,value,UserID,id,authInfo,sTip
		if not that.isSelfOrigin then
			that.error("非法操作")
		end if	
		
		id		= that.req("id")
		field	= that.req("field")
		value	= that.req("rank")
		UserID	= B_( tableName ).where( id ).field("UserID").getOne
		
		authInfo = K_("bbs--Public").AuthCompare( UserID , "的案例进行该操作！" )
		
		if authInfo <> "" then
			Call K_( "bbs--Public" ).CtrlLog( 0, "用户ID: " & S_("adminId") & " ，设置字段对案例ID: " & id & " " & authInfo , authInfo )
		end if				
		objCMS.clearCache
		
		if B_(tableName).where( id ).setField( field , value  ) then
			if field = "ContentStatus" then
				if value = 1 then
					sTip = "恢复成功"
					Call K_( "bbs--Public" ).CtrlLog( 1, "用户ID: " & S_("adminId") & " ，对案例ID: " & id & " " & sTip , sTip )
				elseif value = 2 then
					sTip = "拒绝成功"
					Call K_( "bbs--Public" ).CtrlLog( 1, "用户ID: " & S_("adminId") & " ，对案例ID: " & id & " " & sTip , sTip )
				end if
			else
				that.success("操作成功")
			end if			
		end if		
	End Sub	
	
	'判断单个用户是否被允许发帖
	Private Sub CheckThreadStatus( UserID )	
		if B_("User").where( UserID ).field("ThreadStatus").getOne = 0 then
			K_( "bbs--Public" ).showError( guestConfig.ThreadForbidTip  )
		end if	
	End Sub	
	
	'配置
	sub settings
		dim mailType,data,html
		
		'取得配置
		set data = XM_( extendName & "--Configs").db.path("Configs/Config").select		
		html = P_("flyui4xml").PageConfigContent(data)		
		that.d("htmlstr") = html
		
		'取得图片
		filepath = POP_MVC.appPath & "/runtime/redundancy/table.txt"
		if POP_MVC.file.isfile(filepath) then
			time1 = POP_MVC.file.mtime( filepath )
			time2 = POP_MVC.file.mtime( POP_MVC.config("CMS_UPLOAD_PATH") )
			if  DateDiff("s", time2, time1) > 0 then
				that.d("loopstr") = K_("bbs--FileTool").file_get_test("redundancy")
			end if
		end if
		
		objCMS.show( "Admin/set" )
	end sub	
	
	'js自动填充
	sub jsAuto
		dim dict
		set dict = XM_( extendName & "--Configs").db.path("Configs/Config").field("ConfigName,ConfigValue").getKeyValue
		
		Call K_("bbs--Tools").js_Auto( dict )
	end sub	
	
	'修改配置值
	sub doEditConfigs
		dim key,dict,MailAlert
		set dict = POP_MVC.form2dict
		
		for each key in dict
			Call XM_("Cases--Configs").db.path("Configs/Config[ConfigName='"& key &"']").MergeField( "ConfigValue" , dict(key) )
		next

		Call K_( "bbs--Public" ).CtrlLog( 1, "用户ID: " & S_("adminId") & " ，修改了配置" , "保存成功" )
	end sub
end Class
%>