<%
Class Index  'Extends bbs--Top
	private tableName,orderStr,auditStatus
	private extendName
	sub initialize
		tableName = "self_GuestCases"
		
		objCMS.cmsPrefix = "aspbbs"
		guestConfig.DefaultTemplate = "Cases"
		objCMS.htmlFilePath = "html"

		extendName = "Cases"
		that.d("ExtendName") = extendName
		
		if S_("adminId") <> "" then
			that.d("mycaseUrl") = "?" & extendName & "--User_Index_" & S_("adminId")
		else
			that.d("mycaseUrl") = getLoginUrl()
		end if
	end sub
	
	'显示所有案例
	'所有用户都可以查看
	sub index
		dim obj,where,rs,theYear
		set obj = objCMS
		
		theYear = that.get("id")
		
		if not isDate( theYear & "-1-1" ) then
			theYear = ""
		end if
		
		if theYear = "" then
			theYear = year( now() )
			
			POP_MVC.get("id") = theYear
		end if
		
		set where = D_
				
		if C_("DB_TYPE") = "sqlite3" then
			where( "strftime('%Y',AddTime) = '" & theYear & "'" ) = null
		else
			where( "year(AddTime)" ) = theYear
		end if
		
		where( "a.UserID = b.UserID" ) = null
		where( "ContentStatus" ) = 1
		
		if orderStr = "" then
			orderStr = "TopicID DESC"
		end if
		
		set rs = B_("self_GuestConfig").from("{prefix}self_GuestCases as a,{prefix}User as b").onlysql(0).order(orderStr).field("TopicID,Title,Content,a.AddTime,a.UserID,ZanCount,CaiCount,ZanUser,a.IndexImage,a.ContentSource,b.LoginName,b.Nickname").page("null,20").where( where ).select
		
		that.u(where)
		
		that.d("case") = rs		
		that.d("arrYear") = POP_MVC.arr.range( year( now() ) ,2016 )
		that.d("theYear") = theYear
		that.d("curYear") = year(now)
		
		templatePath= obj.getTemplatePath( "Index/case" )
		obj.load(templatePath)	
		obj.parseHtml	
		objCMS.output
	end sub

	
	'按点赞排行显示案例
	sub ranking
		orderStr = "ZanCount DESC"
		Call index
	end sub
end Class
%>