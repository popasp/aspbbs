<%
Class Thread
	sub init
		'表名 
		'db是popasp_access类的实例化
		db.tableName = "self_GuestCases"
		
		'主键
		db.prikey = "TopicID"
		
		db.auto_ = Array( _
			Array( "IsTop" ,0,  1 ) _
			,Array( "IsRecommend" ,0,  1 ) _
			,Array( "IsHeadline" ,0,  1 ) _
			,Array( "IsFeatured" ,0,  1 ) _
			,Array( "IsNoComment" ,0,  1 ) _
			,Array( "Visits" ,0,  1 ) _
			,Array( "Star" ,0,  1 ) _	
			,Array( "AddTime" ,"POP_MVC.FormatDate( now() , ""YYYY-MM-DD HH:II:SS"" )",  1 , "function" ) _
			,Array( "EditTime" ,"POP_MVC.FormatDate( now() , ""YYYY-MM-DD HH:II:SS"" )",  2 , "function" ) _
			,Array( "DownURL" ,"",  1  ) _
			,Array( "EditIP" , "" ,  1 ) _
			,Array( "EditIpAddr" , "" , 1 )_
			,Array( "ReplyCount" ,"0",  1 ) _
			,Array( "UserID" , Session("adminId") ,  1 ) _
			,Array( "ContentStatus" , iif(guestConfig.ThreadAddCheck=0 OR S_("IsAdmin")=1,1,0) ,  1 ) _
			,Array( "ContentStatus" , iif(guestConfig.ThreadEditCheck=0 OR S_("IsAdmin")=1,1,0) ,  2 ) _
			,Array( "ZanCount" , 0 ,  1 ) _
			,Array( "CaiCount" , 0 ,  1 ) _
			,Array( "AnswerReward" , 0 ,  1 ) _
			,Array( "IP" , "get_client_ip" ,  1, "function" ) _
			,Array( "IpAddr" , "cases--Thread.getIpAddr2" , 1 , "callback" )_
			,Array( "EditIP" , "get_client_ip" ,  2, "function" ) _
			,Array( "EditIpAddr" , "cases--Thread.getIpAddr2" , 2 , "callback" )_
			,Array( "Title" , "cases--Thread.stripTags" , 3 , "callback" , array() )_
			,Array( "Content" , "cases--Thread.replaceLink" , 3 , "callback" , array() )_
		)
		
		'数据验证
		db.validate_ = Array( _
			 Array( "Title" ,"",  "标题不能为空"  , 1, "notempty" , 3  ) _
			,Array( "Title","1,255","标题长度不能超过255个字符" , 1 , "length" , 3 ) _
			,Array( "Content" ,"",  "内容不能为空"  , 1, "notempty" , 3  ) _
			,Array( "Content" , guestConfig.ThreadContentLimit & ",40000",  "内容长度不能少于" & guestConfig.ThreadContentLimit & "个字符"  , 1, "length" , 3  ) _
			,Array( "Content","cases--Thread.TestValidate","有效字符数太少，请修改评论再发布" , 1 , "callback" , 3 , array() ) _
			,Array( "verify","","验证码不正确" , 0 , "verify" , 3 ) _
		)
		',Array( "ContentSource","cases--Thread.SourceValidate","网址不合法" , 0 , "callback" , 3 , array() ) _ '案例同源性判断已移到插件中
	end sub
	
	function stripTags( value )
		if isNul(value) then
			stripTags = ""
		else
			stripTags = POP_MVC.String.strip_tags( value , "html" )
		end if
		'去掉换行
		stripTags = Replace(stripTags, Chr(10), "")
		stripTags = Replace(stripTags, Chr(13), "")
		
		'将连续三个空格替换成空
		stripTags = Replace(stripTags, "   ", "")
	End function
	
	'替换链接
	function replaceLink( byVal value )
		replaceLink = K_("bbs--Public").replaceLink( value )
	End function
	
	function TestValidate( Message ) 
		dim str,cnt
		if S_("IsAdmin") = 1 then
			TestValidate = true
			exit function
		end if
		str = POP_MVC.String.strip_tags( Message , "html" )
		str = replace( str , " " ,"" )
		str = POP_MVC.String.unique(str)
		if len( str ) < 5 then
			TestValidate = false
		else
			TestValidate = true
		end if
	end function
	
	
	function SourceValidate( url ) 
		SourceValidate = true
		if bookConfig.CaseSourceSelf = 1 then
			if not POP_MVC.isSameOrigin( "" ,url ) then
				SourceValidate = false
			end if
		end if
	end function
	
	Function getIpAddr2
		dim ip
		ip = get_client_ip
		getIpAddr2 = getIpAddr(ip)
	End Function
	
	
	Function getIpAddr( ip )
		dim attr
		
		attr = getTableIpAddr( "self_GuestTopic" , "IP" , ip , "IpAddr" )
		if not isNul(attr) then
			getIpAddr = attr
			exit function
		end if
		
		attr = getTableIpAddr( "self_GuestTopic" , "EditIP" , ip ,  "EditIpAddr" )
		if not isNul(attr) then
			getIpAddr = attr
			exit function
		end if	
		
		attr = getTableIpAddr( "self_GuestReply" , "IP" , ip ,  "IpAddr" )
		if not isNul(attr) then
			getIpAddr = attr
			exit function
		end if	
		
		attr = getTableIpAddr( "self_GuestReply" , "EditIP" , ip ,  "EditIpAddr" )
		if not isNul(attr) then
			getIpAddr = attr
			exit function
		end if
		
		if getIpAddr = "" then
			getIpAddr = K_("bbs--ip").getTQQWryAddr(ip)
		end if
	End Function
	
	Function getTableIpAddr(table,IpField , IpValue,AttrField)
		getTableIpAddr = K_("bbs--ip").getTableIpAddr( table,IpField,IpValue,AttrField )
	End Function
End Class
%>