<%
Class User  'Extends bbs--Common
	private tableName,orderStr,auditStatus
	private sqlfile,extendName
	sub initialize
		tableName = "self_GuestCases"
		extendName = "Cases"
		that.d("ExtendName") = extendName
		
		set bookConfig = XM_(  extendName & "--Configs" ).db.path("Configs/Config").field("ConfigName,ConfigValue").getObject
		that.d("bookConfig") = bookConfig		
		
		objCMS.cmsPrefix = "aspbbs"
		guestConfig.DefaultTemplate = extendName
		objCMS.htmlFilePath = "html"	
		isWap = checkWap()		
		sqlfile = POP_MVC.config("CORE_NAME") & "/Extend/" & extendName & "/" & C_("DB_TYPE") & ".sql"	
		
		if S_("adminId") <> "" then
			that.d("mycaseUrl") = "?" & extendName & "--User_Index_" & S_("adminId")
		else
			that.d("mycaseUrl") = getLoginUrl()
		end if
	end sub

	
	'显示某个用户提交的案例
	'仅管理员与自已可以查看
	sub index
		dim obj,where,rs,userId
		set obj = objCMS
		
		userId = S_("adminId")

		if that.get("id") - userId <> 0 then
			that.error( "本人才可以查看自己的案例！" )
		end if
		
		if S_("IsAdmin") <> 1 AND UserID - S_("adminId") <> 0 then
			that.error("您无权查看该页面")
		end if
	
		set where = D_

		where( "a.UserID = b.UserID" ) = null
		where( "a.UserID" ) = userId
		
		if orderStr = "" then
			orderStr = "TopicID DESC"
		end if
		
		set rs = B_("self_GuestConfig").from("{prefix}self_GuestCases as a,{prefix}User as b").onlysql(0).order(orderStr).field("TopicID,Title,Content,ContentStatus,a.AddTime,a.UserID,ZanCount,CaiCount,a.IndexImage,a.ContentSource,b.LoginName,b.Nickname").page("null,20").where( where ).select
		
		that.u(where)

		that.d("case") = rs
		that.d("user") = objCMS.getUserData(UserID)
		objCMS.show( "User/index" )
	end sub	
	
	
	'添加案例
	sub doAddCase
		dim dict,dict2,id,sTip
		
		'判断用户是否禁止发帖
		Call CheckThreadStatus( S_("adminId") )			
		
		set dict = POP_MVC.form2dict
		if dict("agree") <> "on" then
			sTip = "需要同意才能提交案例"
			Call K_("bbs--Public").CtrlLog( 0 , "用户ID: " & S_("adminId") & " 添加案例时，" & sTip , "您" & sTip )
		end if
		set dict2 = D_
		dict2("Title") = dict("title")
		dict2("ContentSource") = dict("link")
		dict2("IndexImage") = dict("cover")
		dict2("Content") = dict("desc")
		dict2("Title") = dict("title")
		
		'管理员直接通过审核，而普通用户需要通过审核
		if S_("IsAdmin") = 1 then
			dict2("ContentStatus") = 1
		else
			dict2("ContentStatus") = 0
		end if
		
		if not K_("bbs--Public").ModelCreate("cases--Thread",dict2,1) then
			that.error( M_( "cases--Thread" ).db.error )
		end if
		
		id = K_("bbs--Public").ModelAdd( "cases--Thread" , null )
		
		if  id > 0 then
			Call K_( "bbs--Public" ).CtrlLog( 1, "用户ID: " & S_("adminId") & " ，添加待审案例 ID: " & id , "您的案例已成功提交，审核通过后将会显示在该页面。敬请留意社区系统消息" )
		else
			Call K_( "bbs--Public" ).CtrlLog( 0, "用户ID: " & S_("adminId") & " ，添加案例失败" , "添加失败，请重新提交" )
		end if	
	end sub
	
	'修改案例
	sub doEditCase
		dim dict,dict2,id,sTip
		
		'判断用户是否禁止发帖
		Call CheckThreadStatus( S_("adminId") )	
		
		set dict = POP_MVC.form2dict
		if dict("agree") <> "on" then
			that.error("您需要同意才能提交案例")
		end if
		set dict2 = D_
		dict2("Title") = dict("title")
		dict2("ContentSource") = dict("link")
		dict2("IndexImage") = dict("cover")
		dict2("Content") = dict("desc")
		dict2("Title") = dict("title")
		dict2("TopicID") = dict("id")
		
		id = dict("id")
		
		'管理员直接通过审核，而普通用户需要通过审核
		if S_("IsAdmin") = 1 then
			dict2("ContentStatus") = 1
		else
			dict2("ContentStatus") = 0
		end if
		
		if not K_("bbs--Public").ModelCreate("cases--Thread",dict2,2) then
			sTip = M_( "cases--Thread" ).db.error
			Call K_( "bbs--Public" ).CtrlLog( 0, sTip , sTip )
		end if
		
		if  K_("bbs--Public").ModelEdit( "cases--Thread" , null ) then
			Call K_( "bbs--Public" ).CtrlLog( 1, "用户ID: " & S_("adminId") & " ，成功修改案例 ID: " & id , "您的修改案例已成功提交，审核通过后将会显示在该页面。敬请留意社区系统消息" )
		else
			Call K_( "bbs--Public" ).CtrlLog( 0, "用户ID: " & S_("adminId") & " ，修改案例失败 ID: " & id , "修改失败，请重新提交" )
		end if	
	end sub
	
	'判断是否为超管
	Private Sub CheckSuperAdmin
		if S_("GroupID") <> 1 then
			that.error("您不是超级管理员，无权进行彻底删除操作！")
		end if
	End Sub
	
		
	'取一条记录
	sub find
		dim obj,where,rs,dict
		set obj = objCMS
		
		set where = D_

		where("a.TopicID") = that.req("id")
		
		set dict = B_("self_GuestConfig").from("{prefix}self_GuestCases as a,{prefix}User as b").onlysql(0).order(orderStr).field("a.TopicID,a.Title,Content,a.AddTime,a.ContentStatus,a.UserID,ZanCount,CaiCount,a.IndexImage,a.ContentSource,b.LoginName,b.Nickname").where(where).getRow
		dict("status") = that.ajaxSuccessStatus
		
		that.u(where)
		
		that.ajaxData(dict)
	end sub
	
	'对案例点赞
	sub doZan
		'{"status":0,"msg":"","praise":9}
		dim dict,rs,where,id,sTip,ZanUser,UserID
		
		id = that.req("id")
		UserID = S_("adminId")
		
		'得到回岾ID
		set where = D_
		where("TopicID") = id
		where("a.UserID = b.UserID") = null
		
		'不能对自己点赞
		set rs = B_(objCMS.baseTable).from("{prefix}self_GuestCases as a,{prefix}User as b").where( where ).field("b.LoginName,b.Nickname,b.UserID,ZanUser").find		
		if rs("UserID") - S_("adminId") = 0 and S_("IsAdmin") - 1 <> 0 then
			that.u(rs)
			
			sTip = "对不起，不能对自己点赞哦！"
			Call K_("bbs--Public").CtrlLog( 0 , "用户ID: " & S_("adminId") & " 对案例ID:" & id & " " & sTip , Array( sTip ,-1 ) )
		end if
		
		ZanUser = rs("ZanUser")
		
		'存在时还得判断 TopicZan 是否包含了id
		if IsNul( ZanUser ) then
			Call B_(tableName).where( id ).setField( "ZanUser" , UserID )		
		elseif inStr( "," & ZanUser & "," , "," & UserID & "," ) > 0 then
			that.u(rs)
			sTip =  "已经赞过了"
			Call K_("bbs--Public").CtrlLog( 0 , "用户ID: " & S_("adminId") & " 对案例ID:" & id & " " & sTip , Array( "您" & sTip ,-1 ) )
		else
			Call B_(tableName).where( id ).setField( "ZanUser" , ZanUser & ","  & UserID )
		end if

		that.u(rs).u(dict)
		B_(tableName).where(id).setInc("ZanCount")
		
		set dict = D_
		dict("status") = that.ajaxSuccessStatus
		dict("msg") = ""
		dict("praise") = B_("self_GuestCases").where(id).field("ZanCount").getOne
		that.ajaxData(dict)
	end sub	
	
	'通过ajax返回点赞的用户
	sub listCaseZan
		dim id,dict,where,rs,arr,TopicID,ZanUser
		
		'案例ID
		TopicID = that.req("id")
		
		'取标题与用户ID
		set rs = B_( tableName ).where(TopicID).field( "TopicID,ZanUser,Title" ).find
		
		'案例不存在时
		if rs.eof then
			that.error( Array("访问的案例不存在") )
		end if	
	
		ZanUser = rs("ZanUser").value
		
		set dict = D_
		dict("title") = rs("Title")
		if not isNul(ZanUser) then
			set where = D_
			where("UserID") = Array( "in" , split(ZanUser , ",") )
			set rs = B_( objCMS.baseTable ).from("{prefix}User").field("UserID as id, Nickname as username, avatar").where( where ).page("1,500").select
			dict.add "data",POP_MVC.rs2dict(rs)
		else
			dict.add "data",D_
		end if	
		dict("status") = that.ajaxSuccessStatus	
		that.u(rs)
		that.ajaxData( dict )
	end sub
	
	'上传图片
	sub upload
		dim filename,dict,logID
		POP_MVC.config("UPLOAD_ALLOW_TYPES") = POP_MVC.config("UPLOAD_IMAGE_TYPES")
		filename = POP_MVC.upload("file")
		set dict  = D_
		filename = K_("bbs--Public").upload( "Topic-upload" , logID )
		if filename <> "" then
			dict("status") = that.ajaxSuccessStatus
			dict("url") = filename
			dict("filename") = POP_MVC.file.basename( filename )			
		else
			dict("status") = that.ajaxErrorStatus
			dict("msg") = POP_MVC.Uploader.description
		end if
		that.ajaxData(dict)
	end sub	
	
	'判断单个用户是否被允许发帖
	Private Sub CheckThreadStatus( UserID )	
		if B_("User").where( UserID ).field("ThreadStatus").getOne = 0 then
			K_( "bbs--Public" ).showError( guestConfig.ThreadForbidTip  )
		end if	
	End Sub
end Class
%>