<%
Class Admin 'Extends bbs--Common
	private isWap,orderStr
	private tableName,sqlfile,extendName
	
	sub initialize
		tableName = "self_GuestComplaint"
		extendName = "Complaint"
		
		
		set bookConfig = XM_(  extendName & "--Configs" ).db.path("Configs/Config").field("ConfigName,ConfigValue").getObject
		that.d("bookConfig") = bookConfig
		
		'此句须写在set bookConfig的后面才能执行
		if not DealCplPower() then
			that.error( "您无权操作！" )
		end if	
	
		objCMS.cmsPrefix = "aspbbs"
		guestConfig.DefaultTemplate = extendName
		objCMS.htmlFilePath = "html"
		isWap = checkWap()		
		sqlfile = POP_MVC.config("CORE_NAME") & "/Extend/" & extendName & "/" & C_("DB_TYPE") & ".sql"
	end sub

	
	''''''以下是站长允许的操作''''''	
	'安装数据表
	sub install
		Call K_("bbs--Tools").InstallTable( extendName , tableName, sqlfile )
	end sub
	
	'缷载数据表
	sub unstall
		Call K_("bbs--Tools").UnstallTable( extendName , tableName )
	end sub
	
	'生成建表语句
	sub createSql
		Call K_("bbs--Tools").createSql( tableName , sqlfile )
	end sub
	
	'查看建表语句
	sub viewSql
		Call K_("bbs--Tools").viewSql( sqlfile )
	end sub
	
	'查看XML配置文件
	sub viewXML
		Call K_("bbs--Tools").viewSql( "./templates/" & extendName & "/#data/#book#.xml" )
	end sub	
	
	''''''以上是站长允许的操作''''''
	
	
	sub index
		Call settings
	end sub

	
	private sub sendMsg( FromUserID,ToUserID,msg )
		Call M_("bbs--Message").SendMessages( FromUserID , ToUserID , msg  )
	End Sub
	

	
	sub setResult
		dim obj,templatePath,stype
		set obj = objCMS
		
		stype = that.req("id")

		
		set result = XM_("complaint--Configs").db.path("Results/Config[ConfigID='" & stype & "']").find
		
		that.d("result") = result

		templatePath= obj.getTemplatePath( "Admin/result-set" )		
		obj.load(templatePath)	
		obj.parseHtml
		objCMS.output
	end sub	
	
	sub doSetResult
		dim dict,data
		set dict = POP_MVC.form2dict
		set data = D_
		data("ConfigName") = dict("ConfigName")
		data("ConfigTitle") = dict("ConfigTitle")
		data("ToUserMsg") = dict("ToUserMsg")
		data("ToUserMail") = dict("ToUserMail")
		data("ToCplUserMsg") = dict("ToCplUserMsg")
		data("ToCpLUserMail") = dict("ToCpLUserMail")
		data("IsToUserMsg") = dict("IsToUserMsg")
		data("IsToCplUserMsg") = dict("IsToCplUserMsg")
		data("IsToUserMail") = dict("IsToUserMail")
		data("IsToCpLUserMail") = dict("IsToCpLUserMail")
		
		Call XM_("complaint--Configs").db.path("Results/Config[ConfigName='" & data("ConfigName") & "']").MergeField( data , null )
		
		Call K_( "bbs--Public" ).CtrlLog( 1, "用户ID: " & S_("adminId") & " ，修改了配置" , Array("修改成功" , "?Complaint--Index_CloseLayer" ) )
	end sub	
	
	sub doQuickSet
		dim dict,data
		dim id,field,fieldValue
		set dict = POP_MVC.form2dict
		id = dict("id")
		field = dict("field")
		fieldValue = dict( field )
		
		Call XM_("complaint--Configs").db.path("Results/Config[ConfigID='" & id & "']").MergeField( field , fieldValue )
		
		Call K_( "bbs--Public" ).CtrlLog( 1, "用户ID: " & S_("adminId") & " ，快捷修改配置" , Array("修改成功" , "?Complaint--Index_CloseLayer" ) )
	end sub	
	
	
	sub settings
		dim templatePath,mailType,data,html
		
		set data = XM_("complaint--Configs").db.path("Configs/Config").select
		
		html = P_("flyui4xml").PageConfigContent(data)
		
		that.u(data)
		
		set data = XM_("complaint--Configs").db.path("Results/Config").field("ConfigName,ConfigTitle,ConfigID").select
		
		that.d("results") = data
		
		that.d("htmlstr") = html

		Call K_("complaint--Public").getDealCplUser
		objCMS.show( "Admin/set" )
	end sub	
	
	'修改配置值
	sub doEditConfigs
		dim key,dict,MailAlert
		set dict = POP_MVC.form2dict
		'set dict = K_("bbs--Plugin").getAjaxData
		
		for each key in dict
			Call XM_("complaint--Configs").db.path("Configs/Config[ConfigName='"& key &"']").MergeField( "ConfigValue" , dict(key) )
		next

		Call K_( "bbs--Public" ).CtrlLog( 1, "用户ID: " & S_("adminId") & " ，修改了配置" , "保存成功" )
	end sub
	
	'发送消息
	sub DealCpl( id, stype )
		dim rs,result,ToUserReward,html
		dim dict
		dim AllProgress

		'取出该条投诉记录
		set rs = B_(tableName).where(id).find
		
		if rs.eof then
			that.error( "未找到ID: " & id & " 投诉记录！" )
		end if
		
		'取出相应处理要求
		set result = XM_( "Complaint--Configs" ).db.path("Results/Config[ConfigName='" & stype & "']").find	
	
		that.d("result") = result
		
		set dict = D_
		
		'未完结则发消息，完结了，不论以后如何处理都不宜再发消息
		'if rs("IsSolved") <> 1 then
			'给投诉者发送消息
			if result("IsToUserMsg") = 1 then
				html = result("ToUserMsg")
				
				if rs("CplType") = 1 then
					html = replace( html , "{#ID#}" , "{p:" & rs("ParentID") & "}" )
				elseif rs("CplType") = 2 then
					html = replace( html , "{#ID#}" , "{r:" & rs("ParentID") & "}" )
				end if				
				html = K_("bbs--Public").replaceLink( html )
				
				objCMS.content = html
				objCMS.parseHtml
					
				Call M_("bbs--Message").SendMessages( S_("adminId") , rs("UserID") , objCMS.content  )
				dict("MsgStatus") = 1
			
			end if

			'给投诉者奖励
			if result( "ToUserReward" ) - 0 > 0 then
				Call B_("user").where( rs("UserID") ).setInc( Array( "Experience" , result( "ToUserReward" ) ) )
			end if
			
			'给被投诉者发送消息
			if result("IsToCplUserMsg") = 1 then
				html = result("ToCplUserMsg")
				if rs("CplType") = 1 then
					html = replace( html , "{#ID#}" , "{p:" & rs("ParentID") & "}" )
				elseif rs("CplType") = 2 then
					html = replace( html , "{#ID#}" , "{r:" & rs("ParentID") & "}" )
				end if							
				html = K_("bbs--Public").replaceLink( html )
				objCMS.content = html
				objCMS.parseHtml			
				Call M_("bbs--Message").SendMessages( S_("adminId") , rs("CplUser") , objCMS.content  )
				dict("MsgStatus") = 1
			end if
		'end if
		

		
		if IsNul( rs("AnswerReward") ) then
			dict("AnswerReward") = result("ToUserReward")
		end if
		
		AllProgress = rs("AllProgress")
		if IsNul( AllProgress ) then
			AllProgress = ""
			AllProgress = S_("adminId") & ":" & stype
		else
			AllProgress = AllProgress & ";" & S_("adminId") & ":" & stype
		end if

		
		dict("AllProgress") = AllProgress
		dict("IsSolved") = 1
		dict("IsClosed") = 0
		dict("Progress") = "处理完成"
		
		that.u(rs)
		
		Call B_(tableName).where(id).setField( dict , null ) 		
		
	end sub
	
	'对帐户状态进行处理
	sub Remove
		dim id,stype
		
		id = that.form("id")
		stype = that.form("type")
			
		Call DealCpl( id , "Remove" & Stype )
		
		if stype = "Topic" or stype = "Reply" then
			Call A_( stype & "/remove")
		elseif stype = "Upload" then
			Call A_("UploadLog--Admin/remove")
		else
			Call A_( stype & "--Admin/remove")
		end if
	end sub
	
	
	
	'对帐户状态进行处理
	sub setStatusField
		dim id,FieldValue,StatusField,CplID,stype
		dim dict,rs,result
		FieldValue = that.form("FieldValue")
		StatusField = that.form("StatusField")

		CplID = that.form("CplID")
		if FieldValue = 0 then
			stype = "Open"
		else
			stype = "Close"
		end if
		'发送消息
		Call DealCpl( CplID , stype & StatusField ) 

		Call A_("bbs--Manage/setStatusField")
	end sub
	
	sub setStatus4Cpl
		dim StatusField,FieldValue
		dim AllProgress,rs
		StatusField = that.form("StatusField")
		FieldValue = that.form("FieldValue")

		CplID = that.form("CplID")
		id = CplID

		'取出该条投诉记录
		set rs = B_(tableName).where(id).find
		
		
		set dict = D_
		
		AllProgress = rs("AllProgress")
		if IsNul( AllProgress ) then
			AllProgress = ""
			AllProgress = S_("adminId") & ":" & StatusField & ":" & iif( FieldValue = 1, 0 , 1 )
		else
			AllProgress = AllProgress & ";" & S_("adminId") & ":" & StatusField & ":" & iif( FieldValue = 1, 0 , 1 )
		end if

		
		dict("AllProgress") = AllProgress
		dict("IsClosed") = iif( FieldValue = 1, 0 , 1 )
		dict("Progress") = "忽略"
		
		that.u(rs)
		
		Call B_(tableName).where(id).setField( dict , null ) 			
		
		Call K_( "bbs--Public" ).CtrlLog( 1, "用户ID: " & S_("adminId") & " ，设置字段IsClosed=" & iif( FieldValue = 1, 0 , 1 ) , "设置成功" )
	end sub
	
	'使用popasp_auto()方法配合js自动填充
	sub jsAuto
		dim dict
		set dict = XM_("complaint--Configs").db.path("Configs/Config").field("ConfigName,ConfigValue").getKeyValue
		
		Call K_("bbs--Tools").js_Auto( dict )
	end sub
end Class
%>