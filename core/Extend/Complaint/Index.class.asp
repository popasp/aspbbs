<%
Class Index 'Extends bbs--Common
	private tableName,isWap
	private orderStr
	sub initialize
		tableName = "self_GuestComplaint"
		
		set bookConfig = XM_( "Complaint--Configs" ).db.path("Configs/Config").field("ConfigName,ConfigValue").getObject
		that.d("bookConfig") = bookConfig
		isWap = checkWap()
	
		objCMS.cmsPrefix = "aspbbs"
		guestConfig.DefaultTemplate = "complaint"
		objCMS.htmlFilePath = "html"
	end sub
	
	'投诉帖子页面
	Sub AddTopic
		call addTopicSub("Topic/add")
	end sub
	
	'投诉帖子弹窗页面
	Sub AddTopic4inc
		call addTopicSub("Topic/add-inc")
	end sub
	
	'投诉回帖页面
	Sub AddReply
		Call AddReplySub("Reply/add")
	End Sub
	
	'投诉回帖弹窗页面
	Sub AddReply4inc
		dim options		
		options =  XM_( "Complaint--Configs" ).db.path("SpecSet/Line[SpecField='TouSu']").field("SpecOptions").getOne
		options = split( options , "###" )
		that.d("options") = options
		Call AddReplySub("Reply/add-inc")
	End Sub
	
	'投诉回帖页面
	Sub AddUploads
		Call AddUploadsSub("Uploads/add")
	End Sub
	
	'投诉回帖弹窗页面
	Sub AddUploads4inc
		dim options		
		options =  XM_( "Complaint--Configs" ).db.path("SpecSet/Line[SpecField='TouSuUpload']").field("SpecOptions").getOne
		options = split( options , "###" )
		that.d("options") = options

		Call AddUploadsSub("Uploads/add-inc")
	End Sub
	
	'投诉案例页面
	Sub AddCases
		call addCasesSub("Cases/add")
	end sub
	
	'投诉案例弹窗页面
	Sub AddCases4inc
		call addCasesSub("Cases/add-inc")
	end sub

	'关闭弹出窗口
	sub closeLayer
		dim obj,templatePath,id,rs,sortRS
		'实例化POPASP_CMS类
		set obj = objCMS

		'模板名，这里要注意，模板文件名必须为 gbook.html
		templatePath= obj.getTemplatePath("Common/close")
		
		'加载
		obj.load(templatePath)	

		'解析模板
		obj.parseHtml

		'输出解析后的模板
		obj.Output
	end sub
	

	
	private sub addTopicSub(template)
		dim id,field,where,where2,authInfo,UserID,NoReplyTip,editCount,templatePath
		dim topicRS,replyRS,arr
		
		'来路不对
		if not that.isSelfOrigin then
			'that.error("非法操作")
		end if	

		'''''帖子'''''
		id = that.get("ID")
		
		if not that.isId( id ) then
			that.error( "请选择投诉帖子!" )
		end if
		
		set where = D_
		
		
		where("a.TopicID") = id
		where("a.UserID = c.UserID ") = null

		set topicRS= B_("self_GuestConfig").onlysql(0).from(C_("THREAD_TABLE")).field(C_("THREAD_FIELD")).where( where ).find

		'帖子不存在
		if topicRs.eof then
			K_("bbs--Public").show404
			response.end
		end if	
	

		'实例化POPASP_CMS类
		set obj = objCMS

		that.d("thread") = topicRS
		
		that.d("user") = B_("user").where( topicRS("UserID") ).find
		
		that.d("specstr") = K_("Complaint--SpecTool").getFormHtml( "" , "self_GuestComplaint" , "Topic" , "" ,rs )
		
		
		'引入summernote之前,要先引入该模块
		POP_MVC.config( "AUTO_EXTENDS_MODULE" ) = "summernote"
		
		'自动化导入summernote
		that.d("summernote") = P_("auto").summernote("#L_content")
		
		objCMS.show( template )
	End Sub
	

	
	Private Sub AddReplySub( template )
		dim id,field,where,where2,authInfo,UserID,NoReplyTip,editCount,templatePath
		dim replyRS,arr
		
		'来路不对
		if not that.isSelfOrigin then
			'that.error("非法操作")
		end if	

		'''''帖子'''''
		id = that.get("ID")
		

		
		if not that.isId( id ) then
			that.error( "请选择投诉回帖!" )
		end if
		
		set where = D_
		
		where("a.ReplyID") = id
		where("a.UserID = b.UserID") = null
		where("b.GroupID = c.GroupID") = null
			
		field = "a.ReplyID,a.TopicID,a.Content,a.IsAdopted,a.Reward,a.ZanCount,a.CaiCount,a.AddTime,b.UserID,b.Avatar,b.Gender,b.LoginName,b.Nickname,b.UserStatus,c.GroupID,c.IsAdmin"
		
		set replyRS= B_("self_GuestConfig").onlysql(0).from("{prefix}self_GuestReply as a,{prefix}User as b,{prefix}UserGroup as c").field(field).where( where ).find

		'回帖不存在
		if replyRS.eof then
			K_("bbs--Public").show404
			response.end
		end if	

		that.d("reply") = replyRS
		
		that.d("user") = B_("user").where( replyRS("UserID") ).find
			
		that.d("specstr") = K_("Complaint--SpecTool").getFormHtml( "" , "self_GuestComplaint" , "Reply" , "" ,rs )
		
		
		'引入summernote之前,要先引入该模块
		POP_MVC.config( "AUTO_EXTENDS_MODULE" ) = "summernote"
		
		'自动化导入summernote
		that.d("summernote") = P_("auto").summernote("#L_content")
		
	
		objCMS.show( template )
	end sub
	
	Sub AddUploadsSub( template )
		dim id,field,where,where2,authInfo,UserID,NoReplyTip,editCount,templatePath
		dim replyRS,arr
		
		'来路不对
		if not that.isSelfOrigin then
			'that.error("非法操作")
		end if	

		'''''上传文件ID'''''
		id = that.get("ID")
		
		if not that.isId( id ) then
			that.error( "请选择投诉的上传文件!" )
		end if
		
		set where = D_
		
		where("a.LogID") = id
		where("a.UserID = b.UserID") = null
		where("b.GroupID = c.GroupID") = null
			
		field = "a.LogID,a.OriginName,a.ZanCount,a.CaiCount,a.LogTime,b.UserID,b.Avatar,b.Gender,b.LoginName,b.Nickname,b.UserStatus,c.GroupID,c.IsAdmin"
		
		set rs= B_("self_GuestConfig").onlysql(0).from("{prefix}self_UploadLog as a,{prefix}User as b,{prefix}UserGroup as c").field(field).where( where ).find

		'上传文件不存在
		if rs.eof then
			K_("bbs--Public").show404
			response.end
		end if	

		that.d("rs") = rs
		
		that.d("user") = B_("user").where( rs("UserID") ).find
			
		that.d("specstr") = K_("Complaint--SpecTool").getFormHtml( "" , "self_GuestComplaint" , "Uploads" , "" ,rs )
		
		
		'引入summernote之前,要先引入该模块
		POP_MVC.config( "AUTO_EXTENDS_MODULE" ) = "summernote"
		
		'自动化导入summernote
		that.d("summernote") = P_("auto").summernote("#L_content")
		
	
		objCMS.show( template )
	end sub
	
	private sub addCasesSub(template)
		dim id,field,where,where2,authInfo,UserID,NoReplyTip,editCount,templatePath
		dim caseRS,replyRS,arr,fields
		
		'来路不对
		if not that.isSelfOrigin then
			'that.error("非法操作")
		end if	

		'''''帖子'''''
		id = that.get("ID")
		
		if not that.isId( id ) then
			that.error( "请选择投诉案例!" )
		end if
		
		set where = D_
		
		
		where("a.TopicID") = id
		where("a.UserID = c.UserID ") = null
		
		fields = "a.TopicID,a.ZanCount,a.AddTime,a.IndexImage,a.DownURL,a.Title,a.IpAddr,a.EditIpAddr,a.ContentSource,a.ContentStatus,a.IsTop,a.Isrecommend,a.IsHeadline,a.IsFeatured,a.IsClosed,a.Visits,a.IsNoComment,a.Content,a.AnswerReward,a.EditTime,a.EditCount,a.IsSolved,c.LoginName,c.Nickname,c.GroupID,c.Avatar,c.UserID,c.Experience,c.ThreadReplyView"

		set caseRS= B_( objCMS.baseTable ).onlysql(0).from( "{prefix}self_GuestCases as a,{prefix}User as c" ).field( fields ).where( where ).find

		'帖子不存在
		if caseRS.eof then
			K_("bbs--Public").show404
			response.end
		end if	
	

		'实例化POPASP_CMS类
		set obj = objCMS

		that.d("thread") = caseRS
		
		that.d("user") = B_("user").where( caseRS("UserID") ).find
		
		that.d("specstr") = K_("Complaint--SpecTool").getFormHtml( "" , "self_GuestCases" , "Topic" , "" ,rs )
		
		
		'引入summernote之前,要先引入该模块
		POP_MVC.config( "AUTO_EXTENDS_MODULE" ) = "summernote"
		
		'自动化导入summernote
		that.d("summernote") = P_("auto").summernote("#L_content")
		
		objCMS.show( template )
	End Sub
	
	'添加操作
	sub doAddTopic
		dim id,dict,rs,AnswerReward,sTip
		
	
		set dict = POP_MVC.form2dict	
		
		'自动验证与自动填充数据
		if not K_("bbs--Public").ModelCreate("Complaint--Thread",dict,1) then			
			sTip = M_( "Complaint--Thread" ).db.error
			Call K_("bbs--Public").CtrlLog( 0 ,  "用户 ID:" &  S_("adminId") & " 投诉失败，" & sTip  , sTip )
		end if
		
		'得到帖子自增ID
		id = K_("bbs--Public").ModelAdd( "Complaint--Thread" , null )

		if  id > 0 then
			'B_("self_GuestConfig").commit
			
		
			'发送邮件提示
			Call Mail4AddTopic( id )
			
			if that.form("closeLayer") = 1 then
				Call K_("bbs--Public").CtrlLog( 1 ,  "用户 ID:" &  S_("adminId") & "，投诉帖子 ID: " &  id & " ，等待管理员审核"  , Array( "投诉完成，请耐心等待管理员审核！"  , "?Complaint--Index_CloseLayer" & guestConfig.PageSuffix ) )
			else
				Call K_("bbs--Public").CtrlLog( 1 ,  "用户 ID:" &  S_("adminId") & "，投诉帖子 ID: " &  id & " ，等待管理员审核"  , Array( "投诉完成，请耐心等待管理员审核！"  , "?Complaint--Index" & guestConfig.PageSuffix ) )
			end if
		else
			'B_("self_GuestConfig").Rollback			
			'that.error( "添加失败，请重新提交" )
			Call K_("bbs--Public").CtrlLog( 0 ,  "用户 ID: " &  S_("adminId") & " 投诉失败"  , "添加失败，请重新提交" )
		end if		
	end sub
	
	'添加操作
	sub doAddReply
		dim id,dict,rs,AnswerReward,sTip
		
	
		set dict = POP_MVC.form2dict
		
		dict.key("TouSu") = "LianDong2"
		
		'自动验证与自动填充数据
		if not K_("bbs--Public").ModelCreate("Complaint--Thread",dict,1) then			
			sTip = M_( "Complaint--Thread" ).db.error
			Call K_("bbs--Public").CtrlLog( 0 ,  "用户 ID:" &  S_("adminId") & " 投诉失败，" & sTip  , sTip )
		end if
		
		set dict = M_( "Complaint--Thread" ).db.getData
		
		'得到帖子自增ID
		id = K_("bbs--Public").ModelAdd( "Complaint--Thread" , dict )

		if  id > 0 then
			'B_("self_GuestConfig").commit
			
		
			'发送邮件提示
			Call Mail4AddReply( id )
			
			if that.form("closeLayer") = 1 then
				response.redirect "?Complaint--Index_CloseLayer" & guestConfig.PageSuffix
			else
				Call K_("bbs--Public").CtrlLog( 1 ,  "用户 ID:" &  S_("adminId") & "，投诉回帖 ID: " &  id & " ，等待管理员审核"  , Array( "投诉完成，请耐心等待管理员审核！"  , "?Complaint--Index_ReplyList" & guestConfig.PageSuffix ) )
			end if			
		else
			'B_("self_GuestConfig").Rollback			
			'that.error( "添加失败，请重新提交" )
			Call K_("bbs--Public").CtrlLog( 0 ,  "用户 ID: " &  S_("adminId") & " 投诉失败"  , "添加失败，请重新提交" )
		end if		
	end sub
	
	'添加操作
	sub doAddUploads
		dim id,dict,rs,AnswerReward,sTip
		
	
		set dict = POP_MVC.form2dict

		dict.key("TouSu") = "LianDong2"
		

		
		'自动验证与自动填充数据
		if not K_("bbs--Public").ModelCreate("Complaint--Thread",dict,1) then			
			sTip = M_( "Complaint--Thread" ).db.error
			Call K_("bbs--Public").CtrlLog( 0 ,  "用户 ID:" &  S_("adminId") & " 投诉失败，" & sTip  , sTip )
		end if
		
		'得到帖子自增ID
		id = K_("bbs--Public").ModelAdd( "Complaint--Thread" , null )
		

		if  id > 0 then
			'B_("self_GuestConfig").commit
			
		
			'发送邮件提示
			Call Mail4AddUploads( id )
			
			if that.form("closeLayer") = 1 then
				response.redirect "?Complaint--Index_CloseLayer" & guestConfig.PageSuffix
			else
				Call K_("bbs--Public").CtrlLog( 1 ,  "用户 ID:" &  S_("adminId") & "，投诉上传文件 ID: " &  id & " ，等待管理员审核"  , Array( "投诉完成，请耐心等待管理员审核！"  , "?Complaint--Index" & guestConfig.PageSuffix ) )
			end if
		else
			'B_("self_GuestConfig").Rollback			
			'that.error( "添加失败，请重新提交" )
			Call K_("bbs--Public").CtrlLog( 0 ,  "用户 ID: " &  S_("adminId") & " 投诉失败"  , "添加失败，请重新提交" )
		end if		
	end sub
	
	'添加操作
	sub doAddCases
		dim id,dict,rs,AnswerReward,sTip
	
		set dict = POP_MVC.form2dict			
	
		'自动验证与自动填充数据
		if not K_("bbs--Public").ModelCreate("Complaint--Thread",dict,1) then		
			sTip = M_( "Complaint--Thread" ).db.error
			Call K_("bbs--Public").CtrlLog( 0 ,  "用户 ID:" &  S_("adminId") & " 投诉失败，" & sTip  , sTip )
		end if
		
		'得到帖子自增ID
		id = K_("bbs--Public").ModelAdd( "Complaint--Thread" , null )

		if  id > 0 then
			'B_("self_GuestConfig").commit
			

			'发送邮件提示
			Call Mail4AddCases( id )
			
			if that.form("closeLayer") = 1 then
				Call K_("bbs--Public").CtrlLog( 1 ,  "用户 ID:" &  S_("adminId") & "，投诉案例 ID: " &  id & " ，等待管理员审核"  , Array( "投诉完成，请耐心等待管理员审核！"  , "?Complaint--Index_CloseLayer" & guestConfig.PageSuffix ) )
			else
				Call K_("bbs--Public").CtrlLog( 1 ,  "用户 ID:" &  S_("adminId") & "，投诉案例 ID: " &  id & " ，等待管理员审核"  , Array( "投诉完成，请耐心等待管理员审核！"  , "?Complaint--Index" & guestConfig.PageSuffix ) )
			end if
		else
			'B_("self_GuestConfig").Rollback			
			'that.error( "添加失败，请重新提交" )
			Call K_("bbs--Public").CtrlLog( 0 ,  "用户 ID: " &  S_("adminId") & " 投诉失败"  , "添加失败，请重新提交" )
		end if		
	end sub

	sub index
		objCMS.show("Index/index")
	end sub
	
	'投诉回帖发送邮件
	Public Property Get Mail4AddReply( CplID )
		dim Title,Content,srcContent,srcTitle,id,tousu
		dim subject,body,rs
		id = CplID
		if POP_MVC.isPost then
			Title = request.form("Title")
			SrcTitle = request.form("SrcTitle")
			Content = request.form("Content")
			SrcContent = request.form("SrcContent")
			tousu = request.form("TouSu")
		else
			set rs = B_("self_GuestComplaint").where( CplID ).field("Title,Content,SrcContent,SrcTitle").find
			if rs.eof then
				POP_MVC.unset( rs )
				Exit Property
			else
				Title = rs("Title")
				SrcTitle = rs("SrcTitle")
				Content = rs("Content")				
				SrcContent = rs("SrcContent")	
				tousu = rs("LianDong2")
				POP_MVC.unset( rs )
			end if
		end if
		subject =  S_("GroupName") & ": " & S_("Nickname") & " 投诉回帖:" & title & "(" & tousu & ")"
		body = K_("bbs--Mail").getBodyHeader("投诉回帖")
		body = body & "<p>" & S_("GroupName") & ": " & S_("Nickname") & " 用" & iif( checkWap() , "手机", "电脑" ) & " 投诉回贴:" & "<a href='" & objCMS.RealLinkPrefix  & "complaint--Reply_detail_" & id & "'>" & SrcTitle & "</a></p>"
		body = body & "<hr />"
		that.u(rs)
		body = body & K_("bbs--Mail").addHttpHost4img( "投诉原因:" & tousu & "<br />" &	content)
		Call K_("bbs--Plugin").sendmail( guestConfig.MessageAlertsEmail , subject , body )
	End Property
	
	'投诉帖子发送邮件
	Public Property Get Mail4AddTopic( CplID )
		dim Title,Content,srcContent,srcTitle,id
		dim subject,body,rs
		id = CplID
		
		
		if POP_MVC.isPost then
			Title = request.form("Title")
			SrcTitle = request.form("SrcTitle")
			Content = request.form("Content")
			SrcContent = request.form("SrcContent")
		else
			set rs = B_("self_GuestComplaint").where( CplID ).field("Title,Content,SrcContent,SrcTitle").find
			if rs.eof then
				POP_MVC.unset( rs )
				Exit Property
			else
				Title = rs("Title")
				SrcTitle = rs("SrcTitle")
				Content = rs("Content")				
				SrcContent = rs("SrcContent")				
				POP_MVC.unset( rs )
			end if
		end if
		

		subject =  S_("GroupName") & ": " & S_("Nickname") & " 投诉帖子:" & title
		body = K_("bbs--Mail").getBodyHeader("投诉帖子")
		body = body & "<p>" & S_("GroupName") & ": " & S_("Nickname") & " 用" & iif( checkWap() , "手机", "电脑" ) & " 投诉贴子:" & "<a href='" & objCMS.RealLinkPrefix & "complaint--Topic_detail_" & id & "'>" & SrcTitle & "</a></p>"
		body = body & "<hr />"
		
		body = body & K_("bbs--Mail").addHttpHost4img(content)	
		Call K_("bbs--Plugin").sendmail( guestConfig.MessageAlertsEmail , subject , body )
	End Property
	
	'投诉帖子发送邮件
	Public Property Get Mail4AddUploads( CplID )
		dim Title,Content,srcContent,srcTitle,id
		dim subject,body,rs
		id = CplID
		
		
		if POP_MVC.isPost then
			Title = request.form("Title")
			SrcTitle = request.form("SrcTitle")
			Content = request.form("Content")
			SrcContent = request.form("SrcContent")
		else
			set rs = B_("self_GuestComplaint").where( CplID ).field("Title,Content,SrcContent,SrcTitle").find
			if rs.eof then
				POP_MVC.unset( rs )
				Exit Property
			else
				Title = rs("Title")
				SrcTitle = rs("SrcTitle")
				Content = rs("Content")				
				SrcContent = rs("SrcContent")				
				POP_MVC.unset( rs )
			end if
		end if
		

		subject =  S_("GroupName") & ": " & S_("Nickname") & " 投诉上传文件:" & title
		body = K_("bbs--Mail").getBodyHeader("投诉上传文件")
		body = body & "<p>" & S_("GroupName") & ": " & S_("Nickname") & " 用" & iif( checkWap() , "手机", "电脑" ) & " 投诉上传文件:" & "<a href='" & objCMS.RealLinkPrefix & "complaint--Topic_detail_" & id & "'>" & SrcTitle & "</a></p>"
		body = body & "<hr />"
		
		body = body & K_("bbs--Mail").addHttpHost4img(content)	
		Call K_("bbs--Plugin").sendmail( guestConfig.MessageAlertsEmail , subject , body )
	End Property
	
	'投诉帖子发送邮件
	Public Property Get Mail4AddCases( CplID )
		dim Title,Content,srcContent,srcTitle,id
		dim subject,body,rs
		id = CplID
		
		
		if POP_MVC.isPost then
			Title = request.form("Title")
			SrcTitle = request.form("SrcTitle")
			Content = request.form("Content")
			SrcContent = request.form("SrcContent")
		else
			set rs = B_("self_GuestComplaint").where( CplID ).field("Title,Content,SrcContent,SrcTitle").find
			if rs.eof then
				POP_MVC.unset( rs )
				Exit Property
			else
				Title = rs("Title")
				SrcTitle = rs("SrcTitle")
				Content = rs("Content")				
				SrcContent = rs("SrcContent")				
				POP_MVC.unset( rs )
			end if
		end if
		

		subject =  S_("GroupName") & ": " & S_("Nickname") & " 投诉案例:" & title
		body = K_("bbs--Mail").getBodyHeader("投诉帖子")
		body = body & "<p>" & S_("GroupName") & ": " & S_("Nickname") & " 用" & iif( checkWap() , "手机", "电脑" ) & " 投诉贴子:" & "<a href='" & objCMS.RealLinkPrefix & "complaint--Topic_detail_" & id & "'>" & SrcTitle & "</a></p>"
		body = body & "<hr />"
		
		body = body & K_("bbs--Mail").addHttpHost4img(content)	
		Call K_("bbs--Plugin").sendmail( guestConfig.MessageAlertsEmail , subject , body )
	End Property	
end Class
%>