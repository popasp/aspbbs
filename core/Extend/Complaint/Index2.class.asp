<%
Class Index 'Extends bbs--Common
	private tableName,isWap,fileType,theStatus,listAll,TopicFieldStr,ReplyFieldStr,TopicFromTable,ReplyFromTable
	private orderStr
	sub initialize
		tableName = "self_GuestComplaint"
		POP_MVC.Config("REQ_IGNORE_HTML") = "test-editormd-markdown-doc"
		POP_MVC.Config("REQ_IGNORE_HTML") = "test-editormd-markdown-doc"
		
		set bookConfig = XM_( "Complaint--Configs" ).db.path("Configs/Config").field("ConfigName,ConfigValue").getObject
		that.d("bookConfig") = bookConfig
		isWap = checkWap()
	
		objCMS.cmsPrefix = "aspbbs"
		guestConfig.DefaultTemplate = "complaint"
		objCMS.htmlFilePath = "html"
		theStatus = 1
		listAll = false
		
		fileType = that.get("type")	
		that.d("fileType") = fileType
		if fileType = "" then
			fileType = POP_MVC.config("MD_DEFAULT_TYPE")
		end if
	
		TopicFieldStr = "b.CplID,a.TopicID,a.SortID,a.Content,a.ZanCount,a.AddTime,a.EditTime,a.IndexImage,a.Title,a.IpAddr as tIpAddr,a.EditIpAddr as tEditIpAddr,a.ContentStatus,a.IsTop,a.Isrecommend,a.IsHeadline,a.IsFeatured,a.IsClosed,a.IsReplyView,a.Visits,a.ReplyCount,a.AnswerReward,a.IsSolved,c.LoginName,c.UserStatus,c.ThreadStatus,c.ReplyStatus,c.Nickname,c.GroupID,c.Avatar,c.UserID,c.Experience,c.ThreadReplyView,b.Title as theTitle,b.CplUser,b.Content as theContent,b.SrcTitle,b.SrcContent,b.AddTime as theTime,b.LianDong1,b.LianDong2,b.Progress,b.IpAddr,(select UserID from {prefix}User where UserID = b.CplUser ) as theUser,(select LoginName from {prefix}User where UserID = b.CplUser ) as theName,(select Nickname from {prefix}User where UserID = b.CplUser ) as theNick,(select Avatar from {prefix}User where UserID = b.CplUser ) as theAvatar,(select GroupID from {prefix}User where UserID = b.CplUser ) as theGroup,(select Experience from {prefix}User where UserID = b.CplUser ) as theExperience,(select UserStatus from {prefix}User where UserID = b.CplUser ) as theUserStatus"

		ReplyFieldStr = "b.CplID,d.Title as TopicTitle,d.IsSolved as TopicIsSolved,a.ReplyID,a.ZanCount,a.TopicID,a.AddTime,a.IsAdopted,a.IpAddr,a.EditIpAddr,a.EditTime,a.Content,c.LoginName,c.UserStatus,c.ThreadStatus,c.ReplyStatus,c.Nickname,c.GroupID,c.Avatar,c.UserID,c.Experience,c.ThreadReplyView,b.Title as theTitle,b.Content as theContent,b.SrcTitle,b.SrcContent,b.AddTime as theTime,b.LianDong2,b.Progress,(select UserID from {prefix}User where UserID = b.CplUser ) as theUser,(select LoginName from {prefix}User where UserID = b.CplUser ) as theName,(select Nickname from {prefix}User where UserID = b.CplUser ) as theNick,(select Avatar from {prefix}User where UserID = b.CplUser ) as theAvatar,(select GroupID from {prefix}User where UserID = b.CplUser ) as theGroup,(select Experience from {prefix}User where UserID = b.CplUser ) as theExperience,(select UserStatus from {prefix}User where UserID = b.CplUser ) as theUserStatus"
		
		TopicFromTable = "{prefix}self_GuestTopic as a,{prefix}self_GuestComplaint as b,{prefix}User as c"
		ReplyFromTable = "{prefix}self_GuestReply as a,{prefix}self_GuestComplaint as b,{prefix}User as c,{prefix}self_GuestTopic as d"
	end sub

	
	sub closeLayer
		dim obj,templatePath,id,rs,sortRS
		'实例化POPASP_CMS类
		set obj = objCMS

		'模板名，这里要注意，模板文件名必须为 gbook.html
		templatePath= obj.getTemplatePath("Common/close")
		
		'加载
		obj.load(templatePath)	

		'解析模板
		obj.parseHtml

		'输出解析后的模板
		obj.Output
	end sub
	
	Sub AddTopic4inc
		call addTopicSub("Index/add-inc")
	end sub
	
	Sub AddTopic
		call addTopicSub("Index/add")
	end sub
	
	private sub addTopicSub(template)
		dim id,field,where,where2,authInfo,UserID,NoReplyTip,editCount,templatePath
		dim topicRS,replyRS,arr,obj
		
		'来路不对
		if not that.isSelfOrigin then
			'that.error("非法操作")
		end if	

		'''''帖子'''''
		id = that.get("ID")
		
		set where = D_
		
		
		where("a.TopicID") = id
		where("a.UserID = c.UserID ") = null
		where("b.GroupID = c.GroupID ") = null

		set topicRS= B_("self_GuestConfig").onlysql(0).from(C_("THREAD_TABLE")).field(C_("THREAD_FIELD")).where( where ).find

		'帖子不存在
		if topicRs.eof then
			K_("bbs--Public").show404
			response.end
		end if	
	

		'实例化POPASP_CMS类
		set obj = objCMS

		that.d("thread") = topicRS
		
		that.d("user") = B_("user").where( topicRS("UserID") ).find
		
		that.d("specstr") = K_("Complaint--SpecTool").getFormHtml( "" , "self_GuestComplaint" , "Topic" , "" ,rs )
		
		
		'引入summernote之前,要先引入该模块
		POP_MVC.config( "AUTO_EXTENDS_MODULE" ) = "summernote"
		
		'自动化导入summernote
		that.d("summernote") = P_("auto").summernote("#L_content")
		
		templatePath=  obj.getTemplatePath(template)
		'加载
		obj.load(templatePath)	
		
		'解析模板
		obj.parseHtml

		'输出解析后的模板
		obj.Output
	End Sub
	
	Sub AddReply
		Call AddReplySub("Index/add-reply")
	End Sub
	
	Sub AddReply4inc
		dim options
		options =  XM_( "Complaint--Configs" ).db.path("SpecSet/Line[SpecField='TouSu']").field("SpecOptions").getOne
		options = split( options , "###" )
		that.d("options") = options
		Call AddReplySub("Index/add-reply-inc")
	End Sub
	
	Sub AddReplySub( template )
		dim id,field,where,where2,authInfo,UserID,NoReplyTip,editCount,templatePath
		dim replyRS,arr,obj
		
		'来路不对
		if not that.isSelfOrigin then
			'that.error("非法操作")
		end if	

		'''''帖子'''''
		id = that.get("ID")
		
		set where = D_
		
		where("a.ReplyID") = id
		where("a.UserID = b.UserID") = null
		where("b.GroupID = c.GroupID") = null
			
		field = "a.ReplyID,a.TopicID,a.Content,a.IsAdopted,a.Reward,a.ZanCount,a.CaiCount,a.AddTime,b.UserID,b.Avatar,b.Gender,b.LoginName,b.Nickname,b.UserStatus,c.GroupID,c.IsAdmin"
		
		set replyRS= B_("self_GuestConfig").onlysql(0).from("{prefix}self_GuestReply as a,{prefix}User as b,{prefix}UserGroup as c").field(field).where( where ).find

		'帖子不存在
		if replyRS.eof then
			K_("bbs--Public").show404
			response.end
		end if	
	
		'实例化POPASP_CMS类
		set obj = objCMS

		that.d("reply") = replyRS
		
		that.d("user") = B_("user").where( replyRS("UserID") ).find
		
		that.d("specstr") = K_("Complaint--SpecTool").getFormHtml( "" , "self_GuestComplaint" , "Reply" , "" ,rs )
		
		
		'引入summernote之前,要先引入该模块
		POP_MVC.config( "AUTO_EXTENDS_MODULE" ) = "summernote"
		
		'自动化导入summernote
		that.d("summernote") = P_("auto").summernote("#L_content")
		
		templatePath=  obj.getTemplatePath(template)
		'加载
		obj.load(templatePath)	
		
		'解析模板
		obj.parseHtml

		'输出解析后的模板
		obj.Output
	end sub
	

	
	'添加操作
	sub doAddTopic
		dim id,dict,rs,AnswerReward,sTip
		
	
		set dict = POP_MVC.form2dict	
		
		'自动验证与自动填充数据
		if not K_("bbs--Public").ModelCreate("Complaint--Thread",dict,1) then			
			sTip = M_( "Complaint--Thread" ).db.error
			Call K_("bbs--Public").CtrlLog( 0 ,  "用户 ID:" &  S_("adminId") & " 投诉失败，" & sTip  , sTip )
		end if
		
		'得到帖子自增ID
		id = K_("bbs--Public").ModelAdd( "Complaint--Thread" , null )

		if  id > 0 then
			'B_("self_GuestConfig").commit
			
		
			'发送邮件提示
			
			if that.form("closeLayer") = 1 then
				Call K_("bbs--Public").CtrlLog( 1 ,  "用户 ID:" &  S_("adminId") & "，投诉帖子 ID: " &  id & " ，等待管理员审核"  , Array( "投诉完成，请耐心等待管理员审核！"  , "?Complaint--Index_CloseLayer" & guestConfig.PageSuffix ) )
			else
				Call K_("bbs--Public").CtrlLog( 1 ,  "用户 ID:" &  S_("adminId") & "，投诉帖子 ID: " &  id & " ，等待管理员审核"  , Array( "投诉完成，请耐心等待管理员审核！"  , "?Complaint--Index" & guestConfig.PageSuffix ) )
			end if
		else
			'B_("self_GuestConfig").Rollback			
			'that.error( "添加失败，请重新提交" )
			Call K_("bbs--Public").CtrlLog( 0 ,  "用户 ID: " &  S_("adminId") & " 投诉失败"  , "添加失败，请重新提交" )
		end if		
	end sub
	
	'添加操作
	sub doAddReply
		dim id,dict,rs,AnswerReward,sTip
		
	
		set dict = POP_MVC.form2dict
		
		dict.key("TouSu") = "LianDong2"
		
		'自动验证与自动填充数据
		if not K_("bbs--Public").ModelCreate("Complaint--Thread",dict,1) then			
			sTip = M_( "Complaint--Thread" ).db.error
			Call K_("bbs--Public").CtrlLog( 0 ,  "用户 ID:" &  S_("adminId") & " 投诉失败，" & sTip  , sTip )
		end if
		
		'得到帖子自增ID
		id = K_("bbs--Public").ModelAdd( "Complaint--Thread" , null )

		if  id > 0 then
			'B_("self_GuestConfig").commit
			
		
			'发送邮件提示
			
			if that.form("closeLayer") = 1 then
				response.redirect "?Complaint--Index_CloseLayer" & guestConfig.PageSuffix
			else
				Call K_("bbs--Public").CtrlLog( 1 ,  "用户 ID:" &  S_("adminId") & "，投诉回帖 ID: " &  id & " ，等待管理员审核"  , Array( "投诉完成，请耐心等待管理员审核！"  , "?Complaint--Index_ReplyList" & guestConfig.PageSuffix ) )
			end if			
		else
			'B_("self_GuestConfig").Rollback			
			'that.error( "添加失败，请重新提交" )
			Call K_("bbs--Public").CtrlLog( 0 ,  "用户 ID: " &  S_("adminId") & " 投诉失败"  , "添加失败，请重新提交" )
		end if		
	end sub
	
	sub index

		dim content,key,page
		orderStr = "TopicID DESC"
		
		page = that.get("page")
		
		if page = "" then
			page = 1
		end if
		call getHotTopic(0)

		Call getHotReply
		Call getHotUser
		Call getHotCplUser
		Call getTopicListData()
		
		
		
		objCMS.output	
		'Call clearRS
	end sub
	
	sub ReplyList

		dim content,key,page
		orderStr = "ReplyID DESC"
		
		page = that.get("page")
		
		if page = "" then
			page = 1
		end if
		call getHotTopic(0)

		Call getHotReply
		Call getHotUser
		Call getHotCplUser
		Call getReplyListData()
		
		
		
		objCMS.output	
		'Call clearRS
	end sub
	
	private sub getTopicListData()
		dim rsObj,where,template,topicRS
		
		set where = D_
		
		
		'where("a.ContentStatus") = theStatus
		
		if not isEmpty( isSolved ) then
			if isSolved = 0 then
				where( "b.IsSolved" ) = 0
			else
				where( "b.IsSolved" ) = Array( "<>" ,0 )
			end if			
		end if
		
		where("a.UserID = c.UserID") = null
		where("b.IsTop") = 1
		where("b.CplType") =1
		
		where("a.TopicID = b.ParentID") = null

		if S_("GroupID") = 1 then
			
		elseif S_("adminId") = 1 then
			where( "b.UserID" ) = Array( "neq" , S_("adminId") )
		else
			where( "b.UserID" ) = S_("adminId")
		end if

		if POP_MVC.req("UserID") <> "" then
			where("b.UserID") = POP_MVC.req("UserID")
		elseif POP_MVC.req("CplUser") <> "" then
			where("b.CplUser") = POP_MVC.req("CplUser")
		elseif POP_MVC.req("date") <> "" then
			where("b.AddTime") = Array( "date" , POP_MVC.req("date") )
		elseif POP_MVC.req("tdate") <> "" then
			where("b.AddTime") = Array( "date" , POP_MVC.req("date") )
		elseif POP_MVC.req("addr") <> "" then
			where("a.IpAddr") =POP_MVC.req("taddr")
		elseif POP_MVC.req("addr") <> "" then
			where("b.IpAddr") = POP_MVC.req("addr")
		end if

		Call B_("self_GuestConfig").onlysql(0).page(array( null, guestConfig.IndexListPageCount ) ).from( TopicFromTable ).field( TopicFieldStr ).where( where )

		if POP_MVC.req("keys") <> "" then
			'适用于搜索
			orderStr = "TopicID DESC"
			set topicRS= B_("self_GuestConfig").order(orderStr).search(POP_MVC.req("keys" ),"b.Title" )
			that.d("recordCount") = topicRS.RecordCount		
		else
			if orderStr <> "" then
				set topicRS= B_("self_GuestConfig").order(orderStr).select
			else
				set topicRS= B_("self_GuestConfig").select
			end if
			that.d("recordCount") = topicRS.RecordCount
		end if
		

		
		
		that.d("thread") = topicRS	

	
		'Call getHotTopic(SortID)
		'Call getNewReply(SortID)
		
		dim obj
		set obj = objCMS
		
		if LCase(POP_MVC.a) = "search" then
			template = "Index/search-Topic"
			templatePath= obj.getTemplatePath( template )
			
			
			if POP_MVC.req("keys") <> "" then
				templatePath= obj.getTemplatePath( "TopicSearch/topic" )
			elseif POP_MVC.req("UserID") <> "" then
				templatePath= obj.getTemplatePath( "TopicSearch/user" )
			elseif POP_MVC.req("CplUser") <> "" then
				templatePath= obj.getTemplatePath( "TopicSearch/CplUser" )
			elseif POP_MVC.req("date") <> "" then
				templatePath= obj.getTemplatePath( "jie/search-date" )
			elseif POP_MVC.req("tdate") <> "" then
				templatePath= obj.getTemplatePath( "jie/search-tdate" )
			elseif POP_MVC.req("addr") <> "" then
				templatePath= obj.getTemplatePath( "jie/search-addr" )
			elseif POP_MVC.req("taddr") <> "" then
				templatePath= obj.getTemplatePath( "jie/search-taddr" )
			end if
		else
		
			
			template = "Index/index"		
			templatePath= obj.getTemplatePath( template )
		end if
	
		obj.load(templatePath)	
		obj.parseHtml	
	end sub
	
	sub getReplyListData()
		dim rsObj,where,template,replyRS
		
		set where = D_
		
		
		'where("a.ContentStatus") = theStatus
		
		if not isEmpty( isSolved ) then
			if isSolved = 0 then
				where( "b.IsSolved" ) = 0
			else
				where( "b.IsSolved" ) = Array( "<>" ,0 )
			end if			
		end if
		
		where("a.UserID = c.UserID") = null
		where("b.IsTop") = 1
		where("b.CplType") = 2
		
		where("a.ReplyID = b.ParentID") = null
		where("a.TopicID = d.TopicID") = null

		if S_("GroupID") = 1 then
			
		elseif S_("adminId") = 1 then
			where( "b.UserID" ) = Array( "neq" , S_("adminId") )
		else
			where( "b.UserID" ) = S_("adminId")
		end if

		if POP_MVC.req("UserID") <> "" then
			where("c.UserID") = POP_MVC.req("UserID")
		elseif POP_MVC.req("date") <> "" then
			where("a.AddTime") = Array( "date" , POP_MVC.req("date") )
		elseif POP_MVC.req("addr") <> "" then
			where("a.IpAddr") = POP_MVC.req("addr")
		elseif POP_MVC.req("reward") <> "" then
			where("a.AnswerReward") = POP_MVC.req("reward")
		end if

		Call B_("self_GuestConfig").onlysql(0).page(array( null, guestConfig.IndexListPageCount ) ).from( ReplyFromTable ).field( ReplyFieldStr ).where( where )

		if POP_MVC.req("keys") <> "" then
			'适用于搜索
			orderStr = "TopicID DESC"
			set replyRS= B_("self_GuestConfig").order(orderStr).search(POP_MVC.req("keys" ),"a.Title" )
			that.d("recordCount") = replyRS.RecordCount		
		else
			if orderStr <> "" then
				set replyRS= B_("self_GuestConfig").order(orderStr).select
			else
				set replyRS= B_("self_GuestConfig").select
			end if
			that.d("recordCount") = replyRS.RecordCount
		end if
		
		
		
		that.d("reply") = replyRS	

	
		'Call getHotTopic(SortID)
		'Call getNewReply(SortID)
		
		dim obj
		set obj = objCMS
		
		if LCase(POP_MVC.a) = "search" then
			if POP_MVC.req("keys") <> "" then
				templatePath= obj.getTemplatePath( "Index/search" )
			elseif POP_MVC.req("UserID") <> "" then
				templatePath= obj.getTemplatePath( "Index/search-user" )
			elseif POP_MVC.req("date") <> "" then
				templatePath= obj.getTemplatePath( "Index/search-date" )
			elseif POP_MVC.req("addr") <> "" then
				templatePath= obj.getTemplatePath( "Index/search-addr" )
			elseif POP_MVC.req("reward") <> "" then
				templatePath= obj.getTemplatePath( "Index/search-reward" )
			end if
		else
		
			
			template = "Index/ReplyList"		
			templatePath= obj.getTemplatePath( template )
		end if
		
	
		obj.load(templatePath)	
		obj.parseHtml	
		obj.output
	end sub
	
	
	'搜索
	sub search
		dim SearchInterval,keys
		guestConfig.ListCacheLifeTime=0
		if guestConfig.SearchInterval - 0 > 0 and  not DealCplPower() then
			SearchInterval = Request.Cookies("LastSearchDate")

			if isDate(SearchInterval) then			
				if datediff( "s" , SearchInterval , now ) - guestConfig.SearchInterval < 0 then
					that.error( Array( "您搜索的太频繁了，系统指定 " & guestConfig.SearchInterval & " 秒钟可搜索一次" , -1 ) )
				end if		
			end if		
			
			P_("cookie").unit = "s"
			P_("cookie").Expires = CInt( guestConfig.SearchInterval )
			P_("cookie").set "LastSearchDate",now
		end if		
	
		if POP_MVC.req("keys") <> "" then
			'限定搜索字符串长度不超过20，可有效防止一句话木马
			keys = POP_MVC.req("keys")
			if len(keys) > 20 then
				that.error("搜索字符串不能超过20个字符！！！")
			end if
			that.d("SearchTip") = ""
			
			if POP_MVC.req("searchType") =1 then
				that.d("SearchTip") = "回帖"
			else
				that.d("SearchTip") = "帖子"
			end if			
			
			that.d("SearchText") = keys
			Call index		
		elseif POP_MVC.req("UserID") <> "" then
			if not that.isID(POP_MVC.req("UserID")) then
				that.error("搜索的作者ID不正确！！！")
			end if
			set rs = B_("User").where( that.dict( "UserID" , POP_MVC.req("UserID") ) ).field("Nickname,LoginName").find
			if rs.eof then
				that.error("搜索的作者ID不存在！！！")
			end if
			that.d("SearchTip") = "作者"
			if rs("Nickname") <> "" then
				that.d("SearchText") = CStr(rs("Nickname"))
			else
				that.d("SearchText") =  CStr(rs("LoginName"))
			end if
			that.u(rs)
			Call index
		elseif POP_MVC.req("date") <> "" then
			if not isDate( POP_MVC.req("date") ) then
				that.error("搜索的日期格式不正确")
			end if
			that.d("SearchTip") = "日期"
			that.d("SearchText") = POP_MVC.req("date")
			Call index
		elseif POP_MVC.req("addr") <> "" then
			if len( POP_MVC.req("addr") ) > 20 then
				that.error("搜索的IP属地格式不正确")
			end if
			that.d("SearchTip") = "IP属地"
			that.d("SearchText") = POP_MVC.req("addr")
			Call index
		else
			
		end if	
		if POP_MVC.config("SHOW_PAGE_TRACE") = 0 then
			Call POP_MVC.ClearSqlRS("*")
		end if
	end sub	
	
	
	
	'开启论坛邮箱
	Public Property Get Mail4AddTopic( CplID )
		dim Title,Content,srcContent,srcTitle,id
		dim subject,body,rs
		id = CplID
		if POP_MVC.isPost then
			Title = request.form("Title")
			SrcTitle = request.form("SrcTitle")
			Content = request.form("Content")
			SrcContent = request.form("SrcContent")
		else
			set rs = B_("self_GuestComplaint").where( CplID ).field("Title,Content,SrcContent,SrcTitle").find
			if rs.eof then
				POP_MVC.unset( rs )
				Exit Property
			else
				Title = rs("Title")
				SrcTitle = rs("SrcTitle")
				Content = rs("Content")				
				SrcContent = rs("SrcContent")				
				POP_MVC.unset( rs )
			end if
		end if
		subject =  S_("GroupName") & ": " & S_("Nickname") & " 投诉帖子:" & title
		body = getBodyHeader("投诉帖子")
		body = body & "<p>" & S_("GroupName") & ": " & S_("Nickname") & " 用" & iif( checkWap() , "手机", "电脑" ) & " 投诉帖子:" & "<a href='" & objCMS.RealLinkPrefix   & id & "'>" & SrcTitle & "</a></p>"
		body = body & "<hr />"
		body = body & addHttpHost4img(content)
		Call K_("bbs--Plugin").sendmail( guestConfig.MessageAlertsEmail , subject , body )
	End Property
	
	
	'本月/周热门投诉帖子
	Private sub getHotTopic(SortID)
		dim where,rs,sql,dict
		
		'if isCached then
		'	exit sub
		'end if
		
		set where = D_		
		
		if SortID <> "" and SortID <> 0 then
			where("a.SortID") = Array("IN",SortID)			
		end if
		where("a.TopicID = b.ParentID") = null
		where("b.CplType") = 1
		where( "b.AddTime" ) = Array( "daydiff" , bookConfig.IndexHotDate )

		
		'本周热议
		sql = B_(tableName).onlysql(1).order("count(a.TopicID) desc").field("a.TopicID").from("{prefix}self_GuestTopic as a, {prefix}self_GuestComplaint as b").where( where ).group("a.TopicID").top(15).select
		
		set dict = B_(tableName).onlysql(0).order("count(a.TopicID) desc").field("a.TopicID,COUNT(a.TopicID) AS cnt").from("{prefix}self_GuestTopic as a, {prefix}self_GuestComplaint as b").where( where ).group("a.TopicID").top(15).getKeyValue
		
		that.d("hotKV") = dict
		
		if POP_MVC.config("DB_TYPE") = "mysql" then
			sql = "SELECT TopicID FROM ( " & sql & " ) as c"
		end if
	
		set where = D_
		where( "TopicID IN (" & sql & ")" ) = null
		

		set rs = B_(tableName).ResumeOpts().onlysql(0).field("TopicID,Title,ReplyCount,IndexImage,DownURL").from("{prefix}self_GuestTopic").where(where).order("ReplyCount DESC").select
		
		that.d("HotTopicCount") = rs.recordCount
		that.d("HotTopic") = rs			
	end sub
	
	'本月/周热门投诉回帖
	Private sub getHotReply()
		dim where,rs,sql,dict
		
		'if isCached then
		'	exit sub
		'end if
		
		set where = D_		
		where("a.ReplyID = b.ParentID") = null
		where("b.CplType") = 2
		where( "b.AddTime" ) = Array( "daydiff" , bookConfig.IndexHotDate )

		
		'本周热议
		sql = B_(tableName).onlysql(1).order("count(a.ReplyID) desc").field("a.ReplyID").from("{prefix}self_GuestReply as a, {prefix}self_GuestComplaint as b").where( where ).group("a.ReplyID").top(15).select
		
		set dict = B_(tableName).onlysql(0).order("count(a.ReplyID) desc").field("a.ReplyID,COUNT(a.ReplyID) AS cnt").from("{prefix}self_GuestReply as a, {prefix}self_GuestComplaint as b").where( where ).group("a.ReplyID").top(15).getKeyValue
		
		that.d("hotReplyKV") = dict
		
		if POP_MVC.config("DB_TYPE") = "mysql" then
			sql = "SELECT TopicID FROM ( " & sql & " ) as c"
		end if
	
		set where = D_
		where( "e.ReplyID IN (" & sql & ")" ) = null
		where( "d.TopicID = e.TopicID" ) = null
		

		set rs = B_(tableName).ResumeOpts().onlysql(0).field("e.ReplyID,e.Content,e.IndexImage,d.Title,e.AddTime as ReplyTime").from("{prefix}self_GuestTopic as d, {prefix}self_GuestReply as e").where(where).order("ReplyID DESC").select
		
		that.d("HotReplyCount") = rs.recordCount
		that.d("HotReply") = rs			
	end sub
	
	'本月/周热门投诉者
	Private sub getHotUser
		dim where,rs,sql,dict
		
		'if isCached then
		'	exit sub
		'end if
		
		set where = D_		
		
		where( "b.AddTime" ) = Array( "daydiff" , bookConfig.IndexHotUserDate )
		where( "a.UserID = b.UserID" ) = null

		
		'本周热议
		sql = B_(tableName).onlysql(1).order("count(b.UserID) desc").field("b.UserID").from("{prefix}User as a, {prefix}self_GuestComplaint as b").where( where ).group("b.UserID").top(15).select
		
		set dict = B_(tableName).onlysql(0).order("count(b.UserID) desc").field("b.UserID,COUNT(a.UserID) AS cnt").from("{prefix}User as a, {prefix}self_GuestComplaint as b").where( where ).group("b.UserId").top(15).getKeyValue
		
		that.d("userKV") = dict
		
		if POP_MVC.config("DB_TYPE") = "mysql" then
			sql = "SELECT UserID FROM ( " & sql & " ) as c"
		end if
	
		set where = D_
		where( "UserID IN (" & sql & ")" ) = null
		

		set rs = B_(tableName).ResumeOpts().onlysql(0).field("UserID,LoginName,Nickname,Avatar").from("{prefix}User").where(where).order("UserID DESC").select
	
		
		that.d("HotUserCount") = rs.recordCount
		that.d("HotUser") = rs			
	end sub
	
	'本月/周热门被投诉者
	Private sub getHotCplUser
		dim where,rs,sql,dict
		
		'if isCached then
		'	exit sub
		'end if
		
		set where = D_		
		
		where( "b.AddTime" ) = Array( "daydiff" , bookConfig.IndexHotUserDate )
		where( "a.UserID = b.CplUser" ) = null

		
		'本周热议
		sql = B_(tableName).onlysql(1).order("count(b.CplUser) desc").field("b.CplUser").from("{prefix}User as a, {prefix}self_GuestComplaint as b").where( where ).group("b.CplUser").top(15).select
		
		set dict = B_(tableName).onlysql(0).order("count(b.CplUser) desc").field("b.CplUser,COUNT(b.CplUser) AS cnt").from("{prefix}User as a, {prefix}self_GuestComplaint as b").where( where ).group("b.CplUser").top(15).getKeyValue
		
		that.d("cplUserKV") = dict
		
		if POP_MVC.config("DB_TYPE") = "mysql" then
			sql = "SELECT UserID FROM ( " & sql & " ) as c"
		end if
	
		set where = D_
		where( "UserID IN (" & sql & ")" ) = null
		

		set rs = B_(tableName).ResumeOpts().onlysql(0).field("UserID,LoginName,Nickname,Avatar").from("{prefix}User").where(where).order("UserID DESC").select
	
		
		that.d("HotCplUserCount") = rs.recordCount
		that.d("HotCplUser") = rs			
	end sub
end Class
%>