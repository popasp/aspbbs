<%
Class [Public]	
	Private tableName
	
	Private Sub Class_Initialize()
		tableName = "self_GuestComplaint"
	end sub	
	
	'本月/周热门投诉帖子
	sub getHotTopic(SortID)
		dim where,rs,sql,dict
		
		'if isCached then
		'	exit sub
		'end if
		
		set where = D_		
		
		if SortID <> "" and SortID <> 0 then
			where("a.SortID") = Array("IN",SortID)			
		end if
		where("a.TopicID = b.ParentID") = null
		where("b.CplType") = 1
		where( "b.AddTime" ) = Array( "daydiff" , bookConfig.IndexHotDate )


		
		'本周热议
		sql = B_(tableName).onlysql(1).order("count(a.TopicID) desc").field("a.TopicID").from("{prefix}self_GuestTopic as a, {prefix}self_GuestComplaint as b").where( where ).group("a.TopicID").top(15).select
		
		set dict = B_(tableName).onlysql(0).order("count(a.TopicID) desc").field("a.TopicID,COUNT(a.TopicID) AS cnt").from("{prefix}self_GuestTopic as a, {prefix}self_GuestComplaint as b").where( where ).group("a.TopicID").top(15).getKeyValue
		
		that.d("hotKV") = dict
		
		if POP_MVC.config("DB_TYPE") = "mysql" then
			sql = "SELECT TopicID FROM ( " & sql & " ) as c"
		end if
	
		set where = D_
		where( "TopicID IN (" & sql & ")" ) = null
		

		

		set rs = B_(tableName).ResumeOpts().onlysql(0).field("TopicID,Title,ReplyCount,IndexImage,DownURL").from("{prefix}self_GuestTopic").where(where).order("ReplyCount DESC").select
		
		that.d("HotTopicCount") = rs.recordCount
		that.d("HotTopic") = rs			
	end sub
	
	'本月/周热门投诉回帖
	sub getHotReply()
		dim where,rs,sql,dict
		
		'if isCached then
		'	exit sub
		'end if
		
		set where = D_		
		where("a.ReplyID = b.ParentID") = null
		where("b.CplType") = 2
		where( "b.AddTime" ) = Array( "daydiff" , bookConfig.IndexHotDate )

		
		'本周热议
		sql = B_(tableName).onlysql(1).order("count(a.ReplyID) desc").field("a.ReplyID").from("{prefix}self_GuestReply as a, {prefix}self_GuestComplaint as b").where( where ).group("a.ReplyID").top(15).select
		
		set dict = B_(tableName).onlysql(0).order("count(a.ReplyID) desc").field("a.ReplyID,COUNT(a.ReplyID) AS cnt").from("{prefix}self_GuestReply as a, {prefix}self_GuestComplaint as b").where( where ).group("a.ReplyID").top(15).getKeyValue
		
		that.d("hotReplyKV") = dict
		
		if POP_MVC.config("DB_TYPE") = "mysql" then
			sql = "SELECT TopicID FROM ( " & sql & " ) as c"
		end if
	
		set where = D_
		where( "e.ReplyID IN (" & sql & ")" ) = null
		where( "d.TopicID = e.TopicID" ) = null
		

		set rs = B_(tableName).ResumeOpts().onlysql(0).field("e.ReplyID,e.Content,e.IndexImage,d.Title,e.AddTime as ReplyTime").from("{prefix}self_GuestTopic as d, {prefix}self_GuestReply as e").where(where).order("ReplyID DESC").select
		
		that.d("HotReplyCount") = rs.recordCount
		that.d("HotReply") = rs			
	end sub
	
	'本月/周热门投诉者
	sub getHotUser
		dim where,rs,sql,dict
		
		'if isCached then
		'	exit sub
		'end if
		
		set where = D_		
		
		where( "b.AddTime" ) = Array( "daydiff" , bookConfig.IndexHotUserDate )
		where( "a.UserID = b.UserID" ) = null

		
		'本周热议
		sql = B_(tableName).onlysql(1).order("count(b.UserID) desc").field("b.UserID").from("{prefix}User as a, {prefix}self_GuestComplaint as b").where( where ).group("b.UserID").top(15).select
		
		set dict = B_(tableName).onlysql(0).order("count(b.UserID) desc").field("b.UserID,COUNT(a.UserID) AS cnt").from("{prefix}User as a, {prefix}self_GuestComplaint as b").where( where ).group("b.UserId").top(15).getKeyValue
		
		that.d("userKV") = dict
		
		if POP_MVC.config("DB_TYPE") = "mysql" then
			sql = "SELECT UserID FROM ( " & sql & " ) as c"
		end if
	
		set where = D_
		where( "UserID IN (" & sql & ")" ) = null
		

		set rs = B_(tableName).ResumeOpts().onlysql(0).field("UserID,LoginName,Nickname,Avatar").from("{prefix}User").where(where).order("UserID DESC").select
	
		
		that.d("HotUserCount") = rs.recordCount
		that.d("HotUser") = rs			
	end sub
	
	'本月/周热门被投诉者
	sub getHotCplUser
		dim where,rs,sql,dict
		
		'if isCached then
		'	exit sub
		'end if
		
		set where = D_		
		
		where( "b.AddTime" ) = Array( "daydiff" , bookConfig.IndexHotUserDate )
		where( "a.UserID = b.CplUser" ) = null

		
		'本周热议
		sql = B_(tableName).onlysql(1).order("count(b.CplUser) desc").field("b.CplUser").from("{prefix}User as a, {prefix}self_GuestComplaint as b").where( where ).group("b.CplUser").top(15).select
		
		set dict = B_(tableName).onlysql(0).order("count(b.CplUser) desc").field("b.CplUser,COUNT(b.CplUser) AS cnt").from("{prefix}User as a, {prefix}self_GuestComplaint as b").where( where ).group("b.CplUser").top(15).getKeyValue
		
		that.d("cplUserKV") = dict
		
		if POP_MVC.config("DB_TYPE") = "mysql" then
			sql = "SELECT UserID FROM ( " & sql & " ) as c"
		end if
	
		set where = D_
		where( "UserID IN (" & sql & ")" ) = null
		

		set rs = B_(tableName).ResumeOpts().onlysql(0).field("UserID,LoginName,Nickname,Avatar").from("{prefix}User").where(where).order("UserID DESC").select
	
		
		that.d("HotCplUserCount") = rs.recordCount
		that.d("HotCplUser") = rs			
	end sub
	
	'本月/周热门被投诉者
	sub getDealCplUser
		dim where,rs,sql,dict
		
		'if isCached then
		'	exit sub
		'end if
	
		set where = D_
		where( "UserID" ) = Array( "in" , bookConfig.CplDelUsers )
		

		set rs = B_("User").field("UserID,LoginName,Nickname,Avatar").where(where).order("UserID ASC").select
	
		
		that.d("dealCplUserCount") = rs.recordCount
		that.d("dealCplUser") = rs			
	end sub
End Class
%>