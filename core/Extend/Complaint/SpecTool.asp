<%
Class SpecTool
	private SepStr
	Private Sub Class_Initialize
		SepStr = "###"
	End Sub

	'获取所有参数的html
	'使用 K_("SpecTool").getFormHtml("S" , tableName , "FormSet" , "faq" , rs  ) 
	'SpecCategory为分类，默认为 C_("SPEC_FIELD_PREFIX")
	'tableName字段所在的表名
	'SpecTable参数所在表名，有SpectSet:Content，UserSet:User，FormSet:GuestBook
	'upSortType上传文件或图片所在目录
	'rs 根据id从tableName表中取出的一条记录
	Function getFormHtml(ByVal SpecCategory,tableName,SpecTable,upSortType,rs)
		on error resume next
		Dim fieldTitle,fieldName,fieldType,fieldValue,SpecOptions,SpecNotNull,temp,item,key
		Dim kvRS,where,pathWhere
		Dim html,str,tplDir
					
		'模板存放目录
		tplDir = "./Static/inc/Layui/"
		
		set where = D_		
		
		pathWhere = ""
		
		if not isNul( SpecCategory ) then
			pathWhere = "SpecCategory='" & SpecCategory & "'"
		end if
		
		if SpecTable <> "" then
			pathWhere = " and TableName='" & SpecTable & "'"
		end if
			
		if pathwhere = "" then
			pathWhere = ""
		else
			pathWhere = POP_MVC.ltrim( pathWhere , " and " )
			pathwhere = "[" & pathwhere & "]"
		end if

		set kvRS = XM_("Complaint--Configs").db.path( "SpecSet/Line" & pathwhere).field("SpecName,SpecCategory,SpecField,SpecControlType,SpecOptions,SpecNotNull").select

		html = ""

		for each key in kvRS
			set item = kvRS(key)
			fieldTitle = item("SpecName")
			fieldName = item( "SpecField" )
			fieldType = item("SpecControlType")
			SpecOptions = repNull(item("SpecOptions")) 
			SpecNotNull = item("SpecNotNull")
			str = POP_MVC.file_get_contents( tplDir & POP_MVC.config( "CMS_SPEC_HTML_FILE" )(fieldType) & ".html" )
			
			str = ReplaceCommon( str ,fieldType , fieldTitle , fieldName , SpecOptions ,upSortType, SpecNotNull )
			
			if fieldType = 2 then
				fieldValue = ""
				if isObject( rs ) then					
					fieldValue = repNull(rs( fieldName ))
				end if	
				str = replace( str, "##content##" , fieldValue )
			elseif fieldType = 10 then
				str = str & getLinkAgeJs( fieldName,SpecOptions,tableName,SpecTable,rs ,false  )
			end if				
			
			html = html & str & vbcrlf
		next
		getFormHtml = html
	End Function
	
	'获取一个参数的html
	'使用 K_("spectool").getSpecHtml(tableName,SpecTable,fieldTitle,fieldName,fieldType,SpecOptions,SpecNotNull,upSortType)
	'tableName字段所在的表名
	'SpecTable参数所在表名，目前均为FormSet
	'fieldTitle字段名称
	'fieldName字段名
	'fieldType字段类型,0-10
	'SpecOptions参数
	'SpecNotNull必须填写
	'upSortType上传文件或图片所在目录
	Function getSpecHtml(tableName,SpecTable,fieldTitle,fieldName,fieldType,SpecOptions,SpecNotNull,upSortType)
		on error resume next
		Dim tplDir,str,temp,theIndex,j,commonFieldName
	
		'模板存放目录
		tplDir = "./Static/inc/Layui/"
		
		str = POP_MVC.file_get_contents( tplDir & POP_MVC.config( "CMS_SPEC_HTML_FILE" )(fieldType) & ".html" )
		
		str = ReplaceCommon( str ,fieldType, fieldTitle , fieldName , SpecOptions ,upSortType ,  SpecNotNull )

		if fieldType = 2 then
			str = replace( str, "##content##" , "" )
		elseif fieldType = 10 then
			str = str & getLinkAgeJs( fieldName,SpecOptions,tableName,SpecTable,"" , true )
		end if
		getSpecHtml = str
	End Function
	
	Function getSelectOptions( ByVal SpecOptions )
		dim arr,num1,num2,maxCount,pattern,prefix,suffix,action,html,j,cnt
		dim innerStr,tempStr
		getSelectOptions = SpecOptions
		pattern = "^\s*(\D*)\s*(\d+)\s*(\D*)\s*[-]\s*\1(\d+)\s*\3\s*$"
		maxCount = 2000
		html = ""
		

		if SpecOptions = "#" then
			SpecOptions = POP_MVC.file_get_contents( C_("sitePath") & "/static/html/common/select-province.html" )
		end if
		if POP_MVC.string.reg_test( SpecOptions , pattern , "im" ) then
			num1= POP_MVC.string.reg_find( SpecOptions , pattern , 2 , "im" )
			num2= POP_MVC.string.reg_find( SpecOptions , pattern , 4 , "im" )
			prefix= POP_MVC.string.reg_find( SpecOptions , pattern , 1 , "im" )
			suffix= POP_MVC.string.reg_find( SpecOptions , pattern , 3 , "im" )
			num1 = CInt( num1 )
			num2 = Cint( num2 )
			if num2 > num1 then
				action = 1
			else
				action = -1
			end if
			cnt = 0
			for j = num1 to num2 step action
				if cnt = maxCount then
					exit for
				end if
				html = html & "<option value='" & j & "'>" & prefix & j & suffix & "</option>" & vbcrlf
				cnt = cnt + 1
			next
			getSelectOptions = html
		elseif inStr( SpecOptions , SepStr ) > 0 then

			innerStr = "<option value='##value##' selected='selected'>##tip##</option>"
			getSelectOptions = getLoopHtml( SpecOptions , innerStr, "selected='selected'" )
		end if
	End Function

	'获取多级联动的js字符串
	'fieldName 字段名，如LianDong3
	'SpecOptions
	'tableName 表名，如Content
	'SpecTable 参数表名，如SpecSet
	'rs 修改时所需的一条记录
	'StopCheckNext 不进行下一个检测，单个取值true，全部取值false
	Function getLinkAgeJs(fieldName,SpecOptions,tableName,SpecTable,rs ,StopCheckNext )
		dim tempVal,changeStr,j,str,pk,theIndex,commonFieldName,nextIndex,bool,options
		theIndex = right( fieldName , 1 )	

		if isNumeric( theIndex ) then
			theIndex  = CInt(theIndex)
			nextIndex = theIndex + 1
			'取LianDong3中的LianDong
			commonFieldName = left( fieldName, len( fieldName ) - 1 )
			'如果不存在LianDong4
			

			if StopCheckNext or not B_(tableName).fieldExists( commonFieldName & nextIndex ) then	
				str = POP_MVC.file_get_contents( C_("sitePath") & "/static/html/common/linkage-js.html" )
				str = replace( str , "通用字段名" , commonFieldName )
				if isObject(rs) then
					pk = B_(tableName).getPK()
				end if
				for j = 1 to theIndex
					if j <> theIndex then 
						options = XM_("Complaint--Configs").db.path("SpecSet/Line[SpecField='" & (commonFieldName & j) & "']").field("SpecOptions").getOne()
					else
						options = SpecOptions
					end if
					options = repNull( options )
					

					
					'去掉换行符
					options = replace( options , chr(10) , "" )
					options = replace( options , chr(13) , "" )
					


					
					'#代表省份 ##地市 ###县区
					if j = 1 and options = "#" then
						options = POP_MVC.file_get_contents(C_("sitePath") & "/static/html/common/linkage-province.html")
					elseif j = 2 and options = "##" then
						options = POP_MVC.file_get_contents(C_("sitePath") & "/static/html/common/linkage-city.html")
					elseif j = 3 and options = "###" then
						options = POP_MVC.file_get_contents(C_("sitePath") & "/static/html/common/linkage-country.html")
					end if
	
					str = replace( str , "{字段参数" & j & "}" , options )
					

					
					'修改时，还需要取得每个值进行替换
					if isObject( rs ) then
						tempVal = B_(tableName).field( commonFieldName & j ).where( rs(pk) ).getOne()
						tempVal = repNull(tempVal)
						str = replace( str , "{字段值" & j & "}" , tempVal )
						changeStr = changeStr & "$('#" & commonFieldName & j & "').change();" & vbcrlf
						changeStr = changeStr & "$('#" & commonFieldName & j & "').val('" &  tempVal  & "');" & vbcrlf
					else
						str = replace( str , "{字段值" & j & "}" , "" )
					end if
				next
			end if
		end if
		getLinkAgeJs = str
	End Function
	
	Function ReplaceCommon( str , fieldType, fieldTitle , fieldName , SpecOptions , upSortType , SpecNotNull )
	
	
		str = replace( str, "字段名称" ,iif( SpecNotNull - 0 = 0 , "" , "*" ) & fieldTitle  )
	
	
		select case fieldType
			case 3
				str = replace( str , "##sorttype##" , upSortType )
			case 4
				str = replace( str, "{$_C.WDATE_FORMAT}" , POP_MVC.config( "WDATE_FORMAT" ) )
				str = replace( str, "{=POP_MVC.formatdate("""",$_C.WDATE_FORMAT)}" , POP_MVC.formatdate("",POP_MVC.config( "WDATE_FORMAT" )) )
			case 9
				str = replace( str , "##sorttype##" , upSortType )	
		end select	
			
		str = replace( str, "字段名" , fieldName )
		
		str = replace( str, "字段描述" , fieldTitle )
	
		if fieldType = 6 then	
			str = ReplaceCheckLoop( "radio" , str, SpecOptions )
		elseif fieldType = 7 then	
			str = ReplaceCheckLoop( "check" , str, SpecOptions )
		elseif fieldType = 8 then
			SpecOptions = getSelectOptions( SpecOptions )
			str = replace( str, "{字段参数}" , repNull(SpecOptions) )
		end if
		ReplaceCommon = str
	End Function
	
	Function ReplaceCheckLoop(ByVal stype , ByVal str, ByRef SpecOptions )
		dim matchstr,loopstr
		
		'匹配替换块，如{radio}{/radio}  {check}{/check}
		matchstr = POP_MVC.String.reg_fetch( str , "{" & stype & "}([\s\S]+){\/" & stype & "}" ,"im" )
		
		'匹配需循环替换的代码
		innerstr = POP_MVC.String.reg_find( str , "{" & stype & "}([\s\S]+){\/" & stype & "}" , 1 ,"im" )
		
		'使用###分割
		loopstr = getLoopHtml( SpecOptions , innerstr , "checked='checked'" )
		str = replace( str , matchstr , loopstr )
		ReplaceCheckLoop = str
	End Function
	
	Function getLoopHtml(SpecOptions,innerstr,checkStr)
		dim loopstr,arr,tempArr,tip,value,tempStr,j	
		arr = split( SpecOptions , SepStr )
		loopstr = ""
		for j = 0 to ubound(arr)
			tempStr =  replace( innerstr , "##index##" , j )	
			if inStr( arr(j) , "#" ) > 0 then
				tempArr = split( arr(j) , "#" )
			else
				tempArr = split( arr(j) , "|" )
				tempStr =  replace( tempStr , checkStr , "" )
			end if
			
			if ubound( tempArr ) > 0 then
				tip = tempArr( 0 )
				value = tempArr( 1 )
			else
				tip = tempArr( 0 )
				value = tip
			end if
			tempStr =  replace( tempStr , "##tip##"   , tip   )
			tempStr =  replace( tempStr , "##value##" , value )
			loopstr = loopstr & tempstr
		next
		getLoopHtml = loopstr
	End Function
	
	Function getModelValidate( ByRef TableName )
		getModelValidate = Array( _
			Array( "SpecName" ,"",  "参数名称不能为空" , 0, "notempty" , 3  ) _
			,Array( "SpecField" ,"\w{1,90}",  "字段名称只能为英文字符、数字、下划线" , 0, "regex" , 3  ) _
			,Array( POP_MVC.form("SpecCategory") & "_" & POP_MVC.form("SpecField"),TableName, POP_MVC.form("SpecCategory") & "_" & POP_MVC.form("SpecField") & " 字段名称已经存在，请修改" , 1, "fieldUnique" , 1  ) _
		)
	End Function
	
	'扩展M_().db.validate_
	Function getValidate( ByVal validateArr , ByRef SpecTable , ByRef TableName, ByVal SpecCategory )
		dim where,tempArr,fieldName,specName,rs,SpecNotNull,SpecValidate
		
		if IsNul(SpecCategory) then
			SpecCategory = C_("SPEC_FIELD_PREFIX")
		end if
		
		set where = D_
		if not isNul( TableName ) then
			where("TableName") = TableName
		end if
		where("SpecCategory") = SpecCategory
		'where("SpecNotNull") = 1
		
		set rs = B_(SpecTable).field( "SpecCategory+'_'+SpecField,SpecName,SpecNotNull,SpecValidate" ).where( where ).order("SpecOrder ASC,SpecID ASC").select
		tempArr = Array()

		do while not rs.eof		
			specName  = repNull( rs(1) )
			fieldName = repNull( rs(0) )
			SpecNotNull = repNull( rs("SpecNotNull") )
			SpecValidate = repNull( rs("SpecValidate") )
			if SpecNotNull = 1 then
				POP_MVC.Arr.push tempArr,Array( fieldName ,"",  specName & "不能为空"  , 0, "notempty" , 3  )
			end if
			if not isNul( SpecValidate ) then
				Call SetValidate( tempArr, fieldName, specName, SpecValidate, SpecNotNull )
			end if
			rs.movenext
		loop
		
		if not isArray( validateArr ) then
			validateArr = Array()
		end if
		
		validateArr = POP_MVC.Arr.merge( validateArr , tempArr )
		getValidate = validateArr
	End Function
	
	'添加附加验证
	Sub SetValidate(ByRef tempArr ,ByRef fieldName,ByRef specName, ByRef SpecValidate,ByVal SpecNotNull)
		dim arr,itemSep,line,lineArr,item,temp2,cnt
		itemSep = "$$"
		arr = split( SpecValidate , SepStr )
		if SpecNotNull = 1 then
			SpecNotNull = 1
		else
			SpecNotNull = 2
		end if
		for i = 0 to ubound(arr)
			line = arr(i)
			lineArr = split( line , itemSep )
			cnt = ubound(lineArr)
			if cnt > 0 then
				if ubound( lineArr ) > 1 then
					temp2 = lineArr(2)
				else
					temp2 = ""				
				end if
				item = Array( fieldName ,temp2 ,  replace( lineArr(1) , "{name}" , specName )  , SpecNotNull, lineArr(0) , 3  )
				POP_MVC.Arr.push tempArr, item
			end if
		next	
	End Sub
	
	Sub AddField( ByVal tableName,ByVal SpecControlType,ByRef fieldName)
		if SpecControlType = 2 then
			Call B_(tableName).addfield( "" , fieldName, "memo" )
		else
			Call B_(tableName).addfield( "" , fieldName, "nvarchar(255)" )
		end if
	End Sub
End Class
%>