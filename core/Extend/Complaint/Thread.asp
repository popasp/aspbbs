<%
Class Thread
	sub init
		'表名 
		'db是popasp_access类的实例化
		db.tableName = "Self_GuestComplaint"
		

		
		'主键
		db.prikey = "CplID"
		
		db.auto_ = Array( _
			 Array( "Visits" ,0,  1 ) _
			,Array( "AddTime" ,"POP_MVC.FormatDate( now() , ""YYYY-MM-DD HH:II:SS"" )",  1 , "function" ) _
			,Array( "EditTime" ,"POP_MVC.FormatDate( now() , ""YYYY-MM-DD HH:II:SS"" )",  2 , "function" ) _
			,Array( "EditIP" , "" ,  1 ) _
			,Array( "EditIpAddr" , "" , 1 )_
			,Array( "TitleColor" ,"#000000",  1 ) _
			,Array( "ReplyCount" ,"0",  1 ) _
			,Array( "UserID" , Session("adminId") ,  1 ) _
			,Array( "ContentStatus" , 0 ,  1 ) _
			,Array( "ZanCount" , 0 ,  1 ) _
			,Array( "CaiCount" , 0 ,  1 ) _
			,Array( "Progress" , "等待审理" ,  1 ) _
			,Array( "AnswerReward" , 0 ,  1 ) _
			,Array( "IsTop" , 1 ,  1 ) _
			,Array( "IsClosed" , 0 ,  1 ) _
			,Array( "IsSolved" , 0 ,  1 ) _
			,Array( "MsgStatus" , 0 ,  1 ) _
			,Array( "IP" , "get_client_ip" ,  1, "function" ) _
			,Array( "IpAddr" , "Complaint--Thread.getIpAddr2" , 1 , "callback" )_
			,Array( "EditIP" , "get_client_ip" ,  2, "function" ) _
			,Array( "EditIpAddr" , "Complaint--Thread.getIpAddr2" , 2 , "callback" )_
			,Array( "IndexImage" ,"Complaint--Thread.getIndexImage" , 3 , "callback" )_
			,Array( "Title" , "Complaint--Thread.stripTags" , 3 , "callback" , array() )_
			,Array( "SrcTitle" , "Complaint--Thread.stripTags" , 3 , "callback" , array() )_
			,Array( "Content" , "K_(""bbs--Public"").replaceLink" , 3 , "function" , array() )_
		)
		
		'数据验证
		db.validate_ = Array( _
			 Array( "Title" ,"",  "标题不能为空"  , 0) _
			,Array( "Title","0,255","标题长度不能超过255个字符" , 0 , "length" , 3 ) _
			,Array( "LianDong1","","请选择投诉类型" , 0 , "neq" , 3 ) _
			,Array( "LianDong2","","请选择投诉原因" , 0 , "neq" , 3 ) _
			,Array( "verify","","验证码不正确" , 0 , "verify" , 3 ) _
		)
	end sub
	
	function stripTags( value )
	

		if isNul(value) then
			stripTags = ""
		else
			stripTags = POP_MVC.String.strip_tags( value , "html" )
		end if
		'去掉换行
		stripTags = Replace(stripTags, Chr(10), "")
		stripTags = Replace(stripTags, Chr(13), "")
		
		'将连续三个空格替换成空
		stripTags = Replace(stripTags, "   ", "")
	End function
	
	
	Function getIpAddr2
		dim ip
		ip = get_client_ip
		getIpAddr2 = getIpAddr(ip)
	End Function
	
	
	Function getIpAddr( ip )
		dim attr
		
		attr = getTableIpAddr( "self_GuestTopic" , "IP" , ip , "IpAddr" )
		if not isNul(attr) then
			getIpAddr = attr
			exit function
		end if
		
		attr = getTableIpAddr( "self_GuestTopic" , "EditIP" , ip ,  "EditIpAddr" )
		if not isNul(attr) then
			getIpAddr = attr
			exit function
		end if	
		
		attr = getTableIpAddr( "self_GuestReply" , "IP" , ip ,  "IpAddr" )
		if not isNul(attr) then
			getIpAddr = attr
			exit function
		end if	
		
		attr = getTableIpAddr( "self_GuestReply" , "EditIP" , ip ,  "EditIpAddr" )
		if not isNul(attr) then
			getIpAddr = attr
			exit function
		end if
		
		if getIpAddr = "" then
			getIpAddr = K_("bbs--ip").getTQQWryAddr(ip)
		end if
	End Function
	
	Function getIndexImage(  )
		getIndexImage = objCMS.Fetch_Img( request.form("Content") )
	End Function
	
	Function getTableIpAddr(table,IpField , IpValue,AttrField)
		getTableIpAddr = K_("bbs--ip").getTableIpAddr( table,IpField,IpValue,AttrField )
	End Function
End Class
%>