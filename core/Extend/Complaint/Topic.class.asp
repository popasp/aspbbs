<%
Class Topic 'Extends bbs--Common
	private tableName,isWap,fileType,theStatus,listAll,TopicFieldStr,TopicFromTable,IsSolved,theCplID
	private orderStr
	
	sub install
		dim sql
		if not B_( "user" ).tableExists("self_GuestUpload") then
			sql = POP_MVC.file_get_contents( POP_MVC.config("CORE_NAME") & "/Extend/UploadLog/sql.sql" )
			Call B_( "user" ).Exec( sql )
		end if
		that.success( "安装完成!" )
	end sub
	
	sub initialize
		tableName = "self_GuestComplaint"
		
		set bookConfig = XM_( "Complaint--Configs" ).db.path("Configs/Config").field("ConfigName,ConfigValue").getObject
		that.d("bookConfig") = bookConfig
		
		if not DealCplPower() then
			that.error("您无权访问!")
		end if
		
		isWap = checkWap()
	
		objCMS.cmsPrefix = "aspbbs"
		guestConfig.DefaultTemplate = "complaint"
		objCMS.htmlFilePath = "html"
		theStatus = 1
		listAll = false
'isClosed

		TopicFieldStr = "b.CplID,b.MsgStatus,b.IsClosed as CplClosed,b.ParentID,a.TopicID,a.SortID,a.Content,a.ZanCount,a.AddTime,a.EditTime,a.IndexImage,a.Title,a.IpAddr as tIpAddr,a.EditIpAddr as tEditIpAddr,a.ContentStatus,a.IsTop,a.Isrecommend,a.IsHeadline,a.IsFeatured,a.IsClosed,a.IsReplyView,a.Visits,a.ReplyCount,a.AnswerReward,a.IsSolved,c.LoginName,c.UserStatus,c.ThreadStatus,c.ReplyStatus,c.Nickname,c.GroupID,c.Avatar,c.UserID,c.Experience,c.ThreadReplyView,b.Title as theTitle,b.CplUser,b.Content as theContent,b.SrcTitle,b.SrcContent,b.AddTime as theTime,b.LianDong1,b.LianDong2,b.Progress,b.IpAddr,(select UserID from {prefix}User where UserID = b.UserID ) as theUser,(select LoginName from {prefix}User where UserID = b.UserID ) as theName,(select Nickname from {prefix}User where UserID = b.UserID ) as theNick,(select Avatar from {prefix}User where UserID = b.UserID ) as theAvatar,(select GroupID from {prefix}User where UserID = b.UserID ) as theGroup,(select Experience from {prefix}User where UserID = b.UserID ) as theExperience,(select UserStatus from {prefix}User where UserID = b.UserID ) as theUserStatus"

		
		TopicFromTable = "{prefix}self_GuestTopic as a,{prefix}self_GuestComplaint as b,{prefix}User as c"
		
		that.d("RemoveType") = "Topic"
		that.d("EditType") = "Topic"
	end sub
	
	sub remove
		Call A_("bbs--Topic/remove")
	end sub

	
	sub solved
		isSolved = 1
		Call index
	end sub
	
	sub unsolved
		isSolved = 0
		Call index
	end sub	
	
	sub detail
		Call index
	end sub
	
	sub index

		dim content,key,page
		orderStr = "CplID DESC"
		
		page = that.get("page")
		
		if page = "" then
			page = 1
		end if
		

		
		Call K_("complaint--Public").getHotTopic(0)
		
		Call K_("complaint--Public").getHotReply
		Call K_("complaint--Public").getHotUser
		Call K_("complaint--Public").getHotCplUser

		Call getTopicListData()
		
		
		
		objCMS.output	
		'Call clearRS
	end sub
	
	private sub getTopicListData()
		dim rsObj,where,template,topicRS,soStr
		
		set where = D_
		
		
		'where("a.ContentStatus") = theStatus
		
		if not isEmpty( isSolved ) then
			if isSolved = 0 then
				where( "b.IsSolved" ) = 0
			else
				where( "b.IsSolved" ) = Array( "<>" ,0 )
			end if			
		end if
		
		where("b.CplUser = c.UserID") = null
		where("b.IsTop") = 1
		where("b.CplType") =1
		
		if LCase(POP_MVC.a) = "detail" then
			where("b.CplID") = that.get("id")
		end if
		
		where("a.TopicID = b.ParentID") = null

		'按投诉者UserID查询
		if POP_MVC.req("UserID") <> "" then
			where("b.UserID") = POP_MVC.req("UserID")
		'按被投诉者UserID查询，在投诉表中为CplUser
		elseif POP_MVC.req("CplUser") <> "" then
			where("b.CplUser") = POP_MVC.req("CplUser")
		'按投诉日期查询
		elseif POP_MVC.req("date") <> "" then
			where("b.AddTime") = Array( "date" , POP_MVC.req("date") )
		'按投诉者IP属地查询
		elseif POP_MVC.req("addr") <> "" then
			where("b.IpAddr") =POP_MVC.req("addr")
		'按被投诉者IP属地查询
		elseif POP_MVC.req("taddr") <> "" then
			where("a.IpAddr") = POP_MVC.req("taddr")
		end if

		Call B_("self_GuestConfig").onlysql(0).page(array( null, guestConfig.IndexListPageCount ) ).from( TopicFromTable ).field( TopicFieldStr ).where( where )

		if POP_MVC.req("keys") <> "" then
			'适用于搜索
			orderStr = "CplID DESC"
			set topicRS= B_("self_GuestConfig").order(orderStr).search(POP_MVC.req("keys" ),"b.Title" )
			that.d("recordCount") = topicRS.RecordCount		
		else
			if orderStr <> "" then
				set topicRS= B_("self_GuestConfig").order(orderStr).select
			else
				set topicRS= B_("self_GuestConfig").select
			end if
			that.d("recordCount") = topicRS.RecordCount
		end if
		
		
		that.d("thread") = topicRS	

	
		'Call getHotTopic(SortID)
		'Call getNewReply(SortID)
		
		dim obj
		set obj = objCMS
		
		if LCase(POP_MVC.a) = "search" then
			template = "Topic/search"
			templatePath= obj.getTemplatePath( template )
			
			
			if POP_MVC.req("keys") <> "" then
				soStr = "&keys=" & POP_MVC.req("keys")
			elseif POP_MVC.req("UserID") <> "" then
				soStr = "&UserID=" & POP_MVC.req("UserID")
			elseif POP_MVC.req("CplUser") <> "" then
				soStr = "&CplUser=" & POP_MVC.req("CplUser")
			elseif POP_MVC.req("date") <> "" then
				soStr = "&date=" & POP_MVC.req("date")
			elseif POP_MVC.req("addr") <> "" then
				soStr = "&addr=" & POP_MVC.req("addr")
			elseif POP_MVC.req("taddr") <> "" then
				soStr = "&taddr=" & POP_MVC.req("taddr")
			end if
			that.d("soStr") = soStr
		else
		
			
			template = "Topic/index"		
			templatePath= obj.getTemplatePath( template )
		end if
	
		obj.load(templatePath)	
		obj.parseHtml	
	end sub
	
	
	'搜索投诉帖子
	sub search
		dim SearchInterval,keys
		guestConfig.ListCacheLifeTime=0
		if guestConfig.SearchInterval - 0 > 0 and  not DealCplPower() then
			SearchInterval = Request.Cookies("LastSearchDate")

			if isDate(SearchInterval) then			
				if datediff( "s" , SearchInterval , now ) - guestConfig.SearchInterval < 0 then
					that.error( Array( "您搜索的太频繁了，系统指定 " & guestConfig.SearchInterval & " 秒钟可搜索一次" , -1 ) )
				end if		
			end if		
			
			P_("cookie").unit = "s"
			P_("cookie").Expires = CInt( guestConfig.SearchInterval )
			P_("cookie").set "LastSearchDate",now
		end if		
	
		if POP_MVC.req("keys") <> "" then
			'限定搜索字符串长度不超过20，可有效防止一句话木马
			keys = POP_MVC.req("keys")
			if len(keys) > 20 then
				that.error("搜索字符串不能超过20个字符！！！")
			end if
			that.d("SearchTip") = ""
			
			that.d("SearchTip") = "帖子"		
			
			that.d("SearchText") = keys
			Call index		
		elseif POP_MVC.req("UserID") <> "" then
			if not that.isID(POP_MVC.req("UserID")) then
				that.error("搜索的投诉者ID不正确！！！")
			end if
			set rs = B_("User").where( that.dict( "UserID" , POP_MVC.req("UserID") ) ).field("Nickname,LoginName").find
			if rs.eof then
				that.error("搜索的投诉者ID不存在！！！")
			end if
			that.d("SearchTip") = "投诉者"
			if rs("Nickname") <> "" then
				that.d("SearchText") = CStr(rs("Nickname"))
			else
				that.d("SearchText") =  CStr(rs("LoginName"))
			end if
			that.u(rs)
			Call index
		elseif POP_MVC.req("CplUser") <> "" then
			if not that.isID(POP_MVC.req("CplUser")) then
				that.error("搜索的被投诉者ID不正确！！！")
			end if
			set rs = B_("User").where( that.dict( "UserID" , POP_MVC.req("CplUser") ) ).field("Nickname,LoginName").find
			if rs.eof then
				that.error("搜索的被投诉者ID不存在！！！")
			end if
			that.d("SearchTip") = "被投诉者"
			if rs("Nickname") <> "" then
				that.d("SearchText") = CStr(rs("Nickname"))
			else
				that.d("SearchText") =  CStr(rs("LoginName"))
			end if
			that.u(rs)
			Call index
		elseif POP_MVC.req("date") <> "" then
			if not isDate( POP_MVC.req("date") ) then
				that.error("搜索的日期格式不正确")
			end if
			that.d("SearchTip") = "投诉日期"
			that.d("SearchText") = POP_MVC.req("date")
			Call index
		elseif POP_MVC.req("addr") <> "" then
			if len( POP_MVC.req("addr") ) > 15 then
				that.error("搜索的投诉者IP属地格式不正确")
			end if
			that.d("SearchTip") = "投诉者IP属地"
			that.d("SearchText") = POP_MVC.req("addr")
			Call index
		elseif POP_MVC.req("taddr") <> "" then
			if len( POP_MVC.req("taddr") ) > 15 then
				that.error("搜索的被投诉者IP属地格式不正确")
			end if
			that.d("SearchTip") = "被投诉者IP属地"
			that.d("SearchText") = POP_MVC.req("addr")
			Call index
		else
			
		end if	
		if POP_MVC.config("SHOW_PAGE_TRACE") = 0 then
			Call POP_MVC.ClearSqlRS("*")
		end if
	end sub	
end Class
%>