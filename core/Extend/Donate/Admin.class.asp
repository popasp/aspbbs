<%
Class Admin 'Extends bbs--Common
	private tableName,orderStr,auditStatus
	private sqlfile,extendName
	sub initialize
		if S_("IsAdmin") <> 1 then
			that.error("无权操作!")
		end if	
	
		tableName = "self_GuestDonate"
		extendName = "Donate"
		
		set bookConfig = XM_(  extendName & "--Configs" ).db.path("Configs/Config").field("ConfigName,ConfigValue").getObject
		that.d("bookConfig") = bookConfig		
	
		objCMS.cmsPrefix = "aspbbs"
		guestConfig.DefaultTemplate = extendName
		objCMS.htmlFilePath = "html"	
		isWap = checkWap()		
		sqlfile = POP_MVC.config("CORE_NAME") & "/Extend/" & extendName & "/" & C_("DB_TYPE") & ".sql"	
	
		that.d("ExtendName") = LCase(extendName)
		tableName = "self_GuestDonate"
		that.d("layuicachepage") = "donate"
	end sub
	
	''''''以下是站长允许的操作''''''	
	'安装数据表
	sub install
		Call K_("bbs--Tools").InstallTable( extendName , tableName, sqlfile )
	end sub
	
	'缷载数据表
	sub unstall
		Call K_("bbs--Tools").UnstallTable( extendName , tableName )
	end sub
	
	'生成建表语句
	sub createSql
		Call K_("bbs--Tools").createSql( tableName , sqlfile )
	end sub
	
	'查看建表语句
	sub viewSql
		Call K_("bbs--Tools").viewSql( sqlfile )
	end sub
	
	'查看XML配置文件
	sub viewXML
		Call K_("bbs--Tools").viewSql( "./templates/" & extendName & "/#data/#book#.xml" )
	end sub
	''''''以上是站长允许的操作''''''
	
	'首页，跳转到设置页面
	sub index
		Call settings
	end sub	

	'配置
	sub settings
		dim mailType,data,html
		'取得配置
		set data = XM_("Donate--Configs").db.path("Configs/Config").select		
		html = P_("flyui4xml").PageConfigContent(data)		
		that.d("htmlstr") = html
		
		'取得图片
		filepath = POP_MVC.appPath & "/runtime/redundancy/table.txt"
		if POP_MVC.file.isfile(filepath) then
			time1 = POP_MVC.file.mtime( filepath )
			time2 = POP_MVC.file.mtime( POP_MVC.config("CMS_UPLOAD_PATH") )
			if  DateDiff("s", time2, time1) > 0 then
				that.d("loopstr") = K_("bbs--FileTool").file_get_test("redundancy")
			end if
		end if
		
		objCMS.show( "Admin/set" )
	end sub		
	
	
	sub DoRemove
		dim id,where
		id = that.req("id")
		
		B_(tableName).where(id).remove
		that.success("删除成功")
	end sub
	
	'基本设置页面
	sub AddDonate
		dim rs
		set rs = B_(tableName).order("DonateID DESC").page("1,30").select
		that.d("RecordCount") = rs.RecordCount
		that.d("donateData") = rs
		objCMS.show( "Admin/add" )
	end sub
	
	Sub DoAdd
		session("DonateInfo") = that.form("DonateInfo")
		session("PayAPP") = that.form("PayAPP")
		session("IndexImage") = that.form("IndexImage")
		that.success( Array("请确认" , "?" & extendName & "--Admin_AddConfirm") )
	End Sub
	
	Sub DoConfirm		
		dim dict,id
		set dict = POP_MVC.form2dict
		if not K_("bbs--Public").ModelCreate("Donate--Donate",dict,1) then
			that.error( M_("Donate--Donate").db.error )
		end if
		id = K_("bbs--Public").ModelAdd( "Donate--Donate" , null )
		if id > 0 then
			that.success( Array("添加成功" , "?" & extendName & "--Admin_AddDonate") )
		end if 
	End Sub
	
	Sub AddConfirm
		dim dict
		
		if session("PayAPP") = "支付宝" then
			set dict = P_("payment").alipay( session("DonateInfo") , 1 )
		elseif session("PayAPP") = "微信" then
			set dict = P_("payment").alipay( session("DonateInfo") , 1 )
		else
			set dict = P_("payment").income( session("DonateInfo") )
		end if
		that.d("pay") = dict
		objCMS.show( "Admin/confirm" )
	End Sub
	
	'js自动填充
	sub jsAuto
		dim dict
		set dict = XM_( extendName & "--Configs").db.path("Configs/Config").field("ConfigName,ConfigValue").getKeyValue
		
		Call K_("bbs--Tools").js_Auto( dict )
	end sub	
	
	'修改配置值
	sub doEditConfigs
		dim key,dict,MailAlert
		set dict = POP_MVC.form2dict
		
		for each key in dict
			Call XM_("Donate--Configs").db.path("Configs/Config[ConfigName='"& key &"']").MergeField( "ConfigValue" , dict(key) )
		next

		Call K_( "bbs--Public" ).CtrlLog( 1, "用户ID: " & S_("adminId") & " ，修改了配置" , "保存成功" )
	end sub
end Class
%>