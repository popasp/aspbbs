<%
Class Donate
	sub init
		'表名 
		'db是popasp_access类的实例化
		db.tableName = "self_GuestDonate"
		
		'主键
		db.prikey = "DonateID"
		
		db.auto_ = Array( _
			Array( "AddTime" ,"POP_MVC.FormatDate( now() , ""YYYY-MM-DD HH:II:SS"" )",  1 , "function" ) _
			,Array( "UserID" , 0 ,  3 ) _			
			,Array( "AdminID" , Session("adminId") ,  3 ) _			
		)
		
		db.validate_ = Array( _
			Array( "DealNo"  ,"self_GuestDonate",  "重复的订单号" ,1, "unique" , 3  ) _
		)
	end sub
End Class
%>