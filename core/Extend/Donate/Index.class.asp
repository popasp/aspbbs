<%
'''捐赠
Class Index 'Extends bbs--Top
	private tableName,orderStr,auditStatus
	private sqlfile,extendName
	sub initialize
		if S_("IsAdmin") <> 1 then
			that.error("无权操作!")
		end if	
	
		tableName = "self_GuestDonate"
		extendName = "Donate"
		
		set bookConfig = XM_(  extendName & "--Configs" ).db.path("Configs/Config").field("ConfigName,ConfigValue").getObject
		that.d("bookConfig") = bookConfig		
	
		objCMS.cmsPrefix = "aspbbs"
		guestConfig.DefaultTemplate = extendName
		objCMS.htmlFilePath = "html"	
		isWap = checkWap()		
		sqlfile = POP_MVC.config("CORE_NAME") & "/Extend/" & extendName & "/" & C_("DB_TYPE") & ".sql"	
	
		that.d("ExtendName") = LCase(extendName)
		tableName = "self_GuestDonate"
		that.d("layuicachepage") = "donate"
	end sub	


	''登陆页面
	sub index
		objCMS.show( "Index/index" )
	end sub
	
	sub Search
		dim dict,data,page,pagesize,key,item,sumPrice,cnt
		page = POP_MVC.get("page")
		pagesize = POP_MVC.get("limit")
		if isNul( page ) then page = 1
		if isNul( pagesize ) then pagesize = 10
		
		set data = B_(tableName).field( "Amount as price,PayInfo as authDesc,DealTime as create_time,FromUser,UserID as uid" ).page(Array( page,pagesize )).getAll
		for each key in data
			set item = data(key)
			item("update_time") = item("create_time")
			set item("user") = D_
			item("user")("username") = item("FromUser")
			item("user")("uid") = item("uid")
			if item("uid") <> 0 then
				item("user")("avatar") = B_("User").where( item("uid") ).field("Avatar").getOne
			else
				item("user")("avatar") = ""
			end if
			
			item.remove("FromUser")
			item.remove("uid")
		next
		sumPrice = B_(tableName).sum( "Amount" )
		cnt = B_(tableName).count()
		set dict = D_
		dict("code") = 0
		dict("count") = cnt
		dict("sumPrice") = sumPrice
		set dict("data") = data
		'POP_MVC.ajax(dict)
		that.AjaxData( dict )
	end sub

end Class
%>