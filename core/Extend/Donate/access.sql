CREATE TABLE [iAspCms_self_GuestDonate] ( 
	[DonateID] Integer  NOT NULL IDENTITY,
	[Amount] VarChar ,
	[FromUser] VarChar ,
	[PayInfo] VarChar ,
	[DealTime] DateTime ,
	[DealMethod] VarChar ,
	[DealNo] VarChar ,
	[IndexImage] VarChar ,
	[DonateInfo] Text ,
	[AddTime] DateTime ,
	[AdminID] Integer ,
	[UserID] Integer ,
	PRIMARY KEY ([DonateID])
);