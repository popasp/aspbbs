<%
Class Admin 'Extends bbs--Common
	private orderStr,statusField,TimeDict,dayDiff
	private tableName,sqlfile,extendName,bookConfig
	
	Sub initialize
		if S_("IsAdmin") <> 1 then
			that.error("无权操作!")
		end if
	
		tableName = "Self_UploadLog"
		extendName = "UploadLog"		
		
		set bookConfig = XM_(  extendName & "--Configs" ).db.path("Configs/Config").field("ConfigName,ConfigValue").getObject
		that.d("bookConfig") = bookConfig
	
		objCMS.cmsPrefix = "aspbbs"
		guestConfig.DefaultTemplate = extendName
		objCMS.htmlFilePath = "html"
		isWap = checkWap()		
		sqlfile = POP_MVC.config("CORE_NAME") & "/Extend/" & extendName & "/" & C_("DB_TYPE") & ".sql"		
		
		dayDiff = ""
	End Sub
	
	''''''以下是站长允许的操作''''''	
	'安装数据表
	sub install
		Call K_("bbs--Tools").InstallTable( extendName , tableName, sqlfile )
	end sub
	
	'缷载数据表
	sub unstall
		Call K_("bbs--Tools").UnstallTable( extendName , tableName )
	end sub
	
	'生成建表语句
	sub createSql
		Call K_("bbs--Tools").createSql( tableName , sqlfile )
	end sub
	
	'查看建表语句
	sub viewSql
		Call K_("bbs--Tools").viewSql( sqlfile )
	end sub
	
	'查看XML配置文件
	sub viewXML
		Call K_("bbs--Tools").viewSql( "./templates/" & extendName & "/#data/#book#.xml" )
	end sub
	''''''以上是站长允许的操作''''''
	
	'还原
	sub restore
		dim id,rs,dict,uploadDir
		id = that.req("id")
		set rs = B_(tableName).where(id).find
		
		if rs.eof then
			that.error( "该文件不存在！" )
		end if
		uploadDir = POP_MVC.file.dir( rs("UploadPath") )
		Call POP_MVC.CreateFolder( uploadDir )
		Call POP_MVC.file.rename( rs("DelPath") , rs("UploadPath") )
		set dict = D_
		dict("DelPath") = DelPath
		dict("DelTime") = now()
		dict("LogStatus") = 1
		Call B_(tableName).where(id).setField( dict,null )
		that.success( "成功将上传文件置入回收站" )
	end sub	
	
	
	'第一次软删除
	sub remove
		dim id,rs,dict,DelPath,recycleDir
		id = that.req("id")
		
		set rs = B_(tableName).where(id).find
		
		if rs.eof then
			that.error( "该文件不存在！" )
		end if
		recycleDir = "#recycle/" & rs("UserID") & "/"
		DelPath = recycleDir & POP_MVC.file.basename(rs("UploadPath"))
		Call POP_MVC.CreateFolder( recycleDir )
		Call POP_MVC.file.rename(rs("UploadPath"),DelPath)
		set dict = D_
		dict("DelPath") = DelPath
		dict("DelTime") = now()
		dict("LogStatus") = 2
		Call B_(tableName).where(id).setField( dict,null )
		that.success( "成功将上传文件置入回收站" )
	end sub
	
	'第2次删除
	sub remove2
		dim id,rs,dict,DelPath,recycleDir
		id = that.req("id")
		set rs = B_(tableName).where(id).find
		
		if rs.eof then
			that.error( "该文件不存在！" )
		end if
	
		Call POP_MVC.file.remove( CStr(rs("DelPath")) )
		set dict = D_

		dict("DelTime") = now()
		dict("LogStatus") = 3
		Call B_(tableName).where(id).setField( dict,null )
		that.success( "成功将上传文件删除" )
	end sub
	
	sub checkStatus
		dim rs,where,DelPath,UploadPath,dict,cnt
		set where = D_
		where("LogStatus") = Array( "eq" , 1 )
		set rs = B_(tableName).where(where).select
		
		cnt = 0
		set dict = D_
		do while not rs.eof 
			UploadPath = rs("UploadPath")
			dict.removeAll
			if Not POP_MVC.file.isFile( UploadPath ) then
				cnt = cnt + 1
				DelPath = "#recycle" & objCMS.SubStrUserUploadFolder( UploadPath )
				if POP_MVC.file.isFile( DelPath ) then
					dict("LogStatus") = 2
					dict("DelPath") = DelPath
					Call B_(tableName).where( rs("LogID") ).setField( dict , null )
				else
					dict("LogStatus") = 3
					dict("DelPath") = ""
					Call B_(tableName).where( rs("LogID") ).setField( dict , null )
				end if				
			end if
			rs.moveNext
		loop
		that.success( cnt & " 个文件状态校正完成" )
	end sub
	
	sub upfile2db
		dim str
		str = K_("UploadLog--Tools").upload2db( POP_MVC.config("CMS_UPLOAD_PATH") & "/1" )
	end sub
	
	sub clearNoExists
		dim rs,where,count
		
		count = 0 
		set rs = B_(tableName).field("LogID,UploadPath").select
		do while not rs.eof
			if NOT POP_MVC.file.isFile( rs("UploadPath") ) then
				POP_MVC.file.remove( rs("UploadPath") )
				B_(tableName).where( rs("LogID") ).remove
				count = count + 1
			end if
			rs.MoveNext
		loop
		
		that.success( "共清除 " & count & " 个不存在的文件!" )
	end sub
	
	sub index
		Call settings
	end sub
	
	'查看回收站文件
	sub viewRecycle
		dim id,rs,DelPath,fileType
		dim Ado		
		id = that.get("id")
		set rs = B_(tableName).where( id ).find
		DelPath = rs("DelPath") 
		
		if not POP_MVC.file.isFile( DelPath ) then
			that.error("文件已物理删除！无法查看！")
		end if
		fileType = POP_MVC.file.getFileType(DelPath)
		if fileType = "img" then			
			Set Ado = POP_MVC.CreateStream 'Ado对象
			Ado.Mode = 3 '1 读，2 写，3 读写。
			Ado.Type = 1 '1 二进制，2 文本。
			Ado.Open
			Ado.LoadFromFile( POP_MVC.realPath(DelPath) ) '载入文件
			i = 0    '计数器
			r = 1024 '每次读取大小(byte)
			
			Response.AddHeader "Pragma","no-cache"
			Response.AddHeader "cache-ctrol","no-cache"
			Response.ContentType = "Image/" & POP_MVC.file.extName( Array(DelPath, true) )
			While i < Ado.Size '循环读取直到读完为止
				Response.BinaryWrite Ado.Read(r) '输出二进制数据流
				Response.Flush '立即发送(要求至少256字节)，不加的话可能提示超过缓存区。
				i = i + r '累加计数器
			Wend
			Ado.Close '关闭文件对象
		else
			var_export DelPath			
		end if
		
		that.u(rs)
		Response.End		
	end sub
	
	'回收站
	sub recycle
		dim where,rs,obj,UserID,fields		
		set where = D_		
		where("a.UserID = b.UserID") = null
		where("a.LogStatus") = 2
		
		if POP_MVC.get("id") = "" then
			POP_MVC.get("id") = 10000
		end if
		
		dayDiff = that.get("id")
		
		if dayDiff <> "" and dayDiff <> "10000" then
			where("a.LogTime") = Array( "daydiff" , dayDiff )
		end if
		fields = "b.UserID as UserID,a.LogID,a.LogStatus,a.ZanCount,a.FileType,a.IsImage,a.ZanUser,a.UploadPath,a.DelPath,a.OriginName,a.LogTime,a.FileSize,b.LoginName,b.Nickname,b.Avatar"
		set rs = B_( objCMS.baseTable ).from( "{prefix}self_UploadLog as a,{prefix}User as b" ).page("null,20").order("a.LogTime DESC,a.LogID DESC").field(fields).where(where).select		
		
		that.d("rs") = rs		
		objCMS.show( "Admin/recycle" )
	end sub
	
	'列表
	sub list
		dim where,rs,obj,UserID,fields		
		set where = D_		
		where("a.UserID = b.UserID") = null
		where("a.LogStatus") = 1
		
		if POP_MVC.get("id") = "" then
			POP_MVC.get("id") = 10000
		end if
		
		dayDiff = that.get("id")
		
		if dayDiff <> "" and dayDiff <> "10000" then
			where("a.LogTime") = Array( "daydiff" , dayDiff )
		end if
		fields = "b.UserID as UserID,a.LogID,a.LogStatus,a.ZanCount,a.FileType,a.IsImage,a.ZanUser,a.UploadPath,a.OriginName,a.LogTime,a.FileSize,b.LoginName,b.Nickname,b.Avatar"
		set rs = B_( objCMS.baseTable ).from( "{prefix}self_UploadLog as a,{prefix}User as b" ).page("null,20").order("a.LogTime DESC,a.LogID DESC").field(fields).where(where).select
		
		that.d("rs") = rs		
		objCMS.show( "Admin/list" )
	end sub
	
	
	
	'未导入数据库上传文件检测
	sub dbTest
		dim action,time1,time2,filepath,arr,flag
		
		Call K_("UploadLog--Tools").dbTest
		
		response.end

		filepath = POP_MVC.appPath & "/runtime/UploadLog/table.txt"
		'如果已经保存了检测，且未再上传，从文件中显示
		if POP_MVC.file.isfile(filepath) then
			time1 = POP_MVC.file.mtime( POP_MVC.appPath & "/runtime/UploadLog/table.txt" )
			time2 = POP_MVC.file.mtime( POP_MVC.config("CMS_UPLOAD_PATH") )
			if  DateDiff("s", time2, time1) < 0 then
				flag = true
			else
				response.write K_("bbs--FileTool").file_get_test("UploadLog")
			end if
		end if
		
		if flag then
			Call K_("UploadLog--Tools").dbTest
		end if
	end sub
	
	'清空数据表
	sub clearDB
		Call B_( objCMS.baseTable ).truncate( tableName )	
		Call K_("bbs--Public").CtrlLog( 1 , "用户ID: " & S_("adminId") & " 清空了数据表" & tableName , Array("插件数据表清空完成" , "?uploadlog--admin#admin") )
	end sub
	
	'将上传导入数据库
	sub importDB
		dim filepath,content,dict,count
		
		filepath = POP_MVC.appPath & "/runtime/UploadLog/file.txt"
		count = 0
		if POP_MVC.file.isfile( filepath ) then
			content = POP_MVC.file_get_contents( filepath )

			
			if content <> "" then
				arr = split( content, "," )
				set dict = D_
				for i = 0 to ubound( arr )
					if POP_MVC.file.isFile( arr(i) ) then
						dict.removeAll
						dict("UploadPath") = arr(i)
						dict("UserID") = POP_MVC.String.reg_fetch( arr(i) , "\d+" , "" )
						dict("LogTime") = POP_MVC.file.mtime( arr(i) )
						dict("LogIP") = ""
						dict("OriginName") = "-1"
						dict("FileSize") = POP_MVC.file.fileSize( arr(i) )
						Call M_("UploadLog--UploadLog").db.data(dict).create(1)
						if M_("UploadLog--UploadLog").db.add > 0 then
							count = count + 1
						end if	
					end if
				next
			end if
			that.success( Array( "成功导入" &  count & "个文件" , "?uploadlog--admin#upload2db" ) )
		else
			that.error( "请先检测需要导入的文件" )
		end if
	end sub
	
	'冗余文件检测
	sub redundancyTest
		dim str
		str = K_("bbs--FileTool").redundancy
	end sub
	
	sub redundancyClear
		Call K_("bbs--FileTool").redundancyClear
		that.redirect("?System#redundancy")
	end sub
	
	sub emptyTest
		dim str
		str = K_("bbs--FileTool").emptyFolder
	end sub
	
	sub emptyClear
		Call K_("bbs--FileTool").emptyFolderClear
		that.redirect("?System#emptyFolder")
	end sub
	
	
	'配置
	sub settings
		dim mailType,data,html
		
		'取得配置
		set data = XM_("UploadLog--Configs").db.path("Configs/Config").select		
		html = P_("flyui4xml").PageConfigContent(data)		
		that.d("htmlstr") = html
		
		that.d("loopstr") = K_("bbs--FileTool").getTableStr("redundancy")
		
		that.d("emptystr") = K_("bbs--FileTool").getTableStr("emptyFolder")
		
		'取得图片
		filepath = POP_MVC.appPath & "/runtime/UploadLog/table.txt"
		if POP_MVC.file.isfile(filepath) then
			time1 = POP_MVC.file.mtime( filepath )
			time2 = POP_MVC.file.mtime( POP_MVC.config("CMS_UPLOAD_PATH") )
			if  DateDiff("s", time2, time1) > 0 then
				that.d("loopstr") = K_("bbs--FileTool").file_get_test("UploadLog")
			end if
		end if
		
		objCMS.show( "Admin/set" )
	end sub	
	
	'js自动填充
	sub jsAuto
		dim dict
		set dict = XM_("UploadLog--Configs").db.path("Configs/Config").field("ConfigName,ConfigValue").getKeyValue
		
		Call K_("bbs--Tools").js_Auto( dict )
	end sub
	
	'填充文件类型
	sub CompleteDB
		dim rs,dict
		set rs = B_(tableName).select
		
		set dict = D_
		do while not rs.eof
			dict( "FileType" ) = objCMS.getExtName( rs( "UploadPath" ) )
			
			if objCMS.testIsImage( rs( "UploadPath" ) ) then
				dict( "IsImage" ) = 1
			else
				dict( "IsImage" ) = 0
			end if

			Call B_(tableName).where( rs("LogID") ).setField( dict, null )
			rs.moveNext
		loop
		
		that.success( "填充完成" )
	end sub
	
	'修改配置值
	sub doEditConfigs
		dim key,dict,MailAlert
		set dict = POP_MVC.form2dict
		
		for each key in dict
			Call XM_("UploadLog--Configs").db.path("Configs/Config[ConfigName='"& key &"']").MergeField( "ConfigValue" , dict(key) )
		next

		Call K_( "bbs--Public" ).CtrlLog( 1, "用户ID: " & S_("adminId") & " ，修改了配置" , "保存成功" )
	end sub
	
	'上传图片
	sub uploads
		dim filename,logID
		POP_MVC.config("WATERMARK_PATH") = ""
		POP_MVC.config("UPLOAD_ALLOW_TYPES") = "jpg;jpeg;png;gif;bmp;pcx;svg"
		filename = POP_MVC.upload("file")
		if filename <> "" then
			that.success( filename )
		else
			that.error( POP_MVC.Uploader.description )
		end if
	end sub
	
	sub testASPJPEG
		if POP_MVC.IsInstall("Persits.Jpeg") then
			that.success( Array( "恭喜您，ASPJPEG组件已安装" ,"window.close",5 ) )
		else
			that.success( Array( "对不起，ASPJPEG组件未在服务器安装" ,"window.close",5 ) )
		end if
	end sub
End Class
%>