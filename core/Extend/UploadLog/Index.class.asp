<%
Class Index 'Extends bbs--Top
	private orderStr,statusField
	private tableName,sqlfile,extendName,bookConfig
	
	Sub initialize
		tableName = "self_UploadLog"
		extendName = "UploadLog"		
		
		set bookConfig = XM_(  extendName & "--Configs" ).db.path("Configs/Config").field("ConfigName,ConfigValue").getObject
		that.d("bookConfig") = bookConfig
	
		objCMS.cmsPrefix = "aspbbs"
		guestConfig.DefaultTemplate = extendName
		objCMS.htmlFilePath = "html"
		isWap = checkWap()		
		sqlfile = POP_MVC.config("CORE_NAME") & "/Extend/" & extendName & "/" & C_("DB_TYPE") & ".sql"		
	End Sub
	
	sub Index
		dim where,rs,obj,UserID,fields		
		set where = D_		
		where("a.UserID = b.UserID") = null
		where("a.LogStatus") = 1
		fields = "b.UserID as UserID,a.LogID,a.ZanCount,a.ZanUser,a.UploadPath,a.OriginName,a.LogTime,a.FileSize,a.DownCount,a.FileType,a.IsImage,b.LoginName,b.Nickname,b.Avatar"
		set rs = B_( objCMS.baseTable ).from( "{prefix}self_UploadLog as a,{prefix}User as b" ).page("null,20").order("a.LogTime DESC,a.LogID DESC").field(fields).where(where).select		
		that.d("rs") = rs		
		objCMS.show( "Index/index" )
	end sub
	
	
	sub Downs
		if S_("IsAdmin") <> 1 then
			that.error( "您无权查看" )
		end if
	
		dim where,rs,obj,UserID,fields		
		set where = D_		
		where("a.UserID = b.UserID") = null
		where("a.LogStatus") = 1
		where("a.IsImage") = 0
		fields = "b.UserID as UserID,a.LogID,a.ZanCount,a.ZanUser,a.UploadPath,a.OriginName,a.LogTime,a.FileSize,a.DownCount,a.FileType,a.IsImage,b.LoginName,b.Nickname,b.Avatar"
		set rs = B_( objCMS.baseTable ).from( "{prefix}self_UploadLog as a,{prefix}User as b" ).page("null,20").order("a.LogTime DESC,a.LogID DESC").field(fields).where(where).select		
		that.d("rs") = rs		
		objCMS.show( "Index/download" )
	end sub
	
	sub User
		dim where,rs,obj,UserID,fields
		UserID = that.get("id")
		set where = D_		
		where("UserID") = UserID
		
		set userRS = B_( objCMS.baseTable ).from( "{prefix}User" ).where(where).find		
		
		where("LogStatus") = 1
		where.remove("UserID")
		where("b.UserID") = UserID
		fields = "b.UserID as UserID,a.LogID,a.ZanCount,a.ZanUser,a.UploadPath,a.OriginName,a.LogTime,a.FileSize,a.DownCount,a.FileType,a.IsImage,b.LoginName,b.Nickname,b.Avatar"
		set rs = B_( objCMS.baseTable ).from( "{prefix}self_UploadLog as a,{prefix}User as b" ).page("null,20").order("a.LogTime DESC,a.LogID DESC").field(fields).where(where).select			
				
		that.d("rs") = rs	
		that.d("user") = userRS
		objCMS.show( "Index/user" )
	end sub
	
	'下载展示页面	
	sub down
		'如果有下载页面，可以先展示，后下载
		Call download
	end sub
	
	'下载操作
	sub download
		dim rs,id
		id = that.get("id")
		set rs = B_( tableName ).where(id).find
		
		if rs.eof then
			that.error( "未找到文件" )
		else
			Call B_( tableName ).where(id).setInc( "DownCount" )
			POP_MVC.download( Array( rs("UploadPath") , rs("OriginName") ) )
		end if
	end sub
End Class
%>