<%
Class [Public]

	'上传日志
	'OriginName 原文件名, UploadPath 上传路径, FileSize 文件大小(B), UploadTool 上传所用工具, UploadStatus 上传状态,ErrDesc 错误描述
	'UploadTool分别为:ueditor upfile.asp upload.asp
	'UploadStatus: 1上传成功，0上传失败
	'ErrDesc: 错误描述，上传成功为空，上传失败则为失败原因
	'返回数据表中的新增id
	Function upload( OriginName, UploadPath , FileSize, UploadTool , UploadStatus ,ErrDesc )	
		dim bookConfig
		'如果上传路径为空，而UploadStatus=1，则定为上传失败
		if isNul( UploadPath ) and UploadStatus <> 1 then
			UploadStatus = 0
			if isNul( ErrDesc ) then
				ErrDesc = "未知错误"
			end if
		end if
		
		set bookConfig = XM_(  "UploadLog--Configs" ).db.path("Configs/Config").field("ConfigName,ConfigValue").getObject
		if bookConfig.waterMark = 1 then
			POP_MVC.config("WATERMARK_WIDTH") = bookConfig.markPicWidth
			POP_MVC.config("WATERMARK_HEIGHT") = bookConfig.markPicHeight
			POP_MVC.config("WATERMARK_POSITION") = bookConfig.waterMarkLocation
			if bookConfig.waterType = 1 then
				POP_MVC.config("WATERMARK_PATH") = bookConfig.waterMarkPic				
			else
				POP_MVC.config("WATERMARK_PATH") = bookConfig.waterMarkFont
			end if
			if POP_MVC.IsInstall("Persits.Jpeg") then
				Call POP_MVC.WaterMark( UploadPath )
			end if
		end if
		
		upload = M_("UploadLog--UploadLog").Log( OriginName, UploadPath , FileSize, UploadTool , UploadStatus ,ErrDesc )
	end Function
	
	'置入回收站
	Function recycle( UploadPath )
		dim rs,fields,DelPath,theDir
		fields = "UploadPath"
		
		'将文件转移至回收站
		DelPath =  "#recycle" & objCMS.SubStrUserUploadFolder( UploadPath )	
		
		theDir = POP_MVC.file.dir( DelPath )
		Call POP_MVC.CreateFolder( theDir )
		
		'目标存在则删除
		if POP_MVC.file.isFile( DelPath ) then
			Call POP_MVC.file.remove( DelPath )
		end if
		
		Call POP_MVC.file.rename( UploadPath , DelPath )		
		
		'在数据表中查找，查到后将状态改为2
		set rs = B_( "Self_UploadLog" ).field("LogID").top(1).search( UploadPath,fields)
		if not rs.eof then			
			set dict = D_			
			dict("DelPath") = DelPath
			dict("DelTime") = now()
			dict("LogStatus") = 2
			Call B_( "Self_UploadLog" ).where( rs("LogID") ).setField( dict,null )
		end if
		rs.close : set rs = nothing
		set dict = nothing
	End Function
End Class
%>