<%
Class Tools
	
	Function dbTest()

		dbTest = K_("bbs--FileTool").TestFiles( "UploadLog--Tools" , "need2db" , "" , "UploadLog" ,  "上传文件入库" )
	End Function	
	
	'是否为需入库文件
	Function need2db( file )
		on error resume next
		dim size,filetype,extname
		dim fields,rs
		
		if file.name = "Thumbs.db" then
			exit Function
		end if		
		
		filetype=POP_MVC.file.getFileType(file.path)
		path = mid( file.path,len(POP_MVC.realpath("./"))+2  )

		path = replace(path,"\" , "/")
		extname = LCase(Mid(file.name, InStrRev(file.name, ".") + 1))
		
		fields = "UploadPath"
		set rs = B_( "Self_UploadLog" ).field("LogID").top(1).search( path,fields)

		if not rs.eof then
			rs.close : set rs = nothing : exit Function
		end if
		
		size = byte2size(file.size)	

		Call K_("bbs--FileTool").setTestStr( file , path , fileType,  extname , extname  )
	End Function

End Class
%>