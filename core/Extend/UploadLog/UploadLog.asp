<%
Class UploadLog
	Sub init
		db.tableName = "Self_UploadLog"
		db.prikey = "LogID"
		db.validate_ = Array( )
		
		db.auto_ = Array( _
			Array( "LogTime" ,"now",  1 , "function" ) _
			,Array( "ErrDesc" ,"",  1  ) _
			,Array( "IpAddr" ,"",  1  ) _
			,Array( "LogStatus" ,1,  1  ) _
			,Array( "DelPath" ,"",  1  ) _
			,Array( "UserID" , Session("adminId") ,  1  ) _
			,Array( "IsImage" , 1,  1  ) _
			,Array( "LogIP" ,"get_client_ip",  1 , "function" ) _
			,Array( "DownCount" ,0,  1 ) _
		)
	end sub
	
	'将上传信息记录于表
	Function Log( OriginName, ByVal UploadPath , FileSize, UploadTool , UploadStatus ,ErrDesc )
		dim str,id,dict,extname
		
		set dict = D_
		dict("OriginName") = OriginName
		dict("UploadPath") = UploadPath
		dict("FileSize") = FileSize
		dict("UploadTool") = UploadTool
		dict("UploadStatus") = UploadStatus		
		dict("ErrDesc") = ErrDesc
		dict("FileType") = objCMS.getExtName( UploadPath )
		dict("IsImage") = testIsImage( UploadPath )

		Call db.data(dict).create(1)
		Log = db.Add
	End Function
	
	Function testIsImage( filepath )
		if objCMS.testIsImage( filepath ) then
			testIsImage = 1
		else
			testIsImage = 0
		end if
	End Function
End Class
%>