<%
Class User 'Extends bbs--Common
	Public tableName,statusField
	Sub initialize
		tableName = "Self_UploadLog"
		objCMS.cmsPrefix = "aspbbs"
		guestConfig.DefaultTemplate = "UploadLog"
		objCMS.htmlFilePath = "html"	
	End Sub
	
	sub index
		dim where,rs,obj,UserID
		
		set where = D_
		
		UserID = that.get("id")
		that.d("user") = objCMS.getUserData(UserID)
		where("UserID") = UserID
		where("LogStatus") = 1
		set rs = B_(tableName).where(where).page("null,20").select
		
		that.d("rs") = rs
		
		set obj = objCMS
		templatePath= obj.getTemplatePath( "User/index" )
		obj.load(templatePath)	
		obj.parseHtml	
		objCMS.output
	end sub
	
	'对案例点赞
	sub doZan
		'{"status":0,"msg":"","praise":9}
		dim dict,rs,where,id,sTip,ZanUser,UserID
		
		id = that.req("id")
		UserID = S_("adminId")
		
		'得到回岾ID
		set where = D_
		where("LogID") = id
		where("a.UserID = b.UserID") = null
		
		'不能对自己点赞
		set rs = B_(objCMS.baseTable).from("{prefix}self_UploadLog as a,{prefix}User as b").where( where ).field("b.LoginName,b.Nickname,b.UserID,ZanUser").find		
		if rs("UserID") - S_("adminId") = 0 and S_("IsAdmin") - 1 <> 0 then
			that.u(rs)
			
			sTip = "对不起，不能对自己点赞哦！"
			Call K_("bbs--Public").CtrlLog( 0 , "用户ID: " & S_("adminId") & " 对帖子ID:" & id & " " & sTip , Array( sTip ,-1 ) )
		end if
		
		ZanUser = rs("ZanUser")
		
		'存在时还得判断 TopicZan 是否包含了id
		if IsNul( ZanUser ) then
			Call B_(tableName).where( id ).setField( "ZanUser" , UserID )		
		elseif inStr( "," & ZanUser & "," , "," & UserID & "," ) > 0 then
			that.u(rs)
			sTip =  "已经赞过了"
			Call K_("bbs--Public").CtrlLog( 0 , "用户ID: " & S_("adminId") & " 对帖子ID:" & id & " " & sTip , Array( "您" & sTip ,-1 ) )
		else
			Call B_(tableName).where( id ).setField( "ZanUser" , ZanUser & ","  & UserID )
		end if

		that.u(rs).u(dict)
		B_(tableName).where(id).setInc("ZanCount")
		
		set dict = D_
		dict("status") = that.ajaxSuccessStatus
		dict("msg") = ""
		dict("praise") = B_(tableName).where(id).field("ZanCount").getOne
		that.ajaxData(dict)
	end sub	
	
	'通过ajax返回点赞的用户
	sub listZan
		dim id,dict,where,rs,arr,LogID,ZanUser
		
		'案例ID
		LogID = that.req("id")
		
		'取标题与用户ID
		set rs = B_( tableName ).where(LogID).field( "LogID,ZanUser,OriginName" ).find
		
		'案例不存在时
		if rs.eof then
			that.u(rs)
			that.error( Array("访问的文件不存在") )
		end if	
	
		ZanUser = rs("ZanUser").value
		
		set dict = D_
		dict("title") = rs("OriginName")
		if not isNul(ZanUser) then
			set where = D_
			where("UserID") = Array( "in" , split(ZanUser , ",") )
			set rs = B_( objCMS.baseTable ).from("{prefix}User").field("UserID as id, Nickname as username, avatar").where( where ).page("1,500").select
			dict.add "data",POP_MVC.rs2dict(rs)
		else
			dict.add "data",D_
		end if	
		dict("status") = that.ajaxSuccessStatus	
		that.u(rs)
		that.ajaxData( dict )
	end sub
End Class
%>