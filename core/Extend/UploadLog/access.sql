CREATE TABLE [iAspCms_Self_UploadLog] ( 
	[LogID] Integer  NOT NULL IDENTITY,
	[UserID] Integer ,
	[OriginName] VarChar ,
	[UploadPath] VarChar ,
	[LogTime] DateTime ,
	[FileSize] Integer ,
	[LogIP] VarChar ,
	[IpAddr] VarChar ,
	[UploadStatus] Bit  NOT NULL,
	[ErrDesc] VarChar ,
	[DownCount] Integer ,
	[ZanCount] SmallInt ,
	[CaiCount] SmallInt ,
	[ZanUser] Text ,
	[CaiUser] Text ,
	[LogStatus] TinyInt ,
	[DelPath] VarChar ,
	[DelTime] DateTime ,
	[IsImage] TinyInt ,
	[FileType] VarChar ,
	PRIMARY KEY ([LogID])
);