<%

private TimeDict

set TimeDict = D_
TimeDict(10000) = "全部"
TimeDict(1) = "1天"
TimeDict(2) = "2天"
TimeDict(3) = "3天"
TimeDict(7) = "1周内"
TimeDict(14) = "2周内"
TimeDict(30) = "1月内"
that.d("TimeDict") = TimeDict
that.d("ExtendName") = "UploadLog"

'案例审核页面链接
Function getCaseAuditLink( theYear, stype )
	getCaseAuditLink = objCMS.linkPrefix & "cases--Admin_audit" & stype & "_" & theYear & guestConfig.PageSuffix
End Function

'获取分页头
Function getLogPageHeader()
	if POP_MVC.a = "index" then
		getLogPageHeader = objCMS.linkPrefix & POP_MVC.c
	else
		getLogPageHeader = objCMS.linkPrefix & POP_MVC.c & "_" & POP_MVC.a
	end if
End Function

%>