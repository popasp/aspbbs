CREATE TABLE [iAspCms_self_UploadLog] ( 
	[LogID] Integer  NOT NULL IDENTITY,
	[UserID] Integer ,
	[OriginName] VarChar ,
	[UploadPath] VarChar ,
	[LogTime] DateTime ,
	[FileSize] Integer ,
	[LogIP] VarChar ,
	[UploadStatus] Bit  NOT NULL,
	[ErrDesc] VarChar ,
	PRIMARY KEY ([LogID])
);