<%
Class [Public]
	public bookConfig

	Private Sub Class_Initialize()
		set bookConfig = XM_( "Adv--Thread" ).db.path("Configs/Config").field("ConfigName,ConfigValue").getObject
	End Sub
	
	Private Sub Class_Terminate()
		set bookConfig = nothing
	End Sub
	
	Function getConfig( ConfigName )
		Execute "getConfig = bookConfig." & ConfigName
	End Function

	sub Assign( dataName,Alias )
		that.d( dataName ) = XM_("Adv--Thread").db.path("Blogs/Blog[Alias='" & Alias & "']").field("Title,Image,Url").select
	end sub
End Class
%>