<%
Class Sort
	Private index4sort_ 
	sub init
		'db是popasp_xmldb类的实例化
		path = "Sorts"
		db.db_path = "./Templates/Adv/#data/#book#.xml"
		
		db.auto_ = Array( _
			Array( "SortStatus" ,1,  1 ) _
			,Array( "SortID" ,bookConfig.index4sort ,  1 , "function") _
			,Array( "AddTime" ,"POP_MVC.FormatDate( now() , ""YYYY-MM-DD HH:II:SS"" )",  1 , "function" ) _
			,Array( "EditTime" ,"POP_MVC.FormatDate( now() , ""YYYY-MM-DD HH:II:SS"" )",  2 , "function" ) _
			,Array( "IP" , "get_client_ip" ,  1, "function" ) _
		)
		
		'数据验证
		db.validate_ = Array( _
			 Array( "SortName" ,"",  "标题不能为空"  , 0, "notempty" , 3  ) _
			,Array( "SortName","1,255","标题长度不能超过255个字符" , 0 , "length" , 3 ) _
			,Array( "Alias","1,255","标题长度不能超过255个字符" , 0 , "length" , 3 ) _	
		)
	end sub
	
	Function incBlogID
		incBlogID = index4sort + 1
	End Function

	
	Public Property Get index4sort()
		if index4sort_ = "" then
			index4sort_ = bookConfig.index4sort
		end if
		index4sort = index4sort_	
	End Property
	
	Public Property Let index4sort( value )
		index4sort_ = value
		Call setSiteConfig( "index4sort" , index4sort_ )
	End Property

	
	Sub setSiteConfig( ConfigName, ConfigValue )
		Call db.path( "Configs/Config[ConfigName='" & ConfigName & "']"  ).setField( "ConfigValue" , ConfigValue )
	End Sub
	
	Function getSortPath( fileName )
		getSortPath = getSortDir() & filename
	End Function
	
	'获取文章内容
	Function getContent( filename )
		getContent = POP_MVC.file_get_contents( getSortPath( fileName ) )
	End Function
	
	Function getSortDir( )
		getSortDir =  "./Templates/Adv/#data/s/"
	End Function
End Class
%>