<%
Class Common
	public son
	sub initialize
		on error resume next
		dim bool,adminId,where,rs
		bool = false
	
		if session("adminId") = "" then
			'先尝试用Cookie登陆
			if guestConfig.LoginUseCookie - 0 <> 0 then
				Call A_("bbs--Login/CookieLogin")
			end if
			if session("adminId") = "" then
				if that.isAjax then
					that.error( Array( "您尚未登陆" , getLoginUrl() ) )
				else
					Response.redirect(getLoginUrl())		
				end if		
			end if
		end if	
		
		'删除旧后台路径
		if session( "MoveAdminPathFrom" ) <> "" then
			POP_MVC.file.remove( session( "MoveAdminPathFrom" ) )
			if NOT POP_MVC.file.isFolder( session( "MoveAdminPathFrom" ) ) then
				session( "MoveAdminPathFrom" ) = ""
			end if
		end if
		
		err.clear
		that.success_wait = 1
		that.error_wait = 3
	end sub

	'启用
	Sub openStatus
		if not that.isSelfOrigin then
			that.error("非法操作")
		end if	
	
		on error resume next
		dim statusField
		dim id
		id = check_id
		statusField = Son.statusField
		if err.number <> 0 then
			statusField = son.tableName & "Status"
		end if
		if B_(son.tableName).where( id ).setField(statusField,1) then
			that.redirect("")
		end if
	End Sub

	'禁用
	Sub closeStatus
		if not that.isSelfOrigin then
			that.error("非法操作")
		end if	
	
		on error resume next
		dim statusField
		dim id
		id = check_id
		statusField = Son.statusField
		if err.number <> 0 then
			statusField = son.tableName & "Status"
		end if
		if B_(son.tableName).where( id ).setField(statusField,0) then
			that.redirect("")
		end if
	End Sub	
	
	'切换状态
	Sub switchStatus
		on error resume next		
		dim id
		id = check_id
		
		dim statusField
		statusField = Son.statusField		
		
		if err.number <> 0 then
			statusField = son.tableName & "Status"
		end if
		
		if B_(son.tableName).where( id ).setNot(statusField) then
			that.redirect("")
		end if
	End Sub	
	
	'切换字段的值
	Sub switchField
		if not that.isSelfOrigin then
			that.error("非法操作")
		end if	
	
		if B_(son.tableName).where( that.get("id") ).setNot( that.get("field") ) then
			that.redirect("")
		end if
	End Sub	
	
	'设置字段的值
	Sub setField
		if not that.isSelfOrigin then
			that.error("非法操作")
		end if	
	
		if B_(son.tableName).where( POP_MVC.req("id") ).setField( that.get("field") , POP_MVC.req( that.get("field") ) ) then
			that.redirect("")
		end if
	End Sub	
	
	Sub switchUniqstatus
		if not that.isSelfOrigin then
			that.error("非法操作")
		end if
		
		Call B_(son.tableName).where( "1=1" ).setField( that.get("field") ,0)
		
		if B_( son.tableName ).where( POP_MVC.req("id") ).setField(that.get("field"),1) then
			that.redirect("")
		end if
	End Sub	

	
	'复制
	sub doCopy
		dim id
		id = check_id	
		if B_(Son.tableName).where( id ).copy then
			that.success( "复制成功")
		end if
	end sub
	
	sub Empty_
		K_("bbs--Public").showError( "您访问的页面不存在！" )
		response.end
	end sub
End Class
%>