<%
Class Digg 'Extends bbs--Common
	private tableName,baseTable
	sub initialize
		tableName = "self_GuestDigg"
		baseTable = objCMS.baseTable
	end sub
	
	'对回帖点赞
	sub doZanReply
		'{"id":"1","ok":"false"}
		dim dict,rs,where,id,sTip
		
		'得到回岾ID
		if POP_MVC.isPost then
			id = that.req("cid")		
		else
			id = that.req("id")		
		end if
		
		if not that.isID( id ) then
			that.error( "非法操作" )
		end if
		
		'不能对自己点赞
		set rs = B_( baseTable ).from( "{prefix}self_GuestReply" ).where( that.dict( "ReplyID" , id )  ).field("UserID").find	
		if rs("UserID") - S_("adminId") = 0 then
			that.u(rs)
			sTip = "对不起，不能对自己点赞哦！"
			Call K_("bbs--Public").CtrlLog( 0 , "用户ID: " & S_("adminId") & " 对回复ID:" & id & " " & sTip , Array( sTip ,-1 ) )
		end if
		that.u(rs)
		
		'查询条件
		set where = D_
		where("UserID") = S_("adminId")
		
		'根据UserID取得RS
		set rs = B_( baseTable ).from( "{prefix}" & tableName ).where(where).field("UserID,ReplyZan").find
		
		'不存在时需要添加
		if rs.eof then
			that.u(rs)
			set dict = D_
			dict("ReplyZan") = id 

			if K_("bbs--Public").ModelCreate("bbs--Digg",dict,1) then
				Call K_("bbs--Public").ModelAdd( "bbs--Digg" , null )
			else
				sTip =  M_("bbs--Digg").db.error
				Call K_("bbs--Public").CtrlLog( 0 , "用户ID: " & S_("adminId") & " 对回复ID:" & id & " " & sTip , Array( sTip ,-1 ) )
			end if
		else
			'存在时还得判断ReplyZan是否包含了id
			if inStr( "," & rs("ReplyZan") & "," , "," & id & "," ) > 0 then
				that.u(rs)
				
				sTip =  "已经赞过了"
				Call K_("bbs--Public").CtrlLog( 0 , "用户ID: " & S_("adminId") & " 对回复ID:" & id & " " & sTip , Array( "您" & sTip ,-1 ) )
			else
				Call B_( tableName ).where( where ).setField( "ReplyZan" , rs("ReplyZan") & ","  & id )
			end if
		end if
		
		B_( "self_GuestReply" ).where( id ).setInc("ZanCount")
		
		that.u(rs).u(dict).u(where)
		
		sTip =  "点赞成功"
		Call K_("bbs--Public").CtrlLog( 1 , "用户ID: " & S_("adminId") & " 对回复ID:" & id & " " & sTip ,  Array( sTip ,-1) )
	end sub
	
	
	'对帖子点赞
	sub doZanTopic
		'{"status":0,"msg":"","praise":9}
		dim dict,rs,where,id,sTip
		
		'得到回岾ID
		if POP_MVC.isPost then
			id = that.req("cid")		
		else
			id = that.req("id")		
		end if
		
		if not that.isID( id ) then
			that.error( "非法操作" )
		end if
		
		'得到回岾ID
		set where = D_
		where("TopicID") = id
		where("a.UserID = b.UserID") = null
		
		'不能对自己点赞
		set rs = B_(baseTable).from("{prefix}self_GuestTopic as a,{prefix}User as b").where( where ).field("b.LoginName,b.Nickname,b.UserID").find		
		if rs("UserID") - S_("adminId") = 0 and S_("IsAdmin") - 1 <> 0 then
			that.u(rs)
			
			sTip = "对不起，不能对自己点赞哦！"
			Call K_("bbs--Public").CtrlLog( 0 , "用户ID: " & S_("adminId") & " 对帖子ID:" & id & " " & sTip , Array( sTip ,-1 ) )
		end if
		
		'查询条件
		set where = D_
		where("UserID") = S_("adminId")
		
		'根据UserID取得RS
		set rs = B_( baseTable ).from( "{prefix}" & tableName ).where(where).field("UserID,TopicZan").find
		
		'不存在时需要添加
		if rs.eof then
			that.u(rs)
			set dict = D_
			dict("TopicZan") = id 
			if K_("bbs--Public").ModelCreate("bbs--Digg",dict,1) then
				Call K_("bbs--Public").ModelAdd( "bbs--Digg" , null )
			else				
				sTip = M_("bbs--Digg").db.error
				Call K_("bbs--Public").CtrlLog( 0 , "用户ID: " & S_("adminId") & " 对帖子ID:" & id & " 点赞时， " & sTip , Array( sTip , -1 ) )
			end if
		else
			'存在时还得判断 TopicZan 是否包含了id
			if inStr( "," & rs("TopicZan") & "," , "," & id & "," ) > 0 then
				that.u(rs)
				sTip =  "已经赞过了"
				Call K_("bbs--Public").CtrlLog( 0 , "用户ID: " & S_("adminId") & " 对帖子ID:" & id & " " & sTip , Array( "您" & sTip ,-1 ) )
			else
				Call B_( tableName ).where( where ).setField( "TopicZan" , rs("TopicZan") & ","  & id )
			end if
		end if
		that.u(rs).u(dict)
		B_( "self_GuestTopic").where( id ).setInc("ZanCount")
		
		set dict = D_
		dict("status") = that.ajaxSuccessStatus
		dict("msg") = ""
		dict("praise") = B_(baseTable).from("{prefix}self_GuestTopic").where( that.dict( "TopicID" , id ) ).field("ZanCount").getOne
		that.ajaxData(dict)
	end sub	
	

	
	private sub findStatus( fieldName )
		dim id,dict,where,data,rs
		
		'得到ID
		
		if POP_MVC.isPost then
			id = that.req("cid")		
		else
			id = that.req("id")		
		end if
		
		if not that.isID( id ) then
			that.error( "非法操作" )
		end if
		
		set where = D_
		where("UserID") = S_("adminId")
		where("inStr( ',' & " & fieldName & " & ',' , '," & id & ",')") = null
		
		set rs = B_( baseTable ).from( "{prefix}" & tableName ).where(where).find	
		
		set dict = D_
		dict("status") = that.ajaxSuccessStatus	
		set data = D_
		
		'如果收藏了
		if not rs.eof then
			data("value") = 1
		end if
		
		that.u(rs)
		
		dict.add "data",data
		that.ajaxData( dict )
	End Sub
	
	'ajax某个帖子ID是否收藏
	sub findCollection
		Call findStatus( "Collection" )
	end sub
	
	'ajax某个帖子ID是否点赞
	sub findTopicZan
		Call findStatus( "TopicZan" )
	end sub
	
	'ajax某个帖子ID是否收藏
	sub findDigg
		dim id,dict,where,data,rs
		
		'得到ID
		
		if POP_MVC.isPost then
			id = that.req("cid")		
		else
			id = that.req("id")		
		end if
		
		if not that.isID( id ) then
			that.error( "非法操作" )
		end if
		
		set where = D_
		where("UserID") = S_("adminId")
		set rs = B_( baseTable ).from( "{prefix}" & tableName ).where(where).find	
		
		set dict = D_
		dict("status") = that.ajaxSuccessStatus	
		set data = D_
		
		'如果收藏了
		if not rs.eof then
			if inStr( "," & rs("Collection") & "," , "," & id & "," ) > 0 then
				data("Collection") = 1
			end if
			if inStr( "," & rs("TopicZan") & "," , "," & id & "," ) > 0 then
				data("TopicZan") = 1
			end if
			if inStr( "," & rs("TopicCai") & "," , "," & id & "," ) > 0 then
				data("TopicCai") = 1
			end if
		end if
		
		data("praise") = B_(baseTable).from("{prefix}self_GuestTopic").where( that.dict( "TopicID" , id ) ).field("ZanCount").getOne
		
		that.u(rs)
		
		dict.add "data",data
		that.ajaxData( dict )
	end sub
	
	'ajax操作，根据ID收藏帖子
	sub addCollection
		dim id,where,rs,dict,sTip
		
		'得到ID
		id = that.req("cid")				
		
		set where = D_
		where("UserID") = S_("adminId")
		
		set rs = B_( baseTable ).from( "{prefix}" & tableName ).where(where).find		
		
		'根据UserID取得RS
		set rs = B_( baseTable ).from( "{prefix}" & tableName ).where(where).field("UserID,Collection").find
		
		'不存在时需要添加
		if rs.eof then
			that.u(rs)
			set dict = D_
			dict("Collection") = id
				
			if K_("bbs--Public").ModelCreate("bbs--Digg",dict,1) then
				Call K_("bbs--Public").ModelAdd( "bbs--Digg" , null )
			else				
				sTip = M_("bbs--Digg").db.error
				Call K_("bbs--Public").CtrlLog( 0 , "用户ID: " & S_("adminId") & " 对帖子ID:" & id & " 收藏时， " & sTip , Array( sTip ,-1 ) )
			end if
		else
			'存在时还得判断 Collection 是否包含了id
			if inStr( "," & rs("Collection") , "," & id & "," ) > 0 then
				that.u(rs)
				
				sTip = "已经收藏了"
				Call K_("bbs--Public").CtrlLog( 0 , "用户ID: " & S_("adminId") & " 对帖子ID:" & id & " 收藏时， " & sTip , Array( "您" & sTip ,-1 ) )
			else
				Call B_( tableName ).where( where ).setField( "Collection" , rs("Collection")  & "," & id )
			end if
		end if
		
		that.u(rs)	
		sTip = "收藏成功"
		Call K_("bbs--Public").CtrlLog( 1 , "用户ID: " & S_("adminId") & " 对帖子ID:" & id & " " & sTip , sTip )
	end sub	
	
	'ajax操作，根据ID取消收藏帖子
	sub removeCollection
		dim id,where,rs,tempStr
		
		'得到ID
		if POP_MVC.isPost then
			id = that.req("cid")		
		else
			id = that.req("id")		
		end if	

		if not that.isID( id ) then
			that.error( "非法操作" )
		end if		
		
		set where = D_
		where("UserID") = S_("adminId")
		
		'根据UserID取得RS
		set rs = B_( baseTable ).from( "{prefix}" & tableName ).where(where).field("UserID,Collection").find
		
		sTip = "没有收藏"
		
		'不存在时需要添加
		if rs.eof then
			that.u(rs)
			Call K_("bbs--Public").CtrlLog( 1 , "用户ID: " & S_("adminId") & " 对帖子ID:" & id & " 取消收藏时，" & sTip , sTip)
		else
			'存在时还得判断 Collection 是否包含了id
			if inStr( "," & rs("Collection") & "," , "," & id & "," ) < 1 then
				that.u(rs)
				Call K_("bbs--Public").CtrlLog( 1 , "用户ID: " & S_("adminId") & " 对帖子ID:" & id & " 取消收藏时，" & sTip , sTip)
			else
				tempStr = "," & rs("Collection") & ","
				tempStr = replace( tempStr, "," & id & "," , "" )
				tempStr = POP_MVC.String.trim( tempStr , "," )
				Call B_( tableName ).where( where ).setField( "Collection" , tempStr )
			end if
		end if
		
		that.u(rs)		
		
		sTip = "取消收藏成功"
		Call K_("bbs--Public").CtrlLog( 1 , "用户ID: " & S_("adminId") & " 对帖子ID:" & id & " " & sTip , sTip )
	end sub
	
	'通过ajax返回点赞的用户
	sub listTopicZan
		dim id,dict,where,rs,arr,TopicID
		
		'案例ID
		TopicID = that.req("id")
	
		'取标题与用户ID
		set rs = B_(baseTable).from("{prefix}self_GuestTopic").where( that.dict( "TopicID" , TopicID ) ).field("UserID,Title").find
		
		'案例不存在时
		if rs.eof then
			that.u(rs)
			that.error( Array("访问的贴子不存在") )
		end if	
		
		'构造ajax返回对象
		set dict = D_
		dict("title") = rs("Title")
		
		'得到用户ID	
		id = rs( "UserID" )
		
		'销毁对象
		that.u(rs)
		
		set where = D_
		where("inStr( TopicZan & ',' , '," & TopicID & ",')") = null		
		arr = B_( baseTable ).from( "{prefix}" & tableName ).where(where).field("UserID").page("1,500").get1arr
		

		
		where.RemoveAll
		if ubound(arr) > -1 then
			where("UserID") = Array( "in" , arr )
		else
			where("1=2") = null
		end if
		
		set rs = B_( objCMS.baseTable ).from("{prefix}User").field("UserID as id, Nickname as username, avatar").where( where ).page("1,500").select
		
		dict("status") = that.ajaxSuccessStatus	
		dict.add "data",POP_MVC.rs2dict(rs)
		that.u(rs)
		that.ajaxData( dict )
	end sub
	
	'通过ajax返回点赞的用户
	sub listReplyZan
		dim id,dict,where,rs,arr,ReplyID
		
		'案例ID
		ReplyID = that.req("id")
		
		'取标题与用户ID
		set rs = B_( baseTable ).from( "{prefix}self_GuestReply" ).where( that.dict( "ReplyID" , ReplyID )  ).field("UserID,TopicID").find
		
		'案例不存在时
		if rs.eof then
			that.u(rs)
			that.error( Array("访问的回贴不存在") )
		end if	
		
		'构造ajax返回对象
		set dict = D_
		
		
		'得到用户ID	
		id = rs( "UserID" )
		
		'销毁对象
		that.u(rs)
		
		set where = D_
		where("inStr( ReplyZan & ',' , '," & ReplyID & ",')") = null		
		arr = B_( baseTable ).from( "{prefix}" & tableName ).where(where).field("UserID").page("1,500").get1arr
		
		where.RemoveAll
		if ubound(arr) > -1 then
			where("UserID") = Array( "in" , arr )
		else
			where("1=2") = null
		end if
		
		set rs = B_( objCMS.baseTable ).from("{prefix}User").field("UserID as id, Nickname as username, avatar").where( where ).page("1,500").select
		
		dict("status") = that.ajaxSuccessStatus	
		dict.add "data",POP_MVC.rs2dict(rs)
		that.u(rs)
		that.ajaxData( dict )
	end sub

end Class
%>