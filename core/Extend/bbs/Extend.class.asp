<%
Class Extend  'Extends bbs--Top
	private tableName
	sub initialize
		on error resume next
		dim bool,adminId,where,rs
		bool = false
		guestConfig.LoginCookieCheckIP = 0
		if session("adminId") = "" then
			'先尝试用Cookie登陆
			if guestConfig.LoginUseCookie - 0 <> 0 then
				Call A_("bbs--Login/CookieLogin")
			end if
		end if	
	end sub
	
	'头部导航
	sub nav
		dim obj,templatePath,arr
		set obj = objCMS

		if S_("adminId") = "" then
			templatePath= obj.getTemplatePath( "Extend/login-nav" )
		else
			templatePath= obj.getTemplatePath( "Extend/user-nav" )
		end if
		obj.load(templatePath)	
		obj.parseHtml
		arr = split( obj.Content, vbcrlf )
		for i = 0 to ubound(arr)
			arr(i) = "document.write(""" & replace(arr(i) ,"""", "\""" ) & """)" & vbcrlf
			response.write arr(i)
		next
	end sub
	
	sub head
		dim obj,templatePath,arr
		set obj = objCMS
		if S_("adminId") = "" then
			templatePath= obj.getTemplatePath( "Extend/head-nav" )
		end if
		obj.load(templatePath)	
		obj.parseHtml
		arr = split( obj.Content, vbcrlf )
		for i = 0 to ubound(arr)
			arr(i) = "document.write(""" & replace(arr(i) ,"""", "\""" ) & """)" & vbcrlf
			response.write arr(i)
		next
	end sub
end Class
%>