<%
class Faq
	sub init
		'表名 
		'db是popasp_access类的实例化
		db.tableName = "self_GuestBook"
		
		'主键
		db.prikey = "FaqID"
		
		db.auto_ = Array( _			
			Array( "Contact" ,"bbs--Thread.stripTags" , 1 , "callback" , array() )_
			,Array( "ContentStatus" ,0,  1 ) _
			,Array( "ParentID" ,0,  1 ) _
			,Array( "userID" , iif( S_("adminId") = "" or POP_MVC.form("HideUser") = "" , 0 , S_("adminId") ) ,  1 ) _
			,Array( "Avatar" , 0 ,  1 ) _
			,Array( "AddTime" ,"now",  1 , "function"  ) _
			,Array( "FloorStep" ,"bbs--Faq.getFloorStep" , 1 , "callback" )_
			,Array( "MailAlert" , "POP_MVC.String.uniqid" , 1 , "function"  ) _
			,Array( "IP" ,"get_client_ip",  1 , "function" ) _
			,Array( "IpAddr" , "bbs--Thread.getIpAddr2" , 1 , "callback" )_
		)
		
		'数据验证
		db.validate_ = Array( _			
			Array( "Content","","内容不能为空" , 0 , "notempty" , 3 ) _	
			,Array( "Content" ,guestConfig.ReplyContentLimit & ",2000",  "内容长度不能少于" & guestConfig.ReplyContentLimit & "个字符"  , 1, "length" , 3  ) _
			,Array( "Content","bbs--faq.TestValidate","有效字符数太少，请修改留言再发布" , 1 , "callback" , 3 , array() ) _
			,Array( "verify","","验证码不能为空" , 0 , "notempty" , 1 ) _
			,Array( "verify","\d{4}","验证码不正确" , 0 , "regex" , 1 ) _
			,Array( "verify","","验证码不正确" , 0 , "verify" , 1 ) _
			,Array( "Content","bbs--Faq.testAllow","已禁止留言" , 0 , "callback" , 3 ) _
			,Array( "Content","bbs--Faq.testIpLimit","留言太过频繁" , 0 , "callback" , 3 , array() ) _
			,Array( "Content","bbs--Faq.testIpMax","留言超过了今天的限额" , 0 , "callback" , 3 , array() ) _
		)
	end sub
	
	function testAllow(byval value)
		testAllow = false
		if not (guestConfig.ReplyAddAllow - 1 = 0 or S_("IsAdmin") = 1) then exit function
		testAllow = true
	end function
	
	function testIpLimit(byval value)
		testIpLimit = true
		if guestConfig.ReplyAddAllow - 1 = 0 or S_("IsAdmin") = 1 then exit function
		dim prevTime,FaqIntervalTime
		FaqIntervalTime = 30
		value = get_client_ip
		prevTime = db.where( "IP = '" & value & "'" ).field("AddTime").order("FaqID desc").getOne
		if prevTime <> "" then
			if datediff( "n",prevTime , now ) < FaqIntervalTime then
				testIpLimit = false
			end if
		end if
	end function
	
	Function getFloorStep(  )
		dim floor
		floor = db.order("FaqID DESC").field("FloorStep").getOne
		if floor = "" then
			floor = 0
		end if

		getFloorStep = floor + 1
	End Function
	
	function testIpMax(byval value)
        testIpMax = true
		if guestConfig.ReplyAddAllow - 1 = 0 or S_("IsAdmin") = 1 then exit function	
		dim cnt,where,FaqDayMax
		FaqDayMax = guestConfig.ReplyDayLimit - 0
		set where = D_
		where("IP") = get_client_ip
		
		if POP_MVC.config( "DB_TYPE" ) = "access" then
			where("datediff( ""d"",AddTime , now )") = 0
		elseif POP_MVC.config( "DB_TYPE" ) = "sqlite3" then
			where( "(JULIANDAY('now') - JULIANDAY(replace( AddTime , '/' , '-' ))) <= 1 " ) = null
		elseif POP_MVC.config( "DB_TYPE" ) = "mysql" then
			where( "TIMESTAMPDIFF(DAY, AddTime, now()" ) = 0
		end if			

		cnt = db.where(where).count( )
		if cnt >= FaqDayMax then
			testIpMax = false
		end if
	end function
	
	function TestValidate( Message ) 
		dim str,cnt
		if S_("IsAdmin") = 1 then
			TestValidate = true
			exit function
		end if
		str = POP_MVC.String.strip_tags( Message , "html" )
		str = replace( str , " " ,"" )
		str = POP_MVC.String.unique(str)
		if len( str ) - guestConfig.ReplyUniqueLen < 0 then
			TestValidate = false
		else
			TestValidate = true
		end if
	end function
	
	Function getReplyList( id )
		dim html,rs,where
		set where = D_
		where("ParentID") = id
		if S_("IsAdmin") = 1 then
			where("ContentStatus") = Array("neq" , 2)
		else
			where("ContentStatus") = 1
		end if
		set rs = db.where( where ).select
		

		if not rs.eof then
			do while not rs.eof
				if rs("FaqType") = 5 then
					html = html + "<div class='layui-elem-quote bbs-faq-answer' style='padding-right:50px;'><a href='?faq_p_" & rs("FaqID") & "' target='_self'><span class='layui-badge layui-bg-orange bbs-faq-badge'>回答</span></a>" + rs("Content")
					if S_("IsAdmin") = 1 then
						html = html + " <a href='?faq_edit_" & rs("FaqID") & "'>编辑</a>" + " <a href='?faq_remove_" & rs("FaqID") & "'>删除</a>" 
					end if
					html = html + "</div>" 
				elseif rs("FaqType") = 1 then
					html = html + "<div class='layui-elem-quote'><a href='?faq_p_" & rs("FaqID") & "' target='_self'><span class='layui-badge layui-bg-green bbs-faq-badge'>追问</span></a><span class='layui-badge-rim'>" + rs("Contact") + ": </span> " + rs("Content") 
					if S_("IsAdmin") = 1 then 
						html = html + " <a href='?faq_remove_" & rs("FaqID") & "'>删除</a>"
					end if
					if rs("ContentStatus") = 0 then
						html = html +  " <a href='?faq_Audit_" & rs("FaqID") & "'>通过</a>"
					end if
					html = html + "</div>"
				end if

				rs.movenext
			loop
			
		end if
		getReplyList = html
	End Function
end class
%>