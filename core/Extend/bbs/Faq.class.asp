<%
Class Faq 'Extends Top
	Public tableName,statusField,baseTable
	Sub initialize
		tableName = "self_GuestBook"
		statusField = "ContentStatus"
		baseTable = objCMS.baseTable
		that.d("layuicachepage") = "jie"
		that.d("keys") = ""
	End Sub
	
	'列表
	Public Sub index
		dim field,where,NoReplyTip,page,replyZanArr
		dim replyRS
		
		NoReplyTip = ""
		
		page = that.get("page")
		
		'''''帖子内容'''''
		
		set where = D_

		Call K_("bbs--public").getHistoryTopic(0)
		
		'登陆后的回帖提示
		that.d("placeholder") = guestConfig.EditorLoginReplyTip

		that.d("summernote") = ""		

		'''''回复内容'''''
		if guestConfig.ReplyAddAllow - 0 = 0 then
			'是否允许回帖
			NoReplyTip = guestConfig.ReplyAddAllowTip
		else
			POP_MVC.config( "AUTO_EXTENDS_MODULE" ) = "summernote"
			that.d("summernote") = P_("auto").summernote("#L_content")		
		
			if guestConfig.ReplyDayLimit - 0 <> 0 then
				
			end if			
		end if
		that.d("NoReplyTip") = NoReplyTip
		'''''回复内容Recordset'''''
		
		where( "ParentID" ) = 0
		if CStr(S_("IsAdmin")) = "1" then
			where( "ContentStatus" ) = array("neq" , 2)
			set replyRS= B_( baseTable ).onlysql(0).page( page & ",5" ).from( "{prefix}" & tableName ).order("FaqID DESC").where( where ).select
		else
			where( "ContentStatus" ) = 1
			set replyRS= B_( baseTable ).onlysql(0).page( page & ",5" ).from( "{prefix}" & tableName ).order("FaqID DESC").where( where ).select
		end if
		
		
		that.u(where)
		
		that.d("reply") = replyRS					

		that.d("ImageBedType") = "postimg"
		objcms.bbsShow("guestBook/index")
	End Sub
	
	'详细页
	Public Sub search
		dim field,where,NoReplyTip,page,replyZanArr,FaqID,keyStr
		dim replyRS
		
		dim SearchInterval,keys,searchType

		guestConfig.ListCacheLifeTime=0
		if guestConfig.SearchInterval - 0 > 0 and  S_("IsAdmin") <> 1 then
			SearchInterval = Request.Cookies("LastSearchDate")

			if isDate(SearchInterval) then			
				if datediff( "s" , SearchInterval , now ) - guestConfig.SearchInterval < 0 then
					that.error( Array( "您搜索的太频繁了，系统指定 " & guestConfig.SearchInterval & " 秒钟可搜索一次" , -1 ) )
				end if		
			end if		
			
			P_("cookie").unit = "s"
			P_("cookie").Expires = CInt( guestConfig.SearchInterval )
			P_("cookie").set "LastSearchDate",now
		end if	

		searchType = POP_MVC.req("searchType")
		searchType = cStr( searchType )
		
		if POP_MVC.req("keys") <> "" then
			'限定搜索字符串长度不超过20，可有效防止一句话木马
			keys = POP_MVC.req("keys")
			keys = replace( keys, "'" , "" )
			if len(keys) > 20 then
				that.error("搜索字符串不能超过20个字符！")
			end if
		else
			Call index
			exit sub
		end if
		
		
		NoReplyTip = ""
		
		page = that.get("page")
		
		'''''帖子内容'''''
		
		set where = D_

		Call K_("bbs--public").getHistoryTopic(0)
		

		
		'登陆后的回帖提示
		that.d("placeholder") = guestConfig.EditorLoginReplyTip

		that.d("summernote") = ""		

		'''''回复内容'''''
		if guestConfig.ReplyAddAllow - 0 = 0 then
			'是否允许回帖
			NoReplyTip = guestConfig.ReplyAddAllowTip
		else
			POP_MVC.config( "AUTO_EXTENDS_MODULE" ) = "summernote"
			that.d("summernote") = P_("auto").summernote("#L_content")		
		
			if guestConfig.ReplyDayLimit - 0 <> 0 then
				
			end if			
		end if
		that.d("NoReplyTip") = NoReplyTip
		'''''回复内容Recordset'''''
		
		select case searchType
			case "0"
				where("FaqType") = 0
			case "1"
				where("FaqType") = 1
			case "5"
				where("FaqType") = 5
		end select
		
		if CStr(S_("IsAdmin")) = "1" then
			where( "ContentStatus" ) = array("neq" , 2)
			set replyRS= B_( baseTable ).onlysql(0).page("null,5").from( "{prefix}" & tableName ).where( where ).search( keys, "Contact,Content" )
		else
			where( "ContentStatus" ) = 1
			set replyRS= B_( baseTable ).onlysql(0).page("null,5").from( "{prefix}" & tableName ).where( where ).search( keys, "Contact,Content" )
		end if
		
		that.u(where)
		
		that.d("thread") = replyRS					
		that.d("keys") = keys					
		that.d("searchType") = searchType					

		that.d("ImageBedType") = "postimg"
		objcms.bbsShow("guestBook/search")
	End Sub
	
	'详细页
	Public Sub P
		dim field,where,NoReplyTip,page,replyZanArr,FaqID
		dim replyRS
		
		NoReplyTip = ""
		
		FaqID = that.get("id")
		page = that.get("page")
		
		'''''帖子内容'''''
		
		set where = D_

		Call K_("bbs--public").getHistoryTopic(0)
		

		
		'登陆后的回帖提示
		that.d("placeholder") = guestConfig.EditorLoginReplyTip

		that.d("summernote") = ""		

		'''''回复内容'''''
		if guestConfig.ReplyAddAllow - 0 = 0 then
			'是否允许回帖
			NoReplyTip = guestConfig.ReplyAddAllowTip
		else
			POP_MVC.config( "AUTO_EXTENDS_MODULE" ) = "summernote"
			that.d("summernote") = P_("auto").summernote("#L_content")		
		
			if guestConfig.ReplyDayLimit - 0 <> 0 then
				
			end if			
		end if
		that.d("NoReplyTip") = NoReplyTip
		'''''回复内容Recordset'''''
		
		if CStr(S_("IsAdmin")) = "1" then
			where( "(ParentID = " & FaqID & " ) OR ( FaqID = " & FaqID & " )" ) = null
			set replyRS= B_( baseTable ).onlysql(0).page("1,10").from( "{prefix}" & tableName ).order("FaqID ASC").where( where ).find
		else
			where( "(ParentID IN (select ParentID FROM " & C_("DB_PREFIX") & tableName & " where FaqID = " & FaqID & ") and ContentStatus = 1 ) OR ( FaqID = " & FaqID & " ) " ) = null
			set replyRS= B_( baseTable ).onlysql(0).page("1,10").from( "{prefix}" & tableName ).order("FaqID ASC").where( where ).find
		end if

		if replyRS.eof then
			K_("bbs--Public").show404
			response.end
		end if
		
		if CStr(S_("IsAdmin")) <> "1" and replyRS("ContentStatus") <> 1 then
			K_("bbs--Public").show404
			response.end
		end if
		
		that.d("Title") = objCMS.htmlcut2( replyRS("Content") , 30 )
		
		
		that.u(where)
		
		that.d("thread") = replyRS					

		that.d("ImageBedType") = "postimg"
		objcms.bbsShow("guestBook/detail")
	End Sub
	
	sub edit
		if CStr(S_("IsAdmin")) = "" then	
			K_("bbs--Public").showError( "管理员登陆后才可以修改留言" )
		end if	
	
		if CStr(S_("IsAdmin")) <> "1" then	
			sTip = "没有权限编辑该留言"
			Call K_("bbs--Public").CtrlLog( 0 , "用户ID: " & S_("adminId") & " 对留言ID: " &  id & " " & sTip , "您" & sTip )
		end if		
	
		dim id,rs,authInfo,sTip,topicRS,where
		id = that.get("id")
		

		set rs = B_(tableName).where( id ).find
		
		if rs("UserID") <> 0 then		
			authInfo = K_("bbs--Public").AuthCompare( rs("UserID") , "的回帖进行编辑操作！" )
		end if
		
		if authInfo <> "" then
			Call K_("bbs--Public").CtrlLog( 0 , "用户ID: " & S_("adminId") & " 对回复ID: " &  id & " " & authInfo , authInfo )
		end if	
		
		that.d("faq") = rs
		
		
		POP_MVC.config( "AUTO_EXTENDS_MODULE" ) = "summernote"
		that.d("summernote") = P_("auto").summernote("#L_content")
		that.d("ImageBedType") = "postimg"
		objcms.bbsShow( "guestBook/edit" )
	end sub
	
	'添加留言操作
	sub Add
		if not POP_MVC.isSelfOrigin or len(Request.ServerVariables("HTTP_USER_AGENT")) < 80 or guestConfig.ReplyAddAllow - 1 <> 0  then
			that.error( Array("非法操作" , "window.close" ) )
		end if
	
		dim data,flag
		
		flag = false
		
		set data = POP_MVC.form2dict
		
		
		set data = POP_MVC.form2dict
		
		if data("FaqType") = 5 then
			if S_("IsAdmin") <> 1 then
				that.error( Array("非法操作" , "window.close" ) )
			end if
		else
			data("Content") = POP_MVC.String.reg_replace( data("Content") , "" , "<(?!\/?img|\/?p|\/?br)[^<>]*>" , "gmi"  )
		end if	
		
		if not M_("bbs--Faq").db.data(data).create(1) then
			that.error( array(M_("bbs--Faq").db.error , -1 , 3 ) )
		end if
		
		set data = M_("bbs--Faq").db.getData
		
		if data("FaqType") = 1 then
			data("Content") = objCMS.stripTags( data("Content") )
		end if
		
		if data("Contact") = "" then
			data("Contact") = data("IpAddr") & " 游客"
		end if
		
		data("Contact") = left( data("Contact") , 50 )
		
		if data("FaqType") = 5 and S_("IsAdmin") = 1 then
			flag = true
			data("ContentStatus") = 1
			data("Content") = K_("bbs--Public").replaceLink( data("Content") )
		end if
		
		dim id		
		id = M_("bbs--Faq").db.data(data).add	
		if id > 0 then
			if not flag then
				Call K_("bbs--Mail").AddFaq(id)
				that.success( Array("添加成功，请耐心等待管理员审核！" ) )
			else
				Call B_(tableName).where( data("ParentID") ).setField( "ContentStatus" , 1 )
				that.success( Array("留言回答成功！" ) )
			end if			
		else
			that.error( "添加失败" )
		end if
	end sub	
	
	'修改留言操作
	sub doEdit
		if not POP_MVC.isSelfOrigin or len(Request.ServerVariables("HTTP_USER_AGENT")) < 80 or guestConfig.ReplyEditAllow - 1 <> 0  then
			that.error( Array("非法操作" , "window.close" ) )
		end if
		
		dim data,flag,Content
		
		flag = false

		set data = POP_MVC.form2dict
		
		if S_("IsAdmin") <> 1 then
			that.error( Array("非法操作" , "window.close" ) )
		end if	
		
		if not M_("bbs--Faq").db.data(data).create(2) then
			that.error( array(M_("bbs--Faq").db.error , -1 , 3 ) )
		end if
		
		set data = M_("bbs--Faq").db.getData
		
		if  S_("IsAdmin") = 1 then
			flag = true
			data("ContentStatus") = 2
			data("Content") = K_("bbs--Public").replaceLink( data("Content") )
		end if
		
		if M_("bbs--Faq").db.data(data).save then
			that.success( Array("留言修改成功，且已公开！" ) )		
		else
			that.error( "留言修改失败" )
		end if
	end sub	
	
	'通过留言,用于公开
	Sub Audit
		if CStr(S_("IsAdmin")) = "" then	
			K_("bbs--Public").showError( "管理员登陆后才可以审核留言" )
		end if	
	
		if CStr(S_("IsAdmin")) <> "1" then	
			sTip = "没有权限处理该留言"
			Call K_("bbs--Public").CtrlLog( 0 , "用户ID: " & S_("adminId") & " 对留言ID: " &  id & " " & sTip , "您" & sTip )
		end if		
	
		dim id,rs,authInfo,sTip
		id = that.req("id")
		
		'根据ID取记录
		set rs = B_(tableName).where( id ).find
		
		'B_("self_GuestReply").where( id ).remove
		Call B_(tableName).where( id ).setField( "ContentStatus" , 1 )
		
		Call K_("bbs--Public").CtrlLog( 1 , "用户 ID: " & S_("adminId") & "，通过审核" & "ID: " &  id , "留言审核成功" )
	End Sub	
	
	'删除某条留言
	sub remove
		if CStr(S_("IsAdmin")) = "" then	
			that.error( "管理员登陆后才可以删除留言" )
		end if	
	
		if CStr(S_("IsAdmin")) <> "1" then	
			sTip = "没有权限处理该留言"
			Call K_("bbs--Public").CtrlLog( 0 , "用户ID: " & S_("adminId") & " 对留言ID: " &  id & " " & sTip , "您" & sTip )
		end if		
	
		dim id,rs,authInfo,sTip
		id = that.req("id")
		
		'根据ID取记录
		set rs = B_(tableName).where( id ).find
		
		'B_("self_GuestReply").where( id ).remove
		Call B_(tableName).where( id ).setField( "ContentStatus" , 2 )
		
		
		that.success( Array( "留言删除成功" , "" ) )
	end sub
	
	'清除全部留言
	sub Clear
		dim cnt
		if S_("GroupID") <> 1 then	
			that.error("只有超级管理员才可以操作！")
		end if	

		cnt = B_(tableName).count
		Call B_(tableName).where( "1=1" ).remove		
		
		that.success( "共清除 " & cnt & " 条留言!" )
	end sub
	
	'清除留言回收站
	sub ClearRecycle
		dim cnt
		if S_("GroupID") <> 1 then	
			that.error("只有超级管理员才可以操作！")
		end if	

		cnt = B_(tableName).where("ContentStatus = 2").count
		Call B_(tableName).where( "ContentStatus = 2" ).remove		
		
		that.success( "共清除 " & cnt & " 条留言!" )
	end sub
	
	'对留言点赞
	sub doZan
		if S_("adminId") = "" then
			that.error("请先登陆后再点赞！")
		end if
		'{"id":"1","ok":"false"}
		dim where,id,sTip,ZanUser,UserID
		
		'得到回岾ID
		if POP_MVC.isPost then
			id = that.req("cid")		
		else
			id = that.req("id")		
		end if
		
		if not that.isID( id ) then
			that.error( "非法操作" )
		end if
		
		'查询条件
		set where = D_
		where("FaqID") = id
		
		'根据UserID取得RS
		ZanUser = B_( baseTable ).from( "{prefix}" & tableName ).where(where).field("ZanUser").getOne
		
		ZanUser = repNull(ZanUser)
		UserID = S_("adminId")
		
		'存在时还得判断ReplyZan是否包含了id
		if inStr( "," & ZanUser & "," , "," & UserID & "," ) > 0 then
			sTip =  "已经赞过了"
			Call K_("bbs--Public").CtrlLog( 0 , "用户ID: " & S_("adminId") & " 对回复ID:" & id & " " & sTip , Array( "您" & sTip ,-1 ) )
		else
			'不存在时需要添加
			if ZanUser = "" then
				ZanUser = UserID
			else
				ZanUser = ZanUser & "," & UserID
			end if
			Call B_(tableName).where( id ).setField( "ZanUser" , ZanUser )
			Call B_(tableName).where( id ).setInc( "ZanCount" )
		end if
		
		that.u(where)
		
		sTip =  "点赞成功"
		Call K_("bbs--Public").CtrlLog( 1 , "用户ID: " & S_("adminId") & " 对回复ID:" & id & " " & sTip ,  Array( sTip ,-1) )
	end sub
	
	'通过ajax返回点赞的用户
	sub listZan
		dim id,dict,where,rs,arr,FaqID
		
		'案例ID
		FaqID = that.req("id")
		
		'取标题与用户ID
		set rs = B_( baseTable ).from( "{prefix}" & tableName ).where( that.dict( "FaqID" , FaqID )  ).field("FaqID,ZanUser").find
		
		'案例不存在时
		if rs.eof then
			that.u(rs)
			that.error( Array("访问的留言不存在") )
		end if	

		'构造ajax返回对象
		set dict = D_
		
		set where = D_
		arr = split( repnull(rs("ZanUser")) )
		if ubound(arr) > -1 then
			where("UserID") = Array( "in" , arr )
		else
			where("1=2") = null
		end if
		
		set rs = B_( objCMS.baseTable ).from("{prefix}User").field("UserID as id, Nickname as username, avatar").where( where ).page("1,500").select
		
		dict("status") = that.ajaxSuccessStatus	
		dict.add "data",POP_MVC.rs2dict(rs)
		that.u(rs)
		that.ajaxData( dict )
	end sub
End Class
%>