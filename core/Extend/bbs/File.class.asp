<%
Class File 'Extends bbs--Common
	sub initialize
		if S_("IsAdmin") <> 1 then
			that.error("您无权查看该页面")
		end if
	end sub
	
	'上传文件列表，暂不能使用
	sub upload_list
		dim path,isTop,TopPath
		path = that.get("path")
		TopPath = POP_MVC.config("CMS_UPLOAD_PATH")
		
		if path = "" then
			path = TopPath
			isTop = true
		else
			'不是文件夹
			if not POP_MVC.file.isFolder( path ) then
				that.error("非法操作")
			end if		
		
			if not POP_MVC.file.in( TopPath , path ) then
				that.error("非法操作")
			end if
			
			if POP_MVC.realPath( path ) = POP_MVC.realPath( TopPath ) then
				isTop = true
			end if
		end if

		that.d( "dir" ) = path
		that.d("isTop") = isTop
		that.d( "updir" ) = POP_MVC.file.dir( path )
		that.show
	end sub
	
	'上传文件，暂不能使用
	sub upload_view
		dim path,isTop,TopPath
		path = that.get("path")
		TopPath = POP_MVC.config("CMS_UPLOAD_PATH")
		P_("page").config("perpageTip") = ""
		P_("page").config("prev") = "前页"
		P_("page").config("[next]") = "后页"
		P_("page").config("pagesize") = Empty
		
		if path = "" then
			path = TopPath
			isTop = true
		else
			'不是文件夹
			if not POP_MVC.file.isFolder( path ) then
				that.error("非法操作")
			end if		
		
			if not POP_MVC.file.in( TopPath , path ) then
				that.error("非法操作")
			end if
			
			if POP_MVC.realPath( path ) = POP_MVC.realPath( TopPath ) then
				isTop = true
			end if
		end if

		that.d( "dir" ) = path
		that.d("isTop") = isTop
		that.d( "updir" ) = POP_MVC.file.dir( path )
		that.show
	end sub
	
	'模板文件显示，暂不能使用
	sub template
		dim fileType,site_path,tplType,tplPath
		fileType = LCase(POP_MVC.get("fileType"))
		site_path = POP_MVC.config("sitePath")
		tplType = POP_MVC.get("tplType")
		if tplType <> "" then
			tplPath = site_path & "/Templates/" & guestConfig.DefaultTemplate & "/" & tplType & "/"
		else
			tplPath = site_path & "/Templates/" & guestConfig.DefaultTemplate & "/"
		end if

		if fileType="css" then 
			tplPath = tplPath & "css"
		elseif fileType="js" then 
			tplPath = tplPath & "js"
		else	
			fileType = "html"
			if tplType = "" then			
				tplPath = tplPath & objCMS.htmlFilePath			
			end if	
		end if
		
		that.d("tplType") = tplType
		that.d("tplPath") = tplPath
		
		that.display("file/template")
	end sub
End Class
%>