<%
Class FileTool
	public countnum,xmlDomObj,soTableFields
	Private testTableStr,testFileStr
	Private testStr
	
	
	Private Sub Class_Initialize
		countnum = 0
		soTableFields = Array( "self_GuestTopic" ,"IndexImage,DownURL,Content,ContentSource", "User" , "Avatar,Address" , "self_GuestConfig" ,"ConfigValue" ,  "self_GuestReply" , "Content" , "self_GuestCases" , "IndexImage" , "self_UploadLog" , "UploadPath" , "self_GuestBook", "Content"  )
	end sub
	
	Private Sub Class_Terminate
		if isObject( xmlDomObj ) then set xmlDomObj = nothing
	end sub
	
	sub flushStart()
		response.write "<center>正在检测" & testStr & "，检测时间可能有点长，请耐心等待……</center>"
		response.flush
	end sub
	
	sub flushEach( path ,size )
		response.write "<center>检测到第" & countnum & "个" & testStr & " <a href='" & path & "' target='_blank'>" & path & "</a>  " & size & "</center>"
		response.flush
	end sub
	
	sub flushEnd()
		response.write "<center>已检测完毕，共发现" & countnum & "个" & testStr &  "，3秒后自动关闭当前页面！<script>setTimeout(function(){parent.location.reload();try{window.close();}catch( err ) {window.open('','_self').close();}finally{window.open('about:blank','_self').close();}},3000);</script></center>"
		response.flush
	end sub
	
	sub file_put_test( folder )
		Call POP_MVC.file_put_contents_without_bom( POP_MVC.appPath & "/runtime/" & folder & "/table.txt" , testTableStr )
		Call POP_MVC.file_put_contents_without_bom( POP_MVC.appPath & "/runtime/" & folder & "/file.txt" , testFileStr )
	end sub
	
	function file_get_test( folder )
		file_get_test = POP_MVC.file_get_contents( POP_MVC.appPath & "/runtime/" & folder & "/table.txt")
	end function
	
	Function TestMap( mapType, ByVal k_name, method, ByVal uploadDir, folder, tip  )
		testStr = tip
		Call flushStart
		testTableStr = ""
		
		if k_name = "" then
			k_name = "bbs--FileTool"
		end if
		
		if uploadDir = "" then
			uploadDir = C_("CMS_UPLOAD_PATH")
		end if
	
		if mapType = "files_map" then
			call POP_MVC.File.files_map( uploadDir , "K_(""" & k_name & """)." & method )
		elseif mapType = "folders_map" then
			call POP_MVC.File.folders_map( uploadDir , "K_(""" & k_name & """)." & method )	
		end if
		
		if testTableStr = "" then
			testTableStr = "<tr><td colspan=5>没有" & testStr & "</td></tr>"
		end if
		
		testTableStr = "<div class='layui-form'><table class='layui-table'><thead><tr><th>序号</th><th>文件名称</th><th>大小</th><th>类型</th><th>修改日期</th></tr></thead><tbody>" & testTableStr	& "</tbody></table></div>"
		TestMap =testTableStr
		Call file_put_test( folder )
		Call flushEnd
	End Function
	
	Function TestFiles( ByVal k_name, method, ByVal uploadDir, folder, tip )
		TestFiles = TestMap( "files_map" , k_name, method, uploadDir, folder, tip )
	End Function
	
	Function TestFolders( ByVal k_name, method, ByVal uploadDir, folder, tip )
		TestFolders = TestMap( "folders_map" , k_name, method, uploadDir, folder, tip )
	End Function
	
	Function getTableStr( FolderName )
		dim time1,time2,filepath
		filepath = POP_MVC.appPath & "/runtime/" & FolderName & "/table.txt"
		'如果已经保存了检测，且未再上传，从文件中显示
		if POP_MVC.file.isfile(filepath) then
			time1 = POP_MVC.file.mtime( POP_MVC.appPath & "/runtime/" & FolderName & "/table.txt" )
			time2 = POP_MVC.file.mtime( POP_MVC.config("CMS_UPLOAD_PATH") )
			if  DateDiff("s", time2, time1) > 0 then
				getTableStr = file_get_test(FolderName)
			end if
		end if
	end Function
	
	'清除冗余文件
	Function FolderClear( FolderName )
		dim filepath,content,arr,i,cnt
		filepath = POP_MVC.appPath & "/runtime/" & FolderName & "/file.txt"
		cnt = 0
		if POP_MVC.file.isfile( filepath ) then
			content = POP_MVC.file_get_contents( filepath )
			if content <> "" then
				arr = split( content, "," )
				for i = 0 to ubound( arr )
					cnt = cnt + 1
					Call K_("bbs--Public").recycleUpload( arr(i) )
				next
			end if
		end if
		POP_MVC.file.remove( POP_MVC.appPath & "/runtime/" & FolderName )	
		FolderClear = cnt
	end Function
	
	'清除冗余文件
	Function redundancyClear
		redundancyClear = FolderClear( "redundancy" )
	end Function
	
	'清除冗余文件
	Function safeTestClear
		safeTestClear = FolderClear( "SafeTest" )
	end Function
	
	'清除空文件夹
	Function emptyFolderClear
		dim filepath,content,arr,i,cnt
		filepath = POP_MVC.appPath & "/runtime/emptyFolder/file.txt"
		if POP_MVC.file.isfile( filepath ) then
			content = POP_MVC.file_get_contents( filepath )
			if content <> "" then
				arr = split( content, "," )
				Call POP_MVC.file.removeFromDir(  C_("CMS_UPLOAD_PATH") ,arr )
			end if
		end if
		POP_MVC.file.remove( POP_MVC.appPath & "/runtime/emptyFolder" )
		emptyFolderClear = ubound(arr) + 1
	end Function
	
	Function redundancy()
		redundancy = TestFiles( "" , "redundancy_" , "" , "redundancy" ,  "冗余文件" )
	End Function
	
	Function safeTest()
		safeTest = TestFiles( "" , "safeTest_" , "" , "safeTest" ,  "挂马图片" )
	End Function
	
	'空文件夹检测
	Function emptyfolder()
		emptyfolder = TestFolders( "" , "emptyfolder_" , "" , "emptyFolder" ,  "空文件夹" )
	End Function
	
	'是否为冗余文件
	Function needRedundancy( path )
		dim fields,rs,i,tableName
		
		needRedundancy = false
		
		for i = 0 to ubound( soTableFields ) step 2			
			tableName = soTableFields( i )
			fields = soTableFields( i + 1 )

			set rs = B_( tableName ).top(1).search( path,fields)
			
			if not rs.eof then
				rs.close : set rs = nothing : exit Function
			end if
		next

		needRedundancy = true
	End Function
	
	'从一个字段中删除多余的图片或附件
	Sub removeFileFromField( fieldValue , ByRef count )
		dim arr,item
		if not isNul( fieldValue ) then
			arr = split( fieldValue, "," )
			for each item in arr
				item = trim(item)
				if POP_MVC.file.isFile(item) then
					if needRedundancy( item ) then
						POP_MVC.file.remove( item )
						count = count + 1
					end if
				end if
			next
		end if
	End Sub	
	
	'从一个文章内容中删除多余的图片或附件
	Sub removeFileFromContent( content, ByRef count )
		dim matches,match,path
		set matches = POP_MVC.String.reg_exec( content, "(?:src|href)\s*=\s*(|'|"")(" & POP_MVC.config("CMS_UPLOAD_PATH") & ".+?)\1" , "gim" )
		for each match in matches
			path = C_("sitePath") &  match.submatches(1)
			if needRedundancy( path ) then
				POP_MVC.file.remove( path )
				count = count + 1
			end if
		next	
	End Sub	
	
	'回调函数，冗余文件处理
	sub redundancy_( file )
		on error resume next
		dim path,size,filetype,extname
		filetype=POP_MVC.file.getFileType(file.path)
		path = mid( file.path,len(POP_MVC.realpath("./"))+2  )

		path = replace(path,"\" , "/")
		extname = LCase(Mid(file.name, InStrRev(file.name, ".") + 1))

		if not needRedundancy( path ) then
			exit sub
		end if
		
		size = byte2size(file.size)	

		Call setTestStr( file , path , fileType,  extname , extname  )
	end sub
	
	sub emptyfolder_( file )
		dim path,size,rs
		
		if file.SubFolders.count > 0 then exit sub
		
		if file.Files.count > 0 then exit sub
		
		path = mid( file.path,len(POP_MVC.realpath("./"))+1  )
		path = replace(path,"\" , "/")
		
		Call setTestStr( file , path , "folder",  "folder" , "文件夹"  )
	end sub
	
	sub SafeTest_( file )
		on error resume next
		dim path,size,filetype,extname,content
		filetype=POP_MVC.file.getFileType(file.path)
		path = mid( file.path,len(POP_MVC.realpath("./"))+2  )

		path = replace(path,"\" , "/")
		extname = LCase(Mid(file.name, InStrRev(file.name, ".") + 1))

		if fileType <> "img" then
			exit sub
		end if
		
		
		if K_("bbs--Public").TestPicMuma( file.path ) = "" then
			exit sub
		end if
		
		size = byte2size(file.size)	

		Call setTestStr( file , path , fileType,  extname , extname  )
	end sub
	
	sub setTestStr( file , path , fileType,  extname , tip  )
		dim size
		size = byte2size(file.size)	
		countnum = countnum + 1	

		Call flushEach( path,size )
		if not isEmpty( testFileStr ) then
			testFileStr = testFileStr & "," & path
		else
			testFileStr = path
		end if
		
		testTableStr = testTableStr & "<tr>"
		testTableStr = testTableStr & "<td>" & countnum & "</td>"
		testTableStr = testTableStr & "<td style='text-align:left'>"

		testTableStr = testTableStr & "<a target='_blank' href='" & path & "'>"
		testTableStr = testTableStr & "<img border=0 width='20' height='20' src='static/images/icon/" & extname &  ".gif' />&nbsp;"
		testTableStr = testTableStr & file.name
		if filetype="img" then
			testTableStr = testTableStr & "</a>"
		end if
		testTableStr = testTableStr & "</td>"
		testTableStr = testTableStr & "<td>" & size & "</td>"
		testTableStr = testTableStr & "<td>" & tip & "</td>"
		testTableStr = testTableStr & "<td>"& file.DateLastModified & "</td>"
		testTableStr = testTableStr & "</tr>" & vbcrlf
	end sub
End Class
%>