<%
Class Index  'Extends bbs--Top
	private tableName,orderStr,TopicFieldStr,baseTable
	private cacheKey,isCached
	
	sub initialize	
		tableName = "self_GuestTopic"
		that.d("layuicachepage") = "index"	
		TopicFieldStr = "a.TopicID,a.SortID,a.AddTime,a.IndexImage,a.DownURL,a.Title,a.IpAddr,a.ContentStatus,a.IsTop,a.Isrecommend,a.IsHeadline,a.IsFeatured,a.IsClosed,a.IsReplyView,a.Visits,a.IsNoComment,a.ReplyCount,a.AnswerReward,a.IsSolved,a.IndexImage,c.LoginName,c.Nickname,c.GroupID,c.Avatar,c.UserID,c.Experience,c.ThreadReplyView"
		baseTable = objCMS.baseTable
	end sub
	
	sub test7
		var_export K_("bbs--Mail").getBodyHeader("abc")
	end sub
	
	private sub getip
		var_export P_("POPASP_TQQWry").getProvince("120.207.96.62")
	end sub
	
	private sub test3
	'sub test3
		dim content,arr,tool
		
		
		var_export K_("IP--Public").look_ip("117.136.12.79")
		
		
		response.end
		content = POP_MVC.file_get_contents( "/key.txt" )
		arr = POP_MVC.String.reg_array( content,"\b((setting|config)\.[\w\_]+\b)" , 1 , "gim" )
		
		for i = 0 to ubound(arr)
			arr(i) = replace( arr(i) , "config." , "" )
			arr(i) = replace( arr(i) , "setting." , "" )
			'arr(i) = "[" & arr(i) & "]"
		next
		arr = POP_MVC.arr.iunique(arr)
		var_export join(arr ,"," )
		response.end
	end sub
	
	sub test4
		var_export K_("ip").getAttr("118.78.38.185")
	end sub
	

	'正则取以config.开头的
	'(?<!\.)\bconfig\.
	'替换为空 D\:\\asp\\asp.*
	private sub getConfigArr
		dim arr
		arr = POP_MVC.String.reg_array( POP_MVC.file_get_contents("./config.txt") , "config\.(\w+)\b(?!\.)" , 1, "gim" )
		arr = POP_MVC.arr.iunique(arr)
		POP_MVC.arr.sort arr
		var_export join(arr,",")
	end sub
	
	'首页
	sub index
		dim content,key,obj,cacheKey
		
		'Call P_("APPLICATION").Clear
	
		cacheKey = "IndexPage1_" &  that.get("id") & "_" & that.get("page")
	
		set obj = objCMS

		isCached = obj.isCached( cacheKey , "" )
		'获取与分配签到数据结束
		Call getSignData
		
		if isEmpty(orderStr) then
			orderStr = "TopicID DESC"
		end if
		Call getOnline
		call getHotTopic(0)		
		Call getHotReply			
		Call getNewReply
		Call GetTopData
		Call GetListData
		Call K_("bbs--public").getHistoryTopic(0)
		
		'分页形式设置，第一页为“获取更多”
		if page = "" then
			that.d("pagelist") = guestConfig.defaultTemplate + "/html/common/pagemore4index"
		else
			that.d("pagelist") =  guestConfig.defaultTemplate + "/html/common/pagehome"
		end if

		obj.show( "jie/home" )

		if POP_MVC.config("SHOW_PAGE_TRACE") = 0 then
			Call POP_MVC.ClearSqlRS("*")
		end if
		
	end sub
	
	'当前在线用户
	private sub getOnline
		dim rs,where
		
		if guestConfig.FetchIndexOnlineUser - 0 = 0 then
			exit sub
		end if
		
		set where = D_
		where("Online") = Array( "neq" , 0 )
		where("LastLoginTime") = array( "minuteDiff" , iif( guestConfig.LoginUseCookie<>0,guestConfig.LoginOnlineLimit,session.Timeout ) )
		set rs = B_(baseTable).from("{prefix}User").where(where).top(40).field("UserID,LoginName,Nickname,Avatar,LastLoginTime").select
		that.d("lineUser") = rs
		that.d("lineUserCount") = rs.recordCount
		set where = nothing
	end sub
	
	'获取置顶帖子
	Private sub GetTopData
		dim where
		
		if guestConfig.FetchIndexTop - 0 = 0 then
			exit sub
		end if
		
		if guestConfig.IndexTopCount - 0 <= 0 then
			exit sub
		end if
		
		if isCached then
			exit sub
		end if
		
		'''''''''''公共条件开始'''''''''''
		set where = D_		
			
		where("a.UserID = c.UserID ") = null
		where("b.GroupID = c.GroupID ") = null
		where("a.ContentStatus") = 1		
		'''''''''''公共条件结束'''''''''''
		
		
		'''''''''''取置顶帖子开始'''''''''''
		where("IsTop") = 1
		orderStr2 = "TopicID DESC"
		

		that.d("topList") = B_(baseTable).onlysql(0).top( guestConfig.IndexTopCount ).from( C_("THREAD_TABLE") ).field( TopicFieldStr ).order(orderStr2).where( where ).select		
		'''''''''''取置顶帖子结束'''''''''''
		
		set where = nothing
	End sub
	
	'获取帖子列表
	private sub getListData	
		dim rsObj,where,orderStr2,field
		
		if guestConfig.FetchIndexList - 0 = 0 then
			exit sub
		end if
		
		if isCached then
			exit sub
		end if
		
		'''''''''''公共条件开始'''''''''''
		set where = D_		
			
		where("a.UserID = c.UserID ") = null
		where("b.GroupID = c.GroupID ") = null
		where("a.ContentStatus") = 1
		
		if guestConfig.SortHideID <> "" then
			where("a.SortID") = Array( "NOTIN" , guestConfig.SortHideID )
		end if		
		
		'''''''''''公共条件结束'''''''''''
		
		where("IsTop") = 0
		
	
		'''''''''''取分页帖子开始'''''''''''
		Call B_(baseTable).onlysql(0).page( array( null, guestConfig.IndexListPageCount ) ).from( C_("THREAD_TABLE") ).field( TopicFieldStr ).where( where )
		set rsObj= B_(baseTable).order(orderStr).select
		
		'''''''''''取分页帖子结束'''''''''''
		

		''分页形式设置结束
		
		that.d("recordCount") = rsObj.RecordCount		
		that.d("thread") = rsObj
		
		set where = nothing
	end sub	
	
	'获取签到
	private sub getSignData
		dim signRS
		'获取与分配签到数据
		that.d("IsSign") = false
		that.d("SignDays") = 0
		
		if S_("adminId") <> "" then			
			set signRS = B_(objCMS.baseTable).from("{prefix}self_GuestDigg").where( "UserID = " & S_("adminId") ).field("UserID,SignTime,SignDays,SignExperience").find

			that.d("SignExperience") = K_("bbs--Experience").getNextSignExperience(signRS)
			if not signRS.eof then
				if  datediff( "d" , signRS("SignTime") , now() ) <=1 then
					that.d("IsSign") = true	
					that.d("SignDays") = signRS("SignDays")
					that.d("SignExperience") = signRS("SignExperience")
				end if
			end if
		end if
		

	end sub
	
	'本月/周热议
	Private sub getHotTopic(SortID)
		dim where,rs,sql,hotKV
		
		if guestConfig.FetchIndexHot - 0 = 0 then
			exit sub
		end if
		
		if isCached then
			exit sub
		end if
		
		set where = D_
		
		
		if SortID <> "" and SortID <> 0 then
			where("a.SortID") = Array("IN",SortID)			
		end if
		where("a.TopicID = b.TopicID") = null
		where("a.ReplyCount") = Array(">",0)
		where("a.ContentStatus") = 1
		where( "b.AddTime" ) = Array( "daydiff" , guestConfig.IndexHotDate )

		
		'本周热议
		sql = B_(baseTable).onlysql(1).order("count(a.TopicID) desc").field("a.TopicID").from("{prefix}self_GuestTopic as a, {prefix}self_GuestReply as b").where( where ).group("a.TopicID").top(15).select
		
		set hotKV = B_(baseTable).onlysql(0).order("count(a.TopicID) desc").field("a.TopicID,count(a.TopicID) as cnt").from("{prefix}self_GuestTopic as a, {prefix}self_GuestReply as b").where( where ).group("a.TopicID").top(15).getKeyValue
		
		that.d("hotKV") = hotKV
		
		if POP_MVC.config("DB_TYPE") = "mysql" then
			sql = "SELECT TopicID FROM ( " + sql + " ) as c"
		end if
	
		set where = nothing
		set where = D_
		where( "TopicID IN (" + sql + ")" ) = null
		

		set rs = B_(baseTable).ResumeOpts().onlysql(0).field("TopicID,Title,ReplyCount,IndexImage,DownURL").from("{prefix}self_GuestTopic").where(where).order("ReplyCount DESC").select
		
		that.d("HotTopicCount") = rs.recordCount
		that.d("HotTopic") = rs	

		set where = nothing
	end sub
	
	'回帖 日/周/月/年 榜
	Private sub getHotReply()
		dim where,rs,ids,dict
		
		if guestConfig.FetchIndexReplyUser - 0 = 0 then
			exit sub
		end if
		
		if isCached then
			exit sub
		end if
		
		set where = D_	
		
		where( "AddTime" ) = Array( "daydiff" , guestConfig.IndexHotReplyDate )
				
		
		'月榜
		set rs = B_(baseTable).from("{prefix}self_GuestReply").onlysql(0).top(10).order("ReplyCount DESC").field("UserID,count(UserID) as ReplyCount").group("UserID").order("count(UserID) DESC").where( where ).select	
		
		ids = ""
		do while not rs.eof
			ids = ids + CStr(rs("UserID")) + ","
			rs.movenext
		loop
	
		set where = nothing
		if ids <> "" then
			ids = POP_MVC.Trim(ids,",")
			set where = D_
			where( "UserID" ) = Array( "IN" , ids )
			set dict =  B_(baseTable).from("{prefix}User").field("UserID,LoginName,Nickname,Avatar").where(where).getPKAll
			

			that.d("kvs") = dict
		end if

		if rs.RecordCount > 0 then
			rs.movefirst
		end if

		that.d("HotReply") = rs	
		set where = nothing
	end sub
	
	'最新回复
	Private sub getNewReply()
		dim where,rs,ids,dict
		
		if guestConfig.FetchIndexReply - 0 = 0 then
			exit sub
		end if
		
		if isCached then
			exit sub
		end if
		
		set where = D_	
		
		'where( "AddTime" ) = Array( "daydiff" , guestConfig.IndexHotReplyDate )
				
		set where = D_
		where("a.ContentStatus") = 1
		where("b.ContentStatus") = 1
		where("a.TopicID = b.TopicID") = null
		where("b.UserID = c.UserID") = null
		set rs = B_(baseTable).field("a.TopicID,Title,b.IndexImage,b.Content,b.AddTime as ReplyTime,b.ZanCount,ReplyID,c.Avatar,c.Nickname,c.LoginName,a.UserID").from("{prefix}self_GuestTopic as a, {prefix}self_GuestReply as b, {prefix}User as c").where( where ).top(10).order("ReplyID DESC").select		

		that.d("NewReply") = rs	
		that.d("NewReplyCount") = rs.RecordCount
		set where = nothing
	end sub
	
	'所有页面
	private sub catalog
		objCMS.show( "catalog" )
	end sub	
	
	private sub filemap
		response.write K_("bbs--Plugin").getExtendDirMap("bbs")
	end sub
end Class
%>