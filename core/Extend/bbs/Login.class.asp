<%
'''登陆控制器
Class Login
	Private NoJump

	''登陆页面
	sub index
		session("LoginRefer") = POP_MVC.vars("HTTP_REFERER")
		if session("adminId") <> "" then
			Response.redirect("?user")
		else
			that.d("layuicachepage") = "user"
		
			objCMS.show( "reg/login" )
		end if
	end sub
	
	'超过6小时的设置为不在线
	private sub setUnline
		dim where
		set where = D_
		where("Online") = Array( "neq" , 0 )
		where("LastLoginTime") = array( "minutegt" , iif( guestConfig.LoginUseCookie<>0,360,session.Timeout ) )
		Call B_("User").where(where).setField( "online" , 1 )
	end sub
	
	''登陆操作
	sub doLogin
		dim rsObj,where,validate,rs,dict,UserID,sTip,LoginName,LoginType
		
		'如果不是通过post进行表单提交
		if not that.isPost then
			that.error("您访问的页面不存在")		
		end if
		
		set dict = POP_MVC.form2dict

		
		'D_是创建Dictionary对象的快捷方法，可自动销毁
		set where = D_
		
		'默认用户名登陆
		LoginType = 1
		LoginName = dict( "LoginName" )
		
		if P_("POPASP_AUTOVALIDATE").regex( LoginName, "email" ) then
			dict("Email") = LoginName
			LoginType = 2
		end if
		
		'邮箱登陆与密码登陆
		if LoginType =1 then		
			validate = array( _
				array( "LoginName" , "" , "用户名不能为空" , 1, "notempty" ) _
				,array( "Password" , "" , "密码不能为空" , 1, "notempty" ) _
				,array( "verify" , "" , "验证码不能为空" , 0, "notempty" ) _
				,array( "verify" , "\w{4}" , "验证码为数字或字母" , 0) _
				,array( "verify" , "" , "验证码错误" , 0,"verify") _
				,array( "LoginName" , "user" , "用户名不存在" , 1, "exists" ) _
			)
			'that.form相当于request.form的简写			
			where("LoginName") = dict("LoginName")
		elseif LoginType = 2 then		'帐户与密码登陆
			validate = array( _
				array( "Email" , "" , "邮箱不能为空" , 1, "notempty" ) _
				,array( "Email" , "" , "必须是邮箱格式" , 1, "email" ) _
				,array( "Password" , "" , "密码不能为空" , 1, "notempty" ) _
				,array( "verify" , "" , "验证码不能为空" , 0, "notempty" ) _
				,array( "verify" , "\w{4}" , "验证码为数字或字母" , 0) _
				,array( "verify" , "" , "验证码错误" , 0,"verify") _
			)
			where("Email") = dict( "Email" )
			dict("Email") = LoginName
		end if
		

		
		'通过P_方法创建的基类对象，为单例模式
		if not P_("autovalidate").handle( validate,dict,3 )	then
			sTip = P_("autovalidate").error
			Call K_( "bbs--Public" ).CtrlLog( 0, sTip , array( sTip ,-1) )
		end if
		
		'rs也可以自动销毁
		set rsObj = B_("User").where( where ).find()		
		if rsObj.eof then	
			that.u(rsObj)
			sTip = "输入的用户名不存在"
			that.u(rsObj)
			Call K_( "bbs--Public" ).CtrlLog( 0, sTip , "您" & sTip )
		elseif rsObj("Password") <> md5(that.form("Password")) then
			sTip = "输入的密码错误"
			that.u(rsObj)
			Call K_( "bbs--Public" ).CtrlLog( 0, sTip , "您" & sTip )
		else
			session("Avatar") = rsObj("Avatar")
			UserID = rsObj("UserID")
			that.u(rsObj)
			Call do_login( UserID , "userpass" )
		end if

		response.end	
	end sub
	
	
	'登陆操作
	'LoginName 帐户
	'stype     登陆方式
	'userpass  帐户/密码 登陆
	'qqreglogin  QQ注册并登陆
	'qqlogin  QQ登陆
	Private sub do_login( UserID , stype )
		dim rsObj,where,randnum,dict,browserInfo,sTip
		dim token,userIdCookie,AdminrandCookie,sUrl
		set where = D_
		where("UserID") = UserID
		where("a.GroupID=b.GroupID") = null
		set rsObj =  B_("User").field("IsAdmin, GroupStatus,GroupName, UserStatus,GroupMark, UserID, adminrand,a.GroupID as group_id,a.Avatar,a.LoginName,Nickname,ThreadStatus,ReplyStatus,Experience").from("{prefix}User as a, {prefix}UserGroup as b").where( where ).onlysql(0).find	
		if not rsObj.eof then		
			if rsObj("GroupStatus")<>1 then 
				sTip = "所在用户组已被禁用！"
				Call K_( "bbs--Public" ).CtrlLog( 0, "用户 ID: " & UserID & " " & sTip , "对不起，您" & sTip )
			end if
			if rsObj("UserStatus")<>1 and rsObj("group_id")<>1 then 
				sTip = "账号已被禁用！"
				Call K_( "bbs--Public" ).CtrlLog( 0, "用户 ID: " & UserID & " " & sTip , "对不起，您的" & sTip )
			end if
			Session("adminName")=rsObj("LoginName")
			Session("GroupName")=rsObj("GroupName")
			Session("adminId")= UserID			
			Session("IsAdmin")= rsObj("IsAdmin")
			Session("GroupID") = rsObj("group_id")
			Session("Nickname") = rsObj("Nickname")
			Session("Avatar") = rsObj("Avatar")			
			Session("Online") = True
			Session("ThreadStatus") = rsObj("ThreadStatus")
			Session("ReplyStatus") = rsObj("ReplyStatus")
			Session("Experience") = rsObj("Experience")
			Session("GroupMark") = rsObj("GroupMark")
	
			randomize
			randnum=clng(rnd*99999999)	
			
			set dict = D_
			if stype <> "cookie" then
				'获取浏览器信息
				browserInfo = left(Request.ServerVariables("HTTP_USER_AGENT"),255)
				
				if checkWap() then
					dict("Waprand") = randnum				
					dict("WapBrowser") = browserInfo
				else
					dict("Adminrand") = randnum				
					dict("Browser") = browserInfo
				end if
				
				dict("LastLoginIP") = get_client_ip()
				
				'如果采用Cookie登陆，则将UserID与Adminrand写入Cookie
				if guestConfig.LoginUseCookie - 0 <> 0 then
					token = getToken()

					userIdCookie	= "UserID_" & token
					AdminrandCookie = "Adminrand_" & token
					P_("cookie").unit = "d"
					P_("cookie").Expires = CInt( guestConfig.LoginUseCookie )
					P_("cookie").set userIdCookie,UserID
					P_("cookie").set AdminrandCookie , randnum
				end if
			else
				session("Avatar") = rsObj("Avatar")
			end if
			
			
			dict("LastLoginTime") = POP_MVC.FormatDate(now,"YYYY-MM-DD HH:II:SS")
			dict("Online") = 1
			session("LastLoginTime") = now()
			
			Call objCMS.setUserData( UserID  , dict , null )
			Call objCMS.setUserInc( UserID  , "LoginCount" )
		end if
		

		
		session("LoginType") = stype
		
		'销毁rsObj对象
		that.u(rsObj)
		
		Call setUnline
		

		
		if guestConfig.LoginReminded - 0 <> 0 then
			if stype = "cookie" then
				'如果使用cookie登陆，且cookie登陆设置提醒时
				if guestConfig.LoginCookieReminded - 0 = 1 then
					K_("bbs--mail").login( stype )
				end if
			else
				K_("bbs--mail").login( stype )
			end if
		end if
		
		sTip = "登陆成功"
		
		if NoJump = "" then
			'如果来自本站，跳转到原页面
			if session( "LoginRefer" ) <> "" then
				if  POP_MVC.String.iStartsWith( POP_MVC.String.reg_replace( session( "LoginRefer" ) , "" , "^https?://" , "i" ),POP_MVC.String.Before(Request.ServerVariables("HTTP_HOST"),":") ) then				
					Call K_( "bbs--Public" ).CtrlLog( 1, "用户 ID: " & UserID & " " & sTip , array( sTip , session( "LoginRefer" ) , 2 ) )
				else
					Call K_( "bbs--Public" ).CtrlLog( 1, "用户 ID: " & UserID & " " & sTip , array( sTip , "./" , 2 ) )
				end if				
			else
				if stype = "reglogin" then
					sTip = "注册成功"
					if guestConfig.DefaultTemplate = "bbs" then
						sUrl = "?user" & guestConfig.PageSuffix
					else
						sUrl = "?" & guestConfig.DefaultTemplate & "--User_settings" & guestConfig.PageSuffix
					end if
					
					Call K_( "bbs--Public" ).CtrlLog( 1, "用户 ID: " & UserID & " " & sTip , array( sTip , sUrl , 2 ) )
				else
					Call K_( "bbs--Public" ).CtrlLog( 1, "用户 ID: " & UserID & " " & sTip , array( sTip , "./" , 2 ) )
				end if				
			end if
		end if

	end sub
	
	'QQ登陆
	sub QQLogin
		on error resume next
		dim openid,userinfo,dict,rs,LoginName,Password,stype,UserID,sTip
	
		openid = S_("QQ_OPENID")
		
		if openid = "" then
			Response.redirect(getLoginUrl())
		end if
		
		set rs = B_("User").where( "QQOpenID = '" & openid & "'"  ).find
	
		'如果是登陆的状态下，则进行QQ绑定
		if S_("adminId") <> "" then
			if not rs.eof then
				if rs("UserID") <> S_("adminId") then				
					sTip = "该QQ已经被用户 " & rs("LoginName") & " 绑定使用"
					Call K_( "bbs--Public" ).CtrlLog( 0, "用户 ID: " & UserID & " " & sTip , Array( sTip , "?UserSet#bind" ) )
				else
					userinfo = S_("API_USERINFO")
					Session.Contents.Remove("API_USERINFO")
					set userinfo = js_decode(userinfo)
					UserID = rs("UserID")
					
					'如果昵称为空，则添加昵称
					if IsNul(rs("Nickname")) then
						Call objCMS.setUserData( S_("adminId") , "Nickname" ,userinfo("nickname") )
					end if
					
					if IsNul(rs("Gender")) then
						Call objCMS.setUserData( S_("adminId") , "Gender" , userinfo("gender_type")  )	
					end if
					
					if IsNul(rs("Province")) then
						Call objCMS.setUserData( S_("adminId") , "Province",userinfo("province") )	
					end if
					
					if IsNul(rs("City")) then
						Call objCMS.setUserData( S_("adminId")  , "City",userinfo("city") )
					end if
					
					if IsNul(rs("Avatar")) then
						Call objCMS.setUserData( S_("adminId")  , "Avatar",userinfo("figureurl_1") )
					end if
					sTip = "QQ成功重新绑定"
					Call K_( "bbs--Public" ).CtrlLog( 1, "用户 ID: " & UserID & " " & sTip , Array( sTip  , "?UserSet#bind" ) )
				end if
			else
				Call objCMS.setUserData( S_("adminId")  , "QQOpenID",openid )
				sTip = "QQ成功绑定"
				Call K_( "bbs--Public" ).CtrlLog( 1, "用户 ID: " & UserID & " " & sTip , Array( sTip ,"?UserSet#bind" ) )
			end if
		end if	
		
		'找不到要注册
		if rs.eof then
			that.u(rs)
			'userinfo = POP_MVC.file_get_contents( "/qq.txt" )
			userinfo = S_("API_USERINFO")
			'Session.Contents.Remove("API_USERINFO")
			set userinfo = js_decode(userinfo)
			LoginName = "u_" & POP_MVC.String.Random(6,5)
			
			if POP_MVC.String.reg_test( userinfo("nickname") , "^\w{6,150}$" , "" ) then
				if B_("User").where( that.dict("LoginName" ,userinfo("nickname") ) ).field("LoginName").getOne = "" then
					LoginName = userinfo("nickname")
				end if
			end if
			
			Password  = md5(POP_MVC.String.Random(6,5))
			set dict = D_
			dict("Nickname")	= userinfo("nickname")
			dict("Gender")		= userinfo("gender_type")
			dict("Province")	= userinfo("province")
			dict("City")		= userinfo("city")
			dict("Avatar")		= userinfo("figureurl_1")
			dict("LoginName")   = LoginName
			dict("Password")    = Password
			dict("QQOpenID")    = openid			
			'Call P_("POPASP_AUTOCOMPLETE").handle( M_("bbs--User").db.auto_ , dict, 1 )
				
			if NOT K_("bbs--Public").ModelCreate("bbs--User",dict,1) then
				sTip = "使用qq注册登陆失败，请使用帐户密码注册登陆！失败原因：<br />" &  M_("bbs--User").db.error
				Call K_( "bbs--Public" ).CtrlLog( 0, sTip , Array( sTip , getLoginUrl() , 7 ) )
			end if
			set dict =  M_("bbs--User").db.getData
			
			'向数据库中添加数据
			UserID = M_("bbs--User").db.data(dict).add
			
			if UserID = "" then			
				sTip = "使用qq注册登陆失败，请使用帐户密码注册登陆！失败原因：<br />不能完成注册"
				Call K_( "bbs--Public" ).CtrlLog( 0, sTip , Array( sTip , getLoginUrl() , 7 ) )
			end if
			stype = "qqreglogin"
		else
			UserID = rs("UserID") 
			session("Avatar") = rs("Avatar")
			stype = "qqlogin"
		end if
		
		that.u(rs)
		Call do_login( UserID , stype )		
	end sub
	
	
	'新浪微博登陆
	sub WeiBoLogin
		dim openid,userinfo,dict,rs,LoginName,Password,stype,UserID
	
		openid = S_("WEIBO_APPKEY")
		
		if openid = "" then
			Response.redirect(getLoginUrl())
		end if
		
		set rs = B_("User").where( "WeiboAppKey = '" & openid & "'"  ).find

		'如果是登陆的状态下，则进行微博绑定
		if S_("adminId") <> "" then
			if not rs.eof then
				if rs("UserID") - S_("adminId") <> 0 then
					sTip = "该微博帐户已经被用户 " & rs("LoginName") & " 绑定使用"
					Call K_( "bbs--Public" ).CtrlLog( 0, "用户 ID: " & S_("adminId") & " " & sTip , Array( sTip , "?UserSet#bind" ) )
				else
					userinfo = S_("API_USERINFO")
					Session.Contents.Remove("API_USERINFO")
					set userinfo = js_decode(userinfo)
					
					'如果昵称为空，则添加昵称
					if IsNul(rs("Nickname")) then
						Call objCMS.setUserData( S_("adminId")  , "Nickname",userinfo("screen_name")  )
					end if
					
					if IsNul(rs("Gender")) then
						Call objCMS.setUserData( S_("adminId")  , "Gender",iif(userinfo("gender")="m",1,0)  )	
					end if
					
					if IsNul(rs("Province")) then
						Call objCMS.setUserData( S_("adminId")  , "Province",userinfo("location") )
					end if
					
					'if IsNul(rs("City")) then
					'	Call objCMS.setUserData( S_("adminId")  , "City",userinfo("city") )
					'end if
					
					if IsNul(rs("Avatar")) then
						Call objCMS.setUserData( S_("adminId")  , "Avatar",userinfo("avatar_large") )
					end if
					
					sTip = "已经绑定了该微博帐户，不要重复操作"
					Call K_( "bbs--Public" ).CtrlLog( 0, "用户 ID: " & S_("adminId") & " " & sTip ,  Array( sTip , "?UserSet#bind" ) )
				end if
			else
				Call objCMS.setUserData( S_("adminId")  , "WeiboAppKey",openid )
				sTip = "微博帐户成功绑定"
				Call K_( "bbs--Public" ).CtrlLog( 1, "用户 ID: " & UserID & " " & sTip , Array( sTip ,"?UserSet#bind" ) )
			end if
		end if	
			
		'找不到要注册
		if rs.eof then
			that.u(rs)
			'userinfo = POP_MVC.file_get_contents( "/qq.txt" )
			userinfo = S_("API_USERINFO")
			Session.Contents.Remove("API_USERINFO")
			set userinfo = js_decode(userinfo)
			LoginName = "u_" & POP_MVC.String.Random(6,5)
			Password  = md5(POP_MVC.String.Random(6,5))
			set dict = D_
			dict("Nickname")	= userinfo("screen_name")
			dict("Gender")		= iif(userinfo("gender")="m",1,0)
			dict("Province")	= userinfo("location")
			'dict("City")		= userinfo("city") '微博中获取的城市为数字代码，不能直接使用
			dict("Avatar")		= userinfo("avatar_large")
			dict("LoginName")   = iif( B_("User").where( that.dict("LoginName" ,userinfo("name") ) ).field("LoginName").getOne = "" , userinfo("name") , LoginName ) 
			dict("Password")    = Password
			dict("WeiboAppKey")    = openid		


			'Call P_("POPASP_AUTOCOMPLETE").handle( M_("bbs--User").db.auto_ , dict, 1 )
			Call K_("bbs--Public").ModelCreate("bbs--User",dict,1)
				
			set dict = M_("bbs--User").db.getData
			'向数据库中添加数据
			UserID = M_("bbs--User").db.data(dict).add
			stype = "weiboreglogin"
		else
			UserID = rs("UserID") 
			session("Avatar") = rs("Avatar")
			stype = "weibologin"
		end if
		
		that.u(rs)

		Call do_login( UserID , stype )		
	end sub	
	
	private function getToken
		getToken = POP_MVC.String.md5( POP_MVC.realPath("./") & C_("CMS_POWER_NAME") )
	end function
	
	'使用Cookie登陆
	'原理，cookie中存储的UserID, Adminrand , LastLoginIP 如果与数据库中的值一致则登陆
	Sub CookieLogin
		dim UserID,Adminrand,LastLoginIP,rs,where,browserInfo
		dim userIdCookie,AdminrandCookie,token
		token = getToken()
		userIdCookie	= "UserID_" & token
		AdminrandCookie = "Adminrand_" & token
		

		
		if P_("cookie").Exists(userIdCookie) and P_("cookie").Exists(AdminrandCookie) then
			UserID 		= Request.Cookies(userIdCookie)
			Adminrand	= Request.Cookies(AdminrandCookie)
			if is_numeric(UserID) and is_numeric(Adminrand) then
				set where = D_
				where("UserID") = UserID
				
				browserInfo = left(Request.ServerVariables("HTTP_USER_AGENT"),255)
				
				if checkWap() then
					where("Waprand") = Adminrand				
					'获取浏览器信息
					where("WapBrowser") = browserInfo
				else
					where("Adminrand") = Adminrand				
					'获取浏览器信息
					where("Browser") = browserInfo
				end if
				
				'ip地址校验
				if guestConfig.LoginCookieCheckIP - 0 = 1 then
					where("LastLoginIP") = get_client_ip()
				end if
				where("LastLoginTime") = Array( "daydiff" , guestConfig.LoginUseCookie )
				set rs = B_("User").where( where ).field("LoginName").find
				if rs.eof then
					that.u(rs)
				else
					that.u(rs)
					'cookie登陆是隐性登陆，不进行跳转
					NoJump = 1
					Call do_login( UserID , "cookie" )
				end if
			end if
		end if	
	End Sub
	
	
	'使用注册并登陆
	'原理，注册时向session中写入adminId = UserID,依此实现登陆
	Sub RegLogin
		if S_("adminId") <> "" then
			if is_numeric( S_("adminId") ) then
				Call do_login( S_("adminId") , "reglogin" )
			end if
		end if	
	End Sub

	sub Empty_
		K_("bbs--Public").showError( "您访问的页面不存在！" )
		response.end
	end sub
end Class
%>