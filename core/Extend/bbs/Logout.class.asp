<%
'''退出控制器
Class Logout 'Extends bbs--Common
	''退出操作
	sub index
		dim dict,ReadThreadID
		

		
		'将数据库中的相关字段值进行设置
		if S_("adminId") <> "" then
			'邮件提醒退出
			if guestConfig.LogoutReminded - 0 <> 0 then
				K_("bbs--mail").Logout
			end if		
		
		
			set dict = D_
			dict("Online") = 0
			if checkWap() then
				dict("Waprand") = ""
			else
				dict("Adminrand") = ""
			end if

			Call B_( "user" ).where( "UserID = " & id  ).setField( fieldName ,fieldValue )
		end if
		
		'清空Session
		Session.Contents.RemoveAll()
		Session.Abandon
		
		ReadThreadID = Request.Cookies("ReadThreadID")
		
		'清空Cookie
		P_("cookie").clear
		Call K_("bbs--Public").setCookie( "ReadThreadID" , ReadThreadID )
		Call K_("bbs--Public").setCookie( "SystemTemplate" , guestConfig.DefaultTemplate )
		
		'跳转到首页
		response.redirect POP_MVC.vars("HTTP_REFERER")
	end sub	
end Class
%>