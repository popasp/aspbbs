<%
Class Mail
	private siteUrl,baseTable
	
	Private sub class_initialize
		siteUrl = iif(is_localhost,POP_MVC.http_host() ,POP_MVC.http_host2() ) & POP_MVC.config("sitePath")
		if right( siteUrl , 1 ) = "/" then
			siteUrl = left( siteUrl , len(siteUrl) - 1 )
		end if
		baseTable = objCMS.baseTable
	end sub

	function getBodyHeader( str )
		getBodyHeader = "<p>您的网站 <a href="""& siteUrl &""">"& siteUrl &"</a> " & str & "</p>"
	end function
	
	'开启论坛邮箱
	Public Property Get OpenSiteStatus( MailAlert )
		dim body,title,url
		title =  S_("GroupName") & ": " & S_("Nickname") & " " & iif( checkWap() , "手机", "电脑" ) & "关闭论坛"
	
		url = objCMS.RealLinkPrefix & "Site_Status_" & MailAlert
	
		body  = getBodyHeader("被关闭")
		body = body & "<p>" & S_("GroupName") & ": <b>" & S_("Nickname") & "</b> 关闭论坛" &  "<br />"
		body = body & "若要重新开启，请点击 <a href='" & url & "'>" & url & "</a>" 
		
		body = body & "</p>"
		
		Call K_("bbs--Plugin").sendmail( guestConfig.MessageAlertsEmail , guestConfig.SiteName & " " & title , body )
		OpenSiteStatus = body
	End Property
	
	'登入时邮箱提醒
	Public Property Get SendMessage( id )
		if guestConfig.MessageSendReminded - 0 <> 1 then
			Exit Property
		end if
		dim body,title,url,rsMessage,rsFrom,rsTo,FromUser,ToUser
		set rsMessage = B_("self_GuestMessage").where(id).find

		set rsTo = B_( baseTable ).from("{prefix}User").where( rsMessage("ToUser") ).find
	
		if isNul(rsTo("Email")) or not isNul( rsTo("MailAlert") ) then
			Exit Property
		end if
				
		set rsFrom = B_( baseTable ).from("{prefix}User").where( rsMessage("FromUser") ).find		
		
		if rsFrom("UserID") = rsTo("UserID") then
			FromUser = "您"
		else
			FromUser = iif(isNul(rsFrom("NickName")) ,rsFrom("LoginName"),rsFrom("NickName") )
		end if
		
		ToUser = iif(isNul(rsTo("NickName")) ,rsTo("LoginName"),rsTo("NickName") )		
		
		title =  FromUser & " 给您发了新消息:" & POP_MVC.String.Cut( rsMessage("Message") , 10 )
	
		body  =  getUserLink( rsFrom("UserID") , FromUser ) & " 给 " & getUserLink( rsTo("UserID") , "您(" & ToUser & ")" )  & " 发了新的消息(" & POP_MVC.FormatDate( rsMessage("AddTime") , "YYYY-MM-DD HH:II:SS" ) & "):"
		body = body & "<p>" & rsMessage("Message") & "</p>"
		
		Call K_("bbs--Plugin").sendmail( guestConfig.MessageAlertsEmail , guestConfig.SiteName & " " & title , body )
	End Property
	
	Function getUserLink( id , UserName )
		getUserLink = "<a href='" & objCMS.RealLinkPrefix  & "u_" & id & "'>" & UserName &  "</a>"
	End Function
	

	'登入时邮箱提醒
	Public Property Get Login( stype )
		on error resume next
		dim body,title,str
		
		title =  S_("GroupName") & ": " & S_("Nickname") & " , "
		body  = getBodyHeader("有用户登入")
		body  = body & "<p>" & S_("GroupName") & ": <b>" & S_("Nickname") & "</b> , "
		select case stype
			case "userpass"
				str = "帐号密码登入"
			case "qqreglogin"
				str = "QQ注册并登入"
			case "weiboreglogin"
				str = "微博注册并登入"
			case "qqlogin"
				str = "QQ登入" 
			case "weibologin"
				str = "微博登入" 
			case "cookie"
				str = "cookie登入"
			case "reglogin"
				str = "注册并登入"
		end select	
		str = "使用" & iif(checkWap(),"手机" , "电脑") & str
		title = title & str
		body  = body & str &  "<br />"
		body  = body & "IP:" & get_client_ip()
		body = body & "<img src='" & iif( P_("POPASP_AUTOVALIDATE").regex( S_("Avatar") , "url" ) , S_("Avatar") , POP_MVC.http_host & "/" & POP_MVC.ltrim(S_("Avatar") , "/") ) & "' />" 
		
		body = body & "</p>"
		
		Call K_("bbs--Plugin").sendmail( guestConfig.MessageAlertsEmail , guestConfig.SiteName & " " & title, body )
	End Property

	'登出时邮箱提醒
	Public Property Get Logout( )
		dim body,title 
		title =  S_("GroupName") & ": " & S_("Nickname") & " " & iif( checkWap() , "手机", "电脑" ) & "登出"
	
		body  = getBodyHeader("有用户登出")
		body = body & "<p>" & S_("GroupName") & ": <b>" & S_("Nickname") & "</b> 登出" &  "<br />"
		body = body & "<img src='" & iif( P_("POPASP_AUTOVALIDATE").regex( S_("Avatar") , "url" ) , S_("Avatar") , POP_MVC.http_host & "/" & POP_MVC.ltrim(S_("Avatar") , "/") ) & "' />" 
		
		body = body & "</p>"
		
		Call K_("bbs--Plugin").sendmail( guestConfig.MessageAlertsEmail , guestConfig.SiteName & " " & title , body )
	End Property
	
	'修改帖子的参数均从form中获取
	Public Property Get EditThread( byval TopicID)	
		dim title,content,id
		dim subject,body,rs
		if is_numeric( TopicID ) then
			set rs = B_("self_GuestTopic").where( TopicID ).field("Title,Content").find
			if rs.eof then
				POP_MVC.unset( rs )
				Exit Property
			else
				title = rs("Title")
				content = rs("Content")
				id = TopicID
				POP_MVC.unset( rs )
			end if
		else
			title = request.form("Title")
			content = request.form("Content")
			id = request.form("TopicID")
		end if
		subject =  S_("GroupName") & ": " & S_("Nickname") & " 修改了帖子:" & title
		body = getBodyHeader("修改了帖子")
		body = body & "<p>" & S_("GroupName") & ": " & S_("Nickname") & " 用" & iif( checkWap() , "手机", "电脑" ) & " 修改了帖子:" & "<a href='" & objCMS.RealLinkPrefix  & id & "'>" & title & "</a></p>"
		body = body & "<hr />"
		body = body & addHttpHost4img(content)
		Call K_("bbs--Plugin").sendmail( guestConfig.MessageAlertsEmail , subject , body )
	End Property
	
	'添加帖子的参数均从form中获取
	Public Property Get AddThread( byval TopicID)	
		dim title,content,id
		dim subject,body,rs
		id = TopicID
		if POP_MVC.isPost then
			title = request.form("Title")
			content = request.form("Content")
		else
			set rs = B_("self_GuestTopic").where( TopicID ).field("Title,Content").find
			if rs.eof then
				POP_MVC.unset( rs )
				Exit Property
			else
				title = rs("Title")
				content = rs("Content")				
				POP_MVC.unset( rs )
			end if
		end if
		subject =  S_("GroupName") & ": " & S_("Nickname") & " 添加了帖子:" & title
		body = getBodyHeader("添加了新帖")
		body = body & "<p>" & S_("GroupName") & ": " & S_("Nickname") & " 用" & iif( checkWap() , "手机", "电脑" ) & " 添加了帖子:" & "<a href='" & objCMS.RealLinkPrefix   & id & "'>" & title & "</a></p>"
		body = body & "<hr />"
		body = body & addHttpHost4img(content)
		Call K_("bbs--Plugin").sendmail( guestConfig.MessageAlertsEmail , subject , body )
	End Property
	
	'添加帖子的参数均从form中获取
	Public Property Get AddFaq( byval FaqID)	
		dim title,content,id,Contact
		dim subject,body,rs
		id = FaqID
		
		if POP_MVC.isPost then
			content = request.form("Content")
			Contact = request.form("Contact")
			title = POP_MVC.String.Cut( content , 30 )
		else
			set rs = B_("self_Guestbook").where( FaqID ).field("FaqID,Content,Contact").find
			
			if rs.eof then
				POP_MVC.unset( rs )
				Exit Property
			else
				title = POP_MVC.String.Cut( rs("Content") , 30 )
				content = rs("Content")		
				Contact = rs("Contact")
				POP_MVC.unset( rs )
			end if
		end if

		subject =  "留言:" & title
		body = ""
		body = body & "<p>" & Contact & " 用" & iif( checkWap() , "手机", "电脑" ) & " 添加了帖子:" & "<a href='" & objCMS.RealLinkPrefix   & id & "'>" & title & "</a></p>"
		body = body & "<hr />"
		body = body & addHttpHost4img(content)
		Call K_("bbs--Plugin").sendmail( guestConfig.MessageAlertsEmail , subject , body )
	End Property
	
	'添加回帖时发送邮件
	Public Property Get AddReply( ReplyID )	
		dim title,content,TopicID,rs
		dim subject,body,page,url,where
		
		set where = D_
		where("a.TopicID = b.TopicID") = null
		where("a.ReplyID") = ReplyID
		set rs = B_(objCMS.baseTable).from("{prefix}self_GuestReply as a,{prefix}self_GuestTopic as b").field("a.TopicID,a.Content,b.Title").where(where).find
		
		if rs.eof then
			POP_MVC.unset(rs)
			Exit Property
		end if
		
		title = rs("Title")
		content = rs("Content")
		TopicID = rs("TopicID")
		page = request.form("page")
		
		if isNul(page) OR page = 0 then
			url = TopicID  & "#item-" & ReplyID
		else
			url = TopicID & "_" & page & "#item-" & ReplyID
		end if			
		
		subject =  S_("GroupName") & ": " & S_("Nickname") & " 回答了《" & title & "》"
		body = getBodyHeader("有新的回复")
		body = "<p>" & S_("GroupName") & ": " & S_("Nickname") & " 用" & iif( checkWap() , "手机", "电脑" ) & " 对 " & "<a href='" & objCMS.RealLinkPrefix  & url & "'>" & title & "</a> 主题进行了回复</p>"
		body = body & "<hr />"
		
		'将图片地址转成真实地址
		body = body & addHttpHost4img(content)
		
		POP_MVC.unset(rs)
		
		'发送邮件
		Call K_("bbs--Plugin").sendmail( guestConfig.MessageAlertsEmail , subject , body )
	End Property
	
	'修改回帖时发送邮件
	Public Property Get EditReply( ReplyID )	
		dim title,content,TopicID,rs
		dim subject,body,page,url,where
		
		set where = D_
		where("a.TopicID = b.TopicID") = null
		where("a.ReplyID") = ReplyID
		set rs = B_(objCMS.baseTable).from("{prefix}self_GuestReply as a,{prefix}self_GuestTopic as b").field("a.TopicID,a.Content,b.Title").where(where).find
		
		if rs.eof then
			POP_MVC.unset(rs)
			Exit Property
		end if
		
		title = rs("Title")
		content = rs("Content")
		TopicID = rs("TopicID")
		url = TopicID		
		
		subject =  S_("GroupName") & ": " & S_("Nickname") & " 修改了《" & title & "》的回帖"
		body = getBodyHeader("修改了帖子")
		body = body & "<p>" & S_("GroupName") & ": " & S_("Nickname") & " 用" & iif( checkWap() , "手机", "电脑" ) & " 修改了 " & "<a href='" & objCMS.RealLinkPrefix  & url & "'>" & title & "</a> 的回帖</p>"
		body = body & "<hr />"
		
		'将图片地址转成真实地址
		body = body & addHttpHost4img(content)
		
		'发送邮件
		Call K_("bbs--Plugin").sendmail( guestConfig.MessageAlertsEmail , subject , body )
	End Property
	
  Public Function AddHttpHost4Img(ByVal str)
	Dim s_rule, Matches, match, s_img, s_src,httpHost
	httpHost = siteUrl
	httpHost = POP_MVC.Rtrim(httpHost , "/") & "/"
	'匹配img标签的正则
	s_rule = "<img[^>]*?\s+src\s*=\s*((?:"")([^""]+)(?:"")|(?:')([^']+)(?:')|([^\s>]+))[^>]*>"
	If POP_MVC.String.reg_test(str, s_rule , "gim") Then
	  '正则匹配所有的img标签
	  Set Matches = POP_MVC.String.reg_exec(str, s_rule , "gim")
	  '取出每个img标签
	  For Each match In Matches
		'取出图片标签
		s_img = match.Value
		'取出图片地址
		s_src = match.SubMatches(0)	
		
		'更新标签中的src地址
		if not P_("POPASP_AUTOVALIDATE").regex( s_src , "url" ) then
			s_src = POP_MVC.Trim(POP_MVC.Trim(s_src,""""),"'")
			if NOT POP_MVC.String.iExists( s_src , C_("NO_FLOW_STR") ) then
				s_img = replace( s_img, s_src , httpHost & POP_MVC.Ltrim(s_src , "/") )
				str = replace( str,  match.value, match.value & "<br />图片链接：<a href='" & httpHost & POP_MVC.Ltrim(s_src , "/") & "'>" & httpHost & POP_MVC.Ltrim(s_src , "/") & "</a><br />" )
				str = replace( str , match.Value , s_img )
			end if
		end if
		
	  Next
	End If
	AddHttpHost4Img = str
  End Function
End Class
%>