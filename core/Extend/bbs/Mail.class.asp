<%
Class Mail  'Extends bbs--Common
	sub initialize
		if not that.isSelfOrigin then
			that.error( array("非法操作" , "window.close") )
		end if
	end sub
	
	private function getMailType(mailType)
		getMailType = K_("bbs--Plugin").getMailType(mailType)
		if getMailType = "" then
			that.error( Array( "非法操作" ))
		end if
	End function	
	
	'切换邮件发送方式
	sub SwitchType
		dim stype,sTip
		stype = POP_MVC.req("type")
		Call B_( "self_GuestConfig" ).where( that.dict("ConfigName","mail_type") ).setField( "ConfigValue",stype )
		Call B_("self_GuestConfig").field("ConfigName as K,ConfigValue as V").SaveXmlFile( "Line" , C_("XML_CONFIG_PATH") )
		sTip = "邮件发送方式已切换到" & getMailType(stype)
		Call K_( "bbs--Public" ).CtrlLog( 1, "用户 ID: " & S_("adminId") & " " & sTip  , sTip )
	end sub
	
	'测试邮件发送方式
	sub MailTest
		dim stype,sTip
		stype =  POP_MVC.req("type")
		Call K_("bbs--Plugin").TestSendMail(stype, "")
		sTip = "已经使用 " & getMailType(stype) & " 发送测试邮件"
		Call K_( "bbs--Public" ).CtrlLog( 1, "用户 ID: " & S_("adminId") & " " & sTip  , sTip )
	end sub
end Class
%>