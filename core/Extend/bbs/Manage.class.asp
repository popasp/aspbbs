<%
Class Manage 'Extends bbs--Common
	private tableName
	sub initialize
that.AjaxArgs("info") = "msg"
that.ajaxSuccessStatus = 0
that.ajaxErrorStatus = 1
		
		if S_("IsAdmin") <> 1 then
			that.error("您无权查看该页面")
		end if
		
		tableName = "User"
		that.d("layuicachepage") = "user"
	end sub
	
	'设置IP
	sub setIP
		dim where,IpField,IpAddrField,data,arr
		
		that.d("fromTable") = "帖子表"
		IpField = "IP"
		IpAddrField = "IpAddr"
		
		set where = D_
		
		'sqlite3无isnull函数
		if POP_MVC.CONFIG("DB_TYPE") = "sqlite3" then
			where( IpField & " <> '' and NOT " & IpField & " IS NULL" ) = null
			where( "( " & IpAddrField & " = '' OR " & IpAddrField & " IS NULL )" ) = null
		else
			where( IpField & " <> '' and NOT ISNULL( " & IpField & " )" ) = null
			where( "( " & IpAddrField & " = '' OR ISNULL(" & IpAddrField & ") )" ) = null
		end if
		
		arr = B_("self_GuestTopic").where(where).field( Array( IpField , "COUNT(" & IpField & ") as cnt" ) ).group( IpField ).top(100).get1arr		
		
		
		if ubound( arr ) < 0 then 		
			IpField = "EditIP"
			IpAddrField = "EditIpAddr"
			
			set where = D_		
			'sqlite3无isnull函数
			if POP_MVC.CONFIG("DB_TYPE") = "sqlite3" then
				where( IpField & " <> '' and NOT " & IpField & " IS NULL" ) = null
				where( "( " & IpAddrField & " = '' OR " & IpAddrField & " IS NULL )" ) = null
			else
				where( IpField & " <> '' and NOT ISNULL( " & IpField & " )" ) = null
				where( "( " & IpAddrField & " = '' OR ISNULL(" & IpAddrField & ") )" ) = null
			end if
			
			arr = B_("self_GuestTopic").where(where).field( Array( IpField , "COUNT(" & IpField & ") as cnt" ) ).group( IpField ).top(100).get1arr		
		end if
		
		
		if ubound( arr ) < 0 then 	
			that.d("fromTable") = "回帖表"
			IpField = "IP"
			IpAddrField = "IpAddr"
			
			set where = D_
			
			'sqlite3无isnull函数
			if POP_MVC.CONFIG("DB_TYPE") = "sqlite3" then
				where( IpField & " <> '' and NOT " & IpField & " IS NULL" ) = null
				where( "( " & IpAddrField & " = '' OR " & IpAddrField & " IS NULL )" ) = null
			else
				where( IpField & " <> '' and NOT ISNULL( " & IpField & " )" ) = null
				where( "( " & IpAddrField & " = '' OR ISNULL(" & IpAddrField & ") )" ) = null
			end if
			
			arr = B_("self_GuestReply").where(where).field( Array( IpField , "COUNT(" & IpField & ") as cnt" ) ).group( IpField ).top(100).get1arr			
		end if
		
		if ubound( arr ) < 0 then 	
			that.d("fromTable") = "回帖表"
			IpField = "EditIP"
			IpAddrField = "EditIpAddr"
			
			set where = D_
			
			'sqlite3无isnull函数
			if POP_MVC.CONFIG("DB_TYPE") = "sqlite3" then
				where( IpField & " <> '' and NOT " & IpField & " IS NULL" ) = null
				where( "( " & IpAddrField & " = '' OR " & IpAddrField & " IS NULL )" ) = null
			else
				where( IpField & " <> '' and NOT ISNULL( " & IpField & " )" ) = null
				where( "( " & IpAddrField & " = '' OR ISNULL(" & IpAddrField & ") )" ) = null
			end if
			
			arr = B_("self_GuestReply").where(where).field( Array( IpField , "COUNT(" & IpField & ") as cnt" ) ).group( IpField ).top(100).get1arr			
		end if
		
		that.d("topicRS") = arr
		
		that.d("IpApi") =  "https://whois.pconline.com.cn/ipJson.jsp?ip="	
		objCMS.BbsShow("manage/ip")
	end sub
	
	'论坛配置
	sub Configs
		dim arr
		arr = B_("self_GuestConfigGroup").top(9).field("GroupID").order("GroupOrder asc").get1arr
		arr = join(arr,",")
		Call SetConfig(arr)
	End Sub
	
	' 删除用户信息及他的所有发布、回复、消息
	sub removeUser
		dim id,authInfo
		
		id = that.req("id")
		
		'来路不对
		if not that.isSelfOrigin then
			that.error("非法操作")
		end if	
		
		authInfo = K_("bbs--Public").AuthCompare( id , "的用户进行删除操作！" )		
		
		if authInfo <> "" then
			Call K_( "bbs--Public" ).CtrlLog( 0, authInfo , authInfo )
		end if
		
		Call B_("User").where(id).remove
		Call B_("self_GuestTopic").where( "UserID = " & id ).remove
		Call B_("self_GuestReply").where( "UserID = " & id ).remove
		Call B_("self_GuestMessage").where( "FromUser = " & id ).remove
		Call B_("self_GuestMessage").where( "ToUser = " & id ).remove
		
		
		sTip = "用户成功删除！"
		Call K_("bbs--Public").CtrlLog( 1 ,  "用户 ID:" &  S_("adminId") & sTip  , sTip )	
	end sub
	
	'网站设置
	sub Configs2
		dim arr
		arr = B_("self_GuestConfigGroup").top(8).field("GroupID").order("GroupOrder Desc").get1arr
		arr = join(arr,",")
		Call SetConfig(arr)
	End Sub
	
	'取反状态字段值
	sub setStatusField
		dim id,StatusField,authInfo,FieldValue
		id = POP_MVC.req("id")
		StatusField = POP_MVC.req("StatusField")
		FieldValue = POP_MVC.req("FieldValue")
		
		authInfo = K_("bbs--Public").AuthCompare( id , "进行该项操作！" )
		
		if authInfo <> "" then
			that.error( authInfo )
		end if
		Call B_(tableName).where(id).setNot( StatusField )
		Call K_( "bbs--Public" ).CtrlLog( 1, "配置: " & StatusField & " 取反" , "设置成功！" )
	end sub
	
	'将状态字段值设为1
	sub OpenStatusField
		dim id,StatusField
		id = POP_MVC.req("id")
		StatusField = POP_MVC.req("StatusField")
		Call objCMS.setUserData( id  , StatusField , 1 )
		Call K_( "bbs--Public" ).CtrlLog( 1,  "设置用户 ID: " & id & " 的字段 " & StatusField & " = 1" , "设置成功！" )
	end sub
	
	'将状态字段值设为0
	sub CloseStatusField
		dim id,StatusField
		id = POP_MVC.req("id")
		StatusField = POP_MVC.req("StatusField")
		Call objCMS.setUserData( id  , StatusField , 0 )
		Call K_( "bbs--Public" ).CtrlLog( 1,  "设置用户 ID: " & id & " 的字段 " & StatusField & " = 0" , "设置成功！" )
	end sub
	
	sub SuperAdminPower
		if S_("GroupID") <> 1 then	
			that.error("非超级管理员无权操作！")	
		end if
	End Sub
	
	'设为管理员
	sub OpenAdmin
		Call SetAdmin( "GroupID" , 2 )
		that.success("设置成功！")
	end sub
	
	'取消管理员
	sub CloseAdmin
		Call SetAdmin( "GroupID" , 3 )
		that.success("设置成功！")
	end sub
	
	'重新计算回复次数，即校正回复数
	Sub RecountReply
		Call SuperAdminPower
		dim rs,sql,cnt,sTip
		
		sql = B_( objCMS.baseTable ).onlysql(1).field("a.TopicID,a.ReplyCount,( select count(TopicID) from iaspcms_self_GuestReply as b where a.TopicID = b.TopicID  ) as cnt").from("{prefix}self_GuestTopic as a").select
		
		set rs = B_( objCMS.baseTable ).onlysql(0).from( "(" & sql & ") as c" ).field("c.TopicID , c.ReplyCount , c.cnt").where("c.ReplyCount <> c.cnt").select
		
		do while not rs.eof
			Call B_("self_GuestTopic").where( rs("TopicID") ).setField( "ReplyCount" , CStr(rs("cnt")) )
			rs.moveNext
		loop	

		cnt = rs.RecordCount
		that.u(rs)
		if cnt > 0 then
			objCMS.clearCache
			sTip = "共 " & cnt & " 个帖子的回复数校正完成！"
		else
			sTip = "回复数全部正确，无需校正！"
		end if
		Call K_( "bbs--Public" ).CtrlLog( 1, "重新计算回复次数，" & sTip , sTip )
	End Sub
	
	'重新计算楼层，即校正楼层
	Sub RecountFloor
		Call SuperAdminPower
		dim rs,cnt,sTip,rs2,floor
		
		cnt = 0
		set where = D_
		
		where("ReplyCount") = Array( ">" , 0 )
		
		set rs = B_( objCMS.baseTable ).field("TopicID,ReplyCount").where(where).from("{prefix}self_GuestTopic").select

		do while not rs.eof
			floor = 1
			set rs2 = B_( objCMS.baseTable ).field("ReplyID,FloorStep").where( "TopicID = " & rs("TopicID") ).from("{prefix}self_GuestReply").select
			do while not rs2.eof
				if isNul(rs2("FloorStep")) or rs2("FloorStep") <> floor then
					Call B_("self_GuestReply").where( rs2("ReplyID") ).setField( "FloorStep" , floor )
					cnt = cnt + 1
				end if
				floor = floor + 1
				rs2.moveNext				
			loop
			that.u( rs2 )
			rs.moveNext
		loop	

		that.u(rs)
		if cnt > 0 then
			objCMS.clearCache
			sTip = "共 " & cnt & " 个回复的楼层校正完成！"
		else
			sTip = "回复楼层全部正确，无需校正！"
		end if
		Call K_( "bbs--Public" ).CtrlLog( 1, "重新计算楼层，" & sTip , sTip )
	End Sub
	
	'填充IndexImage为空字段
	'<V1.6时，IndexImage从内容中动态提取得到，耗时耗内存
	Function FillIndexImage( table,ContentField,ImageField , reload )
		dim rs,where,prikey,IndexImage,cnt
		
		set where = POP_MVC.SCD
		
		where(  "INSTR( " & ContentField & " , '<img' ) > 0" ) = null
		
		'不重新计算时，不需要此条件
		if reload = 0 then		
			'sqlite3无isnull函数
			if POP_MVC.CONFIG("DB_TYPE") = "sqlite3" then
				where( ImageField & " = '' OR ISNULL( " & ImageField & " )" ) = null				
			else
				where( ImageField & " = '' OR " & ImageField & " IS NULL" ) = null
			end if
		end if
		
		prikey = B_( table ).getPK
	
		set rs = B_( table ).where( where ).field( Array( prikey, ContentField  ) ).select
		
		cnt = 0 

		do while not rs.eof
			IndexImage = objCMS.fetch_img( rs( ContentField ) )		
			if IndexImage <> "" then
				cnt = cnt + 1
				Call B_( table ).where( rs( prikey ) ).setField( ImageField , IndexImage )
			end if
			rs.moveNext
		loop
		FillIndexImage = rs.RecordCount
		that.u(rs)
	End Function
	

	
	sub doSetIp
		dim dict,i,where,ip,addr
		set dict = POP_MVC.form2dict

		for i = 1 to dict.count/2
			ip = dict("Ip" & i)
			addr =  trim(dict( "IpAddr" & i ))
			if addr <> "" then
				Call B_( "self_GuestTopic" ).where( "IP = '" & ip & "'" ).setField( "IpAddr" , addr )
				Call B_( "self_GuestTopic" ).where( "EditIP = '" & ip & "'" ).setField( "EditIpAddr" , addr )
				Call B_( "self_GuestReply" ).where( "IP = '" & ip & "'" ).setField( "IpAddr" , addr )
				Call B_( "self_GuestReply" ).where( "EditIP = '" & ip & "'" ).setField( "EditIpAddr" , addr )	
			end if
		next
		Call K_( "bbs--Public" ).CtrlLog( 1, "操作完成" , "操作完成" )
	end sub	

	
	'填充数据表IP地址对应的IP属地字段
	'table 表名
	'IpField IP字段名
	'IpAddrField IP属地字段名
	'reload 是否重新计算，否则只填为空的，1/0
	'isApi 是否直接使用api取值，1/0
	Function FillTableIpAddr( table,IpField,IpAddrField ,reload , isApi )
		dim rs,where,addr,where2
		
		set where = POP_MVC.SCD
		
		'sqlite3无isnull函数
		if POP_MVC.CONFIG("DB_TYPE") = "sqlite3" then
			where( IpField & " <> '' and NOT " & IpField & " IS NULL" ) = null
		else
			where( IpField & " <> '' and NOT ISNULL( " & IpField & " )" ) = null
		end if
		
		'不重新计算时，不需要此条件
		if reload = 0 then		
			'sqlite3无isnull函数
			if POP_MVC.CONFIG("DB_TYPE") = "sqlite3" then
				where( "( " & IpAddrField & " = '' OR " & IpAddrField & " IS NULL )" ) = null		
			else
				where( "( " & IpAddrField & " = '' OR ISNULL(" & IpAddrField & ") )" ) = null
			end if
		end if
	
		set rs = B_( table ).where(where).field( IpField ).group( IpField ).select
		
		set where2 = POP_MVC.SCD
		
		'不重新计算时，不需要此条件
		if reload = 0 then
			'sqlite3无isnull函数
			if POP_MVC.CONFIG("DB_TYPE") = "sqlite3" then
				where2( "( " & IpAddrField & "= '' OR " & IpAddrField & " IS NULL )" ) = null
			else				
				where2( "( " & IpAddrField & "= '' OR ISNULL( " & IpAddrField & " ) )" ) = null
			end if
		end if

		do while not rs.eof
			if isApi = 1 then
				addr = K_( "bbs--IP" ).getTQQWryAddr( rs( IpField ) )
			else
				addr = M_( "bbs--Thread" ).getIpAddr( rs( IpField ) )
			end if
			where2( IpField ) = rs( IpField )		
			Call B_( table ).where( where2 ).setField( IpAddrField , addr )
			rs.moveNext
		loop
		FillTableIpAddr = rs.RecordCount
		that.u(rs)
	End Function
	
	'填写空IP属地
	Sub FillBlankIpAddr
		Call SuperAdminPower
		dim rs,cnt,where,IpField,AttrField,addr,where2,sTip
		
		cnt = 0

		cnt = cnt + FillTableIpAddr( "self_GuestTopic", "IP" , "IpAddr" , 0 , 0)		
		cnt = cnt + FillTableIpAddr( "self_GuestTopic", "EditIP" , "EditIpAddr" , 0 , 0 )
		cnt = cnt + FillTableIpAddr( "self_GuestReply", "IP" , "IpAddr" ,0 , 0)
		cnt = cnt + FillTableIpAddr( "self_GuestReply", "EditIP" , "EditIpAddr" , 0 , 0 )
		
		if cnt > 0 then
			objCMS.clearCache
			sTip = "共 " & cnt & " 个IP属地填充完成！"
		else
			sTip = "IP属地全部填充，不需重新填写！"
		end if
		Call K_( "bbs--Public" ).CtrlLog( 1, "填写空IP属地，" & sTip , sTip )
	End Sub	
	
	Sub FillBlankImage
		dim rs,cnt,where,IpField,AttrField,addr,where2,sTip
		cnt = 0

		cnt = cnt + FillIndexImage( "self_GuestTopic", "Content" , "IndexImage" , 0 )		
		cnt = cnt + FillIndexImage( "self_GuestReply", "Content" , "IndexImage" , 0 )
		
		if cnt > 0 then
			objCMS.clearCache
			sTip = "共 " & cnt & " 个图片填充完成！"
		else
			sTip = "图片全部填充，不需重新填写！"
		end if
		Call K_( "bbs--Public" ).CtrlLog( 1, "填写空IP属地，" & sTip , sTip )
	End Sub
	
	'重新填写IP属地
	Sub ReFillIpAddr
		Call SuperAdminPower
		dim rs,cnt,where,IpField,AttrField,addr,where2,sTip
		
		cnt = 0
		cnt = cnt + FillTableIpAddr( "self_GuestTopic", "IP" , "IpAddr" , 1 , 1 )
		cnt = cnt + FillTableIpAddr( "self_GuestTopic", "EditIP" , "EditIpAddr" , 1 , 0 )
		cnt = cnt + FillTableIpAddr( "self_GuestReply", "IP" , "IpAddr" , 1 , 0 )
		cnt = cnt + FillTableIpAddr( "self_GuestReply", "EditIP" , "EditIpAddr" , 1 , 0 )
		
		if cnt > 0 then
			objCMS.clearCache
			sTip = "共 " & cnt & " 个IP属地填充完成！"			
		else
			sTip = "IP属地全部填充，不需重新填写！"
		end if	
		Call K_( "bbs--Public" ).CtrlLog( 1, sTip , sTip )
	End Sub	

	
	'设置或取消管理员
	'GroupID 2为管理员；3为普通会员。
	Private Sub SetAdmin( field , value )
		Call SuperAdminPower
		dim id,IsAdmin
		id = POP_MVC.req("id")
		IsAdmin = POP_MVC.req("IsAdmin")
		Call objCMS.setUserData( id  , field , value )	
		objCMS.clearCache
		
		if not K_("CtrlLog--Public") is nothing then
			Call K_("CtrlLog--Public").DoLog( 1 , "用户 ID:" &  id & " 设置字段 IsAdmin = " & IsAdmin , "" )
		end if
	End Sub

	'修改配置值
	sub doEditConfigs
		Call SuperAdminPower
		dim key,dict,MailAlert
		set dict = POP_MVC.form2dict
		'set dict = K_("bbs--Plugin").getAjaxData

		if dict.Exists("SiteStatus") then
			if dict("SiteStatus") = "0" then
				MailAlert = POP_MVC.String.uniqid
				Call B_("User").where("LoginName = 'admin'").setField( "MailAlert" , MailAlert )
				Call K_("bbs--mail").OpenSiteStatus( MailAlert)
				Call POP_MVC.file_put_contents( POP_MVC.config("CORE_NAME") & "/closebbs.txt" , date() )
			end if
		end if

		if dict.Exists("DefaultTemplate") then
			if dict("DefaultTemplate") <> guestConfig.DefaultTemplate then
				Call K_("bbs--system").ModifyConfig("configs" , "DefaultTemplate" , dict("DefaultTemplate") )
			end if
		end if

		
		for each key in dict
			Call B_("self_GuestConfig").where( that.dict( "ConfigName" , key  ) ).setField( "ConfigValue",dict(key) )
		next

		if dict("QQ_APP_ID") <> "" then
		'修改QQ登陆配置
		Call doLoginQQEdit( dict("QQ_APP_ID") , dict("QQ_APP_KEY") , dict("QQ_CALLBACL_URL") )	
		end if

		if dict("WEIBO_APP_KEY") <> "" then
		'修改新浪登陆配置
		Call doLoginWeiboEdit( dict("WEIBO_APP_KEY") , dict("WEIBO_APP_SECRET") , dict("WEIBO_CALLBACK_URL") )	
		end if
		Call B_("self_GuestConfig").field("ConfigName as K,ConfigValue as V").SaveXmlFile( "Line" , C_("XML_CONFIG_PATH") )
		objCMS.clearCache
		if POP_MVC.req("keyword") <> "" then
			Call K_( "bbs--Public" ).CtrlLog( 1, "用户ID: " & S_("adminId") & " ，修改了配置" , Array("保存成功","?c=manage&a=search&keyword=" & POP_MVC.req("keyword")  ) )
		else
			Call K_( "bbs--Public" ).CtrlLog( 1, "用户ID: " & S_("adminId") & " ，修改了配置" , Array("保存成功","?" & that.form("reffer") & "#Group-" & that.form("GroupID")  ) )
		end if
	end sub

	'上传图片
	sub uploads
		dim filename,logID
		POP_MVC.config("UPLOAD_ALLOW_TYPES") = "jpg;jpeg;png;gif;bmp;pcx;svg"
		filename = K_("bbs--Public").upload( "Manage-uploads" ,logID )
		if filename <> "" then
			that.success( filename )
		else
			that.error( POP_MVC.Uploader.description )
		end if
	end sub
	
	'上传文件
	sub uploadFile
		dim filename,logID
		POP_MVC.config("UPLOAD_ALLOW_TYPES")  = "flv;swf;mkv;avi;rm;rmvb;mpeg;mpg;ogg;ogv;mov;wmv;mp4;webm;mp3;wav;mid;rar;zip;tar;gz;7z;bz2;cab;iso;doc;docx;xls;xlsx;ppt;pptx;pdf;txt;md;xml;wma;ico;asp;html;htm"
		filename = K_("bbs--Public").upload( "Manage-uploads" ,logID )
		if filename <> "" then
			that.success( filename )
		else
			that.error( POP_MVC.Uploader.description )
		end if
	end sub
	
	'网站配置查询
	sub search
		dim obj,templatePath,keywords,sTip
		dim compiler,rs,where,rs2,configRS,id,value
		
		'post时		
		if that.isPost and that.form("ConfigID") <> "" then	
			Call SuperAdminPower
			id = that.form("ConfigID")
			set where = D_
			where( "ConfigID" ) = id
			where( "ConfigInput" ) = "select"
			value = POP_MVC.String.decodeHtml(POP_MVC.form("Content"))
			Call B_("self_GuestConfig").where(id).setField( "SelectOptions" , value )
			
			Call K_( "bbs--Public" ).CtrlLog( 1, "用户ID: " & S_("adminId") & " ，选项值修改成功！" , Array( "选项值修改成功！"  , "?Manage_Search_" & id & guestConfig.PageSuffix  & "#" & "select" ) )
		end if
		
		Call objCMS.clearCompile
		
		'如果搜索关键字不为空，则进行取值显示
		keywords = POP_MVC.req("keyword")
		if keywords <> "" then		
			set where = D_
			where("a.GroupID = b.GroupID") = null
			if not isNul( GroupID ) then
				where("b.GroupID") = Array( "IN" ,GroupID )
			end if
			set rs = B_("self_GuestConfig").from("{prefix}self_GuestConfigGroup as a,{prefix}self_GuestConfig as b").field("a.GroupID,a.GroupName,b.ConfigID,b.ConfigName,b.ConfigValue,b.ConfigTitle,b.ConfigDesc,b.SelectOptions,b.ConfigInput").where(where).order("a.GroupOrder ASC,a.GroupID ASC,b.ConfigID ASC").page("1,500").search(  keywords , "ConfigTitle,ConfigValue,GroupName" )
			
			that.d("rs") = rs		
			that.d("UserGroupRS") = B_("UserGroup").field("GroupID,GroupName").order("GroupOrder ASC").select		
		end if
		
		'显示所有下拉列表的选项
		set where = D_
		where("a.GroupID = b.GroupID") = null
		where("ConfigInput") = "select"
		set rs2 = B_("self_GuestConfig").from("{prefix}self_GuestConfigGroup as a,{prefix}self_GuestConfig as b").field("a.GroupID,a.GroupName,b.ConfigID,b.ConfigName,b.ConfigValue,b.ConfigTitle,b.ConfigDesc,b.SelectOptions,b.ConfigInput").where(where).order("a.GroupOrder ASC,a.GroupID ASC,b.ConfigID ASC").page("1,500").select
		that.d("rs2") = rs2
		

		
		'对应配置中的ConfigID
		id = that.get("id")
		'如果指定了某项配置ID，则取值准备显示				
		if isNumeric(id) and id <> "" then
			set where = D_
			where("a.GroupID = b.GroupID") = null
			where("ConfigInput") = "select"
			where("ConfigID") = id
			set configRS = B_(objCMS.baseTable).from("{prefix}self_GuestConfigGroup as a,{prefix}self_GuestConfig as b").field("a.GroupID,a.GroupName,b.ConfigID,b.ConfigName,b.ConfigValue,b.ConfigTitle,b.ConfigDesc,b.SelectOptions,b.ConfigInput").where(where).order("a.GroupOrder ASC,a.GroupID ASC,b.ConfigID ASC").page("1,500").find
			
			if configRS.eof then
				that.u(configRS)
				sTip = "查找的 ID:" & id & " 配置不存在"
				Call K_( "bbs--Public" ).CtrlLog( 0, sTip ,  Array(sTip , "?Manage_Search" & guestConfig.PageSuffix & "#select" ) )
			end if
			
			that.d("conf") = configRS
		end if
	
		
		set obj = objCMS
		Call obj.clearCompile
		templatePath= "templates/bbs/html/manage/search.html"
		obj.load(templatePath)			
		
		' 使用模板引擎解析		
		set compiler = P_("TEMPLATE_COMPILER")
		compiler.content = obj.content		
		obj.content = compiler.compile
		
		obj.parseHtml
		obj.output
	end sub	
	
	'清除过程数据
	sub clearRuntime
		dim sTip
		
		objCMS.clearRuntime
		sTip = "过程数据清除成功!"
		Call K_( "bbs--Public" ).CtrlLog( 1, sTip , sTip )
	end sub
	
	'清除缓存
	sub clearCache
		dim cnt,sTip
		'cnt = objCMS.cacheCount
		objCMS.clearCache
		sTip = "缓存清除成功!"
		Call K_( "bbs--Public" ).CtrlLog( 1, sTip , sTip )
	end sub
	
	'清除内存缓存
	sub clearMemory
		dim cnt,sTip
		cnt = P_("APPLICATION").RemoveAll
		sTip = "内存缓存(" & cnt & "个)清除成功!"
		Call K_( "bbs--Public" ).CtrlLog( 1, sTip , sTip )
	end sub
	
	'网站配置
	Private sub SetConfig(GroupID)
		dim obj,templatePath	
		dim compiler,rs,where
		POP_MVC.config("APP_DEBUG") = 1			
		
		set where = D_
		where("a.GroupID = b.GroupID") = null
		if not isNul( GroupID ) then
			where("b.GroupID") = Array( "IN" ,GroupID )
		end if
		set rs = B_("self_GuestConfig").from("{prefix}self_GuestConfigGroup as a,{prefix}self_GuestConfig as b").field("a.GroupID,a.GroupName,b.ConfigID,b.ConfigName,b.ConfigValue,b.ConfigTitle,b.ConfigDesc,b.SelectOptions,b.ConfigInput").where(where).order("a.GroupOrder ASC,a.GroupID ASC,b.ConfigID ASC").page("1,500").select
		
		that.d("rs") = rs
		that.d("GroupRS") = B_("self_GuestConfigGroup").where( GroupID ).order("GroupOrder ASC").page("1,500").select
		
		that.d("UserGroupRS") = B_("UserGroup").field("GroupID,GroupName").order("GroupOrder ASC").select
		
		set obj = objCMS
		templatePath= "templates/bbs/html/manage/config.html"	
		obj.load(templatePath)	
		' 使用模板引擎解析		
		set compiler = P_("TEMPLATE_COMPILER")
		compiler.template = templatePath	
		compiler.content = obj.content		
		obj.content = compiler.compile	
		obj.parseHtml
		obj.output
	end sub		
	
	'修改API/qq/config.asp文件中的配置
	Private sub doLoginQQEdit( QQ_APP_ID , QQ_APP_KEY , QQ_CALLBACL_URL ) 
		dim path,content
		path = "./API/qq/config.asp"
		
		if POP_MVC.File.isFile(path) then
			content = POP_MVC.file_get_contents( path )
			content = POP_MVC.String.reg_replace2( content, "\bQQ\_APP\_ID\s*\=\s*("".*?"")" , "$1" ,"""" & QQ_APP_ID & """"  )		
			content = POP_MVC.String.reg_replace2( content, "\bQQ_APP_KEY\s*\=\s*("".*?"")" , "$1" ,"""" & QQ_APP_KEY & """"  )
			content = POP_MVC.String.reg_replace2( content, "\bQQ_CALLBACL_URL\s*\=\s*("".*?"")" , "$1" ,"""" & QQ_CALLBACL_URL & """"  )
			call POP_MVC.file_put_contents( path,content )		
			objCMS.clearCache
		end if
	End sub
	
	'修改API/sina/config.asp文件中的配置
	Private sub doLoginWeiboEdit( WEIBO_APP_KEY , WEIBO_APP_SECRET , WEIBO_CALLBACK_URL ) 
		dim path,content
		path = "./API/sina/config.asp"
		
		if POP_MVC.File.isFile(path) then
			content = POP_MVC.file_get_contents( path )
			content = POP_MVC.String.reg_replace2( content, "\bWEIBO_API_KEY\s*\=\s*("".*?"")" , "$1" ,"""" & WEIBO_APP_KEY & """"  )
			content = POP_MVC.String.reg_replace2( content, "\bWEIBO_SEKRET_KEY\s*\=\s*("".*?"")" , "$1" ,"""" & WEIBO_APP_SECRET  & """" )
			content = POP_MVC.String.reg_replace2( content, "\bWEIBO_CALLBACK_URL\s*\=\s*("".*?"")" , "$1" ,"""" & WEIBO_CALLBACK_URL & """" )
			call POP_MVC.file_put_contents( path,content )
			objCMS.clearCache
		end if
	End sub
	
	'设置页面
	sub settings
		dim templatePath,str,data
		
		'头像库
		str = B_("self_GuestConfig").field( "SelectOptions" ).where("ConfigName='"& "AvatarType" &"'").getOne()
		set data = K_("bbs--Public").option2dict(str)
		that.d("avatar") = data
		objCMS.BbsShow( "manage/set" )
	end sub
	
	'设置头像库
	sub editAvatarType
		dim dict,str,sTip
		set dict = POP_MVC.form2dict
		for i = 1 to dict.count/2
			if dict( "title" & i ) <> "" then
				str = str & "<option value=""" & dict( "value" & i ) & """>" & dict( "title" & i ) & "</option>" & vbcrlf
			end if
		next
	

		if str <> "" then
			Call B_("self_GuestConfig").where( that.dict( "ConfigName" , "AvatarType"  ) ).setField( "SelectOptions",str ) 
			sTip = "头像库设置成功！"
			Call K_( "bbs--Public" ).CtrlLog( 1, "用户ID: " & S_("adminId") & " ，" & sTip , Array( sTip  , "?Manage_avatar" & guestConfig.PageSuffix  & "#set" ) )
		else
			sTip = "头像库不能少于一个！"
			Call K_( "bbs--Public" ).CtrlLog( 0, "用户ID: " & S_("adminId") & " ，头像库设置失败！"  & sTip , sTip )
		end if		
	end sub
	
	
	'设置头像库
	sub addAvatarType
		dim dict,str,arr,value,title,content,path,cnt,imgpath
		set dict = POP_MVC.form2dict
		
		'路径名
		value = replace(dict("value")," ","")
		
		'说明
		title = replace(dict("title")," ","")
		
		'所有图片
		arr = P_("http").FetchImgUrls(dict("Content"),false)
		
		if ubound(arr) < 0 then
			that.error("未得到有效图片")
		end if
		
		'路径
		path = objCMS.getTplFolderPath() & "/res/images/" & value
		
		
		
		'不存在文件夹则创建
		if not POP_MVC.file.isFolder( path ) then
			Call POP_MVC.CreateFolder( path )
		end if
		
		
		'取得图片个数
		cnt = objCMS.getAvatarCount( value )
		
		'将图片进行移动		
		for i = 0 to ubound(arr)
			imgpath = path & "/" & ( cnt + i ) & POP_MVC.file.extname( arr(i) )
			Call POP_MVC.file.rename( arr(i) ,imgpath )
		next
		
		str = B_("self_GuestConfig").field( "SelectOptions" ).where("ConfigName='"& "AvatarType" &"'").getOne()
		
		'如果不存在还需要向数据库中添加
		if not POP_MVC.String.iExists( str, "value=""" & value & """" ) then
			str = str & "<option value=""" & value & """>" & title & "</option>" & vbcrlf
			Call B_("self_GuestConfig").where( that.dict( "ConfigName" , "AvatarType"  ) ).setField( "SelectOptions",str )  
		end if
		Call K_( "bbs--Public" ).CtrlLog( 1,  "添加了头像库 " & title & "(" & value & ")" , Array( "头像库添加成功！"  , "?user_avatar" & guestConfig.PageSuffix  & "#" & value ) )
	end sub
	
	'头像
	sub avatar
		dim templatePath,str,data
		
		'引入summernote之前,要先引入该模块
		POP_MVC.config( "AUTO_EXTENDS_MODULE" ) = "summernote"
		
		'自动化导入summernote
		that.d("summernote") = P_("auto").summernote("#L_content")
		
		'头像库
		str = B_("self_GuestConfig").field( "SelectOptions" ).where("ConfigName='"& "AvatarType" &"'").getOne()
		set data = K_("bbs--Public").option2dict(str)
		that.d("avatar") = data
		objCMS.BbsShow( "manage/avatar" )
	end sub
	
	'清除回收站
	sub clearRecycle
		Call K_( "bbs--Public" ).clearRecycle
		Call K_( "bbs--Public" ).CtrlLog( 0, "用户ID: " & S_("adminId") & " ，清空了文件回收站！" , "清除完成！" )
	end sub
	
	'查找word头
	sub CountWord
		dim html,rs
		
		set rs = B_(objCMS.baseTable).from( "{prefix}self_GuestTopic" ).field("TopicID,Content,Title").select
		do while not rs.eof
			if POP_MVC.String.reg_test( rs("Content") , "<\!--\[if [\s\S]+?<\!\[endif\]-->" , "im" ) then
				html = html + "<a href='" + getThreadLink( rs("TopicID") ) + "'>" + rs("Title") + "</a>" & "<br />" & vbcrlf 
			end if
			rs.movenext
		loop
		html = html + "<a href='javascript:history.back()'>返回</a>"
		response.write html
		that.u(rs)
	end sub
	
	'查找word头
	sub ClearWord
		dim html,rs,stri,i
		
		i = 0
		set rs = B_(objCMS.baseTable).from( "{prefix}self_GuestTopic" ).field("TopicID,Content,Title").select
		do while not rs.eof
			if POP_MVC.String.reg_test( rs("Content") , "<\!--\[if [\s\S]+?<\!\[endif\]-->" , "im" ) then
				str = POP_MVC.String.reg_replace( rs("Content") ,"", "<\!--\[if [\s\S]+?<\!\[endif\]-->" , "gim" )
				Call B_("self_GuestTopic").where( rs("TopicID") ).setField( "Content" , str  )
				i = i + 1
			end if
			rs.movenext
		loop
		that.success( "共清除 " & i & " 篇" & guestConfig.ThreadName & "的word头"  )
	end sub
end Class
%>