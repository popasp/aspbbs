<%
Class Message
	sub init
		'表名 
		'db是popasp_access类的实例化
		db.tableName = "Self_GuestMessage"
		
		'主键
		db.prikey = "MessageID"
		
		db.auto_ = Array( _
			Array( "AddTime" ,"POP_MVC.FormatDate( now() , ""YYYY-MM-DD HH:II:SS"" )",  -1 , "function" ) _
			,Array( "IsRead" , 0 ,  1 ) _
			,Array( "IP" , "get_client_ip" ,  1 , "function" ) _
		)
			
			',Array( "Message" ,"bbs--Message.stripTags",  3 , "callback" ,  array()  ) _
	end sub
	
	sub index
		if S_("adminId") = "" then
			that.error("请先登陆！")
		else
			that.error("正在跳转！")
		end if
	end sub
	
	function stripTags( Message )
		if isNul(Message) then
			stripTags = ""
		else
			stripTags = POP_MVC.String.strip_tags( Message , "html" )
		end if
		stripTags = left( stripTags , 500 )
	End function
	
	sub SendSuper( FromUserID , msg )
		dim arr,dict
		arr = B_( baseTable ).from("{prefix}User").where( "GroupID = 1" ).field("UserID").get1arr
		
		set dict = D_
		for i = 0 to ubound( arr )
			dict.removeAll
			dict("FromUser") = FromUserID
			dict("ToUser") = arr(i)
			dict("Message") = msg
			Call db.data(dict).create(1)
			set dict = db.getData
			db.data(dict).add
		next
	End Sub
	
	sub SendMessages( FromUserID, ToUserID, msg )
		dim arr,dict
		arr = ToUserID
		if not isArray( ToUserID ) then
			arr = split( ToUserID , "," )
		end if		
		set dict = D_
		for i = 0 to ubound( arr )
			dict.removeAll
			dict("FromUser") = FromUserID
			dict("ToUser") = arr(i)
			dict("Message") = msg
			Call db.data(dict).create(1)
			
			set dict = db.getData
			Call db.data(dict).add
		next
	End Sub
End Class
%>