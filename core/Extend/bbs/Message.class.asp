<%
Class Message
	private tableName
	sub initialize
		tableName = "self_GuestMessage"
		that.d("layuicachepage") = "user"
	end sub
	
	sub index
		call nums
	end sub
	
	sub nums
		dim where,dict
		
		set dict = D_
		if S_("adminId") <> "" then
			set where = D_
			where("ToUser") = S_("adminId")
			where("IsRead") = 0
			dict("status") = that.ajaxSuccessStatus
			dict("count") = B_(tableName).where( where ).count
		else
			dict("status") = that.ajaxSuccessStatus
			dict("count") = 0
		end if
		that.ajaxData( dict )
		if POP_MVC.config("SHOW_PAGE_TRACE") = 0 then
			Call POP_MVC.ClearSqlRS("*")
		end if
	end sub
	
	sub Empty_
		K_("bbs--Public").showError( "您访问的页面不存在！" )
		response.end
	end sub
end Class
%>