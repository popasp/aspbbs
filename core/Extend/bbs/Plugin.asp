<%
Class [Plugin]
	Private params
	Private Sub Class_Intialize	
	End Sub
	
	Private Sub Class_Terminate	
		If isObject( params ) then set params = nothing	
	End Sub

	'获取通过js发送ajax数据
	Public Function getAjaxData
		set getAjaxData = p_("http").getAjaxData
	End Function
	
	'直接解析request.form传递过来的字符串，不必使用_data键名
	Public Function getAjaxData4str( str )
		set getAjaxData4str = p_("http").getAjaxData4str( str )
	End Function
	
	'测试发送邮件
	function TestSendMail( stype, ByVal mail )
		dim typestr,subject,body
		
		select case stype
			case "mail"
				typestr = "mail组件发送"
			case "jmail"
				typestr = "JMail组件发送"
			case "ServerSendMail"
				typestr = "官方服务器发送"	
			case "ServerForwardMail"
				typestr = "官方服务器代发"	
		end select

		subject = typestr & " 测试邮件"
		body = "如果您收到该邮件，祝贺您，可以使用 " & typestr
		
		if stype = "mail" or stype = "jmail" then		
			Call DllMail( stype,"", subject , body)
		elseif stype = "ServerSendMail" then
			Call ServerSendMail("", subject , body)
		elseif stype = "ServerForwardMail" then
			Call ServerForwardMail("", subject , body )		
		end if
	End Function
	
	'官方服务器代发
	'guestConfig.MessageAlertsEmail
	Function ServerForwardMail(ByVal mail,subject,body)
		dim data
		set data = D_	
		
		if isNul(mail) then
			mail = guestConfig.MessageAlertsEmail
		end if		
		
		data("subject") = subject
		data("body") = body
		data("SMTPServer") = guestConfig.smtp_server
		data("User") = guestConfig.smtp_user
		data("Password") = guestConfig.smtp_password
		data("FromMail") = guestConfig.smtp_usermail
		data("ToMail") = mail
		Call P_("http").post(C_( "SERVER_MAIL_URL" ),data)
	End Function
	
	'官方服务器发送
	Function ServerSendMail(ByVal mail,subject,body)
		dim data	
		set data = D_	
		
		if isNul(mail) then
			mail = guestConfig.MessageAlertsEmail
		end if			
			
		data("subject") = subject
		data("body") = body
		'data("SMTPServer") = guestConfig.smtp_server
		'data("User") = guestConfig.smtp_user
		'data("Password") = guestConfig.smtp_password
		'data("FromMail") = guestConfig.smtp_usermail
		data("ToMail") = mail
		Call P_("http").post(C_( "SERVER_MAIL_URL" ),data)
	End Function
	
	'发送邮件
	'目前只支持mail与jmail
	function DllMail( mailType,ByVal mail, subject, body )
		if isNul(mail) then
			mail = guestConfig.MessageAlertsEmail
		end if
	
		with P_(mailType)
		.SMTPServer = guestConfig.smtp_server
		.User = guestConfig.smtp_user
		.Password = guestConfig.smtp_password
		.FromMail = guestConfig.smtp_usermail
		.ToMail = mail
		.Subject = subject
		.Body = body
		DllMail = P_(mailType).send
		end with
	End Function	
	
	'发送邮件
	function sendMail( mail, subject, body )
		dim mailType
		mailType =  guestConfig.mail_type
		if mailType = "mail" or mailType = "jmail" then		
			Call DllMail( mailType,mail, subject , body)
		elseif mailType = "ServerSendMail" then
			Call ServerSendMail(mail, subject , body)
		elseif mailType = "ServerForwardMail" then
			Call ServerForwardMail(mail, subject , body )		
		end if
	End Function

	'获取邮件类型
	Function getMail
		if POP_MVC.IsInstall("JMail.Message") then
			getMail = "jmail"
		end if
		
		if POP_MVC.IsInstall("CDO.Configuration") AND POP_MVC.IsInstall("CDO.Message") then
			getMail = "mail"
		end if
	End Function
	
	'得到邮件发送类型的中文字符
	function getMailType(mailType)
		select case mailType
			case "mail"
				getMailType = "mail组件发送"
			case "jmail"
				getMailType = "JMail组件发送"
			case "ServerSendMail"
				getMailType = "官方服务器发送"
			case "ServerForwardMail"
				getMailType = "官方服务器代发"
		end select
	End Function
	
	'获取真实url地址
	Function getSitePath( )
		dim httpHost
		httpHost = POP_MVC.http_host
		httpHost = POP_MVC.rtrim(httpHost,"/") & "/"
		httpHost = httpHost & P_( C_("CMS_NAME") ).linkPrefix
		getSitePath = httpHost
	End Function
	
	'获取插件的目录结构
	Function getExtendDirMap( ExtendName )
		if isNul( ExtendName ) then
			ExtendName = POP_MVC.CurExtend
		end if
		getExtendDirMap = POP_MVC.file.getFilesMap( POP_MVC.guestConfig("EXTEND_PATH") & "/" & ExtendName, "" ,-1)	
	End Function
End Class
%>