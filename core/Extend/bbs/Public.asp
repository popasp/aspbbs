<%
Class [Public]
	private unreadMsgNums
	'初始化类
	Private Sub Class_Initialize()
	End Sub	
	
	
	'显示404页面
	Public Property Get CloseBBS
		dim templatePath
		that.d("errStr") = guestConfig.SiteCloseTip
		objCMS.show( "other/tips" )
		POP_MVC.quit
	end Property

	'显示404页面
	Public Property Get show404
		objCMS.show( "other/404" )
		POP_MVC.quit
	end Property

	'显示错误提示消息
	Public Property Get showError(ByRef errStr)
		dim templatePath
		that.d("errStr") = errStr
		that.d("layuicachepage") = ""
		objCMS.show( "other/notice" )
		POP_MVC.quit
	end Property
	
	'发送站内消息
	Public Sub SendMessage( FromUser, ToUser, info )
		dim dict
		set dict = D_
		if isNul( FromUser ) then
			dict("FromUser") = 1
		else
			dict("FromUser") = FromUser
		end if
		
		if isNul( ToUser ) then
			dict("ToUser") = S_("adminId")
		else
			dict("ToUser") = ToUser
		end if
		
		dict("Message") = info
		 
		Call ModelCreate("bbs--Message",dict,1)
		
		Call ModelAdd( "bbs--Message" , null )
		POP_MVC.unset(dict)
	End sub
	
	'发送站内消息，回复帖子
	Public Sub SendMessage4reply( TopicID , ReplyID , ToUser, title , page  )
		dim nickname,info
		
		if Cstr( S_("adminId") ) = CStr( ToUser ) then		
			nickname = "您"
		elseif  isNul(S_("Nickname")) then
			nickname = S_("adminName")
		else
			nickname = S_("Nickname")
		end if
		
		info = "<a href='?u_" & S_("adminId") & "' target='_blank'><cite>" & nickname & "</cite></a>回答了您的求解<a target='_blank' href='" & objCMS.getReplyLink( TopicID , ReplyID , page) & "'><cite>" & title & "</cite></a>"
				'POP_MVC.ajax(info)
		Call SendMessage( S_("adminId") , ToUser, info )
	End sub
	
	'发送站内消息，提到了您
	Public Sub SendMessage4cite( TopicID , ReplyID , ToUser, title , page  )
		dim nickname,info
		
		if Cstr( S_("adminId") ) = CStr( ToUser ) then		
			nickname = "您"
		elseif  isNul(S_("Nickname")) then
			nickname = S_("adminName")
		else
			nickname = S_("Nickname")
		end if
		
		
		info = "<a href='?u_" & S_("adminId") & "' target='_blank'><cite>" & nickname & "</cite></a>在 <a target='_blank' href='" & objCMS.getReplyLink( TopicID , ReplyID , page) & "'><cite>" & title & "</cite></a> 中提到了您！"
		Call SendMessage( S_("adminId") , ToUser, info )
	End sub	
	
	'比较当前人员与管理员的权限
	Function AuthCompare( ByRef UserID , ByRef info )
	
		dim rsObj,where
		set where = D_
		where("UserID") = UserID
		where("a.GroupID=b.GroupID") = null
		set rsObj =  B_("User").field("GroupName,GroupMark,UserID").from("{prefix}User as a, {prefix}UserGroup as b").where( where ).onlysql(0).find
		
		if UserID - rsObj("UserID") = 0 and S_("adminId") - UserID = 0 then
			exit Function
		end if		

		if rsObj("GroupMark") - S_("GroupMark") >= 0 then
			AuthCompare = "您无权对 " & rsObj("GroupName") & " " & info
		end if
		POP_MVC.unset(rsObj)
	End Function
	
	'获取某个用户信息
	function getUserInfo(ByRef UserID)
		dim rsObj,where
		set where = D_
		where("UserID") = UserID
		where("a.GroupID=b.GroupID") = null
		set rsObj =  B_("User").field("IsAdmin, GroupStatus,GroupName,GroupMark, UserStatus, UserID, adminrand,a.GroupID as group_id,a.Avatar,a.LoginName,Nickname,ThreadStatus,ReplyStatus,Experience").from("{prefix}User as a, {prefix}UserGroup as b").where( where ).onlysql(0).find
		set getUserInfo = rsObj
	end Function
	
	'将一组<option value="value">title</option>转化为二维Dictionary对象
	function option2data(str)
		dim dataObj,dict,matches,match
		set dataObj = D_
		set matches = POP_MVC.String.reg_exec( str, "<option\s+value=(""|'|)(.+?)\1>(.+?)<\/option>" , "gim" )
		for each match in matches
			set dict = D_
			dict( "value" ) = match.subMatches(1)
			dict( "title" ) = POP_MVC.String.strip_tags(match.subMatches(2) , "html")
			dataObj.add dataObj.count, dict
		next
		set matches = nothing
		set option2data = dataObj
	end function
	
	'将一组<option value="value">title</option>转化为一维Dictionary对象
	function option2dict(str)
		dim dict,matches,match
		set dict = D_
		set matches = POP_MVC.String.reg_exec( str, "<option\s+value=(""|'|)(.+?)\1>(.+?)<\/option>" , "gim" )
		for each match in matches			
			dict( match.subMatches(1) ) = POP_MVC.String.strip_tags(match.subMatches(2) , "html")
		next
		set matches = nothing
		set option2dict = dict
	end function
	
	'包装POP_MVC.upload
	'如果有上传插件，则将文件信息添加到数据表
	function upload( ByRef str , ByRef logID )
		dim filename,errID
		
		filename = POP_MVC.upload("file")
		
		if not K_("UploadLog--Public") is nothing then
			if filename <> "" then
				errID = 0
			else
				errID = POP_MVC.Uploader.ErrorID
			end if			
			logID = K_("UploadLog--Public").upload( POP_MVC.Uploader.SrcName ,  filename , POP_MVC.file.filesize(filename), str , errID ,POP_MVC.Uploader.Description)
		end if
		upload = filename
	end function
	
	'包装POP_MVC.upload
	function upload4base64( ByRef srcName , ByRef filename ,  ByRef logID )
		if not K_("UploadLog--Public") is nothing then	
			logID = K_("UploadLog--Public").upload( srcName ,  filename , POP_MVC.file.filesize(filename), "upload4base64" , 0 ,"success")
		end if
		upload4base64 = filename
	end function
	
	'包装POP_MVC.upload
	'如果有上传插件，则将文件信息添加到数据表
	function recycleUpload( UploadPath )
		dim DelPath,theDir
		if not K_("UploadLog--Public") is nothing then
			Call K_("UploadLog--Public").recycle( UploadPath )
		else
			DelPath =  "#recycle" & objCMS.SubStrUserUploadFolder( UploadPath )
			theDir = POP_MVC.file.dir( DelPath )
			Call POP_MVC.CreateFolder( theDir )
			Call POP_MVC.file.rename( UploadPath , DelPath )	
		end if
		recycleUpload = DelPath
	end function	
	
	'包装POP_MVC.upload
	'如果有上传插件，则将文件信息添加到数据表
	function clearRecycle( )
		dim DelPath,theDir,dict
		if not K_("UploadLog--Public") is nothing then
			set dict = D_
			dict("DelTime") = now()
			dict("LogStatus") = 3				
			Call B_("Self_UploadLog").where( "LogStatus = 2" ).setField( dict ,null )
		end if
		Call POP_MVC.file.remove( "./#recycle" )
		clearRecycle = true
	end function
	
	'包装下载地址
	Function download( ByRef logID , SrcName )
		download = "<a title='" & POP_MVC.String.encodeHtml(SrcName) & "' href='?UploadLog--Index_download_" & logID & "' title=""" & POP_MVC.String.encodeHtml(SrcName) & """>" & SrcName & "</a>"	
	End Function
	
	
	'包装控制器方法
	'SucOrErr 成功或失败，1成功，0失败
	'DbTip 写入数据库的提示
	'arg为that.success或that.error的参数
	Sub CtrlLog( SucOrErr, DbTip ,  arg )
		if not K_("CtrlLog--Public") is nothing then
			Call K_("CtrlLog--Public").DoLog( SucOrErr , DbTip, arg )
		end if
		if SucOrErr = 1 then
			that.success( arg )
		elseif SucOrErr = 0 then
			that.error( arg )
		end if
	End Sub
	
	'包装类似这种  M_( "bbs--Thread" ).db.data(dict).create(2)
	'参数 ModelName：数据模型名，data：数据，stype：验证类型(1添加,2修改,3添加或修改)
	'回调类名为 ModelLog
	'架设参数 ModelName , data , stype , errtip(创建错误提示)
	'回调Call K_("ModelLog--Public").CreateLog( ModelName , data , stype ,M_( ModelName ).db.error  )
	Function ModelCreate( ByVal ModelName , ByRef data , ByRef stype )
		ModelCreate = M_( ModelName ).db.data(data).create( stype )
		if not K_("ModelLog--Public") is nothing then
			Call K_("ModelLog--Public").CreateLog( ModelName , data , stype ,M_( ModelName ).db.error  )
		end if
	End Function

	'包装类似这种  M_( "bbs--Reply" ).db.add 
	'参数 ModelName：数据模型名，data：数据
	'回调类名为 ModelLog
	'架设参数 ModelName , data , stype , errtip(创建错误提示)
	'回调Call K_("ModelLog--Public").AddLog( ModelName , data , ModelAdd )
	'ModelAdd一般为新增ID
	Function ModelAdd( ByVal ModelName , ByRef data  )
		if isNull( data ) then
			ModelAdd = M_( ModelName ).db.add
		else
			ModelAdd = M_( ModelName ).db.data(data).add
		end if
		if not K_("ModelLog--Public") is nothing then
			Call K_("ModelLog--Public").AddLog( ModelName , data , ModelAdd )
		end if
	End Function
	
	'包装类似这种  M_( "bbs--Reply" ).db.save
	'参数 ModelName：数据模型名，data：数据
	'回调类名为 ModelLog
	'架设参数 ModelName , data , stype , errtip(创建错误提示)
	'回调Call K_("ModelLog--Public").SaveLog( ModelName , data , ModelEdit )
	'ModelEdit为添加结果，成果或失败
	Function ModelEdit( ByRef ModelName , ByRef data  )
		if isNull( data ) then
			ModelEdit = M_( ModelName ).db.save
		else
			ModelEdit = M_( ModelName ).db.data(data).save
		end if
		if not K_("ModelLog--Public") is nothing then
			Call K_("ModelLog--Public").SaveLog( ModelName , data , ModelEdit )
		end if
	End Function
	
	'用来判断图片文件是否挂马
	Function TestPicMuma( filePath )
		dim str,ret
		ret = ""
		str = POP_MVC.file_get_contents_charset( filePath , "gb2312" )
		str = LCase(str)
		if (inStr( str, "<" & "%" ) > 0 or inStr( str, "%" & ">" ) > 0) and ( inStr( str, "request" ) > 0 or inStr( str, "eval" ) > 0 )  then
			ret = "asp"
		elseif inStr( str, "<" & "?" & "php" ) > 0 then
			ret = "php"
		end if
		TestPicMuma = ret
	End Function
	
	'处理试图挂马的用户
	'禁帐户、退出登陆
	Function DealPicMuma( filePath )
		DealPicMuma = TestPicMuma( filePath )
		if DealPicMuma <> "" then
			Call objCMS.setUserData( S_("adminId")  , "UserStatus" , 0 )
			Call objCMS.setUserData( S_("adminId")  , "ThreadStatus" , 0 )
			Call objCMS.setUserData( S_("adminId")  , "ReplyStatus" , 0 )
			Call POP_MVC.file.remove( filePath )
			Call M_("bbs--Message").SendSuper( S_("adminId") , S_("adminName") & "上传木马图片，已经注销了该帐户！"  )
		end if
	End Function
	
	'替换链接
	'帖子链接{p:1}
	'回复链接{r:1}
	'栏目链接{list:1}
	'幻灯片链接{slide:1},数字为序号,仅限管理员,链接图片展示
	'广告链接{adv:1},数字为序号,仅限管理员,链接图片展示
	'温馨通道中的链接{url:1},数字为序号,仅限管理员
	'友情链接{link:1},数字为序号,仅限管理员
	function replaceLink( byVal value )
		dim rule,mathces,match,i,id,rs,title,limit,sortName,userName,topicRS,where
		dim linkName,linkUrl,linkPic
		limit = 20
		
		'替换帖子链接{p:1}
		rule = "(\{\s*p\s*\:\s*([1-9]\d*)\s*\})"
		if POP_MVC.String.reg_test( value,rule,"i" ) then
			set matches = POP_MVC.String.reg_exec(value,rule,"ig")
			i = 0 
			for each match in matches
				if i = limit then
					exit for
				end if
				id = match.subMatches(1)
				set rs = B_("self_GuestTopic").where(id).find
				
				if not rs.eof then
					value = replace(value, match.value, "<a href=""" & getThreadLink(id) & """ target=""_blank"" title=""帖子:" & rs("Title") & "，发布于" & rs("AddTime") & """>" & rs("Title") & "</a>" )
				end if
				POP_MVC.unset(rs)
				i = i + 1
			next
			value = POP_MVC.String.reg_replace(value,"<!-- 超过" & limit & "个不显示 $1" & " -->",rule,"i")
		end if
		
		'替换回复链接{r:1}
		rule = "(\{\s*r\s*\:\s*([1-9]\d*)\s*\})"
		if POP_MVC.String.reg_test( value,rule,"i" ) then
			set matches = POP_MVC.String.reg_exec(value,rule,"ig")
			i = 0 
			for each match in matches
				if i = limit then
					exit for
				end if
				id = match.subMatches(1)
				set rs = B_("self_GuestReply").where(id).field("TopicID,AddTime").find
				
				if rs.eof then
					that.u(rs)
					value = POP_MVC.String.reg_replace(value,"<span style='color:red'>帖子回复不存在</span>$1",rule,"i")
				else
					set topicRS= B_("self_GuestTopic").where( rs("TopicID") ).field("Title,AddTime").find
					
					if topicRS.eof then
						that.u(topicRS)
						value = POP_MVC.String.reg_replace(value,"<span style='color:red'>帖子不存在</span>$1",rule,"i")
					else
						value = replace(value, match.value, "<a href=""?Reply_" & id & guestConfig.pageSuffix & """ target=""_blank"" title=""帖子回复:" & topicRS("Title") & "，发帖于" & TopicRs("AddTime") & "，回帖于" & rs("AddTime") & """>" & TopicRs("Title") & "</a>" )
					end if					
				end if
				
				POP_MVC.unset(rs)
				POP_MVC.unset(TopicRS)
				i = i + 1
			next
			value = POP_MVC.String.reg_replace(value,"<!-- 超过" & limit & "个不显示 $1" & " -->",rule,"i")
		end if
		
		'替换反馈留言链接{faq:1}
		rule = "(\{\s*faq\s*\:\s*([1-9]\d*)\s*\})"
		if POP_MVC.String.reg_test( value,rule,"i" ) then
			set matches = POP_MVC.String.reg_exec(value,rule,"ig")
			i = 0 
			for each match in matches
				if i = limit then
					exit for
				end if
				id = match.subMatches(1)
				set rs = B_("self_GuestBook").where("ContentStatus = 1 AND FaqID = " & id).field("FaqID,Content,AddTime").find
				
				if not rs.eof then
					title = objCMS.htmlCut2( rs("Content") , 30 ) 
					value = replace(value, match.value, "<a href=""?Faq_" & id & """ target=""_blank"" title=""留言:" & title & "，发布于" & rs("AddTime") & """>" & title & "</a>" )
				end if
				POP_MVC.unset(rs)
				i = i + 1
			next
			value = POP_MVC.String.reg_replace(value,"<!-- 超过" & limit & "个不显示 $1" & " -->",rule,"i")
		end if
		
		'替换用户链接{u:1}
		rule = "(\{\s*u\s*\:\s*([1-9]\d*)\s*\})"		
		if POP_MVC.String.reg_test( value,rule,"i" ) then
			set matches = POP_MVC.String.reg_exec(value,rule,"ig")
			i = 0 
			for each match in matches
				if i = limit then
					exit for
				end if
				id = match.subMatches(1)
				set rs = B_("User").where( that.dict( "UserID" , id ) ).field("LoginName,Nickname").find
				if not rs.eof then
					userName = iif( isNul(rs("Nickname")) , rs("LoginName"), rs("Nickname")  )
					value = replace(value, match.value, "<a href=""?u_" & id & """ target=""_blank"" title=""用户:" & userName & """>" &  userName & "</a>" )
				end if
				POP_MVC.unset(rs)
				i = i + 1
			next
			value = POP_MVC.String.reg_replace(value,"<!-- 超过" & limit & "个不显示 $1" & " -->",rule,"i")
		end if		
		
		'替换栏目链接{list:1}
		rule = "(\{\s*list\s*\:\s*([1-9]\d*)\s*\})"
		if POP_MVC.String.reg_test( value,rule,"i" ) then
			set matches = POP_MVC.String.reg_exec(value,rule,"ig")
			i = 0 
			for each match in matches
				if i = limit then
					exit for
				end if
				id = match.subMatches(1)
				sortName = getSortName(id)
				value = replace(value, match.value, "<a href=""?list_" & id & guestConfig.PageSuffix & """ target=""_blank"" title=""栏目：" & sortName & """>" & sortName & "</a>" )
				
				i = i + 1
			next
			value = POP_MVC.String.reg_replace(value,"<!-- 超过" & limit & "个不显示 $1" & " -->",rule,"i")
		end if
		
		if S_("isAdmin") = 1 then
		
			' '替换幻灯片链接{slide:1}
			' rule = "(\{\s*slide\s*\:\s*([1-9]\d*)\s*\})"
			' if POP_MVC.String.reg_test( value,rule,"i" ) then
				' set matches = POP_MVC.String.reg_exec(value,rule,"ig")
				' i = 0 
				' for each match in matches
					' if i = limit then
						' exit for
					' end if
					' id = match.subMatches(1)
					' linkName = objCMS.getArrItem( guestConfig.SlideTitle , id, ","  )
					' if linkName <> "" then
						' linkUrl = objCMS.getArrItem( guestConfig.SlideUrl , id, ","  )
						' linkPic = objCMS.getArrItem( guestConfig.SlideImage , id, ","  )
						' value = replace(value, match.value, "<a href=""" & linkUrl &  """ target=""_blank"" title=""幻灯片：" & linkName & """><img src=""" & linkPic & """>" & "</a>" )
					' end if				
					' i = i + 1
				' next
				' value = POP_MVC.String.reg_replace(value,"<!-- 超过" & limit & "个不显示 $1" & " -->",rule,"i")
			' end if
			
			' '替换广告链接{adv:1}
			' rule = "(\{\s*adv\s*\:\s*([1-9]\d*)\s*\})"
			' if POP_MVC.String.reg_test( value,rule,"i" ) then
				' set matches = POP_MVC.String.reg_exec(value,rule,"ig")
				' i = 0 
				' for each match in matches
					' if i = limit then
						' exit for
					' end if
					' id = match.subMatches(1)
					' linkName = objCMS.getArrItem( guestConfig.AdvTitle , id, ","  )
					' if linkName <> "" then
						' linkUrl = objCMS.getArrItem( guestConfig.AdvUrl , id, ","  )
						' linkPic = objCMS.getArrItem( guestConfig.AdvImage , id, ","  )
						' value = replace(value, match.value, "<a href=""" & linkUrl &  """ target=""_blank"" title=""幻灯片：" & linkName & """><img src=""" & linkPic & """>" & "</a>" )
					' end if				
					' i = i + 1
				' next
				' value = POP_MVC.String.reg_replace(value,"<!-- 超过" & limit & "个不显示 $1" & " -->",rule,"i")
			' end if
			
			
			'替换温馨通道中的链接{url:1}
			rule = "(\{\s*url\s*\:\s*([1-9]\d*)\s*\})"
			if POP_MVC.String.reg_test( value,rule,"i" ) then
				set matches = POP_MVC.String.reg_exec(value,rule,"ig")
				i = 0 
				for each match in matches
					if i = limit then
						exit for
					end if
					id = match.subMatches(1)
					linkName = objCMS.getArrItem( guestConfig.SideLinkName , id, ","  )
					if linkName <> "" then
						linkUrl = objCMS.getArrItem( guestConfig.SideLinkUrl , id, ","  )
						value = replace(value, match.value, "<a href=""" & linkUrl &  """ target=""_blank"" title=""温馨通道：" & linkName & """>" & linkName & "</a>" )
					end if				
					i = i + 1
				next
				value = POP_MVC.String.reg_replace(value,"<!-- 超过" & limit & "个不显示 $1" & " -->",rule,"i")
			end if
			
			'替换友情链接{link:1}
			rule = "(\{\s*link\s*\:\s*([1-9]\d*)\s*\})"
			if POP_MVC.String.reg_test( value,rule,"i" ) then
				set matches = POP_MVC.String.reg_exec(value,rule,"ig")
				i = 0 
				for each match in matches
					if i = limit then
						exit for
					end if
					id = match.subMatches(1)
					linkName = objCMS.getArrItem( guestConfig.LinksSiteName , id, ","  )
					if linkName <> "" then
						linkUrl = objCMS.getArrItem( guestConfig.LinksSiteUrl , id, ","  )
						value = replace(value, match.value, "<a href=""" & linkUrl &  """ target=""_blank"" title=""友情链接：" & linkName & """>" & linkName & "</a>" )
					end if				
					i = i + 1
				next
				value = POP_MVC.String.reg_replace(value,"<!-- 超过" & limit & "个不显示 $1" & " -->",rule,"i")
			end if
		end if
		

		replaceLink = value

	End function
	
	
	'将文件中的html标签全部去掉，并将</p>标签替换为<br />
	function escapeHTML(ByVal str)
		dim escape_label
		escape_label = "#QQ_1737025626_POPASP_ESCAPE_QQ_GROUP_124648143_" & POP_MVC.String.uniqid & "_#"
		str = replace( str, "</p>" , escape_label )
		str = replace( str, "</P>" , escape_label )
		str = replace( str, "<br />" , escape_label )
		str = POP_MVC.String.strip_tags(str, "html")
		str = POP_MVC.String.strip_tags(str, "jsiframe")
		str = replace( str, escape_label , "<br />" )
		escapeHTML = str
	end function
	
	'取消息个数
	function getMsgNums
		dim where
		if not isEmpty( unreadMsgNums ) then
			getMsgNums = unreadMsgNums
		else			
			if S_("adminId") <> "" then
				set where = D_
				where("ToUser") = S_("adminId")
				where("IsRead") = 0
				getMsgNums = B_("self_GuestMessage").where( where ).count
				set where = nothing
			else
				getMsgNums = 0
			end if	
			unreadMsgNums = getMsgNums
		end if

	end function
	
	function getTplSwitchHtml( curClass , sep )

		dim arr, i, tpl,html,bnd,names
		
		if guestConfig.TemplateSwitch - 0 = 0 then
			exit function
		end if
		

		arr = split( guestConfig.SiteTplArr , "," )
		names = split( guestConfig.SiteTplNames , "," )
					
		bnd = ubound( arr )
		for i = 0 to bnd
			tpl = arr(i)
			html = html & "<a class=""bbs-template-switch"" href=javascript:;' "
			if guestConfig.DefaultTemplate = tpl then
				html = html & " title='当前模板'"					
				html = html & " style='text-decoration:underline;'"
			else
				html = html & " title='点击切换到 " & tpl & " 模板'"
				html = html & " onclick=""document.cookie='SystemTemplate=" & tpl & "; max-age=' + 30*24*60*60;window.location.reload(true);"""				
			end if
			html = html & " >" & names(i) & "</a>"
			
			if i <> bnd then
				html = html & sep
			end if
		next
		getTplSwitchHtml = html
	end function
	
	'得到浏览历史
	Function getHistoryTopic(SortID)
		dim where,ids
		set where = D_

		ids = Request.Cookies("ReadThreadID")
	
		if isNul( ids ) then
			that.d("HistoryCount") = 0
			that.d("HistoryTopic") = where
			exit function
		end if
		
		
		if that.IsId(ids) then
			where( "a.TopicID" ) = ids
		elseif POP_MVC.String.reg_test( ids , "^[1-9]\d*(,[1-9]\d*)*$" , "" ) then
			ids = split( ids, "," )
			'ids = POP_MVC.arr.slice( ids, 0 , 10 )

			where( "a.TopicID" ) = array( "IN" , ids)
		else
			that.d("HistoryCount") = 0
			that.d("HistoryTopic") = where
			exit function	
		end if
		
		if SortID <> 0 and SortID <> "" then
			where("a.SortID") = SortID
		end if
		
		where("a.UserID = c.UserID ") = null
		where("b.GroupID = c.GroupID ") = null
		set historyRS = B_(objCMS.baseTable).onlysql(0).page("1,10").from(C_("THREAD_TABLE")).field(C_("THREAD_FIELD")).where( where ).select
	
		that.d("HistoryCount") = historyRS.recordCount
		that.d("HistoryTopic") = historyRS
		getHistoryTopic = ids
	End Function
	
	'将文章ID添加到cookie中
	Function setHistoryTopic(id)
		dim ids
		''''cookie设置读取栏目id''''''''''''''''''
		ids = Request.Cookies("ReadThreadID")	
		if ids = "" then
			ids = id
		elseif POP_MVC.String.reg_test( ids , "^[1-9]\d*(,[1-9]\d*)*$" , "" ) then
			ids = replace( "," & ids & "," , id & "," , "" ) ' ",1,2,3,4,5,"
			ids = POP_MVC.trim( ids, "," )
			ids = split( ids, "," )
			ids = POP_MVC.Arr.slice( ids, 0 , 200 )
			ids = id & "," & join( ids, "," )
		else
			ids = id
		end if
		ids = POP_MVC.trim( ids, "," )
		Call setCookie( "ReadThreadID" , ids )
		''''''''''''''''''''''''''''''''''''''''
	End Function
	
	Function setCookie( key, value )
		setCookie = Request.Cookies(key)
		P_("cookie").unit = "d"
		P_("cookie").Expires = CInt( guestConfig.LoginUseCookie )
		P_("cookie").assign key , value		
	End Function
End Class
%>