<%
Class Reg
	Public tableName
	
	sub initialize
		tableName = "User"
	end sub
	
	'注册页面
	sub index
		if S_("adminId") <> "" then
			Response.redirect("?UserSet")
		end if	
		
		that.d("layuicachepage") = "user"
	
	
		dim where
		'判断是否允许新用户注册
		if guestConfig.RegAllow - 0 = 0 then
			K_("bbs--Public").showError( guestConfig.RegAllowTip )
		end if
	
		'判断同IP最大允许注册数
		if guestConfig.RegIPLimit - 0 <> 0 and B_("User").where("RegIP = '" & get_client_ip() & "'").count - guestConfig.RegIPLimit >= 0 then
			K_("bbs--Public").showError( guestConfig.RegIPLimitTip )
		end if
		
		set where = D_
		where("RegTime") = Array("daydiff" , 1)
		'判断同IP最大允许注册数
		if guestConfig.RegDayLimit - 0 <> 0 and B_("User").where(where).count - guestConfig.RegDayLimit >= 0 then
			K_("bbs--Public").showErro( guestConfig.RegDayLimitTip )
		end if
		that.u(where)
	
		objCMS.show( "reg/reg" )
	end sub
	
	'重置密码
	sub doForgetEmail
		dim mailalert,SendForgetMailTime,rs,UserID,alert,templatePath
		
		UserID = that.get("id")
		alert = that.get("page")
		
		set rs = B_("User").where( that.dict( "UserID" , UserID ) ).find
		
		mailalert = rs("ForgetMailAlert")
		SendForgetMailTime = rs("SendForgetMailTime")
		
		'已完成找回或许未发送找回邮件
		if mailalert = "" then
			if isDate( SendForgetMailTime ) then
				K_( "bbs--Public" ).showError( POP_MVC.FormatDate( SendForgetMailTime , "YYYY年MM月DD日 HH:II:SS ") & "发送了找回密码邮件，并且已完成了找回操作。" ) 
			else
				K_( "bbs--Public" ).showError( "未发送找回邮码邮件！"  )
			end if			
			response.end
		end if
		
		templatePath= "reg/resetSecret"
		
		
		'验证码错误
		if mailalert <> alert then
			that.d("errStr") = "非法链接，请重新校验您的信息"
			templatePath= "reg/forget"
			
		end if
		
		'过期的话提示重新发送邮件
		if guestConfig.ForgetMailTime <> "" then
			if isDate(SendForgetMailTime) then
				if datediff( "n",SendForgetMailTime , now ) - guestConfig.ForgetMailTime > 0 then
					that.d("errStr") = "该重置密码链接已失效，请重新校验您的信息"
					templatePath= "reg/forget"
				end if
			end if
		else
			if isDate(SendForgetMailTime) then
				if datediff( "d", SendForgetMailTime , now   )  < 2  then
					that.d("errStr") = "该重置密码链接已失效，请重新校验您的信息"
					templatePath= "reg/forget"
				end if
			end if
		end if
		
		that.d("admin") = rs	
		objCMS.show( templatePath )
	end sub
	
	'激活邮件
	sub doActivateEmail
		'未登入时，先提示登入后操作
		if session("adminName") = "" then
			K_( "bbs--Public" ).showError( "您必须登入后，才能通过该链接进行激活操作" )
			response.end
		end if
	
		dim mailalert,SendActivateMailTime,bool
		mailalert = B_("User").where( S_("adminId") ).field("MailAlert").getOne
		
		that.d("msg") = "恭喜您已完成邮箱激活操作"
		that.d("info") = "已成功激活"
		
		if guestConfig.ActivateMailTime <> "" and mailalert <> "" then
			bool = false
			'先判断是否过期
			SendActivateMailTime = B_("User").where( S_("adminId") ).field("SendActivateMailTime").getOne
			if isDate(SendActivateMailTime) then
				if datediff( "h",SendActivateMailTime , now ) - guestConfig.ActivateMailTime > 0 then
					that.d("msg")  = "您的邮箱激活时间已过期，请重新发送"
					that.d("info") = "激活失败"
					bool = true
				end if
			end if		
			
			if not bool then
				Call objCMS.setUserData( S_("adminId")  , "MailAlert" , "" )
			end if
		elseif mailalert <> "" and mailalert <> that.get("id") then
			'判断验证码是否一致
			that.d("msg") = "您的邮箱验证码错误，无法激活"
			that.d("info") = "激活失败"
		elseif mailalert <> "" then
			Call objCMS.setUserData( S_("adminId")  , "MailAlert" , "" )
		end if
	
		objCMS.show( "reg/activateOk" )
	end sub		
	
	sub resetPassword
		dim mailalert,SendForgetMailTime,rs,UserID,alert,dict
		
		UserID = that.form("UserID")
		alert = that.form("MailAlert")
		
		set rs = B_("User").where( that.dict( "UserID" , UserID ) ).find
		
		mailalert = rs("ForgetMailAlert")
		SendForgetMailTime = rs("SendForgetMailTime")
		
		'已完成找回或许未发送找回邮件
		if mailalert = "" then
			if isDate( SendForgetMailTime ) then
				that.error( POP_MVC.FormatDate( SendForgetMailTime , "YYYY年MM月DD日 HH:II:SS ") & "发送了找回密码邮件，并且已完成了找回操作。" )
			else
				that.error( "未发送找回邮码邮件！"  )
			end if			
		end if
		
		'验证码错误
		if mailalert <> alert then
			that.error( "非法链接，请重新校验您的信息"  )		
		end if
		
		'过期的话提示重新发送邮件
		if guestConfig.ForgetMailTime <> "" then
			if isDate(SendForgetMailTime) then
				if datediff( "n",SendForgetMailTime , now ) - guestConfig.ForgetMailTime > 0 then
					that.error( "该重置密码链接已失效，请重新发送邮件"  )
				end if
			end if
		else
			if isDate(SendForgetMailTime) then
				if datediff( "d", SendForgetMailTime , now   )  < 2  then
					that.error( "该重置密码链接已失效，请重新发送邮件"  )
				end if
			end if
		end if
		
		dim validate

		'数据验证
		validate = Array( _
			Array( "Password","","登陆密码不能为空" , 0 , "notempty" , 3 ) _	
			,Array( "RePassword","","确认密码不能为空" , 0 , "notempty" , 3 ) _	
			,Array( "RePassword","Password","两次输入密码不一致" , 0 , "confirm" , 3 ) _
			,Array( "Password",guestConfig.RegPassLenMin & "," & guestConfig.RegPassLenMax ,POP_MVC.config("PasswordTip"), 0 , "length" , 3 ) _
			,Array( "Password","bbs--User.checkPassword",POP_MVC.config("PasswordTip"), 0 , "callback" , 3 , Array() ) _
			,Array( "verify","","验证码不能为空" , 0 , "notempty" , 3 ) _		
			,Array( "verify","^\d{4}$","验证码不正确" , 0 , "regex" , 3 ) _		
			,Array( "verify","","验证码不正确" , 0 , "verify" , 3 ) _
		)	

		if not P_("POPASP_AUTOVALIDATE").handle( validate , request.form , 2 ) then
			that.error( P_("POPASP_AUTOVALIDATE").error )
		end if
		
		set dict = D_
		dict("Password") = md5( that.form("Password") )
		dict("ForgetMailAlert") = ""
		Call objCMS.setUserData( UserID  , dict , null )
		that.u(dict)
		that.success( Array( "密码修改成功" , getLoginUrl())  )
	end sub
	
	
	'忘记密码
	sub forget
		dim obj,templatePath,errnum
		
		'如果是post，则发送邮件
		if that.isPost then
			dim verify,email,ForgetMailAlert,dict,rs,temp
			verify = that.form("verify")
			email = that.form("email")
			
			'验证码是否错误
			if verify <> S_( POP_MVC.config("SESSION_VERIFY") ) then
				that.error( "验证码错误" )
			end if
			
			'邮件格式是否错误
			if not P_("POPASP_AUTOVALIDATE").regex(email,"email") then
				that.error("邮箱格式错误")
			end if
			
			set rs = B_("User").where( that.dict("Email" , email) ).find
			
			'邮箱是否存在
			if rs.eof then
				that.u(rs)
				that.error("不存在该用户")
			end if
			
			if rs("MailAlert") <> "" then
				that.error("未完成邮箱激活，不能发送邮件")
			end if
			
			if rs("ForgetMailAlert") <> "" then
				if isDate( rs("SendForgetMailTime") ) then
					if guestConfig.ForgetMailTime <> "" then
						temp = datediff( "n", rs("SendForgetMailTime") , now   ) - guestConfig.ForgetMailTime
						if temp < 0 then
							that.error( "密码找回邮件 " & POP_MVC.FormatDate(rs("SendForgetMailTime"), "YYYY年MM月DD日 HH:II:SS" )  & " 已发送！" & abs(temp) & "分钟后才可以再次发送！")
						end if
					else
						if datediff( "d", rs("SendForgetMailTime") , now   )  < 2 then
							that.error( "密码找回邮件 " & POP_MVC.FormatDate(rs("SendForgetMailTime"), "YYYY年MM月DD日 HH:II:SS" )  & " 已发送！明天再来试试吧！")
						end if
					end if
				end if				
			end if
			
			'邮箱的识别码
			ForgetMailAlert = POP_MVC.String.uniqid
			that.d("ForgetMailAlert") = ForgetMailAlert
			
			that.d("admin") = B_("User").where( that.dict("Email" , email) ).field("Nickname,UserID").find
			
			'发送邮件
			set obj = objCMS
			templatePath= obj.getTemplatePath( "reg/forgetMail" )		
			obj.load(templatePath)	
			obj.parseHtml
			
			'发送邮件
			errnum = K_("bbs--Plugin").sendmail( rs("Email"), guestConfig.SiteName & "找回密码", obj.Content )
			if errnum = 0 then 
				set dict = D_
				dict("ForgetMailAlert") = ForgetMailAlert
				dict("SendForgetMailTime") = now()
				'设置识别码与发送时间
				Call B_("User").where( that.dict("Email" , email) ).setField( dict , null )	
				that.success( Array("找回密码邮件已成功发送" , "") )
			else
				Call B_("User").where( that.dict("Email" , email) ).setField( "ForgetMailAlert" , "" )	
				that.error( errnum & " : 由于系统原因，激活邮件发送失败")
			end if	
		else
			objCMS.show( "reg/forget" )
		end if
	end sub
	
	'注册操作
	sub doReg
		if not that.isSelfOrigin then
			that.error("非法操作")
		end if	
	
		dim id,bool,dict
		
		set dict = POP_MVC.form2dict
		
		if NOT K_("bbs--Public").ModelCreate("bbs--User",dict,1) then
			that.error( array(M_("bbs--User").db.error,-1) )
		end if
		
		bool = (guestConfig.RegReminded = 1)
		id = K_("bbs--Public").ModelAdd( "bbs--User" , null )
		if id > 0 then
			if bool then
				Call K_("bbs--Public").sendMessage( "" , id , guestConfig.RegWelcomeMessage )
				if guestConfig.RegReminded - 0 <> 0 then
					dim subject,body
					''''从这儿开始
					subject = guestConfig.SiteName & "新用户注册提醒: " & that.form("LoginName")
					body = "<p>您的网站<a href="""& POP_MVC.http_host &""">"& guestConfig.SiteName &"</a>有新用户注册</p>"		
					
					'发送邮件
					errnum = K_("bbs--Plugin").sendmail( guestConfig.MessageAlertsEmail , subject , body )
					if errnum = 0 then 
						set dict = D_
						dict("ForgetMailAlert") = ForgetMailAlert
						dict("SendForgetMailTime") = now()
						'设置识别码与发送时间
						Call B_("User").where( id ).setField( dict , null )	
					end if	
				end if
				Session("adminId") = id

				Call A_("bbs--login/RegLogin")
				that.success( Array("注册成功" , getLoginUrl()) )
			else
				Session("adminId") = id
				Call A_("bbs--login/RegLogin")
				that.success( Array("注册成功" , getLoginUrl()) )
			end if
		end if
		that.u(dict)
	end sub
End Class
%>