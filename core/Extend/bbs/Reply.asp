<%
Class Reply
	sub init
		'表名 
		'db是popasp_access类的实例化
		db.tableName = "Self_GuestReply"
		
		'主键
		db.prikey = "ReplyID"
		
		db.auto_ = Array( _
			Array( "IsAdopted" ,0,  1 ) _
			,Array( "AddTime" ,"POP_MVC.FormatDate( now() , ""YYYY-MM-DD HH:II:SS"" )",  1 , "function" ) _
			,Array( "EditTime" ,"POP_MVC.FormatDate( now() , ""YYYY-MM-DD HH:II:SS"" )",  2 , "function" ) _
			,Array( "UserID" , Session("adminId") ,  3 ) _
			,Array( "Reward" , 0 ,  1 ) _
			,Array( "ParentID" , 0 ,  1 ) _
			,Array( "ZanCount" , 0 ,  1 ) _
			,Array( "CaiCount" , 0 ,  1 ) _
			,Array( "ContentStatus" , 1 ,  1 ) _
			,Array( "EditIP" , "" ,  1 ) _
			,Array( "EditIpAddr" , "" , 1 )_
			,Array( "IP" , "get_client_ip" ,  1, "function" ) _
			,Array( "IpAddr" , "bbs--Thread.getIpAddr2" , 1 , "callback" )_
			,Array( "EditIP" , "get_client_ip" ,  2, "function" ) _
			,Array( "EditIpAddr" , "bbs--Thread.getIpAddr2" , 2 , "callback"  )_
			,Array( "IndexImage" ,"bbs--Reply.getIndexImage" , 3 , "callback" )_
			,Array( "FloorStep" ,"bbs--Reply.getFloorStep" , 3 , "callback" )_
			,Array( "Content" , "K_(""bbs--Public"").replaceLink" , 3 , "function" , array() )_
		)
		
		'数据验证
		db.validate_ = Array( _
			 Array( "TopicID" ,"",  "主题ID不能为空"  , 1, "notempty" , 1  ) _
			,Array( "Content" ,"",  "内容不能为空"  , 1, "notempty" , 3  ) _
			,Array( "Content" ,guestConfig.ReplyContentLimit & ",2000",  "内容长度不能少于" & guestConfig.ReplyContentLimit & "个字符"  , 1, "length" , 3  ) _
			,Array( "Content","bbs--Reply.TestValidate","有效字符数太少，请修改评论再发布" , 1 , "callback" , 3 , array() ) _
			,Array( "verify","","验证码不正确" , 0 , "verify" , 3 ) _
		)
	end sub
	
	Function getFloorStep(  )
		dim floor
		floor = db.where( "TopicID =" & POP_MVC.req("TopicID") ).order("ReplyID DESC").field("FloorStep").getOne
		if floor = "" then
			floor = 0
		end if

		getFloorStep = floor + 1
	End Function
	
	Function getIndexImage(  )
		getIndexImage = objCMS.Fetch_Img( request.form("Content") )
	End Function
	
	function TestValidate( Message ) 
		dim str,cnt
		'if S_("IsAdmin") = 1 then
		'	TestValidate = true
		'	exit function
		'end if
		str = POP_MVC.String.strip_tags( Message , "html" )
		str = replace( str , " " ,"" )
		str = POP_MVC.String.unique(str)
		if len( str ) - guestConfig.ReplyUniqueLen < 0 then
			TestValidate = false
		else
			TestValidate = true
		end if
	end function
	
	function getCode
		getCode = POP_MVC.String.Rand( 100000, 999999 )
	end Function
End Class
%>