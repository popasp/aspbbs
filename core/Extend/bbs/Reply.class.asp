<%
Class Reply 'Extends bbs--Common
	private tableName
	sub initialize
		tableName = "self_GuestReply"
		that.d("layuicachepage") = "jie"
	end sub
	
	Private Sub CheckReply( id )
		dim sTip
		'不是有效的ID
		if not that.isId( id ) then
			that.error( "非法操作" )
		end if
		
		'来路不对
		if not that.isSelfOrigin then
			that.error("非法操作")
		end if			
		
		if B_(tableName).where(id).field("ReplyID").getOne = "" then
			sTip = "回帖不存在"
			Call K_("bbs--Public").CtrlLog( 0 , "用户ID: " & S_("adminId") & " 对回复ID: " &  id & " " & sTip , sTip )
		end if	
	End Sub
	
	
	'删除回帖，V1.6之后是软删除
	'V1.6之前是一次性彻底删除
	Sub Remove
		dim id,rs,authInfo,sTip
		id = that.req("id")
		
		Call CheckReply(id)	
		
		'根据ID取记录
		set rs = B_(tableName).where(id).find

		if rs("UserID") - S_("adminId") <> 0 AND S_("IsAdmin") <> 1 then
			sTip = "无权删除该回帖"
			Call K_("bbs--Public").CtrlLog( 0 , "用户ID: " & S_("adminId") & " 对回复ID: " &  id & " " & sTip , "您" & sTip )
		end if
		
		'分级删除判断
		authInfo = K_("bbs--Public").AuthCompare( rs("UserID") , "的回帖进行删除操作！" )
		
		if authInfo <> "" then
			Call K_("bbs--Public").CtrlLog( 0 , "用户ID: " & S_("adminId") & " 对回复ID: " &  id & " " & authInfo , authInfo )
		end if	
		
		'回帖个数减1
		'Call B_("self_GuestTopic").where( rs("TopicID") ).setDec("ReplyCount")
		
		that.u(rs)
		
		'B_("self_GuestReply").where( id ).remove
		Call B_("self_GuestReply").where( id ).setField( "ContentStatus" , 2 )
		objCMS.clearCache
		
		Call K_("bbs--Public").CtrlLog( 1 , "用户 ID: " & S_("adminId") & "，成功软删除回复ID: " &  id , "回帖删除成功" )
	End Sub	
	
	'恢复回帖，V1.6之后加上的
	Sub Restore
		dim id,rs,authInfo,sTip
		id = that.req("id")
		
		Call CheckReply(id)	
		
		'根据ID取记录
		set rs = B_(tableName).where(id).find

		if S_("IsAdmin") <> 1 then
			sTip = "无权恢复该回帖"
			Call K_("bbs--Public").CtrlLog( 0 , "用户ID: " & S_("adminId") & " 对回复ID: " &  id & " " & sTip , "您" & sTip )
		end if
		
		'分级删除判断
		authInfo = K_("bbs--Public").AuthCompare( rs("UserID") , "的回帖进行删除操作！" )
		
		if authInfo <> "" then
			Call K_("bbs--Public").CtrlLog( 0 , "用户ID: " & S_("adminId") & " 对回复ID: " &  id & " " & authInfo , authInfo )
		end if	
		
		that.u(rs)
		
		'B_("self_GuestReply").where( id ).remove
		Call B_("self_GuestReply").where( id ).setField( "ContentStatus" , 1 )
		objCMS.clearCache
		
		Call K_("bbs--Public").CtrlLog( 1 , "用户 ID: " & S_("adminId") & "，成功软删除回复" & "ID: " &  id , "回帖删除成功" )
	End Sub	
	
	
	'采纳回帖
	Sub Accept
		dim id,rs,threadRS,sTip
		id = that.req("id")
		
		Call CheckReply(id)
		
		'根据ID取记录
		set rs = B_(tableName).where(id).find

		if rs("UserID") = S_("adminId") then
			sTip = "不能采纳自已的回帖！"
			Call K_( "bbs--Public" ).CtrlLog( 0, "用户 ID:" &  S_("adminId") & " 对回复 ID:" & id & " " & sTip , "您" & sTip )
		end if
		
		set threadRS = B_("self_GuestTopic").where( rs("TopicID") ).find
		
		if threadRS("UserID") - S_("adminId") <> 0 and S_("IsAdmin") <> 1 then
			Call K_( "bbs--Public" ).CtrlLog( 0, "用户 ID:" &  S_("adminId") & " 对回复 ID:" & id &  " 非作者非管理员，无权采纳" , "您无权操作！" )
		end if

		'先将所有的回帖IsAdopted设为0
		Call B_(tableName).where( that.dict( "TopicID" , CStr(rs("TopicID")) ) ).setField( "IsAdopted" , 0 )
		'再将指定ID的回帖IsAdopted设为1
		Call B_(tableName).where(id).setField( "IsAdopted" , 1)
		'设为已结帖子
		Call B_("self_GuestTopic").where( rs("TopicID") ).setField( "IsSolved" , 1 )
		Call B_("self_GuestTopic").where( rs("TopicID") ).setField( "IsClosed" , 1 )

		'将悬赏分添加到Reward字段中
		Call B_(tableName).where(id).setField( "Reward" , threadRS("AnswerReward") )
		'将悬赏分加到相应用户积分中
		Call B_("User").where( threadRS("UserID") ).setInc( Array( "Experience" , threadRS("AnswerReward") ) )
		objCMS.clearCache
		Call K_( "bbs--Public" ).CtrlLog( 1, "用户 ID: " & S_("adminId") & "，采纳回复 ID: " & id , "采纳成功" )
	End Sub		
	

	'修改页面
	sub edit
		'判断是否允许普通用户回帖

		if guestConfig.ReplyEditAllow - 0 = 0 AND S_("IsAdmin")<>1 then
			K_("bbs--Public").showError( guestConfig.ReplyForbidTip )
		end if		
	
		dim id,rs,authInfo,sTip,topicRS,where
		id = that.get("id")
		set rs = B_(tableName).where(id).find
		
		'判断是否有编辑帖子的权限
		if rs("UserID") <> S_("adminId") and S_("IsAdmin") <> 1 then		
			sTip = "没有权限编辑该回帖"
			Call K_("bbs--Public").CtrlLog( 0 , "用户ID: " & S_("adminId") & " 对回复ID: " &  id & " " & sTip , "您" & sTip )
		end if
		
		authInfo = K_("bbs--Public").AuthCompare( rs("UserID") , "的回帖进行编辑操作！" )
		
		if authInfo <> "" then
			Call K_("bbs--Public").CtrlLog( 0 , "用户ID: " & S_("adminId") & " 对回复ID: " &  id & " " & authInfo , authInfo )
		end if	
		
		set where = D_
		where("a.TopicID") = rs("TopicID")
		where("a.UserID = c.UserID ") = null
		where("b.GroupID = c.GroupID ") = null

		set topicRS= B_(objCMS.baseTable).onlysql(0).from(C_("THREAD_TABLE")).field(C_("THREAD_FIELD")).where( where ).find
		
		that.d("thread") = topicRS
		that.d("reply") = rs
		
		
		POP_MVC.config( "AUTO_EXTENDS_MODULE" ) = "summernote"
		that.d("summernote") = P_("auto").summernote("#L_content")
		objCMS.show( "jie/reply-edit" )
	end sub
	
	'修改页面
	sub AjaxEdit
		'判断是否允许普通用户回帖

		if guestConfig.ReplyEditAllow - 0 = 0 AND S_("IsAdmin")<>1 then
			K_("bbs--Public").showError( guestConfig.ReplyForbidTip )
		end if		
	
		dim id,rs,authInfo,sTip
		id = that.get("id")
		set rs = B_(tableName).where(id).find
		
		'判断是否有编辑帖子的权限
		if rs("UserID") <> S_("adminId") and S_("IsAdmin") <> 1 then
			sTip = "没有权限编辑该回帖"
			Call K_("bbs--Public").CtrlLog( 0 , "用户ID: " & S_("adminId") & " 对回复ID: " &  id & " " & sTip , "您" & sTip )
		end if
		
		authInfo = K_("bbs--Public").AuthCompare( rs("UserID") , "的回帖进行编辑操作！" )
		
		if authInfo <> "" then
			Call K_("bbs--Public").CtrlLog( 0 , "用户ID: " & S_("adminId") & " 对回复ID: " &  id & " " & authInfo , authInfo )
		end if	
		
		that.d("reply") = rs
		
		that.ajaxData(rs)
	end sub
	
	'快速回复操作
	sub quickAdd
		dim id,TopicID,dict,sTip
		
		TopicID = that.req("TopicID")
		
		Call addBefore( TopicID )
		
		set dict = POP_MVC.form2dict
			
		if not K_("bbs--Public").ModelCreate("bbs--Reply",dict,1) then
			sTip = M_( "bbs--Reply" ).db.error
			Call K_("bbs--Public").CtrlLog( 0 , "用户ID: " & S_("adminId") & " 添加回复 " & sTip , sTip )
		end if			
		id =  K_("bbs--Public").ModelAdd( "bbs--Reply" , null )
		if id > 0 then
			Call B_("self_GuestTopic").where("TopicID = " & TopicID).setField( "LastReplyID" , id )
		end if
		Call addAfter(id , TopicID , that.form("MessageToUser") , that.form("MessageTitle") , that.form("page") )
	end sub	
	
	'回复操作
	sub doAdd
		dim id,dict,TopicID,sTip
		
		TopicID = that.form("TopicID")	
		Call addBefore( TopicID )
		
		set dict = POP_MVC.form2dict
		
		if not K_("bbs--Public").ModelCreate("bbs--Reply",dict,1) then
			sTip = M_( "bbs--Reply" ).db.error
			Call K_("bbs--Public").CtrlLog( 0 ,  "用户 ID:" &  S_("adminId") & " 对帖子 ID: " & TopicID & "，添加回复，" & sTip  , sTip )
		end if
		
		
		id = K_("bbs--Public").ModelAdd( "bbs--Reply" , null )	
		
		if id > 0 then
			Call B_("self_GuestTopic").where("TopicID = " & TopicID).setField( "LastReplyID" , id )
		end if

		Call addAfter(id , TopicID , that.form("MessageToUser") , that.form("MessageTitle") , that.form("page") )
	end sub	
	
	private sub addBefore( TopicID )
		dim errStr,topicRS,where,where2
		
		'禁止回帖
		if B_("User").where(S_("adminId")).field("ReplyStatus").getOne = 0 then
			sTip = "已被禁止回帖！" 
			Call K_("bbs--Public").CtrlLog( 0 , "用户ID: " & S_("adminId") & " 对帖子ID: " &  TopicID & " " & sTip , "您" & sTip )
		end if
		
		set where = D_		
		
		where("a.TopicID") = TopicID
		where("a.UserID = c.UserID ") = null
		where("b.GroupID = c.GroupID ") = null
		

		set topicRS= B_(objCMS.baseTable).onlysql(0).from(C_("THREAD_TABLE")).field(C_("THREAD_FIELD")).where( where ).find
		

		'''''回复内容'''''
		if guestConfig.ReplyAddAllow - 0 = 0 then
			'是否允许回帖
			errStr = guestConfig.ReplyAddAllowTip
		elseif topicRS("IsNoComment") = 1 then
			'帖子禁回提示
			errStr = guestConfig.ReplyNoCommentTip
		elseif datediff( "d" , topicRS("AddTime") , now ) - guestConfig.ReplyTimeLimit > 0 then
			'超回帖时间提示
			errStr = guestConfig.ReplyTimeLimitTip
		elseif topicRS("ReplyCount") - guestConfig.ReplyCountLimit >= 0 and S_("IsAdmin") - 1 <> 0 then
			'最多回帖提示
			errStr = guestConfig.ReplyCountLimitTip
		elseif guestConfig.ReplyDayLimit - 0 <> 0 and S_("IsAdmin") - 1 <> 0 then
			set where2 = D_
			where2("UserID") = S_("adminId")
			where2("AddTime") = array( "daydiff" , 1 )
			'回帖达到上限后的提示
			if B_("self_GuestReply").where( where2 ).count - guestConfig.ReplyDayLimit >= 0  then
				errStr = guestConfig.ReplyDayLimitTip
			elseif B_("User").where(S_("adminId")).field("ReplyStatus").getOne = 0 then
				'用户禁回帖提示
				errStr = guestConfig.ReplyForbidTip
			end if
		end if
		
		'禁止回帖
		if errStr <> "" then
			Call K_("bbs--Public").CtrlLog( 0 , "用户ID: " & S_("adminId") & " 对帖子ID: " &  TopicID & " " & errStr , errStr )
		end if
	end sub
	
	private sub addAfter(id , TopicID , MessageToUser , MessageTitle , page )
		on error resume next
		dim content,arr
	
		if id > 0 then		
			'回帖个数加１
			Call B_("self_GuestTopic").where( TopicID ).setInc("ReplyCount")
			

			
			'回帖时加上奖励积分
			if not is_numeric( guestConfig.ReplyAddReward ) then
				guestConfig.ReplyAddReward = 0				
			end if
			

			
			if guestConfig.ReplyAddReward - 0 > 0 then
				Call B_("User").where( S_("adminId") ).setInc( Array("Experience" , guestConfig.ReplyAddReward)  )
			end if
			
			'成功跳转
			Call K_( "bbs--Public" ).CtrlLog( 1, "用户 ID:" &  S_("adminId") & " 对帖子 ID: " & TopicID & "，添加回复成功 ID: " & id , Array("回答成功","?Last_" & TopicID & "_" & id & guestConfig.pageSuffix & "#" & id ) )			
			
			'向发帖者发送一条回帖提示消息
			Call K_("bbs--Public").SendMessage4reply( TopicID , id , MessageToUser , MessageTitle,  page   )
			
			'因回复中引用了他人，发送消息提示"提到了您"，也进行邮件提醒			
			content = request.form("Content")
			arr = POP_MVC.String.reg_array( content, "<a[^>]+href\s*=\s*(""|'|)[^>]+u_(\d+)\1[^>]*\>" , 2,"gim" )
			
			for i = 0 to ubound(arr)
				if CStr( arr(i) ) <> CStr( S_("adminId") ) AND CStr( MessageToUser ) <> CStr( arr(i) ) then
					Call K_("bbs--Public").SendMessage4cite( TopicID , id , arr(i) , MessageTitle,  page   )
				end if
			next

			'邮件提醒
			if guestConfig.ReplyReminded - 0 = 1 then
				K_("bbs--mail").AddReply( id )
			end if
			
			'清除缓存
			objCMS.clearCache
			
			'成功跳转
			Call K_( "bbs--Public" ).CtrlLog( 1, "用户 ID:" &  S_("adminId") & " 对帖子 ID: " & TopicID & "，添加回复成功 ID: " & id , Array("回答成功","?Last_" & TopicID & "_" & id & guestConfig.pageSuffix & "#" & id ) )
		else
			Call K_("bbs--Public").CtrlLog( 0 ,  "用户 ID:" &  S_("adminId") & " 对帖子 ID: " & TopicID & "，添加回复失败"  , "添加失败，请重新提交" )
		end if	
	end sub
	
	'修改操作
	sub doEdit
		dim dict,sTip
		
		set dict = POP_MVC.form2dict
		
		if not K_("bbs--Public").ModelCreate("bbs--Reply",dict,2) then
			sTip = M_( "bbs--Thread" ).db.error
			Call K_( "bbs--Public" ).CtrlLog( 0, sTip , sTip )
		end if
		
		if  K_("bbs--Public").ModelEdit( "bbs--Reply" , null ) then		
			'邮件提醒
			if guestConfig.ReplyReminded - 0 = 1 then
				K_("bbs--mail").EditReply( that.form("ReplyID") )
			end if
			
			objCMS.clearCache
			
			Call K_( "bbs--Public" ).CtrlLog( 1, "用户 ID:" &  S_("adminId") & " 对帖子 ID: " & dict("TopicID") & "，回复 ID: " & dict("ReplyID") & " 修改成功" , Array("修改成功" , that.form("refer") ) )
		else
			Call K_( "bbs--Public" ).CtrlLog( 0, "用户 ID:" &  S_("adminId") & " 对帖子 ID: " & dict("TopicID") & "，对回复 ID: " & dict("ReplyID") & " 修改失败" , "修改失败，请重新提交" )
		end if
	end sub
end Class
%>