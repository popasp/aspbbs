<%
Class Sign 'Extends bbs--Top
	'签到控制器
	private tableName
	sub initialize	
		tableName = "self_GuestDigg"
	end sub
	
	'ajax操作，根据ID收藏帖子
	sub Index
		if S_("adminId") = "" then
			that.error("您尚未登陆")
		end if
	
		dim where,rs,dict,dict2,data,sTip
		
		set where = D_
		where("UserID") = S_("adminId")
		
		set rs = B_(tableName).where(where).find		
		
		'根据UserID取得RS
		set rs = B_("self_GuestDigg").where(where).field("UserID,SignTime,SignDays,SignCount,SignExperience").find
		
		
		'不存在时需要添加
		if rs.eof then	
			that.u(rs)
			set dict = K_("bbs--Experience").SignIn( rs )
			dict("UserID") = S_("adminId")
				
			if K_("bbs--Public").ModelCreate("bbs--Digg",dict,1) then
				Call K_("bbs--Public").ModelAdd( "bbs--Digg" , null )
			else
				that.u(rs)
				sTip = M_("bbs--Digg").db.error
				Call K_("bbs--Public").CtrlLog( 0 , "用户ID: " & S_("adminId") & " 签到时，" & sTip , sTip )
			end if
		else
			if isDate( rs("SignTime") ) then
				if Datediff( "d" , rs("SignTime") , now() ) = 0 then
					sTip = "已经签过到了"
					Call K_("bbs--Public").CtrlLog( 0 , "用户ID: " & S_("adminId") & " 签到时，" & sTip , "您" & sTip )
				end if
			end if
			
			set dict = K_("bbs--Experience").SignIn( rs )
			Call B_(tableName).where( where ).setField( dict, null )
		end if
		
		Call B_("User").where( where ).setInc(  Array("Experience" , dict("SignExperience") ) )
		
		that.u(rs)	

		'{"status":0,"data":{"days":1,"experience":5,"signed":true}}
		set dict2 = D_		
		dict2("status") = that.ajaxSuccessStatus
		set data = D_
		data("days") = dict("SignDays")
		data("experience") = dict("SignExperience")
		data("signed") = true
		dict2.add "data",data
		that.ajaxData( dict2 )
	end sub	
	
	'排行榜
	sub Ranking
		'response.write POP_MVC.file_get_contents("./ranking.txt")
		dim newSign,earlySign,totalSign,key,item,data,dict
		dim where
		
		set where = D_
		where( "SignCount" ) = Array( ">" , 0 )		
		
		'最新签到
		set newSign = B_(tableName).field("a.UserID as uid,a.NickName as username,a.Avatar as avatar,b.SignTime as SignTime,b.SignCount as days").from("{prefix}User as a").leftjoin("{prefix}self_GuestDigg as b on a.UserID = b.UserID").where(where).order("b.SignTime DESC").top(20).getAll
	
		'总签到
		set totalSign = B_(tableName).field("a.UserID as uid,a.NickName as username,a.Avatar as avatar,b.SignTime as SignTime,b.SignCount as days").from("{prefix}User as a").leftjoin("{prefix}self_GuestDigg as b on a.UserID = b.UserID").where(where).order("b.SignCount DESC").top(20).getAll

		'今日最快
		where("SignTime") = array("daydiff",0)
		set earlySign = B_(tableName).field("a.UserID as uid,a.NickName as username,a.Avatar as avatar,b.SignTime as SignTime,b.SignCount as days").from("{prefix}User as a").leftjoin("{prefix}self_GuestDigg as b on a.UserID = b.UserID").where(where).order("b.SignTime ASC").top(20).getAll
		
		data = Array()
		POP_MVC.Arr.push data,newSign
		POP_MVC.Arr.push data,earlySign
		POP_MVC.Arr.push data,totalSign
		
		set dict = D_
		dict("status") = that.ajaxSuccessStatus
		dict("data") = data
		that.ajaxData(dict)
	end sub
end Class
%>