<%
Class System
	'初始化数据库，数据库应为access或sqlite3
	'dbPath = "" 时，取当前数据库
	'不清空插件数据表
	'不清空语言、用户数据表
	Public Property Get InitDatabase( ByVal dbPath )
		on error resume next
		Server.ScriptTimeOut=99999
		dim tables,prefix,i,table_name,lockTables,dict
		dim db_path,db_name,theDb,dbName
		
		'不清空的数据表
		lockTables = Array("self_GuestConfig" , "self_GuestConfigGroup" , "usergroup" )

		
		
		Call setDbPathStart( dbPath ,dbName , theDb,db_path,db_name )
		
		'所有的数据表
		tables = B_( Array( "user" , C_("DB_TYPE") , theDb ) ).getAllTables
		
		'表名前缀
		prefix = LCase(POP_MVC.config("DB_PREFIX"))
		
		for i = 0 to ubound( lockTables )
			lockTables(i) = prefix & lockTables(i)
		next
		
		'清空数据表
		for i = 0 to ubound( tables )
			table_name = tables(i)
			if not POP_MVC.arr.iexists( lockTables, table_name ) then				
				if POP_MVC.config( "DB_TYPE" ) = "sqlite3" then
					if NOT POP_MVC.String.iStartsWith( table_name , "sqlite_" ) then
						B_( Array( "user" , C_("DB_TYPE") , theDb ) ).truncate(table_name)
					end if
				else
					B_( Array( "user" , C_("DB_TYPE") , theDb ) ).truncate(table_name)				
				end if				
			end if			
		next
		
		'导入管理员数据
		set dict = js_decode(POP_MVC.file_get_contents( POP_MVC.appPath & "/init/user.txt" ))		
		Call B_( Array( "user" , C_("DB_TYPE") , theDb ) ).data( dict ).add
		
		'导入贴子初始数据
		set dict = js_decode(POP_MVC.file_get_contents( POP_MVC.appPath & "/init/self_GuestTopic.txt" ))			
		Call B_( Array( "self_GuestTopic" , C_("DB_TYPE") , theDb ) ).data( dict ).mAdd

		Call setDbPathEnd( dbPath ,dbName , theDb,db_path,db_name )
	End Property
	
	Private Sub setDbPathStart( dbPath ,dbName , theDb,db_path,db_name  )
		if POP_MVC.config( "DB_TYPE" ) = "mysql" or POP_MVC.config( "DB_TYPE" ) = "sqlserver" then
			dbName = dbPath
			if isNul( dbName ) then
				dbName = POP_MVC.config("DB_NAME")
			end if
			db_name = POP_MVC.config("DB_NAME")
			POP_MVC.config("DB_NAME") = dbName
			theDb = dbName
		else
			if isNul( dbPath ) then
				dbPath = POP_MVC.config("DB_PATH")
			end if	
			db_path = POP_MVC.config("DB_PATH")
			POP_MVC.config("DB_PATH") = dbPath
			theDb = dbPath
		end if
	End Sub
	
	Private Sub setDbPathEnd( dbPath ,dbName , theDb,db_path,db_name  )
		if POP_MVC.config( "DB_TYPE" ) = "mysql" or POP_MVC.config( "DB_TYPE" ) = "sqlserver" then
			POP_MVC.config("DB_NAME") = db_name
		else
			POP_MVC.config("DB_PATH") = db_path
		end if
	End Sub
	
	
	'将数据表Language,User,Config从文件中写入数据库
	'导入的时候会先清空数据表
	Public Sub json2table( ByVal dbPath , ByVal tableArr )
		dim i,table_name,path,dict
		dim db_path,db_name,theDb,dbName
		
		if isNul( tableArr ) then
			tableArr = "User,self_GuestConfig,self_GuestTopic"
		end if
		
		Call setDbPathStart( dbPath ,dbName , theDb,db_path,db_name )
		
		if not isArray( tableArr ) then
			tableArr = split( tableArr , "," )
		end if
		
		for i = 0 to ubound( tableArr )
			table_name = tableArr(i)
			table_name = LCase(table_name)
			path = POP_MVC.appPath & "/init/" & table_name & ".txt"
			if POP_MVC.file.isFile( path ) then
				set dict = js_decode( POP_MVC.file_get_contents( path ) )	
				if table_name = "self_guesttopic" or table_name = "user" then
					B_( Array( "user" , C_("DB_TYPE") , theDb ) ).truncate(table_name)
					Call B_( Array( table_name , C_("DB_TYPE") , theDb ) ).data( dict ).add
				elseif table_name = "self_guestconfig" then
					for each key in dict
						Call B_( Array( table_name , C_("DB_TYPE") , theDb ) ).where( that.dict( "ConfigName" , key  ) ).setField( "ConfigValue",dict(key) )
					next
				end if
			end if
		next
		Call setDbPathEnd( dbPath ,dbName , theDb,db_path,db_name )
	End Sub
	
	'备份数据库
	Public Property Get BackupDatabase( ByVal dbPath )
		if isNul( dbPath ) then
			dbPath = POP_MVC.config("DB_PATH")
		end if 
		
		if POP_MVC.config( "DB_TYPE" ) = "access" or  POP_MVC.config( "DB_TYPE" ) = "sqlite3" then		
			'数据库备份
			Call POP_MVC.file.AccessBackup( dbPath , POP_MVC.file.dir(dbPath) & "backup" )
		end if
	End Property
	
	'''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
	
	
	'将当前数据表Language,User,Config保存成json数据到文件
	'Language与Config表，取的是当前的setting与config对象
	Public Property Get table2json( ByVal tableArr )
		dim i,TableName,path,where
		if isNul( tableArr ) then
			tableArr = "User,self_GuestConfig,self_GuestTopic"
		end if
		
		if not isArray( tableArr ) then
			tableArr = split( tableArr , "," )
		end if
		
		for i = 0 to ubound( tableArr )
			TableName = tableArr(i)
			TableName = LCase( TableName )
			path = POP_MVC.appPath & "/init/" & TableName & ".txt"
			if TableName = "self_guesttopic" then
				Call js_put_contents( path , B_(table_name).where("TopicID <= 4").select )
			elseif TableName = "user" then
				Call js_put_contents( path , B_(TableName).where( S_("adminId") ).find )
			elseif TableName = "self_guestconfig" then
				Call js_put_contents( path , guestConfig )
			end if
		next
	End Property
	
	'获得一个清空了的数据库
	Public Property Get getClearDatabase( dbPath )
		dim key		
		
		if POP_MVC.file.isFile( dbPath ) then					
			POP_MVC.file.remove( dbPath )
		end if		
		
		POP_MVC.file.CopyFile POP_MVC.config("DB_PATH") , dbPath
		
		Call InitDatabase( dbPath )
		Call json2table( dbPath , "" )	
		Call CloseMvcDbClass
		'Call POP_MVC.file.AccessCompress( dbPath )
	End Property
	
	Function getSelfTables( ByVal dbPath )
		dim tables,i,table_name,arr
		
		theDb = dbPath
		
		if POP_MVC.config( "DB_TYPE" ) = "mysql" or POP_MVC.config( "DB_TYPE" ) = "sqlserver" then
			if isNul( theDb ) then
				theDb = POP_MVC.config("DB_NAME")
			end if
		else
			if isNul( theDb ) then
				theDb = POP_MVC.config("DB_PATH")
			end if
		end if
		
		'所有的数据表
		tables = B_( Array("user" , POP_MVC.config("DB_TYPE") , theDb ) ).getAllTables	
		arr = Array()
		for i = 0 to ubound( tables )
			table_name = tables(i)
			if POP_MVC.String.iStartsWith( table_name , POP_MVC.config( "EXTEND_TBL_PREFIX" ) ) then
				POP_MVC.Arr.push arr,table_name
			end if			
		next
		getSelfTables = arr
	End Function
	
	'成功返回空，失败返回错误提示
	'修改后台文件夹名
	Function ModifyAdminFolderName( ByVal newPath , ByVal IsVituralHost )
		dim oldPath,inc_content
		
		oldPath = POP_MVC.file.dir(POP_MVC.appPath)
		oldPath = POP_MVC.trim( oldPath,"/" )
		if not POP_MVC.String.reg_test( newPath , "^[\w\#]+$" , "" ) then
			ModifyAdminFolderName = "核心文件夹名包含非法字符"
			Exit Function
		end if		

		if POP_MVC.file.isFolder( "./" & newPath ) then
			ModifyAdminFolderName = "文件夹" & newPath & "已经存在，请重新命名。"
			Exit Function
		end if

		if IsVituralHost = 1 then
			POP_MVC.file.remove( POP_MVC.appPath & "/Runtime" )
			Call POP_MVC.file.CopyFolder( "./" & oldPath ,"./" & newPath )			
			session( "MoveAdminPathFrom" ) = "./" & oldPath
		else	
			Call CloseMvcDbClass
			if not POP_MVC.File.rename( "./" & oldPath ,"./" & newPath ) then
				ModifyAdminFolderName = "核心文件夹重命名失败"
				Exit Function
			else
				session( "MoveAdminPathFrom" ) = "./" & oldPath
			end if	
		end if
		
		inc_content = POP_MVC.file_get_contents(  Request.ServerVariables("SCRIPT_NAME") )
		
		inc_content = replace( inc_content ,  oldPath & "/popasp" , newPath & "/popasp"  )
		'inc_content = replace( inc_content ,  oldPath & "/Extend" , newPath & "/Extend"  )
		inc_content = POP_MVC.String.reg_replace( inc_content ,"""" & newPath & """ 'coreName" ,  "\""" & oldPath & "\""\s*'coreName" ,  "i" )
		
		if not POP_MVC.file_put_contents_without_bom( Request.ServerVariables("SCRIPT_NAME") , inc_content ) then
			ModifyAdminFolderName = "文件修改失败，请手工将网站目录下面的" & Request.ServerVariables("SCRIPT_NAME") & "中的" & oldPath & "/" & "修改为" & newPath & "/，否则网站无法正常运行！"
			Exit Function
		end if
		POP_MVC.appPath = "./" & newPath
	End Function
	
	'成功返回空，失败返回错误提示
	'修改后台文件夹名
	Function ModifyConfig( ByRef stype, ByVal configName, ByRef configValue )
		dim filePath
		if inStr( stype , "./" ) > 0 then
			filePath = stype
			inc_content = POP_MVC.file_get_contents(  filePath )

			'修改配置如 POP_MVC.config("DB_TYPE") = "access"
			inc_content = POP_MVC.String.reg_replace2( inc_content , "POP_MVC.config\(\""" & configName & "\""\)\s*\=\s*(.*)" , "$1" , """" & POP_MVC.String.vbsEncode( configValue ) & """" )
		else
			if stype = "configs" then
				filePath = POP_MVC.appPath & "/configs.class.asp"
			elseif stype = "settings" then
				filePath = POP_MVC.appPath & "/configs.class.asp"	
			end if
			inc_content = POP_MVC.file_get_contents(  filePath )
			
			'修改配置如 [HtmlFilePath]="html"
			inc_content = POP_MVC.String.reg_replace2( inc_content , "\[" & configName & "\]\s*\=\s*(.*)" , "$1" , """" & POP_MVC.String.vbsEncode( configValue ) & """" )
		end if
		
		ModifyConfig = POP_MVC.file_put_contents_without_bom( filePath , inc_content )
	End Function
	
	'关闭POPASP_DATABASE_TOOL类
	Sub CloseMvcDbClass
		dim i,keys
		keys = POP_MVC.dclass.keys
		for i = 0 to ubound( keys )
			if POP_MVC.String.StartsWith( keys(i) , "POPASP_DATABASE_TOOL" ) then
				POP_MVC.dclass( keys(i) ).conn.close
				set POP_MVC.dclass( keys(i) ).conn = nothing						
				set POP_MVC.dclass( keys(i) ) = nothing
				POP_MVC.dclass.remove( keys(i) )
			end if
		next
	End Sub
End Class
%>