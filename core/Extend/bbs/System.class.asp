<%
Class System 'Extends bbs--Common
	private tableName,accPath,sqlitePath
	sub initialize
that.AjaxArgs("info") = "msg"
that.ajaxSuccessStatus = 0
that.ajaxErrorStatus = 1
	
		if S_("IsAdmin") <> 1 then
			that.error("您无权查看该页面")
		end if
		tableName = "User"
		that.d("layuicachepage") = "system"
		accPath = POP_MVC.config("CORE_NAME") & "/" & POP_MVC.config("DATA_NAME") & "/#aspbbs#.mdb"
		sqlitePath = POP_MVC.config("CORE_NAME") & "/" & POP_MVC.config("DATA_NAME") & "/#aspbbs#.db"
	end sub
	
	
	''''''''''''''''''''下面这组方法可直接用在插件中'''''''''''''''''''
	sub safeTestTest
		dim str
		str = K_("bbs--FileTool").safeTest
	end sub
	
	sub safeTestClear
		Call K_("bbs--FileTool").redundancyClear
		that.redirect("?System#safeTest")
	end sub
	
	'冗余文件检测
	sub redundancyTest
		dim str
		str = K_("bbs--FileTool").redundancy
	end sub
	
	sub redundancyClear
		Call K_("bbs--FileTool").redundancyClear
		that.redirect("?System#redundancy")
	end sub
	
	sub emptyTest
		dim str
		str = K_("bbs--FileTool").emptyFolder
	end sub
	
	sub emptyClear
		Call K_("bbs--FileTool").emptyFolderClear
		that.redirect("?System#emptyFolder")
	end sub
	''''''''''''''''''''上面这组方法可直接用在插件中'''''''''''''''''''	
	
	
	private sub redundancy
		that.d("loopstr") = K_("bbs--FileTool").getTableStr("redundancy")
	end sub
	
	'空文件夹
	'只得到空文件夹数据
	private sub emptyFolder
		that.d("emptystr") = K_("bbs--FileTool").getTableStr("emptyFolder")
	end sub
	
	'木马图片检测
	'只得到空文件夹数据
	private sub safeTest
		that.d("sloopstr") = K_("bbs--FileTool").getTableStr("safeTest")
	end sub
	
	'数据库初始化
	sub InitDataBase
		on error resume next
		Server.ScriptTimeOut=99999
		

		
		'if not that.isSelfOrigin then
		'	that.error("非法操作")
		'end if	
		
		'备份数据库
		'K_("bbs--System").BackupDatabase("")
		
		'初始化数据库
		K_("bbs--System").InitDatabase("")
		
		objCMS.clearRuntime
		Call K_( "bbs--Public" ).CtrlLog( 1, "网站完成初始化" ,  Array("初始化成功" , -1 ) )
	end sub
	
	'基本设置页面
	sub settings
		dim mailType,filepath,time1,time2
		
		Call redundancy

		Call emptyFolder
		
		Call safeTest		
		
		that.d("backupFolder") = POP_MVC.config("CORE_NAME") & "/" & POP_MVC.config("DATA_NAME") & "/backup"		
		
		if not POP_MVC.file.isFile(accPath) then
			that.d("accInfo") = "access数据库不存在"
		else
			that.d("accInfo") = byte2size( POP_MVC.file.filesize( accPath ) )
		end if
		
		if not POP_MVC.file.isFile(sqlitePath) then
			that.d("sqliteInfo") = "sqlite3数据库不存在"
		else
			that.d("sqliteInfo") = byte2size( POP_MVC.file.filesize( sqlitePath ) )
		end if
		that.d("mailType") = K_("bbs--Plugin").getMailType( guestConfig.mail_type )
		objCMS.BbsShow( "Manage/system" )
	end sub	
	
	'压缩access数据库
	sub CompressAccess
		dim sTip
		if not POP_MVC.file.isFile( accPath ) then
			sTip = "系统指定的access数据库不存在"
			Call K_("bbs--Public").CtrlLog( 0 , "用户ID: " & S_("adminId") & " 压缩access数据库时，" & sTip , sTip )
		end if
		Call POP_MVC.file.AccessCompress( accPath )	
		sTip = "ACCESS数据库压缩成功"
		Call K_( "bbs--Public" ).CtrlLog( 1, sTip , sTip )
	end sub
	
	'文件型数据库备份
	sub DbBackup
		dim dbDir,dbPath,dbName,sTip
		dbDir = POP_MVC.config("CORE_NAME") & "/" & POP_MVC.config("DATA_NAME") & "/"
		dbType = POP_MVC.req("type")
		
		select case dbType
			case "access"
				dbName = "#aspbbs#.mdb"
			case "sqlite3"
				dbName = "#aspbbs#.db"
		end select
		
		dbPath = dbDir & dbName
		if not POP_MVC.file.isFile( dbPath ) then		
			sTip = "系统指定的" & dbType & "数据库" & dbName & "不存在，不能完成备份！"
			Call K_("bbs--Public").CtrlLog( 0 , "用户ID: " & S_("adminId") & " 备份数据库时，" & sTip , sTip )
		end if
		K_("bbs--System").BackupDatabase( dbPath )	
		sTip = dbType & "数据库备份成功"
		Call K_( "bbs--Public" ).CtrlLog( 1, sTip , sTip )
	end sub
	
	'数据库转换
	sub db2db
		dim srcType,dstType,srcPath,dstPath,errStr,sTip,dbDir
		
		POP_MVC.config("APP_DEBUG") = 1
		
		Server.ScriptTimeOut=1440
		
		dbDir = POP_MVC.config("CORE_NAME") & "/" & POP_MVC.config("DATA_NAME") & "/"
		srcType = POP_MVC.req("scrType")
		dstType = POP_MVC.req("dstType")
		
		select case srcType
			case "access"
				srcPath = dbDir & "#aspbbs#.mdb"
			case "sqlite3"
				srcPath = dbDir & "#aspbbs#.db"
			case "mysql"
				srcPath = C_("DB_NAME")
		end select
		
		errStr = P_("db").testConnect( Array( srcType , srcPath ) )
		
		'先测试数据库是否能够连接
		if errStr <> "" then
			Call K_("bbs--Public").CtrlLog( 0 , "用户ID: " & S_("adminId") & " 转换数据库时，" & errStr , errStr )
		end if
		
		select case dstType
			case "access"
				dstPath = dbDir & "#aspbbs#.mdb"
			case "sqlite3"
				dstPath = dbDir & "#aspbbs#.db"
			case "mysql"
				dstPath = C_("DB_NAME")
		end select
		
		errStr = P_("db").testConnect( Array( dstType , dstPath ) )
		
		'测试数据库是否能够连接
		if errStr <> "" then
			Call K_("bbs--Public").CtrlLog( 0 , "用户ID: " & S_("adminId") & " 转换数据库时，" & errStr , errStr )
		end if

		
		select case dstType
			case "access"
				Call B_( Array("user" , srcType, srcPath ) ).toAccWithData( dstPath )
			case "sqlite3"
				POP_MVC.file.remove( dbDir & "#aspbbs#.db" )
				Call B_( Array("user" , srcType, srcPath ) ).toSqliteWithData( dstPath )
				Call sqliteFormatDate
			case "mysql"
				Call B_( Array("user" , srcType, srcPath ) ).toMysqlWithData( dstPath )
		end select
		
		sTip = srcType & " 转换到 " & dstType & " 完成!"
		Call K_( "bbs--Public" ).CtrlLog( 1, sTip , sTip )
	end sub
	
	private sub sqliteFormatDate()
		dim rs,dict,dbPath
		
		set dict = D_
		dbPath = POP_MVC.config("CORE_NAME") & "/" & POP_MVC.config("DATA_NAME") & "/#aspbbs#.db"
		
		set rs = B_( Array("self_GuestDigg" , "sqlite3" , dbPath) ).field("UserID,AddTime,EditTime,SignTime").select		
		B_( Array("self_GuestDigg" , "sqlite3" , dbPath) ).begin
		do while not rs.eof
			dict("AddTime") = POP_MVC.FormatDate(rs("AddTime") , "YYYY-MM-DD HH:II:SS" )
			dict("EditTime") = POP_MVC.FormatDate(rs("EditTime") , "YYYY-MM-DD HH:II:SS" )
			dict("SignTime") = POP_MVC.FormatDate(rs("SignTime") , "YYYY-MM-DD HH:II:SS" )
			Call B_( Array("self_GuestDigg" , "sqlite3" , dbPath) ).where( rs("UserID") ).setField( dict,null )
			rs.movenext
		loop
		B_( Array("self_GuestDigg" , "sqlite3" , dbPath) ).commit
		that.u(rs)
		dict.removeAll
		
		set rs = B_( Array("self_GuestDonate" , "sqlite3" , dbPath) ).field("DonateID,DealTime,AddTime").select
		B_( Array("self_GuestDonate" , "sqlite3" , dbPath) ).begin
		do while not rs.eof
			dict("DealTime") = POP_MVC.FormatDate(rs("DealTime") , "YYYY-MM-DD HH:II:SS" )
			dict("AddTime") = POP_MVC.FormatDate(rs("AddTime") , "YYYY-MM-DD HH:II:SS" )
			Call B_( Array("self_GuestDonate" , "sqlite3" , dbPath) ).where( rs("DonateID") ).setField( dict,null )
			rs.movenext
		loop
		B_( Array("self_GuestDonate" , "sqlite3" , dbPath) ).commit
		that.u(rs)
		dict.removeAll
		
		set rs = B_( Array("self_GuestMessage" , "sqlite3" , dbPath) ).field("MessageID,AddTime").select
		B_( Array("self_GuestMessage" , "sqlite3" , dbPath) ).begin		
		do while not rs.eof
			Call B_( Array("self_GuestMessage" , "sqlite3" , dbPath) ).where( rs("MessageID") ).setField( "AddTime",POP_MVC.FormatDate(rs("AddTime") , "YYYY-MM-DD HH:II:SS" ) )
			rs.movenext
		loop
		B_( Array("self_GuestMessage" , "sqlite3" , dbPath) ).commit	
		that.u(rs)
		
		set rs = B_( Array("self_GuestReply" , "sqlite3" , dbPath) ).field("ReplyID,AddTime,EditTime").select	
		B_( Array("self_GuestReply" , "sqlite3" , dbPath) ).begin	
		do while not rs.eof
			dict("AddTime") = POP_MVC.FormatDate(rs("AddTime") , "YYYY-MM-DD HH:II:SS" )
			dict("EditTime") = POP_MVC.FormatDate(rs("EditTime") , "YYYY-MM-DD HH:II:SS" )
			Call B_( Array("self_GuestReply" , "sqlite3" , dbPath) ).where( rs("ReplyID") ).setField( dict,null )
			rs.movenext
		loop
		B_( Array("self_GuestReply" , "sqlite3" , dbPath) ).commit	
		that.u(rs)
		dict.removeAll		
		
		set rs = B_( Array("self_GuestTopic" , "sqlite3" , dbPath) ).field("TopicID,AddTime,EditTime").select
		B_( Array("self_GuestTopic" , "sqlite3" , dbPath) ).begin	
		do while not rs.eof
			dict("AddTime") = POP_MVC.FormatDate(rs("AddTime") , "YYYY-MM-DD HH:II:SS" )
			dict("EditTime") = POP_MVC.FormatDate(rs("EditTime") , "YYYY-MM-DD HH:II:SS" )
			Call B_( Array("self_GuestTopic" , "sqlite3" , dbPath) ).where( rs("TopicID") ).setField( dict,null )
			rs.movenext
		loop
		B_( Array("self_GuestTopic" , "sqlite3" , dbPath) ).commit
		that.u(rs)
		dict.removeAll	
		
		set rs = B_( Array("User" , "sqlite3" , dbPath) ).field("UserID,RegTime,LastLoginTime,Birthday,SendForgetMailTime,SendActivateMailTime").select		
		B_( Array("User" , "sqlite3" , dbPath) ).begin	
		do while not rs.eof
			dict("RegTime") = POP_MVC.FormatDate(rs("RegTime") , "YYYY-MM-DD HH:II:SS" )
			dict("LastLoginTime") = POP_MVC.FormatDate(rs("LastLoginTime") , "YYYY-MM-DD HH:II:SS" )
			dict("Birthday") = POP_MVC.FormatDate(rs("Birthday") , "YYYY-MM-DD HH:II:SS" )
			dict("SendForgetMailTime") = POP_MVC.FormatDate(rs("SendForgetMailTime") , "YYYY-MM-DD HH:II:SS" )
			dict("SendActivateMailTime") = POP_MVC.FormatDate(rs("SendActivateMailTime") , "YYYY-MM-DD HH:II:SS" )
			Call B_( Array("User" , "sqlite3" , dbPath) ).where( rs("UserID") ).setField( dict,null )
			rs.movenext
		loop
		B_( Array("User" , "sqlite3" , dbPath) ).commit	
		that.u(rs)
		dict.removeAll				
	End sub
	
	'切换数据库
	sub switchDb
		dim dstType,srcType,errStr,dbPath,sTip
		dstType = POP_MVC.req("type")
		srcType = POP_MVC.config("DB_TYPE")
		
		if dstType = srcType then
			sTip = "不用切换，当前数据库已经是 " & srcType
			Call K_("bbs--Public").CtrlLog( 0 , "用户ID: " & S_("adminId") & " 切换数据库时，" & sTip , sTip )
		end if
		
		if dstType = "access" then
			dbPath = POP_MVC.config("CORE_NAME") & "/" & POP_MVC.config("DATA_NAME") & "/#aspbbs#.mdb"
			if not POP_MVC.file.isFile(dbPath) then
				errStr = "数据库文件 " & dbPath & " 不存在！"
			end if
		elseif dstType = "sqlite3" then
			dbPath = POP_MVC.config("CORE_NAME") & "/" & POP_MVC.config("DATA_NAME") & "/#aspbbs#.db"
			if not POP_MVC.file.isFile(dbPath) then
				errStr = "数据库文件 " & dbPath & " 不存在！"
			end if
		end if
		
		if errStr <> "" then
			Call K_("bbs--Public").CtrlLog( 0 , "用户ID: " & S_("adminId") & " 切换数据库时，" & errStr , errStr )
		end if		

		errStr = P_("db").testConnect( Array(dstType,dbPath) )
		
		'先测试数据库是否能够连接
		if errStr <> "" then
			Call K_("bbs--Public").CtrlLog( 0 , "用户ID: " & S_("adminId") & " 切换数据库时，" & errStr , errStr )
		end if
		
		Call K_("bbs--system").ModifyConfig("./#config.asp" , "DB_TYPE" , dstType )
		
		sTip = "数据库已经切换到了" & dstType
		Call K_( "bbs--Public" ).CtrlLog( 1, sTip , sTip )
	end sub
	
	
	'导入旧版数据库
	sub importDataBase
		dim dbType,srcdb,errStr,sTip,flag
		dbType = POP_MVC.req("type")
		srcdb = POP_MVC.req("OldDb")

		errStr = P_("db").testConnect( Array( dbType , srcdb ) )
		
		'先测试数据库是否能够连接
		if errStr <> "" then
			that.error( errStr )
		end if
		
		if dbType = "sqlite3" then
			if B_( Array( "user" , dbType , srcDb ) ).count = 0 then
				that.error( dbType & "数据库发生错误！" )
			end if
		end if
		
		flag = B_( Array( "self_GuestReply",dbType , srcdb ) ).fieldExists("ContentStatus")
		
		Call importConfigs( dbType,srcdb )
		
		Call importData( dbType,srcdb )
		
		if POP_MVC.config("DB_TYPE") = "sqlite3" then
			Call sqliteFormatDate
		end if
		
		'Call B_("self_GuestReply").where("1=1").setField( "ContentStatus" , 1 )
		
		if not flag then
			Call B_( Array( "self_GuestReply"  ) ).where("1=1").setField("ContentStatus" , 1)			
		end if
		
		sTip = "旧版数据导入成功"
		
		objCMS.clearRuntime
		Call K_( "bbs--Public" ).CtrlLog( 1, sTip , sTip )		
	end sub
	
	'操作，删除数据库
	sub RemoveDataBase
		dim ret,arr,i,files,sTip
		arr = split( POP_MVC.req("OldDb") , "," )
		
		for i = 0 to ubound(arr)
			ret = POP_MVC.file.removeFromDir( POP_MVC.config("CORE_NAME") & "/" & POP_MVC.config("DATA_NAME") , trim( arr(i) ) )
			POP_MVC.Arr.push files, POP_MVC.file.basename( arr(i) )
			if not ret then
				that.error( ret & "删除失败，中止操作" )
			end if		
		next
		
		sTip = "数据库删除成功"
		Call K_( "bbs--Public" ).CtrlLog( 1, (ubound(arr) + 1) & "个" & sTip ,  Array( sTip , "" ) )
	end sub
	
	'修改核心文件夹名 核心文件夹名称(默认:core)最好是别人猜不到的。
	sub EditCorePath
		dim newPath,errStr
		newPath = POP_MVC.req("folder")
		if newPath = "" then
			newPath = "#" & POP_MVC.String.uniqid()
		end if
		if LCase( POP_MVC.trim(POP_MVC.file.dir(POP_MVC.appPath),"/") ) = LCase(newPath) then
			that.error("与当前名称相同！")
		end if
		Server.ScriptTimeOut=5000
		errStr = K_("bbs--system").ModifyAdminFolderName(newPath , 1 ) 'that.req("_IsVituralHost")
		if errStr <> "" then
			Call K_( "bbs--Public" ).CtrlLog( 0, errStr ,  array( errStr , "" , 5 ) )
		else
			Call K_( "bbs--Public" ).CtrlLog( 1, "核心文件夹名成功改为" & newPath ,  "修改成功" )
		end if	
	end sub
	
	'修改模板文件夹html名 模板文件夹html改名，可以保护模板文件不被别人下载。
	sub EditHtmlPath
		dim configValue,sTip
		configValue = POP_MVC.req("configValue")
		if LCase(objCMS.htmlFilePath) = LCase(configValue) then
			that.error("与当前名称相同！")
		end if
		if POP_MVC.file.rename("./templates/" & guestConfig.DefaultTemplate & "/" & objCMS.htmlFilePath , "./templates/" & guestConfig.DefaultTemplate & "/" & configValue ) then
			Call K_("bbs--system").ModifyConfig("configs" , "HtmlFilePath" , configValue )
			sTip = "成功将模板文件夹改名为" & configValue
			Call K_( "bbs--Public" ).CtrlLog( 1, sTip ,  sTip )
		else
			sTip = "未能成功将模板文件夹改名为" & configValue
			Call K_( "bbs--Public" ).CtrlLog( 0, sTip ,  sTip )
		end if		
	end sub
	
	'导入配置
	Private sub importConfigs( dbType,srcdb )
		on error resume next
		dim dict,arr,where,key
		
		'某些 SelectOptions 属性也需要导入
		arr = Array("AvatarType")		
		
		set dict = B_( Array("self_GuestConfig",dbType , srcdb ) ).field("ConfigName,ConfigValue").getKeyValue()
		Call K_("bbs--update").ModifyConfig( "self_GuestConfig" , dict , "" , "" )
		
		that.u(dict)
		
		'导入某些 SelectOptions
		set where = D_
		where("ConfigName") = Array( "strin" , arr )
		set dict = B_( Array("self_GuestConfig",dbType , srcdb ) ).where(where).field("ConfigName,SelectOptions").getKeyValue()
		for each key in dict
			Call B_(Array("self_GuestConfig" , dbType ,srcdb )).where( that.dict( "ConfigName" , key  ) ).setField( "SelectOptions",dict(key) )
		next
		Call B_(Array("self_GuestConfig" , dbType ,srcdb )).where( that.dict( "ConfigName" , "EditorToolMobile"  ) ).setField( "ConfigValue","[['history', ['undo', 'redo']],['insert', ['emoji']],['font', ['bold', 'underline', 'clear']],['insert', ['hr','picture']]]" )
		
		that.u(where).u(dict)	
	end sub
	
	'操作 导入数据
	Private sub importData( dbType,srcdb )
		Server.ScriptTimeOut=99999
		dim i,toTables
		
		'目前支持数据迁移的表
		toTables = Array("self_GuestDigg" ,"self_GuestDonate" , "self_GuestMessage" , "self_GuestReply" , "self_GuestTopic" , "User" , "UserGroup")
		'toTables = B_(objCMS.baseTable).getAllTables
		
		for i = 0 to ubound( toTables )
			B_( Array("self_GuestConfigGroup" , POP_MVC.config("DB_TYPE") , POP_MVC.config("DB_PATH") ) ).truncate( toTables(i) )
			'var_Export "<div style='margin-left:30%;'>表" & toTables(i) & "清空完成" & "</div>"	
			'Response.flush
			

			Call importTable( toTables(i) , dbType , srcDb )
			'var_Export "<div style='margin-left:30%;'>表" & toTables(i) & "导入成功" & "</div>"	
			'Response.flush
		next
	end sub
	
	'根据表名导入数据表
	private sub importTable( table_name , dbType , srcDb )		
		dim dstDb
		if POP_MVC.config("DB_TYPE") = "mysql" then
			dstDb = POP_MVC.config( "DB_NAME" )
		else
			dstDb = POP_MVC.config( "DB_PATH" )
		end if
		Call K_("bbs--update").importTable( table_name , POP_MVC.config( "DB_PREFIX" ) , dbType , srcdb ,POP_MVC.config("DB_PREFIX") , POP_MVC.config("DB_TYPE") , dstDb   )
	End Sub
end Class
%>