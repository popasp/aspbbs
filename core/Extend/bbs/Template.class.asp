<%
Class Template
	private tableName
	sub initialize
		tableName = "self_GuestReply"
		that.d("layuicachepage") = "user"
		POP_MVC.config("TMPL_ACTION_SUCCESS") = ""
	end sub
	
	
	sub search2
		dim tpl,arr
		tpl = that.req("style")
		
		arr = split( guestConfig.SiteTplArr , "," )
		
		if not POP_MVC.Arr.Exists( arr , tpl ) then
			that.error( "该模板暂不能使用！" )
		end if
		
		if CStr(S_("GroupID")) = "1" or guestConfig.TemplateSwitch - 0 = 1 then
			Call K_("bbs--Tools").setTemplate( tpl )
				that.success( Array("模板切换成功" , "" , 0))
		else
			that.success( Array("管理员已禁止使用模板切换功能"))
		end if
	end sub
	
	
	sub tpllist
		dim filepath,topDir
		topDir = "templates/" & guestConfig.DefaultTemplate & "/html"	

		filepath = that.get("file")
		if isNul(filepath) then
			filepath = 	topDir
		elseif not POP_MVC.file.belong( topDir , filepath ) then
			filepath = topDir
		end if

		that.d("path") = filepath
		that.d("topDir") = topDir
		objCMS.bbsshow( "template/tpllist" )
	end sub
	
	sub search
		dim filepath,dict

		filepath = that.get("file")
		if not POP_MVC.file.isFile( filepath ) then
			that.error( "模板文件：" & filepath & " 不存在！" )
		end if
		
		
		set dict = objCMS.AnalyseLabel(filepath)

		that.d("file") = filepath
		that.d( "labels" ) = dict
		objCMS.bbsshow( "template/AnalyseLabel" )
	end sub
	
	sub msearch
		Call tpllist
	end sub
end Class
%>