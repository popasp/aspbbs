<%
'''捐赠
Class Thanks 'Extends bbs--Top
	private tableName,orderStr,isSolved
	sub initialize
		tableName = "self_GuestDonate"
		that.d("layuicachepage") = "donate"
	end sub


	''登陆页面
	sub index
		objCMS.show( "donate/list" )
	end sub
	
	sub Search
		dim dict,data,page,pagesize,key,item,sumPrice,cnt
		page = POP_MVC.get("page")
		pagesize = POP_MVC.get("limit")
		if isNul( page ) then page = 1
		if isNul( pagesize ) then pagesize = 10
		
		set data = B_(tableName).field( "Amount as price,PayInfo as authDesc,DealTime as create_time,FromUser,UserID as uid" ).page(Array( page,pagesize )).getAll
		for each key in data
			set item = data(key)
			item("update_time") = item("create_time")
			set item("user") = D_
			item("user")("username") = item("FromUser")
			item("user")("uid") = item("uid")
			if item("uid") <> 0 then
				item("user")("avatar") = B_("User").where( item("uid") ).field("Avatar").getOne
			else
				item("user")("avatar") = ""
			end if
			
			item.remove("FromUser")
			item.remove("uid")
		next
		sumPrice = B_(tableName).sum( "Amount" )
		cnt = B_(tableName).count()
		set dict = D_
		dict("code") = 0
		dict("count") = cnt
		dict("sumPrice") = sumPrice
		set dict("data") = data
		'POP_MVC.ajax(dict)
		that.AjaxData( dict )
	end sub

end Class
%>