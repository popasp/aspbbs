<%
Class Thread  'Extends bbs--Top
	private tableName,orderStr,isSolved,isFeature,theStatus,listAll,isOnlyUser,TopicFieldStr
	private cacheKey,isCached,soReplyPrefix,baseTable
	
	sub initialize
		tableName = "self_GuestTopic"
		that.d("layuicachepage") = "jie"
		theStatus = 1
		listAll = true
		isOnlyUser = false
		isCached = false
		soReplyPrefix= POP_MVC.config("SO_REPLY_PREFIX")
		TopicFieldStr = C_("THREAD_FIELD")
		baseTable = objCMS.baseTable
	end sub
	
	sub index
		dim key,page,id,ids
		if isEmpty(orderStr) then
			orderStr = "TopicID DESC"
		end if
		
		if NOT POP_MVC.tpl_vars.exists( "OrderID" ) then
			that.d("OrderID") = "OrderID"
			that.d("OrderReply") = "OrderReply"
		end if
		
		page = that.get("page")

		
		if page = "" then
			page = 1
		end if
		
		id = that.get("id")		
		
		if Cstr(id) = "" then
			id = 0
		end if
		
		if page = "" then
			page = 1
		end if
		
		if page > 1 then
			that.d("PageName") = "首页 第" & page & "页" 
		else
			that.d("PageName") = "首页"
		end if
		
		
		''''cookie设置读取栏目id''''''''''''''''''
		ids = Request.Cookies("ReadListID")		
		if ids = "" then
			ids = id
		elseif POP_MVC.String.reg_test( ids , "^[1-9]\d*(,[1-9]\d*)*$" , "" ) then
			if inStr( "," & ids & "," , "," & id & "," ) > 0 then
				ids = id
			else
				ids = ids & "," & id
			end if
		else
			ids = id
		end if		
		P_("cookie").unit = "d"
		P_("cookie").Expires = CInt( guestConfig.LoginUseCookie )
		P_("cookie").assign "ReadListID" , ids
		''''''''''''''''''''''''''''''''''''''''
		
		Call K_("bbs--public").getHistoryTopic( id )
		
		'设置缓存键名
		key = "Thread_" & that.get("SortID") & "_" & POP_MVC.a   & "_" & page		
		
		cacheKey = that.get("c") & "_" & that.get("a") & "_" & that.get("id") & "_" & that.get("page") & "_1"
		
		isCached = objCMS.isCached( cacheKey , "" )		
		
		Call GetTopData( id )
	
		Call getListData( id )
		objCMS.output	
			
		Call clearRS
	end sub
	
	'按标题、内容、作者、时间等的搜索
	sub search
		dim SearchInterval,keys,searchType
		
		guestConfig.ListCacheLifeTime=0
		if guestConfig.SearchInterval - 0 > 0 and  S_("IsAdmin") <> 1 then
			SearchInterval = Request.Cookies("LastSearchDate")

			if isDate(SearchInterval) then			
				if datediff( "s" , SearchInterval , now ) - guestConfig.SearchInterval < 0 then
					that.error( Array( "您搜索的太频繁了，系统指定 " & guestConfig.SearchInterval & " 秒钟可搜索一次" , -1 ) )
				end if		
			end if		
			
			P_("cookie").unit = "s"
			P_("cookie").Expires = CInt( guestConfig.SearchInterval )
			P_("cookie").set "LastSearchDate",now
		end if	
		
		searchType = POP_MVC.req("searchType")
		searchType = cStr( searchType )
	
		if POP_MVC.req("keys") <> "" then
			'限定搜索字符串长度不超过20，可有效防止一句话木马
			keys = POP_MVC.req("keys")
			keys = replace( keys, "'" , "" )
			if len(keys) > 20 then
				that.error("搜索字符串不能超过20个字符！")
			end if
			that.d("SearchTip") = ""
			
			if POP_MVC.req("searchType") = "1" then
				that.d("SearchTip") = "内容"
			else
				that.d("SearchTip") = "标题"
			end if				
			
			if  POP_MVC.String.StartsWith( keys , soReplyPrefix ) then
				that.d("SearchText") = POP_MVC.String.ltrim(keys , soReplyPrefix)
				that.d("SearchTip") = "回复"
				Call geReplytListData
			else
				that.d("SearchText") = keys
				Call index
			end if			
		elseif POP_MVC.req("UserID") <> "" then
			if not that.isID(POP_MVC.req("UserID")) then
				that.error("搜索的作者ID不正确！！！")
			end if
			set rs = B_( baseTable ).from("{prefix}User").where( that.dict( "UserID" , POP_MVC.req("UserID") ) ).field("Nickname,LoginName").find
			if rs.eof then
				that.u(rs)
				that.error("搜索的作者ID不存在！！！")
			end if
			that.d("SearchTip") = "作者"
			if rs("Nickname") <> "" then
				that.d("SearchText") = CStr(rs("Nickname"))
			else
				that.d("SearchText") =  CStr(rs("LoginName"))
			end if
			that.u(rs)
			Call index
		elseif POP_MVC.req("date") <> "" then
			if not isDate( POP_MVC.req("date") ) then
				that.error("搜索的日期格式不正确")
			end if
			that.d("SearchTip") = "日期"
			that.d("SearchText") = POP_MVC.req("date")
			Call index
		elseif POP_MVC.req("addr") <> "" then
			if len( POP_MVC.req("addr") ) > 20 then
				that.error("搜索的IP属地格式不正确")
			end if
			that.d("SearchTip") = "IP属地"
			that.d("SearchText") = POP_MVC.req("addr")
			Call index
		elseif POP_MVC.req("isTop") <> "" then
			if not is_numeric( POP_MVC.req("isTop") ) then
				that.error("搜索的日期格式不正确")
			end if
			that.d("SearchTip") = "类型"
			that.d("SearchText") = "置顶"
			Call index
		elseif POP_MVC.req("reward") <> "" then
			if not is_numeric( POP_MVC.req("reward") ) then
				that.error("搜索的 " & guestConfig.RewardTitle & " 格式不正确，应为数字")
			end if
			that.d("SearchTip") = guestConfig.RewardTitle
			that.d("SearchText") = POP_MVC.req("reward")
			Call index
		else
			
		end if	
		
		if POP_MVC.config("SHOW_PAGE_TRACE") = 0 then
			Call POP_MVC.ClearSqlRS("*")
		end if		
	end sub
	
	'综合从标题、内容、回复中搜索
	sub msearch
		dim SearchInterval,keys,searchType
		
		searchType = POP_MVC.req("searchType")
		searchType = cStr( searchType )

		guestConfig.ListCacheLifeTime=0
		if guestConfig.SearchInterval - 0 > 0 and  S_("IsAdmin") <> 1 then
			SearchInterval = Request.Cookies("LastSearchDate")

			if isDate(SearchInterval) then			
				if datediff( "s" , SearchInterval , now ) - guestConfig.SearchInterval < 0 then
					that.error( Array( "您搜索的太频繁了，系统指定 " & guestConfig.SearchInterval & " 秒钟可搜索一次" , -1 ) )
				end if		
			end if		
			
			P_("cookie").unit = "s"
			P_("cookie").Expires = CInt( guestConfig.SearchInterval )
			P_("cookie").set "LastSearchDate",now
		end if	
	
		if POP_MVC.req("keys") <> "" then
			'限定搜索字符串长度不超过20，可有效防止一句话木马
			keys = POP_MVC.req("keys")
			keys = replace( keys, "'" , "" )
			if len(keys) > 20 then
				that.error("搜索字符串不能超过20个字符！！！")
			end if
			that.d("SearchTip") = ""
			
			if POP_MVC.req("searchType") = "1" then
				that.d("SearchTip") = "内容"
			else
				that.d("SearchTip") = "标题"
			end if				
			
			if  POP_MVC.String.StartsWith( keys , soReplyPrefix ) then
				that.d("SearchText") = POP_MVC.String.ltrim(keys , soReplyPrefix)
				that.d("SearchTip") = "回复"
				Call geReplytListData
			else
				that.d("SearchText") = keys
				Call index
			end if
		end if	
		
		if POP_MVC.config("SHOW_PAGE_TRACE") = 0 then
			Call POP_MVC.ClearSqlRS("*")
		end if		
	end sub
	
	sub history
		guestConfig.ListCacheLifeTime=0
		Call index
	end sub
	
	sub solved
		that.d("OrderID") = "SolvedOrderID"
		that.d("OrderReply") = "SolvedOrderReply"
		that.d("CurAction") = "solved"
		isSolved = 1
		if POP_MVC.get("id") = "" then
			Call index
		else
			call list
		end if
	end sub
	
	sub unsolved
		that.d("OrderID") = "UnsolvedOrderID"
		that.d("OrderReply") = "UnsolvedOrderReply"
		that.d("CurAction") = "unsolved"
		isSolved = 0
		if POP_MVC.get("id") = "" then
			Call index
		else
			call list
		end if		
	end sub	
	
	sub feature
		that.d("OrderID") = "FeatureOrderID"
		that.d("OrderReply") = "FeatureOrderReply"
		that.d("CurAction") = "feature"
		isFeature = "IsFeatured"
		if POP_MVC.get("id") = "" then
			Call index
		else
			call list
		end if
	end sub	

	sub featureOrderID
		that.d("OrderID") = "FeatureOrderID"
		that.d("OrderReply") = "FeatureOrderReply"
		that.d("CurAction") = "feature"
		isFeature = "IsFeatured"
		Call orderID
	end sub	

	sub featureOrderReply
		that.d("OrderID") = "FeatureOrderID"
		that.d("OrderReply") = "FeatureOrderReply"
		that.d("CurAction") = "feature"
		isFeature = "IsFeatured"
		Call orderReply
	end sub	

	sub unsolvedOrderID
		that.d("OrderID") = "UnsolvedOrderID"
		that.d("OrderReply") = "UnsolvedOrderReply"
		that.d("CurAction") = "unsolved"
		isSolved = 0
		Call orderID
	end sub	
	
	sub unsolvedOrderReply
		that.d("OrderID") = "UnsolvedOrderID"
		that.d("OrderReply") = "UnsolvedOrderReply"
		that.d("CurAction") = "unsolved"
		isSolved = 0
		Call orderReply
	end sub		
	
	sub solvedOrderID
		that.d("OrderID") = "SolvedOrderID"
		that.d("OrderReply") = "SolvedOrderReply"
		that.d("CurAction") = "solved"
		isSolved = 1
		Call orderID
	end sub	
	
	sub solvedOrderReply
		that.d("OrderID") = "SolvedOrderID"
		that.d("OrderReply") = "SolvedOrderReply"
		that.d("CurAction") = "solved"
		isSolved = 1
		Call orderReply
	end sub	
	
	sub IndexOrderId
		Call orderID
	end sub
	
	sub IndexOrderReply
		Call orderReply
	end sub
	
	
	sub orderID
		that.d("PageName") = "按最新"
		if NOT POP_MVC.tpl_vars.exists( "OrderID" ) then
			that.d("OrderID") = "OrderID"
			that.d("OrderReply") = "OrderReply"
		end if
		orderStr = "TopicID DESC"
		if POP_MVC.get("id") = "" then
			Call index
		else
			call list
		end if
	end sub
	
	sub orderReply
		that.d("PageName") = "按回复"
		if NOT POP_MVC.tpl_vars.exists( "OrderID" ) then
			that.d("OrderID") = "OrderID"
			that.d("OrderReply") = "OrderReply"
		end if
		orderStr = "ReplyCount DESC,TopicID DESC"
		if POP_MVC.get("id") = "" then
			Call index
		else
			call list
		end if
	end sub	
	
	sub geReplytListData '(SortID)
		dim rsObj,where,template,SortID,field,keys
		
		keys = POP_MVC.req("keys")
		keys = mid(keys , len(soReplyPrefix) + 1 )
		
		set where = D_		
				
		
		where("a.UserID = b.UserID ") = null
		where("b.GroupID = c.GroupID") = null
		where("a.TopicID = d.TopicID") = null
		where("a.ContentStatus") = 1
	
		field = "d.Title,d.UserID as AuthorID,d.AddTime as AuthorTime,a.ReplyID,a.TopicID,a.IpAddr,a.Content,a.AddTime,b.UserID,b.Online,b.Avatar,b.Gender,b.LoginName,b.Nickname,b.UserStatus,c.GroupID,c.IsAdmin"
		
		set replyRS= B_( baseTable ).onlysql(0).page( "null,10" ).from("{prefix}self_GuestReply as a,{prefix}User as b,{prefix}UserGroup as c,{prefix}self_GuestTopic as d").field(field).order("a.IsAdopted DESC,a.ReplyID ASC").where( where ).search( keys , "a.Content" )	
	
		
		that.d("reply") = replyRS	
		
		
		Call getHotTopic(SortID)		
		Call getNewReply(SortID)
		
		objCMS.show( "jie/search-reply" )
	end sub
	
	private sub getListData(SortID)
		dim rsObj,where,template,keys,topicRS,ids
				
	if not isCached then	
		set where = D_
		
		keys = POP_MVC.req("keys" )
		where("a.ContentStatus") = theStatus
		
		'if guestConfig.SortHideID <> "" then
		'	where("a.SortID") = Array( "NOTIN" , guestConfig.SortHideID )
		'end if
		
		if SortID <> "" and SortID <> 0 then
			that.d("AddSortID") = SortID
			where("a.SortID") = Array("IN",SortID)
		end if
		
		if not isEmpty( isSolved ) then
			if isSolved = 0 then
				where( "IsSolved" ) = 0
			else
				where( "IsSolved" ) = Array( "<>" ,0 )
			end if			
		end if
		
		where("a.UserID = c.UserID ") = null
		where("b.GroupID = c.GroupID ") = null

		if not listAll then
			where( "a.UserID" ) = S_("adminId")
		end if
		
		if isFeature <> "" then
			where( isFeature ) = 1
		end if

		if POP_MVC.req("UserID") <> "" then
			where("c.UserID") = POP_MVC.req("UserID")
		elseif POP_MVC.req("date") <> "" then
			where("a.AddTime") = Array( "date" , POP_MVC.req("date") )
		elseif POP_MVC.req("addr") <> "" then
			where("a.IpAddr") = POP_MVC.req("addr")
		elseif POP_MVC.req("isTop") <> "" then
			where("isTop") = 1
		elseif POP_MVC.req("reward") <> "" then
			where("a.AnswerReward") = POP_MVC.req("reward")
		end if
		
		if keys <> "" and POP_MVC.req("searchType" ) = "1" then
			TopicFieldStr = TopicFieldStr & ",a.Content,a.IsTop,a.IsFeatured,a.IsSolved"
		end if
		
		if keys <> "" and POP_MVC.a = "Msearch" then
			keys = replace( keys, """", "" )
			keys = replace( keys, "'", "" )
			keys = POP_MVC.String.strip_tags( keys, "html" )
			where( "(a.Title LIKE '%" & keys & "%' OR a.Content LIKE '%" & keys & "%' OR a.TopicID in (SELECT TopicID from {prefix}self_GuestReply WHERE Content LIKE '%" & keys & "%'))" ) = null
		end if
		
		
		if POP_MVC.a = "History" then
			ids = Request.Cookies("ReadThreadID")
			if that.IsId(ids) then
				where( "a.TopicID" ) = ids
			elseif POP_MVC.String.reg_test( ids , "^[1-9]\d*(,[1-9]\d*)*$" , "" ) then
				ids = split( ids, "," )
				where( "a.TopicID" ) = array( "IN" , ids)
			else
				where( "a.TopicID" ) = 0	
			end if
		end if

		Call B_( baseTable ).onlysql(0).page(array( null, guestConfig.IndexListPageCount ) ).from(C_("THREAD_TABLE")).field( TopicFieldStr ).where( where )

		if keys <> "" then		
			if POP_MVC.a = "Msearch" then				
				set topicRS= B_( baseTable ).order(orderStr).select
			else
				'适用于搜索
				orderStr = "TopicID DESC"
				if POP_MVC.req("searchType" ) = "1" then
					set topicRS= B_( baseTable ).order(orderStr).search( keys ,"a.Content" )
				else
					set topicRS= B_(baseTable ).order(orderStr).search( keys ,"a.Title" )
				end if	
			end if
			that.d("recordCount") = topicRS.RecordCount
		else
			if orderStr <> "" then
				set topicRS= B_( baseTable ).order(orderStr).select
			else
				set topicRS= B_( baseTable ).select
			end if
			that.d("recordCount") = topicRS.RecordCount
		end if
		
		if POP_MVC.file.isFile( "./Templates/Sort/#data/#book#.xml" ) and SortID > 0 then
			that.d( "sort" ) = XM_("Sort--Thread").db.path( "Blogs/Blog[SortID=" & SortID & "]").find
		else
			that.d( "sort" ) = D_
		end if
		
		that.d("thread") = topicRS
		
	end if
	
		Call getHotTopic(SortID)
		Call getNewReply(SortID)
		Call K_("bbs--public").getHistoryTopic( SortID )
		
		dim obj
		set obj = objCMS
		
		if LCase(POP_MVC.a) = "msearch" then
			templatePath= obj.getTemplatePath( "jie/msearch" )
		elseif LCase(POP_MVC.a) = "search" then
			if POP_MVC.req("keys") <> "" then
				if POP_MVC.req("searchType" ) = "1" then
					templatePath= obj.getTemplatePath( "jie/search-content" )
				else
					templatePath= obj.getTemplatePath( "jie/search" )
				end if
			elseif POP_MVC.req("UserID") <> "" then
				templatePath= obj.getTemplatePath( "jie/search-user" )
			elseif POP_MVC.req("date") <> "" then
				templatePath= obj.getTemplatePath( "jie/search-date" )
			elseif POP_MVC.req("addr") <> "" then
				templatePath= obj.getTemplatePath( "jie/search-addr" )
			elseif POP_MVC.req("reward") <> "" then
				templatePath= obj.getTemplatePath( "jie/search-reward" )
			elseif POP_MVC.req("isTop") <> "" then
				templatePath= obj.getTemplatePath( "jie/search-top" )
			end if
		elseif POP_MVC.a = "History" then
			templatePath= obj.getTemplatePath( "jie/History" )
		elseif POP_MVC.a = "Recycle" or POP_MVC.a = "Drafts" or POP_MVC.a = "Threadcheck" then
			templatePath= obj.getTemplatePath( "jie/Recycle" )
		else
			template = "jie/index"
			if SortID > 0 then		
				if POP_MVC.file.isFile( obj.getTemplatePath( template & "#" & SortID ) ) then				
					template = template & "#" & SortID
				end if
			end if			
			templatePath= obj.getTemplatePath( template )
		end if

		Call obj.CompileContent( templatePath )				
		obj.parseHtml
		
	end sub
	
	sub list
		dim id
		id = that.req("id")
		if isEmpty(orderStr) then
			orderStr = "TopicID DESC"
		end if
		
		Call GetTopData( id )		
		Call getListData( id )
		
		objCMS.output	
		Call clearRS
	end sub
	
	sub recycle
		if S_("IsAdmin") <> 1 then
			that.error( "您无权查看回收站！" )
			listAll = false
		end if
		that.d("PageName") = "回收站"
		
		if isEmpty(orderStr) then
			orderStr = "TopicID DESC"
		end if

		theStatus = 2
		Call getListData( 0 )
		
		objCMS.output
		Call clearRS
	end sub
	
	sub drafts
		that.d("PageName") = "草稿箱"
		
		if isEmpty(orderStr) then
			orderStr = "TopicID DESC"
		end if
		
		if S_("IsAdmin") <> 1 then
			listAll = false
		end if

		theStatus = 3
		Call getListData( 0 )
		
		objCMS.output
		Call clearRS
	end sub
	
	sub ThreadCheck
		that.d("PageName") = "待审"
		
		if isEmpty(orderStr) then
			orderStr = "TopicID DESC"
		end if
		
		if S_("IsAdmin") <> 1 then
			listAll = false
		end if

		theStatus = 0
		Call getListData( 0 )
		
		objCMS.output
		Call clearRS
	end sub
	
	Private Function getReplyView( rs )
		dim where
		
		if guestConfig.ReplyViewMode - 0 = 0 then
			getReplyView = false : exit function
		end if
		
		if guestConfig.ThreadHideMode - 0 = 0 then
			getReplyView = false : exit function
		end if
		
		if S_("IsAdmin") = 1 then
			getReplyView = false : exit function
		end if
		
		if rs("IsReplyView") <> 1 then 
			getReplyView = false : exit function
		end if
		
		if CLng(S_("adminId")) = rs("UserID") then
			getReplyView = false : exit function
		end if
		
		if S_("adminId") <> "" then
			set where = D_
			where("TopicID") = rs("TopicID")
			where("UserID") = S_("adminId")
			if B_( baseTable ).from("{prefix}self_GuestReply").where( where ).field("ReplyID").getOne <> "" then
				getReplyView = false : exit function
			end if
		end if
		

		getReplyView = true
	End Function
	
	
	'计算某个回复的链接，并跳转。
	sub Reply
		dim thread,id,field,topicRS,where,where2,template,authInfo,pageCnt,page,header
		dim ReplyRS,ReplyID

		ReplyID = that.get("ReplyID")
		set ReplyRS = B_( baseTable ).from("{prefix}self_GuestReply").where( that.dict( "ReplyID" , ReplyID ) ).find
		
		if ReplyRS.eof then
			that.u(ReplyRS)
			K_("bbs--Public").showError("回帖不存在")
		end if
		
		
		'''''帖子内容'''''
		id = ReplyRS("TopicID")
		
		that.u(ReplyRS)
		
		set where = D_
		
		
		where("a.TopicID") = id
		where("a.UserID = c.UserID ") = null

		set topicRS= B_( baseTable ).onlysql(0).from(C_("THREAD_TABLE")).field( TopicFieldStr ).where( where ).find


		'帖子不存在
		if topicRs.eof then
			that.u(topicRS)
			K_("bbs--Public").show404
			response.end
		end if
		

		if S_("adminId") <> topicRs("UserID")  then
			if S_("IsAdmin") <> 1 then
				if topicRS("ContentStatus") = 0 then
					K_("bbs--Public").showError( "该帖子尚在审核中，暂不能查看" )
					response.end
				elseif topicRS("ContentStatus") = 2 then
					K_("bbs--Public").showError( "该帖子已经被删除" )
					response.end			
				end if
			else
				if topicRS("ContentStatus") = 2 then
					'判断普通管理员是否在查看管理员已删除帖子
					authInfo = K_("bbs--Public").AuthCompare( topicRs("UserID") , "的已删除帖子进行查看！" )
					if authInfo <> "" then
						K_("bbs--Public").showError( authInfo )
						response.end	
					end if							
				end if
			end if
		end if

		'''''回复内容Recordset'''''
		that.u(where)
		
		set where = D_
		
		where("a.TopicID") = id
		where("a.UserID = b.UserID") = null
		where("b.GroupID = c.GroupID") = null
			
		field = "a.ReplyID,a.FloorStep,a.TopicID,a.ParentID,a.Content,a.IsAdopted,a.Reward,a.ZanCount,a.CaiCount,a.AddTime,b.UserID,b.Online,b.Avatar,b.Gender,b.LoginName,b.Nickname,b.UserStatus,c.GroupID,c.IsAdmin"
		
		where("a.ReplyID") = Array( "<=" , ReplyID )
		
		set replyRS= B_( baseTable ).onlysql(0).page( "null,10" ).from("{prefix}self_GuestReply as a,{prefix}User as b,{prefix}UserGroup as c").field(field).order("a.IsAdopted DESC,a.ReplyID ASC").where( where ).select
		
		pageCnt = replyRS.recordCount
		that.u(replyRS)

		page =  CLng(Abs(Int(- pageCnt / 10)))

		response.redirect( objCMS.getReplyLink( id , ReplyID, page ) )
	end sub
	
	'仅看与某个用户相关的回复
	sub only
		isOnlyUser = true

		Call detail	
	end sub
	
	'帖子详情
	sub detail
		dim id,field,where,where2,template,authInfo,UserID,NoReplyTip,editCount,historyRS
		dim topicRS,replyRS,arr,topicZanStr,diggRS,replyZanStr,replyZanArr
		
		NoReplyTip = ""
		
		'''''帖子内容'''''
		id = that.get("ID")
		
		set where = D_
		
		
		where("a.TopicID") = id
		where("a.UserID = c.UserID ") = null
		where("b.GroupID = c.GroupID ") = null

		set topicRS= B_( baseTable ).onlysql(0).from(C_("THREAD_TABLE")).field(C_("THREAD_FIELD")  & ",UploadId" ).where( where ).find
		
		'帖子不存在
		if topicRs.eof then
			that.u(topicRS)
			K_("bbs--Public").show404
			response.end
		end if		
		
		Call K_("bbs--public").getHistoryTopic( topicRS("SortID") )
		Call K_("bbs--public").setHistoryTopic( id )
		
		POP_MVC.get("SortID") = topicRS("SortID")
		
		that.d("AddSortID") = topicRS("SortID")
		

		if S_("adminId") <> topicRs("UserID")  then
			if S_("IsAdmin") <> 1 then
				if topicRS("ContentStatus") = 0 then
					K_("bbs--Public").showError( "该帖子尚在审核中，暂不能查看" )
					response.end
				elseif topicRS("ContentStatus") = 2 then
					K_("bbs--Public").showError( "该帖子已经被删除" )
					response.end			
				end if
			else
				if topicRS("ContentStatus") = 2 then
					'判断普通管理员是否在查看管理员已删除帖子
					authInfo = K_("bbs--Public").AuthCompare( topicRs("UserID") , "的已删除帖子进行查看！" )
					if authInfo <> "" then
						K_("bbs--Public").showError( authInfo )
						response.end	
					end if							
				end if
			end if
		end if
		
		
		editCount = topicRS("EditCount")
		if isNul( editCount ) then
			editCount = 0
		end if
		that.d("editCount") = editCount

		that.d("thread") = topicRS

		
		that.d("IsReplyView") = getReplyView(topicRS)
		
		'登陆后的回帖提示
		that.d("placeholder") = guestConfig.EditorLoginReplyTip


		that.d("summernote") = ""		

		'''''回复内容'''''
		if guestConfig.ReplyAddAllow - 0 = 0 then
			'是否允许回帖
			NoReplyTip = guestConfig.ReplyAddAllowTip
		elseif topicRS("IsNoComment") = 1 then
			'帖子禁回提示
			NoReplyTip = guestConfig.ReplyNoCommentTip
		elseif datediff( "d" , topicRS("AddTime") , now ) - guestConfig.ReplyTimeLimit > 0 and S_("IsAdmin") - 1 <> 0 then
			'超回帖时间提示
			NoReplyTip = guestConfig.ReplyTimeLimitTip
		elseif topicRS("ReplyCount") - guestConfig.ReplyCountLimit >= 0 then
			'最多回帖提示
			NoReplyTip = guestConfig.ReplyCountLimitTip
		else
			POP_MVC.config( "AUTO_EXTENDS_MODULE" ) = "summernote"
			that.d("summernote") = P_("auto").summernote("#L_content")		
		
			if S_("adminId") = "" then
				'未登陆状态的回帖提示
				NoReplyTip = guestConfig.EditorReplyTip
			elseif guestConfig.ReplyDayLimit - 0 <> 0 and S_("IsAdmin") <> 1 then
				'超日回复数提示，管理员例外
				set where2 = D_
				where2("UserID") = S_("adminId")
				where2("AddTime") = array( "daydiff" , 1 )

				'回帖达到上限后的提示
				if B_( baseTable ).from("{prefix}self_GuestReply").where( where2 ).count - guestConfig.ReplyDayLimit >= 0  then
					NoReplyTip = guestConfig.ReplyDayLimitTip
				elseif B_( baseTable ).from("{prefix}User").where(S_("adminId")).field("ReplyStatus").getOne = 0 then
					'用户禁回帖提示
					NoReplyTip = guestConfig.ReplyForbidTip
				end if
			end if			
		end if
		that.d("NoReplyTip") = NoReplyTip
		'''''回复内容Recordset'''''
		that.u(where)
		
		set where = D_
		
		where("a.TopicID") = id
		where("a.UserID = b.UserID") = null
		where("b.GroupID = c.GroupID") = null
		where("a.ContentStatus") = 1

		if isOnlyUser then
			UserID = that.get("UserID")
			where("(a.UserID = " & UserID & " OR inStr(a.Content, '<a href=""?u_" & UserID & """') > 0  )") = null
		end if
			
		field = C_("REPLY_FIELD") & ",UploadId"
		
		set replyRS= B_( baseTable ).onlysql(0).page( "null,10" ).from(C_("REPLY_TABLE")).field(field).order("a.IsAdopted DESC,a.ReplyID ASC").where( where ).select
		'POP_MVC.ajax(replyRS.source )
		
		that.d("reply") = replyRS			

		arr = array()
		replyZanArr = array()
		
		that.d("IsZanTopic") = false
		
		if S_("adminId") <> "" then
			set diggRS = B_( baseTable ).from("{prefix}self_GuestDigg").where( "UserID=" & S_("adminId") ).field("TopicZan,ReplyZan").find
			if not diggRS.eof then
				topicZanStr = diggRS("TopicZan")
				replyZanStr = diggRS("ReplyZan")
				if not isNul( topicZanStr ) then
					topicZanStr = POP_MVC.trim(topicZanStr, ",")
					if inStr( "," & topicZanStr & "," , "," & id & "," ) > 0 then
						that.d("IsZanTopic") = true
					end if
				end if
				if not isNul( replyZanStr ) then
					replyZanStr = POP_MVC.trim(replyZanStr, ",")
					replyZanArr = split( replyZanStr , "," )
				end if
			end if
		end if
		
		that.d("theArr") = arr
		that.d("reArr") = replyZanArr

	
		Call getHotTopic( topicRS("SortID") )
	
		dim obj
		set obj = objCMS
		template = "jie/detail"
		if POP_MVC.file.isFile( obj.getTemplatePath( template & "#" & topicRS("SortID") ) ) then
			template = template & "#" & topicRS("SortID")
		end if
		templatePath= obj.getTemplatePath( template )
		Call obj.CompileContent( templatePath )
		POP_MVC.get("theSortID") = topicRS("SortID")
		obj.parseHtml
		obj.output
		Response.flush
		B_( "self_GuestTopic").where( id ).setInc("Visits")
		Call clearRS
	end sub
	
	'获取置顶帖子
	Private sub GetTopData(SortID)
		dim where,rs
		
		if guestConfig.FetchSortTop - 0 = 0 then
			exit sub
		end if
		
		if guestConfig.IndexTopCount - 0 <= 0 then
			exit sub
		end if
		
		if isCached then
			exit sub
		end if
		
		'''''''''''公共条件开始'''''''''''
		set where = D_		
			
		where("a.UserID = c.UserID ") = null
		where("b.GroupID = c.GroupID") = null
		
		if SortID <> "" and SortID <> 0 then
			where("a.SortID") = SortID	
		end if
			
		where("a.ContentStatus") = 1		
		'''''''''''公共条件结束'''''''''''
		
		
		'''''''''''取置顶帖子开始'''''''''''
		where("IsTop") = 1
		orderStr2 = "TopicID DESC"
		
		set rs = B_( baseTable ).onlysql(0).top( guestConfig.IndexTopCount ).from( C_("THREAD_TABLE") ).field( TopicFieldStr ).order(orderStr2).where( where ).select
		
		that.d("topCount") = rs.recordCount
		
		that.d("topList") = rs
		'''''''''''取置顶帖子结束'''''''''''
	End sub
	
	'本月/周热议
	Private sub getHotTopic(SortID)
		dim where,rs
		
		if POP_MVC.a = "Index" AND guestConfig.FetchSortHot - 0 = 0 then
			exit sub
		end if
		
		if POP_MVC.a = "Detail" AND guestConfig.FetchDetailHot - 0 = 0 then
			exit sub
		end if
			
		if objCMS.isCached( "hotThread_" & SortID , "" ) then
			exit sub
		end if
		
		
		set where = D_
		
		
		if SortID <> "" and SortID <> 0 then
			where("a.SortID") = Array("IN",SortID)			
		end if
		where("a.TopicID = b.TopicID") = null
		where("a.ReplyCount") = Array(">",0)
		where("a.ContentStatus") = 1
		where( "b.AddTime" ) = Array( "daydiff" , guestConfig.IndexHotDate )

		
		'本周热议
		sql = B_( baseTable ).from( "{prefix}" & tableName).onlysql(1).order("count(a.TopicID) desc").field("a.TopicID").from("{prefix}self_GuestTopic as a, {prefix}self_GuestReply as b").where( where ).group("a.TopicID").top(15).select
		
		set hotKV = B_( baseTable ).from( "{prefix}" & tableName).onlysql(0).order("count(a.TopicID) desc").field("a.TopicID,count(a.TopicID) as cnt").from("{prefix}self_GuestTopic as a, {prefix}self_GuestReply as b").where( where ).group("a.TopicID").top(15).getKeyValue
		
		that.d("hotKV") = hotKV
		
		if POP_MVC.config("DB_TYPE") = "mysql" then
			sql = "SELECT TopicID FROM ( " & sql & " ) as c"
		end if
	
		set where = D_
		where( "TopicID IN (" & sql & ")" ) = null
		

		set rs = B_( baseTable ).from( "{prefix}" & tableName).ResumeOpts().onlysql(0).field("TopicID,Title,ReplyCount,IndexImage,DownURL").from("{prefix}self_GuestTopic").where(where).order("ReplyCount DESC").select
		
		that.d("HotTopicCount") = rs.recordCount
		that.d("HotTopic") = rs			
	end sub
	
	'最新回帖
	Private sub getNewReply(SortID)
		dim where,rs,ids,dict
		
		if guestConfig.FetchSortReply - 0 = 0 then
			exit sub
		end if		
		
		set where = D_
		
		if objCMS.isCached( "newReply_"  & SortID , "" ) then
			exit sub
		end if
		
		'where( "AddTime" ) = Array( "daydiff" , guestConfig.IndexHotReplyDate )
				
		set where = D_
		where("a.ContentStatus") = 1
		where("b.ContentStatus") = 1
		where("a.TopicID = b.TopicID") = null
		where("b.UserID = c.UserID") = null
		
		if SortID <> "" and SortID <> 0 then
			where("a.SortID") = Array("IN",SortID)
		end if
		set rs = B_(tableName).field("a.TopicID,Title,b.IndexImage,b.Content,b.AddTime as ReplyTime,b.ZanCount,ReplyID,c.Avatar,c.Nickname,c.LoginName,a.UserID").from("{prefix}self_GuestTopic as a, {prefix}self_GuestReply as b, {prefix}User as c").where( where ).top(10).order("ReplyID DESC").select		

		that.d("NewReply") = rs	
		that.d("NewReplyCount") = rs.RecordCount
	end sub
	
	private sub test
		'response.write B_( Array( "content" , "access" , "/iaspcms.mdb" ) ).field("Title,SortID,PageDesc,PageKeywords,IndexImage,IsNoComment,IsTop,IsRecommend,IsHeadline,IsFeatured,Visits,Star,AddTime,DownURL,ContentStatus,EditTime,ContentID as TopicID,ImagePath").xml("Blog" , "")
		
		var_export POP_MVC.file.getFilesMap("./templates/bbs/html","",-1)
		response.end
	end sub
	
	private sub clearRS
		if POP_MVC.config("SHOW_PAGE_TRACE") = 0 then
			Call POP_MVC.ClearSqlRS("*")
		end if
	end sub
end Class
%>