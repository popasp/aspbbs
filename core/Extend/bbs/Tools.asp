<%
Class [Tools]
	Public property Get js_Auto( ByVal arg )
		dim key
		if typename(arg) = "Recordset" then
			set arg = POP_MVC.rs2dict(arg)
			for each key in arg
				if isDate( arg(key) ) then
					arg(key) = POP_MVC.FormatDate( arg(key) , POP_MVC.config( "WDATE_DATE_FORMAT" ) )
				end if
			next
		end if
		that.js_auto( arg )
	End property
	
	'安装数据表
	Public Property Get InstallTable( CtrlName,ExtendTable, sqlfile )
		dim sql
		if session("GroupID") <> 1 then
			that.error( "您无权操作！" )
		end if
		
		if not POP_MVC.file.isFile( sqlfile ) then
			that.error( "找不到当前数据库对应的插件数据表!" )
		end if
		if not B_( objCMS.baseTable ).tableExists(ExtendTable) then	
			sql = POP_MVC.file_get_contents( sqlfile )
			Call B_( objCMS.baseTable ).Exec( sql )
			InstallTable = true
			that.success( Array("插件数据表安装完成!" , "?" & CtrlName & "--Admin#admin" ) )
		else
			InstallTable = false
			that.error( Array("插件数据表已经存在!" , "?" & CtrlName & "--Admin#admin" ) )
		end if
	End Property
	
	'缷载数据表
	Public Property Get UnstallTable( CtrlName,ExtendTable)
		if session("GroupID") <> 1 then
			that.error( "您无权操作！" )
		end if	
	
		if B_( objCMS.baseTable ).tableExists(ExtendTable) then
			Call B_( objCMS.baseTable ).DropTable(ExtendTable)
			UnstallTable = true
			that.success( Array("插件数据表缷载完成!" ,"?" & CtrlName & "--Admin#admin" ) )
		else
			UnstallTable = false
			that.error( Array("插件数据表不存在!" ,"?" & CtrlName & "--Admin#admin" ) )		
		end if
	End Property
	
	'生成建表语句
	Public Property Get createSql( ExtendTable , sqlfile )
		dim sql
		
		if session("GroupID") <> 1 then
			that.error( "您无权操作！" )
		end if		
		
		sql = B_( objCMS.baseTable ).getCreateSQL(ExtendTable)
		Call POP_MVC.file_put_contents_without_bom( sqlfile , sql )
		Call K_( "bbs--Public" ).CtrlLog( 1, "用户ID: " & S_("adminId") & " 生成" & tableName & "建表语句" , "创建成功" )
	End Property
	
	'查看建表语句
	Public Property Get viewSql( sqlfile )
		if session("GroupID") <> 1 then
			that.error( "您无权操作！" )
		end if	
	
		response.write P_("auto").getPrismFile( sqlfile, "basic")
	End Property
	
	'查看建表语句
	Public Property Get viewXml( xmlfile )
		if session("GroupID") <> 1 then
			that.error( "您无权操作！" )
		end if	
	
		response.write P_("auto").getPrismFile( xmlfile, "xml")
	End Property	
	
	Public Sub setTemplate(tpl)
		Call K_("bbs--Public").setCookie( "SystemTemplate" , tpl )
	End Sub
End Class
%>