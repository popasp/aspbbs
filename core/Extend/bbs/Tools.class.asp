<%
Class Tools  'Extends bbs--Top
	private tableName
	sub initialize
		tableName = "User"
		that.d("layuicachepage") = "user"
	end sub
	
	'更新历史
	sub changelog
		objCMS.show( "self/changelog" )
	end sub
	

	
	'所有图标
	sub icon
		dim arr,content,arr2
		if POP_MVC.file.isFile("templates/bbs/res/css/iconfont.svg") then
			content = POP_MVC.file_get_contents("templates/bbs/res/css/iconfont.svg")
			arr = POP_MVC.String.reg_array( content , "unicode=\""(.*?)\""" , 1,"gim")
		else
			arr = Array()
		end if
		
		if POP_MVC.file.isFile("templates/bbs/res/css/global.css") then
			content = POP_MVC.file_get_contents("templates/bbs/res/css/global.css")
			arr2 = POP_MVC.String.reg_array( content , ".(icon-\w+)\:before" , 1,"gim")
		else
			arr2 = Array()
		end if
		
		
		that.d("iconArr") = arr
		that.d("iconArr2") = arr2
			
		objCMS.bbsshow( "tool/icon" )
	end sub
	
	'所有图标
	sub icon2
		dim arr,content,arr2
		if POP_MVC.file.isFile("templates\xiunobbs\view\css\bootstrap.css") then
			content = POP_MVC.file_get_contents("templates\xiunobbs\view\css\bootstrap.css")
			arr = POP_MVC.String.reg_array( content , "\.(icon(\-\w+)+)",1,"gim")
		else
			arr = Array()
		end if
		
		arr2 = Array()
		
		that.d("iconArr") = arr
		that.d("iconArr2") = arr2
			
		objCMS.bbsshow( "tool/icon2" )
	end sub
	
	'头像
	sub avatar
		dim arr,path,arr2		
		path = objCMS.getTplFolderPath() & "/res/images/" & guestConfig.AvatarType		
		that.d("path") = path
		objCMS.bbsshow( "tool/avatar" )
	end sub
	

	
	sub AnalyseLabel
		var_Export objCMS.AnalyseLabel( ".\templates\bbs\html\jie\detail.html" )
	end sub
end Class
%>