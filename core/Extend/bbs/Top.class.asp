<%
Class Top
	public son
	sub initialize

		if session("adminId") = "" and LCase(POP_MVC.c) <> "bbs--logout" then
			if guestConfig.LoginUseCookie - 0 <> 0 then
				Call A_("bbs--Login/CookieLogin")
			end if
		end if
	
		if session("adminId") <> "" then
			if DATEDIFF( "n" , session("LastLoginTime") , now ) - guestConfig.LoginOnlineLimit >= 0 then				
				if C_("DB_TYPE") = "sqlite3" then
					Call objCMS.setUserData( S_("adminId")  , "LastLoginTime" ,now )
				else
					Call objCMS.setUserData( S_("adminId")  , "LastLoginTime" ,POP_MVC.formatDate( now , "YYYY-MM-DD HH:II:SS" ) )
				end if
				session("LastLoginTime") = now()
			end if
		end if	

		'若尚未登陆，先尝试用cookie登陆
		if guestConfig.IndexLoginView - 1 = 0 and S_("adminId") = "" then
			if that.isAjax then
				that.error( Array( "请先登陆" , getLoginUrl() ) )
			else
				Response.redirect getLoginUrl()
			end if
		end if	
	end sub
	
	'验证码
	sub checkcode
		P_("verify").output
	end sub
	
	sub Empty_
		K_("bbs--Public").showError( "您访问的页面不存在！" )
		response.end
	end sub
end Class
%>