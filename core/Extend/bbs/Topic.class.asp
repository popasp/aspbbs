<%
Class Topic 'Extends bbs--Common
	private tableName,orderStr,isSolved
	sub initialize
		tableName = "self_GuestTopic"
		that.d("layuicachepage") = "jie"
	end sub
	
	'添加页面
	sub add
		dim where,lastSortID,templatePath,AddSortID
		AddSortID = that.get("id")
		
		'非管理员时进行各种判断
		if S_("IsAdmin") <> 1 then
		
			'判断是否允许普通用户发帖
			Call CheckAddAllow	

			'判断用户是否禁止发帖
			Call CheckThreadStatus( S_("adminId") )		
			
			set where = D_
			
			'判断普通用户最大发帖数
			where("UserID") = S_("adminId")			
			if guestConfig.ThreadLimit - 0 <> 0 and B_(tableName).where(where).count - guestConfig.ThreadLimit >= 0 then
				K_("bbs--Public").showError( guestConfig.ThreadLimitTip )
			end if				
			
			'判断普通用户日最多发帖数
			where("UserID") = S_("adminId")
			where("AddTime") = Array( "daydiff" , 0 )
			if guestConfig.ThreadDayLimit - 0 <> 0 and B_(tableName).where(where).count - guestConfig.ThreadDayLimit >= 0 then
				K_("bbs--Public").showError( guestConfig.ThreadDayLimitTip )
			end if		
		end if
		
		'引入summernote之前,要先引入该模块
		POP_MVC.config( "AUTO_EXTENDS_MODULE" ) = "summernote"
		
		'自动化导入summernote
		that.d("summernote") = P_("auto").summernote("#L_content")
		if AddSortID <> "0" and AddSortID <> "" then
			lastSortID = AddSortID
		else
			lastSortID = B_(tableName).field("SortID").where( "UserID = " & S_("adminId") ).order("TopicID DESC").getOne
		end if
		
		that.d("lastSortID") = lastSortID
		
		'将积分用逗号炸开,形成数组
		templatePath= iif( S_("adminId") -1 = 0 ,"jie/admin-add" , "jie/add" )
		objCMS.show( templatePath )
	end sub
	
	sub rowTable
		dim id
		id = that.get("id")
		that.d("errStr") = B_(tableName).fieldrev("Content").where(id).positionTable
		objCMS.show("other/info") 
	end sub
	
	'修改页面
	sub edit	

	
		'判断是否允许普通用户发帖
		if guestConfig.ThreadEditAllow = 0 AND S_("IsAdmin")<>1 then
			K_("bbs--Public").showError( guestConfig.ThreadEditAllowTip )
		end if		
	
		dim id,rs,templatePath,authInfo
	
		'判断用户是否被允许发帖
		Call CheckThreadStatus( S_("adminId") )
		
		'得到帖子记录
		id = that.get("id")
		set rs = B_("self_GuestTopic").where(id).find
		

		
		if rs.eof then
			that.u(rs)
			Call K_( "bbs--Public" ).CtrlLog( 0, "用户 ID: " & S_("adminId") & " 修改不存在的帖子 ID: " & id  , "要修改的帖子不存在!" )
		end if
		
		'判断是否有编辑帖子的权限
		if cstr(rs("UserID")) <> cstr(S_("adminId")) AND S_("IsAdmin") <> 1 then
			Call K_( "bbs--Public" ).CtrlLog( 0, "用户 ID: " & S_("adminId") & " 修改无权编辑的帖子 ID: " & id  , "您没有权限编辑该帖子" )
		end if
		
		authInfo = K_("bbs--Public").AuthCompare( rs("UserID") , "的帖子进行修改操作！" )
		
		if authInfo <> "" then
			Call K_( "bbs--Public" ).CtrlLog( 0, "用户 ID: " & S_("adminId") & " 修改无权编辑的帖子 ID: " & id  , authInfo )
		end if		
		
		that.d("thread") = rs

		POP_MVC.config( "AUTO_EXTENDS_MODULE" ) = "summernote"
		that.d("summernote") = P_("auto").summernote("#L_content")
		templatePath= iif( S_("adminId") -1 = 0 ,"jie/admin-edit" , "jie/edit" )
		objCMS.show( templatePath )
	end sub	
	
	'存草稿
	sub doDraft
		if that.form("TopicID") <> "" then
			Call edit_draft
		else
			Call add_draft
		end if
	end sub
	
	'修改操作
	sub doEdit
		dim id,dict,sTip
		
		'得到帖子ID
		id = that.form("TopicID")
		
		Call CheckEditPower(id)
		
		'得到数据，Dictionary对象
		set dict = POP_MVC.form2dict
		
		'入口处已经过滤，这儿没必要再过滤
		'dict("Title") = POP_MVC.String.strip_tags( dict("Title") , "html" )		
		
		'自动验证与自动填充数据
		if not K_("bbs--Public").ModelCreate("bbs--Thread",dict,2) then	
			sTip = M_( "bbs--Thread" ).db.error
			Call K_( "bbs--Public" ).CtrlLog( 0, sTip , sTip )
		end if
		
		'修改记录成功或失败
		if  K_("bbs--Public").ModelEdit( "bbs--Thread" , null ) then
		
			'修改次数自增1
			if dict("EditCount") = "" then
				Call M_( "bbs--Thread" ).db.where( id ).setField("EditCount",1)
			else
				Call M_( "bbs--Thread" ).db.where( id ).setInc("EditCount")
			end if
			
			'邮件发送提示
			if guestConfig.ThreadEditReminded - 0 = 1 then
				K_("bbs--mail").EditThread( "" )
			end if
			
			'清理缓存
			objCMS.clearCache
			
			'提示
			sTip = iif( guestConfig.ThreadEditCheck=0 OR S_("IsAdmin")=1,  "帖子已经修改，且正式发布！"  ,"帖子已经修改，管理员审核后才可正式发布！")
			
			'that.success( Array( sTip , getThreadLink( id ) ))
			Call K_( "bbs--Public" ).CtrlLog( 1, "用户 ID: " & S_("adminId") & " 对帖子 ID: " & id & " " & sTip , Array( sTip , getThreadLink( id ) ) )
		else
			'B_("self_GuestConfig").Rollback
			sTip = "修改失败，请重新提交"
			'that.error( sTip )
			Call K_( "bbs--Public" ).CtrlLog( 0, "用户 ID: " & S_("adminId") & " 对帖子 ID: " & id & " 修改失败" , sTip )			
		end if		
	end sub
	

	Private Sub ClearMemory( SortID )
		'清空内存首页，与该分类相关的页面
		P_("application").RemovePrefix( "Index/Index" & "_" & guestConfig.DefaultTemplate )
		P_("application").RemovePrefix( "Thread_" & SortID )
	End Sub
	
	'添加操作
	sub doAdd
		if that.form("TopicID") <> "" then
			Call doEdit
			Exit Sub
		end if
		
		dim id,dict,rs,SortID,AnswerReward,sTip
		
		'取发帖用户记录
		set rs = B_("User").where( that.dict( "UserID" , S_("adminId") )  ).find	
		
		Call CheckAddPower( rs )
		
		'积分
		AnswerReward = that.form("AnswerReward")
		
		set dict = POP_MVC.form2dict

		if POP_MVC.String.reg_test( dict("Content") ,  "\[hide\](.+?)\[\/hide\]" , "m" ) then
			dict("IsReplyView") = 1
		end if
		
		'自动验证与自动填充数据
		if not K_("bbs--Public").ModelCreate("bbs--Thread",dict,1) then			
			sTip = M_( "bbs--Thread" ).db.error
			Call K_("bbs--Public").CtrlLog( 0 ,  "用户 ID:" &  S_("adminId") & " 发贴失败，" & sTip  , sTip )
		end if
		
		
		'采用事务处理
		'B_("self_GuestConfig").begin
		
		'得到帖子自增ID
		id = K_("bbs--Public").ModelAdd( "bbs--Thread" , null )
		
		'分到分类ID
		SortID = dict("SortID")

		if  id > 0 then
			'B_("self_GuestConfig").commit
			
			'发帖时减去悬赏分值
			Call B_("User").where( S_("adminId") ).setDec( Array("Experience" , AnswerReward)  )
			
			'奖励积分若非数字改为0
			if not is_numeric( guestConfig.ThreadAddReward ) then
				guestConfig.ThreadAddReward = 0				
			end if
			
			'加上奖励积分
			Call B_("User").where( S_("adminId") ).setInc( Array("Experience" , guestConfig.ThreadAddReward)  )
			
			'清空内存首页，与该分类相关的页面
			Call ClearMemory( SortID )
			
			'发送邮件提示
			if guestConfig.ThreadReminded - 0 = 1 then
				K_("bbs--mail").AddThread( id )
			end if
			
			'发帖是否需要审核，管理员不需要审核
			if guestConfig.ThreadAddCheck - 0 = 0 or S_("IsAdmin") = 1 then
				objCMS.clearCache
				'that.success( Array( "帖子已经添加，且正式发布！" , "?list_" & SortID & guestConfig.PageSuffix ) )
				Call K_("bbs--Public").CtrlLog( 1 , "用户 ID:" &  S_("adminId") & "，发帖成功 ID:" &  id  , Array( "帖子已经添加，且正式发布！" , "?list_" & SortID & guestConfig.PageSuffix ) )
			else				
				'that.success( Array( "帖子已经添加，管理员审核后才可正式发布！"  , "?list_" & SortID & guestConfig.PageSuffix ) )
				Call K_("bbs--Public").CtrlLog( 1 ,  "用户 ID:" &  S_("adminId") & "，成功添加帖子 ID: " &  id & " ，等待管理员审核"  , Array( "帖子已经添加，管理员审核后才可正式发布！"  , "?list_" & SortID & guestConfig.PageSuffix ) )
			end if			
		else
			'B_("self_GuestConfig").Rollback			
			'that.error( "添加失败，请重新提交" )
			Call K_("bbs--Public").CtrlLog( 0 ,  "用户 ID: " &  S_("adminId") & " 添加帖子失败"  , "添加失败，请重新提交" )
		end if		
	end sub
	
	'添加存草稿
	private sub add_draft
		dim id,dict,rs,SortID,AnswerReward
		
		'取发帖用户记录
		set rs = B_("User").where( that.dict( "UserID" ,S_("adminId") )  ).find
		
		Call CheckAddPower( rs )
		
		AnswerReward = that.form("AnswerReward")
		
		set dict = POP_MVC.form2dict				
		'dict("Title") = POP_MVC.String.strip_tags( dict("Title") , "html" )
		
		if not K_("bbs--Public").ModelCreate("bbs--Thread",dict,1) then
			if not K_("CtrlLog--Public") is nothing then
				Call K_("CtrlLog--Public").DoLog( 0 , "用户 ID:" &  S_("adminId") & " 发贴，" & M_( "bbs--Thread" ).db.error, "" )
			end if
		    
			response.write "<script>if(parent.window.layui){ parent.window.layui.use('layer' ,function(){ parent.window.layui.layer.msg('" & M_( "bbs--Thread" ).db.error & "'); } )}else{alert('" & M_( "bbs--Thread" ).db.error & "');}</script>"
			exit sub
		end if
		
		
		'采用事务处理
		'B_("self_GuestConfig").begin
		
		id = K_("bbs--Public").ModelAdd( "bbs--Thread" , null )
		SortID = dict("SortID")

		if  id > 0 then
			'B_("self_GuestConfig").commit
			
			'修改状态
			Call  M_( "bbs--Thread" ).db.where(id).setField( "ContentStatus" , 3 )		
			
			'发帖时减去悬赏分值			
			Call B_("User").where( S_("adminId") ).setDec( Array("Experience" , AnswerReward)  )
			
			'奖励积分若非数字改为0
			if not is_numeric( guestConfig.ThreadAddReward ) then
				guestConfig.ThreadAddReward = 0				
			end if
			
			'加上奖励积分
			Call B_("User").where( S_("adminId") ).setInc( Array("Experience" , guestConfig.ThreadAddReward)  )
			
			'清空内存首页，与该分类相关的页面
			Call ClearMemory( SortID )
			
			'邮件发送提示
			if guestConfig.ThreadReminded - 0 = 1 then
				K_("bbs--mail").AddThread( id )
			end if	

			if not K_("CtrlLog--Public") is nothing then
				Call K_("CtrlLog--Public").DoLog( 1 , "用户 ID:" &  S_("adminId") & " 帖子 ID:" &  id & " 草稿添加成功"  , "" )
			end if			
			
			response.write "<script>window.parent.document.getElementById('TopicID').value='" & id & "';if(parent.window.layui){ parent.window.layui.use('layer' ,function(){ parent.window.layui.layer.msg('草稿添加成功'); } )}else{alert('草稿添加成功');}</script>"
		else
			'B_("self_GuestConfig").Rollback
			
			'回调保存记录
			if not K_("CtrlLog--Public") is nothing then
				Call K_("CtrlLog--Public").DoLog( 0 , "用户 ID:" &  S_("adminId") & " 草稿添加失败"  , "" )
			end if
			
			response.write "<script>if(parent.window.layui){ parent.window.layui.use('layer' ,function(){ parent.window.layui.layer.msg('草稿保存失败'); } )}else{alert('草稿保存失败');}</script>"
		end if
	end sub
	
	'修改存草稿
	private sub edit_draft
		dim id,dict
		
		id = that.form("TopicID")
		
		Call CheckEditPower(id)
		
		set dict = POP_MVC.form2dict	
		
		'dict("Title") = POP_MVC.String.strip_tags( dict("Title") , "html" )		
		
		if not K_("bbs--Public").ModelCreate("bbs--Thread",dict,2) then
			if not K_("CtrlLog--Public") is nothing then
				Call K_("CtrlLog--Public").DoLog( 0 , "用户 ID:" &  S_("adminId") & " 草稿修改失败，ID: " & id  , "" )
			end if	
			
			response.write "<script>if(parent.window.layui){ parent.window.layui.use('layer' ,function(){ parent.window.layui.layer.msg('" & M_( "bbs--Thread" ).db.error & "'); } )}else{alert('" & M_( "bbs--Thread" ).db.error & "');}</script>"
			exit sub
		end if
		
		if  K_("bbs--Public").ModelEdit( "bbs--Thread" , null ) then
			Call  M_( "bbs--Thread" ).db.where(id).setField( "ContentStatus" , 3 )
			if guestConfig.ThreadEditReminded - 0 = 1 then
				K_("bbs--mail").EditThread( "" )
			end if
			
			if not K_("CtrlLog--Public") is nothing then
				Call K_("CtrlLog--Public").DoLog( 1 , "用户 ID:" &  S_("adminId") & " 草稿修改成功，ID: " & id  , "" )
			end if	
			
			response.write "<script>if(parent.window.layui){ parent.window.layui.use('layer' ,function(){ parent.window.layui.layer.msg('草稿保存成功'); } )}else{alert('草稿保存成功');}</script>"
		else
			'B_("self_GuestConfig").Rollback
			
			if not K_("CtrlLog--Public") is nothing then
				Call K_("CtrlLog--Public").DoLog( 1 , "用户 ID:" &  S_("adminId") & " 草稿修改失败，ID: " & id  , "" )
			end if
			
			response.write "<script>if(parent.window.layui){ parent.window.layui.use('layer' ,function(){ parent.window.layui.layer.msg('草稿保存失败'); } )}else{alert('草稿保存失败');}</script>"
		end if		
	end sub
	
	'置顶
	sub setTop
		dim field,value,UserID,id,authInfo,sTip
		if not that.isSelfOrigin then
			that.error("非法操作")
		end if	
		
		id		= that.req("id")
		UserID	= B_( tableName ).where( id ).field("UserID").getOne
		
		authInfo = K_("bbs--Public").AuthCompare( UserID , "的帖子进行该操作！" )
		
		field	= "IsTop"
		value	= 1		
		
		sTip = "置顶成功"
		objCMS.clearCache
		Call B_(tableName).where( id ).setField( field , value  )
		Call K_( "bbs--Public" ).CtrlLog( 1, "用户ID: " & S_("adminId") & " ，对帖子ID: " & id & " " & sTip , sTip )		
	end sub
	
	'置顶
	sub CancelTop
		dim field,value,UserID,id,authInfo,sTip
		if not that.isSelfOrigin then
			that.error("非法操作")
		end if	
		
		id		= that.req("id")
		UserID	= B_( tableName ).where( id ).field("UserID").getOne
		
		authInfo = K_("bbs--Public").AuthCompare( UserID , "的帖子进行该操作！" )
		
		field	= "IsTop"
		value	=  0	
		
		sTip = "成功取消置顶"
		objCMS.clearCache
		Call B_(tableName).where( id ).setField( field , value  )
		Call K_( "bbs--Public" ).CtrlLog( 1, "用户ID: " & S_("adminId") & " ，对帖子ID: " & id & " " & sTip , sTip )		
	end sub
	
	'加精
	Sub SetFeature
		dim field,value,UserID,id,authInfo,sTip
		if not that.isSelfOrigin then
			that.error("非法操作")
		end if	
		
		id		= that.req("id")
		field	= "IsFeatured"
		value	= 1
		UserID	= B_( tableName ).where( id ).field("UserID").getOne
		
		authInfo = K_("bbs--Public").AuthCompare( UserID , "的帖子进行该操作！" )
		
		if authInfo <> "" then
			Call K_( "bbs--Public" ).CtrlLog( 0, "用户ID: " & S_("adminId") & " ，设置字段对帖子ID: " & id & " " & authInfo , authInfo )
		end if				
		objCMS.clearCache
		if B_(tableName).where( id ).setField( field , value  ) then
			Call B_("User").where( UserID ).setInc( Array(  "Experience" , guestConfig.ThreadFeaturedReward )  )
			sTip = "加精成功，奖励 " & guestConfig.ThreadFeaturedReward & " 积分"
			Call K_( "bbs--Public" ).CtrlLog( 1, "用户ID: " & S_("adminId") & " ，对帖子ID: " & id & " " & sTip , sTip )			
		end if		
	End Sub
	
	'取消加精
	Sub CancelFeature
		dim field,value,UserID,id,authInfo,sTip
		if not that.isSelfOrigin then
			that.error("非法操作")
		end if	
		
		id		= that.req("id")
		field	= "IsFeatured"
		value	= 0
		UserID	= B_( tableName ).where( id ).field("UserID").getOne
		
		authInfo = K_("bbs--Public").AuthCompare( UserID , "的帖子进行该操作！" )
		
		if authInfo <> "" then
			Call K_( "bbs--Public" ).CtrlLog( 0, "用户ID: " & S_("adminId") & " ，设置字段对帖子ID: " & id & " " & authInfo , authInfo )
		end if				
		objCMS.clearCache
		if B_(tableName).where( id ).setField( field , value  ) then
			Call B_("User").where( UserID ).setInc( Array(  "Experience" , guestConfig.ThreadFeaturedReward )  )
			sTip = "加精成功，奖励 " & guestConfig.ThreadFeaturedReward & " 积分"
			Call K_( "bbs--Public" ).CtrlLog( 1, "用户ID: " & S_("adminId") & " ，对帖子ID: " & id & " " & sTip , sTip )		
		end if		
	End Sub
	
	
	'设置字段的值，加精/置顶
	'这儿的操作进行了权限验证
	Sub setField
		dim field,value,UserID,id,authInfo,sTip
		if not that.isSelfOrigin then
			that.error("非法操作")
		end if	
		
		id		= that.req("id")
		field	= that.req("field")
		value	= that.req("rank")
		UserID	= B_( tableName ).where( id ).field("UserID").getOne
		
		if id ="" then
			that.error("请选择帖子再操作")
		end if
		
		authInfo = K_("bbs--Public").AuthCompare( UserID , "的帖子进行该操作！" )
		
		if authInfo <> "" then
			Call K_( "bbs--Public" ).CtrlLog( 0, "用户ID: " & S_("adminId") & " ，设置字段对帖子ID: " & id & " " & authInfo , authInfo )
		end if				
		objCMS.clearCache
		if B_(tableName).where( id ).setField( field , value  ) then
			if field = "IsFeatured" then				
				if value - 0 = 0 then
					Call B_("User").where( UserID ).setDec( Array(  "Experience" , guestConfig.ThreadFeaturedReward )  )
					sTip = "取消加精成功，减去 " & guestConfig.ThreadFeaturedReward & " 积分"
					Call K_( "bbs--Public" ).CtrlLog( 1, "用户ID: " & S_("adminId") & " ，对帖子ID: " & id & " " & sTip , sTip )
				else
					Call B_("User").where( UserID ).setInc( Array(  "Experience" , guestConfig.ThreadFeaturedReward )  )
					sTip = "加精成功，奖励 " & guestConfig.ThreadFeaturedReward & " 积分"
					Call K_( "bbs--Public" ).CtrlLog( 1, "用户ID: " & S_("adminId") & " ，对帖子ID: " & id & " " & sTip , sTip )
				end if
			elseif field = "IsTop" then
				if value - 0 = 1 then	
					sTip = "置顶成功"
					Call K_( "bbs--Public" ).CtrlLog( 1, "用户ID: " & S_("adminId") & " ，对帖子ID: " & id & " " & sTip , sTip )
				else
					sTip = "完成取消置顶"
					Call K_( "bbs--Public" ).CtrlLog( 1, "用户ID: " & S_("adminId") & " ，对帖子ID: " & id & " " & sTip , sTip )
				end if
			elseif field = "ContentStatus" then
				if value - 1 = 0 then
					sTip = "恢复成功"
					Call K_( "bbs--Public" ).CtrlLog( 1, "用户ID: " & S_("adminId") & " ，对帖子ID: " & id & " " & sTip , sTip )
				elseif value - 0 = 0 then
					sTip = "关闭成功"
					Call K_( "bbs--Public" ).CtrlLog( 1, "用户ID: " & S_("adminId") & " ，对帖子ID: " & id & " " & sTip , sTip )
				else
					that.error("未允许操作")
				end if
			else
				that.success("操作成功")
			end if			
		end if		
	End Sub

	'删除帖子
	'作者只能软删除
	'管理员第一次操作是软删除，第二次操作则会永久删除
	Sub Remove
		dim id,rs,authInfo,sTip
		id = that.req("id")
		

	
		'来路不对
		if not that.isSelfOrigin then
			that.error("非法操作")
		end if			
		
		'不是有效的ID
		'if not that.isId( id ) then
		'	that.error( "非法操作" )
		'end if
		
		'根据ID取记录
		set rs = B_(tableName).where(id).find
		
		authInfo = K_("bbs--Public").AuthCompare( rs("UserID") , "的帖子进行删除操作！" )
		
		if authInfo <> "" then
			Call K_( "bbs--Public" ).CtrlLog( 0, authInfo , authInfo )
		end if
		
		'清空内存首页，与该分类相关的页面
		Call ClearMemory( rs("SortID") )
		
		if S_("IsAdmin") <> 1 then
			if S_("adminId") - rs("UserID") = 0 then
				Call B_(tableName).where(id).setField( "ContentStatus" , 2 )
				objCMS.clearCache
				
				sTip = "删除成功"
				Call K_( "bbs--Public" ).CtrlLog( 1, "用户ID: " & S_("adminId") & " ，对帖子ID: " & id & " 软" & sTip , sTip )
			else
				Call K_( "bbs--Public" ).CtrlLog( 0, "用户ID: " & S_("adminId") & " ，对帖子ID: " & id & " 非作者非管理员，无权删除" , "您不是管理员，无权对帖子进行硬删除！" )
			end if
		else
			if rs("ContentStatus") = 1 then
				Call B_(tableName).where(id).setField( "ContentStatus" , 2 )
				objCMS.clearCache
				
				sTip = "删除成功"
				Call K_( "bbs--Public" ).CtrlLog( 1, "用户ID: " & S_("adminId") & " ，对帖子ID: " & id & " 软" & sTip , sTip )
			else
				Call CheckSuperAdmin
			
				'彻底删除帖子的时候，还要彻底删除回帖
				B_(tableName).where(id).remove
				B_("self_GuestReply").where( that.dict( "TopicID" , id ) ).remove
				objCMS.clearCache
				sTip = "删除成功"
				Call K_( "bbs--Public" ).CtrlLog( 1, "用户ID: " & S_("adminId") & " ，对帖子ID: " & id & " " & sTip , Array(sTip ,"" ) )
			end if
		end if		
	End Sub
	
	
	'将帖子全部移除
	sub delete
		dim id,rs,where,refer
		id = that.req("id")
		
		'来路不对
		if not that.isSelfOrigin then
			that.error("非法操作")
		end if	

		Call CheckSuperAdmin		
		
		set where = D_
		
		refer = Lcase(that.refer)
		
		if inStr( refer, "recycle" ) > 0 then
			where( "ContentStatus" ) = 2
		elseif inStr( refer, "drafts" ) > 0 then
			where( "ContentStatus" ) = 3
		elseif inStr( refer, "threadcheck" ) > 0 then
			where( "ContentStatus" ) = 0
		end if	
		
		if id <> "" then
			where("TopicID") = Array( "in" , id )
		end if
		
		Call B_(tableName).where(where).remove
		objCMS.clearCache
		Call K_( "bbs--Public" ).CtrlLog( 1, "用户ID: " & S_("adminId") & " ，对帖子ID: " & id & " 成功移除" , Array("移除成功" ,"" ) )
	end sub
	
	'将帖子全部恢复
	sub restore
		dim id,rs,where,refer
		id = that.req("id")
		
		'来路不对
		if not that.isSelfOrigin then
			that.error("非法操作")
		end if	

		Call CheckSuperAdmin	
		
		set where = D_
		
		refer = Lcase(that.refer)
		
		if inStr( refer, "recycle" ) > 0 then
			where( "ContentStatus" ) = 2
		elseif inStr( refer, "drafts" ) > 0 then
			where( "ContentStatus" ) = 3
		elseif inStr( refer, "threadcheck" ) > 0 then
			where( "ContentStatus" ) = 0
		end if			
		
		
		if id <> "" then
			where("TopicID") = Array( "in" , id )
		end if
		
		Call B_(tableName).where(where).setField( "ContentStatus" , 1 )
		objCMS.clearCache
		Call K_( "bbs--Public" ).CtrlLog( 1, "用户ID: " & S_("adminId") & " ，对帖子ID: " & id & " 成功恢复" , Array("恢复成功" ,"" ) )
	end sub
	
	'上传图片
	'该方法即将废止
	sub upload
		dim filename,dict,logID
		POP_MVC.config("UPLOAD_ALLOW_TYPES") = POP_MVC.config("UPLOAD_IMAGE_TYPES")
		filename = POP_MVC.upload("file")
		set dict  = D_
		filename = K_("bbs--Public").upload( "Topic-upload" , logID )
		if filename <> "" then
			dict("status") = that.ajaxSuccessStatus
			dict("url") = filename
			dict("filename") = POP_MVC.file.basename( filename )			
		else
			dict("status") = that.ajaxErrorStatus
			dict("msg") = POP_MVC.Uploader.description
		end if
		that.ajaxData(dict)
	end sub	
	
	'从论坛全局判断是否允许普通用户发帖
	Private Sub CheckAddAllow
		if guestConfig.ThreadAddAllow - 0 = 0 then
			K_("bbs--Public").showError( guestConfig.ThreadAddAllowTip )
		end if	
	End Sub
	
	'判断单个用户是否被允许发帖
	Private Sub CheckThreadStatus( UserID )	
		if B_("User").where( "UserID = " & UserID ).field("ThreadStatus").getOne = 0 then
			K_( "bbs--Public" ).showError( guestConfig.ThreadForbidTip  )
		end if	
	End Sub
	
	'判断是否为超管
	Private Sub CheckSuperAdmin
		if S_("GroupID") <> 1 then
			that.error("您不是超级管理员，无权进行彻底删除操作！")
		end if
	End Sub
	
	
	'判断编辑
	private Sub CheckEditPower(id)
		'非POST请求必属攻击
		if NOT POP_MVC.isPost then
			Call K_( "bbs--Public" ).CtrlLog( 0, "用户 ID: " & S_("adminId") & " 通过非POST修改帖子 ID: " & id & " ，建议禁掉该帐户"  , "非法操作" )
		end if
		
		'判断是否允许普通用户改帖
		if guestConfig.ThreadEditAllow - 0 = 0 AND S_("IsAdmin") <> 1 then
			K_("bbs--Public").showError( guestConfig.ThreadEditAllowTip )
		end if			
		
		'判断用户是否禁止发帖
		Call CheckThreadStatus( S_("adminId") )
	End Sub
	
	'判断发帖
	private Sub CheckAddPower( rs )
		dim AnswerReward
		
		'非POST请求必属攻击
		if NOT POP_MVC.isPost then
			Call K_( "bbs--Public" ).CtrlLog( 0, "用户 ID: " & S_("adminId") & " 通过非POST发帖" & " ，建议禁掉该帐户"  , "非法操作" )
		end if
		
		'判断是否允许普通用户发帖
		Call CheckAddAllow
		
		'判断用户是否禁止发帖
		if rs("ThreadStatus") = 0 then
			K_( "bbs--Public" ).showError( guestConfig.ThreadForbidTip  )
		end if	
		
		'积分
		AnswerReward = that.form("AnswerReward")
		
		'积分为空，则取0
		if AnswerReward = "" then
			AnswerReward = 0
		end if
		
		'判断剩余积分是否够发帖
		if rs("Experience") - AnswerReward < 0 then
			'that.error( "您的积分不够，不能发帖！" )
			Call K_("bbs--Public").CtrlLog( 0 ,  "用户 ID:" &  S_("adminId") & " 积分不够，不能发帖"  , "您的积分不够，不能发帖！" )
		end if
	End Sub
end Class
%>