<%
Class U  'Extends bbs--Top
	private tableName
	sub initialize

		tableName = "User"
		that.d("layuicachepage") = "user"
	end sub
	
	'用户主页
	sub index
		dim obj,templatePath,UserStatus
		set obj = objCMS
		
		dim userRS,UserID,where
		
		UserID = that.get("id")

		set userRS = B_( objCMS.baseTable ).where( that.dict( "UserID" , UserID ) ).find
		
		if userRS.eof then
			that.u(userRS)
			K_("bbs--Public").show404
		end if
		
		UserStatus = userRS("UserStatus")
		
		if UserStatus <> 1 and S_("IsAdmin") <> 1 then
			if UserStatus = 2 then
				K_("bbs--Public").showError( guestConfig.LoginCancelTip )
			elseif UserStatus = 0 then
				K_("bbs--Public").showError( guestConfig.LoginForbidTip )
			end if
		end if
		
		
		that.d("user") = userRS
		that.d("group") = B_( objCMS.baseTable ).from( "{prefix}UserGroup" ).where( "GroupID = " & userRS("GroupID") ).find

'缓存		
if NOT objCMS.isCached( "UserHome_" & UserID , "" ) then		
		
		'提问
		set where = D_
		
		where("ContentStatus") = 1
		where("UserID") = userRS("UserID") 
		that.d("thread") = B_("self_GuestTopic").field("TopicID,Title,AddTime,Visits,ReplyCount,IsTop,IsFeatured,UserID").where( where ).page("null,30").order("TopicID DESC").select
		
		'回复
		set where = D_
		where("a.ContentStatus") = 1
		where("b.UserID") = userRS("UserID") 
		where("a.TopicID = b.TopicID") = null
		that.d("reply") = B_("self_GuestConfig").field("a.TopicID as tID,a.SortID,a.UserID as TopicUserID,b.UserID as ReplyUserID,a.Title,b.ReplyID,b.Content as ReplyContent,b.AddTime as ReplyTime,a.Visits,a.ReplyCount,a.IsTop,a.IsFeatured,b.ContentStatus").from("{prefix}self_GuestTopic as a, {prefix}self_GuestReply as b").where( where ).page("null,30").order("ReplyID DESC").select
end if

		objCMS.show( "reg/home" )
	end sub
	
	sub timeline
		dim obj,templatePath,rs,dict,data,arr,k,item,str
		set obj = objCMS
		
		dim userRS,UserID,where
		
		set dict = D_
		
		UserID = that.get("id")

		set userRS = B_("User").where( that.dict( "UserID" ,UserID) ).find	
		if userRS.eof then
			that.u(userRS)
			K_("bbs--Public").show404
		end if
		
		that.d("user") = userRS
		
'缓存		
if NOT objCMS.isCached( "UserTimeline_" & UserID , "" ) then
		
		'提问
		set where = D_
		
		where("UserID") = UserID
		where("ContentStatus") = 1
		set rs = B_("self_GuestTopic").field("TopicID,Title,AddTime,Visits,ReplyCount,IsTop,IsFeatured,Content").where( where ).top(500).page("null,30").order("TopicID DESC").select
		do while not rs.eof
			dict( POP_MVC.FormatDate(rs("AddTime") , "YYYY-MM-DD HH:II:SS") ) = "发布了《" & "<a href='" & getThreadLink(rs("TopicID")) & "' class='jie-title'>" & rs("Title") & "</a>" & "》" & " (" & rs("Visits") & "阅/" & rs("ReplyCount") & "答)" & "<p>" & POP_MVC.String.cut( POP_MVC.String.strip_tags( rs("Content") ,"html") , 200 ) & "</p >"
			rs.movenext
		loop
		
		that.u(rs)
		
		'回复
		set where = D_
		where("a.ContentStatus") = 1
		where("b.UserID") = UserID
		where("a.TopicID = b.TopicID") = null
		set rs = B_("self_GuestConfig").field("a.TopicID as tID,a.UserID as TopicUserID,b.UserID as ReplyUserID,Title,b.Content as ReplyContent,a.AddTime as TopicTime,b.AddTime as ReplyTime,a.Visits,a.ReplyCount,a.IsTop,a.IsFeatured,b.ContentStatus").from("{prefix}self_GuestTopic as a, {prefix}self_GuestReply as b").where( where ).top(500).page("null,30").order("ReplyID DESC").select
		
		do while not rs.eof
			if rs("ContentStatus") = 1 then
				str = objCMS.ModifyContent( rs("ReplyContent")  )
			else
				str = C_("DEL_REPLY_TIP")
			end if
		
			dict( POP_MVC.FormatDate(rs("ReplyTime") , "YYYY-MM-DD HH:II:SS") ) = "在《" & "<a href='" & getThreadLink(rs("tID")) & "' class='jie-title'>" & rs("Title") & "</a>" & "》 (" & rs("Visits") & "阅/" & rs("ReplyCount") & "答 ) 中回答:" & "<br />" & str
			rs.movenext
		loop
		
		if rs.recordCount > 0 then
			POP_MVC.Dict.krsort dict
		end if
		
		that.u(rs)
		
		dict( POP_MVC.FormatDate(userRS("RegTime") , "YYYY-MM-DD HH:II:SS")  ) = "注册"	
		
		set data = D_
		
		for each key in dict
			arr = split( key, " " )
			k = arr(0)
			if data.exists( k ) then
				item = data(k)
				POP_MVC.Arr.push item , dict(key)
				data(k) = item
			else
				data.add k,Array(dict(key))
			end if
			dict.remove(key)
		next
		that.u(dict)
		
		for each key in data
			arr = data(key)
			if ubound( arr ) =0 then
				data(key) = arr(0)
			else
				data(key) = ""
				for k = 0 to ubound( arr )
					if k = 0 then
						data( key ) = "<span style='color:#FF5722'>" & (k + 1) & ".</span> " & arr(k)
					else
						data( key ) = data(key) & "<br />" & "<span style='color:#FF5722'>" & (k + 1) & ".</span> " & arr(k)
					end if					 
				next
			end if
		next
		
		that.d("data") = data
end if		

		objCMS.show( "reg/timeline" )
	end sub
end Class
%>