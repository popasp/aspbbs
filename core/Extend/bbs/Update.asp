<%
Class Update
	public countnum
	'sBaseName:文件名 sExtName:文件扩展名 sFileType:文件类型 sSize:文件大小 sPath:文件路径
	Private sBaseName,sExtName,sFileType,sFileName,sSize,sPath
	Private sOldFolder,sNewFolder,sSkipFolder,sNoSkipFolder
	Private sReload,sDirPrefix
	Private sOldPath,sTotalSize,clearDbName
	
	private dstPrefix,dstdb
	private toTables,srcTables
	
	Private Sub Class_Initialize
		countnum = 0
		sTotalSize = 0
		clearDbName = "aspbbs_db.mdb"
		
		
		'数据库路径放到网站目录下面
		dstdb = clearDbName

		
		'目前支持数据迁移的表，除这些之外还有 Language content adv(slide)
		toTables = Array("self_GuestConfig" , "self_GuestConfigGroup" , "self_GuestDigg" , "self_GuestDonate" , "self_GuestMessage" , "self_GuestReply" , "self_GuestTopic" , "User" , "UserGroup" )
		
	end sub
	
	'获取打包后的数据库路径
	Function getPackDB( ByVal OldVer )
		getPackDB = "./" & P_("VERSION").getPackName( OldVer , "" ) & ".data"
	End Function
	
	'获取旧版本版本号
	'从网站根目录config.asp文件中匹配获取
	Function getOldVer( ByVal OldFolder)
		dim configPath
		OldFolder = POP_MVC.rtrim(  POP_MVC.rtrim( OldFolder , "/" ) , "\" )
		configPath = OldFolder & "\#config.asp"
		getOldVer = P_("version").getVersion( configPath )
	End Function
	
	sub flushStart()
		response.write "<center>正在检测待更新文件，检测时间可能有点长，请耐心等待……</center>"
		response.flush
	end sub
	
	sub flushEach( OldFileExists )		
		if not OldFileExists then
			response.write "<center>" & countnum & ". 打包文件 " & replace(sPath , "/" , "\") & " (" & sSize & ") 原文件不存在</center>"
		else
			response.write "<center>" & countnum & ". 打包文件 " & replace(sPath , "/" , "\") & " (" & sSize & ") 文件较新</center>"
		end if
		
		response.flush
	end sub
	
	sub flushEnd()
		response.write "<center>已检测完毕，共发现" & countnum & "个待更新，总大小 " & Byte2size( sTotalSize ) & "</center><center>下一步打包一个初始化了的数据库，请勿离开页面……</center><script>setTimeout(function(){location.href='?update_PackDb'},2000);</script>"
		response.flush
	end sub
	
	'将旧版本与新版本进行比对，将需要更新的文件打包到一个access数据库中
	'生成的数据库名类似iaspcms_update_3.0.0_2.5.2.mdb，末尾的版本号为旧版本的
	Function Pack( ByVal OldFolder , ByVal OldVer )
		Call flushStart
		dim i,NewFolder,SkipFolder,NoSkipFolder,packdb
		
		if not POP_MVC.file.isFolder( OldFolder ) then
			var_export OldFolder & " 文件夹不存在"
			response.end
		end if
		
		'NewFolder为新版需要给旧版更新的目录或文件
		NewFolder =  Array( "./__popasp__" , "./API", "./core", "./static"  , "./default.asp", "./readme.txt" , "./templates" )
		
		'SkipFolder为忽略更新的目录
		SkipFolder = Array( POP_MVC.appPath & "/runtime" , POP_MVC.config("CORE_NAME") & "/" & POP_MVC.config("DATA_NAME"), "./upload" , "./templates/html/self" )
		
		'NoSkipFolder为不能忽略更新的目录，NoSkipFolder如果与SkipFolder冲突，则以NoSkipFolder为准
		NoSkipFolder = Array( )
		
		if isNul( OldVer ) then
			OldVer = getOldVer( OldFolder )
		end if
		
		packdb = getPackDB( OldVer )
		
		P_("ACCESSPACk").DbPath = packdb
		P_("ACCESSPACk").RootPath = "./"
		P_("ACCESSPACk").CreateDb(1)
		
		'得到规范的旧版路径 如: D:\asp\iaspcms2.0
		sOldFolder = OldFolder
		sOldFolder = POP_MVC.rtrim(  POP_MVC.rtrim( sOldFolder , "/" ) , "\" )
		
		'过滤文件夹的形式为数组，或以英文逗号分隔的字符串
		if not isArray( SkipFolder ) then
			if SkipFolder <> "" then
				SkipFolder = split( SkipFolder , "," )
			else
				SkipFolder = Array()
			end if
		end if
		sSkipFolder = SkipFolder
		
		'处理NewFolder
		if not isArray( NewFolder ) then
			if NewFolder <> "" then
				NewFolder = split( NewFolder , "," )
			else
				NewFolder = Array()
			end if
		end if
		
		'处理NoSkipFolder
		if not isArray( NoSkipFolder ) then
			if NoSkipFolder <> "" then
				NoSkipFolder = split( NoSkipFolder , "," )
			else
				NoSkipFolder = Array()
			end if
		end if		
		sNoSkipFolder = NoSkipFolder
		
		'开始检测，并进行打包
		for i = 0 to ubound( NewFolder )
		
			'如果是目录则遍历目录
			if POP_MVC.file.IsFolder( NewFolder(i) ) then
				sNewFolder = NewFolder(i)
				Call POP_MVC.File.files_map( sNewFolder , "K_(""bbs--Update"").FileAction")
			elseif POP_MVC.file.IsFile( NewFolder(i) ) then
			'如果是文件则单独比较文件
				Call FileAction( NewFolder(i) )
			end if
		next
		Call POP_MVC.File.files_map( sNewFolder , "K_(""bbs--Update"").FileAction")		
		Call K_("bbs--system").getClearDatabase( clearDbName )
		Call FlushEnd()
	End Function	

	'打包新版初始化后的数据库
	'Pack与PackDB要一起使用
	Sub PackDB(  ByVal OldFolder , ByVal OldVer )
		response.write "<center>正在打包数据库……</center>"
		response.flush
		dim dbPath,thepackdb
		
		if isNul( OldVer ) then
			OldVer = getOldVer( OldFolder )
		end if
		
		thepackdb = getPackDB( OldVer )
		
		Call POP_MVC.file.AccessCompress( thepackdb )
		
		'得到规范的旧版路径 如: D:\asp\iaspcms2.0
		sOldFolder = OldFolder
		
		dbPath = clearDbName

		P_("ACCESSPACk").DbPath = thepackdb
		P_("ACCESSPACk").RootPath = "./"
		call FileAction( dbPath )
		POP_MVC.file.AccessCompress( thepackdb )
		Call POP_MVC.file.remove( dbPath )
		response.write "<center style='color:green'>文件与数据库打包完成，打包文件为 " & POP_MVC.file.basename(thepackdb) & "。<a href='?Update_Pack'>重新打包</a></center>"
	End sub

	'将文件打包到access数据库的核心函数
	Sub FileAction(file)
		
		on error resume next
		dim path , bool , size
		
		if isObject( file) then
			path = file.path
			size = file.size
		else
			path = POP_MVC.realpath(file)
			size = POP_MVC.file.fileSize(path)
		end if
		
		sFileType=POP_MVC.file.getFileType( path )
		path = mid( path,len(POP_MVC.realpath("./"))+1  )
		path = replace(path,"\" , "/")	
		sPath = path	
		sOldPath = sOldFolder & sPath
		'sSize = Int(file.size / 1000) & "KB"
		sSize = Byte2size(size)
		sFileName = POP_MVC.File.BaseName(path)
		sBaseName = POP_MVC.File.BaseName( array(path , 1) )		
		sExtName = Mid(path, InStrRev(path, ".") + 1)
		

		if BelongSkipFolder(path) then
			Exit sub
		end if

		bool = POP_MVC.file.isFile( sOldPath )

		if not bool then
			countnum = countnum + 1
			sTotalSize = sTotalSize + size
			'如果旧文件不存在
			Call flushEach( bool )
			P_("ACCESSPACk").Add(sPath)
			exit sub
		end if
		
		if dateCompare > 0 then
			countnum = countnum + 1
			sTotalSize = sTotalSize + size
			Call flushEach( bool )
			P_("ACCESSPACk").Add(sPath)
			exit sub
		end if
	End Sub
	
	'文件是否属于过滤文件夹
	Private Function BelongSkipFolder( path )
		dim i
		if not isArray( sNoSkipFolder ) then
			exit function
		end if
		for i = 0 to ubound( sNoSkipFolder )
			if POP_MVC.file.belong( sNoSkipFolder( i ) , path ) then
				exit function
			end if
		next
		for i = 0 to ubound( sSkipFolder )
			if POP_MVC.file.belong( sSkipFolder( i ) , path ) then
				BelongSkipFolder = true
				exit function
			end if
		next
	End Function
	
	'日期比较
	Private Function dateCompare( )
		dim date1,date2,file2,file1		
		file1 = sPath
		file2 = sOldFolder & sPath
		date1 = POP_MVC.File.mtime(file1)
		date2 = POP_MVC.File.mtime(file2)
		dateCompare = DateDiff("s",date2,date1)
	End Function
	
	'解包
	'thePath为解包数据库路径
	'DirPrefix取空值即可
	'reload根据时间比较决定是否覆盖,1覆盖，0不覆盖
	Function UnPack(ByVal thePath , ByVal DirPrefix ,reload )
		Server.ScriptTimeOut = 1440		
		P_("ACCESSPACk").RootPath = "./"
		countnum = 1
		sDirPrefix = DirPrefix
		sReload = reload
		response.write "<center>正在对系统进行升级，请耐心等待……</center>"
		response.flush
		call P_("ACCESSPACk").cmsUnPack(thePath, "" , "K_(""bbs--Update"").UnPack4file" )
		response.write "<center>文件已导入完成，下一步需要导入数据库，请勿离开页面……</center><script>setTimeout(function(){location.href='?update_UnPackDb'},2000);</script>"
		response.flush
	End Function
	
	'解包
	'thePath为解包数据库路径
	'DirPrefix取空值即可
	'reload根据时间比较决定是否覆盖,1覆盖，0不覆盖
	Function UnPack4file(ByVal savePath , rs )
		dim path, editTime,basename,dict,tempBool
		path = rs("thePath")
		editTime = rs("EditTime")
		
		basename = POP_MVC.File.baseName(savePath)
		UnPack4file = false
		
		if reload = 1 then
			bool = true
		else
			bool = false
		end if
		
		tempBool = bool
		
		if not tempBool then			
			if not POP_MVC.file.isFile( savePath ) then
				tempBool = true
			elseif DateDiff("s",editTime, POP_MVC.file.mtime(  savePath ) ) < 0 then
				tempBool = true
			end if			
		end if
		
		countnum = countnum + 1
		
		if not tempBool then
			response.write "<center>" & countnum & ". " & sDirPrefix & savePath & "文件按要求不更新！</center>"
			response.flush	
			UnPack4file = false
			exit function
		else
			UnPack4file = true
			exit function
		end if
	End Function

	
	'解包数据库
	'dstPath 目标数据库路径，初始化后的新版数据库
	'srcPath 源数据库路径，需要导入数据库数据的旧版数据库
	Function UnpackDB( ByVal dstPath, ByVal srcPath )
		on error resume next
		dim i,dbType,srcPrefix,dstPrefix,cnt,cnt1,cnt2,selfArr,sql,selfLen
		
		if isNul( srcPath ) then
			srcPath = POP_MVC.config("DB_PATH")
		end if
		
		if isNul( dstPath ) then
			dstPath = clearDbName
		end if
		
		Call importConfigs("self_GuestConfig")
		dbType = "access"
		srcPrefix = POP_MVC.config("DB_PREFIX")
		dstPrefix = POP_MVC.config("DB_PREFIX")
		srcPath = POP_MVC.config("DB_PATH")
		POP_MVC.file.AccessCompress( dstPath )
		selfArr = K_("bbs--system").getSelfTables("")
		cnt1 = ubound( toTables ) + 1
		'cnt2 = ubound( selfArr ) + 1
		cnt2 = 0
		var_export "<center>准备导入 " & ( cnt1 + cnt2 ) & " 个数据表 …… </center>"
		response.flush
		for i = 0 to ubound( toTables )
			cnt = B_( Array( toTables(i) , dbType, srcPath ) ).count
			var_export "<center>" & (i+1) & ". 正在导入数据表 " & toTables(i) & " (" & cnt & "条记录) …… </center>"
			response.flush
			if cnt > 0 then
				Call importTable( toTables(i) , srcPrefix , dbType , srcPath , dstPrefix , dbType , dstPath   )
			end if			
		next
		'自定义数据表暂时用不上
		for i = 0 to ubound( selfArr )
			exit for
			selfLen = Len( POP_MVC.config("DB_PREFIX") ) + 1
			cnt = B_( Array( selfArr(i) , dbType, srcPath ) ).count
			var_export "<center>" & (i+1+cnt1) & ". 正在导入数据表 " & mid(selfArr(i),selfLen) & " (" & cnt & "条记录) …… </center>"
			response.flush
			
			if not B_( Array("user" , dbType , dstPath ) ).tableExists( selfArr(i) ) then			
				sql = B_( Array("user" , dbType , srcPath ) ).getCreateSQL( selfArr(i) )
				Call B_( Array("user" , dbType , dstPath ) ).exec( sql )
			end if
			
			if cnt > 0 then
				Call importTable( selfArr(i) , srcPrefix , dbType , srcPath , dstPrefix , dbType , dstPath   )
			end if
		next
		Call K_("bbs--system").CloseMvcDbClass
		Call POP_MVC.file.rename( srcPath , POP_MVC.file.getCopyName( srcPath, "-" & POP_MVC.String.uniqid() ) )
		Call POP_MVC.file.rename( dstPath , srcPath )
		response.write "<center>系统升级完成!<a href='?System.html'>点击返回</a></center>"
	End Function
	
	'导入配置
	sub importConfigs( tableName )
		on error resume next
		dim dict,srcdb,dbType
		srcdb = POP_MVC.config("DB_PATH")
		dbType = "access"
		set dict = B_( Array(tableName,dbType , srcdb ) ).field("ConfigName,ConfigValue").getKeyValue()		
		dict.remove("adminPath")
		dict.remove("accDBPath")
		Call ModifyConfig( dict ,dbType , dstdb )
	end sub
	
	'根据表名导入数据表
	sub importTable( table_name , ByVal srcPrefix , ByVal srcDBType, ByVal srcDBPath , ByVal dstPrefix , ByVal dstDBType , ByVal dstDBPath )
		Call P_("POPASP_DBCONVERT").table2table( Array( table_name, srcDBType , srcDBPath ) , srcPrefix , Array( table_name, dstDBType , dstDBPath ) , dstPrefix )
	End Sub
	
	'修改配置
	sub ModifyConfig( tableName, dict , dbType , dbPath )
		dim key,item,oldPath,newPath,bool,inc_content,i,keys,arr
		
		if isNul( dbType ) then
			dbType = POP_MVC.config("DB_TYPE")
		end if
		
		if isNul( dbPath ) then
			dbPath = POP_MVC.config("DB_PATH")
		end if
		
		'修改配置表中的数据
		for each key in dict
			Call B_(Array(tableName , dbType ,dbPath )).where( that.dict( "ConfigName" , key  ) ).setField( "ConfigValue",dict(key) )
		next
	end sub
End Class
%>