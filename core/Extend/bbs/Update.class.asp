<%
Class Update 'Extends bbs--Common
	Sub init
		if not that.isSelfOrigin then
			that.error("非法操作")
		end if
		'response.end
		that.show
	End Sub
	
	sub list
		that.show
	end sub
	
	Sub Pack
		Call K_("bbs--Update").Pack(  "D:\asp\aspbbs1.1.3.1\aspbbs" , ""  )
	End Sub
	
	Sub PackDB
		Call K_("bbs--Update").PackDB( "D:\asp\aspbbs1.1.3.1\aspbbs" , ""  )
	End Sub
	
	'升级unpack
	Sub search
		'Call K_("bbs--Update").UnPack( "./" & that.req("path") , "",0)
		dim newVersion,oldVersion,upfile,arr,sTip
		upfile = "./" & that.req("path")
		'upfile = "./aspbbs_update_1.2.1_1.1.3.1.data"
		
		if not POP_MVC.file.isfile( upfile ) then
			sTip = "升级文件不存在"
			Call K_("bbs--Public").CtrlLog( 0 , "用户ID: " & S_("adminId") & " 升级时，" & sTip , sTip )
		end if
		
		if not  P_("VERSION").isUpdateFile( upfile ) then
			sTip = "不是有效的升级文件！"
			Call K_("bbs--Public").CtrlLog( 0 , "用户ID: " & S_("adminId") & " 升级时，" & sTip , sTip )
		end if
	
		if not  P_("VERSION").isNeedUpdate( upfile ) then
			'that.error("该升级文件不适合当前版本！")
		end if
		
		Call K_("bbs--Update").UnPack( "./" & upfile , "",0)
	end sub
	
	Sub UnPackDB
		Call K_("bbs--update").UnpackDB( "" , "" )
	end sub
End Class
%>