<%
Class User
	private LoginNameTip,PasswordTip
	sub init
		'表名 
		'db是popasp_access类的实例化
		db.tableName = "User"
		
		'主键
		db.prikey = "UserID"
		
		LoginNameTip = POP_MVC.config("LoginNameTip")
		PasswordTip = POP_MVC.config("PasswordTip")
		
		db.auto_ = Array( _
			Array( "RegTime" ,"POP_MVC.FormatDate( now() , ""YYYY-MM-DD HH:II:SS"" )",  1 , "function" ) _
			,Array( "RegIP" ,"get_client_ip",  1 , "function" ) _
			,Array( "Password" ,"bbs--User.encodePassword",  3 , "callback" , array() ) _
			,Array( "RePassword" ,"bbs--User.encodePassword",  3 , "callback" , array() ) _
			,Array( "Password" ,"",  2 , "ignore" ) _
			,Array( "RePassword" ,"",  2 , "ignore" ) _
			,Array( "GroupID" ,"3",  1 ) _
			,Array( "UserStatus" ,1,  1 ) _
			,Array( "ThreadStatus" ,guestConfig.ThreadAddAllow,  1 ) _
			,Array( "ThreadReplyView" ,0,  1 ) _
			,Array( "ReplyStatus" ,guestConfig.ReplyAddAllow,  1 ) _
			,Array( "UserID" , Session("UserID") ,  2 ) _
			,Array( "MailAlert" , "POP_MVC.String.uniqid" , 1 , "function"  ) _
			,Array( "IsAdmin" , "0" , 1 ) _
			,Array( "GroupID" , "3" , 1 ) _
			,Array( "Gender" , "1" , 1 ) _
			,Array( "Experience" , guestConfig.RegExperience , 1 , "string" ) _
			,Array( "Avatar" , "templates/bbs/res/images/avatar/" & POP_MVC.File.getRandFileName("./templates/bbs/res/images/avatar/") , 1 ) _
			,Array( "Nickname" , POP_MVC.form("LoginName") , 1 ) _
		)
		
		'数据验证
		db.validate_ = Array( _
			Array( "LoginName","","用户名不能为空" , 1 , "notempty" , 3 ) _			
			,Array( "LoginName",guestConfig.RegNameLenMin & "," & guestConfig.RegNameLenMax ,LoginNameTip, 1 , "length" , 3 ) _
			,Array( "LoginName","bbs--User.checkUserName",LoginNameTip, 1 , "callback" , 3 , Array() ) _
			,Array( "LoginName","","用户名已经存在，请注册别的用户名" , 1 , "unique" , 1 ) _
			,Array( "Nickname","0,20","昵称不能超过20个字符" , 0 , "length" , 3 ) _
			,Array( "Nickname","[a-z0-9\u0391-\uFFE5]{0,20}","昵称由汉字、英文或数字组成" , 0 , "regex" , 3 ) _
			,Array( "TrueName","","真实姓名不能为空" , 0 , "notempty" , 3 ) _	
			,Array( "TrueName","2,10","真实姓名长度为2~10个字符" , 0 , "length" , 3 ) _
			,Array( "TrueName","chinese","真实姓名必须为汉字" , 0 , "regex" , 3 ) _
			,Array( "Password","","登陆密码不能为空" , 1 , "notempty" , 1 ) _
			,Array( "Password","","登陆密码不能为空" , 0 , "notempty" , 2 ) _			
			,Array( "RePassword","","确认密码不能为空" , 0 , "notempty" , 3 ) _	
			,Array( "RePassword","Password","两次输入密码不一致" , 0 , "confirm" , 3 ) _
			,Array( "Password",guestConfig.RegPassLenMin & "," & guestConfig.RegPassLenMax ,PasswordTip, 1 , "length" , 1 ) _
			,Array( "Password",guestConfig.RegPassLenMin & "," & guestConfig.RegPassLenMax ,PasswordTip, 0 , "length" , 2 ) _
			,Array( "Password","bbs--User.checkPassword",PasswordTip, 1 , "callback" , 1 , Array() ) _
			,Array( "Password","bbs--User.checkPassword",PasswordTip, 0 , "callback" , 2 , Array() ) _
			,Array( "Agreement","1","必须同意服务条款才能注册！", 1 , "eq" , 1 ) _
			,Array( "Gender","0,1","性别选择不正确" , 2 , "in" , 3 ) _
			,Array( "Mobile","require","手机号不能为空" , 0 , "regex" , 3 ) _
			,Array( "Mobile","mobile","手机号码不正确" , 0 , "regex" , 3 ) _
			,Array( "Phone","Phone","电话号码不正确" , 2 , "regex" , 3 ) _
			,Array( "Email","email","邮箱格式不正确" , 2 , "regex" , 3 ) _
			,Array( "Email:LoginName<>" & Session("adminName"),"","邮箱已经被使用" , 0 , "unique" , 3 ) _
			,Array( "QQ","0,200","邮箱不能超过200个字符" , 2 , "length" , 3 ) _
			,Array( "QQ","^\d{5,13}$","不是有效的qq号" , 2 , "regex" , 3 ) _
			,Array( "QQ","","qq号码已经被使用" , 2 , "unique" , 3 ) _
			,Array( "Address","","地址不能为空" , 2 , "notempty" , 3 ) _	
			,Array( "Address","0,200","联系地址不能超过个200字符" , 0 , "length" , 3 ) _
			,Array( "Sign","0,200","签名不能超过200个字符" , 0 , "length" , 3 ) _
			,Array( "City","0,200","城市不能超过200个字符" , 0 , "length" , 3 ) _
			,Array( "verify","","验证码不能为空" , 0 , "notempty" , 3 ) _		
			,Array( "verify","^\w{4}$","验证码不正确" , 0 , "regex" , 3 ) _		
			,Array( "verify","","验证码不正确" , 0 , "verify" , 3 ) _
		)
	end sub
	
	function checkUserNameTip()
		checkUserNameTip = LoginNameTip
	End Function
	
	function checkUserName( value )
		dim rule , arr
		checkUserName = true
		
		select case guestConfig.RegNameType
			case "英文、数字、下划线"
				rule = "\w"
			case "英文、数字"
				rule = "[a-z0-9]"
			case "英文"
				rule = "[a-z]"
			case "汉字、英文、数字、下划线"
				rule = "[\w\u0391-\uFFE5]"
			case "汉字、英文、数字"
				rule = "[a-z0-9\u0391-\uFFE5]"
			case "汉字、英文"
				rule = "[a-z\u0391-\uFFE5]"
			case "汉字、数字"
				rule = "[0-9\u0391-\uFFE5]"
			case "汉字"
				rule = "[\u0391-\uFFE5]"
		end select
		
		
		rule = rule & "{" & guestConfig.RegNameLenMin & "," & guestConfig.RegNameLenMax & "}"
		
		if not POP_MVC.String.reg_test( value,rule,"i" ) then
			checkUserName = false : exit Function
		end if
		
		if inStr( guestConfig.RegNameType , "英文" ) > 0 then		
			if inStr( LCase(value) , "admin" ) > 0 then
				if CStr(S_("IsAdmin")) <> "1" then
					LoginNameTip = "包含了不可用字符串 " & "admin"
					checkUserName = false : exit Function
				end if				
			end if
		
			arr = Array("fuck","shit")
			for i = 0 to ubound(arr)
				if POP_MVC.String.iExists( value,arr(i) ) then
					LoginNameTip = "包含了不可用字符串 " & arr(i)
					checkUserName = false : exit Function
				end if
			next
		end if
		
		if inStr( guestConfig.RegNameType , "数字" ) > 0 then
			arr = Array("110","120","122", "119")
			for i = 0 to ubound(arr)
				if POP_MVC.String.iExists( value,arr(i) ) then
					LoginNameTip = "包含了不可用字符串 " & arr(i)
					checkUserName = false : exit Function
				end if
			next
		end if
	end function
	
	function checkPassword( value )
		dim rule , arr
		checkPassword = true
		
		if guestConfig.RegPassType = "英文、数字、下划线" then
			if not POP_MVC.String.reg_test( value, "^\w{" & guestConfig.RegPassLenMin & "," & guestConfig.RegPassLenMax & "}$" , "i" ) then
				checkPassword = false : exit Function
			end if
		end if
		
		if inStr(guestConfig.RegPassType , "数字" ) > 0 then
			if not POP_MVC.String.reg_test( value, "[0-9]" , "" ) then
				checkPassword = false : exit Function
			end if
		end if
		if inStr(guestConfig.RegPassType , "小写字母" ) > 0 OR inStr(guestConfig.RegPassType , "大小写字母" ) > 0 then
			if not POP_MVC.String.reg_test( value, "[a-z]" , "" ) then
				checkPassword = false : exit Function
			end if
		end if
		
		if inStr(guestConfig.RegPassType , "大写字母" ) > 0 OR inStr(guestConfig.RegPassType , "大小写字母" ) > 0 then
			if not POP_MVC.String.reg_test( value, "[A-Z]" , "" ) then
				checkPassword = false : exit Function
			end if
		end if
		
		if inStr(guestConfig.RegPassType , "符号" ) > 0 then
			if not POP_MVC.String.reg_test( value, "[^0-9a-zA-Z]" , "" ) then
				checkPassword = false : exit Function
			end if
		end if
	end function
	
	'检测当前密码是否正确
	Function CheckCurPass( pass )
		dim where,rs
		set where = D_
		where("UserID") = S_("adminId")
		where("Password") = md5( pass )
		
		set rs = B_("User").where( where ).find
		CheckCurPass = not rs.eof
		Call POP_MVC.unset( rs )
	End Function
	
	Function encodePassword( pass )
		if trim( pass ) <> "" then
			encodePassword = md5( pass )
		end if
	End Function
End Class
%>