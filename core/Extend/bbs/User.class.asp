<%
Class User 'Extends bbs--Common
	private tableName,baseTable
	sub initialize
		tableName = "User"
		that.d("layuicachepage") = "user"
		that.d("admin") = objCMS.getUserData( S_("adminId") )
		baseTable = objCMS.baseTable
	end sub
	
	'用户管理 用户列表
	sub list
		dim templatePath,sTip
	
		if S_("IsAdmin") <> 1 then
			sTip = "无权查看该页面"
			Call K_("bbs--Public").CtrlLog( 0 , "用户ID: " & S_("adminId") & " " & sTip , "您" & sTip )
		end if
		
		objCMS.BbsShow("user/list")
	end sub
	

	
	'全部头像
	sub avatar
		dim arr,path,arr2,str,data

		str = B_("self_GuestConfig").field( "SelectOptions" ).where("ConfigName='"& "AvatarType" &"'").getOne()
		set data = K_("bbs--Public").option2data(str)		
		that.d("data") = data
		objCMS.BbsShow( "user/avatar" )
	end sub
	
	'用户中心
	sub index
		dim templatePath,UserID,curtpl

		UserID = S_("adminId")
		
		curtpl = B_("self_GuestConfig").where("ConfigName='DefaultTemplate'").field("ConfigValue").getOne
		
		that.d("curtpl") = curtpl
		that.d("user") = objCMS.getUserData(UserID)	
		
		Call getSignData
		
		that.d("tplArr") = guestConfig.SiteTplArr
		
		objCMS.BbsShow("user/index")
	end sub
	
	'我的帖子与收藏帖子
	sub thread
		dim templatePath
		
		dim UserID,where,ids
		UserID = S_("adminId")
		
		'提问
		set where = D_
		
		where("ContentStatus") = 1
		where("UserID") = S_("adminId") 
		that.d("thread") = B_("self_GuestTopic").field("TopicID,Title,AddTime,Visits,ReplyCount,IsTop,IsFeatured").where( where ).top(30).page("null,30").order("TopicID DESC").select
		
		ids = B_(baseTable).from("{prefix}self_GuestDigg").field("Collection").where("UserID = " & UserID).getOne
		ids = POP_MVC.trim(ids,",")
		
		'收藏帖子
		set where = D_
		
		'where("UserID") = S_("adminId") 
		if not isNul(ids) then
			where("TopicID") = Array("In",ids)
		else
			where("1=2") = null
		end if
		that.d("collection") = B_("self_GuestTopic").field("TopicID,AddTime,Title,Visits,ReplyCount").where( where ).top(30).page("null,30").select		
	
		objCMS.BbsShow( "user/thread" )
	end sub
	
	'重新激活
	sub Reactivate
		dim templatePath,dict,errnum
		set dict = D_
		dict("MailAlert") = POP_MVC.String.uniqid
		dict("SendActivateMailTime") = now()
		Call objCMS.setUserData( S_("adminId")  , dict,null )
		that.d("msg") = "激活邮件已成功发送"
		that.d("info") = "待激活"
		objCMS.Bbsshow( "user/activateOk" )
	end sub	
	
	'消息列表
	sub Message
		dim rs,where,obj,templatePath,rs2,item,key,href
		set where = D_
		
		
		where("ToUser") = S_("adminId")
		where("IsRead") = Array( "<" , 20 )
		set rs = B_(baseTable).field("a.UserID,a.NickName,a.Avatar,a.LoginName,b.AddTime,b.Message,b.MessageID").from("{prefix}User as a").leftjoin("{prefix}self_GuestMessage as b on a.UserID = b.FromUser").where(where).order("MessageID desc").select
		
		that.d("RecordCount") = rs.RecordCount
		that.d("message") = rs
		
		if rs.RecordCount > 0 then
			set where = D_
			where("IsRead") = Array( "<" , 20 )
			where("ToUser") = S_("adminId")
			Call B_("self_GuestMessage").where(where).setInc("IsRead")
		end if

		objCMS.BBsShow( "user/message" )
	end sub		
	
	
	'基本设置页面
	sub settings
		objCMS.Bbsshow( "user/set" )
	end sub
	
	'消息列表
	private sub Message0
		dim rs,where,obj,templatePath,rs2,item,key,href
		set where = D_
		
		
		where("ToUser") = S_("adminId")
		where("IsRead") = Array( "<" , 5 )
		set rs = B_( baseTable ).field("a.UserID,a.NickName,a.Avatar,a.LoginName,b.AddTime,b.Message").from("{prefix}User as a").leftjoin("{prefix}self_GuestMessage as b on a.UserID = b.FromUser").where(where).order("MessageID desc").select
		where.remove("IsRead")
		set rs2 = B_( baseTable ).field("a.Avatar as img,b.Message as info,a.UserID as href,a.LoginName,b.AddTime").from("{prefix}User as a").leftjoin("{prefix}self_GuestMessage as b on a.UserID = b.FromUser").top(15).where(where).order("MessageID desc").getAll
		
		for each key in rs2
			set item = rs2(key)
			href = item("href")
			if (inStr( item("info") , "回答了您的求解" ) > 0 or inStr( item("info") , "提到了您" ) >0) and inStr( item("info") , "<a " ) > 0 then
				item("href") = POP_MVC.String.getInStr(item("info") , "<a target='_blank' href='" , "'")
			else
				item("href") = getUserULink( href )
			end if

			item("info") = POP_MVC.String.cut( item("info"),30 )
			item("title") =  POP_MVC.dateStr(item("AddTime")) & " "  & item("info")
			item.remove("AddTime")
			item.remove("LoginName")
		next

		that.d("barrager") = rs2
		
		that.d("RecordCount") = rs2.count
		that.d("message") = rs
		
		if rs.RecordCount > 0 then
			set where = D_
			where("IsRead") = Array( "<" , 5 )
			where("ToUser") = S_("adminId")
			Call B_("self_GuestMessage").where(where).setInc("IsRead")
		end if

		objCMS.bbsShow( "user/message" )
	end sub	
	
	'发送消息
	sub sendMessage
		dim dict,id,sTip
		
		set dict = POP_MVC.form2dict		
		
		if not K_("bbs--Public").ModelCreate("bbs--Message",dict,1) then
			sTip = M_("bbs--Message").db.error
			Call K_( "bbs--Public" ).CtrlLog( 0, "用户 ID: " & S_("adminId") & " 发送消息 " & sTip  , sTip )
		end if
		
		id = K_("bbs--Public").ModelAdd( "bbs--Message" , null )
		if id > 0 then
			Call K_("bbs--Mail").sendMessage(id)
			Call K_( "bbs--Public" ).CtrlLog( 1, "用户 ID: " & S_("adminId") & " 发送消息成功ID: " & id  , "发送成功！" )
		end if		
	end sub
	
	'删除消息
	sub removeMessage
		dim id,where
		id = that.req("id")
		
		set where = D_
		where("ToUser") = S_("adminId")
		if not isNul(id) then
			where("MessageID") = Array( "IN" , id )
		end if
		B_("self_GuestMessage").where(where).remove
		Call K_("bbs--Public").CtrlLog( 1 , "用户ID: " & S_("adminId") & " 删除消息" , "删除成功" )
	end sub

	
	'上传头像
	sub upload
		dim filename,logID,sTip,mumaFlag
		POP_MVC.config("UPLOAD_ALLOW_TYPES") = POP_MVC.config("UPLOAD_IMAGE_TYPES")
		filename = POP_MVC.upload("file")
		filename = K_("bbs--Public").upload( "User-upload" , logID )
		if filename <> "" then
			mumaFlag = K_("bbs--Public").TestPicMuma( filename )
			if mumaFlag <> "" then				
				Call K_("bbs--Public").DealPicMuma( filename )
				filename = "static/images/nopic.gif"
			else
				Call objCMS.setUserData( S_("adminId")  , "Avatar" , filename )				
			end if
			that.success( filename )
		else		
			sTip = POP_MVC.Uploader.description
			Call K_("bbs--Public").CtrlLog( 0 , "用户ID: " & S_("adminId") & " 上传头像 " & sTip , Array( sTip ,-1 ) )
		end if
	end sub
	
	'修改信息
	sub editInfo
		dim dict,key,sTip
		set dict = POP_MVC.form2dict
		
		'全部去掉html标签
		for each key in dict
			dict(key) = POP_MVC.String.strip_tags( dict(key) , "html" )
		next
		
		if not K_("bbs--Public").ModelCreate("bbs--User",dict,2) then
			sTip = M_("bbs--User").db.error
			Call K_("bbs--Public").CtrlLog( 0 , "用户ID: " & S_("adminId") & " 修改个人信息 " & sTip , Array( sTip ,-1 ) )
		end if
		
		set dict = M_("bbs--User").db.getData
		if M_("bbs--User").db.data(dict).where( S_("adminId") ).Save then
			Call K_("bbs--Public").CtrlLog( 1 , "用户ID: " & S_("adminId") & " 修改个人信息" , Array("修改成功" , "?user_settings_" & POP_MVC.String.uniqid() & "#info") )
		end if
	end sub
	
	'修改密码操作
	sub editPassword	
		dim where,rs,sTip
that.AjaxArgs("info") = "msg"
that.ajaxSuccessStatus = 0
that.ajaxErrorStatus = 1
		
		'验证
		Call set_password_check
		
		if not B_("User").create(2) then
			sTip = B_("User").error
			Call K_("bbs--Public").CtrlLog( 0 , "用户ID: " & S_("adminId") & " 修改密码 " & sTip , Array( sTip ,-1 ) )
		end if
		
		Call objCMS.setUserData( S_("adminId")  , "Password" , md5( that.form("newpassword") ) )
		
		that.u(rs)
		sTip = "密码修改成功"
		Call K_("bbs--Public").CtrlLog( 1 ,  "用户 ID:" &  S_("adminId") & sTip  , Array( sTip , "?user_settings_" & POP_MVC.String.uniqid() & "#pass") )
	end sub
	
	'修改密码操作
	sub editLoginName
		dim fieldName,fieldValue,where,sTip
		fieldName = that.req("fieldName")
		fieldValue = that.req("fieldValue")

		'验证
		if not M_("bbs--User").checkUserName( fieldValue ) then
			sTip = M_("bbs--User").checkUserNameTip()
			Call K_("bbs--Public").CtrlLog( 0 , "用户ID: " & S_("adminId") & " 修改密码 " & sTip , sTip )
		end if
		
		set where = D_
		where("UserID") = Array("neq" , S_("adminId"))
		where("LoginName") = fieldValue
		
		if B_(tableName).where(where).count() > 0 then
			sTip = "该用户名已经被使用，请重试别的！"
			Call K_("bbs--Public").CtrlLog( 1 ,  "用户 ID:" &  S_("adminId") & sTip  , sTip )
		end if
		Call objCMS.setUserData( S_("adminId")  , "LoginName" , fieldValue )
		
		Session("adminName") = fieldValue
		
		sTip = "登陆名修改成功"
		Call K_("bbs--Public").CtrlLog( 1 ,  "用户 ID:" &  S_("adminId") & sTip  , Array( sTip , "?user") )
	end sub
	
	'修改头像操作
	sub editAvatar
		dim fieldName,fieldValue,where,sTip
		fieldName = that.req("fieldName")
		fieldValue = that.req("fieldValue")
		
		Call objCMS.setUserData( S_("adminId")  ,"Avatar" , fieldValue  )
		
		Session("Avatar") = fieldValue
		
		sTip = "头像修改成功"
		Call K_("bbs--Public").CtrlLog( 1 ,  "用户 ID:" &  S_("adminId") & sTip  , Array( sTip , "?user") )
	end sub
	
	Private sub set_password_check
		B_("User").validate_ = Array( _
			Array( "oldpassword","","当前密码不能为空" , 1 , "notempty" , 3 ) _	
			,Array( "oldpassword","bbs--User.CheckCurPass","当前密码不正确！" ,0 , "callback" , 3 , Array() ) _
			,Array( "newpassword","","新密码不能为空" , 1 , "notempty" , 3 ) _	
			,Array( "renewpassword","","确认密码不能为空" , 1 , "notempty" , 3 ) _	
			,Array( "newpassword","renewpassword","两次输入密码不一致" , 0 , "confirm" , 3 ) _
			,Array( "newpassword",guestConfig.RegPassLenMin & "," & guestConfig.RegPassLenMax ,POP_MVC.config("PasswordTip"), 0 , "length" , 3 ) _
			,Array( "newpassword","bbs--User.checkPassword",POP_MVC.config("PasswordTip"), 0 , "callback" , 3 , Array() ) _
		)
	end sub
	
	'解除绑定
	sub Unbind
		dim stype,field,sTip
		stype = that.form( "type" )
		if stype = "qq_id" then
			field = "QQOpenID"
			sTip = "QQ"
		elseif stype = "weibo_id" then
			field = "WeiboAppKey"
			sTip = "新浪微博"
		end if
		Call objCMS.setUserData( S_("adminId")  , field,""  )
		
		sTip = sTip & "解绑成功"
		Call K_("bbs--Public").CtrlLog( 1 ,  "用户 ID:" &  S_("adminId") & sTip  , sTip )
	end sub
	

	
	'激活邮箱
	sub activate
		dim templatePath,mailalert
		mailalert = B_("User").where( S_("adminId") ).field("MailAlert").getOne
		if mailalert = "" then		
			that.d("msg") = "恭喜您已完成邮箱激活操作"
			that.d("info") = "已成功激活"
			templatePath= obj.getTemplatePath( "user/activateOk" )
		else
			templatePath= obj.getTemplatePath( "user/activate" )
		end if	
		objCMS.show( templatePath )
	end sub
	
	'发送激活邮件
	sub SendActivateMail		
		dim obj,templatePath,errnum,dict,MailAlert,rs,bool,sTip
		
		bool = false
		set rs = B_("User").where( that.dict( "UserID" ,S_("adminId") ) ).find
		if rs("MailAlert") <> "" then
			if isDate(rs("SendActivateMailTime")) then
				if guestConfig.ActivateMailTime <> "" then
					if datediff( "h", rs("SendActivateMailTime") , now   ) - guestConfig.ActivateMailTime < 0 then
						bool = true
					end if
				else
					bool = true
				end if
			end if
		end if
		
		set obj = objCMS
		
		if not bool then
			set dict = D_
			MailAlert = POP_MVC.String.uniqid
			that.d("MailAlert") = MailAlert
			dict("MailAlert") = MailAlert
			dict("SendActivateMailTime") = now()
			Call objCMS.setUserData( S_("adminId")  , dict,null )
			
			templatePath= obj.getTemplatePath( "user/activateMail" )		
			obj.load(templatePath)	
			obj.parseHtml
			errnum = K_("bbs--Plugin").sendmail( V_("admin.Email"), guestConfig.SiteName & "激活邮箱", obj.Content )
			if errnum = 0 then 
				Call objCMS.setUserData( S_("adminId")  , "SendActivateMailTime" , now() )
				Call K_( "bbs--Public" ).CtrlLog( 1, "用户 ID: " & S_("adminId") & " 发送激活邮件" , "激活邮件已成功发送" )
				that.success()
			else
			
				sTip = "由于系统原因，激活邮件发送失败"
				Call K_("bbs--Public").CtrlLog( 0 , "用户ID: " & S_("adminId") & " " & sTip , sTip )
			end if
		else
			that.d("msg") = "激活邮件已在 " & rs("SendActivateMailTime") & " 成功发送，请前往邮箱" & rs("Email") & "激活！"
			that.d("info") = "待激活"
			objCMS.show(  "user/activateOk")
		end if
	end sub
	
	'注销账户
	sub setStatusField
		dim StatusField,FieldValue,sTip
		StatusField = POP_MVC.req("StatusField")
		FieldValue = POP_MVC.req("FieldValue")
		Call objCMS.setUserData( S_("adminId")  , StatusField , FieldValue )
		'清空Session
		Session.Contents.RemoveAll()
		Session.Abandon		
		'清空Cookie
		P_("cookie").clear
		sTip = "注销成功！"
		Call K_("bbs--Public").CtrlLog( 1 ,  "用户 ID:" &  S_("adminId") & sTip  , sTip )		
	end sub

	
	'用户管理 送积分
	sub IncExperience
		dim UserID,value,sTip
		if S_("GroupID") <> 1 then
			sTip = "无权增加用户积分"
			Call K_("bbs--Public").CtrlLog( 0 , "用户ID: " & S_("adminId") & " " & sTip , "您" & sTip )
		end if
		
		'用户ID
		UserID = that.form("id")
		
		'赠送的积分
		value = that.form("Experience")
		
		'修改记录
		Call B_(tableName).where( UserID ).setInc( Array("Experience" , value ))
		
		'发送消息
		Call M_("bbs--Message").SendMessages( S_("adminId") , UserID , S_("adminName") & "站长赠送您 " &  value & " " & guestConfig.ExperienceName  )
		
		Call K_("bbs--Public").CtrlLog( 1 , S_("adminName") & "站长赠送用户ID:" & UserID & " " &  value & " " & guestConfig.ExperienceName , "设置成功！" )
	end sub
	
	'用户管理 减积分
	sub DecExperience
		dim UserID,value,sTip
		
		if S_("GroupID") <> 1 then
			sTip = "无权减少用户积分"
			Call K_("bbs--Public").CtrlLog( 0 , "用户ID: " & S_("adminId") & " " & sTip , "您" & sTip )
		end if
		
		'用户ID
		UserID = that.form("id")
		
		'惩罚的积分
		value = that.form("Experience")
		
		'修改记录
		Call B_(tableName).where( UserID ).setDec( Array("Experience" , value ) )
		
		'发送邮件
		Call M_("bbs--Message").SendMessages( S_("adminId") , UserID , S_("adminName") & "站长惩罚您 " &  value & " " & guestConfig.ExperienceName  )
		
		Call K_("bbs--Public").CtrlLog( 1 , S_("adminName") & "站长惩罚用户ID:" & UserID & " " &  value & " " & guestConfig.ExperienceName , "设置成功！" )
	end sub
	
	'用户管理 动态数据ajax
	sub search
		dim page,perpage,sTip
		dim rs,where,order,oPage,keyword,IsAdmin
		
		if S_("IsAdmin") <> 1 then
			sTip = "无权查看该页面"
			Call K_("bbs--Public").CtrlLog( 0 , "用户ID: " & S_("adminId") & " " & sTip , "您" & sTip )
		end if
		
		if that.get("IsAdmin") = 1 then
			IsAdmin = 1
		else
			IsAdmin = 0
		end if

		
		set where = D_
		order = "a.GroupID ASC,UserID desc"
		page = that.req("page")
		perpage = that.req("limit")
		
		'注意这儿的用法，不返回值，而是不断的添加条件
		B_( baseTable ).field("UserID,IsAdmin,LoginName,Gender,Email,QQ,LastLoginTime,UserStatus,a.GroupID,GroupName,Mobile,Address,RegTime,LastLoginIP,TrueName,Nickname,Experience,Avatar").from("{prefix}User as a, {prefix}UserGroup as b").page( array( page,perpage ) ).order(order)
		
		where("a.GroupID=b.GroupID") = null
		'where("{prefix}UserGroup.IsAdmin") = IsAdmin
		
		if that.req("ViewType") <> "" then
			where(that.req("ViewType")) = null
		end if
		
		if that.get("GroupID") <> "" then
			where("{prefix}User.GroupID") = that.get("GroupID")
		end if
		

		

	
		
		'下面才得到返回值
		if POP_MVC.req("keyword") <> "" then	
			set rs = B_( baseTable ).where(where).search( CStr( POP_MVC.req("keyword") ) , "LoginName,Nickname" )
		else
			set rs = B_( baseTable ).where(where).select
		end if

		Call that.LayuiTableAjax( rs, "" , 0 )
	end sub
	
	'获取签到
	private sub getSignData
		dim signRS
		
		'获取与分配签到数据
		that.d("IsSign") = false
		that.d("SignDays") = 0
		if S_("adminId") <> "" then			
			set signRS = B_( baseTable ).from("{prefix}self_GuestDigg").where( "UserID = " & S_("adminId") ).field("UserID,SignTime,SignDays,SignExperience").find
			
			that.d("SignExperience") = K_("bbs--Experience").getNextSignExperience(signRS)
			
			if not signRS.eof then
				if  datediff( "d" , signRS("SignTime") , now() ) <=1 then
					that.d("IsSign") = true	
					that.d("SignDays") = signRS("SignDays")
					that.d("SignExperience") = signRS("SignExperience")
				end if
			end if
		end if
	end sub
end Class
%>