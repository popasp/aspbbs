<%
Class [Code]
	private theNum,intervalMinute
	Public LastCode
	sub init
		'表名 
		'db是popasp_access类的实例化
		db.tableName = "Self_Code"		
		'主键
		db.prikey = "CodeID"
		
		intervalMinute = 1
		
		db.auto_ = Array( _
			 Array( "AddTime"  ,"POP_MVC.FormatDate( now() , ""YYYY-MM-DD HH:II:SS"" )",  1 , "function" ) _
			,Array( "EditTime" ,"POP_MVC.FormatDate( now() , ""YYYY-MM-DD HH:II:SS"" )",  2 , "function" ) _
			,Array( "EmailSendCount" , 0 ,  1 ) _
			,Array( "MobileSendCount" , 0 ,  1 ) _
			,Array( "CodeNum" , "bbs--Code.getCode" ,  1 , "callback" )_
			,Array( "ValidMinute" , 5 ,  3  )_
			,Array( "EditIP" , "" ,  1 ) _
			,Array( "EditIpAddr" , "" , 1 )_
			,Array( "IP" , "get_client_ip" ,  1, "function" ) _
			,Array( "IpAddr" , "bbs--Thread.getIpAddr2" , 1 , "callback" )_
			,Array( "EditIP" , "get_client_ip" ,  2, "function" ) _
			,Array( "EditIpAddr" , "bbs--Thread.getIpAddr2" , 2 , "callback"  )_
		)
		
		'数据验证
		db.validate_ = Array( )
	end sub
	
	function getCode
		getCode = POP_MVC.String.Rand( 100000, 999999 )
		theNum = getCode
		LastCode= theNum
	end Function
	
	'返回记录行ID
	Function AddEmailCode(Email , ValidMinute )
		dim dict
		set dict = D_
		dict("Email") = Email
		dict("ValidMinute") = ValidMinute
		dict("LimitTime") = POP_MVC.FormatDate( DateAdd( "n" , ValidMinute , now) , "YYYY-MM-DD HH:II:SS" )
		Call db.data(dict).create(1)
		AddEmailCode = db.add
	End Function
	
	'返回记录行ID
	Function ReAddEmailCode(Email , ValidMinute)
		dim rs,dict
		set rs = db.where( "Email = '" & Email & "'" ).getRow

		if rs.count = 0 then
			ReAddEmailCode = AddEmailCode( Email , ValidMinute )
		else
			ReAddEmailCode = rs("CodeID")
			'有效范围期内
			if datediff( "n" ,  now,  rs("LimitTime") ) - intervalMinute > 0 then								
				Call db.data(rs).create(2)
				Call db.save	
				LastCode= rs("CodeNum")
			else
				set dict = D_
				dict("Email") = rs("Email")
				dict("CodeNum") = getCode
				dict("CodeID") = rs("CodeID")
				Call db.data(dict).create(2)
				db.save
				Call db.where( "CodeID=" & rs("CodeID") ).setInc( "EmailSendCount" )
			end if
		end if
	End Function
	
	Function getLastCode
		getLastCode = LastCode
	End Function
End Class
%>