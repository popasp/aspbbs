<%
'操作设置
if guestConfig.DefaultTemplate = "xiunobbs" then '操作设置
	that.AjaxArgs("info") = "message"
	that.AjaxArgs("status") = "code"
	that.ajaxSuccessStatus = 0
	that.ajaxErrorStatus = -1
else
	that.AjaxArgs("info") = "msg"
	that.ajaxSuccessStatus = 0
	that.ajaxErrorStatus = 1
end if


'上传目录设置
POP_MVC.config("UPLOAD_SAVE_PATH") =  POP_MVC.config("CMS_UPLOAD_PATH") & "/" & S_("adminId") & "/"

'案例的分类ID值
'POP_MVC.config("BBS_CASE_SORTID") = 58

POP_MVC.config("APPLICATION_PREFIX") = POP_MVC.get_site_path()

POP_MVC.config("LoginNameTip") = "用户名须由 " & guestConfig.RegNameLenMin & "到" & guestConfig.RegNameLenMax & "个" & guestConfig.RegNameType & " 组成，且不能包含敏感字词!"
POP_MVC.config("PasswordTip")  = "密码应为 " & guestConfig.RegPassLenMin & "到" & guestConfig.RegPassLenMax & " 位，" & guestConfig.RegPassType

POP_MVC.config("THREAD_FIELD") = "a.TopicID,a.ZanCount,a.SortID,a.AddTime,a.IndexImage,a.DownURL,a.Title,a.IpAddr,a.EditIpAddr,a.ContentSource,a.ContentStatus,a.IsTop,a.Isrecommend,a.IsHeadline,a.IsFeatured,a.IsClosed,a.IsReplyView,a.Visits,a.IsNoComment,a.Content,a.ReplyCount,a.AnswerReward,a.EditTime,a.EditCount,a.IsSolved,a.PageDesc,a.PageKeywords,c.LoginName,c.Sign,c.AccountCash,c.Nickname,c.GroupID,c.Avatar,c.UserID,c.Experience,c.ThreadReplyView,b.GroupName"

POP_MVC.config("REPLY_FIELD") = "a.ReplyID,a.FloorStep,a.TopicID,a.IpAddr,a.ContentStatus,a.Content,a.IsAdopted,a.Reward,a.ZanCount,a.CaiCount,a.AddTime,b.UserID,b.Online,b.Avatar,b.Gender,b.Experience,b.LoginName,b.Nickname,b.UserStatus,c.GroupID,c.IsAdmin"

if POP_MVC.config("DB_TYPE") = "access" then
	POP_MVC.config("THREAD_FIELD1") =  "UserID as uid,SignDays as days,SignTime as [time]"
	POP_MVC.config("THREAD_FIELD2") =  "UserID as uid,SignCount as days,SignTime as [time]"
	POP_MVC.config("SIGN_FIELD1")   =   ""
else
	POP_MVC.config("THREAD_FIELD1") =  "UserID as uid,SignDays as days,SignTime as [time]"
	POP_MVC.config("THREAD_FIELD2") =  "UserID as uid,SignCount as days,SignTime as `time`"
end if

POP_MVC.config("THREAD_TABLE") = "{prefix}self_GuestTopic as a,{prefix}UserGroup as b,{prefix}User as c"
POP_MVC.config("REPLY_TABLE") = "{prefix}self_GuestReply as a,{prefix}User as b,{prefix}UserGroup as c"


that.d("ExtendLinks") = split( repNull(guestConfig.ExtendLinks) , ";" )

if guestConfig.DefaultTemplate <> "bbs" then
	POP_MVC.config("TMPL_ACTION_SUCCESS") = "./Templates/" & templateName_ & "/html/" & templateName_ & "/success.html"
	POP_MVC.config("TMPL_ACTION_ERROR") = "./Templates/" & templateName_ & "/html/" & templateName_ & "/error.html"
end if

%>