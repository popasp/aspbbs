<%
Class [IP]
	function getTQQWryAddr(ip)
		getTQQWryAddr  = P_("POPASP_TQQWry").getProvince(ip)
	end Function

	'显示404页面
	Public Function getApiAddr(ip)
		getApiAddr = getIpApi(ip)
	End Function
	
	Function getFromPconline( ip )
		dim str,pro,attr,IPAPI
		IPAPI = "https://whois.pconline.com.cn/ipJson.jsp?ip="	
		str = P_("http").get( IPAPI & ip)
		pro = POP_MVC.String.getInStr( str, """pro"":""" , """" )
		if pro = "" then
			getFromPconline = POP_MVC.String.getInStr( str, """addr"":""" , """" )
		else
			getFromPconline = pro
		end if
	End Function
	
	Function getFreeapi( ip )
		dim str,pro,attr,IPAPI
		IPAPI = "https://freeapi.ipip.net/"	
		str = P_("http").get( IPAPI & ip)
		if not isNul( str ) then
			pro = POP_MVC.String.getInStr( str, ",""" , """" )
			getFreeapi = pro
		end if
	End Function
	
	Function getIpApi( ip )
		dim str,pro,attr,IPAPI
		IPAPI = "http://ip-api.com/json/"	
		str = P_("http").get( IPAPI & ip & "?lang=zh-CN" )

		if not isNul( str ) then
			pro = POP_MVC.String.getInStr( str, "regionName"":""" , """" )
			getIpApi = pro
		end if
	End Function
	
	Function getTableIpAddr(ByVal table,IpField,IpValue,AddrField)
		dim where,preStr
		if IpValue = "" then
			getTableIpAddr = ""
			Exit Function
		end if
		
		set where = POP_MVC.SCD
		
		preStr = POP_MVC.String.getInStr( IpValue, POP_MVC.String.getInStr( IpValue,"", "." ) & "." , "." ) & "."
		
		where(IpField) = Array( "like" , preStr & "%" )
		
		if POP_MVC.CONFIG("DB_TYPE") = "sqlite3" then
			where( AddrField & " <> '' and NOT " & AddrField & " IS NULL" ) = null
		else
			where( AddrField & " <> '' and NOT ISNULL(" & AddrField & ")" ) = null
		end if		
		getTableIpAddr = B_(table).where( where ).field( AddrField ).getOne
		set where = nothing
	End Function
End Class
%>