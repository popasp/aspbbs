<%
Class Upload4summernote 'Extends bbs--Common
	sub upload		
		dim dict,filename,picSuffix,fileSuffix,extname,tempName,mumaFlag
		dim logID
		
		'图片上传类型
		picSuffix  = POP_MVC.config("UPLOAD_IMAGE_TYPES")
		
		'文件上传类型
		fileSuffix = POP_MVC.config("UPLOAD_DOWNLOAD_TYPES")
		
		'非管理员只能上传图片
		if 1 - S_("IsAdmin") = 1 then
			POP_MVC.config("UPLOAD_ALLOW_TYPES") = picSuffix
		end if
		
		'上传文件
		filename = K_("bbs--Public").upload( "summernote" ,logID )
		
		set dict  = D_
		if filename <> "" then
			dict("status") = that.ajaxSuccessStatus
			dict("msg") = filename
			dict("filename") = POP_MVC.file.basename( filename )
			
			'文件的扩展名
			extname = POP_MVC.file.extName( Array( dict("filename") , true ) ) 
			extname = LCase(extname)			
		
			if not POP_MVC.Arr.iExists( split(picSuffix , ";") , extName ) then			
				if logID = "" then
					if extname = "asp" or extname = "html" or extname = "htm" then
						tempName = left(filename, len(filename)-len(extName) -1 ) & ".rar"
						Call POP_MVC.file.rename( filename , tempName  ) 
					end if
					dict("link") = "<a target='_blank' title='" & POP_MVC.Uploader.SrcName & "' href='" & tempName & "' download=""" & POP_MVC.String.encodeHtml(POP_MVC.Uploader.SrcName)  & """>" & POP_MVC.Uploader.SrcName & "</a>"
				else
					dict("link") = K_("bbs--Public").download(logID, POP_MVC.Uploader.SrcName )				
				end if
			else
				mumaFlag = K_("bbs--Public").TestPicMuma( filename )
				if mumaFlag <> "" then
					dict("msg") = "static/images/nopic.gif"
					dict("filename") = "非法上传" & mumaFlag & "挂马文件"
					Call K_("bbs--Public").DealPicMuma( filename )
				end if
			end if
		else
			dict("status") = that.ajaxErrorStatus
			dict("msg") = POP_MVC.Uploader.description
		end if
		that.AjaxData(dict)
	end sub
End Class
%>