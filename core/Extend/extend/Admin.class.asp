<%
Class Admin 'Extends bbs--Common
	private orderStr,statusField,TimeDict
	private tableName,sqlfile,extendName,bookConfig
	private isWap,theStatus
	
	Sub initialize
		if S_("IsAdmin") <> 1 then
			that.error("无权操作!")
		end if
	
		tableName = "Blogs/Blog"
		extendName = "Extend"		
		
		set bookConfig = XM_( "Extend--Thread" ).db.path("Configs/Config").field("ConfigName,ConfigValue").getObject
		that.d("bookConfig") = bookConfig
	
		objCMS.cmsPrefix = "aspbbs"
		guestConfig.DefaultTemplate = extendName
		objCMS.htmlFilePath = "html"
		isWap = checkWap()		
	End Sub
	
	private sub getListData
		dim rsObj,where,pathWhere	

		set where = D_		

		'适用于搜索
		if POP_MVC.req("keys") <> "" then
			where( "Title" ) = Array( "instr" , POP_MVC.req("keys") )
		else
			where("ContentStatus") = theStatus
		end if
	
		set topicRS = XM_("extend--Thread").db.path(tableName).page(array( null, bookConfig.PageSize ) ).field("TopicID,UserID,AddTime,Title,UserID,Visits,Avatar,LoginName,StripContent,Alias,PageDesc,ContentStatus").where(where).select

		that.d("recordCount") = XM_("extend--Thread").db.curNodes.length
				
		that.d("thread") = topicRS	
		
		dim obj
		set obj = objCMS
		
		if LCase(POP_MVC.a) = "search" then
			if POP_MVC.req("keys") <> "" then
				templatePath= obj.getTemplatePath( "Admin/search" )
			else
				templatePath= obj.getTemplatePath( "Admin/" & POP_MVC.a )
			end if
		else
			templatePath= obj.getTemplatePath( "Admin/" & POP_MVC.a )
		end if

		obj.load(templatePath)	

		obj.parseHtml	
		
		obj.output
	end sub
	
	sub index
		theStatus = 1
		Call getListData
	end sub
	
	sub Unchecked
		theStatus = 0
		Call getListData
	end sub
	
	sub Failed
		theStatus = 3
		Call getListData
	end sub
	
	sub recycle
		theStatus = 2
		Call getListData
	end sub
	
	
	'搜索页
	sub search
		Call getListData
	end sub
	
	
	private Sub setContentStatus( id , ContentStatus )
		dim rs
		if not that.IsID( id ) then
			that.error( "非法操作" )
		end if
		
		set rs = XM_("extend--Thread").db.path(tableName & "[TopicID=" &id & "]").find
		
		if rs.count = 0 then
			that.error( "该" & bookConfig.ThreadTitle & "不存在！" )
		end if
		Call XM_("extend--Thread").db.path(tableName & "[TopicID=" &id & "]").setField( "ContentStatus" , ContentStatus)
	End Sub

	'采纳
	sub accept
		Call setContentStatus( that.req("id") , 1 )
		that.success( Array("还原" , -1) )
	end sub	
	
	'未审
	sub uncheck
		Call setContentStatus( that.req("id") , 0 )
		that.success( Array("还原" , -1) )
	end sub	
	
	'拒绝
	sub refuse
		Call setContentStatus( that.req("id") , 3 )
		that.success( Array("还原" , -1) )
	end sub	
	
	'第一次软删除del
	sub remove
		Call setContentStatus( that.req("id") , 2 )
		that.success( Array("还原" , -1) )
	end sub
	
	private sub clearTable(ContentStatus)
		Call XM_("extend--Thread").db.path(tableName & "[ContentStatus=" & ContentStatus & "]").remove
		that.success( "删除成功" )
	End Sub
	
	'清空未审核
	Sub clearUnchecked
		Call clearTable(0)
	end sub
	
	'清空审核未过
	Sub clearFailed
		Call clearTable(3)
	end sub
	
	'清空回收站
	Sub clearRecycle
		Call clearTable(2)
	end sub
	
	'第2次删除，彻底删除
	sub remove2
		dim id,rs,dict,DelPath,recycleDir
		id = that.req("id")
		set rs = XM_("extend--Thread").db.path(tableName & "[TopicID=" &id & "]").find
		
		if rs.count = 0 then
			that.error( "该" & bookConfig.ThreadTitle & "不存在！" )
		end if

		Call XM_("extend--Thread").db.path(tableName & "[TopicID=" &id & "]").remove
		that.success( "成功将该" & bookConfig.ThreadTitle & "彻底删除" )
	end sub
	

	'未导入数据库上传文件检测
	sub dbTest
		dim action,time1,time2,filepath,arr,flag
		
		Call K_("Extend--Tools").dbTest
		
		response.end

		filepath = POP_MVC.appPath & "/runtime/UploadLog/table.txt"
		'如果已经保存了检测，且未再上传，从文件中显示
		if POP_MVC.file.isfile(filepath) then
			time1 = POP_MVC.file.mtime( POP_MVC.appPath & "/runtime/UploadLog/table.txt" )
			time2 = POP_MVC.file.mtime( POP_MVC.config("CMS_UPLOAD_PATH") )
			if  DateDiff("s", time2, time1) < 0 then
				flag = true
			else
				response.write K_("bbs--FileTool").file_get_test("UploadLog")
			end if
		end if
		
		if flag then
			Call K_("Extend--Tools").dbTest
		end if
	end sub
	
	'配置
	sub settings
		dim data,html
		'取得配置
		set data = XM_("Extend--Thread").db.path("Configs/Config").select	

		that.d("ListCount") = XM_("Extend--Thread").db.path(tableName & "[ContentStatus=1]").Count
		that.d("RecycleCount") = XM_("Extend--Thread").db.path(tableName & "[ContentStatus=2]").Count
		that.d("UncheckedCount") = XM_("Extend--Thread").db.path(tableName & "[ContentStatus=0]").Count
		that.d("FailedCount") = XM_("Extend--Thread").db.path(tableName & "[ContentStatus=3]").Count

		html = P_("flyui4xml").PageConfigContent(data)		
		that.d("htmlstr") = html
		
		objCMS.show( "Admin/set" )
	end sub	
	
	'js自动填充
	sub jsAuto
		dim dict
		set dict = XM_("Extend--Thread").db.path("Configs/Config").field("ConfigName,ConfigValue").getKeyValue
		
		Call K_("bbs--Tools").js_Auto( dict )
	end sub
	
	'修改配置值
	sub doEditConfigs
		dim key,dict,MailAlert
		set dict = POP_MVC.form2dict
		
		for each key in dict
			Call XM_("Extend--Thread").db.path("Configs/Config[ConfigName='"& key &"']").MergeField( "ConfigValue" , dict(key) )
		next

		Call K_( "bbs--Public" ).CtrlLog( 1, "用户ID: " & S_("adminId") & " ，修改了配置" , "保存成功" )
	end sub
	
	'上传图片
	sub uploads
		dim filename,logID
		POP_MVC.config("WATERMARK_PATH") = ""
		POP_MVC.config("UPLOAD_ALLOW_TYPES") = "jpg;jpeg;png;gif;bmp;pcx;svg"
		filename = POP_MVC.upload("file")
		if filename <> "" then
			that.success( filename )
		else
			that.error( POP_MVC.Uploader.description )
		end if
	end sub
	
	sub testASPJPEG
		if POP_MVC.IsInstall("Persits.Jpeg") then
			that.success( Array( "恭喜您，ASPJPEG组件已安装" ,"window.close",5 ) )
		else
			that.success( Array( "对不起，ASPJPEG组件未在服务器安装" ,"window.close",5 ) )
		end if
	end sub
End Class
%>