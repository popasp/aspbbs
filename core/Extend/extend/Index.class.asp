<%
Class Index 
	private tableName,isWap,theStatus,orderStr
	sub initialize
		tableName = "Blogs/Blog"
		set bookConfig = XM_( "extend--Thread" ).db.path("Configs/Config").field("ConfigName,ConfigValue").getObject
		
		that.d("bookConfig") = bookConfig
		isWap = checkWap()

		objCMS.cmsPrefix = "aspbbs"
		guestConfig.DefaultTemplate = "extend"
		objCMS.htmlFilePath = "html"	
	end sub
	
	sub demo
		dim rs
		set rs = B_("User").where("LoginName='admin'").find
		that.d("userRS") = rs
		objCMS.show("Index/demo")
	end sub
	
	sub detail
		dim rs,id,where
		id = that.get("id")		
		set rs = XM_("extend--Thread").db.path( tableName & "[TopicID=" & id & "]").find
		
		if rs.count = 0 then
			that.error("要查找的组件不存在!")
		end if
		that.d("thread") = rs
		
		set where = D_
		where("UserID") = rs("UserID")
		that.d("userRS") = B_("User").where(where).find
		Call XM_("extend--Thread").db.path( tableName & "[TopicID=" & id & "]").setInc("Visits")
		objCMS.show("Index/detail")
	end sub
	
	private sub getListData
		dim rsObj,where,pathWhere
		
		theStatus = 1
		
		pathWhere = "ContentStatus=" & theStatus

		set where = D_

		'适用于搜索
		if POP_MVC.req("keys") <> "" then
			where( "Title" ) = Array( "instr" , POP_MVC.req("keys") )
		end if
	
		set topicRS = XM_("extend--Thread").db.path(tableName & "[" & pathWhere & "]").page(array( null, bookConfig.PageSize ) ).field("TopicID,UserID,AddTime,Title,UserID,Visits,Avatar,LoginName,StripContent,Alias,PageDesc").where(where).select

		that.d("recordCount") = XM_("extend--Thread").db.curNodes.length
				
		that.d("thread") = topicRS	
		
		dim obj
		set obj = objCMS
		
		if LCase(POP_MVC.a) = "search" then
			if POP_MVC.req("keys") <> "" then
				templatePath= obj.getTemplatePath( "Index/search" )
			else
				templatePath= obj.getTemplatePath( "Index/index" )
			end if
		else
			templatePath= obj.getTemplatePath( "Index/index" )
		end if

		obj.load(templatePath)	

		obj.parseHtml	
		
		obj.output
	end sub
	
	sub index
		Call getListData
	end sub
	
	
	'搜索页
	sub search
		Call getListData
	end sub	
	
	'获取文章内容
	sub getContent
		dim id,rs

		id = that.get("id")
		set rs = XM_("extend--Thread").db.path( tableName & "[TopicID=" & id & "]").find
		
		if rs.count = 0 then
			that.error("要修改的帖子不存在!")
		end if	
		
		response.write XM_("extend--Thread").getContent(rs("Content"))
		response.end
	end sub
end Class
%>