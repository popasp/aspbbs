<%
Class [Public]
	Function getCnStatus( ContentStatus )
		dim status
		if ContentStatus = 0 then
			status = "未审核"
		elseif ContentStatus = 1 then
			status = "审核通过"
		elseif ContentStatus = 2 then
			status = "已删除"
		elseif ContentStatus = 3 then
			status = "审核未过"
		end if
		getCnStatus = status
	End Function
End Class
%>