<%
Class Thread
	Private index4blog_ , index4sort_ 
	sub init
		'db是popasp_xmldb类的实例化
		path = "Blogs"
		db.db_path = "./Templates/extend/#data/#book#.xml"
		
		db.auto_ = Array( _
			 Array( "TopicID" , "extend--Thread.incBlogID" ,  1 , "callback") _
			,Array( "Visits" ,0,  1 ) _
			,Array( "ContentStatus" ,iif(bookConfig.ThreadAddCheck = 0 or S_("GroupID") = 1 , 1 , 0),  1 ) _
			,Array( "ContentStatus" ,iif(bookConfig.ThreadAddCheck = 0 or S_("GroupID") = 1 , 1 , 0),  2 ) _
			,Array( "UserID" , Session("adminId") ,  3 ) _
			,Array( "LoginName" , Session("LoginName") ,  3 ) _
			,Array( "Avatar" , Session("Avatar") ,  3 ) _
			,Array( "AddTime" ,"POP_MVC.FormatDate( now() , ""YYYY-MM-DD HH:II:SS"" )",  1 , "function" ) _
			,Array( "EditTime" ,"POP_MVC.FormatDate( now() , ""YYYY-MM-DD HH:II:SS"" )",  2 , "function" ) _
			,Array( "IP" , "get_client_ip" ,  1, "function" ) _
			,Array( "Title" , "extend--Thread.stripTags" , 3 , "callback" , array() )_
			,Array( "Content" , "extend--Thread.SaveContent" , 3 , "callback" , array() )_
		)
		
		'数据验证
		db.validate_ = Array( _
			 Array( "Title" ,"",  "标题不能为空"  , 0, "notempty" , 3  ) _
			,Array( "Title","1,255","标题长度不能超过255个字符" , 0 , "length" , 3 ) _
			,Array( "verify","","验证码不能为空" , 0 , "notempty" , 3 ) _		
			,Array( "verify","^\d{4}$","验证码不正确" , 0 , "regex" , 3 ) _		
			,Array( "verify","","验证码不正确" , bookConfig.ThreadAddVerify , "verify" , 1 ) _
			,Array( "verify","","验证码不正确" , bookConfig.ThreadEditVerify , "verify" , 1 ) _
		)
	end sub
	
	Function incBlogID
		incBlogID = index4blog + 1

	End Function

	
	Public Property Get index4blog()
		if index4blog_ = "" then
			index4blog_ = bookConfig.index4blog
		end if
		index4blog = index4blog_	
	End Property
	
	Public Property Let index4blog( value )
		index4blog_ = value
		Call setSiteConfig( "index4blog" , index4blog_ )
	End Property

	
	Sub setSiteConfig( ConfigName, ConfigValue )
		Call db.path( "Configs/Config[ConfigName='" & ConfigName & "']"  ).setField( "ConfigValue" , ConfigValue )
	End Sub
	
	function stripTags( value )
		if isNul(value) then
			stripTags = ""
		else
			stripTags = POP_MVC.String.strip_tags( value , "html" )
		end if
		'去掉换行
		stripTags = Replace(stripTags, Chr(10), "")
		stripTags = Replace(stripTags, Chr(13), "")
		
		'将连续三个空格替换成空
		stripTags = Replace(stripTags, "   ", "")
	End function
	
	Function getTopicPath( fileName )
		getTopicPath = getTopicDir() & filename
	End Function
	
	'获取文章内容
	Function getContent( filename )
		getContent = POP_MVC.file_get_contents( getTopicPath( fileName ) )
	End Function
	
	Function SaveContent( value )
		dim filepath,filename
		
		if POP_MVC.form("TopicID") <> "" then
			filename = POP_MVC.form("TopicID") & ".txt"
		else
			filename = (index4blog + 1) & ".txt"
		end if		
		filepath = getTopicDir() & filename
		Call POP_MVC.file_put_contents_without_bom( filepath , value )
		SaveContent = filename
	End Function
	
	Function getTopicDir( )
		getTopicDir =  "./Templates/extend/#data/c/"
	End Function
End Class
%>