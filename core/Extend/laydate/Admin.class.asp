<%
Class Admin 
	Private diary_path
	sub initialize	
		diary_path = K_("laydate--Public").db_path
		POP_MVC.config("CMS_TAG_LABEL") = "aspbbs"
		objCMS.cmsPrefix = "aspbbs"
		
		Call CheckAdminPower
	end sub
	
	'写入权限判断
	sub CheckAdminPower
		if S_("adminId") = "" then
			that.error( Array("您尚未登陆！" , "?Login.html" ))
		end if
	
		if S_("IsAdmin") <> 1 then	
			that.error("日记功能仅限管理员操作！")	
		end if
	End Sub
	
	'可写入
	sub Index
		dim obj,templatePath,theYear
		
		'实例化POPASP_CMS类
		set obj = objCMS

		'模板名，这里要注意，模板文件名必须为 gbook.html
		templatePath= "./Templates/laydate/html/admin.html"
		
		'模板文件不存在时
		if not POP_MVC.file.isFile(templatePath) then 
			Call obj.ShowError4Template( templatePath )
		end if	

		'加载
		obj.load(templatePath)	
		
		theYear = that.get("id")
		
		if theYear = "" then
			theYear = Year( date() )
		end if
		
		that.d("curYear") = theYear
		
		
		'解析模板
		obj.parseHtml

		'输出解析后的模板
		obj.Output
	end sub
	
	sub getData
		dim rs,theYear
		
		theYear =  that.get("id")
		if isNul( theYear ) then
			theYear = year( date() )
		end if
		'set rs = X_( Array( "Diarys[@year='" & theYear & "']/Diary" , diary_path ) ).field("time,value").top( 365 ).select
		set rs = X_( Array( "Diarys/Diary" , diary_path ) ).field("time,value").top( 1000 ).select
		Call that.LayuiTableAjax(array( rs , rs.count), "请求成功" , 0)
	end sub
	
	sub setData
		dim data,item,theYear,arr,i,j
		
		set data = POP_MVC.form2dict
		arr = Array()
		if data.count <1 then
			exit sub
		end if			
		
		for i = 0 to data.count/2 - 1
			if isDate(data("[" & i & "][time]")) then
				theYear = Year( data("[" & i & "][time]") )
				Call X_( Array( "Diarys[@year='" & theYear & "']" , diary_path ) ).remove
				if not POP_MVC.arr.exists( arr, theYear	) then					
					POP_MVC.arr.push arr, theYear				
				end if
			end if
		next

		'排序
		POP_MVC.arr.rsort arr
		
		'添加年节点
		for i = 0  to ubound( arr )
			theYear = arr(i)
			Call addDiarys( theYear )
		next		
		
		set item = POP_MVC.SCD
		for i = 0 to data.count/2 - 1
			item("value") = data("[" & i & "][value]")
			item("time") = data("[" & i & "][time]")
			theYear = Year( data("[" & i & "][time]") )	
			if POP_MVC.arr.exists( arr, theYear ) and item("value") <> "" then
				if X_( Array( "Diarys[@year='" & theYear & "']/Diary[time='" & item("time") & "']" , diary_path ) ).count = 0 then
					Call X_( Array( "Diarys[@year='" & theYear & "']" , diary_path ) ).data(item).prepend( "Diary" , "" )
				else
					Call X_( Array( "Diarys[@year='" & theYear & "']/Diary[time='" & item("time") & "']" , diary_path ) ).setField("value",item("value"))
				end if
			end if
		next
		objCMS.clearCache
	end sub
	
	'添加年节点
	private sub addDiarys( byVal theYear )
		dim node,obj,pnode
		set obj = X_( Array( "Diarys[@year='" & theYear & "']" , diary_path ) ).init
		if not X_( Array( "Diarys[@year='" & theYear & "']" , diary_path ) ).exists	then
			set node = obj.createNode( "Diarys" , "" )
			Call obj.setNodeAttr( node, "year" , theYear )
			set pnode = obj.getNode(  "aspblog" , 0 )
			pnode.appendChild( node )
			Call obj.SaveXml
		end if
	end sub
end Class
%>