<%
Class Index 
	Private diary_path
	sub initialize	
		diary_path = K_("laydate--Public").db_path
		POP_MVC.config("CMS_TAG_LABEL") = "aspbbs"
		objCMS.cmsPrefix = "aspbbs"
	end sub
	
	'首页，展示，均可查看
	sub index
		dim obj,templatePath,theYear
		'实例化POPASP_CMS类
		set obj = objCMS

		'模板名，这里要注意，模板文件名必须为 gbook.html
		templatePath= "./Templates/laydate/html/index.html"
		
		'模板文件不存在时
		if not POP_MVC.file.isFile(templatePath) then 
			Call obj.ShowError4Template( templatePath )
		end if	
		
		that.d("data") = X_( Array( "Diarys/Diary" , diary_path ) ).field("time,value").top( 1000 ).select
		
		'加载
		obj.load(templatePath)	
		
		theYear = that.get("id")
		
		if theYear = "" then
			theYear = Year( date() )
		end if
		
		that.d("curYear") = theYear
		
		
		'解析模板
		obj.parseHtml

		'输出解析后的模板
		obj.Output
	end sub
end Class
%>