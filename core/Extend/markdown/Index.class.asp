<%
Class Index 
	'首页
	sub index
		dim obj,templatePath,theYear
		'实例化POPASP_CMS类
		set obj = objCMS

		'模板名，这里要注意，模板文件名必须为 gbook.html
		templatePath= "templates/markdown/html/markdown.html"
		
		'模板文件不存在时
		if not POP_MVC.file.isFile(templatePath) then 
			Call obj.ShowError4Template( templatePath )
		end if	

		'加载
		obj.load(templatePath)	
		
		'解析模板
		obj.parseHtml

		'输出解析后的模板
		obj.Output
	end sub
end Class
%>