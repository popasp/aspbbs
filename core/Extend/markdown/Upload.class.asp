<%
Class Upload
	sub search		
		dim dict,filename,picSuffix,fileSuffix,extname,tempName,errID
		picSuffix = POP_MVC.config("UPLOAD_IMAGE_TYPES")
		fileSuffix = POP_MVC.config("UPLOAD_DOWNLOAD_TYPES")

		filename = POP_MVC.upload("editormd-image-file")
		
		if not K_("UploadLog--Public") is nothing then
			if filename <> "" then
				errID = 0
			else
				errID = POP_MVC.Uploader.ErrorID
			end if
			Call K_("UploadLog--Public").upload( POP_MVC.Uploader.SrcName ,  filename , POP_MVC.file.filesize(filename), str , errID ,POP_MVC.Uploader.Description)
		end if		
		
		set dict  = D_
		if filename <> "" then
			dict("success") = 1
			dict("url") = filename
			dict("filename") = POP_MVC.Uploader.SrcName
			extname = POP_MVC.file.extName( Array( dict("filename") , true ) ) 
			extname = LCase(extname)

			if not POP_MVC.Arr.iExists( split(picSuffix , ";") , extName ) then
				if extname = "asp" or extname = "html" or extname = "htm" then
					tempName = left(filename, len(filename)-len(extName) -1 ) & ".rar"
					Call POP_MVC.file.rename( filename , tempName  ) 
				end if	
				dict("link") = "<a target='_blank' title='" & POP_MVC.Uploader.SrcName & "' href='" & tempName & "' download=""" & POP_MVC.String.encodeHtml(POP_MVC.Uploader.SrcName)  & """>" & POP_MVC.Uploader.SrcName & "</a>"
			end if
		else
			dict("success") = 0
			dict("message") = POP_MVC.Uploader.description
		end if
		that.ajaxData(dict)
		
	end sub
End Class
%>