<%
Class Admin 'Extends bbs--Common
	private tableName,isWap,fileType
	sub initialize
		if S_("adminId") = "" then
			that.error( Array("您尚未登陆！" , "?Login.html" ))
		end if
		
		if NOT POP_MVC.file.isFile( XM_("md--Thread").db.db_path ) then
			that.error( Array("您的文档尚未开通，请联系超管开通！" , "?" ))
		end if
	
		tableName = "Blogs/Blog"
		POP_MVC.Config("REQ_IGNORE_HTML") = "test-editormd-markdown-doc"
		POP_MVC.Config("REQ_IGNORE_HTML") = "test-editormd-markdown-doc"
		set bookConfig = XM_( "md--Thread" ).db.path("Configs/Config").field("ConfigName,ConfigValue").getObject
		that.d("bookConfig") = bookConfig
		
		isWap = checkWap()
		objCMS.cmsPrefix = "aspbbs"
		guestConfig.DefaultTemplate = "md"
		objCMS.htmlFilePath = "html"
	end sub
	
	sub index
		Call list
	end sub
	
	'首页
	sub list
		dim obj,templatePath,id,rs,sortRS
		'实例化POPASP_CMS类
		set obj = objCMS

		'模板名，这里要注意，模板文件名必须为 gbook.html
		templatePath= obj.getTemplatePath("Admin/list")
		
		that.d("xmlfile") = XM_("md--Thread").db.db_path
		
		id = that.get("id")
		
		if id <> "" then
			set rs = XM_("md--Thread").db.path( tableName & "[TopicID=" & id & "]").find		
			if rs.count > 0 then				
				that.d("thread") = rs
				set sortRS = XM_("md--Thread").db.path( "Blogs[@SortID='" & rs("SortID") & "']/SortInfo").find
				that.d("sort") = sortRS
			else
				id = ""
			end if
		end if
		that.d("theID") = id

		
		'模板文件不存在时
		if not POP_MVC.file.isFile(templatePath) then 
			Call obj.ShowError4Template( templatePath )
		end if	
		
		that.d("SelectSort") = Request.Cookies( "SelectSort" )
		Call getZtree( id )
		
		'加载
		obj.load(templatePath)	

		'解析模板
		obj.parseHtml

		'输出解析后的模板
		obj.Output
	end sub	
	
	
	'搜索
	sub search
		dim obj,templatePath,id,rs
		'实例化POPASP_CMS类
		set obj = objCMS

		'模板名，这里要注意，模板文件名必须为 gbook.html
		templatePath= obj.getTemplatePath("Admin/search")
		
		that.d("xmlfile") = XM_("md--Thread").db.db_path
		
		id = that.get("id")
		
		if id <> "" then
			set rs = XM_("md--Thread").db.path( tableName & "[TopicID=" & id & "]").find
			if rs.count > 0 then				
				that.d("thread") = rs
				that.d("theID") = id
				set sortRS = XM_("md--Thread").db.path( "Blogs[@SortID='" & rs("SortID") & "']/SortInfo").find
				that.d("sort") = sortRS
			else
				id = ""
			end if
		end if

		
		'模板文件不存在时
		if not POP_MVC.file.isFile(templatePath) then 
			Call obj.ShowError4Template( templatePath )
		end if	
		
		that.d("SelectSort") = Request.Cookies( "SelectSort" )
		Call getSoZtree
		
		'加载
		obj.load(templatePath)	

		'解析模板
		obj.parseHtml

		'输出解析后的模板
		obj.Output
	end sub	
	
	'添加页面
	sub add
		dim obj,templatePath
		'实例化POPASP_CMS类
		set obj = objCMS

		'模板名，这里要注意，模板文件名必须为 gbook.html
		
		if checkWap() then
			templatePath=  obj.getTemplatePath("Admin/add-m")
		else
			templatePath=  obj.getTemplatePath("Admin/add")
		end if
		
		that.d("xmlfile") = XM_("md--Thread").db.db_path
		
		'模板文件不存在时
		if not POP_MVC.file.isFile(templatePath) then 
			Call obj.ShowError4Template( templatePath )
		end if	
		
		that.d("SelectSort") = Request.Cookies( "SelectSort" )
		
		'加载
		obj.load(templatePath)	
		
		'解析模板
		obj.parseHtml

		'输出解析后的模板
		obj.Output
	end sub
	
	'修改页面
	sub edit
		dim id,rs

		id = that.get("id")
		set rs = XM_("md--Thread").db.path( tableName & "[TopicID=" & id & "]").find
		
		if rs.count = 0 then
			that.error("要修改的帖子不存在!")
		end if	
		
		that.d("thread") = rs

		dim obj,templatePath
		set obj = objCMS
		if checkWap() then
			templatePath=  obj.getTemplatePath("Admin/edit-m")
		else
			templatePath=  obj.getTemplatePath("Admin/edit")
		end if
		
		that.d("xmlfile") = XM_("md--Thread").db.db_path
		
		'模板文件不存在时
		if not POP_MVC.file.isFile(templatePath) then 
			Call obj.ShowError4Template( templatePath )
		end if	
		
		
		obj.load(templatePath)	
		obj.parseHtml
		obj.output
	end sub	
	
	'获取文章内容
	sub getContent
		dim id,rs

		id = that.get("id")
		set rs = XM_("md--Thread").db.path( tableName & "[TopicID=" & id & "]").find
		
		if rs.count = 0 then
			that.error("要修改的帖子不存在!")
		end if	
		
		response.write XM_("md--Thread").getContent(rs("Content"))
	end sub
	
	sub getZtree(id)
		dim data,dict,rs,item,k,line,flag,filesize,filepath
		
		flag = false
		
		set data = XM_("md--Thread").db.path("Blogs/SortInfo").field("SortID,SortName").select
		
		for each key in data
			set item = data(key)
			item.key("SortName") = "name"
			item.key("SortID") = "id"
			item.remove("_NODE_INDEX_")
			item("pId") = 0
			item("isParent") = 1

			set dict = XM_("md--Thread").db.path("Blogs[@SortID=" & item("id") & "]/Blog").field("Title,TopicID").select
			for each k in dict
				set line = dict(k)
				line.key("Title") = "name"
				line.key("TopicID") = "id"
				if line("id") = id then
					item("open") = 1	
					filepath = XM_("md--Thread").db.path("Blogs/Blog[TopicID=" & line("id") & "]").field("Content").getOne
					filesize =  POP_MVC.file.filesize(XM_("md--Thread").getTopicPath( filepath ))
					line("name") = line("name") & " (" & byte2size(filesize) & ")"
				end if
				line("url") = "?md--Admin_list_" & line("id")
				line("target") = "_self"
			next
			if isWap then
				item("open") = 1
			end if
			item("name") = item("name") & " (" & dict.count & ")"
			item.add "children",dict
		next
		
		set dict = D_
		dict("id") = 0 
		dict("pId") = 0 
		dict("name") = "目录" 
		dict("open") =  true
		dict("target") = "main"
		
		POP_MVC.dict.unshift data,"",dict		
		
		
		that.d("zNodes") = data
	end sub
	
	sub getSoZtree
		dim data,dict,rs,item,k,line,id,flag,where,keys,nums
		
		keys = POP_MVC.req("keys")
		nums = 0
		
		set where = D_
		
		where( "StripContent" ) = Array( "instr" , keys )
		
		set data = XM_("md--Thread").db.path("Blogs/SortInfo").field("SortID,SortName").select
		
		for each key in data
			set item = data(key)
			item.key("SortName") = "name"
			item.key("SortID") = "id"
			item.remove("_NODE_INDEX_")
			item("pId") = 0
			item("isParent") = 1
			item("open") = 1
			set dict = XM_("md--Thread").db.where(where).path("Blogs[@SortID=" & item("id") & "]/Blog").field("Title,TopicID").select
			if dict.count = 0 then
				data.remove(key)
			else
				for each k in dict
					set line = dict(k)
					line.key("Title") = "name"
					line.key("TopicID") = "id"
					line("url") = "?c=md--Admin&a=search&id=" & line("id") & "&keys=" & keys
					line("target") = "_self"
				next
				nums = nums + dict.count
				item("name") = item("name") & " (" & dict.count &")"
			end if			
			item.add "children",dict
		next
		
		set dict = D_
		dict("id") = 0 
		dict("pId") = 0 
		dict("name") = "搜索 (" & nums & "): " & keys
		dict("open") =  true
		dict("target") = "main"
		
		POP_MVC.dict.unshift data,"",dict		
		
		
		'that.ajaxdata(data )
		that.d("zNodes") = data
		
		'var_export data
		'response.end
	end sub
	
	
	'存草稿
	sub doDraft
		if that.form("TopicID") <> "" then
			Call edit_draft
		else
			Call add_draft
		end if
	end sub
	
	'添加操作
	sub doAdd
		if that.form("TopicID") <> "" then
			Call doEdit
			Exit Sub
		end if
	
		dim data,SortID,posID,id,NewSortName,key
		set data = POP_MVC.form2dict		
		
		data.key("test-editormd-markdown-doc") = "Content"
		data.key("test-editormd-html-code") = "StripContent"
		data("StripContent") =  delBlank(data("StripContent"))
		SortID = POP_MVC.String.before(data("SortID"),":")
		
		if SortID = "" then
			'添加章节
			NewSortName = data("NewSortName")
			SortID = XM_( "md--Thread" ).index4sort + 1
			XM_( "md--Thread" ).index4sort = SortID
			Call addSort( NewSortName , SortID )
		else
			posID = POP_MVC.String.after(data("SortID"),":")
		end if

		data("SortID") = SortID
		data.remove("NewSortName")	
	
		
		if not XM_( "md--Thread" ).db.data(data).create(1) then
			that.error( XM_( "md--Thread" ).db.error )
		end if
		
		set data = XM_( "md--Thread" ).db.getData()

		
		for each key in data
			if data(key) = "" then
				data.remove(key)
			end if
		next
		
		P_("cookie").Assign "SelectSort" , SortID
		
		id = XM_( "md--Thread" ).db.path("Blogs[@SortID=" & SortID & "]").data(data).append("Blog" , "TopicID")
		
		if  id > 0 then			
			if posID <> "" then
				XM_( "md--Thread" ).db.path("Blogs[@SortID=" & SortID & "]/Blog[TopicID=" & id &  "]").setPosition(Clng(posID))
			end if
			id = XM_( "md--Thread" ).index4blog + 1
			XM_( "md--Thread" ).index4blog = id
			that.success( Array("文章添加成功" , "?md--Admin_Add") )
		else
			that.error( "添加失败，请重新提交" )
		end if
	end sub
	
	'添加存草稿操作
	private sub add_draft
		dim data,SortID,posID,id,NewSortName,key
		set data = POP_MVC.form2dict	
		
		data.key("test-editormd-markdown-doc") = "Content"
		data.key("test-editormd-html-code") = "StripContent"
		data("StripContent") =  delBlank(data("StripContent"))	
		SortID = POP_MVC.String.before(data("SortID"),":")
		
		if SortID = "" then
			'添加章节
			NewSortName = data("NewSortName")
			SortID = XM_( "md--Thread" ).index4sort + 1
			XM_( "md--Thread" ).index4sort = SortID
			Call addSort( NewSortName , SortID )
		else
			posID = POP_MVC.String.after(data("SortID"),":")
		end if
		
		data("SortID") = SortID
		data.remove("NewSortName")		
		
		if not XM_( "md--Thread" ).db.data(data).create(1) then
			that.error( XM_( "md--Thread" ).db.error )
		end if
		
		set data = XM_( "md--Thread" ).db.getData()
		
		for each key in data
			if data(key) = "" then
				data.remove(key)
			end if
		next
		
		P_("cookie").Assign "SelectSort" , SortID
		
		id = XM_( "md--Thread" ).db.data(data).prepend("Blog" , "TopicID")
		
		if  id - 0 > 0  then	
			'Call X_(tableName & "[TopicID=" & id & "]").setField( "ContentStatus" , 3 )
			if posID <> "" then
				XM_( "md--Thread" ).db.path("Blogs[@SortID=" & SortID & "]/Blog[TopicID=" & id &  "]").setPosition(Clng(posID))
			end if			
			
			XM_( "md--Thread" ).index4blog = XM_( "md--Thread" ).index4blog + 1
			response.write "<script>window.parent.document.getElementById('TopicID').value='" & id & "';if(parent.window.layui){ parent.window.layui.use('layer' ,function(){ parent.window.layui.layer.msg('草稿保存成功'); } )}else{alert('草稿保存成功');}</script>"
		else
			response.write "<script>if(parent.window.layui){ parent.window.layui.use('layer' ,function(){ parent.window.layui.layer.msg('草稿保存失败'); } )}else{alert('草稿保存失败');}</script>"
		end if
	end sub	
	
	'修改存草稿操作
	private sub edit_draft
		dim data,SortID,posID,id,NewSortName,key
		set data = POP_MVC.form2dict	
		
		data.key("test-editormd-markdown-doc") = "Content"
		data.key("test-editormd-html-code") = "StripContent"
		data("StripContent") =  delBlank(data("StripContent"))	
		SortID = POP_MVC.String.before(data("SortID"),":")
		
		if SortID = "" then
			'添加章节
			NewSortName = data("NewSortName")
			SortID = XM_( "md--Thread" ).index4sort + 1
			XM_( "md--Thread" ).index4sort = SortID
			Call addSort( NewSortName , SortID )
		else
			posID = POP_MVC.String.after(data("SortID"),":")
		end if
		
		data("SortID") = SortID
		data.remove("NewSortName")		
		
		if not XM_( "md--Thread" ).db.data(data).create(2) then
			that.error( XM_( "md--Thread" ).db.error )
		end if
		
		set data = XM_( "md--Thread" ).db.getData()
		
		for each key in data
			if data(key) = "" then
				data.remove(key)
			end if
		next
		
		id = data("TopicID")
		
		if  XM_( "md--Thread" ).db.path(tableName & "[TopicID=" & id & "]").MergeField(data,null) then
			'Call X_(tableName & "[TopicID=" & id & "]").setField( "ContentStatus" , 3 )
			
			
			response.write "<script>if(parent.window.layui){ parent.window.layui.use('layer' ,function(){ parent.window.layui.layer.msg('草稿保存成功'); } )}else{alert('草稿保存成功');}</script>"
		else
			response.write "<script>if(parent.window.layui){ parent.window.layui.use('layer' ,function(){ parent.window.layui.layer.msg('草稿保存失败'); } )}else{alert('草稿保存失败');}</script>"
		end if
	end sub
	
	'修改操作
	sub doEdit
		dim data,SortID,posID,id,NewSortName,key
		set data = POP_MVC.form2dict	
		
		data.key("test-editormd-markdown-doc") = "Content"
		data.key("test-editormd-html-code") = "StripContent"
		data("StripContent") =  delBlank(data("StripContent"))	
		
		id = data("TopicID")
		
		'表单中的SortID为 1:1( SortID:位置 )
		SortID = POP_MVC.String.before(data("SortID"),":")
		
		if SortID = "" then
			'添加章节
			NewSortName = data("NewSortName")
			SortID = XM_( "md--Thread" ).index4sort + 1
			XM_( "md--Thread" ).index4sort = SortID
			Call addSort( NewSortName , SortID )
		else
			posID = POP_MVC.String.after(data("SortID"),":")
		end if
		
		data("SortID") = SortID
		data.remove("NewSortName")
	
		if not XM_( "md--Thread" ).db.data(data).create(2) then
			that.error( XM_( "md--Thread" ).db.error )
		end if
	
		set data = XM_( "md--Thread" ).db.getData()
		
		for each key in data
			if data(key) = "" then
				data.remove(key)
			end if
		next
		
		if  XM_( "md--Thread" ).db.path(tableName & "[TopicID=" &id & "]").MergeField(data,null) then
			if posID <> "" then
				XM_( "md--Thread" ).db.path("Blogs/Blog[TopicID=" & id &  "]").setPosition(Clng(posID))
			end if	
		
			that.success( Array( "修改完成" , "?md--Admin_list_" & id  ))	
		else
			that.error( "修改失败，请重新提交" )
		end if
	end sub
	
	sub remove
		dim id
		id = that.get("id")
		Call XM_( "md--Thread" ).db.path(tableName & "[TopicID=" &id & "]").remove
		that.success( Array("删除成功" , "?md--Admin_list") )
	end sub
	
	private function delBlank(ByVal str)
		str = POP_MVC.String.reg_replace( str, " ", "^\s*(?=\r?$)\n" , "gm" )		
		str = POP_MVC.String.reg_replace( str, "", "[`~!@#$^\-&*()=|{}':;',\\\[\]\.<>\/?~！@#￥……&*（）——|{}【】'；：""'。，、？]" , "gm" )
		str = POP_MVC.String.reg_replace( str, " ", "\s+" , "gm" )
		delBlank = str
	End Function
	
	'添加章节
	private Sub addSort( byVal NewSortName , ByVal SortID )
		dim node,obj,pnode,infoNode
		set obj = XM_( "md--Thread" ).db.init
		
		'创建 Blogs 节点
		set node = obj.createNode( "Blogs" , "" )
		
		'创建 SortInfo 节点
		set infoNode = obj.createNode( "SortInfo" , "" )
		
		'创建属性
		Call obj.setNodeAttr( node, "SortID" , SortID )
		
		'<SortInfo><SortID>1</SortID><SortName>新章节</SortName></SortInfo>
		infoNode.appendChild( obj.createNode( "SortID" , SortID ) )
		infoNode.appendChild( obj.createNode( "SortName" , NewSortName ) )
		
		'<Blogs SortID=1><SortInfo></SortInfo></Blogs>
		node.appendChild( infoNode )		
		
		set pnode = obj.getNode(  "aspblog" , 0 )
		pnode.appendChild( node )		
		
		Call obj.SaveXml
	end Sub
end Class
%>