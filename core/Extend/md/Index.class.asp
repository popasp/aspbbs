<%
Class Index 
	private tableName,isWap,fileType
	sub initialize
		tableName = "Blogs/Blog"
		POP_MVC.Config("REQ_IGNORE_HTML") = "test-editormd-markdown-doc"
		POP_MVC.Config("REQ_IGNORE_HTML") = "test-editormd-markdown-doc"
		set bookConfig = XM_( "md--Thread" ).db.path("Configs/Config").field("ConfigName,ConfigValue").getObject
		that.d("bookConfig") = bookConfig
		isWap = checkWap()
	
		objCMS.cmsPrefix = "aspbbs"
		guestConfig.DefaultTemplate = "md"
		objCMS.htmlFilePath = "html"		
		
		fileType = that.get("type")	
		that.d("fileType") = fileType
		if fileType = "" then
			fileType = POP_MVC.config("MD_DEFAULT_TYPE")
		end if
	end sub
	
	'首页
	sub Index
		dim obj,templatePath,id,rs,sortRS,prevObj,nextObj
		'实例化POPASP_CMS类
		set obj = objCMS

		'模板名，这里要注意，模板文件名必须为 gbook.html
		templatePath= obj.getTemplatePath("Index/index")
		
		that.d("xmlfile") = XM_("md--Thread").db.db_path
		
		id = that.get("id")
		
		if id <> "" then
			set rs = XM_("md--Thread").db.path( tableName & "[TopicID=" & id & "]").find		
			if rs.count > 0 then				
				that.d("thread") = rs
				set sortRS = XM_("md--Thread").db.path( "Blogs[@SortID='" & rs("SortID") & "']/SortInfo").find
				that.d("sort") = sortRS
				
				fields = "TopicID,Title"
				
				'得到上一篇的rs
				
				set prevObj = XM_("md--Thread").db.path( "Blogs/Blog").field( fields ).prevRow( "TopicID" , id )

				set nextObj = XM_("md--Thread").db.path( "Blogs/Blog").field( fields ).nextRow( "TopicID" , id )
				that.d("prevObj") = prevObj
				that.d("nextObj") = nextObj
			end if
		end if
		that.d("theID") = id
		
		'模板文件不存在时
		if not POP_MVC.file.isFile(templatePath) then 
			Call obj.ShowError4Template( templatePath )
		end if	
		
		Call getZtree(id)
		
		that.d("SelectSort") = Request.Cookies( "SelectSort" )
		
		'加载
		obj.load(templatePath)	

		'解析模板
		obj.parseHtml

		'输出解析后的模板
		obj.Output
	end sub	
	
	
	'首页
	sub search
		dim obj,templatePath,id,rs,sortRS
		'实例化POPASP_CMS类
		set obj = objCMS

		'模板名，这里要注意，模板文件名必须为 gbook.html
		templatePath= obj.getTemplatePath("Index/search")
		
		that.d("xmlfile") = XM_("md--Thread").db.db_path
		
		id = that.get("id")
		
		if id <> "" then
			set rs = XM_("md--Thread").db.path( tableName & "[TopicID=" & id & "]").find		
			if rs.count > 0 then				
				that.d("thread") = rs
				set sortRS = XM_("md--Thread").db.path( "Blogs[@SortID='" & rs("SortID") & "']/SortInfo").find
				that.d("sort") = sortRS
			end if
		end if
		that.d("theID") = id
		
		'模板文件不存在时
		if not POP_MVC.file.isFile(templatePath) then 
			Call obj.ShowError4Template( templatePath )
		end if	
		
		Call getSoZtree
		
		that.d("SelectSort") = Request.Cookies( "SelectSort" )
		
		'加载
		obj.load(templatePath)	

		'解析模板
		obj.parseHtml

		'输出解析后的模板
		obj.Output
	end sub	
	
	'获取文章内容
	sub getContent
		dim id,rs

		id = that.get("id")
		set rs = XM_("md--Thread").db.path( tableName & "[TopicID=" & id & "]").find
		
		if rs.count = 0 then
			that.error("要修改的帖子不存在!")
		end if	
		
		response.write XM_("md--Thread").getContent(rs("Content"))
		response.end
	end sub
	
	sub getZtree(id)
		dim data,dict,rs,item,k,line,flag,filesize,filepath
		
		flag = false
		
		set data = XM_("md--Thread").db.path("Blogs/SortInfo").field("SortID,SortName").select
		
		for each key in data
			set item = data(key)
			item.key("SortName") = "name"
			item.key("SortID") = "id"
			item.remove("_NODE_INDEX_")
			item("pId") = 0
			item("isParent") = 1
			if id = "" then
				item("open") = 1
			end if
			set dict = XM_("md--Thread").db.path("Blogs[@SortID=" & item("id") & "]/Blog").field("Title,TopicID").select
			for each k in dict
				set line = dict(k)
				line.key("Title") = "name"
				line.key("TopicID") = "id"
				if line("id") = id then
					item("open") = 1	
					filepath = XM_("md--Thread").db.path("Blogs/Blog[TopicID=" & line("id") & "]").field("Content").getOne
					filesize =  POP_MVC.file.filesize(XM_("md--Thread").getTopicPath( filepath ))
					line("name") = line("name") & " (" & byte2size(filesize) & ")"
				end if
				line("url") = "?md--Index_" & fileType & "_" & line("id")
				line("target") = "_self"
			next
			if isWap then
				item("open") = 1
			end if
			item("name") = item("name") & " (" & dict.count & ")"
			item.add "children",dict
		next
		
		set dict = D_
		dict("id") = 0 
		dict("pId") = 0 
		dict("name") = "目录" 
		dict("open") =  true
		dict("target") = "main"
		
		POP_MVC.dict.unshift data,"",dict		
		
		that.d("zNodes") = data
	
		'var_export data
		'response.end
	end sub
	
	sub getSoZtree
		dim data,dict,rs,item,k,line,id,flag,where,keys,nums
		
		keys = POP_MVC.req("keys")
		nums = 0
		
		set where = D_
		
		where( "StripContent" ) = Array( "instr" , keys )
		
		set data = XM_("md--Thread").db.path("Blogs/SortInfo").field("SortID,SortName").select
		
		for each key in data
			set item = data(key)
			item.key("SortName") = "name"
			item.key("SortID") = "id"
			item.remove("_NODE_INDEX_")
			item("pId") = 0
			item("isParent") = 1
			item("open") = 1
			set dict = XM_("md--Thread").db.where(where).path("Blogs[@SortID=" & item("id") & "]/Blog").field("Title,TopicID").select
			if dict.count = 0 then
				data.remove(key)
			else
				for each k in dict
					set line = dict(k)
					line.key("Title") = "name"
					line.key("TopicID") = "id"
					line("url") = "?c=md--Index&a=search&type=" & fileType & "&id=" & line("id") & "&keys=" & keys
					line("target") = "_self"
				next
				nums = nums + dict.count
				item("name") = item("name") & " (" & dict.count &")"
			end if			
			item.add "children",dict
		next
		
		set dict = D_
		dict("id") = 0 
		dict("pId") = 0 
		dict("name") = "搜索 (" & nums & "): " & keys
		dict("open") =  true
		dict("target") = "main"
		
		POP_MVC.dict.unshift data,"",dict		
		
		
		'that.ajaxdata(data )
		that.d("zNodes") = data
		
		'var_export data
		'response.end
	end sub
end Class
%>