<%
'PATHINFO模式下的匹配规则中需包含下面三项。
'POP_MVC.config( "URL_RULE" ) = array( _
'	,Array( "^[a-z0-9]+--[a-z][a-z0-9]*$" , "c" , "a=index" ) _
'	,Array( "^[a-z0-9]+--[a-z][a-z0-9]*_[a-z][a-z0-9]*$" , "c_a" ) _
'	,Array( "^[a-z0-9]+--[a-z][a-z0-9]*_[a-z][a-z0-9]*_[1-9]\d*$" , "c_a_id" ) _
'

dim bookConfig

'默认文档
POP_MVC.config("MD_DEFAULT_TYPE") = 1

POP_MVC.config("CMS_UPLOAD_PATH") = POP_MVC.config("CMS_UPLOAD_NAME") & "/md"

'上传目录设置
if S_("adminId") = "" then	
	POP_MVC.config("UPLOAD_SAVE_PATH") = POP_MVC.config("CMS_UPLOAD_NAME") & "/md/" & POP_MVC.config("MD_DEFAULT_TYPE") & "/"
else
	POP_MVC.config("UPLOAD_SAVE_PATH") = POP_MVC.config("CMS_UPLOAD_NAME") & "/md/" & S_("adminId") & "/"
end if

%>