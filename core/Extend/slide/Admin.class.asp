<%
Class Admin 'Extends bbs--Common
	private orderStr,statusField,TimeDict,soID
	private tableName,sortTable,sqlfile,extendName
	private isWap,theStatus
	
	Sub initialize
		if S_("IsAdmin") <> 1 then
			that.error("无权操作!")
		end if
	
		tableName = "Blogs/Blog"
		sortTable = "Sorts/Sort"
		extendName = "Slide"		
		
		set bookConfig = XM_( "Slide--Thread" ).db.path("Configs/Config").field("ConfigName,ConfigValue").getObject
		that.d("bookConfig") = bookConfig
	
		objCMS.cmsPrefix = "aspbbs"
		guestConfig.DefaultTemplate = extendName
		objCMS.htmlFilePath = "html"
		isWap = checkWap()	
		POP_MVC.config( "AUTO_EXTENDS_MODULE" ) = "summernote"
	End Sub
	
	private sub getSortListData
		dim rsObj,where,pathWhere	

		set where = D_	

		'适用于搜索
		if POP_MVC.req("keys") <> "" then
			where( "SortName" ) = Array( "instr" , POP_MVC.req("keys") )
		end if
	
		set topicRS = XM_("Slide--Sort").db.path(sortTable).page(array( null, bookConfig.PageSize ) ).field("SortID,SortName,Alias,PageDesc").where(where).select

		that.d("recordCount") = XM_("Slide--Sort").db.curNodes.length
				
		that.d("thread") = topicRS	
		
		dim obj
		set obj = objCMS
		
		if LCase(POP_MVC.a) = "msearch" then
			if POP_MVC.req("keys") <> "" then
				templatePath= obj.getTemplatePath( "Admin/msearch" )
			else
				templatePath= obj.getTemplatePath( "Admin/sortList" )
			end if
		else
			templatePath= obj.getTemplatePath( "Admin/sortList" )
		end if

		obj.load(templatePath)	

		obj.parseHtml	
		
		obj.output
	end sub
	
	private sub getListData
		dim rsObj,where,pathWhere	

		set where = D_	

		'适用于搜索
		if POP_MVC.req("keys") <> "" then
			where( "Title" ) = Array( "instr" , POP_MVC.req("keys") )
		end if
		
		if soID <> "" then
			where( "Alias" ) = "sort" & soID
		end if
	
		set topicRS = XM_("Slide--Thread").db.path(tableName).page(array( null, bookConfig.PageSize ) ).field("Title,TopicID,Image,Alias").where(where).select

		that.d("recordCount") = XM_("Slide--Thread").db.curNodes.length
				
		that.d("thread") = topicRS	
		
		dim obj
		set obj = objCMS
		
		if LCase(POP_MVC.a) = "search" then
			if POP_MVC.req("keys") <> "" then
				templatePath= obj.getTemplatePath( "Admin/search" )
			else
				templatePath= obj.getTemplatePath( "Admin/Index" )
			end if
		else
			templatePath= obj.getTemplatePath( "Admin/Index" )
		end if

		obj.load(templatePath)	

		obj.parseHtml	
		
		obj.output
	end sub
	
	sub index
		Call getListData
	end sub
	
	sub sort
		soID = that.get("id")
		Call getListData
	end sub
	
	sub sortList
		Call getSortListData
	end sub
	
	
	'搜索页
	sub search
		Call getListData
	end sub
	
	'搜索页
	sub msearch
		Call getSortListData
	end sub
	
	private sub clearTable(ContentStatus)
		Call XM_("Slide--Thread").db.path(tableName & "[ContentStatus=" & ContentStatus & "]").remove
		that.success( "删除成功" )
	End Sub
	
	'彻底删除分组，不会删除幻灯片
	sub RemoveSort
		dim id,rs,dict,DelPath,recycleDir
		id = that.req("id")
		set rs = XM_("Slide--Sort").db.path( sortTable & "[SortID=" &id & "]").find
		
		if rs.count = 0 then
			that.error( "该" & bookConfig.ThreadTitle & "分组不存在！" )
		end if

		Call XM_("Slide--Sort").db.path( sortTable & "[SortID=" &id & "]").remove
		that.success( "成功将该" & bookConfig.ThreadTitle & "分组彻底删除" )
	end sub
	
	sub xml2index
		'新版本将去掉bbs组件中的幻灯片
		guestConfig.SlideUrl   = XM_("Slide--Thread").db.path( tableName & "[Alias='site']/Url" ).getArrStr(",")
		guestConfig.SlideTitle = XM_("Slide--Thread").db.path( tableName & "[Alias='site']/Title" ).getArrStr(",")
		guestConfig.SlideImage = XM_("Slide--Thread").db.path( tableName & "[Alias='site']/Image" ).getArrStr(",")
	
		Call B_( "self_GuestConfig" ).where( that.dict("ConfigName","SlideUrl") ).setField( "ConfigValue", guestConfig.SlideUrl )
		Call B_( "self_GuestConfig" ).where( that.dict("ConfigName","SlideTitle") ).setField( "ConfigValue", guestConfig.SlideTitle )			
		Call B_( "self_GuestConfig" ).where( that.dict("ConfigName","SlideImage") ).setField( "ConfigValue", guestConfig.SlideImage )
		objCMS.clearRuntime
		
	end sub
	
	'第2次删除，彻底删除
	sub remove2
		dim id,rs,dict,DelPath,recycleDir,arr,pos
		dim tempArr
		id = that.req("id")
		set rs = XM_("Slide--Thread").db.path(tableName & "[TopicID=" &id & "]").find
		
		if rs.count = 0 then
			that.error( "该" & bookConfig.ThreadTitle & "不存在！" )
		end if

		Call XM_("Slide--Thread").db.path(tableName & "[TopicID=" &id & "]").remove
		
		if rs("Alias") = "site" then
			Call xml2index
		end if
		
		that.success( "成功将该" & bookConfig.ThreadTitle & "彻底删除" )
	end sub
	
	'配置
	sub settings
		dim data,html
		'取得配置
		set data = XM_("Slide--Thread").db.path("Configs/Config").select	

		html = P_("flyui4xml").PageConfigContent(data)		
		that.d("htmlstr") = html
		
		objCMS.show( "Admin/set" )
	end sub	
	
	'js自动填充
	sub jsAuto
		dim dict
		set dict = XM_("Slide--Thread").db.path("Configs/Config").field("ConfigName,ConfigValue").getKeyValue
		
		Call K_("bbs--Tools").js_Auto( dict )
	end sub
	
	'修改配置值
	sub doEditConfigs
		dim key,dict,MailAlert
		set dict = POP_MVC.form2dict
		
		for each key in dict
			Call XM_("Slide--Thread").db.path("Configs/Config[ConfigName='"& key &"']").MergeField( "ConfigValue" , dict(key) )
		next

		Call K_( "bbs--Public" ).CtrlLog( 1, "用户ID: " & S_("adminId") & " ，修改了配置" , "保存成功" )
	end sub
	
	'上传图片
	sub uploads
		dim filename,logID
		POP_MVC.config("WATERMARK_PATH") = ""
		POP_MVC.config("UPLOAD_ALLOW_TYPES") = "jpg;jpeg;png;gif;bmp;pcx;svg"
		filename = POP_MVC.upload("file")
		if filename <> "" then
			that.success( filename )
		else
			that.error( POP_MVC.Uploader.description )
		end if
	end sub
	
	sub testASPJPEG
		if POP_MVC.IsInstall("Persits.Jpeg") then
			that.success( Array( "恭喜您，ASPJPEG组件已安装" ,"window.close",5 ) )
		else
			that.success( Array( "对不起，ASPJPEG组件未在服务器安装" ,"window.close",5 ) )
		end if
	end sub
	
	sub index2xml
		dim titles,urls,images,i,dict,id,sortRS,SortID,TopicID
		titles = guestConfig.SlideTitle		
		urls = guestConfig.SlideUrl		
		images = guestConfig.SlideImage	
		
		if images <> "" then
			urls = split(urls, ",")
			titles = split(titles, ",")
			images = split(images, ",")
			if ubound(images) <> ubound(titles) or ubound(urls) <> ubound(images) then
				that.error( bookConfig.ThreadTitle & " 数组个数不一致！" )
			end if
			
			'首页取得首页幻灯片分组，如果没有则添加，首页幻灯片分组的标记是Alias='site'
			set sortRS = XM_("Slide--Sort").db.path( sortTable & "[Alias='site']" ).find
			
			if sortRS.count = 0 then
				set dict = D_
				dict("Alias") = "site"
				dict("SortName") = "首页幻灯片"
				Call XM_( "Slide--Sort" ).db.data(dict).create(1)
				set dict = XM_( "Slide--Sort" ).db.getData()
				
				'添加分组
				id = XM_( "Slide--Sort" ).db.path("Sorts").data(dict).append("Sort" , "SortID")
				
				if  id > 0 then	
					SortID = id
					id = XM_( "Slide--Sort" ).index4sort + 1
					XM_( "Slide--Sort" ).index4sort = id					
				else
					that.error( "网站幻灯片分组添加失败！" )
				end if
			else
				SortID = sortRS("SortID")
			end if
			
			Call XM_("Slide--Thread").db.path( tableName & "[Alias='site']" ).remove
			
			TopicID = XM_( "Slide--Thread" ).index4blog
			
			for i = ubound(urls) to lbound(urls) step -1
				set dict = D_
				dict("Title") = titles(i)
				dict("Url")  = urls(i)
				dict("Image") = images(i)
				dict("Alias") = "site"
				dict("SortID") = SortID				
				Call XM_("Slide--Thread").db.data(dict).create(1)
				set dict = XM_("Slide--Thread").db.getData()
				
				dict("TopicID") = TopicID
				id = XM_("Slide--Thread").db.path("Blogs").data(dict).append("Blog" , "TopicID")
				
				if  id > 0 then	
					TopicID = TopicID + 1
				else
					that.error( "首页幻灯片添加失败！" )
				end if				
				
				if dict.exists("_NODE_INDEX_") then
					dict.remove("_NODE_INDEX_")
				end if	
			next
			XM_( "Slide--Thread" ).index4blog = TopicID	
			that.success( "导入成功" )
		else
			that.success( "未发现可导入的幻灯片" )
		end if	
		
	end sub	
	
	sub addSort
		objCMS.show("Admin/addSort")
	end sub
	
	'修改页面
	sub editSort
		dim id,rs,topicRS

		id = that.get("id")
		set rs = XM_("Slide--Sort").db.path( sortTable & "[SortID=" & id & "]").find
		
		if rs.count = 0 then
			that.error("要修改的分组不存在!")
		end if
		
		that.d("thread") = rs
		
		set topicRS= XM_("Slide--Sort").db.path( sortTable ).top(1000).field("SortID,SortName").select
		that.d("topicRS") = topicRS
		
		that.d("xmlfile") = XM_("Slide--Thread").db.db_path
		'that.d("summernote") = P_("auto").summernote("#L_content")
		objCMS.show("Admin/editSort")
	end sub	
	
	'添加操作
	sub doAddSort
		dim data,id,key
		set data = POP_MVC.form2dict
		
		if not XM_( "Slide--Sort" ).db.data(data).create(1) then
			that.error( XM_( "Slide--Sort" ).db.error )
		end if
		
		set data = XM_( "Slide--Sort" ).db.getData()
		
		for each key in data
			if data(key) = "" then
				data.remove(key)
			end if
		next
		
		if data.Exists("Verify") then		
			data.remove("Verify")
		end if
		
		id = XM_( "Slide--Sort" ).db.path("Sorts").data(data).append("Sort" , "SortID")
		

		if  id > 0 then			
			id = XM_( "Slide--Sort" ).index4sort + 1
			XM_( "Slide--Sort" ).index4sort = id
			that.success( Array( bookConfig.ThreadTitle & "分组添加成功" , "?Slide--Admin_AddSort") )
		else
			that.error( "添加失败，请重新提交" )
		end if
	end sub
	
	'修改操作
	sub doEditSort
		dim data,posID,id,NewSortName,key
		set data = POP_MVC.form2dict	
		
		id = data("SortID")
		'posID = data("PosID")		

		'data.remove("PosID")		
	
		if not XM_( "Slide--Sort" ).db.data(data).create(2) then
			that.error( XM_( "Slide--Sort" ).db.error )
		end if
		
		set data = XM_( "Slide--Sort" ).db.getData()	
	
		for each key in data
			if data(key) = "" then
				data.remove(key)
			end if
		next
		
		if  XM_( "Slide--Sort" ).db.path( sortTable & "[SortID=" & id & "]").MergeField(data,null) then
			'if posID <> "" then
			'	XM_( "Slide--Sort" ).db.path("Blogs/Blog[SortID=" & id &  "]").setRPosition(Clng(posID))
			'else
			'	XM_( "Slide--Sort" ).db.path("Blogs/Blog[SortID=" & id &  "]").setRPosition(1000)
			'end if		
			that.success( Array(  bookConfig.ThreadTitle & "分组修改完成" , "?Slide--Admin_SortList" ))	
		else
			that.error( "修改失败，请重新提交" )
		end if
	end sub	
	
	'添加页面
	sub add
		Dim AddSortID
		AddSortID = that.get("id")
		if AddSortID <> "0" and AddSortID <> "" then
			lastSortID = AddSortID
		else
			lastSortID = XM_("Slide--Thread").db.path( tableName ).field("SortID").getOne
		end if	
		that.d("LastSortID") = lastSortID
		
		that.d("sortRS") = XM_("Slide--Sort").db.path( sortTable ).select
		objCMS.show("Admin/add")
	end sub

	
	'修改页面
	sub edit
		dim id,rs
		dim topicRS,i,key

		id = that.get("id")
		set rs = XM_("Slide--Thread").db.path( tableName & "[TopicID=" & id & "]").find
		
		if rs.count = 0 then
			that.error("要修改的" & bookConfig.ThreadTitle & "不存在!")
		end if
		
		that.d("thread") = rs
		
		set topicRS= XM_("Slide--Thread").db.path(tableName).top(1000).field("Title,TopicID,Alias").select
		
		
		i = 1
		for each key in topicRS			
			if topicRS(key)("Alias") <> rs("Alias") then
				topicRS.remove(key)
			else
				topicRS(key)("pos") = i
			end if					
			i = i + 1
		next

		that.d("topicRS") = topicRS
		
		that.d("xmlfile") = XM_("Slide--Thread").db.db_path
		objCMS.show("Admin/edit")
	end sub	
	
	'添加操作
	sub doAdd
		dim data,posID,sortRS,key
		set data = POP_MVC.form2dict	
		
		
		posID = data("PosID")		

		data.remove("PosID")		
	
		if not XM_( "Slide--Thread" ).db.data(data).create(1) then
			that.error( XM_( "Slide--Thread" ).db.error )
		end if
		
		set data = XM_( "Slide--Thread" ).db.getData()
		
		set sortRS = XM_( "Slide--Sort" ).db.path( sortTable & "[Alias='" & data("Alias") & "']").find
		
		data("SortID") = sortRS("SortID")
	
		for each key in data
			if data(key) = "" then
				data.remove(key)
			end if
		next
		
		id = XM_( "Slide--Thread" ).db.path( "Blogs" ).data(data).prepend("Blog" , "TopicID")
		
		if  id > 0 then
			id = XM_( "Slide--Thread" ).index4blog + 1
			XM_( "Slide--Thread" ).index4blog = id
			
			if data("Alias") = "site" then
				Call xml2index
			end if
			
			that.success( Array( bookConfig.ThreadTitle & "添加成功" , "?Slide--Admin_Add") )
		else
			that.error( "修改失败，请重新提交" )
		end if
	end sub

	
	'修改操作
	sub doEdit
		dim data,posID,id,NewSortName,key
		set data = POP_MVC.form2dict	
		
		id = data("TopicID")
		posID = data("PosID")		

		data.remove("PosID")		
	
		if not XM_( "Slide--Thread" ).db.data(data).create(2) then
			that.error( XM_( "Slide--Thread" ).db.error )
		end if
		
		set data = XM_( "Slide--Thread" ).db.getData()	
	
		for each key in data
			if data(key) = "" then
				data.remove(key)
			end if
		next
		
		if  XM_( "Slide--Thread" ).db.path(tableName & "[TopicID=" & id & "]").MergeField(data,null) then
			if posID <> "" then
				XM_( "Slide--Thread" ).db.path("Blogs/Blog[TopicID=" & id &  "]").setRPosition(Clng(posID))
			else
				XM_( "Slide--Thread" ).db.path("Blogs/Blog[TopicID=" & id &  "]").setRPosition(1000)
			end if	

			if data("Alias") = "site" then
				Call xml2index
			end if			
			that.success( Array( "修改完成" , "?Slide--Admin" ))	
		else
			that.error( "修改失败，请重新提交" )
		end if
	end sub
End Class
%>