<%
Class Index 
	private tableName
	sub initialize
		tableName = "Blogs/Blog"
		set bookConfig = XM_( "Slide--Thread" ).db.path("Configs/Config").field("ConfigName,ConfigValue").getObject
		that.d("bookConfig") = bookConfig
		
		objCMS.cmsPrefix = "aspbbs"
		guestConfig.DefaultTemplate = "Slide"
		objCMS.htmlFilePath = "html"	
	end sub
	
	private sub getListData
		dim rsObj,where,pathWhere

		set where = D_

		'适用于搜索
		if POP_MVC.req("keys") <> "" then
			where( "Title" ) = Array( "instr" , POP_MVC.req("keys") )
		end if
	
		set topicRS = XM_("Slide--Thread").db.path(tableName).page(array( null, bookConfig.PageSize ) ).field("Title,TopicID,Image,Alias").where(where).select


		that.d("recordCount") = XM_("Slide--Thread").db.curNodes.length
				
		that.d("thread") = topicRS	
		
		dim obj
		set obj = objCMS
		
		if LCase(POP_MVC.a) = "search" then
			if POP_MVC.req("keys") <> "" then
				templatePath= obj.getTemplatePath( "Index/search" )
			else
				templatePath= obj.getTemplatePath( "Index/index" )
			end if
		else
			templatePath= obj.getTemplatePath( "Index/index" )
		end if

		obj.load(templatePath)	

		obj.parseHtml	
		
		obj.output
	end sub
	
	sub index
		dim slidedata
		Call getListData
	end sub
	
	
	'搜索页
	sub search
		Call getListData
	end sub	
end Class
%>