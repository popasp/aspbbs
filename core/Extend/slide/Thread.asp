<%
Class Thread
	Private index4blog_ 
	sub init
		'db是popasp_xmldb类的实例化
		path = "Blogs"
		db.db_path = "./Templates/Slide/#data/#book#.xml"
		
		db.auto_ = Array( _
			Array( "ContentStatus" ,1,  1 ) _
			,Array( "TopicID" , bookConfig.index4blog ,  1 , "function") _
			,Array( "AddTime" ,"POP_MVC.FormatDate( now() , ""YYYY-MM-DD HH:II:SS"" )",  1 , "function" ) _
			,Array( "EditTime" ,"POP_MVC.FormatDate( now() , ""YYYY-MM-DD HH:II:SS"" )",  2 , "function" ) _
			,Array( "IP" , "get_client_ip" ,  1, "function" ) _
		)
		
		'数据验证
		db.validate_ = Array( _
			 Array( "Title" ,"",  "标题不能为空"  , 0, "notempty" , 3  ) _
			,Array( "Alias" ,"",  "分组不能为空"  , 0, "notempty" , 3  ) _
			,Array( "Title","1,255","标题长度不能超过255个字符" , 0 , "length" , 3 ) _
			,Array( "Image" ,"",  "图片未上传"  , 0, "notempty" , 3  ) _
		)
	end sub
	
	Function incBlogID
		incBlogID = index4blog + 1
	End Function

	
	Public Property Get index4blog()
		if index4blog_ = "" then
			index4blog_ = bookConfig.index4blog
		end if
		index4blog = index4blog_	
	End Property
	
	Public Property Let index4blog( value )
		index4blog_ = value
		Call setSiteConfig( "index4blog" , index4blog_ )
	End Property

	
	Sub setSiteConfig( ConfigName, ConfigValue )
		Call db.path( "Configs/Config[ConfigName='" & ConfigName & "']"  ).setField( "ConfigValue" , ConfigValue )
	End Sub
End Class
%>