<%
Class Admin 'Extends bbs--Common
	private orderStr,statusField,TimeDict
	private tableName,sqlfile,extendName,groupTable
	private isWap,theStatus
	
	Sub initialize
		if S_("IsAdmin") <> 1 then
			that.error("无权操作!")
		end if
	
		tableName = "Blogs/Blog"
		groupTable = "Groups/Group"
		extendName = "Sort"		
		
		set bookConfig = XM_( "Sort--Thread" ).db.path("Configs/Config").field("ConfigName,ConfigValue").getObject
		that.d("bookConfig") = bookConfig
	
		objCMS.cmsPrefix = "aspbbs"
		guestConfig.DefaultTemplate = extendName
		objCMS.htmlFilePath = "html"
		isWap = checkWap()	
		POP_MVC.config( "AUTO_EXTENDS_MODULE" ) = "summernote"
	End Sub
	
	private sub getListData
		dim rsObj,where,pathWhere	

		set where = D_	

		'适用于搜索
		if POP_MVC.req("keys") <> "" then
			where( "SortName" ) = Array( "instr" , POP_MVC.req("keys") )
		else
			where("SortType") = theStatus
		end if
	
		set topicRS = XM_("Sort--Thread").db.path(tableName).page(array( null, bookConfig.PageSize ) ).field("SortID,SortName,Content,PageDesc,Keywords,IndexImage,Alias").where(where).select

		that.d("recordCount") = XM_("Sort--Thread").db.curNodes.length
				
		that.d("thread") = topicRS	
		
		dim obj
		set obj = objCMS
		
		if LCase(POP_MVC.a) = "search" then
			if POP_MVC.req("keys") <> "" then
				templatePath= obj.getTemplatePath( "Admin/search" )
			else
				templatePath= obj.getTemplatePath( "Admin/Index" )
			end if
		else
			templatePath= obj.getTemplatePath( "Admin/Index" )
		end if

		obj.load(templatePath)	

		obj.parseHtml	
		
		obj.output
	end sub
	
	private sub getSortListData
		dim rsObj,where,pathWhere	

		set where = D_	

		'适用于搜索
		if POP_MVC.req("keys") <> "" then
			where( "GroupName" ) = Array( "instr" , POP_MVC.req("keys") )
		end if
	
		set topicRS = XM_("Sort--Sort").db.path(groupTable).page(array( null, bookConfig.PageSize ) ).field("GroupID,GroupName,Alias,PageDesc").where(where).select

		that.d("recordCount") = XM_("Sort--Sort").db.curNodes.length
				
		that.d("thread") = topicRS	
		
		dim obj
		set obj = objCMS
		
		if LCase(POP_MVC.a) = "msearch" then
			if POP_MVC.req("keys") <> "" then
				templatePath= obj.getTemplatePath( "Admin/msearch" )
			else
				templatePath= obj.getTemplatePath( "Admin/sortList" )
			end if
		else
			templatePath= obj.getTemplatePath( "Admin/sortList" )
		end if

		obj.load(templatePath)	

		obj.parseHtml	
		
		obj.output
	end sub
	
	sub index
		theStatus = "User"
		Call getListData
	end sub
	
	sub sortList
		Call getSortListData
	end sub
	
	
	sub admin
		theStatus = "Admin"
		Call getListData
	end sub
	
	sub hidden
		theStatus = "Hide"
		Call getListData
	end sub
	
	
	'搜索页
	sub search
		Call getListData
	end sub
	
	'搜索页
	sub msearch
		Call getSortListData
	end sub
	
	'彻底删除分组，不会删除幻灯片
	sub RemoveSort
		dim id,rs,dict,DelPath,recycleDir
		id = that.req("id")
		set rs = XM_("Sort--Sort").db.path( groupTable & "[GroupID=" &id & "]").find
		
		if rs.count = 0 then
			that.error( "该" & bookConfig.ThreadTitle & "分组不存在！" )
		end if

		Call XM_("Sort--Sort").db.path( groupTable & "[GroupID=" &id & "]").remove
		that.success( "成功将该" & bookConfig.ThreadTitle & "分组彻底删除" )
	end sub
	
	'添加页面
	sub add
		dim arr,lastSortType,AddGroupID,TopicRS
		
		AddGroupID = that.get("id")
		if AddGroupID <> "0" and AddGroupID <> "" then
			AddGroupID = AddGroupID
		else
			AddGroupID = XM_("Sort--Sort").db.path( groupTable ).field("GroupID").getOne
		end if	

		that.d("LastGroupID") = AddGroupID		
		
		arr =  XM_("Sort--Thread").db.path( tableName & "/SortID" ).getArr
		Call POP_MVC.arr.natsort( arr )
		
		
		lastSortType =  XM_("Sort--Thread").db.path( tableName & "/SortType" ).getArr
		lastSortType = POP_MVC.Arr.pop( lastSortType )
		
		set topicRS= XM_("Sort--Thread").db.path(tableName ).top(1000).field("SortID,SortName,SortType,Alias").select
		
		for each key in topicRS
			if topicRS(key)("Alias") = "" then
				topicRS(key)("Alias") = ""
			end if
		next
		
		that.d("topicRS") = topicRS
		that.d("lastSortType") = lastSortType
		that.d("curID") = join( arr,"," )
		that.d("sortRS") = XM_("Sort--Sort").db.path( groupTable ).select
		objCMS.show("Admin/add")
	end sub
	
	
	'修改页面
	sub edit
		dim id,rs
		dim topicRS,i,key,groupRS

		id = that.get("id")
		set rs = XM_("Sort--Thread").db.path( tableName & "[SortID=" & id & "]").find
		
		if rs.count = 0 then
			that.error("要修改的" & bookConfig.ThreadTitle & "不存在!")
		end if		
		
		set topicRS= XM_("Sort--Thread").db.path(tableName ).top(1000).field("SortID,SortName,SortType,Alias").select
		set groupRS= XM_("Sort--Sort").db.path(groupTable ).top(1000).field("GroupID,GroupName,Alias").select

		i = 1
		for each key in topicRS
			if topicRS(key)("SortType") <> rs("SortType") then
				topicRS.remove(key)
			else
				topicRS(key)("pos") = i
			end if	
			i = i + 1
		next
		
		that.d("thread") = rs
		that.d("topicRS") = topicRS
		that.d("groupRS") = groupRS
		objCMS.show("Admin/edit")
	end sub	
	
	'添加操作
	sub doAdd
		dim data,key,rs,SortID
		set data = POP_MVC.form2dict
	
		if not XM_( "Sort--Thread" ).db.data(data).create(1) then
			that.error( XM_( "Sort--Thread" ).db.error )
		end if
		
		set data = XM_( "Sort--Thread" ).db.getData()
		
		if data("SortID") <> "" then
			if not that.IsID( data("SortID") ) then
				that.error( bookConfig.ThreadTitle & " ID 必须为正整数")
			end if
			
			set rs = XM_( "Sort--Thread" ).db.path( tableName & "[SortID=" & data("SortID") & "]" ).find
			if rs.count <> 0 then
				that.error( bookConfig.ThreadTitle & " ID 不能重复(与栏目【" & rs("SortName") & ":" & rs("SortID") & "】相同)")
			end if
		end if
		
		for each key in data
			if data(key) = "" then
				data.remove(key)
			end if
		next
		
		SortID = XM_( "Sort--Thread" ).db.path( "Blogs" ).data(data).append("Blog" , "SortID")
		
		if  SortID > 0 then
			Call xml2sort( data("SortType") )
			that.success( Array( bookConfig.ThreadTitle & "添加成功" , "?Sort--Admin_Add") )
		else
			that.error( "修改失败，请重新提交" )
		end if
	end sub
	
	'修改操作
	sub doEdit
		dim data,posID,id,NewSortName,key
		set data = POP_MVC.form2dict	
		
		id = data("SortID")
		
		'取位置
		posID = data("PosID")
		data.remove("PosID")		
	
		'数据的自动完成与验证
		if not XM_( "Sort--Thread" ).db.data(data).create(2) then
			that.error( XM_( "Sort--Thread" ).db.error )
		end if
		
		'取得数据
		set data = XM_( "Sort--Thread" ).db.getData()
		
		'栏目中含有GroupID、Alias
		data("Alias") = XM_( "Sort--Sort" ).db.path( groupTable & "[GroupID=" & data("GroupID") & "]" ).field("Alias").getOne
	
		for each key in data
			if data(key) = "" then
				data.remove(key)
			end if
		next
		
		if  XM_( "Sort--Thread" ).db.path(tableName & "[SortID=" & id & "]").MergeField(data,null) then
			if posID <> "" then
				XM_( "Sort--Thread" ).db.path( tableName & "[SortID=" & id &  "]").setRPosition(Clng(posID))
			else
				XM_( "Sort--Thread" ).db.path( tableName & "[SortID=" & id &  "]").setRPosition(1000)
			end if	
		
			Call xml2sort( data("SortType") ) 
			that.success( Array( "修改完成" , "?Sort--Admin" & iif( data("SortType") = "Admin" , "_Admin" , "" ) ))	
		else
			that.error( "修改失败，请重新提交" )
		end if
	end sub
	
	sub addSort
		objCMS.show("Admin/addSort")
	end sub
	
	'修改页面
	sub editSort
		dim id,rs,topicRS

		id = that.get("id")
		set rs = XM_("Sort--Sort").db.path( groupTable & "[GroupID=" & id & "]").find
		
		if rs.count = 0 then
			that.error("要修改的分组不存在!")
		end if
		
		that.d("thread") = rs
		
		set topicRS= XM_("Sort--Sort").db.path( groupTable ).top(1000).field("GroupID,GroupName").select
		that.d("topicRS") = topicRS
		
		that.d("xmlfile") = XM_("Sort--Thread").db.db_path
		'that.d("summernote") = P_("auto").summernote("#L_content")
		objCMS.show("Admin/editSort")
	end sub	
	
	'添加操作
	sub doAddSort
		dim data,id,key
		set data = POP_MVC.form2dict
		
		if not XM_( "Sort--Sort" ).db.data(data).create(1) then
			that.error( XM_( "Sort--Sort" ).db.error )
		end if
	
		set data = XM_( "Sort--Sort" ).db.getData()
		
		for each key in data
			if data(key) = "" then
				data.remove(key)
			end if
		next
		
		if data.Exists("Verify") then		
			data.remove("Verify")
		end if
		
		id = XM_( "Sort--Sort" ).db.path("Groups").data(data).append("Group" , "GroupID")

		if  id > 0 then			
			id = XM_( "Sort--Sort" ).index4sort + 1
			XM_( "Sort--Sort" ).index4sort = id
			that.success( Array( bookConfig.ThreadTitle & "分组添加成功" , "?Sort--Admin_AddSort") )
		else
			that.error( "添加失败，请重新提交" )
		end if
	end sub
	
	'修改操作
	sub doEditSort
		dim data,posID,id,NewSortName,key
		set data = POP_MVC.form2dict	
		
		id = data("GroupID")
		posID = data("PosID")		

		data.remove("PosID")		
	
		if not XM_( "Sort--Sort" ).db.data(data).create(2) then
			that.error( XM_( "Sort--Sort" ).db.error )
		end if
		
		set data = XM_( "Sort--Sort" ).db.getData()	
	
		for each key in data
			if data(key) = "" then
				data.remove(key)
			end if
		next
		
		if  XM_( "Sort--Sort" ).db.path( groupTable & "[GroupID=" & id & "]").MergeField(data,null) then
			if posID <> "" then
				XM_( "Sort--Sort" ).db.path("Blogs/Blog[GroupID=" & id &  "]").setRPosition(Clng(posID))
			else
				XM_( "Sort--Sort" ).db.path("Blogs/Blog[GroupID=" & id &  "]").setRPosition(1000)
			end if		
			that.success( Array( "修改完成" , "?Sort--Admin_SortList" ))	
		else
			that.error( "修改失败，请重新提交" )
		end if
	end sub	
	
	private Sub setContentStatus( id , ContentStatus )
		dim rs
		if not that.IsID( id ) then
			that.error( "非法操作" )
		end if
		
		set rs = XM_("Sort--Thread").db.path(tableName & "[SortID=" &id & "]").find
		
		if rs.count = 0 then
			that.error( "该" & bookConfig.ThreadTitle & "不存在！" )
		end if
		Call XM_("Sort--Thread").db.path(tableName & "[SortID=" &id & "]").setField( "ContentStatus" , ContentStatus)
	End Sub

	'采纳
	sub accept
		Call setContentStatus( that.req("id") , 1 )
		that.success( Array("采纳" , -1) )
	end sub	
	
	'未审
	sub uncheck
		Call setContentStatus( that.req("id") , 0 )
		that.success( Array("未审" , -1) )
	end sub	
	
	'拒绝
	sub refuse
		Call setContentStatus( that.req("id") , 3 )
		that.success( Array("拒绝" , -1) )
	end sub	
	
	'第一次软删除del
	sub remove
		Call setContentStatus( that.req("id") , 2 )
		that.success( Array("删除" , -1) )
	end sub
	
	private sub clearTable(ContentStatus)
		Call XM_("Sort--Thread").db.path(tableName & "[ContentStatus=" & ContentStatus & "]").remove
		that.success( "删除成功" )
	End Sub
	
	'清空未审核
	Sub clearUnchecked
		Call clearTable(0)
	end sub
	
	'清空审核未过
	Sub clearFailed
		Call clearTable(3)
	end sub
	
	'清空回收站
	Sub clearRecycle
		Call clearTable(2)
	end sub
	
	'第2次删除，彻底删除
	sub remove2
		dim id,rs,dict,DelPath,recycleDir,where,cnt
		id = that.req("id")
		

		set rs = XM_("Sort--Thread").db.path(tableName & "[SortID=" &id & "]").find

		if rs.count = 0 then
			that.error( "该" & bookConfig.ThreadTitle & "不存在！" )
		end if
		
		set where = D_
		
		where("SortID") = id
		where("ContentStatus") = 1
		
		cnt = B_("self_GuestTopic").where( where ).count
		
		if cnt > 0 then
			that.error( "该" & bookConfig.ThreadTitle & "下有" & cnt & "篇正常显示的" & guestConfig.ThreadName & "，不能删除！" )
		end if

		Call XM_("Sort--Thread").db.path(tableName & "[SortID=" & id & "]").remove
		Call B_("self_GuestTopic").where( that.dict("SortID" , id )  ).remove
		that.success( "成功将该" & bookConfig.ThreadTitle & "彻底删除" )
	end sub
	

	'未导入数据库上传文件检测
	sub dbTest
		dim action,time1,time2,filepath,arr,flag
		
		Call K_("Sort--Tools").dbTest
		
		response.end

		filepath = POP_MVC.appPath & "/runtime/UploadLog/table.txt"
		'如果已经保存了检测，且未再上传，从文件中显示
		if POP_MVC.file.isfile(filepath) then
			time1 = POP_MVC.file.mtime( POP_MVC.appPath & "/runtime/UploadLog/table.txt" )
			time2 = POP_MVC.file.mtime( POP_MVC.config("CMS_UPLOAD_PATH") )
			if  DateDiff("s", time2, time1) < 0 then
				flag = true
			else
				response.write K_("bbs--FileTool").file_get_test("UploadLog")
			end if
		end if
		
		if flag then
			Call K_("Sort--Tools").dbTest
		end if
	end sub
	
	'配置
	sub settings
		dim data,html
		'取得配置
		set data = XM_("Sort--Thread").db.path("Configs/Config").select	

		html = P_("flyui4xml").PageConfigContent(data)		
		that.d("htmlstr") = html
		
		objCMS.show( "Admin/set" )
	end sub	
	
	'修改配置值
	sub doEditConfigs
		dim key,dict,MailAlert
		set dict = POP_MVC.form2dict
		
		for each key in dict
			Call XM_("Sort--Thread").db.path("Configs/Config[ConfigName='"& key &"']").MergeField( "ConfigValue" , dict(key) )
		next

		Call K_( "bbs--Public" ).CtrlLog( 1, "用户ID: " & S_("adminId") & " ，修改了配置" , "保存成功" )
	end sub
	
	'上传图片
	sub uploads
		dim filename,logID
		POP_MVC.config("WATERMARK_PATH") = ""
		POP_MVC.config("UPLOAD_ALLOW_TYPES") = "jpg;jpeg;png;gif;bmp;pcx;svg"
		filename = POP_MVC.upload("file")
		if filename <> "" then
			that.success( filename )
		else
			that.error( POP_MVC.Uploader.description )
		end if
	end sub
	
	sub testASPJPEG
		if POP_MVC.IsInstall("Persits.Jpeg") then
			that.success( Array( "恭喜您，ASPJPEG组件已安装" ,"window.close",5 ) )
		else
			that.success( Array( "对不起，ASPJPEG组件未在服务器安装" ,"window.close",5 ) )
		end if
	end sub
	
	sub SortUser2xml
		Call sort2xml( "User" )
	end sub
	
	sub SortAdmin2xml
		Call sort2xml( "Admin" )
	end sub
	
	sub SortHide2xml
		Call sort2xml( "Hide" )
	end sub
	
	sub xml2sort( stype )	
		if stype = "User" then
			guestConfig.SortUserName = XM_("Sort--Thread").db.path( tableName & "[SortType='" & stype & "']/SortName" ).getArrStr(",")
			guestConfig.SortUserID   = XM_("Sort--Thread").db.path( tableName & "[SortType='" & stype & "']/SortID" ).getArrStr(",")
			Call B_( "self_GuestConfig" ).where( that.dict("ConfigName","SortUserName") ).setField( "ConfigValue", guestConfig.SortUserName )
			Call B_( "self_GuestConfig" ).where( that.dict("ConfigName","SortUserID") ).setField( "ConfigValue", guestConfig.SortUserID )
		elseif stype = "Admin" then
			guestConfig.SortAdminName = XM_("Sort--Thread").db.path( tableName & "[SortType='" & stype & "']/SortName" ).getArrStr(",")
			guestConfig.SortAdminID   = XM_("Sort--Thread").db.path( tableName & "[SortType='" & stype & "']/SortID" ).getArrStr(",")
			Call B_( "self_GuestConfig" ).where( that.dict("ConfigName","SortAdminName") ).setField( "ConfigValue", guestConfig.SortAdminName )
			Call B_( "self_GuestConfig" ).where( that.dict("ConfigName","SortAdminID") ).setField( "ConfigValue", guestConfig.SortAdminID )
		elseif stype = "Hide" then
			guestConfig.SortHideName = XM_("Sort--Thread").db.path( tableName & "[SortType='" & stype & "']/SortName" ).getArrStr(",")
			guestConfig.SortHideID   = XM_("Sort--Thread").db.path( tableName & "[SortType='" & stype & "']/SortID" ).getArrStr(",")
			Call B_( "self_GuestConfig" ).where( that.dict("ConfigName","SortHideName") ).setField( "ConfigValue", guestConfig.SortHideName )
			Call B_( "self_GuestConfig" ).where( that.dict("ConfigName","SortHideID") ).setField( "ConfigValue", guestConfig.SortHideID )
		end if
		
		objCMS.clearRuntime
	end sub
	
	Private sub sort2xml( stype )
		dim ids,names,i,dict,id,name,rs,arr
		Execute "ids = guestConfig.Sort" & stype & "ID"		
		
		if ids <> "" then
			ids = split( ids, "," ) 
			
			Execute "names = guestConfig.Sort" & stype & "Name"
			names = split( names, "," ) 
			
			if ubound(ids) <> ubound(names) then
				that.error( bookConfig.ThreadTitle & " ID 与 名称 个数不一致！" )
			end if
			
			'Call XM_( "Sort--Thread" ).db.path( tableName & "[SortType='" & stype & "']").remove
			
			for i = ubound(ids) to lbound(ids) step -1
				set dict = D_
				id = ids(i)
				dict("SortID") = id
				dict("SortName") = names(i)
				dict("SortType") = stype
				if XM_("Sort--Thread").db.path(tableName & "[SortID=" & id & "]").find.count = 0 then
					Call XM_("Sort--Thread").db.path("Blogs").data(dict).prepend("Blog" , "SortID")
				else
					Call XM_("Sort--Thread").db.path(tableName & "[SortID=" & id & "]").MergeField(dict,null)
				end if				
			next
		end if	
		that.success( "导入成功" )
	end sub	
	
	'js自动填充
	sub jsAuto
		dim dict
		set dict = XM_("Sort--Thread").db.path("Configs/Config").field("ConfigName,ConfigValue").getKeyValue
		
		Call K_("bbs--Tools").js_Auto( dict )
	end sub
End Class
%>