<%
Class User 
	private tableName,isWap,theStatus,orderStr
	sub initialize
		tableName = "Blogs/Blog"
		set bookConfig = XM_( "Sort--Thread" ).db.path("Configs/Config").field("ConfigName,ConfigValue").getObject
		bookConfig.SystemAdmin = split( bookConfig.SystemAdmin , "," )
		that.d("bookConfig") = bookConfig
		isWap = checkWap()

		objCMS.cmsPrefix = "aspbbs"
		guestConfig.DefaultTemplate = "Sort"
		objCMS.htmlFilePath = "html"
		POP_MVC.config( "AUTO_EXTENDS_MODULE" ) = "summernote"
	end sub
	
	private sub getListData
		dim rsObj,where,pathWhere
		
		pathWhere = "UserID=" & session("adminId")

		set where = D_		

		'适用于搜索
		if POP_MVC.req("keys") <> "" then
			where( "Title" ) = Array( "instr" , POP_MVC.req("keys") )
		else
			where("ContentStatus") = theStatus
		end if
	
		set topicRS = XM_("Sort--Thread").db.path(tableName & "[" & pathWhere & "]").page(array( null, bookConfig.PageSize ) ).field("TopicID,UserID,AddTime,Title,UserID,Visits,Avatar,LoginName,StripContent,Alias,PageDesc,ContentStatus").where(where).select

		that.d("recordCount") = XM_("Sort--Thread").db.curNodes.length
				
		that.d("thread") = topicRS	
		
		dim obj
		set obj = objCMS
		
		if LCase(POP_MVC.a) = "search" then
			if POP_MVC.req("keys") <> "" then
				templatePath= obj.getTemplatePath( "User/search" )
			else
				templatePath= obj.getTemplatePath( "User/" & POP_MVC.a )
			end if
		else
			templatePath= obj.getTemplatePath( "User/" & POP_MVC.a )
		end if

		obj.load(templatePath)	

		obj.parseHtml	
		
		obj.output
	end sub
	
	sub index
		theStatus = 1
		Call getListData
	end sub
	
	sub Unchecked
		theStatus = 0
		Call getListData
	end sub
	
	sub Failed
		theStatus = 3
		Call getListData
	end sub
	
	sub recycle
		theStatus = 2
		Call getListData
	end sub	
	
	
	'搜索页
	sub search
		Call getListData
	end sub	
	
	'添加页面
	sub add
		that.d("summernote") = P_("auto").summernote("#L_content")		
		objCMS.show("User/add")
	end sub
	

	
	'修改页面
	sub view
		dim rs,id,where,status

		id = that.get("id")
		set rs = XM_("Sort--Thread").db.path( tableName & "[TopicID=" & id & "]").find
		
		if rs.count = 0 then
			that.error("要修改的组件不存在!")
		end if
		
		if rs("ContentStatus") - 1 = 0 then
			response.redirect "?Sort--Index_detail_" & id
			response.end
		end if
		
		status = K_("Sort--Public").getCnStatus( rs("ContentStatus") )
		
		that.d("ThreadStatus") = status 
		
		if rs("UserID") - session("adminId") <> 0 and Not POP_MVC.arr.exists( bookConfig.SystemAdmin , S_("adminId") ) then
			that.error("您无权查看该" & bookConfig.ThreadTitle)
		end if
		that.d("thread") = rs
		set where = D_
		where("UserID") = rs("UserID")
		that.d("userRS") = B_("User").where(where).find
		objCMS.show("User/view")
	end sub	
	
	
	'添加操作
	sub doAdd
		dim data,id,key
		set data = POP_MVC.form2dict
		
		data("Avatar") = session("Avatar")
		data("LoginName") = session("adminName")
		data("Nickname") = session("Nickname")
		
		if not XM_( "Sort--Thread" ).db.data(data).create(1) then
			that.error( XM_( "Sort--Thread" ).db.error )
		end if
		
		set data = XM_( "Sort--Thread" ).db.getData()
		
		for each key in data
			if data(key) = "" then
				data.remove(key)
			end if
		next
		
		if data.Exists("Verify") then		
			data.remove("Verify")
		end if
		
		id = XM_( "Sort--Thread" ).db.path("Blogs").data(data).prepend("Blog" , "TopicID")
		

		if  id > 0 then			
			id = XM_( "Sort--Thread" ).index4blog + 1
			XM_( "Sort--Thread" ).index4blog = id
			that.success( Array("组件添加成功" , "?Sort--User_Add") )
		else
			that.error( "添加失败，请重新提交" )
		end if
	end sub

	

	
	sub remove
		dim id
		id = that.get("id")
		Call XM_( "Sort--Thread" ).db.path(tableName & "[TopicID=" &id & "]").remove
		that.success( Array("删除成功" , "?Sort--User_list") )
	end sub
	

end Class
%>