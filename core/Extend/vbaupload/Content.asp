<%
Class Content
	Sub init
		'表名 
		'db是popasp_access类的实例化
		db.tableName = "Self_GuestTopic"
		
		'主键
		db.prikey = "TopicID"
		
		db.auto_ = Array( _
			Array( "IsTop" ,0,  1 ) _
			,Array( "IsRecommend" ,0,  1 ) _
			,Array( "IsHeadline" ,0,  1 ) _
			,Array( "IsFeatured" ,0,  1 ) _
			,Array( "IsNoComment" ,0,  1 ) _
			,Array( "Visits" ,0,  1 ) _
			,Array( "Star" ,0,  1 ) _	
			,Array( "AddTime" ,"POP_MVC.FormatDate( now() , ""YYYY-MM-DD HH:II:SS"" )",  1 , "function" ) _
			,Array( "EditTime" ,"POP_MVC.FormatDate( now() , ""YYYY-MM-DD HH:II:SS"" )",  2 , "function" ) _
			,Array( "IndexImage" ,"",  1  ) _
			,Array( "DownURL" ,"",  1  ) _
			,Array( "TitleColor" ,"#000000",  1 ) _
			,Array( "ReplyCount" ,"0",  1 ) _
			,Array( "ContentStatus" , 1 ,  1 ) _
			,Array( "ContentStatus" , 1 ,  2 ) _
			,Array( "UserID" , 1 ,  3 ) _
			,Array( "ZanCount" , 0 ,  1 ) _
			,Array( "CaiCount" , 0 ,  1 ) _
			,Array( "AnswerReward" , 0 ,  1 ) _
			,Array( "IP" , "get_client_ip" ,  1, "function" ) _
			,Array( "Title" , "bbs--Thread.stripTags" , 3 , "callback" , array() )_
			,Array( "Content" , "bbs--Thread.replaceLink" , 3 , "callback" , array() )_
		)
		
		'数据验证
		db.validate_ = Array( _
			 Array( "Title" ,"",  "标题不能为空"  , 1, "notempty" , 3  ) _
			,Array( "Title","1,255","标题长度不能超过255个字符" , 1 , "length" , 3 ) _
			,Array( "SortID",0,"请选择分类" , 1 , "neq" , 3 ) _
			,Array( "SortID","db_id","分类必须是正整数" , 1 , "regex" , 3 ) _
			,Array( "Content" ,"",  "内容不能为空"  , 1, "notempty" , 3  ) _
			,Array( "verify","","验证码不正确" , 0 , "verify" , 3 ) _
		)
	end sub
End Class
%>