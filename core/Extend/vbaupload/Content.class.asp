<%
Class Content
	Sub list
		dim rs
		set rs = B_("Content").field("ContentID,SortName,Title").from("{prefix}Content as a").leftJoin( "{prefix}Sort as b on a.SortID = b.SortID" ).where( "SortName <> ''" ).top(10).order("ContentID DESC").select
		that.ajaxData(rs)
	end sub
	
	sub sortList
		dim dict,dSort,key,i,arr
		set dSort = D_
		
		arr = split( guestConfig.SortUserID , "," )
		for i = 0 to ubound( arr )
			set dict = D_
			dict("SortID") = arr(i)
			dict("SortName") = getSortName( arr(i) )
			dict("SortTypeName") = "普通会员栏目"
			dict("popasp_level") = 1
			dSort.add dSort.count,dict	
		next
		arr = split( guestConfig.SortAdminID , "," )
		for i = 0 to ubound( arr )
			set dict = D_
			dict("SortID") = arr(i)
			dict("SortName") = getSortName( arr(i) )
			dict("SortTypeName") = "管理员栏目"
			dict("popasp_level") = 1
			dSort.add dSort.count,dict	
		next
		
		arr = split( guestConfig.SortHideID , "," )
		for i = 0 to ubound( arr )
			set dict = D_
			dict("SortID") = arr(i)
			dict("SortName") = getSortName( arr(i) )
			dict("SortTypeName") = "隐藏栏目"
			dict("popasp_level") = 1
			dSort.add dSort.count,dict	
		next
		
		that.ajaxData(dSort)
	end sub
	
	sub langList
		dim dict,dLang,key,item,data
		set data = B_("Language").getAll()
		set dLang = D_
		for each key in data
			set item = data(key)
			set dict = D_
			'dict("LanguageID") = item("LanguageID")
			dict("LanguageName") = item("LanguageName")
			dict("Alias") = item("Alias")
			dict("DefaultTemplate") = item("DefaultTemplate")
			'dict("SortCount") = B_("Sort").where( "LanguageID=" & item("LanguageID") ).count
			dict("SortCount") = B_("Sort").count
			dict("IsDefault") = item("IsDefault")
			dict("LanguageStatus") = item("LanguageStatus")
			dLang.add dLang.count,dict
		next
		that.ajaxData(dLang)
	end sub
	
	sub SiteInfo
		dim dict,where
		set where = D_
		where("GroupID") = 17
		set dict = B_("self_GuestConfig").field("ConfigTitle,ConfigValue").where(where).getKeyValue
		that.ajaxData(dict)
	end sub

End Class
%>