<%
Class Index
	private tableName
	
	
	Sub initialize		
		tableName = "vbaupload--Content"	
		POP_MVC.config("UPLOAD_SAVE_PATH") = POP_MVC.config("CMS_UPLOAD_NAME") & "/system/3/"
	End Sub
	
	sub test
		var_export POP_MVC.String.reg_test( POP_MVC.config("CMS_UPLOAD_NAME") & "/system/3/202212311655551389.txt" , "\d{15}[.]txt" ,"i"  )
	end sub
	
	sub upload
		dim filename
		
		filename = POP_MVC.upload( "filedata" )
		
		set upfile = POP_MVC.Uploader
		if filename <> "" then
			'写入数据库
			if not POP_MVC.String.reg_test( POP_MVC.file.basename(filename) , "\d{15}[.]txt" ,"i"  ) then
				Call M_("UploadLog--UploadLog").Log( upfile.SrcName,  filename ,  upfile.TotalSize , "upfiles" , upfile.ErrorID ,upfile.description )
			end if
		end if
		
		response.write filename
	end sub
	
	'添加文章
	sub content
		on error resume next
		dim dict
		set dict = P_("http").getAjaxData4str( request.form )
		
		if not dict.Exists("Title") then
			dict("Title") = "测试用word发布文章"
		end if
		dict("ContentStatus") = 1	
		dict("Content") = POP_MVC.file_get_contents( dict("ContentPath") )
		dict("Content") = K_("bbs--Public").escapeHTML( dict("Content") )
		
		Call POP_MVC.file.remove( dict("ContentPath") )

		
		if Not M_(tableName).db.data(dict).create(1) then
			that.error( Array(M_(tableName).db.error , -1 ) )
		end if
		
		set dict = M_(tableName).db.getData
		id = M_(tableName).db.data( dict ).add 
		
		if id > 0 then
			response.write id
		end if
	end sub
End Class
%>