<%
Class CONFIGS
Public vars__
Public [waterMark],[waterType],[waterMarkFont],[waterMarkPic]
Private Sub Class_Initialize
	vars__ = Array("waterMark","waterType","waterMarkFont","waterMarkPic")

	'是否开启水印功能,水印功能需要相关组件的支持，只能对图片进行加水印
	[waterMark]="1"

	'水印类型
	[waterType]="1"

	'水印文字
	[waterMarkFont]="ASPBBS"

	'图片路径
	[waterMarkPic]="/Upload/20181128/20181128134619171917.jpg"
End Sub
End Class
%>