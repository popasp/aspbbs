	'summernote编辑器
	Public Function summernote( textarea )
		Dim method_name,content,jsonAsp,url
		
		method_name = "upload4summernote"
		
		summernote = POP_MVC.file_get_contents( POP_MVC.mvc_dir & "Module/summernote/editor.js" )
		summernote = Replace( summernote , "TEXTAREA" , textarea )
		summernote = Replace( summernote , "AUTO_FOLDER_NAME" , POP_MVC.config("AUTO_FOLDER_NAME") )
		
		if POP_MVC.config( "URL_MODE" ) = 1 then
			url = "?upload4summernote_upload"
		else
			url = "?c=" & POP_MVC.c & "&a=" & "upload4summernote"
		end if
		
		summernote = Replace( summernote , "UPLOADURL" , url )
		
		If Not is_empty( POP_MVC.config( "APP_DEBUG" ) ) Then POP_MVC.pushAuto( "生成JS代码 " & Server.HtmlEncode( summernote ) )		
		
		If Not POP_MVC.file.isExists( "./" & POP_MVC.config("AUTO_FOLDER_NAME") & "/summernote" ) Then
			POP_MVC.file.CopyFolder POP_MVC.config("AUTO_EXTENDS_PATH") & "summernote/summernote" , "./" & POP_MVC.config("AUTO_FOLDER_NAME") & "/summernote"
		End If		
		
		
		'如果方法体已经写入Runtime/Class/IndexAction.Class.asp中，则不再往下执行		
		if method_exists( POP_MVC.c & "Action" , method_name ) then
			Exit Function
		else
			content = POP_MVC.file_get_contents( POP_MVC.config("AUTO_EXTENDS_PATH") & "summernote/js.txt" ) & VbCrLf
			content = "<" & "%" & VbCrLf & inc_content & content & VbCrLf & "%" & ">"
			Call POP_MVC.file_put_contents( inc_file ,content )	
		end if
	End Function