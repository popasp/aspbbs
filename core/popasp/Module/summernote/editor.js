<link href='AUTO_FOLDER_NAME/summernote/summernote-lite.min.css' rel='stylesheet' /> 
<link href='AUTO_FOLDER_NAME/summernote/tam-emoji/css/emoji.css' rel='stylesheet' /> 
<script src="AUTO_FOLDER_NAME/summernote/jquery-3.4.1.min.js"></script>   
<script src="AUTO_FOLDER_NAME/summernote/summernote-lite.min.js"></script>   
<script src="AUTO_FOLDER_NAME/summernote/summernote-zh-CN.min.js"></script>   
<script src="AUTO_FOLDER_NAME/summernote/tam-emoji/js/config.js"></script>   
<script src="AUTO_FOLDER_NAME/summernote/tam-emoji/js/tam-emoji.min.js"></script>   
<script>
	document.emojiSource = 'AUTO_FOLDER_NAME/summernote/tam-emoji/img';
	document.emojiButton = 'iconfont icon-biaoqing';
  $('TEXTAREA').summernote({
	placeholder: $('TEXTAREA').attr('placeholder'),
	tabsize: 2,
	height: $('TEXTAREA').css('height'),
	fontNames: ['Arial', 'Arial Black', 'Comic Sans MS', 'Courier New'],
	lang: 'zh-CN',
	prettifyHtml: true,
	toolbar: $('TEXTAREA').attr('toolbar') ? eval($('TEXTAREA').attr('toolbar')) : [
	  ['history', ['undo', 'redo']],
	  ['insert', ['emoji']],
	  ['style', ['style']],
	  ['font', ['bold', 'underline', 'clear']],
	  ['color', ['color']],
	  ['para', ['ul', 'ol', 'paragraph']],
	  ['table', ['table']],
	  ['insert', ['hr','link', 'picture', 'video']],
	  //['view', ['fullscreen', 'codeview', 'help']]
	  ['view', [{if1:S_("GroupID") = 1}'codeview',{end if1}'help']]
	],
	callbacks:{
		onImageUpload: function(files,editor,editable){
			sendFile(files[0]);
		}
	}
  });
//选择图片时把图片上传到服务器再读取服务器指定的存储位置显示在富文本区域内
function sendFile(file, editor, editable) {
	$(".note-toolbar.btn-toolbar").append('正在上传图片');
	var filename = "";
	try{
		filename = file['name'];

	} catch(e){filename = "";}

	if(!filename){$(".note-alarm").remove();}		
	var formdata = new FormData();
	formdata.append("file", file );
	$.ajax({
		data : formdata,
		type : "POST",
		url : "UPLOADURL", //图片上传出来的url，返回的是图片上传后的路径，http格式
		cache : false,
		contentType : false,
		processData : false,
		dataType : "json",
		success: function(data) {
			if ( data.status == 1 ) {
				alert("上传失败:"+data.msg);
				
			} else {
				if( data.link ){
					$('TEXTAREA').summernote('editor.pasteHTML', data.link );	
				} else {
					$('TEXTAREA').summernote('editor.insertImage', data.msg , filename );	
				}
							
				$(".note-alarm").html("上传成功,请等待加载");
				setTimeout(function(){
					$(".note-alarm").remove();
				},3000);	
			}
		},
		error:function(data){
			alert("上传失败:"+data.msg);
			//$(".note-alarm").html("上传失败:"+data.msg );
			setTimeout(function(){
				$(".note-alarm").remove();
			},3000);
		}
	});	  	  
	}
</script>