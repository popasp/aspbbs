<%
' +----------------------------------------------------------------------
' | POPASP [ ASP MVC ] 测试中，谨慎使用...
' +----------------------------------------------------------------------
' | Copyright (c) 2016 http://popasp.com All rights reserved.
' +---------------------------------------------------------------------' | Author: popasp <1737025626@qq.com>
' +----------------------------------------------------------------------
Class POPASP_HTTP
  Public host	'host name we are connecting to
  Public port	'port we are connecting to
  
  Public agent	'agent we masquerade as
  public accept		'http accept types
  public referer	'referer info to pass
  
  'Public expandlinks_	'expand links to fully qualified URLs.其功能还在开发中...
  
  Public  CharSet	'POPASP Charset,UTF-8
  
  Private options	'连贯操作接受的参数
  
  Public ResolveTimeout, ConnectTimeout, SendTimeout, ReceiveTimeout
  Private dHeader,ClassType
  
  Public [error]	'错误信息
  
  
  Private Sub Class_Initialize
	ClassType = typename(Me)
  
    '服务器解析超时（毫秒）
    ResolveTimeout = 20000
    '服务器连接超时（毫秒）
    ConnectTimeout = 20000
    '发送数据超时（毫秒）
    SendTimeout = 300000
    '接受数据超时（毫秒）
    ReceiveTimeout = 60000
	
	'expandlinks_ = true
  
    CharSet = "UTF-8"	'系统采用UTF-8编码	
	
    Set dHeader = D_	
	set options = D_
	
	agent = "User-Agent:Mozilla/5.0 (Windows NT 6.1; rv:2.0.1) Gecko/20100101 Firefox/4.0.1"
	accept = "image/gif, image/x-xbitmap, image/jpeg, image/pjpeg, */*"
	dHeader("User-Agent") = agent
	dHeader("Accept") = Accept
	dHeader("Referer") = referer
  End Sub
  
  
  Private Sub Class_Terminate
    Set dHeader = Nothing
  End Sub
  
  '设置请求头信息
  Public Sub SetHeader(ByVal a)
    Dim i,n,v,arr
    If isArray(a) Then
      For i = 0 To Ubound(a)
		arr = split( a(i) , ":" )
        n = Trim( arr(0) )
        v = Trim( arr(1) )
        dHeader(n) = v
      Next
    Else
	  arr = split( a, ":" )
      n = Trim( arr(0) )
      v = Trim( arr(1) )
      dHeader(n) = v
    End If
  End Sub
  
  '设置或获取单项请求头信息
  Public Property Let RequestHeader(ByVal name, ByVal value)
    dHeader(name) = value
  End Property  
  Public Property Get RequestHeader(ByVal name)
    If dHeader.exists(name) Then
      RequestHeader = dHeader(name)
    End If
  End Property
  
  '连贯操作，设置uri
  Public Property Get uri( arg )
	on error resume next
	options("uri") = arg
	referer = GetHost( options("uri") )
	set uri = Me
  End Property
  
  '连贯操作，设置请求方法，常用的有GET、POST
  Public Property Get method( arg )
	options("method") = arg
	set method = Me
  End Property
  
  '连贯操作，是否为同步请求，默认为false
  Public Property Get async( arg )
	options("async") = arg
	set async = Me
  End Property
  
  '连贯操作，请求数据，arg为二维数组或字符串
  Public Property Get data( arg )
	POP_MVC.Dict.Edit options,"data",arg
	set data = Me
  End Property
  
  '连贯操作，用户名
  Public Property Get user( arg )
	options("user") = arg
	set user = Me
  End Property
  
  '连贯操作，密码
  Public Property Get password( arg )
	options("password") = arg
	set password = Me
  End Property
  
  Public Property Get [Get]( byref url )
	referer = GetHost(url)
	[Get] = Me.uri(url).method("GET").GetHTML()	
  End Property
  
  Public Property Get Post( byref url , byref arg )

	Post = Me.uri(url).method("POST").data(arg).GetHTML()
  End Property
  

  '获取http的responseBody
  '使用方法：P_("http").uri("网址").GetBody
  Public Property Get GetBody()
	dim http
	set http = GetHttp
	if not is_empty( http ) then
		GetBody = http.responseBody
	end if
	set http = nothing
	set options = nothing
	set options = D_
  End Property
  
  '将网上图片保存到本地
  '使用方法：P_("http").uri("图片地址").GetImage
  Public Property Get GetImage( savePath )  
	on error resume next
	dim http,bytes
	bytes = GetBody	
	if not isEmpty(bytes) then
		Call POP_MVC.stream_put_contents( savePath,bytes )
	end if
	Call L_( ClassType & ".GetImage" )
  End Property
  
  '是否支持采集
  Public Property Get IsInstall( ) 
	on error resume next
	dim http	

	set http = GetHttp	
	if not http is nothing then	
		IsInstall= true
	else
		IsInstall = false	
	end if
	set http = nothing
  End Property
 
  
  
  '获取http返回的文档，并转化为UTF-8
  '使用方法：P_("http").uri("网址").GetHTML
  Public Property Get GetHTML()
	dim http,context,headers,s_charset
	set http = GetHttp
	if http is nothing then
		POP_MVC.warning( "无法采集" )
		Exit Property
	end if
	
	
	if is_empty( http ) then
		Exit Property
	end if
	context = http.responseText
	headers = http.getAllResponseHeaders()
	s_charset = ""
	
	'从Header中提取编码信息
	If POP_MVC.String.reg_test(Headers,"charset=([\w-]+)","gim") Then
	  s_charset = POP_MVC.String.reg_replace(Headers,"$2","([\s\S]+)charset=([\w-]+)([\s\S]+)","im")
	'如果是Xml文档，从文档中提取编码信息
	ElseIf POP_MVC.String.reg_test(Headers,"Content-Type: ?text/xml","i") Then
	  s_charset = POP_MVC.String.reg_replace(context,"$1","^<\?xml\s+[^>]+encoding\s*=\s*""([^""]+)""[^>]*\?>([\s\S]+)","im")
	'从文件源码中提取编码
	ElseIf POP_MVC.String.reg_test(context,"<meta\s+http-equiv\s*=\s*[""']?content-type[""']?\s+content\s*=\s*[""']?[^>]+charset\s*=\s*([\w-]+)[^>]*>","i") Then
	  s_charset = POP_MVC.String.reg_replace(context,"$2","([\s\S]+)<meta\s+http-equiv\s*=\s*[""']?content-type[""']?\s+content\s*=\s*[""']?[^>]+charset\s*=\s*([\w-]+)[^>]*>([\s\S]+)","i")
	End If	
	
	GetHTML = context
	
	'如果无法获取远程页的编码则继承的编码设置
	if StrComp( Lcase(s_charset), Lcase(Me.Charset) ) <> 0 then
		if s_charset <> "" then
			GetHTML = Bytes2Bstr( http.responseBody , s_charset )
		end if
	end if
	
	If Instr(GetHTML, vbLf) Then
		GetHTML = Replace(GetHTML, vbLf, vbCrLf)
		GetHTML = Replace(GetHTML, vbCr & vbCrLf, vbCrLf)
	End If
	set http = nothing
	set options = nothing
	set options = D_
  End Property
  

  '获取http对象
  '使用方法：P_("http").uri("网址").GetHttp
  Public Property Get GetHttp( )
    Dim a_http, i, ht 
	dim startTime : startTime = timer()

	'解析参数
	Call ParseOptions
    If is_empty( options("uri") ) Then 
		Exit Property
	End If

    a_http = Split("Msxml2.ServerXMLHTTP.6.0 MSXML2.serverXMLHTTP MSXML2.XMLHTTP Microsoft.XMLHTTP")
    i = 0

    Do While True
      On Error Resume Next
	  set ht = POP_MVC.SCO( array(a_http(i)) )
      If err.number = 0 Then	  
        If Instr(a_http(i), "server") Then
        '设置超时时间
          ht.SetTimeOuts ResolveTimeout, ConnectTimeout, SendTimeout, ReceiveTimeout
        End If
		
        '打开远程页
        If not is_empty(options("user")) Then	'如果有用户名和密码          
          ht.open options("method"), options("uri"), options("async"), options("user"), options("password")
        Else          
          ht.open options("method"), options("uri"), options("async")
        End If
		
        If options("method") = "POST" Then
          If Not dHeader.Exists("Content-Type") Then
            dHeader("Content-Type") = "application/x-www-form-urlencoded"
          End If
		  Call SetHeader_( ht )
          '有发送的数据
          ht.send options("data")
        Else
		  Call SetHeader_( ht )
          ht.send
        End If
		
        If Err.Number = 0 Then
          Exit Do
		else
		  set ht = nothing
        End If
      End If
      If i < Ubound(a_http) Then
        i = i + 1
      Else
		Me.error = "没有可支持http的组件"
		set GetHttp = nothing
		Exit Property
        Exit Do
      End If
      On Error Goto 0
    Loop	

    '检测返回数据
    If ht.readyState <> 4 Then
      Me.error = "error:server is down"
      Set ht = Nothing
      Exit Property
    ElseIf ht.Status = 200 Then
      Headers = ht.getAllResponseHeaders()
      set GetHttp = ht
	  call POP_MVC.pushTime( startTime ,"得到http对象(" & a_http(i) & ")" )
	  exit Property
    Else
	  Me.error = "error:" & ht.Status & " " & ht.StatusText
    End If
	set GetHttp = nothing
    L_("POPASP_HTTP.GetHttp")
  End Property 
 

  '取出html片段中<img>标签，返回数组
  'string为要取出图片的字符串
  'hasTag为是否需要<img>标签
  Public Function FetchImgUrls(ByVal string, ByVal hasTag)
    Dim s_rule, a, Matches, match, i, s_img, s_src, s_path
    '去掉script标签，因为其中可能含有不正确有图片地址
    string = POP_MVC.String.reg_replace(string, "", "<script([\s\S]+?)</script>","gim")
    '匹配img标签的正则
    s_rule = "<img[^>]*?\s+src\s*=\s*((?:"")([^""]+)(?:"")|(?:')([^']+)(?:')|([^\s>]+))[^>]*>"
    i = 0
    If POP_MVC.String.reg_test(string, s_rule , "gim") Then
      '取消所有的换行和缩进
      string = Replace(string, vbCrLf, " ")
      string = Replace(string, vbTab, " ")
      '正则匹配所有的img标签
      Set Matches = POP_MVC.String.reg_exec(string, s_rule , "gim")
      ReDim a(Matches.Count-1)
      '取出每个img标签
      For Each match In Matches
        '取出图片标签
        s_img = match.Value
        '取出图片地址
        s_src = Replace(Replace(match.SubMatches(0), """", ""), "'", "")
        '更新标签中的src地址
        s_img = ReplacePart(s_img, s_rule, "$1", """" & s_src & """")
        a(i) = iif(hasTag, s_img, s_src)
        i = i + 1
      Next
    Else
      a = Array()
    End If
    FetchImgUrls = a
  End Function
  
  '替换正则表达式编组
  '说明：按正则表达式的规则替换一个字符串中某个捕获编组的内容
  '示例：ReplacePart("photo-3.html", "^(\w+)-(\d+)\.html$", "$2", "4")
  '     返回： photo-4.html
  Public Function ReplacePart(ByVal string, ByVal rule, ByVal group, ByVal replaceWith)
    If Not POP_MVC.String.reg_test(string, rule,"gim") Then
      '如果规则不匹配则直接返回字符串
      ReplacePart = string
      Exit Function
    End If
    Dim o_match, i, j, s_match, i_pos, s_left, s_tmp
    '获取编组号
    i = Int(Mid(group,2))-1
    '取得正则编组
    Set o_match = POP_MVC.String.reg_exec(string,rule,"gim")(0)
    '循环编组查找匹配项
    For j = 0 To o_match.SubMatches.Count-1
      s_match = o_match.SubMatches(j)
      '取得当前组的字符开始位置
      i_pos = Instr(string,s_match)
      If i_pos > 0 Then
        '把字符串按当前组的位置分为两部分
        s_tmp = Left(string,i_pos-1)
        string = Mid(string,Len(s_tmp)+1)
        '如果找到匹配的编组号则仅替换本组中的字符串
        If i = j Then
          '把替换后的字符串和前一部分组合起来
          ReplacePart = s_left & s_tmp & Replace(string,s_match,replaceWith,i_pos-len(s_tmp),1,0)
          Exit For
        End If
        '如果没有找到匹配则把当前组的字符串换到前一部分中去
        s_left = s_left & s_tmp & s_match
        '在后面部分的字符串中继续下一次扫描匹配
        string = Mid(string, Len(s_match)+1)
      End If
    Next
    Set o_match = Nothing
  End Function
  
  '将目录路径转换为目标页面的绝对路径
  '参数：  url - 目标页面，path将以此url为基准
  '       path - 要转换的目录
  '示例： expandlinks("http://www.easyaps.cn/test/mypage.html", "/path1/page2.jpg")
  '       返回： http://www.easyaps.cn/path1/page2.jpg
  '      expandlinks("http://www.easyaps.cn/test/mypage.html", "path1/page2.jpg")
  '       返回： http://www.easyaps.cn/test/path1/page2.jpg
  Function expandlinks(ByVal url, ByVal path)
    '如果本来就是绝对路径则直接取出
    If Left(path,7)="http://" Or Left(path,8)="https://" Then expandlinks = path : Exit Function
    Dim tmp, ser, fol
    '页面地址
    tmp = POP_MVC.String.Before(url,"?")
    '服务器地址
    If Left(url,7)<>"http://" And Left(url,8)<>"https://" Then
      ser = ""
    Else
      ser =POP_MVC.String.reg_replace(tmp,"$1","^(https?://[a-zA-Z0-9-.]+)/(.+)$" , "gim")
    End If
    '页面所在路径
    fol = Mid(tmp,1,InstrRev(tmp,"/"))
    expandlinks = iif(Left(path,1) = "/", ser, fol) & path
  End Function
  
  'url参数化
  'a为二维数组
  Private Function seriallize(ByVal a)
    Dim tmp, i, n, v : tmp = ""
	
    If is_empty(a) Then Exit Function
    If isArray(a) Then
      For i = 0 To Ubound(a)
		if isArray( a(i) ) then	'第一个是名，第二个是值
			n = a(i)(0)
			v = a(i)(1)
		else
			n = POP_MVC.String.Before(a(i),":")
			v = POP_MVC.String.After(a(i),":")
		end if
        tmp = tmp & "&" & n & "=" & v
      Next
      If Len(tmp)>1 Then tmp = Mid(tmp,2)
      seriallize = tmp
	elseif isObject( a ) then
	  seriallize = js_encode(a)
    Else
      seriallize = a
    End If	
  End Function
  
  '编码转换
  Private Function Bytes2Bstr(ByVal s, ByVal char) 
    dim oStrm	
    set oStrm = POP_MVC.CreateStream
	
    With oStrm
      .Type = 1
      .Mode =3
      .Open
      .Write s
      .Position = 0
      .Type = 2
      .Charset = char
      Bytes2Bstr = .ReadText
      .Close
    End With
    set oStrm = nothing
  End Function
  
  Private Sub ParseOptions
    '方法：POST或GET
	if is_empty( options("method") ) then
		options("method") = "GET"
	else
		options("method") = UCase(options("method"))
	end if  
  
    '异步
	if is_empty( options("async") ) then
		options("async") = False
	end if
	
	
    '构造Get传数据的URL
	if not is_empty( options("data") ) then
		options("data") = seriallize( options("data") )
	end if

	'拼接参数
    If options("method") = "GET" And not is_empty( options("data") ) Then
		if Instr( options("uri") ,"?")>0 then
			options("uri") = options("uri") & "&"
		else
			options("uri") = options("uri") & "?"
		end if
		options("uri") = options("uri") & options("data")
	end if    
  End Sub
  
  Private Sub SetHeader_( ByRef http )
	dim key
	for each key in dHeader
		http.setRequestHeader key, dHeader(key)
	next
  End Sub  
  
  Public Property Get GetHost(url)
	dim pos
	if POP_MVC.String.iStartsWith( url, "http" ) then
		pos = inStr( 10,url, "/" )
	else
		pos = inStr( 3,url, "/" )
	end if
	if pos <= 0 then
		GetHost = url
	else
		GetHost = mid( url, 1 , pos-1 )
	end if
  End Property  
  
	'获取区间内容
	'context:html文档内容
	'l_d:左定界符
	'r_d:右定界符
	'startPos:起始位置
  Public Function getRange( context , l_d , r_d , startPos,ByRef errDesc )
	dim pos_l,pos_d
	errDesc = ""
	pos_l = inStr( startPos , context , l_d )
	if pos_l <= 0 then
		errDesc = "找不到左侧定界符"
		exit function
	end if
	pos_d = inStr( pos_l + len(l_d) , context , r_d )
	if pos_d <= 0 then
		errDesc = "找不到右侧定界符"
		exit function
	end if
	
	if referer <> "" then
	
	end if
	getRange = mid( context , pos_l + len(l_d) , pos_d - ( pos_l + len(l_d))  )
  End Function
  
	'将区间内容本地化
	'包括采集图片、附件等
  Public Function html2local( host, byval content )
	on error resume next
	dim matches,match,path,arr,filename
	'arr = Array()
	set matches = POP_MVC.String.reg_exec( content, "(?:src)\s*=\s*(""|'|)(.+?)\1" , "gim" )
	for each match in matches
		path =  match.subMatches(1)
		if not POP_MVC.String.reg_test( path, "^http(s?):\/\/(?:[A-za-z0-9-]+\.)+[A-za-z]{2,4}(?:[\/\?#][\/=\?%\-&~`@[\]\':+!\.#\w]*)?$","i" ) then
			path = host & "/" & POP_MVC.ltrim( path, "/" )	
			filename = "/Upload/collect/" & POP_MVC.GetUploadName & POP_MVC.file.extname( path )
			Me.uri( path ).GetImage( filename )
			content = replace( content , match.value , "src='" & filename & "'" )
		end if
		'POP_MVC.Arr.push arr,path
	next

	html2local = content
	Call L_( ClassType & ".html2local" )
  End Function  
  
  '获取标签内容
  'context: html文档内容
  'tag:标签名称，如img、title、h1等
  Public Function GetTagContent( context , tag )
	dim match,matches,pattern,arr
	arr = Array()
	if Lcase( tag ) = "img" then
		pattern = "<img[^>]+src=(""|'|)(.+?)\1[^>]*>"
	else
		pattern = "<" & tag & "[^>]*?>(.*?)</" & tag & ">"
	end if
	
	set matches = POP_MVC.String.reg_exec( context, pattern , "gim" )
	if matches.count > 0 then
		if Lcase( tag ) = "img" then
			for each match in matches
				POP_MVC.Arr.push arr, match.submatches(1)
			next
		else
			for each match in matches
				POP_MVC.Arr.push arr, match.subMatches(0)
			next
		end if
	end if
	GetTagContent = arr
	set match = nothing
	set matches = nothing
  End Function
  
  '获取meta内容
  'context: html文档内容
  'stype:属性值，如keywords、description
  Public Function GetMeta( context , stype )
	dim matches,pattern,temp
	pattern = "<meta[^>]+?name=(""|'|)" & stype & "\1[^>]*?>"	
	set matches = POP_MVC.String.reg_exec( context, pattern , "im" )
	if matches.count > 0 then
		temp = matches(0)
		set matches = nothing
		set matches =  POP_MVC.String.reg_exec( temp, "content\s*=\s*(""|'|)(.+)\1[^>]" , "gim" )

		if matches.count > 0 then
			temp = matches(0).subMatches(1)
		end if
	end if
	temp = POP_MVC.String.trim( temp, """" )
	GetMeta = temp
	set matches = nothing
  End Function
  
	'真正解析解析json
	Private Sub decode( ByRef arg )
		on error resume next		
		dim key,stype,keys,value
		stype = typename( arg )		
		if stype = "Dictionary" then
			keys = arg.keys
			if ubound( keys ) > 0 then
				for each key in arg
					if isObject( arg(key) ) then
						set value = arg(key)
					else
						value = arg(key)
					end if 
					Call decode( value )
					if isObject( value ) then
						set arg(key) = value
					else
						arg(key) = value
					end if						
				next
			end if
		elseif isArray( arg ) then
			for each key in arg				
				if isObject( arg(key) ) then
					set value = arg(key)
				else
					value = arg(key)
				end if 
				Call decode( value )
				if isObject( value ) then
					set arg(key) = value
				else
					arg(key) = value
				end if					
			next
		end if
		Call L_( typename(Me) & ".decode" )
	end sub
	
	'解析使用post传递的数据，数据最外层是_data键名
	Public Function getAjaxData
		on error resume next
		dim bool
		set params = js_decode( request.form( "_data" ) )
		call decode( params )
		set getAjaxData = params
	End Function
	
	'直接解析request.form传递过来的字符串，不必使用_data键名
	Public Function getAjaxData4str( str )
		on error resume next
		dim bool
		set params = js_decode( str )
		call decode( params )
		set getAjaxData4str = params
	End Function
	
	'获取alexa排名
	'返回Dictionary对象， rank:世界排名 delta:排名升降 country:国家 countryRank:国家排名
	function Alexa(AlexaURL):
		on error resume next 
		dim getsms,url,dict
		dim star,endd
		url="http://data.alexa.com/data?cli=10&dat=snba&url="&AlexaURL
		getsms=[get](url)
		set dict = D_
		if getsms<>"" then
			if POP_MVC.String.reg_test(getsms,"<REACH\s+RANK=""(\d+)""\s*\/>","im") then
				dict("rank") = POP_MVC.String.reg_find( getsms ,  "<REACH\s+RANK=""(\d+)""\s*\/>", 1 ,"im" )
				dict("delta") = POP_MVC.String.reg_find( getsms ,  "<RANK\s+DELTA=""([-\d]+)""\s*/>", 1 ,"im" )
				dict("country") = POP_MVC.String.reg_find( getsms ,  "<COUNTRY\s+CODE=""(\w+)""\s+NAME=""(\w+)""\s+RANK=""(\d+)""\s*\/>", 2 ,"im" )
				dict("countryRank") = POP_MVC.String.reg_find( getsms ,  "<COUNTRY\s+CODE=""(\w+)""\s+NAME=""(\w+)""\s+RANK=""(\d+)""\s*\/>", 3 ,"im" )
			end if
		end if
		set Alexa=dict
	end function
  
End Class
%>