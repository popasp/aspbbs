<%
' +----------------------------------------------------------------------
' | POPASP [ PAYMENT ]
' +----------------------------------------------------------------------
' | Copyright (c) 2016 http://popasp.com All rights reserved.
' +---------------------------------------------------------------------
' | Author: popasp <1737025626@qq.com>
' +----------------------------------------------------------------------
Class POPASP_PAYMENT
	Private Sub Class_Initialize
		
	End Sub
	
	Private Sub Class_Terminate		
		
	End Sub
	
	'从微信转帐中获取帐单信息
	'目前支持红包、转账、二维码收付款
	'1为收入,0为支出
	Function wechat( str , stype )
		dim inStypeArr,outStypeArr,qrcodePayStr,i,item
		
		inStypeArr  = Array( "微信红包-来自" , "转账-来自" , "二维码收款-来自" )
		outStypeArr = Array( "微信红包-发给" , "转账-转给" , "扫二维码收款-给", "扫码付款-给"  )
		'qrcodePayStr = "可在支持的商户扫码退款,支付成功"
		
		if stype = 1 or stype = "" then			
			for i = 0 to ubound( inStypeArr )
				item = inStypeArr( i )
				if inStr( str , item ) > 0 then
					set wechat = getWeChatInComeDict( str , 1 , item )
				end if
			next			
		end if
		
		if stype = 0 or stype = "" then			
			for i = 0 to ubound( outStypeArr )
				item = outStypeArr( i )
				if inStr( str , item ) > 0 then
					set wechat = getWeChatPayDict( str , 0 , item )
				end if
			next			
		end if
	End Function
	
	Function getWeChatInComeDict( str, stype , item )
		dim dict
		set dict = D_
		dict("Amount")   = POP_MVC.String.getInStr( str , "+" , chr(10) )
		dict("FromUser") = POP_MVC.String.getInStr( str , item , "+" & dict("Amount") )		
		dict("DealTime")  = getDealTime(str, "收款时间" ,21)
		dict("DealMethod")  = POP_MVC.String.getInStr( item , "" , "-" )
		if left( dict("DealMethod") , 2 ) <> "微信" then
			dict("DealMethod")  = "微信" & dict("DealMethod")
		end if
		dict("DealType")  = stype
		dict("DealNo")  = getDealNo( str, "单号" )
		if inStr(item,"转账") then
			dict("PayInfo")  = POP_MVC.String.getInStr( str , "转账说明" , "转账时间" )
		end if
		
		Call trimSpace4dict(dict)
		set getWeChatInComeDict = dict
	End function
	
	Function getWeChatPayDict( str, stype , item )
		dim dict
		set dict = D_
		dict("Amount")   = POP_MVC.String.getInStr( str , "-" , chr(10) )
		dict("ToUser") = POP_MVC.String.getInStr( str , item , "-" & dict("Amount")  )		
		dict("DealTime")  = getDealTime(str, "支付时间" ,21)
		dict("DealMethod")  = POP_MVC.String.getInStr( item , "" ,  "-" )
		if left( dict("DealMethod") , 2 ) <> "微信" then
			dict("DealMethod")  = "微信" & dict("DealMethod")
		end if
		dict("DealType")  = stype
		if inStr(item,"转账") then
			dict("PayInfo")  = POP_MVC.String.getInStr( str , "转账说明" , "转账时间" )
		end if
		
		Call trimSpace4dict(dict)
		set getWeChatPayDict = dict
	End function
	
	
	'从微信转帐中获取帐单信息
	'支付宝目前支持转账与红包
	'1为收入,0为支出
	Function alipay( str , stype )
		dim inStypeArr,outStypeArr,qrcodePayStr,i,item
		set alipay = D_
		if stype =1 then
			if inStr( str , "转账备注" ) > 0 then
				set alipay = getAliPayFromTransferDict(str)
			end if
			if inStr( str , "红包说明" ) > 0 then
				set alipay = getAliPayFromHongBaoDict(str)
			end if
		end if
		
		if stype =0 then
			if inStr( str , "转账备注" ) > 0 then
				set alipay = getAliPayToTransferDict(str)
			end if
			if inStr( str , "红包说明" ) > 0 then
				set alipay = getAliPayToHongBaoDict(str)
			end if
		end if
	End Function
	
	'支付宝转账收款
	Function getAliPayFromTransferDict( str )
		dim dict
		set dict = D_
		dict("Amount")   = POP_MVC.String.getInStr( str , "+" , chr(10) )
		dict("FromUser") = POP_MVC.String.getInStr( str , "对方账户", "创建时间" )	
		dict("PayInfo")  = POP_MVC.String.getInStr( str , "转账备注" , "对方账户" )
		dict("DealTime")  = getDealTime(str, "创建时间" , 18)
		dict("DealMethod")  = "支付宝转账"
		dict("DealType")  = 1
		dict("DealNo")  = getDealNo( str, "订单号" )

		Call trimSpace4dict(dict)
		set getAliPayFromTransferDict = dict
	End function
	
	'支付宝转账付款
	Function getAliPayToTransferDict( str )
		dim dict
		set dict = D_
		dict("Amount")   = POP_MVC.String.getInStr( str , "-" , chr(10) )
		dict("ToUser") = POP_MVC.String.getInStr( str , "对方账户", "创建时间" )	
		dict("PayInfo")  = POP_MVC.String.getInStr( str , "转账备注" , "对方账户" )
		dict("DealTime")  = getDealTime(str, "创建时间" , 18)
		dict("DealMethod")  = "支付宝转账"
		dict("DealType")  = 0
		dict("DealNo")  = getDealNo( str, "订单号" )

		Call trimSpace4dict(dict)
		set getAliPayToTransferDict = dict
	End function
	
	'支付宝红包收款
	Function getAliPayFromHongBaoDict( str )
		dim dict
		set dict = D_
		dict("Amount")   = POP_MVC.String.getInStr( str , "+" , chr(10) )
		dict("FromUser") = POP_MVC.String.getInStr( str , "红包来自", "创建时间" )	
		dict("PayInfo")  = POP_MVC.String.getInStr( str , "红包说明" , "红包来自" )
		dict("DealTime")  = getDealTime(str, "创建时间" , 18)
		dict("DealMethod")  = "支付宝红包"
		dict("DealType")  = 1
		dict("DealNo")  = getDealNo( str, "订单号" )

		Call trimSpace4dict(dict)
		set getAliPayFromHongBaoDict = dict
	End function
	
	'支付宝红包付款
	Function getAliPayToHongBaoDict( str )
		dim dict
		set dict = D_
		dict("Amount")   = POP_MVC.String.getInStr( str , "-" , chr(10) )
		dict("ToUser") = ""
		dict("PayInfo")  = POP_MVC.String.getInStr( str , "红包说明" , "创建时间" )
		dict("DealTime")  = getDealTime(str, "创建时间" , 18)
		dict("DealMethod")  = "支付宝红包"
		dict("DealType")  = 0
		dict("DealNo")  = getDealNo( str, "订单号" )	
		
		Call trimSpace4dict(dict)
		set getAliPayToHongBaoDict = dict
	End function
	
	'获取订单号
	Function getDealNo( str, labelStr )
		dim tempStr
		tempStr = POP_MVC.String.getInStr( str , labelStr, "" )
		tempStr = replace( tempStr , chr(10) , "" )
		tempStr = replace( tempStr , chr(13) , "" )
		getDealNo = POP_MVC.String.reg_fetch( tempStr, "\w+" , "m" )
	End Function
	
	'获取处理时间
	Function getDealTime( str, labelStr , strLen )
		getDealTime = left(POP_MVC.String.getInStr( str , labelStr, "" ),strLen)
		getDealTime = trimSpace( getDealTime )
		if mid( getDealTime , 11,1 ) <> "" then
			getDealTime = left( getDealTime , 10 ) & " " & mid( getDealTime,11 )
		end if
	End Function
	
	'去掉头尾空白
	Function trimSpace( str )
		trimSpace = POP_MVC.String.reg_replace( str , "" , "^\s+|\s+$" , "gim" )
	End Function

	'为dict的每一项去掉头尾空白
	sub trimSpace4dict( dict )
		dim key
		
		for each key in dict
			dict( key ) = trimSpace( dict(key) )
		next
	End sub
End Class
%>