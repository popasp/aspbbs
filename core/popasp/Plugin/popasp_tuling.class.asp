<%
' +----------------------------------------------------------------------
' | POPASP [ ASP MVC ] 图灵机器人
' +----------------------------------------------------------------------
' | Copyright (c) 2016 http://popasp.com All rights reserved.
' +---------------------------------------------------------------------' | Author: popasp <1737025626@qq.com>
' +----------------------------------------------------------------------
Class POPASP_TULING
  Public API_URL	'http://www.tuling123.com/openapi/api
  Public API_KEY	'需用户输入自已的APIKEY
  
  
  Private Sub Class_Initialize
	if not is_empty( POP_MVC.config("TULING_API_URL") ) then
		API_URL = POP_MVC.config("TULING_API_URL")
	else
		API_URL = "http://www.tuling123.com/openapi/api"
	end if
	if not is_empty( POP_MVC.config("TULING_API_KEY") ) then
		API_KEY = POP_MVC.config("TULING_API_KEY")
	end if
  End Sub
  
  Public Default Function Constructor( byref key )
	API_KEY = key
	set Constructor = Me
  End Function
  
  Public Property Get [Get]( byref info )
	dim http,dict
	set http = P_("HTTP")
	http.RequestHeader("Content-Type") = "application/json"
	set dict = D_
	dict("key") = API_KEY
	dict("info") = info
	dict("userid") = "12345678"
	
	[Get] = http.post( API_URL, dict )
  End Property   
End Class
%>