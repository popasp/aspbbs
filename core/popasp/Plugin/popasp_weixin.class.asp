<%
' +----------------------------------------------------------------------
' | POPASP [ ASP MVC ] 测试中，功能完善中，谨慎使用...
' +----------------------------------------------------------------------
' | Copyright (c) 2016 http://popasp.com All rights reserved.
' +---------------------------------------------------------------------
' | Author: popasp <1737025626@qq.com>
' +----------------------------------------------------------------------
Class POPASP_WEIXIN
	Public xml_dom
	Public Msg
	Public parsed
	
	Private Sub Class_Initialize
		set xml_dom = Server.CreateObject("MSXML2.DOMDocument")
		set Msg = D_
		Parsed = False
	End Sub
	
	Private Sub Class_Terminate		
		set xml_dom = nothing
	End Sub
	
	'解析
	Public Property Get Parse
		on error resume next
		Parsed = true
		
		Me.xml_dom.load request.BinaryRead( request.TotalBytes )
		
		if Me.xml_dom.getelementsbytagname("MsgType").length < 1 then
			set Parse = Me.Msg
			Exit Property
		end if
		
		'消息类别
		Me.Msg( "MsgType" )		= Me.xml_dom.getelementsbytagname("MsgType").item(0).text
		
		'开发者微信号
		Me.Msg( "ToUserName" )	= Me.xml_dom.getelementsbytagname("ToUserName").item(0).text
		
		'发送方帐号（一个OpenID）
		Me.Msg( "FromUserName" )= Me.xml_dom.getelementsbytagname("FromUserName").item(0).text
		
		'消息创建时间 （整型）
		Me.Msg( "CreateTime" )	= Me.xml_dom.getelementsbytagname("CreateTime").item(0).text
		
		'消息id，64位整型
		Me.Msg( "MsgId" )		= Me.xml_dom.getelementsbytagname("MsgId").item(0).text

				
		
		select case Me.Msg( "MsgType" )
			'文本消息
			case "text"
				Me.Msg( "Content" )	= Me.xml_dom.getelementsbytagname("Content").item(0).text
				
			'图片消息
			case "image"
				'图片链接（由系统生成）
				Me.Msg( "PicUrl" )	= Me.xml_dom.getelementsbytagname("PicUrl").item(0).text
				'视频消息媒体id，可以调用多媒体文件下载接口拉取数据。
				Me.Msg( "MediaId" )	= Me.xml_dom.getelementsbytagname("MediaId").item(0).text
				
			'语音消息
			case "voice"
				'语音格式，如amr，speex等
				Me.Msg( "Format" )	= Me.xml_dom.getelementsbytagname("Format").item(0).text
				Me.Msg( "MediaId" )	= Me.xml_dom.getelementsbytagname("MediaId").item(0).text
				
				if Me.xml_dom.getelementsbytagname("Recognition").length >0 then
					Me.Msg( "Recognition" )	= Me.xml_dom.getelementsbytagname("Recognition").item(0).text
				end if
			
			'视频消息
			case "video"
				'视频消息缩略图的媒体id，可以调用多媒体文件下载接口拉取数据。
				Me.Msg( "ThumbMediaId" )	= Me.xml_dom.getelementsbytagname("ThumbMediaId").item(0).text
				Me.Msg( "MediaId" )	= Me.xml_dom.getelementsbytagname("MediaId").item(0).text

			'小视频消息
			case "shortvideo"
				'视频消息缩略图的媒体id，可以调用多媒体文件下载接口拉取数据。
				Me.Msg( "ThumbMediaId" )	= Me.xml_dom.getelementsbytagname("ThumbMediaId").item(0).text
				Me.Msg( "MediaId" )	= Me.xml_dom.getelementsbytagname("MediaId").item(0).text

			'地理位置消息
			case "shortvideo"
				'地理位置维度
				Me.Msg( "Location_X" )	= Me.xml_dom.getelementsbytagname("Location_X").item(0).text
				'地理位置经度
				Me.Msg( "Location_Y" )	= Me.xml_dom.getelementsbytagname("Location_Y").item(0).text
				'地图缩放大小
				Me.Msg( "Scale" )	= Me.xml_dom.getelementsbytagname("Scale").item(0).text
				'地理位置信息
				Me.Msg( "Label" )	= Me.xml_dom.getelementsbytagname("Label").item(0).text
			'链接消息
			case "shortvideo"
				'消息标题
				Me.Msg( "Title" )	= Me.xml_dom.getelementsbytagname("Title").item(0).text
				'消息描述
				Me.Msg( "Description" )	= Me.xml_dom.getelementsbytagname("Description").item(0).text
				'消息链接
				Me.Msg( "Url" )	= Me.xml_dom.getelementsbytagname("Url").item(0).text				
		End Select
		set Parse = Me.Msg
		Call L_("POPASP_WEIXIN.Parse")
	End Property
	
	'获取文本消息的字符串
	public property get text( byref Content )
		if not Me.parsed then Me.Parse
		text = get_xml( Me.text_data( Content ) )
	end property
	
	public property get music( byref arg )
		if not Me.parsed then Me.Parse
		music = get_xml( Me.music_data( arg ) )
	end property
	
	public property get news( byref data )
		if not Me.parsed then Me.Parse
		news = get_xml( Me.news_data( data ) )		
		call file_var_export( "./wx.txt" , news )
	end property
	
	'如果输入字符串，那么只要提供音乐 MusicUrl 就可以了
	'还可以输入 Dictionary 对象，应与
	public property get music_data( byval arg )
		dim dict,stype,data
		set dict = D_
		set data = D_
		dict( "Title" ) = "音乐"		
		dict( "Description" ) = "来自微信公众号“" & Me.Msg("ToUserName") & "”的音乐"
		stype = typename( arg )
		
		'如果不是Dictionary对象，则默认为MusicUrl地址
		if stype <> "Dictionary" then			
			dict( "MusicUrl" ) = arg
		end if
		
		'如果是Dictionary对象，用Merge合并
		if stype = "Dictionary" then
			set dict = POP_MVC.Dict.Merge( dict,arg )
		end if
		
		'如果没有分配高清音乐，用普通音乐地址代替
		if not dict.exists( "HQMusicUrl" ) then
			dict( "HQMusicUrl" ) = dict( "MusicUrl" )
		end if
		
		data( "ToUserName" ) = Me.Msg("FromUserName")
		data( "FromUserName" ) = Me.Msg("ToUserName")
		data( "CreateTime" ) = ToUnixTime( "" , 8 )
		data( "MsgType" ) = "music"
		POP_MVC.Dict.edit data,"Music",dict
		set music_data = data
		set dict = nothing
		set data = nothing
	end property
	
	'获取图文消息 news
	'data必须是一个二维的Dictionary对象
	'每条记录必须含有Title、Description、PicUrl、Url
	'Title  图文消息标题
	'Description  图文消息描述
	'PicUrl  图片链接，支持JPG、PNG格式，较好的效果为大图360*200，小图200*200
	'Url  点击图文消息跳转链接
	public property get news_data( byref data )
		dim dict
		set dict = D_
		dict( "ToUserName" ) = Me.Msg("FromUserName")
		dict( "FromUserName" ) = Me.Msg("ToUserName")
		dict( "CreateTime" ) = ToUnixTime( "" , 8 )
		dict( "MsgType" ) = "news"
		dict( "ArticleCount" ) = data.count		
		POP_MVC.Dict.Edit dict,"Articles",data
		set news_data = dict
	end property
	
	'获取文本消息 text 
	public property get text_data( byref Content )
		dim dict
		set dict = D_
		dict( "ToUserName" ) = Me.Msg("FromUserName")
		dict( "FromUserName" ) = Me.Msg("ToUserName")
		dict( "CreateTime" ) = ToUnixTime( "" , 8 )
		dict( "MsgType" ) = "text"
		dict( "Content" ) = Content
		set text_data = dict
	End property
	
	private function dict2xml( byref dict )
		on error resume next
		dim str,key
		str = ""
		
		for each key in dict
			if isArray( dict(key) ) then
				for i = 0 to ubound( dict(key) )
					str = str & "<item>"  & vbcrlf
					str = str & dict2xml( dict(key)(i) )  & vbcrlf
					str = str & "</item>"  & vbcrlf
				next
			elseif TypeName(dict(key)) = "Dictionary" then				
				if IsNumeric( key ) then					
					str = str & "<item>"  & vbcrlf
					str = str & dict2xml( dict(key) )
					str = str & "</item>"  & vbcrlf					
				else
					str = str & "<" & key & ">"  & vbcrlf
					str = str & dict2xml(dict(key)) 
					str = str & "</" & key & ">" & vbcrlf
				end if
			else
				str = str & "<" & key & "><![CDATA[" & dict(key) & "]]></" & key & ">" & vbcrlf
			end if
		next
		dict2xml = str		
	end function
	
	public function get_xml( dict )		
		get_xml = "<xml>" & vbcrlf & dict2xml( dict ) & "</xml>"
		call file_var_export( "./wx.txt",get_xml )
	End Function
	

	'=========================================================
	'把标准时间转换为UNIX时间戳
	'参数：strTime:要转换的时间；intTimeZone：该时间对应的时区
	'返回值：strTime相对于1970年1月1日午夜0点经过的秒数
	'示例：ToUnixTime("2008-5-23 10:51:0", +8)，返回值为1211511060
	'=========================================================
	Function ToUnixTime(strTime, intTimeZone)
		If IsEmpty(strTime) Or Not IsDate(strTime) Then strTime = Now
		If IsEmpty(intTimeZone) Or Not IsNumeric(intTimeZone) Then intTimeZone = 0
		ToUnixTime = DateAdd("h", - intTimeZone, strTime)
		ToUnixTime = DateDiff("s", "1970-1-1 0:0:0", ToUnixTime)
	End Function

	'=========================================================
	'把UNIX时间戳转换为标准时间
	'参数：intTime:要转换的UNIX时间戳；intTimeZone：该时间戳对应的时区
	'返回值：intTime所代表的标准时间
	'示例：FromUnixTime("1211511060", +8)，返回值2008-5-23 10:51:0
	'=========================================================
	Function FromUnixTime(intTime, intTimeZone)
		If IsEmpty(intTime) Or Not IsNumeric(intTime) Then
			FromUnixTime = Now()
			Exit Function
		End If
		If IsEmpty(intTime) Or Not IsNumeric(intTimeZone) Then intTimeZone = 0
		FromUnixTime = DateAdd("s", intTime, "1970-1-1 0:0:0")
		FromUnixTime = DateAdd("h", intTimeZone, FromUnixTime)
	End Function	
End Class
%>