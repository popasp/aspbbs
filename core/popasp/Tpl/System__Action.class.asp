<%
Class System__
	sub initialize
		if LCase(POP_MVC.a) = "show_page_trace" then
			if Request.ServerVariables("SCRIPT_NAME") <> traceAspFile then
				exit sub
			end if			
		end if
		if LCase(POP_MVC.a) = "cache" and not isEmpty( session("POPASP_PAGE_CACHE_URL") ) and POP_MVC.import("POPASP_CONTROLLER").isSelfOrigin then
			exit sub
		end if

		if is_empty( POP_MVC.config("SHOW_PAGE_TRACE") ) then
			echo "非法操作"
			response.end
		end if
		POP_MVC.config( "SHOW_PAGE_TRACE" ) = 0
		POP_MVC.config( "APP_DEBUG" ) = 0
	end sub
	
	sub cache
		dim content,sep,pos
		if not isEmpty( session("POPASP_PAGE_CACHE_URL") ) then
			content = POP_MVC.import("POPASP_HTTP").get( session("POPASP_PAGE_CACHE_URL") )
			sep = "<!--QQ_1737025626_POPASP_PAGE_TRACE_CACHE_QQ_GROUP_124648143-->"
			if len(content) > 0 then
				pos = instr( content,sep )
				if pos > 0 then
					content = left( content,pos-1 )
				end if
				content = POP_MVC.String.reg_replace(content , "" , "^\s+|\s+$" , "g" )
				Call POP_MVC.file_put_contents( session("POPASP_PAGE_CACHE_PATH") , content )
			end if
			Session.contents.remove("POPASP_PAGE_CACHE_URL")
			Session.contents.remove("POPASP_PAGE_CACHE_PATH")			
		end if		
		response.end
	end sub

	sub index
		echo "欢迎使用POPASP控制台"
	end sub
	
	sub readPOPASPClass
		dim sClass,dict,cnt,str
		sClass = POP_MVC.get("class")
		
		
		if POP_MVC.String.Exists( sClass , "*" ) then
			sClass = Mid( sClass, 1 , inStr( sClass,"*" )-1 )
		end if
		sClass = "POPASP_" & POP_MVC.ltrim(sClass,"POPASP_")

		set dict = POP_MVC.import("POPASP_REJECTION").Parse( sClass )
		str = ""
		cnt = POP_MVC.count( dict("public_vars") )
		str = str & "公有属性 (" & cnt & ") 个 : " & join( dict("public_vars") , " , " )  & "<br />" & chr(13) & chr(10) & "<br />" & chr(13) & chr(10)
		
		cnt = POP_MVC.count( dict("public_method") )
		str = str & "公有方法 (" & cnt & ") 个 : " & join( dict("public_method") , " , " ) & "<br />" & chr(13) & chr(10) & "<br />" & chr(13) & chr(10)
		
		cnt = POP_MVC.count( dict("private_vars") )
		str = str & "私有属性 (" & cnt & ") 个 : " & join( dict("private_vars") , " , " ) & "<br />" & chr(13) & chr(10) & "<br />" & chr(13) & chr(10)
		
		cnt = POP_MVC.count( dict("private_method") )
		str = str & "私有方法 (" & cnt & ") 个 : " & join( dict("private_method") , " , " )
		
		Response.write str
	end sub
	
	'读取文件内容
	sub readFile
		dim file,content
		file = POP_MVC.get("file")
		content = POP_MVC.file_get_contents( file )
		echo content
	end sub
	
	'读session
	sub readSession
		echo session( POP_MVC.get("key") )
	end sub
	
	'删除session中的一项键值
	sub removeSession
		Session.Contents.Remove( POP_MVC.get("key") )
		echo "成功删除session("""& POP_MVC.get("key") & """)"
	end sub
	
	private sub echo( arg )
		Response.write arg
	end sub
	
	sub show_page_trace
		dim trace,content,url,fileName,runtime,file_count,trace_dir
		dim objFolder,objFiles
		
		'最多存放多少个文件
		if POP_MVC.config.Exists("SHOW_PAGE_TRACE_LIMIT") then
			file_count = POP_MVC.config("SHOW_PAGE_TRACE_LIMIT")
		else
			file_count = 1000		
		end if
		
		'存放目录
		if POP_MVC.config.Exists("SHOW_PAGE_TRACE_DIR") then
			trace_dir = POP_MVC.config("SHOW_PAGE_TRACE_DIR")
		else
			trace_dir = POP_MVC.appPath & "/runtime/Trace/"
		end if
		
		trace_dir = POP_MVC.String.rtrim( trace_dir , "/" )
		trace_dir = POP_MVC.String.rtrim( trace_dir , "\" ) & "/"
		
		if POP_MVC.file.isFolder( trace_dir ) then
		
			'文件夹对象
			set objFolder=POP_MVC.fso.GetFolder( POP_MVC.realpath(trace_dir) )
			set objFiles=objFolder.Files
			
			'超过数量就清空
			if objFiles.count >= file_count then
				Call POP_MVC.file.remove(trace_dir)
			end if
		
		end if
		

		
		'得到数据
		set trace = js_decode( POP_MVC.file_get_contents(jsDataFile) )

		'运行时间
		runtime = trace("runtime")
		that.d("runtime") = runtime
		
		'网址
		url = trace("url")
		
		
		trace.remove("runtime")
		trace.remove("url")
		
		'生成文件名
		fileName = POP_MVC.String.reg_replace( url , "$" , "[:|/\\*<>?""]" , "g" )
		
		'时间后缀
		if POP_MVC.config.Exists("SHOW_PAGE_TRACE_FORMATDATE") then
			fileName = fileName & "_" & POP_MVC.FormatDate( now() , POP_MVC.config("SHOW_PAGE_TRACE_FORMATDATE") )		
		end if
		
		
		
		fileName = left( fileName,200 )
		
		fileName = trace_dir & fileName & ".html"

		that.d("trace") = trace
		
		content = that.fetch("System__/page_trace_tpl")
		
		Call POP_MVC.file_put_contents( fileName , content )		
		
		Call POP_MVC.file.remove( jsDataFile )
		call POP_MVC.file.remove(traceAspFile)
		response.end
	end sub
End Class
%>