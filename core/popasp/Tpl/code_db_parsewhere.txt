		On Error Resume Next
		
		dim key,ts,str,j
		dim arr,bound,i,tempType,temp,tempArr,k
		ParseWhere = ""	
		
		if TypeName( opts ) = "String" Then
			if trim(opts) <> "" then
				' 注意这儿并未对输入字符串作safe处理
				ParseWhere = ParseWhere & " AND " & opts
			end if
		elseif isObject(opts) Then			
			if POP_MVC.count(opts) > 1 then
				parseWhere = parseWhere & " ( "
			end if
			j = 0
			
			for each key in opts	
				str = ""
				
				if is_array( opts(key) ) Then	' 如果是数组，则应有这样的格式 array("in","1,2,3,4,5,6")					
					if isObject( opts(key) ) Then						
						opts(key) = tool.obj2arr( opts(key) )
					End If
					arr = opts(key)
					bound = ubound( arr )
					
					if bound>0 Then
						tempType = TypeName( arr(1) )
						select case LCase(arr(0))
							case "in"		' in								
								if is_array( arr(1) ) then	'array("in" , array(1,2,3,4) )
									if isObject( arr(1) ) Then
										arr(1) = tool.obj2arr( arr(1) )
									End If
									if ubound(arr(1)) > -1 then
										for k = lbound( arr(1) ) to ubound( arr(1) )
											if is_numeric(arr(1)(k)) then
												POP_MVC.Arr.push tempArr , arr(1)(k)
											end if
										next
										
										if ubound( tempArr ) > -1 then
											str = key & " IN (" & Join( tempArr ,",") & ") "
										else
											call POP_MVC.error( Me.db_type & " parseWhere in")
										end if
										tempArr  = Array()
									end if									
								Elseif POP_MVC.isScalar(arr(1)) Then	'array("in" , "1,2,3,4" )
									str =  key & " IN (" & POP_MVC.String.reg_replace(arr(1) , "," , "[,]{2,}" , "gi" ) & ") "
								Else
									call POP_MVC.error( Me.db_type & " parseWhere in")
								End If
							case "notin"		' in								
								if is_array( arr(1) ) then	'array("not in" , array(1,2,3,4) )
									if isObject( arr(1) ) Then
										arr(1) = tool.obj2arr( arr(1) )
									End If
									if ubound(arr(1)) > -1 then
										for k = lbound( arr(1) ) to ubound( arr(1) )
											if is_numeric(arr(1)(k)) then
												POP_MVC.Arr.push tempArr , arr(1)(k)
											end if
										next
										
										if ubound( tempArr ) > -1 then
											str = key & " NOT IN (" & Join( tempArr ,",") & ") "
										else
											call POP_MVC.error( Me.db_type & " parseWhere not in")
										end if
										tempArr  = Array()
									end if									
								Elseif POP_MVC.isScalar(arr(1)) Then	'array("not in" , "1,2,3,4" )
									str =  key & " NOT IN (" & POP_MVC.String.reg_replace(arr(1) , "," , "[,]{2,}" , "gi" ) & ") "
								Else
									call POP_MVC.error( Me.db_type & " parseWhere in")
								End If
							case "strin"		' strin	
								if is_array( arr(1) ) then
									if isObject( arr(1) ) Then
										arr(1) = tool.obj2arr( arr(1) )
									End If
									if ubound(arr(1)) > -1 then
										str = key & " IN ('" & Join(arr(1),"','") & "') "
									end if
								Elseif POP_MVC.isScalar(arr(1)) Then
									str =  key & " IN ('" & join( POP_MVC.String.reg_split(arr(1) , "[,，;；|]" , "gi" ) , "','" ) & "') "
								Else
									call POP_MVC.error( Me.db_type & " parseWhere in")
								End If
							case "eq","="		' eq
								if POP_MVC.isScalar(arr(1)) Then	'array("eq" ,"scale" )
									if is_numeric( arr(1) ) and typename(arr(1)) = "String" and ( db_type = "access" or db_type="sqlserver" ) then
										str =  key & " = '" & arr(1) & "'"
									else
										str =  key & " = " & getSqlStr( VarType(arr(1)) ,arr(1) )
									end if
									
								else 
									call POP_MVC.error( Me.db_type & " parseWhere eq")
								End If
							case "neq","<>","!="
								if POP_MVC.isScalar(arr(1)) Then	'array("neq" ,"scale" )
									str =  key & " <> " & getSqlStr( VarType(arr(1)) ,arr(1) )
								else
									call POP_MVC.error( Me.db_type & " parseWhere neq")
								End If
							case "gt",">"
								if isNumeric( arr(1) ) Then	'array("gt" ,3 )
									str =  key & " > " & arr(1)
								else 
									call POP_MVC.error( Me.db_type & " parseWhere gt")
								End If
							case "lt","<"
								if isNumeric( arr(1) ) Then	'array("gt" ,3 )
									str =  key & " < " & arr(1)
								else
									call POP_MVC.error( Me.db_type & " parseWhere lt")

								End If
							case "elt","<="
								if isNumeric( arr(1) ) Then	'array("elt" ,3 )
									str =  key & "<=" & arr(1)
								else
									call POP_MVC.error( Me.db_type & " parseWhere elt")
								End If
							case "egt",">="
								if isNumeric( arr(1) ) Then	'array("glt" ,3 )
									str =  key & " >= " & arr(1)
								else 
									call POP_MVC.error( Me.db_type & " parseWhere glt")
								End If
							case "like"
								if typename( arr(1) ) = "String" then
									str =  key & " LIKE '" & arr(1) & "'"
								else
									call POP_MVC.error( Me.db_type & " parseWhere like")
								End If
							case "glob"
								if typename( arr(1) ) = "String" then
									str =  key & " GLOB '" & arr(1) & "'"
								else 
									call POP_MVC.error( Me.db_type & " parseWhere glob")
								End If
							case "exp"
								if typename( arr(1) ) = "String" then
									str =  key & " " & arr(1)
								else 
									call POP_MVC.error( Me.db_type & " parseWhere exp")
								End If
							case "between","[]"		' array("between", "1,3" ) OR array("between", array(1,3) )
								temp = arr(1)
								if tempType = "String" Then
									temp = split( arr(1) ) 
									if ubound( temp ) = 0 Then
										temp = split( arr(1) , "," )
									End If
								end if
								
								if isObject( arr(1) ) and is_array( arr(1) ) Then
									arr(1) = tool.obj2arr( arr(1) )
								End If
								
								if isArray( temp )  and ubound(temp)>0 Then
									str =  key & " BETWEEN " & temp(0) & " AND " & temp(1)
								else 
									call POP_MVC.error( Me.db_type & " parseWhere between")
								End If	
							case "not between","notbetween"
								temp = arr(1)
								if tempType = "String" Then
									temp = split( arr(1) ) 
									if ubound( temp ) = 0 Then
										temp = split( arr(1) , "," )
									End If
								end if
								
								if isObject( arr(1) ) and is_array( arr(1) ) Then
									arr(1) = tool.obj2arr( arr(1) )
								End If
								
								if isArray( temp )  and ubound(temp)>0 Then
									str =  key & " NOT BETWEEN " & temp(0) & " AND " & temp(1)
								else 
									call POP_MVC.error( Me.db_type & " parseWhere between")
								End If	
							case "daydiff"
								if tool.isMicroDB then
									str =  "DATEDIFF( 'd' , " & key & " , now ) <= " & arr(1)
								elseif db_type = "mysql" then
									str =  "DATEDIFF( now() , " & key & " ) <= " & arr(1)
								elseif db_type = "sqlite3" then
									str = "round(julianday(datetime('now','localtime')) - JULIANDAY(" & key & "),0) <= " & arr(1)
								end if
							case "hourdiff"
								if tool.isMicroDB then
									str =  "DATEDIFF( 'h' , " & key & " , now ) <= " & arr(1)
								elseif db_type = "mysql" then
									str =  "TIMESTAMPDIFF( HOUR, " & key & " , now()  ) <= " & arr(1)
								elseif db_type = "sqlite3" then
									str = "round(julianday(datetime('now','localtime')) - JULIANDAY(" & key & "),5)*24 <= " & arr(1)
								end if
							case "minutediff"
								if tool.isMicroDB then
									str =  "DATEDIFF( 'n' , " & key & " , now() ) <= " & arr(1)
								elseif db_type = "mysql" then
									str =  "TIMESTAMPDIFF( MINUTE, " & key & " , now()  ) <= " & arr(1)
								elseif db_type = "sqlite3" then
									str = "round(julianday(datetime('now','localtime')) - JULIANDAY(" & key & "),5)*1440 <= " & arr(1)
								end if
							case "minutegt"
								if tool.isMicroDB then
									str =  "DATEDIFF( 'n' , " & key & " , now ) > " & arr(1)
								elseif db_type = "mysql" then
									str =  "TIMESTAMPDIFF( MINUTE, " & key & " , now() ) > " & arr(1)
								elseif db_type = "sqlite3" then
									str = "round(JULIANDAY('now') - JULIANDAY(" & key & "),5)*60 > " & arr(1)
								end if
							case "date"
								if tool.isMicroDB then
									str =  "DATEDIFF( 'd' , " & key & " , '" & arr(1) & "' ) = 0"
								elseif db_type = "mysql" then
									str =  "datediff( " & key & " , '" & arr(1) & "' ) = 0"
								elseif db_type = "sqlite3" then
									str = "substr(  " & key & ",1,10) = substr( '" & POP_MVC.FormatDate(arr(1),"YYYY-MM-DD") & "' , 1,10 )"
								end if
						End Select
					end If
						
				' 至此 判断完 isArray
				elseif isObject( opts(key) ) Then
					str = ParseWhere( opts(key) )
					if trim(str) <> "" then
						str = "( " & str & " )"
					end if
				else					
					if Not isEmpty(options("table")) Then
						set ts = getTableStructure( options("table") )
					elseif Not isEmpty( tableName ) Then
						set ts = getTableStructure( tableName )
					end If
					if not isEmpty( ts ) Then
						temp = POP_MVC.Arr.iSearch( ts.keys, key )
						if temp > -1 Then
							if ( isnull(opts(key)) ) then
								str = key
							else
								arr = ts.keys
								str = key & " = " & getSqlStr( ts( arr(temp) ) ,opts(key) )
							end if							
						Else
							if ( isnull(opts(key)) ) then
								str = key
							else
								str = key & " = " & getSqlStr( VarType(opts(key)) ,opts(key) )
							end if							
						End If
					else
						if ( isnull(opts(key)) ) then
							str = key
						else
							str = key & " = " & getSqlStr( VarType(opts(key)) ,opts(key) ) 
						end if						
					end if					
				End If
				
				if trim(str) <> "" then
					if j = 0 then
						ParseWhere = ParseWhere & " " & str
					else
						ParseWhere = ParseWhere & " AND " & str
					end if
				end if
				j = j + 1

			next	' 循环对象opts			
			if POP_MVC.count(opts) > 1 then
				parseWhere = parseWhere & " ) "
			end if
			
		elseif isArray( opts ) Then
			
			bound = ubound(opts)
			if bound >= 0 then ParseWhere = ParseWhere & " ( "
			for i = 0 to bound
				if i = 0 then
					ParseWhere = ParseWhere &  ParseWhere( opts(i) )
				else
					ParseWhere = ParseWhere & " OR " & ParseWhere( opts(i) )
				end if
			next
			if bound >= 0 then ParseWhere = ParseWhere & " ) "
		End If
		
		ParseWhere = trim(ParseWhere)		
		if POP_MVC.String.StartsWith(ParseWhere,"AND") Then
			ParseWhere = mid( ParseWhere,4 )
		elseif POP_MVC.String.StartsWith(ParseWhere,"OR") Then
			ParseWhere = mid( ParseWhere,3 )
		End If			
		Call L_( Me.db_type & " ParseWhere" )