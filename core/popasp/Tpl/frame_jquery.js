function Db(){
	this.ajaxPath = '?c=Ajax__&a=';
	this.url = '';
}

Db.prototype.http = function(url,data,fn){
	var fd = new FormData();
	for( var attr in data ) {
		fd.append(attr,data[attr] );
	}
	url: this.url ? this.url : this.getUrl(url);
	if ( this.url ) {
		this.url = '';
	}	
	$.post( url , data, function(){
		if ( fn ) {
			fn();			
		}		
	} );
}

//根据条件查询多条
//params为对象，{  where:{}, field:'', page:''...  }
Db.prototype.find = function(table,options,fn){
	var url = this.ajaxPath + 'find';
	this.http( url, {'params':{'options':options}} , fn  );
};