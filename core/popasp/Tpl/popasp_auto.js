/* 
 * popasp_auto 1.0
 * Copyright (c) popasp
 * Date: 2019-4-18
 * 表单填充
*/  
var popasp_auto = function(objForm,auto) {
	var aErr = {};
	objForm = ( objForm===undefined ) ? document.forms[0] : objForm;
	auto = ( auto!==undefined ) ? auto : POPASP_AUTO.auto_data;
	
	
	return handle();
	
	function classof(o){
		if (null===o) return null;
		if (undefined===o) return undefined;
		return Object.prototype.toString.call(o).slice(8,-1);
	}
	
	//array(验证字段,验证规则,错误提示,[验证条件,附加规则,验证时间])
	function handle(  ){
		var key;
		
		for( key in auto ){
			try{
				setValue( key );
			} catch( err ) {
				aErr[key] = "无法自动填充,错误原因:" + err.message;
				continue;
			}
		}
		console.log( aErr );
	}
	
	//获取表单值
	function setValue( field ) {
		var value = auto[field];
		var obj =  objForm[field];
		var temp = null;
		if ( undefined === obj ) {
			aErr[ field ] = "表单中无此字段"
		}else if( classof( obj ).slice( -4 ).toLowerCase() != "list" ) {	//只有一个			
			switch( obj.tagName.toLowerCase() ) {
				case "input" :	//input标签
					switch( obj.type.toLowerCase() ){
						case "radio":		//radio	
							if( obj.checked ) {
								obj.value = value;
							}
							break;
						case "checkbox":	//checkbox
							if ( obj.value == value ) {
								obj.checked = "checked";
							}
							break;
						default:
							if ( obj.value == ''){	
								console.log(obj.value);
								obj.value = value;
							}
							
					}
					break;
				case "select":
					if( value === true ) {
						value = 1;
					} else if ( value === false ) {
						value = 0;	
					} else if ( value === null ) {
						value = "";	
					}					
					obj.value = value;
					//console.log(obj.getElementsByTagName('option')[0].value);
					if ( obj.value!=value ) {
						obj.value = "";
					}
					if ( jQuery ) {
						jQuery(obj).change();
					}					
					break;
				case "textarea":
					obj.value = value;
					break;
				default:
					obj.value = value;
			}
		} else {
			var j,checkedValue = null;
			for ( var i = 0 ; i< obj.length; i++ ) {
				switch( obj[i].tagName.toLowerCase() ) {
					case "input" :	//input标签
						
						switch( obj[i].type.toLowerCase() ){
							case "radio":		//radio		
								obj[i].checked = "";
								if ( obj[i].value == value ) {
									obj[i].checked = "checked";								
								}
								break;
							case "checkbox":	//checkbox		
								if (checkedValue === null ){
									checkedValue = value.split(/\||\,/);
								}								
								for ( j = 0 ; j < checkedValue.length; j++ ){
									if ( obj[i].value == checkedValue[j].replace(/(^\s+)|(\s+$)/g, "") ) {
										
										obj[i].checked = "checked";
									}
								}
								break;
							default:
								if ( obj[i].value == '') {
									obj[i].value = value;									
								}								
						}
						break;
					case "textarea":
					case "select":
						obj[i].value = value;
						if ( jQuery ) {							
							$(obj[i]).change();
						}						
						break;
					default:
						obj[i].value = value;
				}
				checkedValue = null;
			}
		}
	}
	
	function showMsg( key ) {
		if(POPASP_AUTO.SHOW_PAGE_TRACE){
			console.log( key +("字段值无法自动填充") );	
		}
	}
};