<%@LANGUAGE="VBSCRIPT" CODEPAGE="65001"%>
<%
Option Explicit
Server.ScriptTimeOut = 300
Response.Buffer = True  '设置输出缓冲区
Session.CodePage=65001
Response.Charset = "utf-8"  '设置输出缓冲区
Response.Expires = 0
Response.ExpiresAbsolute = Now() - 1
Response.CacheControl = "no-cache"
Response.AddHeader "Expires",Now() - 1
Response.AddHeader "Pragma","no-cache"
Response.AddHeader "Cache-Control","private, no-cache, must-revalidate"
%>
<!--#include file="popasp_mvc4bbs.class.asp" -->
<!--#include file="popasp.func.asp" -->
<!--#include file="popasp_string.class.asp" -->
<!--#include file="popasp_dictionary.class.asp" -->
<!--#include file="popasp_arr.class.asp" -->
<!--#include file="popasp_file.class.asp" -->
<!--#include file="popasp_url.class.asp" -->
<!--#include file="popasp_rejection.class.asp" -->
<!--#include file="popasp_aspbbs.class.asp" -->
<!--#include file="popasp_controller.class.asp" -->
<!--#include file="popasp_xmldb4bbs.class.asp" -->
<!--#include file="popasp_xml.class.asp" -->
<!--#include file="../Extend/bbs/Top.class.asp" -->
<!--#include file="../Extend/bbs/Common.class.asp" -->
<!--#include file="../Extend/bbs/Empty__.class.asp" -->
<%
Dim POP_MVC : Set POP_MVC = New POPASP_MVC4BBS '实例化POPASP_MVC
%>
<!--#include file="popasp.convention.asp" -->
<%'页面加载完毕后销毁POP_MVC实例%>
<script language="vbscript" runat="server">If TypeName(POP_MVC) = "POPASP_MVC4BBS" Then Call POP_MVC.show_page_trace() : Set POP_MVC = Nothing : set objCMS = nothing : set that = nothing</script>