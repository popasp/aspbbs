<%
''网站是否处于调试状态，开发中应设为1，上线应后设为0
POP_MVC.config("APP_DEBUG") = 1

''显示页面Trace信息
POP_MVC.config("SHOW_PAGE_TRACE") = 0

''***************Application应用设置 Start***************
'是否将程序文件（部分）存在application中,2.3版本开始使用POP_MVC.applicationOn代替POP_MVC.config("APPLICATION_ON")

''程序文件的部分可以存在application中，其键名前缀
POP_MVC.config("APPLICATION_PREFIX") = ""
''***************Application应用设置 End***************


''***************URL相关设置 Start***************

''URL访问模式支持 0 (普通模式); 1 (PATHINFO 模式); 
POP_MVC.config("URL_MODE") = 0

''PATHINFO模式下的参数分割符 
POP_MVC.config("URL_DEPR") = "_"

''PATHINFO模式下的匹配规则 POP_MVC.config("URL_RULE")

''***************URL相关设置 END***************

''***************数据库相关设置 Start***************

''数据库类型，目前支持access、sqlite3、Excel、sqlserver(中间无空格)、mysql、txt
POP_MVC.config("DB_TYPE") = "access"

''仅适用于sqlserver、mysql
POP_MVC.config("DB_HOST") = "(local)"

''仅适用于sqlserver、mysql
POP_MVC.config("DB_NAME") = ""

''仅适用于sqlserver、mysql
POP_MVC.config("DB_USER") = ""

''ACCESS的类型，仅当数据库为access时才起作用，目前支持2003(mdb)、2007(accdb)
POP_MVC.config("ACCESS_TYPE") = "2003"

''ODBC中sqlite3驱动名称，默认为SQLite3 ODBC Driver
POP_MVC.config("SQLITE3_DRIVER_NAME") = "SQLite3 ODBC Driver"

''access、sqlite3、excel、txt数据库文件的路径，如果有操作服务器的条件，最好将它放到网站根目录的上级目录
POP_MVC.config("DB_PATH") = "/popasp.mdb"

''数据库密码
POP_MVC.config("DB_PWD") = ""

''数据库端口，适用于Mysql(默认3306)与Sqlserver(默认1433)
POP_MVC.config("DB_PORT") = ""

''POP_MVC.rs2dict最多可以取多少行
POP_MVC.config("RS_2DICT_LIMIT") = 3000

''默认每页取多少条记录
POP_MVC.config("PAGE_PAGESIZE") = 10
''***************数据库相关设置 End***************

''扩展配置文件名，为空时不调用
POP_MVC.config("EXT_CONFIG") = ""

''***************上传相关设置 Start***************
''允许上传的类型,只允许上传图片jpg;jpeg;png;gif;bmp
''POP_MVC.config("UPLOAD_ALLOW_TYPES") = "*"

''上传的文件限制
''POP_MVC.config("UPLOAD_MAX_FILESIZE") = 10*1024*1024

''上传限制
''POP_MVC.config("UPLOAD_MAX_SIZE") = 100*1024*1024

''上传路径，为空则默认为/Upload/yyyymmdd/
''POP_MVC.config("UPLOAD_SAVE_PATH") = ""

''水印图片写路径，水印文件写文字即可，留空则不进行水印处理
POP_MVC.config("WATERMARK_PATH") = "POPASP框架给大家一个留恋ASP的理由"

''水印位置：1顶部居左、2顶部居中、3顶部居右、4左边居中、5图片中心、6右边居中、7底部居左、8底部居中、9底部居右
''其他数字值为随机位置
POP_MVC.config("WATERMARK_POSITION") = 9


''生成图片后的最大宽度
POP_MVC.config("WATERMARK_WIDTH") = 1000

''生成图片后的最大高度
POP_MVC.config("WATERMARK_HEIGHT") = 1000

''水印位置透明度
POP_MVC.config("WATERMARK_ALPHA") = 100
''***************上传关设置 End***************


''***************模板引擎相关设置 Start***************
''数据缓存，单位为秒，默认为1天
''POP_MVC.config("TMPL_CACHE_LIFETIME") = "86400"

''数据缓存，默认存放的文件夹，如果为空，则存放到 Runtime/Cache
''POP_MVC.config("DATA_CACHE_FOLDER") = ""

''默认成功跳转对应的模板文件，如果设为空字符串，会自动调用系统的跳转文件
''POP_MVC.config("TMPL_ACTION_SUCCESS") = ""

''默认错误跳转对应的模板文件，如果设为空字符串，会自动调用系统的跳转文件
''POP_MVC.config("TMPL_ACTION_ERROR") = ""

''模板引擎的左定界符
POP_MVC.config("TMPL_L_DELIM") = "{"

''模板引擎的右定界符	
POP_MVC.config("TMPL_R_DELIM") = "}"

''模板文件名的后缀	
POP_MVC.config("TMPL_TEMPLATE_SUFFIX") = ".html"

''模板布局文件的替换字符串
POP_MVC.config("TMPL_LAYOUT_LABEL") = "{__CONTENT__}"

''不需要模板布局的替换标签
POP_MVC.config("TMPL_NOLAYOUT_LABEL") = "{__NOLAYOUT__}"

''是否自动引入模板布局文件
POP_MVC.config("TMPL_LAYOUT_ON") = 0

''模板布局文件
POP_MVC.config("TMPL_LAYOUT_FILE") = "layout"

''V_函数的变量名之间的分隔符，默认为.
POP_MVC.config("TMPL_VARNAME_SEPARATOR") = "."

''在assign分配变量时，that.assign "key",array(rs,prefix,blcase)对应要去掉的前缀，多个用英文逗号分隔
POP_MVC.config("TMPL_ASSIGN_RS_PREFIX") = ""

''在assign分配变量时，that.assign "key",array(rs,prefix,blcase)对应是否转为小写
POP_MVC.config("TMPL_ASSIGN_RS_BLCASE") = false

''页面缓存，单位为秒，默认为14天
POP_MVC.config("PAGE_CACHE_LIFETIME") = "1209600"

''页面缓存，默认存放的文件夹，如果为空，则存放到 Runtime/Page
POP_MVC.config("PAGE_CACHE_FOLDER") = ""

''默认成功时的表情符号
POP_MVC.config("TMPL_SUCCESS_FACE") = ":)"

''默认失败时的表情符号
POP_MVC.config("TMPL_ERROR_FACE") = ":("
''***************模板引擎相关设置 End***************


''***************URL设置 Start***************

''阻止页面跳转，即是否允许that.success、that.error进行页面跳转
POP_MVC.config("STOP_PAGE_JUMP") = 0

''在使用A_("ctrl/act")时，如果没有act方法，是否去执行父类的act方法，默认执行
POP_MVC.config("EXEC_PARENT_SAME_ACTION") = 1

''空操作模块名
POP_MVC.config("EMPTY_MODULE") = "Empty__"

''空操作方法名
POP_MVC.config("EMPTY_ACTION") = "Empty_"

''系统操作模块名，默认名为system后加双下划线，项目中不能有重名
''若需修改此名称，同时要修改 popasp/tpl/System__Action.class.asp文件的名称与内容中的类名
POP_MVC.config("SYSTEM_MODULE") = "System__"

''系统操作默认方法名
POP_MVC.config("SYSTEM_ACTION") = "index"

''默认控制器名
POP_MVC.config("DEFAULT_MODULE") = "Index"

''默认操作名
POP_MVC.config("DEFAULT_ACTION") = "index"

''控制器的initialize操作方法名
POP_MVC.config("INITIALIZE_ACTION") = "initialize"

''前置操作名的前缀,不区分大小写
POP_MVC.config("BEFORE_ACTION_PREFIX") = "before__"

''后置操作名的前缀,不区分大小写
POP_MVC.config("AFTER_ACTION_PREFIX") = "after__"

''工具类的文件夹
POP_MVC.config("PLUGIN_PATH") = "./Tool/"

''项目扩展(二次开发)控制器、模板、模型所用到的分隔符
POP_MVC.config("EXTEND_SEPARATOR") = "--"

''项目扩展(二次开发)的文件夹
POP_MVC.config("EXTEND_PATH") = "./Extend"

'默认插件名
''POP_MVC.config("DEFAULT_EXTEND") = ""

'是否继承默认插件的配置
POP_MVC.config("INHERIT_DEFAULT_EXTEND") = 1

''项目扩展(二次开发)的默认插件名
'POP_MVC.config("EXTEND_PATH") = ""

''***************URL设置 End***************

''***************POPASP_SELF_OBJECT设置 Start***************
''验证码在session中的名字
POP_MVC.config("POPASP_SELF_OBJECT_PREFIX") = "POPASP_SELF_OBJECT_"



''***************SESSION设置 Start***************
''验证码在session中的名字
POP_MVC.config("SESSION_VERIFY") = "verify"

''***************COOKIE设置 Start***************

''cookie 仅送往到达该域的请求。空值则ASP自动取当前域名
POP_MVC.config("COOKIE_DOMAIN") = ""

''cookie 的失效日期。如果没有规定日期，cookie 会在 session 结束时失效。以秒计，默认为86400秒，即一天
POP_MVC.config("COOKIE_EXPIRES") = "86400"

''cookie 过期时间的计算单位。"s"为秒，"n"为分钟， "h"为小时，"d"为天
POP_MVC.config("COOKIE_EXPIRES_UNIT") = "s"

''如果设置，cookie 仅送往到达此路径的请求。如果没有设置，则使用应用程序的路径。
POP_MVC.config("COOKIE_PATH") = "/"

''指示 cookie 是否安全。
POP_MVC.config("COOKIE_SECURE") = "False"

''***************COOKIE设置 End***************

'' 控制器的简写
POP_MVC.config("VAR_CONTROLLER") = "that"

'' 默认的AJAX提交变量
POP_MVC.config("VAR_AJAX_SUBMIT") = "ajax"

''默认模块获取变量
POP_MVC.config("VAR_MODULE") = "c"

''默认操作获取变量
POP_MVC.config("VAR_ACTION") = "a"

''页码切换变量名
POP_MVC.config("VAR_PAGE") = "page"

''每页显示多少个条记录变量名
POP_MVC.config("VAR_PAGESIZE") = "pagesize"

''rs位移
POP_MVC.config("VAR_POSITION") = "position"

''***************其他设置 Start***************

POP_MVC.config("MVC_COPYRIGHT") = "<p><a title='官方网站' target='_blank' href='http://www.popasp.com'>POPASP</a><sup><script>echo(json.version);</script></sup> { Fast &amp; Simple OOP ASP Framework } -- [ POPASP给asper一个留恋asp的理由 ]</p>"

'popasp_auto扩展的路径，如果为空，默认取popasp/Module
POP_MVC.config("AUTO_EXTENDS_PATH") = ""

'popasp_auto扩展的路径，用英文逗号分隔名称
POP_MVC.config("AUTO_EXTENDS_MODULE") = "mood"

'auto自动化生成的文件夹名
POP_MVC.config("AUTO_FOLDER_NAME") = "__popasp__"
''***************其他设置 End***************
%>