<%
' +----------------------------------------------------------------------
' | POPASP [ ASP MVC ]
' +----------------------------------------------------------------------
' | Copyright (c) 2016 http://popasp.com All rights reserved.
' +----------------------------------------------------------------------
' | Author: popasp <1737025626@qq.com>
' +----------------------------------------------------------------------



' 输出变量
Sub var_export ( arg )
	dim item,arr,stype,dict,key
	stype = typename(arg)
	'Recordset或Dictionary类型
	if stype = "Byte()" then
		Response.BinaryWrite arg
	elseif stype = "Field" then
		var_export arg.value
	elseif isArray(arg) or stype = "Recordset" OR stype = "Dictionary" or POP_MVC.String.StartsWith(stype , POP_MVC.config("POPASP_SELF_OBJECT_PREFIX")) then
		Response.write js_encode( arg )
	'IRequestDictionary类型，如Request.ServerVariables、Request.QueryString、Request.Form等
	elseif  stype = "IRequestDictionary" then
		set dict = Server.CreateObject("Scripting.Dictionary")
		for each key in arg
			dict.add key,arg(key)
		next
		call var_export(dict)
		set dict = nothing
	'标量
	elseif POP_MVC.isScalar(arg) OR stype = "IStringList" then
		Response.write(arg)
	'POPASP_开头的框架类
	elseif POP_MVC.String.iStartsWith(stype,"popasp_")  then
		set dict = POP_MVC.import("POPASP_REJECTION" ).Parse(stype)
		POP_MVC.Dict.Unshift dict,"typename",stype
		var_export dict
		set dict = nothing
	elseif stype = "IXMLDOMElement" then
		var_export POP_MVC.String.encodehtml(arg.xml)		
	elseif stype = "IXMLDOMSelection" then
		for each item in arg
			POP_MVC.Arr.push arr,POP_MVC.String.encodehtml(item.xml)
		next
		var_export arr
	'控制器Action类
	elseif POP_MVC.String.iEndsWith(stype,"action") and POP_MVC.File.isFile(POP_MVC.get_ctrl_path( left(stype, len(stype)-6 ) )) then
		set dict = POP_MVC.import("POPASP_REJECTION" ).Parse(stype)
		POP_MVC.Dict.Unshift dict,"typename",stype
		var_export dict
		set dict = nothing
	'数据模型Model类
	elseif POP_MVC.String.iEndsWith(stype,"model") then
		set dict = POP_MVC.import("POPASP_REJECTION" ).Parse( stype )
		POP_MVC.Dict.Unshift dict,"typename",stype
		var_export dict
		set dict = nothing
	'正则
	elseif stype = "ISubMatches" then		
		for each item in arg
			POP_MVC.arr.push arr,item
		next
		var_export arr
	'正则
	elseif stype = "IMatch2" then
		Response.write(arg)
	'正则
	elseif stype = "IMatchCollection2" then
		for each item in arg
			POP_MVC.arr.push arr,CStr( item )
		next
		var_export arr
	elseif stype = "Unknown" then
		Response.write("<span style='color:red;font-weight:bold;'> 未知类型Unknown </span>")
	else
		Response.write("<span style='color:red;font-weight:bold;'>不能解析的类型" & typename(arg) &  "</span>")
	end if
End Sub


'将变量输出到文件
Sub file_var_export (  path , arg  )
	dim item,arr,stype,dict,key
	stype = typename(arg)
	if stype = "Field" then
		call file_var_export( path , arg.value )
	'Recordset或Dictionary类型
	elseif isArray(arg) or stype = "Recordset" OR stype = "Dictionary" or POP_MVC.String.StartsWith(stype , POP_MVC.config("POPASP_SELF_OBJECT_PREFIX")) then
		Call file_var_export( path , js_encode( arg ) )
	'IRequestDictionary类型，如Request.ServerVariables、Request.QueryString、Request.Form等
	elseif  stype = "IRequestDictionary" then
		set dict = Server.CreateObject("Scripting.Dictionary")
		for each key in arg
			dict.add key,arg(key)
		next
		call file_var_export( path , dict )
		set dict = nothing
	'标量
	elseif POP_MVC.isScalar(arg) OR stype = "IStringList" then
		Call POP_MVC.file_append_contents( path ,  arg & vbCrLf )
	elseif stype = "IXMLDOMElement" then
		Call POP_MVC.file_append_contents( path ,  arg.xml & vbCrLf )
	elseif stype = "IXMLDOMSelection" then
		for each item in arg
			POP_MVC.Arr.push arr,item.xml
		next
		call file_var_export( path , arr )
	'POPASP_开头的框架类
	elseif POP_MVC.String.iStartsWith(stype,"popasp_")  then
		set dict = POP_MVC.import("POPASP_REJECTION" ).Parse(stype)
		POP_MVC.Dict.Unshift dict,"typename",stype
		call file_var_export( path , dict )
		set dict = nothing
	'控制器Action类
	elseif POP_MVC.String.iEndsWith(stype,"action") and POP_MVC.File.isFile(POP_MVC.get_ctrl_path( left(stype, len(stype)-6 ) )) then
		set dict = POP_MVC.import("POPASP_REJECTION" ).Parse(stype)
		POP_MVC.Dict.Unshift dict,"typename",stype
		call file_var_export( path , dict )
		set dict = nothing
	'数据模型Model类
	elseif POP_MVC.String.iEndsWith(stype,"model") and POP_MVC.File.isFile(POP_MVC.getModelDstPath( left(stype, len(stype)-5 ) )) then
		set dict = POP_MVC.import("POPASP_REJECTION" ).Parse(left(stype, len(stype)-5 ))
		POP_MVC.Dict.Unshift dict,"typename",stype
		call file_var_export( path , dict )
		set dict = nothing
	'正则
	elseif stype = "ISubMatches" then		
		for each item in arg
			POP_MVC.arr.push arr,item
		next
		call file_var_export( path , arr )
	'正则
	elseif stype = "IMatch2" then
		call file_var_export( path , arg )
	'正则
	elseif stype = "IMatchCollection2" then
		for each item in arg
			POP_MVC.arr.push arr,CStr( item )
		next
		call file_var_export( path , arr )
	else
		call file_var_export( path , "<span style='color:red;font-weight:bold;'>不能解析的类型" & typename(arg) &  "</span>" )
	end if
End Sub

'gb2312转utf8
Function gb2utf( ByVal str )
	Response.Charset = "GB2312"
	gb2utf = iconv( "GB2312" , "UTF-8" , str )
	gb2utf = Replace(gb2utf, "^\uFEFF", "" , 1,1,0)
	Response.Charset = "utf-8"
End Function

'utf8转gb2312
Function utf2gb( ByVal str )
	str = Replace(str, "^\uFEFF", "" , 1,1,0)
	utf2gb = iconv( "UTF-8" , "GB2312" , str )
End Function

'用法：iconv("UTF-8","GB2312","这是gbk编码") 'UTF-8转GB2312
Function iconv(InCharset,OutCharset,OutStr)

    Dim File

    Set File = server.CreateObject("Adodb.Stream")

    File.Charset = InCharset

    File.Mode = 3

    File.Open

    File.Type = 2

    File.Position = 0

    File.WriteText OutStr

    File.Position = 0

    File.Charset = OutCharset

    iconv = File.ReadText

End Function

'//三目运算,表达式，返回值1，返回值2
Function iif(a,b,c)
	If POP_MVC.import("POPASP_CONTROLLER" ).get_suc_bool( a ) then
		iif = b
	else
		iif = c
	end if
End Function

Function iftrue( a,b )
	If POP_MVC.import("POPASP_CONTROLLER" ).get_suc_bool( a ) then
		iftrue = b
	end if
End Function

'依现有的请求参数，重建请求字符串
'rewriteDict，是要重写的参数，应为Dictionary对象，如果为数组，那么Array( key,value )，当然也可以为 k1=v1&k2=v2 这样的字符串
'filterArr，是要过滤的参数，应为数组，如果为字符串，请以英文逗号分隔
Function http_build_query( rewriteDict, filterArr )
	dim str,key,rewriteBool,filterBool,filterStr,keys,tempStr,dict,arr,i,temp,lkey
	
	if POP_MVC.config("URL_MODE") = 0 then
		set POP_MVC.dQuery = POP_MVC.get2dict
	end if
	
	rewriteBool = false
	if typename( rewriteDict ) = "Dictionary" then
		rewriteBool = 1
		keys = join( rewriteDict.keys , "," )
		keys = LCase( keys ) & ","
	elseif isArray( rewriteDict ) then
		if ubound( rewriteDict ) > 0 then
			rewriteBool = -1
		end if
	elseif TypeName( rewriteDict ) = "String" then
		set dict = D_
		arr = split( rewriteDict , "&" )

		for i = 0 to ubound(arr)
			temp = split( arr(i) , "=" , 2 )
			lkey = LCase( temp(0) )
			dict( lkey ) = temp(1)
			keys =  keys & lkey & ","
		next
		set rewriteDict = dict
		rewriteBool = 1
	end if

	filterBool = false
	if isArray( filterArr ) then
		filterBool = true
		filterStr = join( filterArr,"," ) & ","
	elseif typename( filterArr ) = "String" then
		filterBool = true
		filterStr = filterArr & ","
	end if
	
	str = ""
	
	if filterBool then
		if inStr( filterStr , POP_MVC.config("VAR_MODULE") & "," ) < 1 then
			str = str & POP_MVC.config("VAR_MODULE") & "=" & POP_MVC.c
		end if
		
		if inStr( filterStr , POP_MVC.config("VAR_ACTION") & "," ) < 1 then
			str = str & "&" & POP_MVC.config("VAR_ACTION") & "=" & POP_MVC.a
		end if		
	end if

	for each key in POP_MVC.dQuery
		if key <> POP_MVC.config("VAR_MODULE") and key <> POP_MVC.config("VAR_ACTION") and inStr( filterStr , key ) < 1 then
			if rewriteBool = 1 then
				if inStr(keys , key & "," ) then
					tempStr = key & "=" & rewriteDict(key)
					keys = replace( keys , key & "," , "" )
					if keys = "" then
						rewriteBool = false
					end if
				else
					tempStr = key & "=" & POP_MVC.dQuery(key)
				end if
			elseif rewriteBool = -1 then
				if key = LCase( rewriteDict(0) ) then
					tempStr = key & "=" & rewriteDict(1)
					rewriteBool = false
				end if
			else
				tempStr = key & "=" & POP_MVC.dQuery(key)
			end if
			str = str & "&" & tempStr
		end if
	next
	
	if keys <> "" then
		for each key in rewriteDict
			if inStr( filterStr , key & "," ) < 1 and keys <> "" then
				str = str & "&" & key & "=" & rewriteDict(key)
				keys = replace( keys , key & "," , "" )
			end if
		next
	end if	
	
	str = POP_MVC.String.ltrim( str, "&" )
	
	http_build_query = str
End Function

'依现有的请求参数，重建请求字符串
'rewriteDict，是要重写的参数，应为Dictionary对象，如果为数组，那么Array( key,value )，当然也可以为 k1=v1&k2=v2 这样的字符串
'filterArr，是要过滤的参数，应为数组，如果为字符串，请以英文逗号分隔
Function query2query( queryStr,rewriteDict, filterArr )
	dim str,key,rewriteBool,filterStr,keys,tempStr,dict,arr,i,temp,lkey,data,tempArr,filterBool
	
	set data = D_
	arr = split( queryStr , "&")	
	for i = 0 to ubound(arr)
		tempArr = split( arr(i) , "=" )
		data( tempArr(0) ) = tempArr(1)
	next
	
	rewriteBool = false
	if typename( rewriteDict ) = "Dictionary" then
		rewriteBool = 1
		keys = join( rewriteDict.keys , "," )
		keys = "," & LCase( keys ) & ","
	elseif isArray( rewriteDict ) then
		if ubound( rewriteDict ) > 0 then
			rewriteBool = -1
		end if
	elseif TypeName( rewriteDict ) = "String" then
		set dict = D_
		arr = split( rewriteDict , "&" )
		for i = 0 to ubound(arr)
			temp = split( arr(i) , "=" , 2 )
			dict( temp(0) ) = temp(1)
			keys = keys & LCase( temp(0) ) & ","
		next		
		if keys <> "" then
			keys = "," & keys
		end if
		set rewriteDict = dict
		rewriteBool = 1
	end if

	str = ""
	
	filterBool = false
	if not isArray( filterArr ) then
		filterArr = split( filterArr , "," )
	end if
	if isArray( filterArr ) then
		filterBool = true
		filterStr = "," & join( filterArr,"," ) & ","
	elseif typename( filterArr ) = "String" then
		filterBool = true
		filterStr = "," & filterArr & ","
	end if
	
	set data = POP_MVC.dict.iMerge( data ,rewriteDict  )	

	Call POP_MVC.Dict.iSubtract(data,filterArr)
	
	for each key in data
		str = str & "&" & key & "=" & data(key)
	next
	
	if str <> "" then
		str = mid(str, 2)
	end if
	query2query = str
End Function

' 控制器的跳转
Sub A_(url)
	Call POP_MVC.Action(url)
End Sub

'得到模型对象
Function M_( ByVal modelName )
	Set M_ = POP_MVC.Model( modelName )
End Function

'得到模型对象
Function K_( ByVal modelName )
	Set K_ = POP_MVC.MethodModel( modelName )
End Function

'得到模型对象
Function B_( ByVal tableName )
	Set B_ = POP_MVC.db( tableName )
End Function

'得到模型对象
Function X_( ByVal path )
	Set X_ = P_("model").CreateXML( path )
End Function

'得到模型对象
Function XM_( ByVal path )
	Set XM_ = P_("model").CreateXMLModel( path )
End Function

'获取/设置属性值，并非 Cookie,而是POP_MVC.config
Function C_(key)
	if isArray( key ) Then
		if typename( key(1) ) = "Dictionary" then
			set POP_MVC.config(key(0)) = key(1)
		else
			POP_MVC.config(key(0)) = key(1)
		end if		
	elseif TypeName(key)="Dictionary" Then	'如果分配的是Dictionary对象则设置config
		set POP_MVC.config = POP_MVC.dict.Merge(POP_MVC.config,key)
	else
		C_ = POP_MVC.getConfig(key)
	End If
End Function

'读取/设置session
Function S_(key)
	dim i,rs,dict,k
	if isArray( key ) Then		'如果为数组，第一个元素是键名，第二个元素是值
		if isNull(key(1)) Then	'如果第二个元素为null,则删除该键值对
			call session.Contents.Remove( key(0) )
		elseif typename(key(1)) = "Recordset" Then
			set session( key(0) ) = POP_MVC.rs2dict( key(1) )
		elseif typename(key(1)) = "Dictionary" Then
			set session( key(0) ) = key(1)
		else
			if isObject( key(1) )  Then
				set session( key(0) ) = key(1)
			else 
				session( key(0) ) = key(1)
			End If
		End If		
	elseif typename(key) = "Recordset" Then
		set rs = key
		for i = 0 to rs.fields.count-1
			session( rs.fields(i).Name ) = rs( rs.fields(i).Name )		
		next
	elseif typename(key) = "Dictionary" Then
		set dict = key
		for each k in dict
			if isNull( dict(k) ) then
				call session.Contents.Remove( k )
			else
				session( k ) = dict( k )
			end if
		next		
	elseif isNull(key) then
		session.Contents.RemoveAll	
	else
		if isObject( session(key) ) Then
			set S_ = session(key)
		else
			S_ = session(key)
		end If
	End If
End Function


'创建Dictionary对象
Function D_()
	Set D_ = POP_MVC.CreateDict()
End Function

' 实例化MVC的核心类
Function P_( args )
	dim ctrl,key
	key = ""
	if isArray( args ) then
		ctrl = args(0)
		if ubound( args ) > 0 then
			key = args(1)
		end if
	else
		ctrl = args		
	end if
	ctrl = UCase(ctrl)
	if left(ctrl,7) <> "POPASP_" Then
		ctrl = "POPASP_" & ctrl 
	End If
	if key = "" then
		Set P_ = POP_MVC.import( ctrl )
	else
		Set P_ = POP_MVC.import_with_key( ctrl , "*" & key )
	end if
End Function

' 实例化插件类
Function T_( ctrl )
	Set T_ = POP_MVC.import_plugin( ctrl )
End Function

Function V_( byref var_name )
	on error resume next
	dim arr,str,i,bound,stype,bool,prefix,suffix,arr2
	if typename( var_name ) = "String" then		'如果变量名是字符串，则获取值
		if inStr( var_name, POP_MVC.config("TMPL_VARNAME_SEPARATOR") ) > 0 and NOT POP_MVC.tpl_vars.exists(var_name) then
			str = "(""" & Replace( var_name, POP_MVC.config("TMPL_VARNAME_SEPARATOR") , """)(""" ) & """)"
			arr2 = split( var_name , POP_MVC.config("TMPL_VARNAME_SEPARATOR") , 2 )
			prefix = arr2(0) : suffix = arr2(1)
			if prefix = "_GET" then
				str = "V_ = POP_MVC.get(""" & suffix & """)"
				Execute( str )	
			elseif prefix = "_FORM" then
				str = "V_ = POP_MVC.Form(""" & suffix & """)"
				Execute( str )	
			elseif prefix = "_REQ" then
				str = "V_ = POP_MVC.Req(""" & suffix & """)"
				Execute( str )
			elseif prefix = "_SERVER" then
				str = "V_ = Request.ServerVariables(""" & suffix & """)"
				Execute( str )	
			elseif prefix = "C_" OR prefix = "_C" then
				str = "V_ = POP_MVC.Config(""" & suffix & """)"
				Execute( str )
			elseif prefix = "S_" OR prefix = "_S" then
				str = "V_ = S_(""" & arr2(1) & """)"
				Execute(str)
			elseif prefix = "_COOKIE" then
				str = "V_ = Request.Cookies(""" & suffix & """)"
				Execute( str )
			else
				arr = split( var_name,POP_MVC.config("TMPL_VARNAME_SEPARATOR") )
				stype = typename( POP_MVC.tpl_vars(arr(0)) )
				if POP_MVC.String.StartsWith( stype , "POPASP_SELF_OBJECT" ) then

					str = "V_ = POP_MVC.tpl_vars(arr(0))." & arr(1)
				else
					str = "V_ = POP_MVC.tpl_vars" & str						
				end if
				Execute( str )
				if isEmpty(V_) then
					Call POP_MVC.Warning("不能正确解析变量“" & var_name & "”，变量未分配或变量名不正确（请注意大小写）")
				end if				
			end if
		else
			if POP_MVC.tpl_vars.Exists( var_name ) then	'如果tpl_vars含有该键名
				if typename( POP_MVC.tpl_vars(var_name))="Dictionary" OR  typename( POP_MVC.tpl_vars(var_name))="Recordset" then
					set V_ =  POP_MVC.tpl_vars(var_name)					
				elseif isObject(  POP_MVC.tpl_vars(var_name) ) then
					set V_ = POP_MVC.tpl_vars(var_name)
				else
					V_ = POP_MVC.tpl_vars(var_name)
				end if
			elseif var_name = "__APP__" then
				V_ = APP__
			end if		
		end if
	elseif isArray( var_name ) then		'如果变量名是数组，则分配值
		bound = ubound( var_name )
		if bound > 0 then	'如果数组中含两个元素，则第一个是键名，第二个是值，分配给POP_MVC.tpl_vars
			stype = typename(var_name(1))
			if stype = "Dictionary" or stype = "Recordset" then
				if inStr( var_name(0), POP_MVC.config("TMPL_VARNAME_SEPARATOR") ) > 0 then '如果变量名中含有分隔符
					str = "(""" & Replace( var_name(0), POP_MVC.config("TMPL_VARNAME_SEPARATOR") , """)(""" ) & """)"
					str = "set POP_MVC.tpl_vars" & str & " = " & var_name(1)
					Execute( str )
				else
					set POP_MVC.tpl_vars( var_name(0) ) = var_name(1)
				end if
			else
				if inStr( var_name(0), POP_MVC.config("TMPL_VARNAME_SEPARATOR") ) > 0 then '如果变量名中含有分隔符
					str = "(""" & Replace( var_name(0), POP_MVC.config("TMPL_VARNAME_SEPARATOR") , """)(""" ) & """)"
					str = "POP_MVC.tpl_vars" & str & " = " & var_name(1)
					Execute( str )
				else
					POP_MVC.tpl_vars( var_name(0) ) = var_name(1)
				end if
			end if
		end if
	end if
	Call L_("")
end function

'是否为空
Function isNul(str)
	if isArray(str) then 
		if ubound( str ) > - 1 then
			isNul = False
		else
			isNul = True
		end if
		exit Function
	end if
	if isnull(str) or str=""  then isNul=true else isNul=false
End Function	

' 记录和统计时间（微秒）和内存使用情况
' 使用方法:
' <code>
' G_("begin") ' 记录开始标记位
' ' ... 区间运行代码
' G_("end") ' 记录结束标签位
' Response.Write G_( array("begin","end",3) ) ' 统计区间运行时间 精确到小数后3位
' </code>
' @param string start 开始标签 args(0)
' @param string end 结束标签 args(1)
' @param integer dec 小数位 args(2)
' @return mixed
Function G_( args )
	on error resume next
	if isArray(args) Then
		if isEmpty( POP_MVC.dG_( args(1) ) ) OR isEmpty( POP_MVC.dG_( args(0) ) ) Then
			G_ = "-"
		else
			G_ = POP_MVC.dG_( args(1) ) - POP_MVC.dG_( args(0) )
			
			if ubound(args) > 1 then
				G_ = FormatNumber(G_,args(2),-1,0,0)
			else
				G_ = FormatNumber(G_,2,-1,0,0)
			end if
		end if
	else
		POP_MVC.dG_( args ) = timer()
	end if
End Function

Function F_( args )
	if is_empty( C_("SHOW_PAGE_TRACE") ) then
		exit Function
	end if
	POP_MVC.dFlow.add POP_MVC.dFlow.count,args
End Function

Function L_( str )
	'Exit Function
	if err.Number <> 0 Then		
		if str<>"" Then
			L_ =  Now & " " & err.Number & " [" & str  & "] " & " [" & err.Source & "] " & "[" & err.description & "]" 
		else
			L_ =  Now & " " & err.Number & " [" & err.Source & "] " & "[" & err.description & "]" 
		end if
		if POP_MVC.isAjax then
			P_("POPASP_LOG").write( L_ )
		else
			if C_("ERROR_LOG") then
				P_("POPASP_LOG").write( L_ )
			end if
			POP_MVC.dL_( POP_MVC.dL_.count + 1 ) = L_
		end if	

		err.clear
	else
		L_ = 0
	End If	
End Function

' 判断类中是否有该方法
Function method_exists( className,method )
	method_exists = POP_MVC.import("POPASP_REJECTION" ).method_exists(className,method)
End Function

' 执行自定义sub过程
Sub call_user_sub( byval func_name,byval args )
	dim item,str
	
	if typename(args) = "Dictionary" then
		args = args.items
	end if

	if not isArray(args) then
		args = array(args)
	end if
	
	str = ""
	for each item in args
		if TypeName(item) = "String" Then
			str = str & "," & """" & item & """"
		else
			str = str & "," & item
		end if
	next
	str = POP_MVC.ltrim(str,",")
	if str <> "" then
		Execute "Call " & func_name & "( " & str & " )"
	else
		Execute "Call " & func_name & "()"
	end if
End Sub

' 返回自定义函数的Execute字符串
Function call_user_func( byVal var_name,byval func_name,byval args )
	dim item,str
	
	if typename(args) = "Dictionary" then
		args = args.items
	end if
	
	if not isArray(args) then
		args = array(args)
	end if
	
	str = ""
	for each item in args
		if TypeName(item) = "String" Then
			str = str & "," & """" & item & """"
		else
			str = str & "," & item
		end if
	next
	str = POP_MVC.ltrim(str,",")
	var_name = trim(var_name)
	var_name = POP_MVC.rtrim(var_name,"=")
	
	if str <> "" then
		call_user_func = var_name & " = " & func_name & "( " & str & " )"
	else
		call_user_func = var_name & " = " & func_name & "()"
	end if
end function

' 向文件中写入js
Function js_put_contents(byref filePath,byref oJson)
	dim content: content = js_encode( oJson )
	js_put_contents = POP_MVC.file_put_contents(filePath,content)
End Function

' 从文件中读出js字符串
Function js_get_contents(filePath)
	dim obj
	js_get_contents = POP_MVC.file_get_contents( filePath )
	js_get_contents = mid( js_get_contents,7,len(js_get_contents)-10 )
End Function

Function file_get_contents( filePath )
	file_get_contents = POP_MVC.file_get_contents(filePath)
End Function



' 将json解析成Dictionary对象，可以处理数组、Dictionary、Recordset类型
Function js_encode( byref data )
	dim key,dict,startTime : startTime = timer()
	dim i
	if typename( data ) = "Recordset" then
		set POP_MVC.import("POPASP_JSON").data = POP_MVC.rs2dict( data )
	elseif typename(data) = "Dictionary" then
		set POP_MVC.import("POPASP_JSON").data = data
	elseif POP_MVC.String.StartsWith(typename(data) , POP_MVC.config("POPASP_SELF_OBJECT_PREFIX")) then
		set dict = Server.CreateObject("Scripting.Dictionary")
		for i = 0 to ubound( data.vars__ )
			dict.add data.vars__(i),data.get__( data.vars__(i) )
		next
		set POP_MVC.import("POPASP_JSON").data = dict
	elseif typename(data) = "IRequestDictionary" then
		set dict = Server.CreateObject("Scripting.Dictionary")
		for each key in data 
			dict.add key,data(key)
		next
		set POP_MVC.import("POPASP_JSON").data = dict
	else
		POP_MVC.import("POPASP_JSON").data = data
	end if
	js_encode = POP_MVC.import("POPASP_JSON").JSONoutput
	
	call POP_MVC.pushTime( startTime , "js_encode ( length : " & byte2size(len(js_encode)) & ")" )
End Function

Function js_decode( byVal str )	
	dim startTime : startTime = timer()
	if AscB(MidB(str,1,1))=255 and AscB(MidB(str,2,1))=254 Then			
		str = MidB( str, 3 )
	End If
	
	set POP_MVC.import("POPASP_JSON").data = D_
	call POP_MVC.import("POPASP_JSON").loadJSON(str)	
	set js_decode = POP_MVC.import("POPASP_JSON").data
	call POP_MVC.pushTime( startTime , "js_decode ( length : " & byte2size( len(str) ) & ")" )
End Function


'将byte显示为KB、MB等格式
'如	size = 1024;返回1KB
Function byte2size( size )
	dim i,arr
	size = CDbl(size)    
    if ( 0 = size ) Then
		byte2size = 0
		exit function
	End If
	arr = array("Bytes", "KB", "MB", "GB")
    i = Int( log(size) / log(1024) )
    byte2size = round((size / 1024^i) , 2) & " " & arr(i)
End Function

' Strip HTML and ASP tags from a string 
Function strip_tags(byval fString) 
	Dim re 
	Set re = New RegExp 
	re.IgnoreCase = True 
	re.Global = True
	re.Pattern = "<(.[^>]*)>" 
	fString = re.Replace(fString, "") 
	Set re = Nothing 
	strip_tags = fString 
End Function 

'获取客户端IP地址
Function get_client_ip()  
	Dim strIPAddr  
	If Request.ServerVariables("HTTP_X_FORWARDED_FOR") = "" OR InStr(Request.ServerVariables("HTTP_X_FORWARDED_FOR"), "unknown") > 0 Then  
	 strIPAddr = Request.ServerVariables("REMOTE_ADDR")  
	ElseIf InStr(Request.ServerVariables("HTTP_X_FORWARDED_FOR"), ",") > 0 Then  
	 strIPAddr = Mid(Request.ServerVariables("HTTP_X_FORWARDED_FOR"), 1, InStr(Request.ServerVariables("HTTP_X_FORWARDED_FOR"), ",")-1)  
	ElseIf InStr(Request.ServerVariables("HTTP_X_FORWARDED_FOR"), ";") > 0 Then  
	 strIPAddr = Mid(Request.ServerVariables("HTTP_X_FORWARDED_FOR"), 1, InStr(Request.ServerVariables("HTTP_X_FORWARDED_FOR"), ";")-1)  
	Else  
	 strIPAddr = Request.ServerVariables("HTTP_X_FORWARDED_FOR")  
	End If  
	get_client_ip = Trim(Mid(strIPAddr, 1, 30))  
End Function 

' URL重定向
' 可只传入url,默认time=0，msg=""，如果想传入多个参数 array(url,time,msg)
' @param string url 重定向的URL地址
' @param integer time 重定向的等待时间（秒）
' @param string msg 重定向前的提示信息
' @return void
sub R_(args)
	on error resume next
	call C_(Array("SHOW_PAGE_TRACE",0))
    dim str,i,dict,url,time,msg
	set dict = D_	' 非POPASP下把D_换成 Server.CreateObject("Scripting.Dictionary")
	dict(0) = 0		'url
	dict(1) = 0		'time
	dict(2) = ""	'msg
	if isArray(args) Then
		for i = 0 to UBound(args)
			dict(i) = args(i)
		next
	else 
		dict(0) = args	'不是数组的话，默认当只传递了url
	end if
	
	url = dict(0) : time = dict(1) : msg = dict(2)
	
    if msg="" Then msg  = "系统将在" & time & "秒之后自动跳转到" & url & "！"

	' 先当没有设置发送时redirect
	if 0 = time Then
		'设置头信息的方法有时候不好使
		Response.write("<script language='javascript' type='text/javascript'>window.location='" & url & "';</script>")	
		'Response.Redirect(url)
	else
		Response.AddHeader "REFRESH", time & ";URL=" & url
		if -2147467259<>err.number Then
			Response.write msg
		End if
	end if
	' 如果出错，再当已经设置了发送头再进行redirect
    if -2147467259=err.number  then	'这个错误代码头信息已发送
		str    = "<meta http-equiv='Refresh' content='" & time& ";URL=" & url & "'>"
		if 0<>time then str =  str & msg
		Response.write str
	End If
	call L_("")
End Sub

'取参数
'目前filter参数不能使用，输入0即可
Function I_( keyname , filter )
	I_ = POP_MVC.req(keyname)
End Function

Function U_( args )
	dim url,reg,matches,c,a,script_name,params,key
	c = POP_MVC.url.get_ctrl_name()
	params = ""		'参数
	if isArray( args ) Then		'如果是数组，U_(array("Index/Find","id=5"))
		url = args(0)
		if ubound( args ) > 0 Then
			if typename( args(1) ) = "Dictionary" Then				
				for each key in args(1)
					params = params & "&" & key & "=" & args(1)(key)
				next
			else
				params = args(1)
			end if
		end if
		params = POP_MVC.String.ltrim(params,"&")
	else
		url = args
	end if
	script_name =  Request.ServerVariables("SCRIPT_NAME")
	set reg = POP_MVC.reg
	reg.Global = true
	reg.IgnoreCase = true
	reg.MultiLine = false
	
	U_= url
	reg.Pattern = "^[a-z]\w*$"
	if reg.test(url) Then
		a = url
		U_ = script_name & "?" & C_("VAR_MODULE") & "=" & c & "&" & C_("VAR_ACTION") & "=" & a
	End if	
	reg.Pattern = "^(\w+)[/\\](\w+)$"
	if reg.test(url) Then
		set matches = reg.execute(url)
		c = matches(0).subMatches(0)
		a = matches(0).subMatches(1)
		U_ = script_name & "?" & C_("VAR_MODULE") & "=" & c & "&" & C_("VAR_ACTION") & "=" & a
	End if	
	if params <> "" Then
		U_ = U_ & "&" & params
	end if
	set matches = nothing
End Function

' 判断参数args是否为数组，或者为类数组的Dictionary对象
Function is_array( args )
	dim i,stype : stype = typename( args )
	dim key
	is_array = false
	if isArray(args) Then
		is_array = true
		exit function
	elseif stype = "Dictionary" Then
		i = 0
		for each key in args 
			if key <> i Then
				is_array = false
				exit Function
			End if
			i = i+1
		next
		is_array = true
	else 
		is_array = false
	End If		
End Function

'判断为空
Function is_empty(arg)	
	if isArray( arg ) Then
		if ubound(arg)<0 Then is_empty = true : exit Function
	end if
	if isNumeric(arg) Then
		if arg = 0 Then is_empty = true : exit Function
	End If
	is_empty = false
	select case typename(arg)
		case "Empty","Null","Nothing"
			is_empty = true
		case "Dictionary","IVariantDictionary","IRequestDictionary"
			is_empty = (0 = arg.count)
		case "Recordset"
			is_empty = (0 = arg.RecordCount)
		case "ISessionObject"
			is_empty = (0 = arg.Contents.Count)
		case "String"
			is_empty = ("" = arg)
		case "Boolean"
			is_empty = (false = arg)
		case else
			'其它情况还未发现 
	end Select
End Function

'判断是否为数字
Function is_numeric( ByVal arg)
	is_numeric = false
	if isNumeric(arg) Then
		is_numeric = true
		if typename( arg ) = "String" then
			if POP_MVC.String.Exists(arg,",") then
				is_numeric = false
			end if
		end if
		if typename(arg) = "Boolean" Then
			is_numeric = false
		End If
	end If
End Function

'判断是否本地运行
Function is_localhost()
is_localhost = (Request.ServerVariables("LOCAL_ADDR") = Request.ServerVariables("REMOTE_ADDR"))
End Function

Function md5(str)
	md5 = POP_MVC.String.md5(str)
End Function


Function JSONtoXML(jsonText)
  Dim idx, max, ch, mode, xmldom, xmlelem, xmlchild, name, value

  Set xmldom = CreateObject("Microsoft.XMLDOM")
  xmldom.loadXML "<XML/>"
  Set xmlelem = xmldom.documentElement

  max = Len(jsonText)
  mode = 0
  name = ""
  value = ""
  While idx < max
    idx = idx + 1
    ch = Mid(jsonText, idx, 1)
    Select Case mode
    Case 0 ' Wait for Tag Root
      Select Case ch
      Case "{"
        mode = 1
      End Select
    Case 1 ' Wait for Attribute/Tag Name
      Select Case ch
      Case """"
        name = ""
        mode = 2
      Case "{"
        Set xmlchild = xmldom.createElement("TAG")
        xmlelem.appendChild xmlchild
        xmlelem.appendchild xmldom.createTextNode(vbCrLf)
        xmlelem.insertBefore xmldom.createTextNode(vbCrLf), xmlchild
        Set xmlelem = xmlchild
      Case "["
        Set xmlchild = xmldom.createElement("TAG")
        xmlelem.appendChild xmlchild
        xmlelem.appendchild xmldom.createTextNode(vbCrLf)
        xmlelem.insertBefore xmldom.createTextNode(vbCrLf), xmlchild
        Set xmlelem = xmlchild
      Case "}"
        Set xmlelem = xmlelem.parentNode
      Case "]"
        Set xmlelem = xmlelem.parentNode
      End Select
    Case 2 ' Get Attribute/Tag Name
      Select Case ch
      Case """"
        mode = 3
      Case Else
        name = name + ch
      End Select
    Case 3 ' Wait for colon
      Select Case ch
      Case ":"
        mode = 4
      End Select
    Case 4 ' Wait for Attribute value or Tag contents
      Select Case ch
      Case "["
        Set xmlchild = xmldom.createElement(UCase(name))
        xmlelem.appendChild xmlchild
        xmlelem.appendchild xmldom.createTextNode(vbCrLf)
        xmlelem.insertBefore xmldom.createTextNode(vbCrLf), xmlchild
        Set xmlelem = xmlchild
        name = ""
        mode = 1
      Case "{"
        Set xmlchild = xmldom.createElement(UCase(name))
        xmlelem.appendChild xmlchild
        xmlelem.appendchild xmldom.createTextNode(vbCrLf)
        xmlelem.insertBefore xmldom.createTextNode(vbCrLf), xmlchild
        Set xmlelem = xmlchild
        name = ""
        mode = 1
      Case """"
        value = ""
        mode = 5
      Case " "
      Case Chr(9)
      Case Chr(10)
      Case Chr(13)
      Case Else
        value = ch
        mode = 7
      End Select
    Case 5
      Select Case ch
      Case """"
        xmlelem.setAttribute name, value
        mode = 1
      Case "\"
        mode = 6
      Case Else
        value = value + ch
      End Select
    Case 6
      value = value + ch
      mode = 5
    Case 7
      If Instr("}], " & Chr(9) & vbCr & vbLf, ch) = 0 Then
        value = value + ch
      Else
        xmlelem.setAttribute name, value
        mode = 1
        Select Case ch
        Case "}"
          Set xmlelem = xmlelem.parentNode
        Case "]"
          Set xmlelem = xmlelem.parentNode
        End Select
      End If
    End Select
  Wend

  Set JSONtoXML = xmlDom
End Function

Function replaceStr(Byval str,Byval finStr,Byval repStr)
	on error resume next
	if isNull(repStr) then repStr=""
	replaceStr=replace(str,finStr,repStr)
	if err then replaceStr="" : err.clear
End Function

'将null替换成空
Function repnull(str)
	repnull=str
	if isnul(str) then repnull=""
End Function

Function getVarType( ByRef num )
	dim ret
	Select case num			
		case 2	'SmallInt
			ret="整形"	
		case 3	'Int
			ret="长整型"	'AutoNumber OR Number
		case 4	'Real
			ret="单精度型"
		case 5	'Float 
			ret="双精度型"
		case 6	'Currency
			ret = "货币"	'Currency
		case 7	'DateTime
			ret = "日期/时间"	'Date/Time
		case 8	'String
			ret = "String"
		case 11	'Bit 
			ret = "是/否" 	'Yes/No
		case 13 'TimeStamp
			ret = "时间戳"
		case 17	'TinyInt
			ret = "字节"
		case 72	'UniqueIdentifier
			ret = "同步复制ID"
		case 128	'Binary
			ret = "二进制"
		case 129	'Char
			ret = "字符型"
		case 130	'NChar
			ret = "NChar"
		case 131	'Decimal
			ret = "小数"
		case 133	'DateTime
			ret = "DateTime"
		case 135	'SmallDateTime
			ret = "SmallDateTime"
		case 200	'VarChar
			ret = "VarChar"
		case 201	'Text
			ret = "Text"
		case 202	'VarChar
			ret = "文本"
		case 203	'备注
			ret = "备注"
		case 204	'Binary
			ret = "Binary"
		case 205	'Image
			ret = "OLE 对象"	
		case else
			ret = "未知的" & num
	end Select
	getVarType = ret
End Function

'使用JMAIL邮件发送
'邮件服务器,邮件账号,邮件密码,发件人邮箱,发件人姓名,收件人邮箱,标题，内容
'Call sendMail( "smtp.163.com" , "popasp@163.com" , "密码","popasp@163.com","popasp", "popasp@163.com;709701017@qq.com","测试POPASP","测试POPASP")
Function sendJMail(smtp_server,smtp_user,smtp_password,smtp_usermail,from_name,to_email,zhuti,mailbody)
	err.clear
	on error resume next	
	dim jmail
	Server.ScriptTimeOut=5000
	dim startTime : startTime = timer()
	Set jmail = Server.CreateObject("JMail.Message")   '创建一个JMAIL对象
	if err.number <> 0 then
		POP_MVC.exit("未安装JMail")
	end if
    jmail.silent = True    'JMAIL不会抛出例外错误，返回的值为FALSE跟TRUE
    jmail.logging = False   '启用邮件日志
    jmail.Charset = "utf-8"  '邮件文字的代码为简体中文 
    jmail.ISOEncodeHeaders = False '防止邮件标题乱码
    jmail.ContentType = "text/html"    '邮件的格式为HTML格式
	dim i,arr
	arr = split( to_email, ";" )
	for i = 0 to arr
		jmail.AddRecipient arr(i)    '邮件收件人的地址
	next
    jmail.From = smtp_usermail    '邮件发送者的邮件地址
    jmail.FromName = from_name    '邮件发送者的姓名 
    jmail.MailServerUserName = smtp_user    '登录邮件服务器所需的用户名
    jmail.MailServerPassword = smtp_password     '登录邮件服务器所需的密码
    jmail.Subject = zhuti    '邮件的标题 
    jmail.Body = mailbody      '邮件的内容
    jmail.Priority = 3      '邮件的紧急程序，1 为最快，5 为最慢， 3 为默认值
    jmail.Send( smtp_server )     '执行邮件发送（通过邮件服务器地址）
	sendJMail = jmail.errorCode
	call POP_MVC.pushTime( startTime , "使用jmail发送主题为 " & zhuti & " 的邮件")
	
	jmail.Close()   '关闭对象
	set jmail = nothing	
End Function


'检测是否为手机端
Function CheckWap()
	dim arr,i,agent
	CheckWap = false
	arr = Array("Android", "iPhone", "SymbianOS", "Windows Phone", "iPad", "iPod","webOS")
	agent = Request.ServerVariables("HTTP_USER_AGENT")
	for i = 0 to ubound(arr)
		if inStr( agent, arr(i) ) > 0 then
			CheckWap = true
			Exit Function
		end if
	next
End Function


'使用CDO.Message邮件发送
'邮件服务器,邮件账号,邮件密码,发件人邮箱,发件人姓名,收件人邮箱,标题，内容
'Call sendMail( "smtp.163.com" , "popasp@163.com" , "密码","popasp@163.com","popasp", "popasp@163.com;709701017@qq.com","测试POPASP","测试POPASP")
Function sendMail(smtp_server,smtp_user,smtp_password,smtp_usermail,from_name,to_email,zhuti,mailbody)
  on error resume next  
  Dim objConfig,objMessage,Fields
  dim startTime : startTime = timer()
  Set objConfig = Server.CreateObject("CDO.Configuration") 
  Set Fields = objConfig.Fields 
  
  dim schema : schema = "http://schemas.microsoft.com/cdo/configuration/"
  With Fields 
  .Item( schema & "sendusing" ) = 2	'发送邮件端口 
  .Item( schema &  "smtpserver" ) = smtp_server
  .Item( schema &  "smtpserverport") = 25	'SMTP服务器端口                
  .Item( schema &  "smtpconnectiontimeout") = 10     
  .Item( schema &  "smtpauthenticate") = 1
  .Item( schema &  "sendusername" ) = smtp_user		'邮箱登陆帐户名
  .Item( schema & "sendpassword") = smtp_password	'邮箱登陆密码
  .Update 
  End With 	  
  
  Set objMessage = Server.CreateObject("CDO.Message") 
  Set objMessage.Configuration = objConfig 
  
  With objMessage
  .BodyPart.Charset = "utf-8"
  .To=to_email
  .From= from_name & "<" & smtp_usermail & ">"
  .Subject=zhuti
  .HtmlBody=mailbody  'html正文
   '.TextBody = Trim(POP_MVC.Form("m_neirong")) '文本正文
   '.AddAttachment "C:/Scripts/Output.txt"'邮件附件    
  .Send 
  End With 
  sendMail = err.number
  if err.number = 0 then
	call POP_MVC.pushTime( startTime , "使用CDO.Message发送主题为 " & zhuti & " 的邮件耗时")
  else
	Call L_( "SendMail" )
  end if
  Set Fields = Nothing 
  Set objMessage = Nothing 
  Set objConfig = Nothing
End Function
%>