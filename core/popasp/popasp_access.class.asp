<%
' +----------------------------------------------------------------------
' | POPASP [ ASP MVC ]
' +----------------------------------------------------------------------
' | Copyright (c) 2016 http://popasp.com All rights reserved.
' +----------------------------------------------------------------------
' | Author: popasp <1737025626@qq.com>
' +----------------------------------------------------------------------

'在本类中已经使用了Replace作为方法名，所以在本类中不能再使用Replace函数了，请考虑使用正则的Replace方法吧。
Class POPASP_ACCESS
	'为了减轻数据库操作类的重量，特将相同代码写到了另外一个文件
	'POPASP_REPLACE_CLASS_CODE_POPASP'
	
	' 获取表的所有字段与字段类型
	Function getTableFields( ByVal table_name )
		table_name = getTrueTableName(table_name)
		set getTableFields = tool.getTableFields("SELECT TOP 1 * FROM [" & table_name & "]" )
	End Function
	
	
	'获取字段说明
	Public Function getFieldProp( ByVal table_name , ByVal field_name )
		Dim MyDB,MyTable,MyField,dict,ts
		set mydb=POP_MVC.SCO("adox.catalog")
		set mytable=POP_MVC.SCO("adox.table")
		set myfield =POP_MVC.SCO("adox.column")
		table_name = getTrueTableName(table_name)
		MyDB.ActiveConnection = tool.conn
		set MyTable = MyDB.Tables(table_name)
		set MyField = MyTable.Columns(field_name)
		set dict = D_
		set ts = getTableStructure(table_name)

		if isObject( MyField ) then
			dict("Description") = MyField.Properties("Description").value
			dict("Nullable") = MyField.Properties("Nullable").value
			dict("Increment") = MyField.Properties("Increment").value
			dict("Default") = MyField.Properties("Default").value
			dict("Autoincrement") = MyField.Properties("Autoincrement").value		
			dict("FieldType") = ts( field_name )		
			dict("VarType") = getVarType(ts( field_name ))		
		end if
		set getFieldProp = dict
	End Function
	
	'获取字段说明
	Public Function getFieldDesc( ByVal table_name , ByVal field_name )
		Dim MyDB,MyTable,MyField,dict
		set mydb=POP_MVC.SCO("adox.catalog")
		set mytable=POP_MVC.SCO("adox.table")
		set myfield =POP_MVC.SCO("adox.column")
		table_name = getTrueTableName(table_name)
		field_name = LCase(field_name)
		MyDB.ActiveConnection = tool.conn
		set MyTable = MyDB.Tables(table_name)
		set MyField = MyTable.Columns(field_name)
		getFieldDesc = MyField.Properties("getFieldDesc").value
	End Function
	
	'从table_name表所有文本型字段中查找findStr，替换为repStr
	function soReplace4access( ByVal table_name , ByVal findStr, ByVal repStr )
		dim dict,repFields,fields,pkey,item,field,i
		
		call parseOptions
		if isNul( table_name ) then
			table_name = parsedOptions("table")
		end if
		
		pkey = getPrikey( table_name )
		
		'如果不存在主键，则不进行替换
		if isNul( pkey ) then 
			POP_MVC.Warning( "ACCESS数据库 " & table_name & " 表不存在主键，无法完成替换" )
			Exit Function
		end if
		
		'POP_MVC.Arr.push soFields,pkey
		
		'得到表的所有文本型字段
		set fields = getTableFields( table_name )
		for each key in fields
			if fields(key) = 201 or fields(key) = 202 or fields(key) = 203 then
				POP_MVC.Arr.push soFields , key
			end if
		next
		
		'没有文本型字段则退出函数
		if isEmpty( soFields ) then
			POP_MVC.Warning( "ACCESS数据库 " & table_name & " 表不存在文本型字段，无法完成替换" )
			Exit Function
		end if
		
		'解析搜索条件
		set dict = parseSearch( findStr,soFields )
		
		'设置where条件
		if not isEmpty( parsedOptions("where") ) then
			parsedOptions("where") = "( " & parsedOptions("where") & " )  AND " & dict( "sql" )
		else
			parsedOptions("where") = dict( "sql" )
		end if
		
		set dict = nothing
		
		'select的字段
		parsedOptions("field") = pkey & "," & join( soFields , "," )		
		
		'得到sql
		lastSql = getSelectSql(parsedOptions)
		
		'获取 Recordset 对象
		set rs = getRS( lastSql )		
		
		'使用事务处理
		Call Me.Begin 
		
		Do While not rs.eof
			repFields = Array()
			
			'遍历每一个文本型字段
			for i = 0 to ubound( soFields )
				field = soFields(i)
				item = rs( field )
				if not isNul( item ) then
					'在字段中找到查找字符串才进行替换
					if inStr( item , findStr ) > 0 then
						item = replacestr( item , findStr , repStr )
						POP_MVC.Arr.push repFields , "[" & field & "] = '" &  tool.safe( item ) & "'"
					end if
				end if
			next
			
			'拼接update用的sql
			lastSql = "UPDATE " & table_name & " SET " & join( repFields , " , " ) & " WHERE " & pkey & " = " & iif( isNumeric( rs(pkey) ) , rs(pkey) , "'" & rs(pkey) & "'" )
			
			Call exec( lastSql )			
			rs.moveNext
		loop
		Call Me.Commit
		
		soReplace4access = rs.recordCount
		
		Call tool.closeRS( rs )
	end function
	
	Function InsertByTable( table,data )
		lastSql = tool.getInsertSql( table, data , getTableStructure( table ) )
		InsertByTable = tool.InsertByTable( table , data , getPrikey( table ) )
	End Function
	
	' 向数据表中插入数据，data为Dictionary对象，其键名与字段名相对应，如果data含主键，须手动删除
	Public Function Insert(ByVal table,ByVal data)
		table = getTableFromInput(table)
		call handlerData( table,data , 1 ) '剔除不存在的字段与主键
		Insert = InsertByTable( table , data )
	End Function
	
	' 向数据表中添加数据
	Public Property Get Add()
		call parseOptions	'解析参数
		
		if is_empty(parsedOptions("table")) then 
			Call setPoptsTable( parsedOptions )	
		end if
	
		if isEmpty( parsedOptions("data") ) Then
			set parsedOptions("data") = POP_MVC.Form2Dict()
		end If
		
		if ( not is_empty( parsedOptions("data") ) ) Then			
			'剔除不存在的字段与主键
			if isEmpty( Me.prikey ) then				
				call handlerData( parsedOptions("table"),parsedOptions("data") , 0 )
			else
				call handlerData( parsedOptions("table"),parsedOptions("data") , 1 )
			end if
			


			Add = InsertByTable( parsedOptions("table") , parsedOptions("data") )
		else 
			POP_MVC.error( "POPASP_ACCESS.Add" )
		end If		
		call ResumeOpts
	End Property
	
	' Replace，无则添加，有则修改，此时where无效，返回结果是最后添加或修改的ID
	Public Property Get [Replace]()
		On Error Resume Next
		dim popts,pk,max,findCount	
		call parseOptions	'解析参数		
		set popts = parsedOptions
		
		if is_empty(popts("table")) then
			Call setPoptsTable( popts )	
		end if
		
		pk = getPrikey( popts("table") )	'得到主键		
		if popts("data").Exists( pk ) Then	'如果数据中存在主键，则需要从最大ID与是否存在该ID两方面判断
			set rs = getRS( "SELECT TOP 1 MAX([" & pk & "]) AS theResult FROM [" & popts("table") & "]")
			rs.moveFirst
			max = rs("theResult")	'得到最大ID
			
			tool.closeRS rs
			set rs = getRS( "SELECT TOP 1 * FROM [" & popts("table") & "] WHERE " & pk & " = " & popts("data")(pk) )
			findCount = rs.RecordCount	'以该ID查找一行记录，找到为1，找不到为0			
			tool.closeRS rs
			
			
			if max >= popts("data")(pk) then	'数据ID <= 最大ID				
				if findCount > 0 then	'如果能找到，则修改
					options.Remove("where")	
					[Replace] = popts("data")(pk)	'返回实际修改的ID
					if Save() = 0 then						
						[Replace] = 0
					end if
				else	'如果找不到，插入到数据ID的位置上。返回该数据ID
					[Replace] = Add()
				end if
			else	' 超出最大ID，则添加，如果数据表被全部删除，此时生成的ID并不是1
				call options("data").Remove(pk)
				[Replace] = Add()
			end if	

		else	'不存在主键，直接添加
			[Replace] = Add()
		end if		
		call L_( Me.db_type & " Replace" )
	End Property
	
	' 得到数据库中的所有表名，返回一个对象，键名为数字，值为表名
	'如果想得到所有表的数组，请使用 getAllTables()
	Public Function getTables()
		on error resume next
		dim shm,arr,content,cnt
		dim filename	

		' 如果已经保存了数据，直接返回
		if not isEmpty(dTables) Then				
			set getTables = dTables
			if not isEmpty( getTables ) then
				Exit Function
			end if			
		End If
		'如果对应文件不存在，则须先将数据生成json保存到文件中
		filename = tool.getDataFileName("_tables_")
		if Not is_empty( POP_MVC.config("APP_DEBUG") ) OR Not tool.file_exists( filename ) Then			
			call tool.initConn		
			Set shm = tool.conn.OpenSchema(20)
			lastSql = "Access OpenSchema"
			
			POP_MVC.num_db_query = POP_MVC.num_db_query + 1

			shm.MoveFirst 
			'[ { "TABLE_CATALOG": null, "TABLE_SCHEMA": null, "TABLE_NAME": "~TMPCLP186201", "TABLE_TYPE": "TABLE", "TABLE_GUID": null, "DESCRIPTION": null, "TABLE_PROPID": null, "DATE_CREATED": "2018/12/6 11:27:27", "DATE_MODIFIED": "2018/12/6 11:43:18" },... ]
			cnt = 0
			Do While Not shm.EOF and NOt shm.BOF
				if cnt > 2000 then
					exit do
				end if
				If shm("TABLE_TYPE") = "TABLE" and mid(shm("TABLE_NAME"),1,1) <> "~" Then					
					POP_MVC.Arr.Push arr, shm("TABLE_NAME")
				End If	
				
				shm.MoveNext 
				cnt = cnt + 1
			Loop
			tool.closeRS shm
			call tool.file_put_contents(filename, join(arr,",") )	
		else	'从文件中取出数据
			content = POP_MVC.file_get_contents(tool.getFilePath(filename))
			content = mid(content,2)
			arr = split(content,",")
		End If		
		set dTables = POP_MVC.Arr.toDict(arr)
		set getTables = dTables		
		Call L_("POPASP_ACCESS.getTables")
	End Function
	


	'多次测试mdb不能适用，accdb可以使用
	Public Function setCounter( ByVal table_name , ByRef dSeed, ByRef dIncrement )
		on error resume next
		dim conn,conn_str,pk,dbPath
		table_name = getTrueTableName(table_name)
		pk = getPrikey( table_name )
		P_("db").db_type = db_type
		P_("db").db_path = db_path
		if not isEmpty(pk) then
			set tool.conn = nothing
			dbPath = POP_MVC.config("DB_PATH")
			if POP_MVC.String.iEndsWith( db_path, ".mdb" ) then
				P_("db").execute( "ALTER TABLE [" & table_name & "] ALTER COLUMN [" & pk & "] LONG" )
			end if
			P_("db").execute( Array("ALTER TABLE [" & table_name & "] ALTER COLUMN [" & pk & "] COUNTER(" & dSeed & "," & dIncrement & ")" , 0) )
			set P_("db").conn = nothing
			POP_MVC.removePopaspClass("POPASP_DB")
			POP_MVC.config("DB_PATH") = db_path
			tool.initConn
		end if
	End Function
	
	'获取数据表结构
	Public Function getTableStructure( ByVal tableName )
		on error resume next
		call getTables	
		tableName = getTrueTableName(tableName)

		if dTables.count = 0 then
			Call getTables()
		end if
	
		set getTableStructure = tool.getTableStructure( tableName,dTables,dTS,"SELECT TOP 1 * FROM [" & tableName & "]")	
	End Function
	
	'将nodes节点转化为建表sql
	'节点名为字段名，属性对应修饰
	'要求属性名为prikey,autoIncrement,allowNull,type,size
	'分别对应 主键(1或空),自动编号(1或空),允许为空(1或空),类型(1或空),设定大小(数字或空)
	'sqlType为0或空时得到建表语句，1则返回一个数组，Array( 建表语句, 字段修改语句... )
	Public Function Nodes2CreateSQL( ByRef nodes , ByVal table_name , sqlType )
		dim node,cSql,arr,typs
		dim primarykey,autoIncrement,allowNull,stype,size,pkey
		table_name = getTrueTableName( table_name )
		cSql = "CREATE TABLE [" & table_name & "] ( " & chr(10)
		POP_MVC.Arr.Push arr,"建表语句"
		for each node in nodes			
			primarykey = repNull(node.getAttribute("prikey"))
			if primarykey <> "" and isEmpty(pKey) then
				pKey = node.text
			end if
			

			stype = repNull(node.getAttribute("type"))
			autoIncrement = repNull(node.getAttribute("autoIncrement"))
			size = repNull(node.getAttribute("size"))
			allowNull = repNull(node.getAttribute("allowNull"))
			
			cSql = cSql & vbTab & "[" & node.text & "] "
			
			sql = "ALTER TABLE [" & table_name & "] "
			if stype <> "" then 
				cSql = cSql & stype
				sql = sql & stype
			end if
			
			typs = UCase( stype )
			
			if typs = "VARCHAR" or typs = "BINARY" or typs = "CHAR" then				
				if size <> "" then 
					cSql = cSql & "(" & size & ")"
					sql = sql & "(" & size & ") "
				end if
			end if
			
			if allowNull = "" then 
				cSql = cSql & " not null"
				sql = sql & " not null"
			end if
			
			if autoIncrement <> "" and primarykey <> "" then 
				cSql = cSql & " identity"
				sql = sql & " identity"
			end if
			cSql = cSql & "," & chr(10)
			POP_MVC.Arr.push arr,sql			
		next 	
		
			if pKey <> "" then
				cSql = cSql & vbTab & "PRIMARY KEY ([" & pKey & "])" & chr(10)
				cSql = cSql & ");"
			else
				cSql = Mid(cSql,1,Len(cSql)-2)
				cSql = cSql & chr(10) & ");"
			end if	
		arr(0) = cSql
		if sqlType = 0 or isNul(sqlType) then
			Nodes2CreateSQL = cSql
		else
			Nodes2CreateSQL = arr
		end if		
	End Function
	
	'获取数据表的建表语句
	Public Function getCreateSQL( ByVal tableName )
		on error resume next
		dim primarykey,sql,obj,typs,attrib
		primarykey = getPrikey( tableName )
		set obj = P_("dbmanage")
		sql = "CREATE TABLE [" & getTrueTableName(tableName) & "] ( " & chr(10)
		Set rs = tool.conn.execute("SELECT * FROM " & getTrueTableName(tableName) )
		if err = 0 then
			for i = 0 to rs.fields.count-1
			   sql = sql & vbTab & "[" & rs(i).name & "] "
			   typs = obj.getTypeName(rs(i).type)
			   if typs = "VARCHAR" or typs = "BINARY" or typs = "CHAR" then
				 sql = sql & typs & "(" & rs(i).definedsize & ")"
			   else
				 sql = sql & typs & " "
			   end if
			   attrib = rs(i).attributes
			   if (attrib and obj.adFldIsNullable) = 0 then
				 sql = sql&" NOT NULL"
			   end if
			   if rs(i).Properties("ISAUTOINCREMENT") = True then
				 sql = sql & " IDENTITY"
			   end if
			   sql = sql & "," & chr(10)
			next
			if primarykey <> "" then
				sql = sql & vbTab & "PRIMARY KEY ([" & primarykey & "])" & chr(10)
				sql = sql & ");"
			else
				sql = Mid(sql,1,Len(sql)-2)
				sql = sql & ");"
			end if
		else
			sql = "CREATE TABLE [" & table & "];"
		end if
		rs.close : set rs = nothing
		getCreateSQL = getCreateSQL & sql		
	End Function	
	
	
	'清空数据表，ID重新设置
	Public Property Get Truncate( ByVal table_name )
		on error resume next
		dim sql
		table_name = getTrueTableName(table_name)
		Truncate = Me.table(table_name).where(" 1 = 1 ").remove
		Truncate = setCounter( table_name, 1,1 )
	End Property
	
	'删除一个数据表
	Public Property Get DropTable( ByVal table_name )
		DropTable = P_("POPASP_DBCONVERT").DropAccTable( db_path , getTrueTableName( table_name ) )
	End Property
	
	'将 acc 数据库转化成 mysql 数据库，只要结构不要数据，如果 mysql 中已经存在同名表，则不去创建
	'mysql 的配置参数在配置中进行配置，并且要求 mysql 中已经有建好的数据库 DB_NAME
	Public Property Get toMysql4desc( ByVal dbName )
		toMysql4desc = P_("POPASP_DBCONVERT").acc2mysql4desc(db_path,dbName,"","")
	End Property
	
	'将 acc 数据库转化成 mysql 数据库，结构连数据，如果 mysql 中已经存在同名表，则不去创建
	'mysql 的配置参数在配置中进行配置，并且要求 mysql 中已经有建好的数据库 DB_NAME
	Public Property Get toMysqlWithData( ByVal dbName )
		Call P_("POPASP_DBCONVERT").DropMysqlTables( dbName)
		toMysqlWithData = P_("POPASP_DBCONVERT").acc2mysqlWithData(db_path,dbName,"","")
	End Property
	
	'将 acc 数据表转化成 mysql 数据表，结构连数据，如果 mysql 中已经存在同名表，则不去创建
	'mysql 的配置参数在配置中进行配置，并且要求 mysql 中已经有建好的数据库 DB_NAME	
	Public Function  toMysqlWithData4table( ByVal dbName, ByVal access_prefix , ByVal mysql_prefix )
		dim table_name
		table_name = MID( tableName, len( db_prefix ) + 1 )
		Call P_("POPASP_DBCONVERT").TruncateMysqlTable( table_name,mysql_prefix )
		toMysqlWithData4table = P_("POPASP_DBCONVERT").acc2mysqlWithData4table( table_name ,db_path ,dbName, access_prefix , mysql_prefix )
	End Function
	
	'将acc数据库转化成 sqlite3 数据库，只要结构不要数据，如果 sqlite3 中已经存在同名表，则不去创建
	'sqlite3的配置参数在配置中进行配置，并且要求 sqlite3 中已经有建好的数据库 DB_NAME
	Public Property Get toSqlite4desc( ByVal sqlitePath )
		toSqlite4desc = P_("POPASP_DBCONVERT").acc2sqlite4desc(db_path,sqlitePath,"","")
	End Property
	
	'将acc数据库转化成 sqlite3 数据库，结构连数据，如果 sqlite3 中已经存在同名表
	'sqlite3 的配置参数在配置中进行配置，并且要求 sqlite3 中已经有建好的数据库 DB_NAME
	Public Property Get toSqliteWithData( ByVal splitePath )
		Call P_("POPASP_DBCONVERT").DropSqliteTables( splitePath )
		toSqliteWithData = P_("POPASP_DBCONVERT").acc2sqliteWithData(db_path,splitePath,"","")
	End Property
	
	'将acc数据表转化成 sqlite3 数据表，结构连数据，如果 sqlite3 中已经存在同名表
	'sqlite3 的配置参数在配置中进行配置，并且要求 sqlite3 中已经有建好的数据库 DB_NAME	
	Public Function  toSqliteWithData4table( ByVal sqlitePath, ByVal access_prefix , ByVal sqlite_prefix )
		dim table_name
		table_name = MID( tableName, len( db_prefix ) + 1 )
		Call P_("POPASP_DBCONVERT").TruncateSqliteTable( sqlitePath , table_name,sqlite_prefix )
		toSqliteWithData4table = P_("POPASP_DBCONVERT").acc2sqliteWithData4table( table_name ,db_path , sqlitePath, access_prefix , sqlite_prefix )
	End Function	
End Class
%>