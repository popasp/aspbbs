<%
' +----------------------------------------------------------------------
' | POPASP [ ASP MVC ]
' +----------------------------------------------------------------------
' | Copyright (c) 2016 http://popasp.com All rights reserved.
' +----------------------------------------------------------------------
' | Author: popasp <1737025626@qq.com>
' +----------------------------------------------------------------------

'借用access数据库mdb打包和解包
Class POPASP_ACCESSPACk
	Private s_DbPath,s_RootPath,s_RootPathLen,s_Dbldb
	Private Conn,rs,stream
	
	'类的销毁
	Private Sub Class_Terminate
		if isObject( Conn ) then
			Conn.close
			set Conn = nothing
		end if
	End Sub

	'赋值数据库路径
	Public Property Let DbPath( value )
		s_DbPath = value
		s_DbPath = POP_MVC.rtrim( s_DbPath , "\" )
		s_DbPath = POP_MVC.rtrim( s_DbPath , "/" )
		s_DbPath = POP_MVC.realPath( s_DbPath )
		
		if POP_MVC.String.iEndsWith( s_DbPath , ".mdb" ) then
			s_Dbldb = left( s_DbPath , len(s_DbPath) - 4 ) & ".ldb"
		else
			s_Dbldb = s_DbPath & ".ldb"
		end if
	End Property
	
	'得到数据库路径
	Public Property Get DbPath(  )
		DbPath = s_DbPath		
	End Property
	
	'赋值打包文件的根路径
	Public Property Let RootPath( value )
		s_RootPath = value
		s_RootPath = POP_MVC.rtrim( s_RootPath , "\" )
		s_RootPath = POP_MVC.rtrim( s_RootPath , "/" )
		s_RootPath = POP_MVC.realPath( s_RootPath )
		s_RootPathLen = Len( s_RootPath ) + 1
	End Property
	
	'得到打包文件的根路径
	Public Property Get RootPath(  )
		RootPath = s_RootPath		
	End Property
	
	'创建数据库
	'isOver,1时重建，0时不重建
	Public Property Get CreateDb( isOver )
		'存在的话先删除
		if POP_MVC.File.isFile(s_DbPath) and isOver = 1 then
			POP_MVC.file.remove( s_DbPath )
		end if
		
		Dim conn, connStr, adoCatalog
		Set conn = Server.CreateObject("ADODB.Connection")
		
		Set adoCatalog = Server.CreateObject("ADOX.Catalog")
		connStr = "Provider=Microsoft.Jet.OLEDB.4.0; Data Source=" & s_DbPath
		adoCatalog.Create connStr
		conn.Open connStr
		conn.Execute("Create Table FileData(Id int IDENTITY(1,1) PRIMARY KEY CLUSTERED,EditTime DateTime, thePath VarChar, fileContent Image)")

		Set conn = Nothing
		Set adoCatalog = Nothing
	End Property
	
	'打包
	'添加就会打包
	Public sub Add(ByVal thePath)
		on error resume next
		Server.ScriptTimeOut = 5000
		Dim connStr
		if isEmpty( conn ) then			
			Set conn = Server.CreateObject("ADODB.Connection")
			connStr = "Provider=Microsoft.Jet.OLEDB.4.0; Data Source=" & s_DbPath
			conn.Open connStr
		end if
		Set rs = Server.CreateObject("ADODB.RecordSet")
		rs.Open "FileData", conn, 3, 3
		Set stream = Server.CreateObject("ADODB.Stream")
		stream.Open
		stream.Type = 1
		thePath = POP_MVC.realPath( thePath )
		fsoTreeForMdb thePath, rs, stream
		rs.Close
		stream.Close
		Set rs = Nothing
		Set stream = Nothing
	End sub

	Sub fsoTreeForMdb( ByVal thePath, rs, stream)	
		'on error resume next

		Dim item, theFolder, folders, files
		if POP_MVC.file.isFile( thePath ) then
			if LCase(thePath) <> LCase(s_DbPath) AND LCase(thePath) <> LCase(s_Dbldb) then
				rs.AddNew
				rs("EditTime") = POP_MVC.file.mtime( thePath )
				rs("thePath") = mid(thePath,s_RootPathLen)
				stream.LoadFromFile(thePath)
				rs("fileContent") = stream.Read()
				rs.Update
			end if
		elseif POP_MVC.file.isFolder( thePath ) then
			dim objFolder,objFiles,i,fileList
			set objFolder=POP_MVC.fso.GetFolder( thePath )
			set objFiles=objFolder.Files
			for each file in objFiles
				call fsoTreeForMdb( file.Path , rs , stream )
			next 
			set objFiles=nothing				
			Set objFiles = objFolder.SubFolders	  
			For Each file In objFiles		
				call fsoTreeForMdb( file.Path , rs , stream )
			Next
			set objFiles=nothing	
			set objFolder=nothing
		end if
	End Sub
	
	'解包
	'thePath为解包数据库路径
	Public Function unPack(ByVal thePath , ByVal DirPrefix)
		Server.ScriptTimeOut = 5000
		
		if isNul( thePath ) then
			thePath = s_DbPath
		end if
		Dim rs,  conn, stream, connStr, theFolder
		Set rs = Server.CreateObject("ADODB.RecordSet")
		Set stream = Server.CreateObject("ADODB.Stream")
		Set conn = Server.CreateObject("ADODB.Connection")
		connStr = "Provider=Microsoft.Jet.OLEDB.4.0;Data Source=" & POP_MVC.realPath(thePath) & ";"
		conn.Open connStr
		rs.Open "FileData", conn, 1, 1
		stream.Open
		stream.Type = 1
		
		DirPrefix = POP_MVC.rtrim( POP_MVC.rtrim( DirPrefix , "/" ) , "\" )
		Do Until rs.Eof
			theFolder = POP_MVC.file.dir( DirPrefix & rs("thePath") )
			if POP_MVC.CreateFolder(theFolder) then
				stream.SetEos()
				stream.Write rs("fileContent")
				stream.SaveToFile POP_MVC.RealPath( DirPrefix & rs("thePath")), 2
				rs.MoveNext			
			end if
		Loop
		rs.Close
		conn.Close
		stream.Close
		Set rs = Nothing
		Set stream = Nothing
		Set conn = Nothing
	End Function
	
	'解包
	'thePath为解包数据库路径
	Public Function cmsUnPack(ByVal thePath , ByVal DirPrefix , ByRef callback  )
		Server.ScriptTimeOut = 5000
		
		if isNul( thePath ) then
			thePath = s_DbPath
		end if
		Dim rs, conn,  connStr, theFolder,savePath,theLen,adminName,theAdmin
		Set rs = Server.CreateObject("ADODB.RecordSet")
		Set conn = Server.CreateObject("ADODB.Connection")
		connStr = "Provider=Microsoft.Jet.OLEDB.4.0;Data Source=" & POP_MVC.realPath(thePath) & ";"
		conn.Open connStr
		rs.Open "FileData", conn, 1, 1
		
		if LCase(C_("CMS_NAME")) = "iaspcms" then
			adminName = "admin123"
			theAdmin  = POP_MVC.file.BaseName( POP_MVC.realPath("./") )
		elseif LCase(C_("CMS_NAME")) = "aspbbs" then
			adminName = "core"
			theAdmin = coreName
		end if
		
		
		theLen = len(adminName) + 2
		if isNul( DirPrefix ) then
			DirPrefix = POP_MVC.config("sitePath")
		end if
		
		Do Until rs.Eof
			savePath = rs("thePath")
			
			'获取路径
			theFolder = POP_MVC.file.dir( DirPrefix & savePath )
			
			if left( savePath , theLen ) = "\" & adminName & "\" then
				if LCase(C_("CMS_NAME")) = "iaspcms" then
					savePath = "..\" & theAdmin & "\" & mid( savePath , theLen + 1 )
				elseif LCase(C_("CMS_NAME")) = "aspbbs" then
					savePath =  theAdmin & "\" & mid( savePath , theLen + 1 )
				end if
			end if
		
			savePath = replace( savePath, "\" , "/" )
			Execute "bool = " & callback & "( savePath , rs )"
				
			if bool then
				response.write "<center>" & (i+1) & ". 正在更新 " & savePath & "</center>"
				if POP_MVC.CreateFolder(theFolder) then
					Call P_("VERSION").stream2file(rs("fileContent") , savePath  )					
				end if	 
			end if

			rs.MoveNext	
		Loop
		rs.Close
		conn.Close
		Set rs = Nothing
		Set conn = Nothing
	End Function
End Class
%>