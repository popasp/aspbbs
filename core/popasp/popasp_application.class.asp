<%
' +----------------------------------------------------------------------
' | POPASP [ ASP MVC ]
' +----------------------------------------------------------------------
' | Copyright (c) 2016 http://popasp.com All rights reserved.
' +----------------------------------------------------------------------
' | Author: popasp <1737025626@qq.com>
' +----------------------------------------------------------------------
Class POPASP_APPLICATION
	Public Property Get Export
		Dim x,i : i=1
		for each x in Application.Contents
			Response.Write( "Index: " & i  & ","&  x & "=" & Application.Contents(x) & "<br />")
			i = i+1
		next
	End Property
	
	' 清空所有 Application.Contents
	Public Property Get Clear()
		Application.Contents.RemoveAll
	End Property
	
	' 删除所有
	Public Property Get RemoveAll
		dim key,length,cnt
		RemoveAll = Application.Contents.count
		Clear
	End Property
	
	' 清除前缀为keyPrefix的那一部分
	Public Property Get RemovePrefix(ByVal keyPrefix)
		dim key,length
		length = Len(keyPrefix)
		For Each key in Application.Contents
			if Left(key,length) = keyPrefix Then
				Remove key
			End If
		Next 
	End Property
	
	' 清除某一个键值对
	Public Property Get Remove(Byval key)
		Application.Contents.Remove key
	End Property
	
	' 某个键是否存在
	Public Function Exists(Byval key)
		Exists =  (Not isEmpty(Application.Contents(key)))
	End Function
	
	' 得到某个键对应值
	Public Function [Get](byVal key)		
		[Get] = Application.Contents(key)
	End Function
	
	' 添加或修改一个键值对
	Public Sub [Set](byVal key,value)
		call Lock		

		if isDate(value) Then
			Application(key) = CStr(value)
		else 
			Application(key) = value
		End If		

		call Unlock
	End Sub
	
	'判断是否缓存
	'key为键名，LifeTime为缓存时间，单位秒
	Public Function getTime( ByVal key )	
		getTime = Me.get( key & "_" & "time" )
	End Function
	
	'判断是否缓存
	'key为键名，LifeTime为缓存时间，单位秒
	Public Function IsCached( ByVal key, LifeTime )
		dim appKey,timeValue
		
		'缓存时间至少为１秒
		if LifeTime <=0 then
			IsCached = false
			Exit Function
		end if
		
		'判断是否存在该键值对
		if not Exists( key ) then
			IsCached = false
			Exit Function
		end if

		
		timeValue = Me.get( key & "_" & "time" )
		
		
		'判断是否存在时间键名
		if not Exists( key & "_" & "time" ) or not IsDate( timeValue ) then
			IsCached = false
			Exit Function
		end if

		
		'判断时间键值对是否过期
		if DateDiff( "s" , timeValue, now() ) - LifeTime > 0 then
			IsCached = false
			Exit Function
		end if
		
		IsCached = True		
	End Function
	
	
	'缓存，过期则缓存，未过期不缓存
	'key为键名，LifeTime为缓存时间，单位秒
	Public Sub Cache( ByVal key, LifeTime , content )
		'缓存时间至少为１秒
		if LifeTime <=0 then			
			Exit Sub
		end if		
		if not IsCached( key , LifeTime ) then
			Me.set key & "_" & "time" , now
			Me.set key , content
		end if
	End Sub	
	
	' 自增
	' 如果key为字符串，则每次自增1，如果为数组，第一个是键名，第二个是增长步长
	Public Property Get SetInc( ByVal key )
		dim k,v
		v = 1
		
		if isArray( key ) then
			k = key(0)
			if ubound( key ) > 0 then
				v = key(1)
			end if			
		else
			k = key
		end if

		if IsNumeric( Application(k) ) then
			Application(k) = Application(k) + v
		else
			Application(k) = v
		end if
		SetInc = Application(k)
	End Property
	
	' 自减
	' 如果key为字符串，则每次自增1，如果为数组，第一个是键名，第二个是增长步长
	Public Property Get SetDec( ByVal key )
		dim k,v
		v = 1
		
		if isArray( key ) then
			k = key(0)
			if ubound( key ) > 0 then
				v = key(1)
			end if			
		else
			k = key
		end if

		if IsNumeric( Application(k) ) then
			Application(k) = Application(k) - v
		else
			Application(k) = -v
		end if
		SetDec = Application(k)
	End Property
	
	'锁定
	Public Property Get Lock
		Application.lock
	End Property
	
	' 解锁
	Public Property Get Unlock
		Application.unlock
	End Property
	
	Private Sub Class_Initialize
	End Sub
	
	Private Sub Class_Terminate

	End Sub
End Class
%>