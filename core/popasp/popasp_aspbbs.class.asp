<%
' +----------------------------------------------------------------------
' | POPASP [ ASP MVC ]
' +----------------------------------------------------------------------
' | Copyright (c) 2016 http://popasp.com All rights reserved.
' +----------------------------------------------------------------------
' | Author: popasp <1737025626@qq.com>
' +----------------------------------------------------------------------
Class POPASP_ASPBBS
	'这个文件的代码块是为了服务于CMS
	Public content,currentPage,replaceContent,matchValue,listRepContent,htmlPageTotal,CommonTable,indexLinkPrefix,cmsPrefix,selfTpl,tplExecNum
	Private labelRule,sitePath,needCached,cacheLeftLabel,cacheRightLabel,cacheEndLabel
	Private repKey,dictLabel
	Private staticName,dataName,dictNewSort
	Private templateDir,labelStr,cms_home_path,authorDict,blockLoopMax,blockloopCounter
	Private cmsLabelPrefix,blockLabelDict,loop4dataName,loop4keyName
	Private escape_label,files_counter,compilePath,needCompiled
	Public loopStr,RealLinkPrefix,ClassType,innerTime
	
	Public page_firstlink,page_prevlink,page_nextlink,page_lastlink,page_total,page_pagesize,page_current,page_count,page_bar,page_numlist,page_prev,page_next,linkPrefix,baseTable,htmlFilePath
	
	Public fileExt,APP_DEBUG
	
	private xml_count
	
	'初始化类
	Private Sub Class_Initialize()
		on error resume next

		htmlPageTotal = 0
		sitePath = POP_MVC.config("sitePath")
		CommonTable = "table"
		Call initLinkPrefix
		cmsPrefix = POP_MVC.config("CMS_TAG_LABEL")	
		cmsLabelPrefix = POP_MVC.config("CMS_SELF_TAG_LABEL")	
		
		if POP_MVC.config("cms_home_path") <> "" then
			cms_home_path = POP_MVC.rtrim(POP_MVC.config("cms_home_path"),"/")
		else
			cms_home_path = POP_MVC.appPath
		end if
		
		loop4dataName = "data"
		loop4keyName  = "key"
		
		baseTable = "UserGroup"
		
		page_firstlink = "[page:firstlink]"
		page_prevlink = "[page:prevlink]"
		page_nextlink = "[page:nextlink]"
		page_lastlink = "[page:lastlink]"
		page_total = "[page:total]"
		page_pagesize = "[page:pagesize]"
		page_current = "[page:current]"
		page_prev = "[page:prev]"
		page_next = "[page:next]"
		page_count = "[page:count]"
		page_bar = "[page:bar]"
		page_numlist = "\[page:\d+\]"
		escape_label = "#QQ_1737025626_POPASP_ESCAPE_QQ_GROUP_124648143_#"
		files_counter = 0
		ClassType = typename(Me)
		staticName = POP_MVC.config("STATIC_NAME")
		dataName = POP_MVC.config("DATA_NAME")
		
		cacheLeftLabel = "<!--aspbbs:cache/"
		cacheRightLabel = "/-->"
		cacheEndLabel = "<!--/aspbbs:cache/-->"
		testStart = timer
		tplExecNum = 0
		compilePath = ""
		needCompiled = true
		innerTime = 0
		fileExt = ".html"
		htmlFilePath = "html"
		APP_DEBUG = POP_MVC.config("APP_DEBUG")
		xml_count = 0
	End Sub
	
	Private Sub Class_Terminate()
		set blockLabelDict = nothing
		set authorDict = nothing
		set dictLabel = nothing
	End Sub
	
	Public Function loadFile(Byval filePath)
		filePath = replace( filePath , "//" , "/" )
		loadFile = POP_MVC.file_get_contents( filePath )
	End Function
	
	Function file_get_contents( ByVal filepath )
	  dim stm
	  filePath = POP_MVC.realpath(filepath)
	  Set stm=server.CreateObject("Adodb.Stream")
	  stm.Type=2
	  stm.Mode=3
	  stm.Charset="utf-8"
	  stm.Open
	  stm.loadfromfile filePath
	  file_get_contents=stm.readtext
	  stm.Close
	  Set stm=Nothing
	End Function
	
	'加载文件
	Public Function load(Byval filePath)
		content=loadFile(filePath)
		templateDir = POP_MVC.file.dir( filePath )
	End Function	

	
	'解析模板文件，生成html
	Public Property Get parseHtml()	
		blockloopCounter = 0
		
		if needCompiled then
			'加载其他的控制器方法 Call A_("ctrl/method")，只需要写{iaspcms:action:ctrl/method}
			parseAction()
		
			'加载头部与尾部文件
			parseTopAndFoot()
			'加载引用模板文件
			parseAuxiliaryTemplate() 
			'parseMvcTpl()

			RemoveComment()	
			
			if guestConfig.html_compress - 1 =0 then
				'content = POP_MVC.String.reg_replace ( content , "$1" , "\s*[-|_]*\s*Powered\s+By[^<>]+?(</title>)" , "mi" ) 
				content = POP_MVC.String.reg_replace ( content , " - Powered By " + POP_MVC.config("CMS_POWER_NAME") + " " + POP_MVC.config("CMS_VERSION") + "$1" , "(</title>)" , "mi" ) 			
				content = POP_MVC.String.HtmlCompress(content)
			end if
		end if
	
		Call parseLabel
		
		if guestConfig.html_compress - 1 =0 then
			content = POP_MVC.String.HtmlCompress(content)
		end if
		
		if POP_MVC.config("sitePath") <> "" then
			Content = POP_MVC.String.reg_replace( Content , "$1" +  POP_MVC.config("sitePath") + "/" + POP_MVC.config("CMS_UPLOAD_NAME") + "/" ,  "(""|')/" + POP_MVC.config("CMS_UPLOAD_NAME") + "/" , "gim" )
		end if	
		
		blockloopCounter = 0
	End Property
	
	'解析Call A_("ctrl/method")
	'{iaspcms:action:ctrl/method}
	Public Property Get parseAction()
		on error resume next
		dim labelRule,match,matches
		if inStr( content,"{" + cmsPrefix + ":action:" ) > 0 then
			labelRule = "\{" + cmsPrefix + ":action:\s*([^}]+)(?!{" + cmsPrefix + ":)}"		
			set matches = POP_MVC.String.reg_exec( content,labelRule , "igm" )		
			for each match in matches
				labelStr = match.SubMatches(0)				
				execute( "Call A_(""" + labelStr + """)" )
				content = replace( content, match.value ,"" )
				if err.number <> 0  then
					POP_MVC.error( match + "--" + err.description )
				end if
			next
			set matches = nothing
		end if	
	End Property
	
	'删掉注释
	Public Property Get RemoveComment
		dim left_delimiter,right_delimiter
		' The left delimiter used for the template tags.
		left_delimiter  =  "{"

		' The right delimiter used for the template tags.
		right_delimiter =  "}"	
		
		' 一行注释的替换 {//注释内容 }
		content = POP_MVC.String.reg_replace( content , "", left_delimiter + "//.*?" + right_delimiter , "gim")
		
		' 多注释的替换 {/*注释内容*/}
		content = POP_MVC.String.reg_replace( content , "" , left_delimiter + "\/\*[\s\S]*?\*\/" + right_delimiter , "gim")	
	End Property
	
	'输出
	Public Property Get Output
		Response.write Content
	End Property 	
	
	Function getComPath( templatePath )
		getComPath = cms_home_path + "/Runtime/Compile/" + guestConfig.DefaultTemplate + "/" + replace( templatePath, "/" , "_" )
	End Function
	
	Function IsCompiled( templatePath )
		dim comPath,cacheLife
		cacheLife = guestConfig.ListCacheLifeTime
		if cacheLife - 0 <> 0 then
			comPath = getComPath( templatePath )
			if not POP_MVC.file.isFile( comPath ) then	
				compilePath = comPath
				needCompiled = true
			elseif dateCompare(now(),POP_MVC.file.mtime( comPath )) - cacheLife > 0 then
				compilePath = comPath
				needCompiled = true
			else	
				needCompiled = false							
			end if						
		end if
		IsCompiled = not needCompiled
	End Function
	
	'生成解析文件，即将替换模板包含后的文件保存起来
	sub CompileContent( templatePath )
		Call IsCompiled( templatePath )
		
		if needCompiled then
			Call load(templatePath)	
		else
			content = POP_MVC.file_get_contents( getComPath( templatePath ) )	
		end if	
	end Sub
	
	Public Property Get BbsShow( template )
		dim templatePath	
		templatePath = "templates/bbs/html/" + template + ".html"

		Call CompileContent( templatePath )
		
		Call parseHtml
		Call output
	End Property
	
	Public Property Get Show( template )
		dim templatePath
		
		templatePath= getTemplatePath( template )		
		
		Call CompileContent( templatePath )

		Call parseHtml
		Call output
	End Property
	
	sub pushDict( dict , key )
		if key = "" then exit sub
		if dict.exists( key ) then
			dict(key) = dict(key) + 1
		else
			dict(key) = 1
		end if
	end sub
	
	Public Function AnalyseLabel( template )
		on error resume next
		dim templatePath,ruleVar,pattern,pattern1,dict,content,theLabel,srcDict
		dim labelRule
		dim matches,match,tempStr,labelStr
		dim blockLabelDict,k,key,html
		Dim srcTemplate,tplPath,src,flag
		dim matchesfield,matchfield,loopStr
		
		'templatePath= getTemplatePath( template )	
		
		templateDir = POP_MVC.file.dir( template )
		

		
		content = POP_MVC.file_get_contents(template)
	
		set dict = D_
		
		theLabel = getTagLabel("top")
		if inStr(content, theLabel ) > 0 then 
			if POP_MVC.file.isFile( templateDir + "top" + fileExt ) then
				content=replace(content,theLabel,loadFile( templateDir + "top" + fileExt ))
			elseif POP_MVC.file.isFile( templateDir + "head" + fileExt ) then
				content=replace(content,theLabel,loadFile( templateDir + "head" + fileExt ))
			end if
			
			Call pushDict( dict, "模板 " & theLabel )
		end if
		
		theLabel = getTagLabel("head")
		if instr(content,theLabel) > 0 then 
			if POP_MVC.file.isFile( templateDir + "head" + fileExt ) then
				content=replace(content,theLabel,loadFile( templateDir + "head" + fileExt))
			elseif POP_MVC.file.isFile( templateDir + "head" + fileExt ) then
				content=replace(content,theLabel,loadFile( templateDir + "top" + fileExt))
			end if
			Call pushDict( dict,  "模板 " & theLabel )
		end if
		
		theLabel = getTagLabel("left")
		if inStr(content,theLabel) > 0 then 
			content=replace(content,theLabel,loadFile( templateDir + "left" + fileExt))
			Call pushDict( dict,  "模板 " & theLabel )
		end if
		
		theLabel = getTagLabel("comm")
		if inStr(content,theLabel) > 0 then 
			content=replace(content,theLabel,loadFile( templateDir + "comm" + fileExt))	
			Call pushDict( dict,  "模板 " & theLabel )
		end if

		pattern = "\{" + cmsPrefix + ":template([\s\S]*?)\}"
		k = 0
		do while inStr( content ,"{" + cmsPrefix + ":template" ) > 0 and k < 1000
			set matches = POP_MVC.String.reg_exec(content,pattern,"gim")
			for each match in matches
				flag = false
				set srcDict = parseArr(match.SubMatches(0))		

				
				if srcDict.Exists( "rsrc" ) then
					src = srcDict("rsrc")
					flag = true
				elseif srcDict.Exists( "src" ) then
					src = srcDict("src")
				end if
				srcTemplate = src			

				if mid( srcTemplate , 1 , 1) = "$" then
					srcTemplate = V_( mid( srcTemplate , 2) )
				end if
				

				srcTemplate = POP_MVC.rtrim( srcTemplate , fileExt ) + fileExt
			
				if flag then
					tplPath = "templates/" + srcTemplate
				elseif inStr( srcTemplate , "/" ) > 0 then
					tplPath = getTemplatePath( srcTemplate )
				else
					tplPath = templateDir + srcTemplate
				end if

				Call pushDict( dict,  "模板 " & match.value ) 
	
				content=replace(content,match.value,loadFile( tplPath ))
					
			next
			set matches = nothing
			k = k + 1
		loop

		
if inStr( content,"{:"  ) > 0 then
		ruleVar = "[\w\(\)\.\""\u0391-\uFFE5]+"
		pattern = l_d + "\s*([\:]+)([\s\S]+?)" + r_d
		pattern1 = l_d + "\s*[\:]\$(" + ruleVar + ")\|remove\s*" + r_d
		set dict = D_

		'匹配变量删除
		set matches = POP_MVC.String.reg_exec(content, pattern1  ,"gmi")
		for each match in matches
			Call pushDict( dict, match.value ) 
		next
		set matches = nothing
			
		'匹配变量分配
		set matches = POP_MVC.String.reg_exec(content, pattern  ,"gm")
		for each match in matches
			Call pushDict( dict, match.value )
		next
		set matches = nothing
end if

		labelRule = "\{" + cmsPrefix + ":\s*(guestConfig\.|_C\.|)([\w]+)(?!{" + cmsPrefix + ":)}"
		set matches = POP_MVC.String.reg_exec( content,labelRule , "igm" )	
		
		for each match in matches
			tempStr = match.SubMatches(0)
			labelStr = match.SubMatches(1)
			if tempStr = "_C." then
				labelStr = POP_MVC.config( labelStr )			 
				content = replace( content, match.value ,labelStr )
				Call pushDict( dict, "系统设置 " & match.value )
			end if
		next			
		
		for each match in matches
			tempStr = match.SubMatches(0)
			labelStr = match.SubMatches(1)
			if tempStr = "guestConfig." then
				Execute "labelStr = " + tempStr + labelStr			 
				content = replace( content, match.value ,labelStr )
				Call pushDict( dict, "后台配置 " & match.value )
			end if
		next	

		
		for each match in matches
			tempStr = match.SubMatches(0)
			labelStr = match.SubMatches(1)
			if tempStr = "" then				
				labelStr = LCase(labelStr)
				if inStr( ",sitepath,sitetplpath,version,sitelogo,checkcode,checkimg,indexasp,indexprefix,extendprefix,ctrlprefix,realindexprefix,siteurl,homepage,runtime,totalquery,runinfo,enruninfo," , "," & labelStr & "," ) > 0 then
					Call pushDict( dict, "默认设置 " & match.value )
				end if
			end if
		next
		content = POP_MVC.String.reg_replace( content,"" , labelRule , "igm" )	
					
		
		labelRule = "\{" + cmsPrefix + ":\s*([_]GET\.|[_]REQ\.|[_]S\.|)([\w]+)(?!{" + cmsPrefix + ":)}"
		set matches = POP_MVC.String.reg_exec( content,labelRule , "igm" )	
	
		for each match in matches
			tempStr = match.SubMatches(0)
			labelStr = match.SubMatches(1)
			if tempStr = "_GET." then
				Call pushDict( dict, "GET参数 " & match.value )
			end if
		next
		
		for each match in matches
			tempStr = match.SubMatches(0)
			labelStr = match.SubMatches(1)
			if tempStr = "_REQ." then
				Call pushDict( dict, "REQ参数 " & match.value )
			end if
		next
		
		for each match in matches
			tempStr = match.SubMatches(0)
			labelStr = match.SubMatches(1)
			if tempStr = "_S." then
				Call pushDict( dict, "SESSION " & match.value )
			end if
		next
		
		for each match in matches
			tempStr = match.SubMatches(0)
			labelStr = match.SubMatches(1)
			if tempStr = "" then
				Call pushDict( dict, "分配变量 " & match.value )
			end if
		next
		set matches = nothing
		
		
		content = POP_MVC.String.reg_replace( content,"" , labelRule , "igm" )
		
		labelRule = "\{" + cmsPrefix + ":\s*([\w]+[.][\w]+)(?!{" + cmsPrefix + ":)}"		
		set matches = POP_MVC.String.reg_exec( content,labelRule , "igm" )			
		for each match in matches
			Call pushDict( dict, "分配对象" & match.value )
		next		
		set matches = nothing	
		content = POP_MVC.String.reg_replace( content,"" , labelRule , "igm" )

		'解析{aspbbs:func:POP_MVC.runtime} {aspbbs:execute:SUBNAME arg1,arg2}
		if inStr( content,"{" + cmsPrefix + ":func:" ) > 0 or inStr( content,"{" + cmsPrefix + ":execute:" ) > 0 then
			labelRule = "\{" + cmsPrefix + ":(?:func|execute):\s*([^}]+)(?!{" + cmsPrefix + ":)}"	
			set matches = POP_MVC.String.reg_exec( content,labelRule , "igm" )					
			for each match in matches
				Call pushDict( dict, "方法 " &  match.value )
			next
			set matches = nothing
		end if	
	
		'解析块标签
		set blockLabelDict = getBlockLabelsDict(content,"","")

		for each k in blockLabelDict			
			set match = blockLabelDict(k)	
			loopStr = match("content")
			key = match("label")			
			if key	= "cache" then
				tempstr = match("left_flag") & "..." &  "{/" + cmsPrefix & ":" & key & "}"
				Call pushDict( dict, "缓存 " &  tempstr )
			End if			
		next	
		


		for each k in blockLabelDict			
			set match = blockLabelDict(k)	
			loopStr = match("content")
			key = match("label")			
			if key	<> "cache" then
				set matchesfield = getInnerLabelsDict( loopStr , key )
				tempstr = match("left_flag")
				set srcDict = D_
				
				for each labelStr in matchesfield
					if isObject( matchesfield(labelStr) ) then
						set matchfield = matchesfield(labelStr)
						tempStr = tempStr & " [" & key & ":" & matchfield("label") & "] "
					end if
				next
				tempStr = tempStr &  "{/" + cmsPrefix & ":" & key & "}"
				Call pushDict( dict,"遍历块 " & tempstr )
			End if			
		next		
		set matchesfield = nothing
		set blockLabelDict = nothing
		
	
		
		set AnalyseLabel = dict
	End Function
	
	
	
	'解析头部和底部
	Public Property Get parseTopAndFoot()
		dim theLabel
		
		theLabel = getTagLabel("top")
		if inStr(content, theLabel ) > 0 then 
			if POP_MVC.file.isFile( templateDir + "top" + fileExt ) then
				content=replace(content,theLabel,loadFile( templateDir + "top" + fileExt ))
			elseif POP_MVC.file.isFile( templateDir + "head" + fileExt ) then
				content=replace(content,theLabel,loadFile( templateDir + "head" + fileExt ))
			end if
		
		end if
		
		theLabel = getTagLabel("head")
		if instr(content,theLabel) > 0 then 
			if POP_MVC.file.isFile( templateDir + "head" + fileExt ) then
				content=replace(content,theLabel,loadFile( templateDir + "head" + fileExt))
			elseif POP_MVC.file.isFile( templateDir + "head" + fileExt ) then
				content=replace(content,theLabel,loadFile( templateDir + "top" + fileExt))
			end if
		end if
		theLabel = getTagLabel("left")
		if inStr(content,theLabel) > 0 then content=replace(content,theLabel,loadFile( templateDir + "left" + fileExt))
		
		theLabel = getTagLabel("comm")
		if inStr(content,theLabel) > 0 then content=replace(content,theLabel,loadFile( templateDir + "comm" + fileExt))
	End Property

	'解析辅助模板
	Public Property Get parseAuxiliaryTemplate()
		Dim pattern,matches,match,srcTemplate,tplPath,src,dict,flag,k
		pattern = "\{" + cmsPrefix + ":template([\s\S]*?)\}"
		k = 0
		do while inStr( content ,"{" + cmsPrefix + ":template" ) > 0 and k < 1000
			set matches = POP_MVC.String.reg_exec(content,pattern,"gim")
			for each match in matches
				flag = false
				set dict = parseArr(match.SubMatches(0))				
				if dict.Exists( "rsrc" ) then
					src = dict("rsrc")
					flag = true
				elseif dict.Exists( "src" ) then
					src = dict("src")
				end if
				srcTemplate = src			

				if mid( srcTemplate , 1 , 1) = "$" then
					srcTemplate = V_( mid( srcTemplate , 2) )
				end if
				srcTemplate = POP_MVC.rtrim( srcTemplate , fileExt ) + fileExt
				
				if flag then
					tplPath = "templates/" + srcTemplate
				elseif inStr( srcTemplate , "/" ) > 0 then
					tplPath = getTemplatePath( srcTemplate )
				else
					tplPath = templateDir + srcTemplate
				end if
				content=replace(content,match,loadFile( tplPath ))
			next
			set matches = nothing
			k = k + 1
		loop
	End Property
	
	'解析辅助模板
	'加载框架中模板文件，并解析
	Public Property Get parseMvcTpl()
		Dim pattern,matches,match,srcTemplate,k
		pattern = "\{" + cmsPrefix + ":tpl([\s\S]*?)\}"
		k = 0 
		do while inStr( content , "{" + cmsPrefix + ":tpl" )  > 0 and k < 1000
			set matches = POP_MVC.String.reg_exec(content,pattern,"gim")		
			for each match in matches			
				srcTemplate = parseArr(match.SubMatches(0))("src")	
				
				content=replace(content,match,loadFile( getTplPath(srcTemplate) ))
			next
			set matches = nothing
			k = k + 1
		loop
	End Property
	

	'解析标签
	Public Property Get ParseLabel
		on error resume next
		dim matches,match,labelRule,settingLables,tempStr,cachePath,key,startTime,labelStrKey,extendName,k

		extendName = split( POP_MVC.c , "--" )(0)
		
		if inStr( content,"{:"  ) > 0 then
			Call parseExecExp( "{" , "}" )
		end if
		
		'自定义标签替换
		'if inStr( content, "{" + cmsLabelPrefix + ":" ) > 0 then
		'	Call ParseSelfLabel
		'end if	
		
		startTime = timer
		
		blockloopCounter = 0
		BlockLoopMax = 10
	
		if needCompiled then
			'{" & cmsPrefix & ":sitepath} 网站终极目录(可放在二级目录,其它语言则在三级目录)
			'{" & cmsPrefix & ":username} 当前登陆用户
			'{" & cmsPrefix & ":userright} 用户权限，用户权限的读取0为超级管理员，1为注册用户，2为游民
			'{label:****} 自定义标签
			'{" & cmsPrefix & ":onlineservice} 在线客服
			'{" & cmsPrefix & ":kf} 其它客服系统
			'{" & cmsPrefix & ":floatad} 漂浮广告
			'{" & cmsPrefix & ":coupletad} 对联广告
			'{" & cmsPrefix & ":windowad} 弹出广告
			'{" & cmsPrefix & ":onekeyshare} 在文章中可调用一键分享
			'{" & cmsPrefix & ":version}	程序版本信息
			'1.解析{aspbbs:guestConfig.XXX}
			'  解析{aspbbs:_C.XXX}
			'  解析{aspbbs:XXX}其中XXX是系统计算的常量，而非分配量			
			labelRule = "\{" + cmsPrefix + ":\s*(guestConfig\.|_C\.|)([\w]+)(?!{" + cmsPrefix + ":)}"
			set matches = POP_MVC.String.reg_exec( content,labelRule , "igm" )	
			startTime = timer
			for each match in matches
				tempStr = match.SubMatches(0)
				labelStr = match.SubMatches(1)
				if tempStr = "guestConfig." then
					Execute "labelStr = " + tempStr + labelStr			 
					content = replace( content, match.value ,labelStr )
				elseif tempStr = "_C." then
					labelStr = POP_MVC.config( labelStr )			 
					content = replace( content, match.value ,labelStr )
				else					
					labelStrKey = LCase(labelStr)
					'此处不用select case，是为了提升性能，加快速度
					if labelStrKey = "sitepath" then
						content = replace( content, match.value ,sitePath )
					elseif labelStrKey = "sitetplpath" then
						content = replace( content, match.value ,sitePath + "/Templates/" + guestConfig.DefaultTemplate )
					elseif labelStrKey = "version" then
						content = replace( content, match.value ,C_("CMS_VERSION") )
					elseif labelStrKey = "sitelogo" then
						content = replace( content, match.value ,guestConfig.SiteLogo )			
					elseif labelStrKey = "checkcode" then
						content = replace( content, match.value ,P_("auto").cmsverify )
					elseif labelStrKey = "checkimg" then
						content = replace( content, match.value ,P_("auto").cmsverify4code )
					elseif labelStrKey = "indexasp" then
						content = replace( content, match.value ,left(indexLinkPrefix , len(indexLinkPrefix)-1 ) )
					elseif labelStrKey = "indexprefix" then
						content = replace( content, match.value ,linkPrefix )
					elseif labelStrKey = "extendprefix" then
						content = replace( content, match.value ,linkPrefix + extendName )
					elseif labelStrKey = "ctrlprefix" then
						content = replace( content, match.value ,linkPrefix + POP_MVC.c )
					elseif labelStrKey = "realindexprefix" then
						content = replace( content, match.value ,RealLinkPrefix )
					elseif labelStrKey = "siteurl" then
						content = replace( content, match.value ,POP_MVC.http_host() )
					elseif labelStrKey = "homepage" then
						content = replace( content, match.value ,getHomePage() )
							
						'''''下面这几个是最后才替换的，所以先“保护”起来，>2.5版，2021.1.20;enruninfo > 1.5，2023-7-29 22:00
					elseif labelStrKey = "runtime" or labelStrKey = "totalquery" or labelStrKey = "runinfo" or labelStrKey =  "enruninfo" then
						content = replace( content, match.value ,escape_label + labelStrKey )
					end if
				end if
			next
			tplExecNum = tplExecNum + 1			
			Call POP_MVC.cmsPushTime( startTime,"替换配置(" + CStr(matches.count) + ") " + cmsPrefix + ":guestConfig.XXX/_C.XXX/系统常量XXX}" )
			set matches = nothing
		end if
		
		'到这儿就可以保存解析文件了
		if compilePath <> "" then
			Call POP_MVC.file_put_contents_without_bom( compilePath , content )
		end if
		

		'2. 匹配变量，{aspbbs:_GET.id}、{aspbbs:_S.adminId}、{aspbbs:XXX}
		startTime = timer
		labelRule = "\{" + cmsPrefix + ":\s*([_]GET\.|[_]REQ\.|[_]S\.|)([\w]+)(?!{" + cmsPrefix + ":)}"
		set matches = POP_MVC.String.reg_exec( content,labelRule , "igm" )			
		for each match in matches
			tempStr = match.SubMatches(0)
			labelStr = match.SubMatches(1)
			if tempStr = "_GET." then
				labelStr = POP_MVC.get( labelStr )
				content = replace( content, match.value ,labelStr )
			elseif tempStr = "_REQ." then
				labelStr = POP_MVC.req( labelStr )
				content = replace( content, match.value ,labelStr )
			elseif tempStr = "_S." then
				labelStr = session( labelStr )
				content = replace( content, match.value ,labelStr )
			else
				if APP_DEBUG = 1 then
					if POP_MVC.tpl_vars.exists( labelStr ) then 
						tempStr = POP_MVC.tpl_vars( labelStr )
						if isNull( tempStr ) then
							tempStr = ""
						end if
						content = replace( content, match.value ,tempStr )
					else
						POP_MVC.error(  "未找到变量，不能替换" + match )
					end if
				else
					tempStr = POP_MVC.tpl_vars( labelStr )
					if isNull( tempStr ) then
						tempStr = ""
					end if
					content = replace( content, match.value ,tempStr )
				end if	
			end if	
				
		next
		
		tplExecNum = tplExecNum + 1
		Call POP_MVC.cmsPushTime( startTime , "替换单标签(" & matches.count & ") {" + cmsPrefix +  ":_GET.XXX/_S.XXX/分配变量XXX" + "}"  )
		set matches = nothing
		
		'3. 匹配分配变量，还有{aspbbs:thread.Title}
		startTime = timer		
		labelRule = "\{" + cmsPrefix + ":\s*([\w]+[.][\w]+)(?!{" + cmsPrefix + ":)}"		
		set matches = POP_MVC.String.reg_exec( content,labelRule , "igm" )			
		for each match in matches
			labelStr = match.SubMatches(0)
			Execute "tempStr = V_(""" & labelStr & """)"
			tempStr = repNull( tempStr )
			content = replace( content, match.value , tempStr )
		next		
		tplExecNum = tplExecNum + 1
		Call POP_MVC.cmsPushTime( startTime,"替换单标签(" & matches.count & ")  {" + cmsPrefix + ":XXXX.YYY}" )
		set matches = nothing
		
		if APP_DEBUG = 1 then
		    '解析{aspbbs:sitepath}这样的标签，这类标签不含从that.d分配过来的变量
			labelRule = "\{" + cmsPrefix + ":\s*(\w+)(?!{" + cmsPrefix + ":)}"		
			set matches = POP_MVC.String.reg_exec( content,labelRule , "igm" )	
			for each match in matches
				POP_MVC.error(  "未找到变量，不能替换" & match )
			next
			set matches = nothing
		end if
		

		Call parseBlockLabel("cache")
		
		'解析{aspbbs:func:POP_MVC.runtime} {aspbbs:execute:SUBNAME arg1,arg2}
		if inStr( content,"{" + cmsPrefix + ":func:" ) > 0 or inStr( content,"{" + cmsPrefix + ":execute:" ) > 0 then
			labelRule = "\{" + cmsPrefix + ":(?:func|execute):\s*([^}]+)(?!{" + cmsPrefix + ":)}"	
			set matches = POP_MVC.String.reg_exec( content,labelRule , "igm" )					
			for each match in matches
				labelStr = match.SubMatches(0)		
				tempStr = ""
				if inStr(labelStr , "[") < 1 and  inStr(labelStr , "]") < 1 then
					labelStr = match.SubMatches(0)
					
					labelStr = getMvcTplVars( labelStr )	

					if inStr( match.value, ":func:" ) > 0 then					
						execute( "tempStr =" + labelStr )	
					elseif inStr( match.value, ":execute:" ) > 0 then
						execute( labelStr )
					end if
					content = replace( content, match.value ,tempStr )
				end if
				if err.number <> 0   then
					POP_MVC.error( match + "--" + err.description ) 
				end if
				err.clear
			next
			Call POP_MVC.cmsPushTime( startTime,"替换单标签{" + cmsPrefix + ":func/execute:method}" )
			set matches = nothing
		end if		
		
		'解析{isaspcms:arealist}，因cms不同
		'Call parseAreaList

	
		'Call parseBlockLabel("navlist")
		Call parseBlockLabel("")		
		
		if inStr( content, "{/" + cmsPrefix + ":" ) > 0 then
			Call parseBlockLabel("")
		end if
	
		if inStr( content,"{" + cmsPrefix + ":func:" ) > 0 then
			labelRule = "\{" + cmsPrefix + ":func:\s*([^}]+)(?!{" + cmsPrefix + ":)}"		
			set matches = POP_MVC.String.reg_exec( content,labelRule , "igm" )		
			for each match in matches
				labelStr = match.SubMatches(0)
				labelStr = match.SubMatches(0)
				labelStr = getMvcTplVars( labelStr )				
				execute( "tempStr =" + labelStr )
				content = replace( content, match.value ,tempStr )
				
				if err.number <> 0 then
					POP_MVC.error( match + "--" + err.description )
				end if
			next
			set matches = nothing
		end if
		
		if inStr( content, "{if" ) > 0 then
			parseIf("")
		end if
		
		k = 0
		do while inStr( content , cacheEndLabel ) > 0 and k < 1000
			key	= POP_MVC.String.getInStr( content,cacheLeftLabel,cacheRightLabel )
			
			match = POP_MVC.String.getInStr( content , cacheLeftLabel + key + cacheRightLabel , cacheEndLabel )
			needCached = true			
			cachePath = cms_home_path + "/Runtime/Cache/" + guestConfig.DefaultTemplate + "/" + key + ".txt"
			

			Call cache_put_contents( cachePath , match )
			content = replace( content,cacheLeftLabel + key + cacheRightLabel,"<!-- ASPBBS CACHE SAVED ( " & now() & " ) -->" ,1,1 )
			content = replace( content,cacheEndLabel,"<!-- ASPBBS CACHE END -->" ,1,1 )
			k = k + 1
		loop		
		
		'''''将前面保护起来的几个单标签进行替换
		content = replace( content, escape_label + "runtime" , POP_MVC.runtime )
		content = replace( content, escape_label + "totalquery" , POP_MVC.num_db_query )
		content = replace( content, escape_label + "runinfo" , "运行" & POP_MVC.runtime & "秒&nbsp;"  + "标签:"  & tplExecNum  & "&nbsp;" + POP_MVC.config("DB_TYPE") & ":" & POP_MVC.num_db_query   )		
		content = replace( content, escape_label + "enruninfo" , "Processed: <b>" & POP_MVC.runtime & "</b> , Parsed: <b>" & tplExecNum & "</b> , " + POP_MVC.config("DB_TYPE") + ": <b>" & POP_MVC.num_db_query & "</b>" )		
		
		'var_Export dContainer
		Call L_( ClassType + ".parseLabel" )
	End Property
	
	
	'解析块标签
	Public Property Get parseBlockLabel( labelName )
		'on error resume next
		dim match,loopCounter,cachePath,key,k,SortID,moreParse,rsObj,cachekey,full_content

		if blockloopCounter >= BlockLoopMax then
			exit Property
		end if
		moreParse = array()
		
		SortID = POP_MVC.get("SortID")
		
		loopCounter = 0
	
		'解析块标签
		set blockLabelDict = getBlockLabelsDict(content,labelName,"")



		for each k in blockLabelDict			
			set match = blockLabelDict(k)			
				
			key = match("label")
if inStr( content,match("left_flag") ) > 0 then
			cachePath = ""
			cachekey = ""
			needCached = false
			full_content = match("full_content")
			replaceContent = match("full_content")
			matchValue = match("full_content")
			labelStr = match("attr")
			loopStr = POP_MVC.String.HtmlCompress(match("content"))
			
			if inStr( match("left_flag") , "cachekey" ) > 0 then
				cachekey = POP_MVC.String.reg_find( match("left_flag") , "\bcachekey=([^\s]+?)\b" , 1, "i" )
				cachePath = cms_home_path + "/Runtime/Cache/" + guestConfig.defaultTemplate + "/" + cachekey + ".txt"
			end if
			
			
				if key	= "looparr" then
					Call ParseData( "looparr" , "" )						
					content = replace( content, full_content , replaceContent )
				elseif key	= "loopdata" then
					Call ParseData( key , "" )					
					content = replace( content, full_content , replaceContent )
				elseif key	= "file" then
					Call ParseFiles( POP_MVC.get("page") , "file" , "" )					
					content = replace( content, full_content , replaceContent )
				elseif key	= "xml"	 then
					Call ParseXml( POP_MVC.get("page") , "xml" , "" )
					content = replace( content, full_content , replaceContent )
				elseif key	= "cache" then
						if is_cached( cachePath, match("left_flag") ) then						
							if guestConfig.CacheType - 0 = 0 then
								content = replace( content, full_content , "<!-- ASPBBS CACHE START ( APPLICATION:" & P_("APPLICATION").getTime( cachePath ) & " ) FOR " & guestConfig.ListCacheLifeTime & "s -->" + P_("APPLICATION").get( cachePath ) + "<!-- ASPBBS CACHE END -->" )
							else
								content = replace( content, full_content , "<!-- ASPBBS CACHE START ( FILE:" & POP_MVC.file.mtime(cachePath) & " ) FOR " & guestConfig.ListCacheLifeTime & "s -->" & Me.loadFile(cachePath) & "<!-- ASPBBS CACHE END -->" )
							end if
						else
							listRepContent =  full_content
							if needCached then
								listRepContent = replace( listRepContent, match("left_flag") , cacheLeftLabel + cachekey + cacheRightLabel )
								listRepContent = replace( listRepContent, "{/aspbbs:cache}" , cacheEndLabel )
							else
								listRepContent = replace( listRepContent, match("left_flag") , "" )
								listRepContent = replace( listRepContent, "{/aspbbs:cache}" , "" )
							end if
							content = replace( content, full_content ,listRepContent )
						end if	

				elseif key	= CommonTable or key = "looprs" then
						Call Me.parseList( SortID , POP_MVC.get("page") ,key,"", key )
						Me.content = replace( Me.content, full_content , replaceContent )	
						Call cache_put_contents( cachePath , replaceContent )
				else

				End if
				
else
	POP_MVC.arr.push moreParse , key
end if

				loopCounter = loopCounter + 1
		next	
		
		
		loopCounter = loopCounter + 1
		
		blockloopCounter = blockloopCounter + 1
		

		if ubound(moreParse) > -1 then
			Call parseBlockLabel( moreParse )
		end if

		Call L_( ClassType + ".parseBlockLabel" )
	End Property
	
	Private Sub cache_put_contents( cachePath,ByVal cacheContent )
		if needCached then 
			if guestConfig.CacheType - 0 = 0 then
				Call P_("APPLICATION").Cache( cachePath , guestConfig.ListCacheLifeTime - 0 , cacheContent )
			elseif guestConfig.CacheType - 0 <> 0 then			
				call POP_MVC.file_put_contents_without_bom( cachePath , cacheContent )
			end if
		end if
	End Sub
	
	'清除缓存
	Public Property Get clearCompile
		Call POP_MVC.file.remove(cms_home_path + "/Runtime/Compile/")	
	End Property
	
	'清除缓存
	Public Property Get clearMemory
		Call P_("APPLICATION").RemovePrefix(cms_home_path + "/Runtime/Cache/")
	End Property	
	
	
	'清除缓存
	Public Property Get clearCache
		Call clearMemory
		Call POP_MVC.file.remove(cms_home_path + "/Runtime/Cache/")	
	End Property
	
	'缓存个数
	Public Property Get cacheCount
		cacheCount = POP_MVC.file.FileCount(cms_home_path + "/Runtime/Cache/")	
	End Property
	
	'清除过程数据
	Public Property Get clearRuntime
		Call clearMemory
		Call POP_MVC.file.remove(cms_home_path + "/Runtime/")	
	End Property	
	
	'判断模板中的块是否在缓存期内
	Private function is_cached( byval cachePath ,  byref left_flag )
		dim cacheLife
		needCached = false
		
		
		is_cached = false
		cacheLife = guestConfig.ListCacheLifeTime - 0
		
		if cacheLife < 1 then
			is_cached = false
			Exit Function
		end if
		
		needCached = true
		'		缓存文件不存在
		if guestConfig.CacheType - 0 = 0 and not P_("APPLICATION").IsCached( cachePath , cacheLife ) then
			is_cached = false
			Exit Function			
		'				缓存文件过期
		ElseIf guestConfig.CacheType - 0 <> 0 Then	
			if Not POP_MVC.File.isExists( cachePath )  Then
				is_cached = false
				Exit Function
			end if
			if dateCompare(now(),POP_MVC.file.mtime( cachePath )) - cacheLife > 0 then
				is_cached = false
				Exit Function
			end if
		End If	
		
		is_cached = true
		needCached = false				
	End Function
	
	'程序中判断某个文件是否缓存
	Function isCached( cacheKey, cacheLife )
		dim cache_lifetime,cachePath

		if isNul( cacheLife ) then
			cache_lifetime = guestConfig.ListCacheLifeTime - 1
		else 
			cache_lifetime = cacheLife
		end if
		isCached = false
		

		if cache_lifetime - 0 <= 0 then
			exit function
		end if
		
		cachePath = cms_home_path + "/Runtime/Cache/" + guestConfig.DefaultTemplate + "/" + cacheKey + ".txt"
		
		if guestConfig.CacheType - 0 = 0 then
			isCached = P_("APPLICATION").IsCached( cachePath , cache_lifetime )
			exit function
		elseif guestConfig.CacheType - 0 <> 0 then				
			if not POP_MVC.file.isFile( cachePath ) then
				exit function
			end if
			
			If dateCompare(now(),filemtime(cachePath)) > cache_lifetime Then	
				Exit Function
			End If
		end if		
		isCached = true	
	End Function
	
	Function filemtime(filename)
		filemtime = POP_MVC.File.mtime(filename)
	End Function
	
	'如果date1>date2返回正，否则返回负数
	Function dateCompare( date1,date2 )
		dateCompare = DateDiff("s",date2,date1)
	End Function
	
	Public Property Get getSelfLabel(labelName)
		if isEmpty( dictLabel ) then 
			set dictLabel = B_( baseTable ).from("{prefix}Labels").field("LabelName,LabelContent").getkeyvalue
		end if
		getSelfLabel = dictLabel( labelName )
	end Property
	
	'解析自定义标签替换
	Public Property Get ParseSelfLabel
		dim labelRule,match,matches,repKey,tempStr,arr,keys
		if isEmpty( dictLabel ) then 
			set dictLabel = B_( baseTable ).from("{prefix}Labels").field("LabelName,LabelContent").getkeyvalue
		end if
		if dictLabel.count > 0 then
			labelRule = "{" + cmsLabelPrefix + ":([\s\S]*?)}"	
			set matches = POP_MVC.String.reg_exec( content,labelRule , "igm" )		
			for each match in matches
				repKey = match.SubMatches(0)
				if dictLabel.exists( repKey ) then
					tempStr = dictLabel(repKey)
					tempStr = POP_MVC.String.decodeHtml(tempStr)
					tempStr = POP_MVC.trim( tempStr, "$$$" )
					'如果在自定义标签内容中发现$$$，则以$$$分割，进行随机显示，否则直接替换
					if inStr( tempStr, "$$$" ) > 0 then
						arr = split(tempStr,"$$$")
						
						if guestConfig.ListCacheLifeTime > 0 then
							keys = "POPASP_SELF_LABEL_" + B_( baseTable ).from("Labels").where("LabelName ='" + repKey + "'").field("LabelID").getOne
							tempStr = "var " + keys + " = " + js_encode( arr ) + ";" + vbCrLf
							tempStr = tempStr + keys + " = " + keys + "[Math.floor(Math.random()*" + keys + ".length)];" + vbCrLf
							tempStr = tempStr + "document.write(" + keys + ");" + vbCrLf
							tempStr = "<script type=""text/javascript"">" + vbcrlf + tempStr
							tempStr = tempStr +	"</script>"								
						else
							Randomize
							tempStr = Arr(CLng(Rnd * ubound(arr)))
						end if
					end if
					content = replace( content, match.value, tempStr )
				else
					POP_MVC.warning("有未解析的标签{" + cmsLabelPrefix + ":" + repKey + "}")
				end if
			next
			set matches = nothing
		end if
	End Property
	
	Public Sub ParseCommonTable( id, tableName )
		matchValue = content
		labelStr = ""
		loopStr = content
		Call ParseList( id , 0, CommonTable, tableName ,"" )	
	End Sub
	
	'分页条
	Function replacePageLabel(ByVal str ,ByVal dTotal, ByVal dPerpage, ByVal dPage, ByVal dID, ByVal sLinkType, ByVal sShowType, ByVal sKeys )
		'on error resume next
		dim pageLabel,PageCount,obj,k,matches,match,dict,b1,b2,fieldArr,labelArr,i,loopStrTotal
		dim tmp,lineArr,j,full_content

		if dPage = "" then
			dPage = 1
		end if
		
		if isNul( POP_MVC.get("page") ) then
			str= replace( str, page_current , 1 )
		else
			str= replace( str, page_current , POP_MVC.get("page") )
		end if
		
		'自定义分页	
		b1= inStr( str, "[page:" ) > 0
		b2= inStr( str , "{" + sLinkType + ":pagenumber" ) > 0	

		if b1 or b2 then
			set obj = P_("bbspage")
				Call obj.init( dTotal,dPerpage,dPage,dID,sLinkType,sShowType,sKeys )
			'if b2 then
					
				set matches = getBlockLabelsDict( str , "pagenumber" , sLinkType )
			
				for each k in matches
					'lineArr = Array()
					set match = matches(k)
					full_content = match("full_content")
					if dTotal = 0 then
						str = replace( str , full_content , "" )
					else
						set labelArr = parseArr( match("attr") )
						if not labelArr.exists( "len" ) then
							labelArr("len") = 10
						end if

						Call obj.NumList( labelArr("len") )
					
						'redim lineArr( obj.rightPage - obj.leftPage )
						loopStrTotal = ""
						'if obj.totalPage > 1 then
							j = 0
							for i = obj.leftPage to obj.rightPage
								tmp = replace( match("content") , "[pagenumber:page]" , i )
								tmp = replace( tmp , "[pagenumber:link]" , obj.PageLink(i) )
								loopStrTotal = loopStrTotal + tmp
								'POP_MVC.Arr.push lineArr , tmp
								'lineArr(j) = tmp
								j = j + 1
							next
							'loopstrTotal = join( lineArr , "" )
						'end if

						'set lineArr = nothing
						str = replace( str , full_content , loopStrTotal )
					end if
					
						if err.number <> 0 then
							POP_MVC.error( match & "--" + err.description )
						end if	
				next
			'end if

			if isNul( obj.page ) then
				obj.page = 1
			end if
			

			
			str= replace( str, page_firstlink , obj.FirstLink )
			str= replace( str, page_prevlink , obj.PrevLink )
			str= replace( str, page_nextlink , obj.NextLink )
			str= replace( str, page_lastlink , obj.LastLink )
			str= replace( str, page_prev , obj.page - 1 )
			str= replace( str, page_next , obj.page + 1 )
			str= replace( str, page_total , obj.total )
			str= replace( str, page_pagesize , obj.perpage )
			
			str= replace( str, page_count , obj.totalPage )
			str= replace( str, page_bar , obj.bar(10) )
			pageLabel = POP_MVC.String.reg_fetch( str, page_numlist , "" )

			PageCount = POP_MVC.String.reg_fetch( pageLabel , "\d+" , "" )

			str= replace( str, pageLabel , obj.NumList( PageCount ) )			
		end if	
		
		set obj = nothing
		replacePageLabel = str
	End Function
	
	Function replacePageList(byVal str,ByVal pageListType, dTotal,dPerpage,dPage,dID,sLinkType,sShowType,sKeys )
		dim matchesPagelist,matchPagelist,labelRulePagelist,lenPagelist,pageSeperator,strPagelist
		labelRulePagelist = "\["&pageListType&":pagenumber([\s\S]*?)\]"
		set matchesPagelist = POP_MVC.String.reg_exec(str,labelRulePagelist,"im")
		if matchesPagelist.count > 0 then
			Call P_("bbspage").init( dTotal,dPerpage,dPage,dID,sLinkType,sShowType,sKeys )
			for each matchPagelist in matchesPagelist	
				if P_("bbspage").totalPage=0 then
					str = replace(str,matchPagelist.value,"")
				else
					lenPagelist = parseArr(matchPagelist.SubMatches(0))("len")
					pageSeperator = parseArr(matchPagelist.SubMatches(0))("sep")
					if not isNul( pageSeperator ) then
						P_("bbspage").seperator = pageSeperator
					end if
					
					if isNul(lenPagelist) then lenPagelist = 10 else lenPagelist = cint(lenPagelist)

					strPagelist = P_("bbspage").bar( lenPagelist )
					str = replace(str,matchPagelist.value,strPagelist)
				end if
			next
		end if
		replacePageList = str		
	End Function	
	
	'用来解析文件
	Public Function parseFiles( ByVal currentPage,ByVal pageListType,typeIds )
		on error resume next
	    dim labelRuleField,labelArr,matchesfield,tempStr,str,k,lineArr,listIndex
		Dim loopstrTotal,i,nloopstr,matchfield,fieldNameArr,fieldName,fieldArr,typeStr,key,item,parseBool		
		Dim path,listType,objFolder,file,objFiles,arr,start_,end_,perpage,pageBool,sitePathLen,extStr,extName
		Dim startTime : startTime = timer()
		labelRuleField = typeIds & pageListType
		
		'lineArr = Array()
		set labelArr = parseArr(labelStr)
		
		'遍历文件方式方法
		listType = "file"
		if labelArr.exists("type") then
			listType = labelArr("type")
		end if
		
		if labelArr.exists("ext") then
			extStr = POP_MVC.trim( labelArr("ext") , "|" )
			extStr = POP_MVC.String.reg_replace( extStr , "|" , "\W" , "g" )
			extStr = "|" + extStr + "|"
		end if
		
		path = labelArr("path")
		
		if left(path,1) = "$" then
			path = V_( mid(path ,2 ) )
		else
			path = sitePath + "/" + POP_MVC.ltrim( path , "/" )
		end if
		
		sitePathLen = Len(POP_MVC.realPath(sitePath + "/")) + 2
		
		if currentPage = "" then
			currentPage = 1
		end if
		
		perpage = getPageSize( labelArr ,10 )
		
		pageBool = false

		if listType = "file" then
			set objFolder=POP_MVC.fso.GetFolder( POP_MVC.realPath(path) )		
			set objFiles=objFolder.Files			
		elseif listType = "folder" then
			set objFolder=POP_MVC.fso.GetFolder( POP_MVC.realPath(path) )		
			set objFiles=objFolder.SubFolders	
		end if
		
		set matchesfield = getInnerLabelsDict( loopStr , labelRuleField )
		i = 0
		listIndex = 1
		set fieldArr = D_
		For Each file In objFiles
			
			if extStr <> "" then
				extName = POP_MVC.file.extName( array(file.name,1) )
			end if

			if extStr = "" or ( extStr <> "" and inStr( extStr , "|" + extName + "|" ) > 0 ) then
			
				if labelArr.exists("num") then
					if CStr(labelArr("num")) =  CStr(i)  then
						exit for
					end if
				end if

				nloopstr=loopStr
				
				for each k in matchesfield
					set matchfield = matchesfield(k)
					fieldName = matchfield("label")
					if matchfield("attr") <> "" then
						set fieldArr = parseAttr( matchfield("attr") )
					else
						fieldArr.removeAll
					end if					
					tempStr = ""
					parseBool = true
					if fieldName = "i" then
						tempStr = listIndex
					elseif fieldName = "filepath" then
						tempStr = replace(mid( file.path , sitePathLen ) , "\" , "/")
					elseif fieldName = "basename" then
						tempStr = POP_MVC.file.baseName( array(file.name,1) )
					elseif fieldName = "extname" then
						tempStr = POP_MVC.file.extName( array(file.name,1) )
					elseif fieldName = "date" then
						tempStr = file.DateLastModified
					else
						Execute( "tempStr = file." + fieldName )							
					end if
					
					if fieldArr.count > 0 then
						tempStr = FormatTempStr( tempStr , fieldArr , matchfield("full_content") ,file )
					end if
					nloopstr = replace(nloopstr,matchfield("full_content"), tempStr )
				next
				
				if i >= (currentPage - 1) * perpage  and i < currentPage * perpage  then
					if labelArr("order") = "0" then
						loopstrTotal = nloopstr + loopstrTotal
						'POP_MVC.Arr.unshift lineArr , nloopstr
					else
						loopstrTotal = loopstrTotal + nloopstr
						'POP_MVC.Arr.push lineArr , nloopstr
					end if					
				end if
			
				listIndex = listIndex + 1
				i = i +1					
			end if
			files_counter = files_counter + 1
		Next
		
			'loopstrTotal = join( lineArr , "" )
			'set lineArr = nothing
			set matchesfield = nothing
			'replaceContent = replace(replaceContent,matchValue,loopstrTotal)	
replaceContent = replace(replaceContent,matchValue,"<!-- " + "替换块标签 (" + cmsPrefix + ":" & str & pageListType + " " + labelStr + ") " &  FormatNumber( (timer() - startTime ) *1000,0,-1,0,0) & "ms -->" & loopstrTotal & "<!-- /替换块标签" + str & pageListType + " -->")
Call POP_MVC.cmsPushTime( startTime , "替换块标签 (" + cmsPrefix + ":" + pageListType + " " + labelStr + ") "  )
			
			content = replacePageLabel( content , i, perpage  , currentPage , POP_MVC.get("SortID"), "file" , POP_MVC.get("template") ,"file" )			
			content = replacePageList( content , "file" , i, perpage , currentPage , POP_MVC.get("SortID"), "file" , POP_MVC.get("template") ,"file" )			
			
		if instr(loopstrTotal,"{" + cmsPrefix + ":sub" + pageListType  )>0 then
			str = "sub"
		else
			if str="" then str=0
			str = str + 1		
		end if
		'if str > 5 then exit function
		
		if instr(loopstrTotal,"{" + cmsPrefix + ":" & str & pageListType)>0 then 		
			set matches = getBlockLabelsDict( loopstrTotal , str & pageListType , ""  )
			for each k in matches					
				set match = matches(k)
				key = match("label")
				matchValue = match("full_content")					
				labelStr = match("attr")								
				loopStr = match("content")		
				Call parseFiles( currentPage,pageListType , str )
			next
			set matches = nothing
		end if	

		files_counter = 0
		Call L_( ClassType + ".parseFiles " + pageListType )
	End Function
	
	'用来解析xml
	Public Function parseXml( ByVal currentPage,ByVal pageListType,typeIds )
		'on error resume next
	    dim labelRuleField,labelArr,matchesfield,tempStr,str,listIndex,k,lineArr
	
		Dim loopstrTotal,i,nloopstr,matchfield,fieldName,attr,fieldArr,key,item
		
		Dim listType,perpage
		Dim data,nodes,node,xmlobj,path,match,matches
			
		dim startTime : startTime = timer()
		
		if xml_count > 99 then
			var_Export "parseXml已经运行了100次，似乎程序或模板有错误，请检查！"
			response.end
		end if
		
		
		
		'lineArr = Array()
		labelRuleField = typeIds & pageListType
		
		set labelArr = parseArr(labelStr)
		
		'采用何种方式
		
		if labelArr.exists("type") then
			listType = labelArr("type")
		else
			listType = POP_MVC.config("XML_TYPE")
		end if
		
		if listType = "" then
			listType = "file"
		end if
		
		set  xmlobj = P_("xml")	
		


		if not labelArr.exists("data") then
			data = POP_MVC.config("XML_PATH")
		else		
			data = labelArr("data")
			
			if left(data,1) = "$" then
				if isObject( V_( mid(data ,2 ) ) ) then
					set data = V_( mid(data ,2 ) )
				else
					data = V_( mid(data ,2 ) )
				end if			
			end if
		end if
		
		if listType = "file" or listType = "xmlfile" then
			data = sitePath + "/" + POP_MVC.ltrim( data , "/" )
			Call xmlobj.load( data ,"xmlfile")
		elseif listType = "dom" or listType = "xmldocument" then
			Call xmlobj.load( data ,"xmldocument")
		elseif listType = "url" then
			Call xmlobj.load( data ,"transfer")
		end if
		
		



		'路径
		path = labelArr("path")
		
		if left(path,1) = "$" then
			path = V_( mid(path ,2 ) )
		end if
		


		set  nodes = xmlobj.getNodes(path)
		
		if currentPage = "" then
			currentPage = 1
		end if
		
		perpage = getPageSize( labelArr , 10 )
		
		set matchesfield = getInnerLabelsDict( loopStr , labelRuleField )

		i = 0
		listIndex = 1
		set fieldArr = D_
	
		for i = 0 to nodes.Length - 1
			if labelArr("page") <> 0 and perpage =  i + 1 and typeIds="" then
				exit for
			end if

			nloopstr=loopStr
			set item = nodes.item(i)

			for each k in matchesfield
				set matchfield = matchesfield(k)
				fieldName = matchfield("label")
				if matchfield("attr") <> "" then
					set fieldArr = parseAttr( matchfield("attr") )
				else
					fieldArr.removeAll
				end if

				tempStr = ""
				
				'以@开头取属性
				if left(fieldName,1) = "@" then
					attr = mid( fieldName , 2 )
					tempstr = CStr(item.getAttribute(attr))
				else
					if fieldName = "i" then	
						tempStr = listIndex - (currentPage - 1) * perpage - 1
					elseif fieldName = "$" then
						tempStr = item.text
					else 
						set tempstr =item.selectSingleNode(fieldName)
						if not tempstr is nothing then
							tempstr = tempstr.text
						else
							tempstr = ""
						end if
					end if
				end if
				if fieldArr.count > 0 then
					tempStr = FormatTempStr( tempStr , fieldArr , matchfield("full_content") ,item )
				end if

				nloopstr = replace(nloopstr,matchfield("full_content"), tempStr )
			next
				
			if typeIds = "" then
				if i >= (currentPage - 1) * perpage  and i < currentPage * perpage then
					if labelArr("order") = "0" then
						loopstrTotal = nloopstr + loopstrTotal
						'POP_MVC.Arr.unshift lineArr , nloopstr
					else
						loopstrTotal = loopstrTotal + nloopstr
						'POP_MVC.Arr.push lineArr , nloopstr
					end if					
				end if			
			else
					if labelArr("order") = "0" then
						loopstrTotal = nloopstr + loopstrTotal
						'POP_MVC.Arr.unshift lineArr , nloopstr
					else
						loopstrTotal = loopstrTotal + nloopstr
						'POP_MVC.Arr.push lineArr , nloopstr
					end if	
			end if

		
			listIndex = listIndex + 1

		Next	

			set matchesfield = nothing
	
			'loopstrTotal = join( lineArr , "" )
			'set lineArr = nothing
			'replaceContent = replace(replaceContent,matchValue,loopstrTotal)
			
replaceContent = replace(replaceContent,matchValue,"<!-- " + "替换块标签 (" + cmsPrefix + ":" & str & pageListType + " " + labelStr & ") " &  FormatNumber( (timer() - startTime ) *1000,0,-1,0,0) & "ms -->" & loopstrTotal & "<!-- /替换块标签" & str & pageListType + " -->")
Call POP_MVC.cmsPushTime( startTime , "替换块标签 (" + cmsPrefix & ":" + pageListType + " " + labelStr + ") "  )

			content = replacePageLabel( content , i, labelArr("size")  , currentPage , POP_MVC.get("SortID"), "xml" , POP_MVC.get("template") ,"xml" )			
			content = replacePageList( content , "xml" , i, labelArr("size")  , currentPage , POP_MVC.get("SortID"), "xml" , POP_MVC.get("template") ,"xml" )			
			
		if instr(loopstrTotal,"{" & cmsPrefix & ":sub" & pageListType  )>0 then
			str = "sub"
		else
			if str="" then str=0
			str = str + 1		
		end if
		'if str > 5 then exit function
		
		if instr(loopstrTotal,"{" + cmsPrefix + ":" & str & pageListType) >0 then 		
			set matches = getBlockLabelsDict( loopstrTotal , str &pageListType , ""  )
			for each k in matches					
				set match = matches(k)
				key = match("label")
				matchValue = match("full_content")					
				labelStr = match("attr")								
				loopStr = match("content")		
				Call parseXml( currentPage,pageListType , str )
			next
			set matches = nothing
		end if
		tplExecNum = tplExecNum + 1
		Call POP_MVC.cmsPushTime( startTime , "替换标签" + mid( matchValue , 1 , instr(matchValue,"}") )  )
		xml_count = xml_count + 1
		Call L_( ClassType & ".parseXml " + pageListType )
	End Function
	
	
	'用来替换二维Dictonary对象、二维数组
	'主要针对无限分类数据表与banner展示
	Public Function parseData(ByVal pageListType,typeIds)
		on error resume next
	    dim labelRuleField,labelArr,matchesfield,where,tempStr,str,listIndex,k,pos
	
		Dim loopstrTotal,i,nloopstr,matchfield,fieldNameArr,m,fieldName,fieldArr,namelen,infolen,timestyle,matchesPagelist,matchPagelist,contentlen,pagecontent,typeStr,desclen,m_des,key,item,navListSort,dataIsArray,parseBool,fieldsArr
		Dim attachObj,dataObj,descObj,imageObj,lineArr
		
		dim startTime : startTime = timer()
		
		'lineArr = Array()
		labelRuleField = pageListType
		
		'根据不同标签类型，设置不同正则匹配模式
		'if pageListType = "navlist" then
		'	str = typeIds
		'	labelRuleField = str + pageListType
		'end if		

			set where = POP_MVC.SCD
			set labelArr = parseArr(labelStr)	
			listIndex = 1

		if 	pageListType="looparr" then
			if not labelArr.exists(loop4dataName) then
				dataObj = Array()
			else
				dataObj = getArrObj( labelArr , loop4dataName )				
			end if
			
			attachObj = getArrObj( labelArr , loop4keyName )
			descObj = getArrObj( labelArr , "desc" )
			imageObj = getArrObj( labelArr , "image" )
			
			if labelArr.Exists( "start" ) and isNumeric( labelArr("start") ) then
				dataObj = POP_MVC.Arr.Slice( dataObj , Cint(labelArr("start")) - 1 , -1 )
				
				if labelArr.exists(loop4keyName) then
					attachObj = POP_MVC.Arr.Slice( attachObj , Cint(labelArr("start")) - 1 , -1 )
				end if
				
				if labelArr.exists("desc") then
					descObj = POP_MVC.Arr.Slice( descObj , Cint(labelArr("start")) - 1 , -1 )
				end if
				
				if labelArr.exists("image") then
					imageObj = POP_MVC.Arr.Slice( imageObj , Cint(labelArr("start")) - 1 , -1 )
				end if
				
				labelArr.remove("start")
			end if
			
			if labelArr.Exists( "count" ) and not labelArr.Exists( "num" ) then
				labelArr("num") = labelArr("count")
				labelArr.remove("count")
			end if
		elseif 	pageListType="loopdata" then
			if labelArr.exists(loop4dataName) then	
				set dataObj = POP_MVC.tpl_vars( labelArr(loop4dataName) )
			else
				set dataObj = POP_MVC.SCD
			end if			
		end if
		
		if pageListType = "loopdata" and labelArr.exists("page") then
			content = replacePageLabel( content , CInt(labelArr("total")) , labelArr("pagesize"), POP_MVC.req("page") , typeIds, pageListType , "","" )
			
		end if			
		
		
			'set matchesfield = POP_MVC.String.reg_exec(loopStr,labelRuleField,"gim")
 			set matchesfield = getInnerLabelsDict( loopStr , labelRuleField )
			
			if POP_MVC.count( dataObj ) > 0 then
				loopstrTotal = ""
				i = 0
	dataIsArray = false
	if isArray( dataObj ) then
		dataIsArray = true
	end if
	set fieldArr = POP_MVC.SCD

	tempStr = dataObj.keys
	if dataObj.count > 0 then
		fieldsArr = dataObj( tempStr(0) ).keys
	end if
	
		'设置起始i值，默认从1开始
	if labelArr.Exists( "start" ) then
		if is_numeric( labelArr("start") ) then
			for i = 1 to labelArr("start") - 1 
				if dataObj.count > 0 then
					Call POP_MVC.dict.shift( dataObj )
				end if				
			next
		end if
	end if
	i = 0
	
		'if pageListType <> "looparr" then
		'	redim lineArr( dataObj.count - 1 )
		'elseif pageListType = "looparr" then
		'	redim lineArr( ubound( dataObj ) )
		'end if
	

	'循环每条记录
	for each key in dataObj
		if pageListType <> "looparr" then

			if isObject( dataObj(key) ) then
				set item = dataObj(key)
			else
				item = dataObj(key)
			end if
		elseif pageListType = "looparr" then
			item = key
		end if

		if labelArr.exists("num") then
			if labelArr("num") - 0 =  i  then
				exit for
			end if
		end if
		
		'var_Export item
					
		nloopstr=loopStr
		
		for each k in matchesfield
			set matchfield = matchesfield(k)		

			parseBool = true
			fieldName = matchfield("label")
			if matchfield("attr") <> "" then
				set fieldArr = parseAttr( matchfield("attr") )
			else
				fieldArr.removeAll
			end if	
			
			tempStr = ""
			
		if pageListType="loopdata" then
				if fieldName = "i" then
					tempStr = listIndex
				elseif fieldName = "_key_" then
					tempStr = key
				elseif fieldName = "_item_" then
					tempStr = item
				else
					parseBool = true
				End IF
		elseif pageListType="looparr" then
				if fieldName = "i" then
					tempStr = listIndex
				elseif fieldName = "item" or fieldName = loop4dataName then
					tempStr = key
				elseif fieldName = loop4keyName then
					tempStr = attachObj(i)
				elseif fieldName = "desc" then
					tempStr = descObj(i)
				elseif fieldName = "image" then
					tempStr = imageObj(i)
				else
					parseBool = true
				End if
		end if
		
	
		
						if not parseBool or pageListType = "loopdata"  then
							pos  = POP_MVC.Arr.iSearch( fieldsArr, fieldName )	
							if pos > -1 then
								parseBool = true
								tempStr = repNull(item(fieldsArr(pos)))
							end if
						end if

						
						if parseBool then							
							if fieldArr.count > 0 then
								tempStr = FormatTempStr( tempStr , fieldArr , matchfield("full_content") ,item )
							end if
							nloopstr = replace(nloopstr,matchfield("full_content"), tempStr )
							tplExecNum = tplExecNum + 1
						elseif APP_DEBUG = 1  then
							Call POP_MVC.cmsPushTime( 0 , "不能解析标签" & matchfield("full_content") & "，位置：" + "{" + cmsPrefix + ":" + pageListType + " " + labelStr + "}"  )						
						end if		
		
		
					next					
					loopstrTotal = loopstrTotal + nloopstr
					'POP_MVC.Arr.push lineArr , nloopstr
					'lineArr(i) = nloopstr
					listIndex = listIndex + 1
					i = i +1
				Next				
			end if
			set matchesfield = nothing
			
			'loopstrTotal = join( lineArr , "" )
			'set lineArr = nothing
			'replaceContent = replace(replaceContent,matchValue,loopstrTotal)
replaceContent = replace(replaceContent,matchValue,"<!-- " + "替换块标签 (" + cmsPrefix + ":" + pageListType + " " + labelStr + ") " &  FormatNumber( (timer() - startTime ) *1000,0,-1,0,0) & "ms -->" & loopstrTotal & "<!-- /替换块标签" + pageListType + " -->")
Call POP_MVC.cmsPushTime( startTime , "替换块标签 (" + cmsPrefix + ":" + pageListType + " " + labelStr + ") "  )

			set dataObj = nothing
			tplExecNum = tplExecNum + 1
Call POP_MVC.cmsPushTime( startTime , "替换块标签 (" + cmsPrefix + ":" + pageListType + " " + labelStr + ") "  )


		Call L_( ClassType + ".parseData " + pageListType )
	End Function
	
	'格式化替换字符串
	'tempStr 字段值
	'fieldArr 属性字典
	'tagContent 标签字符串
	'obj 当前的一条记录
	Public Function FormatTempStr( ByVal tempStr, ByRef fieldArr , ByRef tagContent , ByVal obj )
		on error resume next

		dim str,attrValue,arr,sep,result,stype,flag,key,bool,temp,startTime
		startTime = timer
		sep = "#"
		stype = typename(obj)
		if isNull( tempStr ) then
			tempStr = repNull(tempStr)
		end if

		for each key in fieldArr
			attrValue = fieldArr(key)
			bool = false
			flag = false
			if inStr( attrValue , "$" ) > 0 then
				if mid(attrValue,1,1) = "$" and mid(attrValue,1,2) <> "$$" then
					attrValue = trim(mid( attrValue , 2 ))
					attrValue = V_(attrValue)
					bool = true
				end if
			elseif inStr( attrValue , "@" ) > 0 then
				if mid(attrValue,1,1) = "@" and attrValue <> "@me" then
					attrValue = trim(mid( attrValue , 2 ))
					if stype = "File" or stype = "Folder" then
						Execute( "attrValue = obj." & tempStr )
					else
						attrValue = obj(attrValue)
					end if	
					bool = true
				end if
			end if
			
			if not bool then
				attrValue = trim(attrValue)
				attrValue = getMvcTplVars(attrValue)
			end if
			
			'对于eq,neq,gt,et
			'使用 eq:结果=值
			if inStr( key , sep ) > 0 then
				arr = split( key, sep )
				key = arr(0)
				result = arr(1)
			end if

			if key = "len" then
					'截取字符串，[content:title len=200]
					tempStr = POP_MVC.String.cut( stripTags(tempStr),attrValue )
			elseif key = "left" then
					tempStr = left(tempStr,attrValue)
			elseif key = "right" then
					tempStr = right(tempStr,attrValue)
			elseif key = "style" then
					if isDate(tempStr) then
						tempStr = POP_MVC.FormatDate( tempStr ,attrValue )
					end if
			elseif key = "math" then
					if isNum( tempStr ) then
						str = replace( attrValue , "$$" , "tempstr" )
						Execute( "tempStr = " & str )
					end if
			elseif key = "selected" or key ="checked" then
					if CStr(tempStr) = CStr(attrValue) then
						tempStr = key & "=""" & key & """"
					else
						tempStr = ""
					end if
				' eq#className=$_GET.a
				' 判断自身值是否同 POP_MVC.get("a") 是否相等，相等的话返回className
			elseif key = "eq" then
					tempStr = iif( CStr(tempStr) = CStr(attrValue) , result, "" )
			elseif key = "ieq" then
					tempStr = iif( LCase(CStr(tempStr)) = LCase(CStr(attrValue)) , result, "" )
			elseif key = "neq" then
					tempStr = iif( CStr(tempStr) <> CStr(attrValue) , result, "" )
			elseif key = "ineq" then
					tempStr = iif( LCase(CStr(tempStr)) <> LCase(CStr(attrValue)) , result, "" )
			elseif key = "gt" then
					tempStr = iif( tempStr - attrValue > 0 , result, "" )
			elseif key = "get" then
					tempStr = iif( tempStr - attrValue >= 0 , result, "" )
			elseif key = "lt" then
					tempStr = iif( tempStr - attrValue < 0 , result, "" )
			elseif key = "let" then
					tempStr = iif( tempStr - attrValue <= 0 , result, "" )
			elseif key = "highlight" then
					Execute( "str = " & attrValue )
					tempStr = replace(tempStr, str , "<span style='color:#" & result & "'>" & str & "</span>" )
			elseif key = "highlightc" then
					Execute( "str = " & attrValue )
					tempStr = replace( ModifyContent(tempStr), str , "<span style='color:#" & result & "'>" & str & "</span>" )
			elseif key = "newthread" then
					if guestConfig.NewSortTime > 0 and isDate(tempstr) then
						if Datediff("h",Cdate(tempstr),now()) - guestConfig.NewSortTime <= 0 then
							tempStr = attrValue
							flag = true
						end if
					end if
					if not flag then
						tempStr = ""
					end if
			elseif key = "newsort" then
					if inNewSort( tempStr ) then
						tempStr =  "<span class='layui-badge-dot " & attrValue & "' title='" & getNewSortCnt(tempStr) & "'></span>"
					else
						tempStr = ""
					end if
			elseif key = "daydiff" then
					if isDate(tempStr) then
						if Datediff("d",Cdate(tempstr),now()) - result <= 0 then
							tempStr = attrValue
							flag = true
						end if
					end if
					if not flag then
						tempStr = ""
					end if
			elseif key = "func" then
					str = replace( attrValue , """@me""" , "tempStr" )
					str = replace( attrValue , "@me" , "tempStr" )		
					temp = tempStr
					Execute( "tempStr = " & str )
			elseif key = "encode" then
					if attrValue = "html" then
						tempStr = POP_MVC.String.encodeHtml( tempStr )
					elseif attrValue = "vbs" then
						tempStr = POP_MVC.String.vbsEncode( tempStr )
					elseif attrValue = "json" then
						tempStr = POP_MVC.String.jsonEncode( tempStr )
					elseif attrValue = "url" then
						tempStr = POP_MVC.String.URLEncode( tempStr )
					elseif attrValue = "escape" then
						tempStr = escape( tempStr )
					end if
			elseif key = "default" then
					if isNul(tempStr) then
						tempStr = fieldArr("default")
						if mid(tempStr,1,1) = "@" then			
							if stype = "File" or stype = "Folder" then
								Execute( "tempStr = obj." & mid(tempStr,2) )
							else
								tempStr = obj( mid(tempStr,2) )
							end if
						else
							tempStr = replace( tempStr, "_EQ_" , "=" )
						end if
					end if				
			end if	
			if err.number <> 0 then
				POP_MVC.error( tagContent & "--" & err.description )
			end if
		next
		innerTime = innerTime + timer - startTime
		FormatTempStr = tempStr
	End Function
	
	Function escape( ByVal str )		
		str = repnull( str )
		str = replace( str, "[hide]" , escape_label & "_left" )
		str = replace( str, "[/hide]" , escape_label & "_right" )
		str = replace( str , "{" , "&#123;" )
		str = replace( str , "}" , "&#125;" )
		str = replace( str , "[" , "&#91;" )
		str = replace( str , "]" , "&#93;" )
		str = replace( str , escape_label & "_left" , "[hide]")
		str = replace( str , escape_label & "_right" , "[/hide]")
		escape = str
	End Function
	
	
	'解析if
	Public Property Get parseIf(x)
		on error resume next
		if instr(content,"{if"&x&":") < 0 then Exit Property	
		dim nextone
		if isnum(x) then
			nextone=Int(x)+1
		else
			nextone=1
		end if

		if inStr(content,"{if"&nextone&":") > 0 then parseIf(nextone)
		dim matchIf,matchesIf,strIf,strThen,strThen1,strElse1,labelRule2,labelRule3
		dim ifFlag,elseIfArray,elseIfSubArray,elseIfArrayLen,resultStr,elseIfLen,strElseIf,strElseIfThen,elseIfFlag
		labelRule="{if"&x&":([\s\S]+?)}([\s\S]*?){end\s+if"&x&"}":labelRule2="{elseif"&x&"":labelRule3="{else}":elseIfFlag=false
		set matchesIf=POP_MVC.String.reg_exec(content,labelRule,"gim")
		for each matchIf in matchesIf 			
			strIf=matchIf.SubMatches(0)
			strIf = getMvcTplVars(strIf)
			
			strThen=matchIf.SubMatches(1)	
			if instr(strThen,labelRule2)>0 then
				elseIfArray=split(strThen,labelRule2):elseIfArrayLen=ubound(elseIfArray):elseIfSubArray=split(elseIfArray(elseIfArrayLen),labelRule3)
				resultStr=elseIfSubArray(1)
				Execute("if "&strIf&" then resultStr=elseIfArray(0)")
				for elseIfLen=1 to elseIfArrayLen-1
					strElseIf=getSubStrByFromAndEnd(elseIfArray(elseIfLen),":","}","")
					strElseIfThen=getSubStrByFromAndEnd(elseIfArray(elseIfLen),"}","","start")
					Execute("if "&strElseIf&" then resultStr=strElseIfThen")
					Execute("if "&strElseIf&" then elseIfFlag=true else  elseIfFlag=false")
					if elseIfFlag then exit for
				next
				
				Execute("if "& getMvcTplVars(getSubStrByFromAndEnd(elseIfSubArray(0),":","}",""))  &" then resultStr=getSubStrByFromAndEnd(elseIfSubArray(0),""}"","""",""start""):elseIfFlag=true")
				content=replace(content,matchIf.value,resultStr)
			else 
				if instr(strThen,"{else}")>0 then 
					strThen1=split(strThen,labelRule3)(0)
					strElse1=split(strThen,labelRule3)(1)
					Execute("if "&strIf&" then ifFlag=true else ifFlag=false")
					if ifFlag then content=replace(content,matchIf.value,strThen1) else content=replace(content,matchIf.value,strElse1)
				else	
					Execute("if "&strIf&" then ifFlag=true else ifFlag=false")
					if ifFlag then content=replace(content,matchIf.value,strThen) else content=replace(content,matchIf.value,"")
				end if
			end if
			elseIfFlag=false
			if err.number <> 0 then
				POP_MVC.error( matchIf.value )
				content=replace(content,matchIf.value,"")
				err.clear
			end if
		next
		set matchesIf=nothing
		Call L_( ClassType + ".parseIf" )
	End Property
	
	'$前缀变量用V_代替
	Function getMvcTplVars( str )
		if inStr( str , "$" ) > 0 then
			getMvcTplVars = POP_MVC.String.reg_replace( str ,  "V_(""$1"")"  ,"\$([\w]+(?:\.[\w]+)?)" , "gim" )
		else
			getMvcTplVars = str
		end if
	End Function
	
	Function getSubStrByFromAndEnd(str,startStr,endStr,operType)
		dim location1,location2
		if operType = "start" then
				location1=instr(str,startStr)+len(startStr):location2=len(str)+1
		elseif operType = "end" then
				location1=1:location2=instr(location1,str,endStr)
		else
				location1=instr(str,startStr)+len(startStr):location2=instr(location1,str,endStr)
		end if
		getSubStrByFromAndEnd=mid(str,location1,location2-location1) 
	End Function
	
	
	Function getQQmessage( qqNumber, imgType, className, tip )
		getQQmessage = "<a target='_blank' class='" + className + "' href='http://wpa.qq.com/msgrd?v=3&uin=" + qqNumber + "&site=qq&menu=yes'><img border='0' src='http://wpa.qq.com/pa?p=2:" & qqNumber & ":" & imgType & "' alt='" & tip & "' title='" & tip & "'></a>"
	End Function
	
	Function getWangWangmessage( wwNumber, imgType, className, tip )
		getWangWangmessage = "<a target='_blank' class='" + className + "' href='http://amos1.taobao.com/msg.ww?v=2&uid="& wwNumber & "&s=" & imgType & "'><img border='0' src='http://amos1.taobao.com/online.ww?v=2&uid=" & wwNumber & "&s=" & imgType & "' alt='" & tip & "' /></a>"
	End Function	

	
	'获取可用标签参数
	Public Function parseAttr(Byval attr)
		on error resume next
		dim singleAttr,dict
		set dict = POP_MVC.SCD
		attr = trim(attr)
		if inStr( attr,"=" ) > 0  then
			singleAttr = split( attr, "=" , 2 )
			dict( trim(singleAttr(0)) ) = trim( singleAttr(1) )
		end if
		set parseAttr = dict
		Call L_( ClassType + ".parseAttr" )
	End Function	
	
	'获取可用标签参数
	Public Function parseArr(Byval attr)
		dim attrStr,attrArray,i,singleAttr,singleAttrKey,singleAttrValue,strDictionary
		attrStr = POP_MVC.String.reg_replace(attr,chr(32),"[\s]+","g")
		attrStr = trim(attrStr)
		attrArray = split(attrStr,chr(32))
		
		set strDictionary = POP_MVC.SCD	
		for i=0 to ubound(attrArray)
			singleAttr = split(attrArray(i),chr(61),2)			
			singleAttrKey =  singleAttr(0) : singleAttrValue =  singleAttr(1)
			if not strDictionary.Exists(singleAttrKey) then 
				strDictionary.add singleAttrKey,singleAttrValue 
			else 
				strDictionary(singleAttrKey) = singleAttrValue
			end if
		next
		set parseArr = strDictionary
	End Function	

	
	'获取首页链接
	function getIndexLink
		getIndexLink = left(linkPrefix , len(linkPrefix) - 1 )
	end function

	
	'获取数据表中的title字段值
	Function getDate( ByRef dvalue,ByRef fieldArr )	
		on error resume next
		if Not fieldArr.exists( "style" ) Then 
			tempStr = "m-d"
		else
			tempStr = fieldArr("style")
		end if

		getDate = 	POP_MVC.FormatDate( dvalue ,tempStr)
		Call L_( ClassType + ".getDate" )
	End Function
	
	'获取数据表中的title字段值
	Function getTitle( ByRef title,ByRef labelArr )
		on error resume next
		getTitle = title
		if labelArr.exists("len") then   								
			getTitle = POP_MVC.String.cut( title,labelArr("len") )
		end if	
		Call L_( ClassType + ".getTitle" )
	End Function
	
	'获取模板文件夹路径，如 templates/bbs
	Public Function getTplFolderPath( )
		getTplFolderPath =  "templates/" + guestConfig.DefaultTemplate
		getTplFolderPath = replace( getTplFolderPath , "//" , "/" )
	End Function	
	
	'获取模板路径，如 templates/bbs/html/common/index.html
	Public Function getTemplatePath(  ByRef templateFile  )
		dim tplPath
		tplPath = POP_MVC.rtrim( templateFile , fileExt ) + fileExt
		getTemplatePath = getTplFolderPath() + "/" + htmlFilePath + "/" + tplPath
	End Function
	
	'获取头像个数，如 templates/bbs/res/images/avatar 中 头像图片个数
	Public Function getAvatarCount( stype )
		getAvatarCount = POP_MVC.file.fileCount( getTplFolderPath() + "/res/images/" + stype )
		if stype = "avatar" then
			getAvatarCount = getAvatarCount - 1
		end if
	End Function

	Public Function getTplPath(  ByVal tpl  )
		if tpl = "" Then
			tpl = POP_MVC.c + "/" + POP_MVC.a
		End If
		getTplPath = cms_home_path + "/Tpl/"	+ tpl + fileExt
	End Function
	
	'是否为数字
	Function isNum(str)
		if not isNul(str) then  isNum=isnumeric(str) else isNum=false
	End Function
	
	Sub initLinkPrefix
		if not isEmpty( linkPrefix ) then Exit Sub
		linkPrefix=sitePath& "/" + "?"
		indexLinkPrefix = sitePath + "/" + POP_MVC.config("INDEX_PAGE") + "?"
		linkPrefix = replace( linkPrefix , "//" , "/" )
		if indexLinkPrefix = "" then indexLinkPrefix = linkPrefix
		POP_MVC.config("INDEX_LINK_PREFIX") = indexLinkPrefix
		
		'带域名的链接前缀，如：http://127.0.0.1:1251/bbs/
		RealLinkPrefix = iif(is_localhost,POP_MVC.http_host() ,POP_MVC.http_host2() )
		RealLinkPrefix = POP_MVC.rtrim(RealLinkPrefix,"/") + linkPrefix
	End Sub

	'将html内容去标签，并按指定长度截取
	Public Function htmlCut( str, len )
		if isNul( str ) then
			htmlCut = ""
			exit function
		end if
		htmlCut = POP_MVC.String.cut( stripTags(str) , Cint(len) * 2 )	
	End Function
	
	Public Function htmlCut2( str, len )
		htmlCut2 = htmlCut( str , len )
		if htmlCut2 = "" then
			htmlCut2 = "..."
		end if
	End Function
	
	'去掉html标签
	Public Function stripTags( str )
		if isNul( str ) then
			stripTags = ""
			Exit Function
		end if
		stripTags = POP_MVC.String.strip_tags(POP_MVC.String.decodeHtml(str) , "html")
	End Function
	
	
	'获取内部标签，比如[list:title]，x为块标签的文本内容，labelName为标签名，比如list
	'返回二维Dictionary对象
	' dict("flag_start") 	标签左起位置
	' dict("flag_end")		标签结束位置
	' dict("full_content")  整个标签内容，比如[list:title len=30]
	' dict("attr")			标签中的属性，比如[list:title len=30]
	' dict("label")			标签标记，同labelName，如list
	Public Function getInnerLabelsDict( ByRef x , ByRef labelName )
		set getInnerLabelsDict = getInnerLabelsDict_( x , labelName, "[" , "]" )
	End Function
	
	
	'获取内部标签，比如[list:title]，x为块标签的文本内容，labelName为标签名，比如list
	'返回二维Dictionary对象
	' dict("flag_start") 	标签左起位置
	' dict("flag_end")		标签结束位置
	' dict("full_content")  整个标签内容，比如[list:title len=30]
	' dict("attr")			标签中的属性，比如[list:title len=30]
	' dict("label")			标签标记，同labelName，如list
	Public Function getInnerLabelsDict_ ( ByRef x , ByRef labelName , leftLabel, rightLabel )
		dim flag_start,flag_end,space_start,flagStart,startLen
		dim curPos,cusLabel,curFullContent,curAttr
		dim dict,ret,loopMax,loopCnt,prevStart,tempPos
		
		set ret = POP_MVC.SCD
		
		'结束标记
		flagStart = leftLabel + labelName + ":"
		
		'起始标记长度
		startLen = Len(flagStart)	

		curPos = 1
		
		loopMax = 1000
		loopCnt = 0		

		do		
			if loopCnt >= loopMax then
				exit do
			end if
			'左始
			flag_start = inStr( curPos, x, flagStart )
			
			if flag_start < 1 then
				exit do
			end if
			
			'左尾
			flag_end = inStr(flag_start,x,rightLabel)
			
			if flag_end < 1 then
				Me.ShowError4html( "在位置" & flag_start & "处的标签" + mid( x, flag_start , startLen + 100 ) + "……" + "这里未找到闭合标识符的方括号]"  )
				exit do
			end if
			
			'全内容
			curFullContent = mid( x, flag_start, flag_end - flag_start +1  )			
			space_start = instr( startLen  ,curFullContent , " " )
			
			curAttr = ""
			if space_start > 0 then
				curAttr = mid( curFullContent , space_start+ 1 , flag_end - flag_start - space_start )				
				cusLabel = mid( curFullContent , startLen + 1 ,  space_start - startLen -1 )
			else
				cusLabel = mid( curFullContent , startLen + 1 ,  flag_end - flag_start - startLen  )
			end if
			
			set dict = POP_MVC.SCD
			dict("flag_start") = flag_start
			dict("flag_end") = flag_end
			dict("full_content") = curFullContent
			dict("attr") = curAttr
			dict("label") = cusLabel			

			ret.add flag_start , dict

			loopCnt = loopCnt + 1
			curPos = flag_end+1
		loop while curPos > 1

		Call POP_MVC.dict.ksort( ret )
		set getInnerLabelsDict_ = ret
	End Function
	
	
	'获取块标签，比如{iaspcms:list sort=1} ... {/iaspcms:list}
	'x为文档内容，labelName为块标签名，比如list，如果为空，取全部块标签
	'返回二维Dictionary对象
	' dict("left_start")	{iaspcms:list sort=1}开始位置，即{位置
	' dict("left_end")		{iaspcms:list sort=1}结束位置，即}位置
	' dict("right_start")	{/iaspcms:list}开始位置，即{位置
	' dict("right_end")		{/iaspcms:list}结束位置，即}位置
	' dict("full_content")	{iaspcms:list sort=1} ... {/iaspcms:list}块标签全部内容
	' dict("content")		块标签里面的内容
	' dict("attr")			块标签属性字符串，{iaspcms:list sort=1}中的 sort=1
	' dict("label")			块标签标记名，如list
	' dict("is_parse")		是否解析，为０
	' dict("left_flag")		标签的开始标记，如{iaspcms:list sort=1}
	Public Function getBlockLabelsDict( ByRef x , ByVal labelName , ByVal prefix )
		dim left_start,left_end,right_start,right_end,end_len,start_len,left_flag,labelEnd,labelStart
		dim curPos,cusLabel,curFullContent,curAttr,curContent
		dim endLen,startLen
		dim dict,ret,loopMax,loopCnt,prevStart,tempPos,startTime
		
		if isArray(labelName) then
			labelName = join(labelName , ",") + ","
		elseif labelName <> "" then
			labelName = "," + labelName + ","
		end if
		
		startTime = timer
		set ret = POP_MVC.SCD
		
		if prefix = "" then
			prefix = cmsPrefix
		end if
		
		'结束标记
		labelEnd = "{/" + prefix + ":"
		
		'结束标记长度
		endLen = Len(labelEnd)	

		curPos = 1
		prevStart = 1
		
		loopMax = 500
		loopCnt = 0
		
		do		
			if loopCnt >= loopMax then
				exit do
			end if
			'右始
			right_start = inStr( curPos, x, labelEnd )
			
			if right_start < 1 then
				exit do
			end if
			
			'右尾
			right_end = inStr(right_start,x,"}")
			
			if right_end < 1 then
				Me.ShowError4html( "在位置" & right_start & "处的标签" + mid( x, right_start , endLen + 30 ) + "……" + "这里未找到闭合标识符尖括号}"  )
				exit do
			end if
			
			'当前标签名
			cusLabel = mid( x,right_start+endLen,right_end-right_start-endLen )
			
if labelName = "" or inStr(labelName , "," + cusLabel + "," ) > 0 then
			'起始标记
			labelStart = "{" + prefix + ":" + cusLabel
			
			'
			startLen = len( labelStart )
			
			'左始
			left_start = inStrRev( x, labelStart , right_start )
			
			if left_start < 1 then
				Me.ShowError4html( "在位置" & right_start & "处的标签" + labelEnd + cusLabel + "}" + "，未找到对应的起始标签"  )
				exit do
			end if
			

			
			'全内容
			curFullContent = mid( x, left_start, right_end - left_start +1  )

			'左尾
			left_end = inStr( startLen + 1,curFullContent, "}" ) + left_start - 1
			tempPos = inStr( startLen + 1,curFullContent, "{" ) + left_start - 1
			
			if left_end > tempPos then
				Me.ShowError4html( "在块标签中未找到起始标签的}，块标签为：" + curFullContent  )
				exit do
			end if
			
			'left_flag
			left_flag = mid( curFullContent , 1 , left_end - left_start + 1 )
			
			'属性
			curAttr = mid( curFullContent , startLen +1 , left_end - left_start - startLen  )
			
			'标签内的循环内容
			curContent = mid( curFullContent , left_end - left_start + 2 ,right_start - left_end -1)
			
			set dict = POP_MVC.SCD
			dict("left_start") = left_start
			dict("left_end") = left_end
			dict("right_start") = right_start
			dict("right_end") = right_end
			dict("full_content") = curFullContent
			dict("content") = curContent
			dict("attr") = curAttr
			dict("label") = cusLabel
			dict("is_parse") = 0
			dict("left_flag") = left_flag
			

			ret.add left_start , dict			
end if
			loopCnt = loopCnt + 1
			curPos = right_end+1
		loop while curPos > 1

		POP_MVC.dict.sortByNumeric = True

		Call POP_MVC.dict.ksort( ret )
		tplExecNum = tplExecNum + 1
		if APP_DEBUG = 1 then
			if isNul( labelName ) then
				Call POP_MVC.cmsPushTime( startTime , "获取 全部块标签("& ret.count &"个) 并转为Dictionary对象" )
			else
				Call POP_MVC.cmsPushTime( startTime , "获取 {/" + prefix & ":" + POP_MVC.trim(labelName,",") +  "} 块标签并转为Dictionary对象" )
			end if
		end if
		set getBlockLabelsDict = ret 
	End Function

	
	'获取定时发布的where条件
	Function getTimeStatusWhere
		if ClassType = "POPASP_PAPCMS" then
			Exit Function
		end if
		if POP_MVC.config( "DB_TYPE" ) = "access" then
			getTimeStatusWhere = "(TimeStatus = 0  OR ( TimeStatus AND NOT ISNULL( Timeing) AND datediff( ""s"",Timeing , now() ) > 0 ) )"
		elseif POP_MVC.config( "DB_TYPE" ) = "sqlite3" then
			getTimeStatusWhere = "(TimeStatus = 0  OR ( TimeStatus AND Timeing IS NOT NULL AND (JULIANDAY('now') - JULIANDAY( REPLACE( Timeing , '/' , '-' ) )) >= 0) )"
		elseif POP_MVC.config( "DB_TYPE" ) = "mysql" then
			getTimeStatusWhere = "(TimeStatus = 0  OR ( TimeStatus AND NOT ISNULL( Timeing) AND TIMESTAMPDIFF( SECOND,Timeing , now() ) > 0 ) )" 
		end if	
	End Function
	
	'得到 {iaspcms:label} 这样的标签字符串
	Public Property Get getTagLabel( ByRef label )
		getTagLabel = "{" + cmsPrefix + ":" + label + "}"
	End Property	
	
	sub parseExecExp( byref l_d,byref r_d)
		on error resume next
		dim str,matches,match,condition,str_true,str_false,pattern,repStr,pattern1,pos,str1,ruleVar
				
		'表达式运算
		': POP_MVC.String.range( 5,100 )
		'exec_exp
	
		ruleVar = "[\w\(\)\.\""\u0391-\uFFE5]+"
		pattern = l_d + "\s*([\:]+)([\s\S]+?)" + r_d
		pattern1 = l_d + "\s*[\:]\$(" + ruleVar + ")\|remove\s*" + r_d

		if POP_MVC.String.reg_test( content, pattern1 , "" ) then
			set matches = POP_MVC.String.reg_exec(content, pattern1  ,"gmi")
			for each match in matches
				pos = inStrRev(  match.subMatches(0), "." )
				if pos then
					str = mid(match.subMatches(0),pos+1)
					str1 = mid(match.subMatches(0),1,pos-1)
					str1= replace( str1, ".",""")("""  )
					str1= "(""" + str1 + """)"
					str = "POP_MVC.tpl_vars" + str1 + ".remove(""" & str & """)"
				else
					str = match.subMatches(0)
					str = "POP_MVC.tpl_vars.remove(""" & str & """)"
				end if
				Execute(  str )
				content = Replace(content,match.value, "" )
			next
		else
			set matches = POP_MVC.String.reg_exec(content, pattern  ,"gm")
			for each match in matches
				str = match.subMatches(1)	
				if match.subMatches(0) = ":" then
					str =  getMvcTplVars( str )
				elseif match.subMatches(0) = "::" then
					'不做任何修改
				end if
				str = replace( str, "V_" , "POP_MVC.tpl_vars" ,1,1,0)

				Execute(  str )

				if err.number <> 0 then
					POP_MVC.error( ClassType + ".parseExecExp:" + "解析 " & match.value & "为" + POP_MVC.String.encodeHtml( str ) + "时不能被Execute,错误：" + err.description )
				end if
				content = Replace(content,match.value, "" )
			next
		end if

		set matches = nothing
		Call L_( ClassType + ".parseExecExp" )
	end sub		
	
	'获取项目的文件组织结构图
	function cmsFilesTree
		dim arr,mvcPath,sitePath,adminPath
		mvcPath = POP_MVC.mvc_dir
		adminPath = POP_MVC.file.realDir( mvcPath )
		sitePath = POP_MVC.file.realDir( adminPath )
		arr = array( sitePath + "\Upload" ,sitePath + "\" + staticName , sitePath + "\Templates" , sitePath + "\" + dataName   )
		cmsFilesTree = POP_MVC.file.getFilesMap( sitePath , arr ,-1 )
	end function
	
	'首页链接
	function getHomePage
		dim homePage
		homePage = sitePath + "/" 
		getHomePage = homePage
	end function
	
	function getPageSize( labelArr , num )
		if labelArr.exists("size") then labelArr("num") = labelArr("size")
		if isNul(labelArr("num")) then labelArr("num") = num  else labelArr("num") = cint(labelArr("num"))
		getPageSize = labelArr("num")
	end function
	''''''''''''''''''''''''''''''''''''''''''''''
	
	function getArrItem( ByVal arrStr, num , splitSep )
		num = CInt(num)
		Execute( "arrStr = split( POP_MVC.String.vbsEncode(arrStr) , splitSep ) "  )
		if num >0 and num <= ubound(arrStr) + 1 then
			getArrItem = arrStr( num-1 )
		end if		
	End function

	'获取从标签中解析的Array
	function getArrObj( labelArr,keyName)
		dim splitSep
		splitSep = ","
		if labelArr.Exists("split") then
			splitSep = labelArr("split")
		end if
		if labelArr.exists(keyName) then
			if left(labelArr(keyName),1) = "$" then
				getArrObj = POP_MVC.String.vbsEncode(V_( mid( labelArr(keyName) ,2 ) ))
			elseif POP_MVC.String.reg_test( labelArr(keyName) , "^\w+\.\w+$" , "i" ) then
				Execute( "getArrObj = split( POP_MVC.String.vbsEncode(" + labelArr(keyName) + " ) , splitSep ) "  )
			else
				getArrObj = split( labelArr(keyName) , splitSep )
			end if
		end if
		if not isArray( getArrObj ) then
			getArrObj = split( getArrObj , splitSep )
		end if
	end function		
	
	
	'替换List循环标签
	Public Function parseList(typeIds,currentPage,ByVal pageListType,keys,showType)	
		'on error resume next
	    dim lenPagelist,TypeId,strPagelist,rsObj,labelRulePagelist,labelArr,matchesfield,matchfield,where,tempStr,tempStr2,arr,cnt
		Dim loopstrTotal,i,k,nloopstr,fieldName,fieldArr,matchesPagelist,matchPagelist,loopCounter,fieldsArr,parseBool,listIndex,start_,pageSeperator
		Dim tableName,lineArr,full_content
		dim startTime : startTime = timer()		
		labelRulePagelist = "\["&pageListType&":pagenumber([\s\S]*?)\]"
		
		set where = POP_MVC.SCD
		
		cnt = -1

		set labelArr = parseArr(labelStr)
		listIndex = 1
		'lineArr = Array()
		
		if labelArr.Exists("num") or labelArr.Exists("size") then		
			labelArr("num") = getPageSize( labelArr , 10 )
			cnt = CInt(labelArr("num"))
		end if
					
		if 	pageListType="looprs" then

				if labelArr.exists(loop4dataName) then	
					if POP_MVC.tpl_vars.Exists( labelArr(loop4dataName) ) then
						if typename( POP_MVC.tpl_vars( labelArr(loop4dataName) ) ) = "Recordset" then
							set rsObj = POP_MVC.tpl_vars( labelArr(loop4dataName) )
						end if						
					end if
				end if
				if typename(rsObj) <> "Recordset" then
					replaceContent = ""
					Exit Function
				end if
		elseif 	pageListType = CommonTable then
				if labelArr("table") <> "" then
					tableName = labelArr("table")
				else
					tableName = keys				
				end if	
		
				for each tempStr in labelArr
					tempStr2 = labelArr(tempStr)
					if POP_MVC.String.startsWith( tempStr2 , "$" ) then
						tempStr2 = mid(tempStr2,2)
						if POP_MVC.tpl_vars.exists( tempStr2 ) then
							if isObject( POP_MVC.tpl_vars(tempStr2) ) then
								set labelArr( tempStr ) = POP_MVC.tpl_vars( tempStr2 )
							else
								labelArr( tempStr ) = POP_MVC.tpl_vars( tempStr2 )
							end if
						else
							labelArr.remove( tempStr )
						end if
					end if
				next		
				
				for each tempStr in labelArr
					select case tempStr
					case "size"
						B_( tableName ).page( array( currentPage,Cint( labelArr("size") ) ) )
					case "table"
					case "pagelabel"
						showType = labelArr( tempStr )
					case "id"
						typeIds = labelArr( tempStr )
					case else
						execute( "B_(""" + tableName + """)." + tempStr + "( labelArr(tempStr) )" )
					end select
				next
				if not isNul( typeIds ) then
					set rsObj = B_( tableName ).where(typeIds).find
				else
					set rsObj = B_( tableName ).select
				end if
		end if	
		
		if pageListType = CommonTable then
			if isNul(keys) then
				set matchesfield = getInnerLabelsDict( loopStr , CommonTable )
			else
				set matchesfield = getInnerLabelsDict( loopStr , keys )
			end if
		else
			set matchesfield = getInnerLabelsDict( loopStr , pageListType )
		end if
		
		'自定义分页	
		
		if pageListType <> "looprs" or ( pageListType = "looprs" and labelArr.exists("page") ) then
			content = replacePageLabel( content , rsObj.recordCount, rsObj.pagesize, currentPage , TypeIds, pageListType , showType,keys )					
		end if
		
		if rsObj.eof then 
			''''
		else

		set fieldArr =	D_
		fieldsArr = P_("recordset").getFields(rsObj)
	
		if not isEmpty(start_) then
			for i = 1 to start_ - 1
				if not rsObj.eof then
					rsObj.moveNext
				end if			
			next
		end if
	
	
		loopstrTotal = ""
		i = 0
		i = Clng(i)	
	'redim lineArr( rsObj.pageSize - 1 )
	do while not rsObj.eof  and i < rsObj.pageSize	
		loopCounter = loopCounter + 1
		
		if loopCounter > rsObj.pageSize then
			exit do
		end if
		
		if cnt > 0 and loopCounter - labelArr("num") > 0 then
			exit do
		end if
		
		nloopstr=loopStr
		for each k in matchesfield
			parseBool = true
			
			set matchfield = matchesfield(k)
			full_content = matchfield("full_content")
			fieldName = matchfield("label")
			if matchfield("attr") <> "" then
				set fieldArr = parseAttr( matchfield("attr") )
			else
				fieldArr.removeAll
			end if
			
			tempStr = ""

		if pageListType=CommonTable or pageListType="looprs" then
			if fieldName = "i" then
				tempStr = listIndex
			else
				parseBool = true
			end if
		end if

						if not parseBool or pageListType=CommonTable or pageListType = "looprs" then
							if POP_MVC.arr.iExists( fieldsArr, fieldName) then
								tempStr = repNull(rsObj( fieldName ))
								parseBool = true
							end if
						end if
								
						if parseBool then
							if fieldArr.count > 0 then
								tempStr = FormatTempStr( tempStr , fieldArr , full_content ,rsObj  )
							end if
							nloopstr = replace(nloopstr,full_content, tempStr )
							tplExecNum = tplExecNum + 1
						elseif APP_DEBUG = 1 then
							Call POP_MVC.cmsPushTime( 0 , "不能解析标签" & full_content & "，位置：" + "{" + cmsPrefix + ":" + pageListType + " " + labelStr + "}"  )						
						end if
					next
					'lineArr(i) = nloopstr
					loopstrTotal = loopstrTotal + nloopstr
					
					rsObj.movenext
					i = i +1
					listIndex = listIndex + 1
				loop
				
			end if

			'loopstrTotal = join( lineArr, "" )
			'set lineArr = nothing
replaceContent = replace(replaceContent,matchValue,"<!-- " + "替换块标签 (" + cmsPrefix + ":" + pageListType + " " + labelStr + ") " &  FormatNumber( (timer() - startTime ) *1000,0,-1,0,0) & "ms -->" & loopstrTotal & "<!-- /替换块标签 -->")
tplExecNum = tplExecNum + 1
Call POP_MVC.cmsPushTime( startTime , "替换块标签 (" + cmsPrefix + ":" + pageListType + " " + labelStr + ") "  )
		
			rsObj.close: set rsObj = nothing
			set where = nothing
			set labelArr = nothing
			set matchesPagelist = nothing
			set matchesfield = nothing	
		Call L_( typename(Me) + ".parseList" )
	End Function
	
	Function getReplyLink( TopicID,ReplyID,page )
		dim header,url
		
		if isNul(guestConfig.ThreadUrlMode) then
			header = "?"
		else
			header = "?" + guestConfig.ThreadUrlMode + "_"
		end if
		
		if isNul(page) then
			url = header & TopicID & guestConfig.PageSuffix + "#" & ReplyID
		elseif page=1 or page = 0 then
			url = header & TopicID & guestConfig.PageSuffix + "#" & ReplyID
		else
			url = header & TopicID & "_"  & page & guestConfig.PageSuffix + "#" & ReplyID
		end if	
		getReplyLink = url
	End Function
	
	'得到封面图片
	Function Fetch_Img( str )
		Dim s_rule,  Matches, match, i, s_img, s_src, s_path
		'匹配img标签的正则
		s_rule = "<img[^>]*?\s+src\s*=\s*((?:"")([^""]+)(?:"")|(?:')([^']+)(?:')|([^\s>]+))[^>]*>"
		
		set matches = POP_MVC.String.reg_exec( str , s_rule , "gim" )
			
		for each match in matches
			if NOT POP_MVC.String.iExists( match.value , C_("NO_FLOW_STR") ) then
				Fetch_Img = match.SubMatches(1)
				exit for
			end if
		next
	End Function
	
	Function getViewContent( ByVal str , hideRepStr )
		if hideRepStr = "" then
			hideRepStr = "<p style='border:1px dashed #FF9A9A;padding:5px;'> <img src='static/images/lock.gif'> 温馨提示："
			if S_("adminName") <> "" then
				hideRepStr = hideRepStr & "（以下是本帖隐藏的内容，普通会员需要回复才能查看）"
			else
				hideRepStr = hideRepStr & "游客" & "，如果您要查看本帖隐藏内容，请 <a href='" & getLoginLink() & "'>回复</a> 才能查看</p>"
			end if
		end if

		getViewContent = POP_MVC.String.reg_replace( str, hideRepStr, "\[hide\](.+?)\[\/hide\]" , "gm" )
	End Function
	
	Function ModifyThreadContent( ByVal str )
		dim hideRepStr
		hideRepStr = "<div style='border:1px dashed #FF9A9A;padding:5px;'> <img src='static/images/lock.gif'> 温馨提示："
		if CStr(S_("IsAdmin")) = "1" then
			hideRepStr = hideRepStr & "（以下是本" & guestConfig.ThreadBriefName & "隐藏的内容，您是管理员，可以直接查看！）<br /><br />"
		else
			hideRepStr = hideRepStr & "（以下是本" & guestConfig.ThreadBriefName & "隐藏的内容，您已解锁，可以查看！）<br /><br />"
		end if
		
	
		ModifyThreadContent = POP_MVC.String.reg_replace( str, hideRepStr &  "$1" & "</div>", "\[hide\](.+?)\[\/hide\]" , "gm" )
	End Function
	
	'修饰内容
	'图片懒加载
	Function ModifyContent( ByVal str )
		on error resume next
		Dim s_rule,  Matches, match, i, s_img, s_src, s_path
		'匹配img标签的正则
		s_rule = "(<img[^>]*?\s+)(src\s*=\s*((?:"")([^""]+)(?:"")|(?:')([^']+)(?:')|([^\s\>]+)))([^>]*>)"
		

		
		set matches = POP_MVC.String.reg_exec( str , s_rule , "gim" )
			
		for each match in matches
			s_img = match.value
			if NOT POP_MVC.String.iExists( s_img , C_("NO_FLOW_STR") ) then
				str = replace( str , match.subMatches(1) ,   "src=""" + "Templates/" + guestConfig.DefaultTemplate + "/res/images/load.gif" + """ lay-" & match.subMatches(1) )
			end if
		next
		
		ModifyContent = str
		ModifyContent = escape( ModifyContent )
	End Function
	
	Function getUserData( id )	
		set getUserData = B_( baseTable ).from( "{prefix}User" ).where( "UserID = " & id ).find
	End Function
	
	Sub setUserData( id , fieldName, fieldValue )
		Call B_( "user" ).where( id  ).setField( fieldName ,fieldValue )
	End Sub
	
	Sub setUserInc( id , fieldName )
		Call B_( "user" ).where( id ).setInc( fieldName )
	End Sub
	
	Sub setUserDec( id , fieldName )
		Call B_( "user" ).where( id ).setDec( fieldName )
	End Sub
	
	'取有了新文章的栏目， {"1":"2" , "2":"1"}
	sub initNewSort
		dim rs,where		
		
		if guestConfig.NewSortTime = 0 then
			Exit sub
		end if
		
		if isEmpty(dictNewSort) then
			set where = D_		
			set dictNewSort = POP_MVC.SCD
			where("ContentStatus") = 1
			where( "AddTime" ) = Array( "hourdiff" , guestConfig.NewSortTime )
			set rs = B_( baseTable ).from("{prefix}self_GuestTopic").where(where).field("SortID,COUNT(SortID) AS cnt").group("SortID").select
			do while not rs.eof
				dictNewSort( CStr( rs("SortID").value ) ) = CStr(rs("cnt"))
				rs.MoveNext
			loop	
		end if
	End Sub
	
	'id栏目中是否有了新文章
	Function inNewSort( id )
		if guestConfig.NewSortTime = 0 then
			inNewSort = false
			Exit Function
		end if	
	
		Call initNewSort
		if dictNewSort.Exists( CStr(id) ) then
			inNewSort = true
		else
			inNewSort = false
		end if
	End Function
	
	'id栏目中新文章个数，有返回数字，无返回空
	Function getNewSortCnt( ByVal id )
		if guestConfig.NewSortTime = 0 then
			getNewSortCnt = 0
			Exit Function
		end if		
		Call initNewSort
		id = CStr(id)
		if dictNewSort.Exists( id ) then
			getNewSortCnt = dictNewSort( id )
		end if
	End Function
	
	'一般消息类通知信息在拿到时间后，会更贴近发布该内容多久时间，比如：刚刚、十分钟前、两小时前、两天前等
	'如果超过一定的时间后，则显示发布的日期。对于用户来说，更加友好地显示时间。
	Function DateStr( ByVal t )
	

		dim days,minutes
		if not isDate(t) then
			DateStr = "" : Exit Function
		end if
		days = datediff( "d" , t , now() )
	
		if days > 2 then
			if year( t ) = year( date() ) then
				DateStr = POP_MVC.FormatDate( t, "M-D" ) : Exit Function
			else
				DateStr = POP_MVC.FormatDate( t, "YYYY-MM-DD" ) : Exit Function
			end if
		elseif days = 2 then
			DateStr = "前天" : Exit Function
		elseif days = 1 then
			DateStr = "昨天" : Exit Function
		end if		

		DateStr = "刚刚"
	End Function
	
	'截取字符串"Upload/bbs/1/202006181820208793.jpg"中的"/1/202006181820208793.jpg"
	Function SubStrUserUploadFolder( path )
		if not isNul( path ) then
			SubStrUserUploadFolder = mid(path , InStrRev( path, "/" , InStrRev(path, "/") - 1))
		end if
	End Function 
	
	'获取文件的扩展名
	Function getExtName( filepath )
		dim extname
		extname = POP_MVC.file.extName( Array( filepath , true ) ) 
		extname = LCase(extname)

		getExtName = extName
	End Function
	
	'判断文件是否为图片
	'判断依据为POP_MVC.config("UPLOAD_IMAGE_TYPES")
	Function testIsImage( filepath )
		dim extname
		extname = getExtName( filepath )
	
		if POP_MVC.Arr.iExists( split( POP_MVC.config("UPLOAD_IMAGE_TYPES") , ";") , extName ) then
			testIsImage = true
		else
			testIsImage = false
		end if
	End Function
	
	Sub ShowError4html( info )
		POP_MVC.exit(  "在当前模板文件中发现：<br >" & info )
	End Sub	
End Class
%>