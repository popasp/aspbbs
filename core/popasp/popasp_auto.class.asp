<%
' +----------------------------------------------------------------------
' | POPASP [ ASP MVC ]
' +----------------------------------------------------------------------
' | Copyright (c) 2016 http://popasp.com All rights reserved.
' +----------------------------------------------------------------------
' | Author: popasp <1737025626@qq.com>
' +----------------------------------------------------------------------
' 自动完成代码类
Class POPASP_AUTO
	'控制器包含文件，如 app/Runtime/Class/IndexAction.inc.asp
	Private inc_file
	Private upload_dict
	Private upload_names
	Private token_value
	
	Private Sub Class_Initialize
		inc_file = POP_MVC.appPath & "/Runtime/Class/" & POP_MVC.c & "Action.inc.asp"
		
		token_value = Replace(timer() ,"." , "" )
		
		'进行初始化配置
		If IsEmpty( POP_MVC.config( "AUTO_VERIFY" ) )  Then POP_MVC.config( "AUTO_VERIFY" ) = "popasp_verify"
		If IsEmpty( POP_MVC.config( "AUTO_JPEGVERIFY" ) )  Then POP_MVC.config( "AUTO_JPEGVERIFY" ) = "popasp_jpegverify"
		If IsEmpty( POP_MVC.config( "AUTO_VERIFY_INPUT" ) )  Then POP_MVC.config( "AUTO_VERIFY_INPUT" ) = "popasp_verify_input"
		If IsEmpty( POP_MVC.config( "AUTO_VERIFY_IMG" ) )  Then POP_MVC.config( "AUTO_VERIFY_IMG" ) = "popasp_verify_img"
		If IsEmpty( POP_MVC.config( "AUTO_CNVERIFY" ) ) Then POP_MVC.config( "AUTO_CNVERIFY" ) = "popasp_cnverify"
		If IsEmpty( POP_MVC.config( "AUTO_REMOVEFILE" ) ) Then POP_MVC.config( "AUTO_REMOVEFILE" ) = "popasp_remove_file"
		If IsEmpty( POP_MVC.config( "AUTO_DOWNLOAD" ) ) Then POP_MVC.config( "AUTO_DOWNLOAD" ) = "download_"
		If IsEmpty( POP_MVC.config( "AUTO_UPLOAD" ) ) Then POP_MVC.config( "AUTO_UPLOAD" ) = "popasp_upload_"
		If IsEmpty( POP_MVC.config( "AUTO_CHECKADD" ) ) Then POP_MVC.config( "AUTO_CHECKADD" ) = "popasp_js_check_add"
		If IsEmpty( POP_MVC.config( "AUTO_CHECKSAVE" ) ) Then POP_MVC.config( "AUTO_CHECKSAVE" ) = "popasp_js_check_modify"
		If IsEmpty( POP_MVC.config( "AUTO_AUTO" ) ) Then POP_MVC.config( "AUTO_AUTO" ) = "popasp_js_check_modify"
		If IsEmpty( POP_MVC.config( "AUTO_DBREMOVE" ) ) Then POP_MVC.config( "AUTO_DBREMOVE" ) = "popasp_remove"
		If IsEmpty( POP_MVC.config( "AUTO_DBNOT" ) ) Then POP_MVC.config( "AUTO_DBNOT" ) = "popasp_not"
		If IsEmpty( POP_MVC.config( "AUTO_DBSET" ) ) Then POP_MVC.config( "AUTO_DBSET" ) = "popasp_set_field"
		If IsEmpty( POP_MVC.config( "AUTO_DBSWAP" ) ) Then POP_MVC.config( "AUTO_DBSWAP" ) = "popasp_swap"
		If IsEmpty( POP_MVC.config( "AUTO_DBINC" ) ) Then POP_MVC.config( "AUTO_DBINC" ) = "popasp_set_inc"
		If IsEmpty( POP_MVC.config( "AUTO_DBDEC" ) ) Then POP_MVC.config( "AUTO_DBDEC" ) = "popasp_set_dec"
		If IsEmpty( POP_MVC.config( "AUTO_DB1" ) ) Then POP_MVC.config( "AUTO_DB1" ) = "popasp_set_1"
		If IsEmpty( POP_MVC.config( "AUTO_DB0" ) ) Then POP_MVC.config( "AUTO_DB0" ) = "popasp_set_0"
		If IsEmpty( POP_MVC.config( "AUTO_CSS" ) ) Then POP_MVC.config( "AUTO_CSS" ) = "css_"
		If IsEmpty( POP_MVC.config( "AUTO_JS" ) ) Then POP_MVC.config( "AUTO_JS" ) = "js_"
		If IsEmpty( POP_MVC.config( "AUTO_IMG" ) ) Then POP_MVC.config( "AUTO_IMG" ) = "img_"
	End Sub
	
	Private Sub Class_Terminate
		If not isEmpty( upload_dict ) then
			set upload_dict = nothing
			set upload_names = nothing
		End If
	End Sub
	
	'自动化生成英文验证码
	'生成示例:<input type='text' name='verify' class=yzm' /> <img src='/admin/login.asp?c=Login&a=popasp_cnverify' onclick="javascript:this.src='/admin/login.asp?c=Login&a=popasp_verify&id='+Math.random()" style='cursor:pointer' alt='验证码' title='看不清，点击更换一张' class='' />
	'input的样式配置POPASP_VERIFY_INPUT，img的样式配置POPASP_VERIFY_IMG
	Public Property Get Verify(  )
		Verify = verify_( POP_MVC.config( "AUTO_VERIFY" ) , "auto_verify.txt" )
	End Property
	
	'用于cms中生成验证码图片
	'必须在配置中先声明 C_("CMS_CLASS_NAME")，用于明确使用哪个CMS类
	Public Property Get cmsverify4code(  )
		dim method_name,srcname
		method_name = "checkcode"
		srcname = "auto_verify.txt"

		Dim content,bReload,inc_content,url
		
		url =  P_( C_("CMS_CLASS_NAME") ).indexLinkPrefix & "checkcode"		
				
		cmsverify4code = "<img src='" & url & "' onclick=""javascript:this.src='" & url & "_'+Math.random()"" style='cursor:pointer' alt='验证码' title='看不清，点击更换一张' class='checkcode_img' />"
		
		If Not is_empty( POP_MVC.config( "SHOW_PAGE_TRACE" ) ) Then POP_MVC.pushAuto( "生成链接 " & POP_MVC.String.encodeHtml( cmsverify4code ) )

		'如果方法体已经写入Runtime/Class/IndexAction.Class.asp中，则不再往下执行		
		if method_exists( POP_MVC.c & "Action" , method_name ) then
			Exit Property
		end if	

		'是否重载
		bReload = not ctrl_inc_method( method_name,inc_content )
		
		'如果需要重载，则重新写入文件
		if bReload then
			content = POP_MVC.file_get_contents( POP_MVC.mvc_dir & "Tpl/" & srcname )
			content = Replace( content, "METHOD_NAME" , method_name )
			content = "<" & "%" & VbCrLf & inc_content & content & VbCrLf & "%" & ">"
			Call POP_MVC.file_put_contents( inc_file ,content )
			If Not is_empty( POP_MVC.config( "SHOW_PAGE_TRACE" ) ) Then POP_MVC.pushAuto( "写入方法 " & method_name & " " & POP_MVC.import("POPASP_PAGETRACE" ).readFile( inc_file , "查看文件" )  )
		end if
	End Property
	
	'用于cms中生成验证码图片
	'必须在配置中先声明 C_("CMS_CLASS_NAME")，用于明确使用哪个CMS类
	Public Property Get cmsverify(  )
		dim method_name,srcname
		method_name = "checkcode"
		srcname = "auto_verify.txt"

		Dim content,bReload,inc_content,url
		
		url =  P_( C_("CMS_CLASS_NAME") ).indexLinkPrefix & "checkcode"		
				
		cmsverify = "<input type='text' name='" & C_("SESSION_VERIFY") & "' class='checkcode_input' /> <img src='" & url & "' onclick=""javascript:this.src='" & url & "_'+Math.random()"" style='cursor:pointer' alt='验证码' title='看不清，点击更换一张' class='checkcode_img' />"
		
		If Not is_empty( POP_MVC.config( "SHOW_PAGE_TRACE" ) ) Then POP_MVC.pushAuto( "生成链接 " & POP_MVC.String.encodeHtml( cmsverify ) )

		'如果方法体已经写入Runtime/Class/IndexAction.Class.asp中，则不再往下执行		
		if method_exists( POP_MVC.c & "Action" , method_name ) then
			Exit Property
		end if	

		

		'是否重载
		bReload = not ctrl_inc_method( method_name,inc_content )
		
		'如果需要重载，则重新写入文件
		if bReload then
			content = POP_MVC.file_get_contents( POP_MVC.mvc_dir & "Tpl/" & srcname )
			content = Replace( content, "METHOD_NAME" , method_name )
			content = "<" & "%" & VbCrLf & inc_content & content & VbCrLf & "%" & ">"
			Call POP_MVC.file_put_contents( inc_file ,content )
			If Not is_empty( POP_MVC.config( "SHOW_PAGE_TRACE" ) ) Then POP_MVC.pushAuto( "写入方法 " & method_name & " " & POP_MVC.import("POPASP_PAGETRACE" ).readFile( inc_file , "查看文件" )  )
		end if
	End Property
	
	'自动化生成英文验证码
	'生成示例:<input type='text' name='verify' class=yzm' /> <img src='/admin/login.asp?c=Login&a=popasp_cnverify' onclick="javascript:this.src='/admin/login.asp?c=Login&a=popasp_verify&id='+Math.random()" style='cursor:pointer' alt='验证码' title='看不清，点击更换一张' class='' />
	'input的样式配置POPASP_VERIFY_INPUT，img的样式配置POPASP_VERIFY_IMG
	Public Property Get jpegVerify(  )
		jpegVerify = verify_( POP_MVC.config( "AUTO_JPEGVERIFY" ) , "auto_jpegverify.txt" )
	End Property
	
	'自动化生成中文验证码
	'生成示例:<input type='text' name='verify' class=yzm' /> <img src='/admin/login.asp?c=Login&a=popasp_cnverify' onclick="javascript:this.src='/admin/login.asp?c=Login&a=popasp_cnverify&id='+Math.random()" style='cursor:pointer' alt='验证码' title='看不清，点击更换一张' class='' />
	'input的样式配置POPASP_VERIFY_INPUT，img的样式配置POPASP_VERIFY_IMG	
	Public Property Get cnVerify(  )
		cnVerify = verify_( POP_MVC.config( "AUTO_CNVERIFY" ) , "auto_cnverify.txt" )
	End Property
	
	'删除文件(夹)
	Public Function removeFile( file )
		Dim method_name,content,bReload,inc_content
		
		method_name = POP_MVC.config( "AUTO_REMOVEFILE" )
		
		removeFile = URL__ & method_name & "&file=" & POP_MVC.realPath(file)
		
		If Not is_empty( POP_MVC.config( "SHOW_PAGE_TRACE" ) ) Then POP_MVC.pushAuto( "生成链接 " & removeFile )
		
		'如果方法体已经写入Runtime/Class/IndexAction.Class.asp中，则不再往下执行		
		if method_exists( POP_MVC.c & "Action" , method_name ) then
			Exit Function
		end if		

		'是否重载
		bReload = not ctrl_inc_method( method_name,inc_content )
		
		'如果需要重载，则重新写入文件
		if bReload then
			content = POP_MVC.file_get_contents( POP_MVC.mvc_dir & "Tpl/auto_removeFile.txt" )
			content = Replace( content, "METHOD_NAME" , method_name )
			content = "<" & "%" & VbCrLf & inc_content & content & VbCrLf & "%" & ">"
			Call POP_MVC.file_put_contents( inc_file ,content )
			If Not is_empty( POP_MVC.config( "SHOW_PAGE_TRACE" ) ) Then POP_MVC.pushAuto( "写入方法 " & method_name & " " & POP_MVC.import("POPASP_PAGETRACE" ).readFile( inc_file , "查看文件" )  )
		end if
	End Function
	
	'下载的自动完成
	'参数同POP_MVC.Download
	'自动写入下载的控制器方法
	'自动完成下载链接的输出
	'比如：<a href="<%=P_("auto").download( Array("./初一期末成绩单.xls" , "成绩.xls" ) )%>">下载excel</a>
	'使用该方法会使用一次md5，因此性能会降低大约4ms
	Public Property Get Download( arg )
		Dim method_name,content,bReload,download_path,inc_content,file_path
		
		if not isArray( arg ) then
			download_path = chr(34) & arg & chr(34)
			file_path = POP_MVC.file.basename( arg )
		else
			download_path = "Array( """ & join( arg , """,""" ) &  """ )"			
			file_path = POP_MVC.file.basename(arg(0))
			if ubound( arg ) > 0 then file_path = arg(1)
		end if
		
		'方法名
		method_name = POP_MVC.config( "AUTO_DOWNLOAD" ) & md5( download_path )
		
		Download = URL__ & method_name
		
		If Not is_empty( POP_MVC.config( "SHOW_PAGE_TRACE" ) ) Then POP_MVC.pushAuto( "生成下载链接" & " <a href='>" & Download  & "'>" & "下载“" & file_path & "”</a>" )
		
		'如果方法体已经写入Runtime/Class/IndexAction.Class.asp中，则不再往下执行		
		if method_exists( POP_MVC.c & "Action" , method_name ) then
			Exit Property
		end if		

		'是否重载
		bReload = not ctrl_inc_method( method_name,inc_content )
		
		'如果需要重载，则重新写入文件
		if bReload then
			content = POP_MVC.file_get_contents( POP_MVC.mvc_dir & "Tpl/auto_download.txt" )
			content = Replace( content, "METHOD_NAME" , method_name )
			content = Replace( content, "DOWNLOAD_PATH" , download_path )
			content = "<" & "%" & VbCrLf & inc_content & content & VbCrLf & "%" & ">"
			Call POP_MVC.file_put_contents( inc_file ,content )
			If Not is_empty( POP_MVC.config( "SHOW_PAGE_TRACE" ) ) Then POP_MVC.pushAuto( "写入方法 " & method_name & " " & POP_MVC.import("POPASP_PAGETRACE" ).readFile( inc_file , "查看文件" )  )
		end if
	End Property
	
	'上传文件的自动完成
	'自动生成上传文件popasp_upload_type.asp
	'自动完成上传链接的生成
	'参数：字符串或数组 Array( input_name(一般与数据表中的字段名相同),文件类型,附加参数,默认值,弹窗参数 )
	'可配置两个样式
	'input : POPASP_UPLOAD_INPUT
	'a : POPASP_UPLOAD_A
	Public Property Get Upload( arg )
		Dim upfile_name,srcname,content,upload_types,input_name,token_key,input_index,ex_args,def_value
		
		upload_types = POP_MVC.config("UPLOAD_ALLOW_TYPES")	'默认上传类型
		
		open_window_args = "width=500,height=240,left='+(window.screen.availWidth - 500 )/2+',top='+(window.screen.availHeight - 240 )/2+',status=yes,toolbar=no,menubar=no,location=no"		
		
		ex_args = ""
		def_value = ""
		if isArray( arg ) then		
			input_name = arg( 0 )
			if Ubound( arg ) > 0 then 
				if not isNull( arg(1) ) then upload_types = arg(1)
			end if
			if Ubound( arg ) > 1 then 
				if NOt isNull(arg(2)) then ex_args = arg(2) & "&"
			end if
			if Ubound( arg ) > 2 then 
				if NOt isNull(arg(3)) then def_value = arg(3)
			end if
			if Ubound( arg ) > 3 then 
				if not isNull(arg(4)) then	open_window_args = arg(4)
			end if
		else
			input_name = arg					
		end if
		
		'如果指定upload_types为空，则取配置
		if trim(upload_types) = "" then
			upload_types = POP_MVC.config("UPLOAD_ALLOW_TYPES")
		end if
		
		if isEmpty( upload_dict ) then
			set upload_dict = D_
		end if
		
		if isEmpty( upload_names ) then
			set upload_names = D_
		end if
		
		if upload_names.Exists( input_name ) then
			upload_names( input_name ) = upload_names( input_name ) + 1
		else
			upload_names( input_name ) = 0
		End If
		
		input_index = upload_names( input_name )
		
		'上传文件名auto_upload_types
		upfile_name = "./" & POP_MVC.config("AUTO_FOLDER_NAME") & "/" & POP_MVC.config( "AUTO_UPLOAD" ) & md5(POP_MVC.String.reg_replace( upload_types , "" , "\W" , "gi" )) & ".asp"
		
		token_key = POP_MVC.file.basename( Array(upfile_name ,1 ) )
		
		session( token_key & "_" & token_value ) = token_value
		
		'html代码
		Upload = "<input type='text' id='" & input_name & "' name='" & input_name & "' class='" & POP_MVC.config("POPASP_UPLOAD_INPUT") & "' value='" & def_value & "'/> <a class='" & POP_MVC.config("POPASP_UPLOAD_A") & "' href='###' onclick=""javascript:window.open('" & mid( upfile_name , 3 ) & "?token_key=" & token_key & "&token_value=" & token_value & "&input_name=" & input_name & "&" & ex_args & "input_index=" & input_index & "','popasp文件上传','" & open_window_args & "')"">上传</a>"
		
		upload_dict( upload_types ) = "<input type='text' id='" & input_name & "' name='" & input_name & "' class='" & POP_MVC.config("POPASP_UPLOAD_INPUT") & "' value='" & def_value & "'/> <a class='" & POP_MVC.config("POPASP_UPLOAD_A") & "' href='###' onclick=""javascript:window.open('" & mid( upfile_name , 3 ) & "?token_key=" & token_key & "&token_value=" & token_value & "&input_name=" & input_name & "&" & ex_args & "input_index=" & input_index & "','popasp文件上传','" & open_window_args & "')"">上传</a>"
		
		If Not is_empty( POP_MVC.config( "SHOW_PAGE_TRACE" ) ) Then POP_MVC.pushAuto( "生成上传html " & Server.HtmlEncode( Upload ) )
		
		'自动生成上传文件
		srcname = POP_MVC.mvc_dir & "Tpl/auto_upload.txt"
		if Not POP_MVC.file.isExists( upfile_name ) then		
			content = POP_MVC.file_get_contents( srcname )
			content = Replace( content, "MVC_DIR" , "../" & POP_MVC.String.ltrim(POP_MVC.String.ltrim(POP_MVC.mvc_dir , "./" ) , ".\" ) )
			content = Replace( content, "APP_PATH" , "../" & POP_MVC.String.ltrim(POP_MVC.String.ltrim(POP_MVC.appPath , "./" ) , ".\" ) )
			content = Replace( content, "UPLOAD_TYPES" , upload_types )
			Call POP_MVC.file_put_contents( upfile_name , content )
			If Not is_empty( POP_MVC.config( "SHOW_PAGE_TRACE" ) ) Then POP_MVC.pushAuto( "生成上传文件 " & upfile_name & POP_MVC.import("POPASP_PAGETRACE" ).readFile( upfile_name , "查看文件" ) )
		end if
	End Property
	
	'自动生成添加数据的html,界面粗糙，只可作为演示或者管理员临时修改数据使用
	'使用: <body> <%=P_("auto").htmlAdd(模型名)%> </body>	
	'该方法中使用了 checkAdd 、dbAdd ，形成了一套完整的可前后台验证的添加数据模块
	'如果模型名与表名不一致，而又传入的是表名，则起不到前后台验证效果
	'生成的代码大致如下:
	'<script src='save.asp?c=控制器名&a=popasp_js_check_add'></script>
	'<form style='width:50%;' method='post' action='add.asp?c=控制器名&a=popasp_do_add' onsubmit="javascript:return popasp_check();"><table class='popasp_table'>......</table></form>
	Public Property Get htmlAdd( arg )
		dim html,data,prikey,m,model_name,fieldRev,i
		
		if not isArray( arg ) then
			model_name = arg
			fieldRev = Array()
		else
			model_name = arg(0)
			if ubound( arg ) > 0 then fieldRev = arg(1)
			if not isArray( fieldRev ) Then
				fieldRev = split( fieldRev,"," )
			end if
		end if	
		
		set m = M_(model_name).db

		set data = m.getTableFields(m.tableName)

		if not is_empty( m.getPK ) then
			Call POP_MVC.Dict.iRemove( data,m.prikey )
		end if
		
		for i = 0 to ubound( fieldRev )
			if data.Exists( fieldRev(i) ) then
				data.remove( fieldRev(i) )
			end if
		next
		
		html = Me.checkAdd( model_name )
		html = html & "<form style='width:50%;margin:0 auto;' method='post' action='" & Me.dbAdd( model_name ) & "' onsubmit=""javascript:return popasp_check();"">" & vbcrlf
		html = html & "<table class='popasp_table'>" & vbcrlf
		if typename( data ) = "Recordset" then set data = POP_MVC.rs2dict( data )
		for each key in data
			html = html & vbTab & "<tr>" & vbcrlf
			html = html & vbTab & vbTab & "<td style='text-align:center;'>" & key & "</td>" & vbcrlf
			html =  html & vbTab & vbTab & "<td style='padding:0px 10px;margin:0;'><input name='" & key & "' style='width:90%;height:30px;margin:0;border:1px dashed green;padding:0 10px;' /></td>" & vbcrlf
			html = html & vbTab & "</tr>" & vbcrlf			
		next
		html = html & vbTab & "<tr><td colspan='2' style='text-align:center;'><input type='submit' value='添加'></td></tr>" & vbcrlf
		html = html & "</table>" & vbcrlf
		html = html & "</form>" & vbcrlf
		htmlAdd = html
	End Property
	
	'自动生成修改数据的html,界面粗糙，只可作为演示或者管理员临时修改数据使用
	'使用: <body> <%=P_("auto").htmlSave(表名或模型名)%> </body>
	'该方法中使用了 checkSave 、auto、 dbSave，形成了一套完整的可前后台验证的添加数据模块
	'如果模型名与表名不一致，而又传入的是表名，则起不到前后台验证效果
	'浏览器地址栏必须传入id，如 save.asp?id=1，否则无法定位修改哪条记录
	'在html中该方法使用js进行数据自动填充
	'生成的代码大致如下:
	'<script src='save.asp?c=控制器名&a=popasp_js_check_modify'></script>
	'<script src='save.asp?c=控制器名&a=popasp_js_auto&fieldRev=&id=ID'></script>
	'<form style='width:50%;' method='post' action='save.asp?c=控制器名&a=popasp_do_modify' onsubmit="javascript:return popasp_check();"><table class='popasp_table'>......</table><input type='hidden' name='主键名' value='' /></form>
	Public Function htmlSave( arg )
		dim html,data,prikey,m,model_name,id
		
		id = POP_MVC.get("id")
		if not isArray( arg ) then
			model_name = arg			
		else
			model_name = arg(0)
			if ubound( arg ) > 0 then id = arg(1)
		end if		
		
		set m = M_(model_name).db
		set data = m.getTableFields(m.tableName)

		if not is_empty( m.prikey ) then
			data.remove( m.prikey )
		end if
		
		html = Me.checkSave( model_name )
		html = html & Me.auto( model_name , id , "" )
		
		html = html & "<form style='width:50%;;margin:0 auto;' method='post' action='" & Me.dbSave( model_name ) & "' onsubmit=""javascript:return popasp_check();"">" & vbcrlf
		html = html & "<table class='popasp_table'>" & vbcrlf
		if typename( data ) = "Recordset" then set data = POP_MVC.rs2dict( data )
		for each key in data
			html = html & vbTab & "<tr>" & vbcrlf
			html = html & vbTab & vbTab & "<td style='text-align:center;'>" & key & "</td>" & vbcrlf
			html =  html & vbTab & vbTab & "<td style='padding:0px 10px;margin:0;'><input name='" & key & "' style='width:90%;height:30px;margin:0;border:1px dashed green;padding:0 10px;' /></td>" & vbcrlf
			html = html & vbTab & "</tr>" & vbcrlf			
		next
		html = html & vbTab & "<tr><td colspan='2' style='text-align:center;'><input type='submit' value='修改'></td></tr>" & vbcrlf
		html = html & "</table>" & vbcrlf
		html = html & "<input type='hidden' name='" & m.prikey & "' value='' />" & vbcrlf
		html = html & "</form>" & vbcrlf
		html = html & "<script>" & vbcrlf
		html = html & "popasp_auto();" & vbcrlf
		html = html & "</script>" & vbcrlf
		htmlSave = html
	End Function
	
	'生成目录列表
	public Function htmlList( ByRef rs , ByRef tableName , ByRef prikey, ByRef save_asp )
		dim html,i,k1,k2,item,j
		dim dict,page
		page = P_( Array("POPASP_PAGE" , POP_MVC.config("VAR_PAGE") ) )(rs).show
		set dict = POP_MVC.import("POPASP_CONTROLLER").rs2dict(rs)
		
		if dict.count > 0 then
			html = "<table class='popasp_table'>"
			i = 0
		end if
		
		for each k1 in dict			
			set item = dict(k1)			
			if i = 0 then
				html = html & VbCrLf & vbTab & "<tr>"
				html = html & VbCrLf & vbTab & vbTab
					for each k2 in item
						html = html & "<th>" & k2 & "</th>"
					next
					html = html & "<th>操作</th>"
				html = html & VbCrLf & vbTab & "</tr>"
			end if

	
			if i mod 2 = 1 then
				html = html & VbCrLf & vbTab & "<tr class='popasp_even_tr'>"
			else
				html = html & VbCrLf & vbTab & "<tr>"
			end if
			
			html = html & VbCrLf & vbTab & vbTab
			j = 0
			for each k2 in item
				if j = 0 then
					html = html & "<td style='text-align:center;'>" & item(k2) & "</td>"
				else
					html = html & "<td>" & item(k2) & "</td>"
				end if
				j = j + 1
			next
			
			
			if item.exists(prikey) then
				html = html & "<td style='text-align:center'><a href='" & save_asp & "?id=" & item(prikey) & "'>修改</a> | <a href='" & Me.dbRemove( tableName, item(prikey) ) & "' onclick=""javascript: return confirm('您确信要删除吗？删除之后，将无法恢复！')"">删除</a></td>"
			else
				html = html & "<td style='text-align:center'>&nbsp;</td>"
			end if

			html = html & VbCrLf & vbTab & "</tr>"				

			i = i + 1
		next
		
		
		if dict.count > 0 then
			html = html & VbCrLf & vbTab & "<tr><td colspan='" & ( item.count + 1) & "' style='text-align:center;'>" & page & "</td></tr>"
			
			set item = nothing
			html = html & VbCrLf & "</table>"
		end if
		htmlList = html
	End Function
	
	
	'数据模型添加数据时的js前端验证
	'自动写入控制器相关方法
	'自动完成链接的输出
	'使用了db.validate_，如果模型名与表名不一致，而又传入的是表名，则起不到前端验证效果
	'比如：在<head><%=P_("auto").checkAdd( Array( 模型名 , 方法名 ) )%></head>
	'生成字符串为js，如<script src="article.asp?c=Article&a=popasp_js_check_add"></script>
	'在form表单中还须使用popasp_check <form method="post" action="" onsubmit="return popasp_check();" >
	Public Property Get checkAdd( arg )
		checkAdd = checkMethod( arg ,POP_MVC.config( "AUTO_CHECKADD" ) , "auto_checkAdd.txt"  )
	End Property
	
	'数据模型修改数据时的js前端验证
	'自动写入控制器相关方法
	'自动完成链接的输出
	'使用了db.validate_，如果模型名与表名不一致，而又传入的是表名，则起不到前端验证效果
	'比如：在<head><%=P_("auto").checkSave( 模型名 , 方法名 )%></head>
	'生成字符串为js，如<script src="article.asp?c=Article&a=popasp_js_check_modify"></script>
	'在form表单中还须使用popasp_check <form method="post" action="" onsubmit="return popasp_check();" >
	Public Property Get checkSave( arg )
		checkSave = checkMethod( arg ,POP_MVC.config( "AUTO_CHECKSAVE" ) , "auto_checkSave.txt"  )
	End Property	
	
	'js填充表单
	'自动写入控制器相关方法
	'自动完成链接的输出	
	'参数说明：model_name为表名或模型名，id为数据表中某条记录的主键值，fieldRev为排除字段
	'fieldRev相当于db.fieldRev(排除字段)中的参数，取值为字符串，如"password,other_field"
	'使用：<head><%=P_("auto").auto( 表名或模型名, id , 排除的字段 )%></head>    若的id一般用POP_MVC.get("id")获取
	'生成的js代码为:<script src='article_save.asp?c=Index&a=popasp_js_auto&fieldRev=&id=27'></script>
	'在表单的hmtl下面还需要写入js代码<script>popasp_auto();</script>
	Public Function [auto]( model_name , id , fieldRev )
		Dim method_name,content,bReload,inc_content

		method_name = "popasp_js_auto"

		[auto] = "<script src='" &  URL__ & method_name & "&fieldRev=" & fieldRev & "&id=" & id & "'></script>" & VbCrLf
		
		If Not is_empty( POP_MVC.config( "SHOW_PAGE_TRACE" ) ) Then POP_MVC.pushAuto( "生成JS代码 " & Server.HtmlEncode([auto]) )		
		
		'如果方法体已经写入Runtime/Class/IndexAction.Class.asp中，则不再往下执行		
		if method_exists( POP_MVC.c & "Action" , method_name ) then
			Exit Function
		end if		

		'是否重载
		bReload = not ctrl_inc_method( method_name,inc_content )
		
		'如果需要重载，则重新写入文件
		if bReload then
			content = POP_MVC.file_get_contents( POP_MVC.mvc_dir & "Tpl/auto_auto.txt" )
			content = Replace( content, "METHOD_NAME" , method_name )
			content = Replace( content, "MODEL_NAME" , model_name )
			content = Replace( content, "FIELDREV" , fieldRev )
			content = "<" & "%" & VbCrLf & inc_content & content & VbCrLf & "%" & ">"
			Call POP_MVC.file_put_contents( inc_file ,content )
			If Not is_empty( POP_MVC.config( "SHOW_PAGE_TRACE" ) ) Then POP_MVC.pushAuto( "写入方法 " & method_name & " " & POP_MVC.import("POPASP_PAGETRACE" ).readFile( inc_file , "查看文件" )  )
		end if
	End Function	
	
	'数据模型添加的自动完成
	'自动写入控制器相关方法
	'参数：字符串或数组 Array( 模型名 , 控制器的方法名 ) ，控制器的方法名默认popasp_do_add，自定义时不能与其他冲突
	'使用了db.create，如果模型名与表名不一致，而又传入的是表名，则db.create永远为真
	'自动完成链接的输出
	'比如：<form method="post" action="<%=P_("auto").dbAdd("模型名")%>">
	Public Property Get dbAdd( arg )
		dbAdd = dbAddOrDbSave( arg ,"popasp_do_add" , "auto_dbAdd.txt"  )
	End Property
	
	'数据模型修改的自动完成
	'注意：表单提交数据必须含主键ID
	'参数：字符串或数组 Array( 模型名 , 控制器的方法名 ) ，控制器的方法名默认popasp_do_modify，自定义时不能与其他冲突
	'使用了db.create，如果模型名与表名不一致，而又传入的是表名，则db.create永远为真
	'自动写入控制器相关方法
	'自动完成链接的输出
	'比如：<form method="post" action="<%=P_("auto").dbSave("模型名")%>">
	Public Property Get dbSave( arg )
		dbSave = dbAddOrDbSave( arg ,"popasp_do_modify" , "auto_dbSave.txt"  )
	End Property	
	
	'数据模型删除一行或多行记录的操作
	'数据模型删除的自动完成，使用了db.Remove
	'自动写入控制器相关方法
	'自动完成链接的输出
	'比如：<a href="<%=P_("auto").dbRemove(表名,ID)%>" onclick="javascript: return confirm('您确信要删除吗？删除之后，将无法恢复！')">删除</a>
	Public Function dbRemove( model_name , id )
		Dim method_name,content,bReload,inc_content
		
		method_name = POP_MVC.config( "AUTO_DBREMOVE" )
		
		if isArray( id ) then
			dbRemove = URL__ & method_name & "&id=" & join(id,",")
		else
			dbRemove = URL__ & method_name & "&id=" & id
		end if
		
		If Not is_empty( POP_MVC.config( "SHOW_PAGE_TRACE" ) ) Then POP_MVC.pushAuto( "生成链接 " & dbRemove )
		
		'如果方法体已经写入Runtime/Class/IndexAction.Class.asp中，则不再往下执行		
		if method_exists( POP_MVC.c & "Action" , method_name ) then
			Exit Function
		end if		

		'是否重载
		bReload = not ctrl_inc_method( method_name,inc_content )
		
		'如果需要重载，则重新写入文件
		if bReload then
			content = POP_MVC.file_get_contents( POP_MVC.mvc_dir & "Tpl/auto_dbRemove.txt" )
			content = Replace( content, "METHOD_NAME" , method_name )
			content = Replace( content, "MODEL_NAME" , model_name )
			content = "<" & "%" & VbCrLf & inc_content & content & VbCrLf & "%" & ">"
			Call POP_MVC.file_put_contents( inc_file ,content )
			If Not is_empty( POP_MVC.config( "SHOW_PAGE_TRACE" ) ) Then POP_MVC.pushAuto( "写入方法 " & method_name & " " & POP_MVC.import("POPASP_PAGETRACE" ).readFile( inc_file , "查看文件" )  )
		end if
	End Function
	
	
	
	'数据模型取反的自动完成，使用了db.setNot
	'自动写入控制器相关方法
	'自动完成链接的输出
	'比如：<a href="<%=P_("auto").dbNot(表名,ID,字段名)%>" >值取反</a>
	Public Function dbNot( model_name , id , field_name )
		Dim method_name,content,bReload,inc_content
		
		method_name = POP_MVC.config( "AUTO_DBNOT" )
		
		if isArray( id ) then
			dbNot = URL__ & method_name & "&field=" & field_name & "&id=" & join(id,",")
		else
			dbNot = URL__ & method_name & "&field=" & field_name & "&id=" & id
		end if
		
		If Not is_empty( POP_MVC.config( "SHOW_PAGE_TRACE" ) ) Then POP_MVC.pushAuto( "生成链接 " & dbNot )
		
		'如果方法体已经写入Runtime/Class/IndexAction.Class.asp中，则不再往下执行		
		if method_exists( POP_MVC.c & "Action" , method_name ) then
			Exit Function
		end if		

		'是否重载
		bReload = not ctrl_inc_method( method_name,inc_content )
		
		'如果需要重载，则重新写入文件
		if bReload then
			content = POP_MVC.file_get_contents( POP_MVC.mvc_dir & "Tpl/auto_dbNot.txt" )
			content = Replace( content, "METHOD_NAME" , method_name )
			content = Replace( content, "MODEL_NAME" , model_name )
			content = "<" & "%" & VbCrLf & inc_content & content & VbCrLf & "%" & ">"
			Call POP_MVC.file_put_contents( inc_file ,content )
			If Not is_empty( POP_MVC.config( "SHOW_PAGE_TRACE" ) ) Then POP_MVC.pushAuto( "写入方法 " & method_name & " " & POP_MVC.import("POPASP_PAGETRACE" ).readFile( inc_file , "查看文件" )  )
		end if
	End Function
	
	'数据模型取反的自动完成，使用了db.swap
	'自动写入控制器相关方法
	'自动完成链接的输出
	'比如：<a href="<%=P_("auto").dbSwap(表名,ids,字段名)%>" >交换</a>
	Public Function dbSwap( model_name , ids , field_name )
		Dim method_name,content,bReload,inc_content
		
		method_name = POP_MVC.config( "AUTO_DBSWAP" )
		
		if isArray( ids ) then
			dbSwap = URL__ & method_name & "&field=" & field_name & "&ids=" & join(ids,",")
		else
			dbSwap = URL__ & method_name & "&field=" & field_name & "&ids=" & ids
		end if
		
		If Not is_empty( POP_MVC.config( "SHOW_PAGE_TRACE" ) ) Then POP_MVC.pushAuto( "生成链接 " & dbSwap )
		
		'如果方法体已经写入Runtime/Class/IndexAction.Class.asp中，则不再往下执行		
		if method_exists( POP_MVC.c & "Action" , method_name ) then
			Exit Function
		end if		

		'是否重载
		bReload = not ctrl_inc_method( method_name,inc_content )
		
		'如果需要重载，则重新写入文件
		if bReload then
			content = POP_MVC.file_get_contents( POP_MVC.mvc_dir & "Tpl/auto_dbSwap.txt" )
			content = Replace( content, "METHOD_NAME" , method_name )
			content = Replace( content, "MODEL_NAME" , model_name )
			content = "<" & "%" & VbCrLf & inc_content & content & VbCrLf & "%" & ">"
			Call POP_MVC.file_put_contents( inc_file ,content )
			If Not is_empty( POP_MVC.config( "SHOW_PAGE_TRACE" ) ) Then POP_MVC.pushAuto( "写入方法 " & method_name & " " & POP_MVC.import("POPASP_PAGETRACE" ).readFile( inc_file , "查看文件" )  )
		end if
	End Function
	
	'数据模型设置值的自动完成，使用了db.setField，并只能设置一个字段的值
	'自动写入控制器相关方法
	'自动完成链接的输出
	'比如：<a href="<%=P_("auto").dbSet(表名,ID,字段名,字段值)%>" >设置值</a>
	Public Function dbSet( model_name , id , field_name , field_value )
		dbSet = dbIncDecSet( model_name , id , field_name , field_value , "auto_dbSet.txt" , POP_MVC.config( "AUTO_DBSET" ) )
	End Function
	
	'数据模型自增长的自动完成，使用了db.setInc
	'自动写入控制器相关方法
	'自动完成链接的输出
	'比如：<a href="<%=P_("auto").dbInc(表名,ID,字段名,增长值)%>" >自增</a>
	Public Function dbInc( model_name , id , field_name , inc_value )
		dbInc = dbIncDecSet( model_name , id , field_name , inc_value , "auto_dbInc.txt" , POP_MVC.config( "AUTO_DBINC" ) )
	End Function
	
	'数据模型自减小的自动完成，使用了db.setDec
	'自动写入控制器相关方法
	'自动完成链接的输出
	'比如：<a href="<%=P_("auto").dbDec(表名,ID,字段名,减小值)%>" >自减</a>
	Public Function dbDec( model_name , id , field_name , dec_value )
		dbDec = dbIncDecSet( model_name , id , field_name , dec_value , "auto_dbDec.txt" , POP_MVC.config( "AUTO_DBDEC" ) )
	End Function
	
	'数据模型值设置为1的自动完成，使用了db.setField
	'自动写入控制器相关方法
	'自动完成链接的输出
	'比如：<a href="<%=P_("auto").db1(表名,ID,字段名)%>" >值设为1</a>	
	Public Function db1( model_name,id,field_name )
		db1 = db01( model_name , id , field_name , 1 , POP_MVC.config( "AUTO_DB1" ) )
	End Function	
	
	'数据模型值设置为0的自动完成，使用了db.setField
	'自动写入控制器相关方法
	'自动完成链接的输出
	'比如：<a href="<%=P_("auto").db0(表名,ID,字段名)%>" >值设为0</a>
	Public Function db0( model_name,id,field_name )
		db0 = db01( model_name , id , field_name , 0 , POP_MVC.config( "AUTO_DB0" ) )
	End Function
	
	'根据文件路径自动化生成js标签
	'在控制器中写入相应方法,如果不指定方法名,则使用 css_9277810cff364712a952c46890445e73(32位md5加密值)
	'采用自动生成方法名时,因md5加密,性能降低约4ms
	Public Property Get css( arg )
		Dim method_name,content,bReload,inc_content,file_path
		
		if not isArray( arg ) then
			file_path = arg
		else
			file_path = arg(0)
			if ubound( arg ) > 0 then method_name = arg(1)
		end if
		
		If method_name = "" then
			method_name = POP_MVC.config( "AUTO_CSS" ) & md5( POP_MVC.realPath( file_path ) )
		End If	
	
		css = "<link href='" & URL__ & method_name &"' rel='stylesheet' type='text/css' />"  
		
		If Not is_empty( POP_MVC.config( "SHOW_PAGE_TRACE" ) ) Then POP_MVC.pushAuto( "生成css的link标签 " & Server.HtmlEncode( css ) )
		
		'如果方法体已经写入Runtime/Class/IndexAction.Class.asp中，则不再往下执行		
		if method_exists( POP_MVC.c & "Action" , method_name ) then
			Exit Property
		end if		

		'是否重载
		bReload = not ctrl_inc_method( method_name,inc_content )
		
		'如果需要重载，则重新写入文件
		if bReload then
			content = POP_MVC.file_get_contents( POP_MVC.mvc_dir & "Tpl/auto_css.txt" )
			content = Replace( content, "METHOD_NAME" , method_name )
			content = Replace( content, "FILE_PATH" , file_path )
			content = "<" & "%" & VbCrLf & inc_content & content & VbCrLf & "%" & ">"
			Call POP_MVC.file_put_contents( inc_file ,content )
			If Not is_empty( POP_MVC.config( "SHOW_PAGE_TRACE" ) ) Then POP_MVC.pushAuto( "写入方法 " & method_name & " " & POP_MVC.import("POPASP_PAGETRACE" ).readFile( inc_file , "查看文件" )  )
		end if		
	End Property
	
	'根据文件路径自动化生成js标签
	'在控制器中写入相应方法,如果不指定方法名,则使用 js_d1ea86dba599577538de3e7e8adcba2e(32位md5加密值)
	'采用自动生成方法名时,因md5加密,性能降低约4ms
	Public Property Get js( arg )
		Dim method_name,content,bReload,inc_content,file_path
		
		if not isArray( arg ) then
			file_path = arg
		else
			file_path = arg(0)
			if ubound( arg ) > 0 then method_name = arg(1)
		end if
		
		If method_name = "" then
			method_name = POP_MVC.config( "AUTO_JS" ) & md5( POP_MVC.realPath( file_path ) )
		End If	
	
		js = "<script type='text/javascript' src='" & URL__ & method_name & "'></script>"		

		If Not is_empty( POP_MVC.config( "SHOW_PAGE_TRACE" ) ) Then POP_MVC.pushAuto( "生成js的script标签 " & Server.HtmlEncode( js ) )
		
		'如果方法体已经写入Runtime/Class/IndexAction.Class.asp中，则不再往下执行		
		if method_exists( POP_MVC.c & "Action" , method_name ) then
			Exit Property
		end if		

		'是否重载
		bReload = not ctrl_inc_method( method_name,inc_content )
		
		'如果需要重载，则重新写入文件
		if bReload then
			content = POP_MVC.file_get_contents( POP_MVC.mvc_dir & "Tpl/auto_js.txt" )
			content = Replace( content, "METHOD_NAME" , method_name )
			content = Replace( content, "FILE_PATH" , file_path )
			content = "<" & "%" & VbCrLf & inc_content & content & VbCrLf & "%" & ">"
			Call POP_MVC.file_put_contents( inc_file ,content )
			If Not is_empty( POP_MVC.config( "SHOW_PAGE_TRACE" ) ) Then POP_MVC.pushAuto( "写入方法 " & method_name & " " & POP_MVC.import("POPASP_PAGETRACE" ).readFile( inc_file , "查看文件" )  )
		end if		
	End Property
	
	'根据文件路径自动化生成img地址，注意，不是img的标签
	'在控制器中写入相应方法,如果不指定方法名,则使用 img_9277810cff364712a952c46890445e73(32位md5加密值)
	'采用自动生成方法名时,因md5加密,性能降低约4ms
	Public Property Get img( arg )
		Dim method_name,content,bReload,inc_content,file_path
		
		if not isArray( arg ) then
			file_path = arg
		else
			file_path = arg(0)
			if ubound( arg ) > 0 then method_name = arg(1)
		end if
		
		If method_name = "" then
			method_name = POP_MVC.config( "AUTO_IMG" ) & md5( POP_MVC.realPath( file_path ) )
		End If	
	
		img = URL__ & method_name
		
		If Not is_empty( POP_MVC.config( "SHOW_PAGE_TRACE" ) ) Then POP_MVC.pushAuto( "生成img的src地址 " & img )
		
		'如果方法体已经写入Runtime/Class/IndexAction.Class.asp中，则不再往下执行		
		if method_exists( POP_MVC.c & "Action" , method_name ) then
			Exit Property
		end if		

		'是否重载
		bReload = not ctrl_inc_method( method_name,inc_content )
		
		'如果需要重载，则重新写入文件
		if bReload then
			content = POP_MVC.file_get_contents( POP_MVC.mvc_dir & "Tpl/auto_img.txt" )
			content = Replace( content, "METHOD_NAME" , method_name )
			content = Replace( content, "FILE_PATH" , file_path )
			content = "<" & "%" & VbCrLf & inc_content & content & VbCrLf & "%" & ">"
			Call POP_MVC.file_put_contents( inc_file ,content )
			If Not is_empty( POP_MVC.config( "SHOW_PAGE_TRACE" ) ) Then POP_MVC.pushAuto( "写入方法 " & method_name & " " & POP_MVC.import("POPASP_PAGETRACE" ).readFile( inc_file , "查看文件" )  )
		end if		
	End Property

	
	Private Function db01( model_name , id , field_name , field_value , method_name )
		Dim content,bReload,inc_content
		
		if isArray( id ) then
			db01 = URL__ & method_name & "&field=" & field_name & "&id=" & join(id,",")
		else
			db01 = URL__ & method_name & "&field=" & field_name & "&id=" & id
		end if
		
		If Not is_empty( POP_MVC.config( "SHOW_PAGE_TRACE" ) ) Then POP_MVC.pushAuto( "生成链接 " & db01 )
		
		'如果方法体已经写入Runtime/Class/IndexAction.Class.asp中，则不再往下执行		
		if method_exists( POP_MVC.c & "Action" , method_name ) then
			Exit Function
		end if		

		'是否重载
		bReload = not ctrl_inc_method( method_name,inc_content )
		
		'如果需要重载，则重新写入文件
		if bReload then
			content = POP_MVC.file_get_contents( POP_MVC.mvc_dir & "Tpl/auto_db01.txt" )
			content = Replace( content, "METHOD_NAME" , method_name )
			content = Replace( content, "MODEL_NAME" , model_name )
			content = Replace( content, "FIELD_VALUE" , field_value )
			content = "<" & "%" & VbCrLf & inc_content & content & VbCrLf & "%" & ">"
			Call POP_MVC.file_put_contents( inc_file ,content )
			If Not is_empty( POP_MVC.config( "SHOW_PAGE_TRACE" ) ) Then POP_MVC.pushAuto( "写入方法 " & method_name & " " & POP_MVC.import("POPASP_PAGETRACE" ).readFile( inc_file , "查看文件" )  )
		end if
	End Function
	
	Private Function dbIncDecSet( model_name , id , field_name , value, srcname , method_name )
		Dim content,bReload,inc_content
		
		if isArray( id ) then
			dbIncDecSet = URL__ & method_name & "&field=" & field_name & "&value=" & value & "&id=" & join(id,",")
		else
			dbIncDecSet = URL__ & method_name & "&field=" & field_name & "&value=" & value & "&id=" & id
		end if
		
		If Not is_empty( POP_MVC.config( "SHOW_PAGE_TRACE" ) ) Then POP_MVC.pushAuto( "生成链接 " & dbIncDecSet )
		
		'如果方法体已经写入Runtime/Class/IndexAction.Class.asp中，则不再往下执行		
		if method_exists( POP_MVC.c & "Action" , method_name ) then
			Exit Function
		end if		

		'是否重载
		bReload = not ctrl_inc_method( method_name,inc_content )
		
		'如果需要重载，则重新写入文件
		if bReload then			
			content = POP_MVC.file_get_contents( POP_MVC.mvc_dir & "Tpl/" & srcname )
			content = Replace( content, "METHOD_NAME" , method_name )
			content = Replace( content, "MODEL_NAME" , model_name )
			content = "<" & "%" & VbCrLf & inc_content & content & VbCrLf & "%" & ">"
			Call POP_MVC.file_put_contents( inc_file ,content )
			If Not is_empty( POP_MVC.config( "SHOW_PAGE_TRACE" ) ) Then POP_MVC.pushAuto( "写入方法 " & method_name & " " & POP_MVC.import("POPASP_PAGETRACE" ).readFile( inc_file , "查看文件" )  )
		else
			
		end if
	End Function
	
	'数据模型增加/修改的自动完成
	Private Property Get dbAddOrDbSave( arg , default_method , srcname )
		Dim model_name,method_name,content,bReload,inc_content
		
		if not isArray( arg ) then
			model_name = arg			
		else
			model_name = arg(0)
			if ubound( arg ) > 0 then method_name = arg(1)
		end if
		
		if method_name = "" then method_name = default_method
		
		dbAddOrDbSave = URL__ & method_name
		
		If Not is_empty( POP_MVC.config( "SHOW_PAGE_TRACE" ) ) Then POP_MVC.pushAuto( "生成链接 " & dbAddOrDbSave )
		
		'如果方法体已经写入Runtime/Class/IndexAction.Class.asp中，则不再往下执行		
		if method_exists( POP_MVC.c & "Action" , method_name ) then
			Exit Property
		end if		

		'是否重载
		bReload = not ctrl_inc_method( method_name,inc_content )		
		
		'如果需要重载，则重新写入文件
		if bReload then
			content = POP_MVC.file_get_contents( POP_MVC.mvc_dir & "Tpl/" & srcname )
			content = Replace( content, "METHOD_NAME" , method_name )
			content = Replace( content, "MODEL_NAME" , model_name )
			content = "<" & "%" & VbCrLf & inc_content & content & VbCrLf & "%" & ">"
			Call POP_MVC.file_put_contents( inc_file ,content )
			If Not is_empty( POP_MVC.config( "SHOW_PAGE_TRACE" ) ) Then POP_MVC.pushAuto( "写入方法 " & method_name & " " & POP_MVC.import("POPASP_PAGETRACE" ).readFile( inc_file , "查看文件" )  )
		end if
		
	End Property
	
	
	'用于checkAdd与checkSave中
	Private Property Get checkMethod( arg , default_method , srcname )
		Dim model_name,method_name,content,bReload,inc_content		
		
		if not isArray( arg ) then
			model_name = arg			
		else
			model_name = arg(0)
			if ubound( arg ) > 0 then method_name = arg(1)
		end if
		
		if method_name = "" then method_name = default_method
		
		checkMethod = "<script src='" & URL__ & method_name & "'></script>" & VbCrLf
		
		If Not is_empty( POP_MVC.config( "SHOW_PAGE_TRACE" ) ) Then POP_MVC.pushAuto( "生成JS代码 " & Server.HtmlEncode(checkMethod) )
		
		'如果方法体已经写入Runtime/Class/IndexAction.Class.asp中，则不再往下执行		
		if method_exists( POP_MVC.c & "Action" , method_name ) then
			Exit Property
		end if		

		'是否重载
		bReload = not ctrl_inc_method( method_name,inc_content )
		
		'如果需要重载，则重新写入文件
		if bReload then
			content = POP_MVC.file_get_contents( POP_MVC.mvc_dir & "Tpl/" & srcname )
			content = Replace( content, "METHOD_NAME" , method_name )
			content = Replace( content, "MODEL_NAME" , model_name )
			content = "<" & "%" & VbCrLf & inc_content & content & VbCrLf & "%" & ">"
			Call POP_MVC.file_put_contents( inc_file ,content )
			If Not is_empty( POP_MVC.config( "SHOW_PAGE_TRACE" ) ) Then POP_MVC.pushAuto( "写入方法 " & method_name & " " & POP_MVC.import("POPASP_PAGETRACE" ).readFile( inc_file , "查看文件" )  )
		end if
	End Property
	
	'判断类似app/Runtime/Class/IndexAction.inc.asp这样的文件中是否含有某个方法，比如popasp_js_auto
	Private Function ctrl_inc_method( ByRef method_name,ByRef inc_content )
		if not POP_MVC.file.isExists( inc_file ) then
			ctrl_inc_method = false
			inc_content = ""
			Exit Function
		end if
		
		'取得inc文件内容
		inc_content = POP_MVC.asp_get_contents(inc_file) & VbCrLf
		
		'如果不存在该方法
		if not POP_MVC.String.reg_test( inc_content , "^\s*(Public\s+)?(Sub|Function|Property)\s+\[?" & method_name & "\]?\s*" , "gim" ) then
			ctrl_inc_method = false
			Exit Function
		end if
		
		ctrl_inc_method = true
	End Function
	
	'在配置文件中配置两个样式的类名
	'一个是input：POPASP_VERIFY_INPUT
	'一个是img：POPASP_VERIFY_IMG
	Private Function verify_( method_name , srcname )
		Dim content,bReload,inc_content,url,input_name
		
		input_name = POP_MVC.config( "SESSION_VERIFY" )
		url = URL__ & method_name
		
		
		verify_ = "<input type='text' name='verify' id='code' class='" & POP_MVC.config("AUTO_VERIFY_INPUT") & "' /> <img src='" & url & "' onclick=""javascript:this.src='" & url & "&id='+Math.random()"" style='cursor:pointer' alt='验证码' title='看不清，点击更换一张' class='" & POP_MVC.config("AUTO_VERIFY_IMG") & "' />"
		
		If Not is_empty( POP_MVC.config( "SHOW_PAGE_TRACE" ) ) Then POP_MVC.pushAuto( "生成链接 " & POP_MVC.String.encodeHtml( verify_ ) )
		
		'如果方法体已经写入Runtime/Class/IndexAction.Class.asp中，则不再往下执行		
		if method_exists( POP_MVC.c & "Action" , method_name ) then
			Exit Function
		end if		

		'是否重载
		bReload = not ctrl_inc_method( method_name,inc_content )
		
		'如果需要重载，则重新写入文件
		if bReload then
			content = POP_MVC.file_get_contents( POP_MVC.mvc_dir & "Tpl/" & srcname )
			content = Replace( content, "METHOD_NAME" , method_name )
			content = "<" & "%" & VbCrLf & inc_content & content & VbCrLf & "%" & ">"
			Call POP_MVC.file_put_contents( inc_file ,content )
			If Not is_empty( POP_MVC.config( "SHOW_PAGE_TRACE" ) ) Then POP_MVC.pushAuto( "写入方法 " & method_name & " " & POP_MVC.import("POPASP_PAGETRACE" ).readFile( inc_file , "查看文件" )  )
		end if		
	End Function	
	
	'input表单
	'完成表单html的生成
	'array(  input_name, 表单值, input类型  )
	Public Property Get htmlInput( arg )
		Dim input_name,input_type,form_value
		form_value = ""
		if isArray( arg ) then		
			input_name = arg( 0 )
			if Ubound( arg ) > 0 then				
				if isObject( arg(1) ) then
					form_value = arg(1)(input_name)
				else
					form_value = arg(1)
				end if			
			end if
			if Ubound( arg ) > 1 then input_type = LCase(arg(2))
		else
			input_name = arg
			input_type = "text"
		end if
		
		if isNull( form_value ) then
			form_value = ""
		end if
		
		htmlInput = "<input class='popasp_form_input' type='" & input_type & "' name='" & input_name & "' value=""" & form_value & """>"
	End Property
	
	Public Property Get getRegLicenseContent
		getRegLicenseContent = POP_MVC.file_get_contents( POP_MVC.mvc_dir & "Tpl/register_license.html" )		
	End Property
	
	'input表单
	'完成表单html的生成
	'array(  input_name, 表单值, input类型  )
	Public Function htmlSelect( input_name,select_arr,form_value )
		dim i,html,value

		if isObject( form_value ) then
			value = form_value(input_name)
		else
			value = form_value
		end if
		
		
		html = "<select class='popasp_form_select' name='" & input_name &  "'>" & VbCrLf
		
		for i = 0 to ubound( select_arr )
			html = html & "<option" & ifTrue( select_arr(i) = value , " selected='selected'" ) & ">" & select_arr(i) & "</option>" & VbCrLf
		next
		
		html = html & "</select>"
		htmlSelect = html
	End Function
	
	Public Function getPrismFile( filePath, language )
		dim content		
		content = POP_MVC.file_get_contents( POP_MVC.mvc_dir & "tpl/readfile.html" )	
		content = replace( content, "{LANGUAGE}" , language )	
		content = replace( content, "{CSS}" , POP_MVC.file_get_contents(POP_MVC.mvc_dir & "tpl/prism.css") )
		content = replace( content, "{SCRIPT}" , POP_MVC.file_get_contents(POP_MVC.mvc_dir & "tpl/prism.js") )
		content = replace( content, "{CONTENT}" , POP_MVC.String.encodeHtml(POP_MVC.file_get_contents(filePath)) )	
		getPrismFile = content
	End Function

End Class
%>