<%
' +----------------------------------------------------------------------
' | POPASP [ ASP MVC ]
' +----------------------------------------------------------------------
' | Copyright (c) 2016 http://popasp.com All rights reserved.
' +----------------------------------------------------------------------
' | Author: popasp <1737025626@qq.com>
' +----------------------------------------------------------------------
Class POPASP_AUTOCOMPLETE
	'自动完成

    ' 操作状态
    Private MODEL_INSERT	'  插入模型数据
    Private MODEL_UPDATE	'  更新模型数据
    Private MODEL_BOTH		'  包含上面两种方式	
	Public modelType
	
	Sub handle( ByRef [auto], ByRef data, ByRef mode )
		on error resume next
		dim row,bound,arr,model,rule,replace_str,i,bnd,str,key,key2,keys,pos

		If not isEmpty( [auto] ) and isArray( [auto] ) Then
			keys = data.keys
			for each row in [auto]
				key2 = null
				key = row(0)
				pos = POP_MVC.Arr.iSearch( keys, key )
				if pos >= 0 then 
					key2 = keys(pos)
				else
					key2 = key
				end if
				' 填充因子定义格式
                ' array('field','填充内容','填充条件','附加规则',[额外参数])
				bound = ubound(row)
				if bound < 2 Then	'默认为新增的时候自动填充
					bound = bound+1
					ReDim Preserve row(bound)
					row(bound) = 1
				End If
				if mode = abs(row(2)) OR MODEL_BOTH = abs(row(2)) Then
					if bound<3 Then
						bound = bound+1
						ReDim Preserve row(bound)
						row(bound) = "string"
					End If
					rule = trim( row(3) )
					select case rule						
						case "function","callback"	'使用函数进行填充 字段的值作为参数
							if not (row(2) < 0 and not isNul( data(key2) )) then
								if rule = "callback" then
									arr = split(row(1),".")
									model = arr(0)
									if POP_MVC.String.iEndsWith( model , "model" ) then
										model = left( model , len(model) - 5 )
									end if
									
									if modelType = "xml" then
										set model = XM_( model )
									else
										set model = POP_MVC.Model( model )
									end if
									replace_str = "model." & arr(1)									
								else
									replace_str = row(1)
								end if
								
								if bound = 4 Then	'如果有分配的参数								
									if not isArray( row(4) ) Then	'如果不是数组，转化成数组再处理
										row(4) = array( row(4) )
									end If

									if isArray( row(4) ) Then
										if data.Exists( key2 ) Then	'如果data中存在row(0)这个键
											str = ""
											for i = 0 to ubound( row(4) )
												str = str & ",row(4)(" & i & "),"
											next														
											str = "data(""" & key2 & """)" & "=" &  replace_str & "( data(""" & key2 & """) "  & str
											str = POP_MVC.rtrim(str, ",")
											str = str & " )"
											Execute str
										else	'如果data中不存在row(0)这个键
											str = ""
											for i = 0 to ubound( row(4) )
												str = str & ",row(4)(" & i & "),"
											next
											if str <> "" then str = mid( str,2 )
											Execute "data.add """ & key2 & """" & "," &  replace_str & "( "  & str  & " )"
										End If
									End If
								Else	'如果没有分配参数，则当不使用参数	
									Execute "data(""" & key2 & """)" & "=" &  replace_str
								End If
								call L_("")		
							end if
						case "field"	'用其它字段的值进行填充		
							if IsNul( data(key2) ) and not isNull(key2)  then
								data( key2 ) = data( row(1) )
							end if
						case "ignore"	' 为空忽略	
							if data( key2 ) = "" and not isNull(key2) Then								
								data.remove( key2 )
							End If
						case "string"	'[额外参数]存在，则当内容为空时赋值，否则不存在时赋值
							if bound > 3 then
								if data(key2) = "" then
									data( key2 ) = row(1)
								end if
							else
								if IsNul( data(key2) ) and not isNull(key2) then
									data( key2 ) = row(1)
								end if
							end if
						case else
							if IsNul( data(key2) ) and not isNull(key2) then
								data( key2 ) = row(1)
							end if
					End Select
				
				end If
			next
		end if

	End Sub
	
	Private Sub Class_Initialize
		MODEL_INSERT =1	'  插入模型数据
		MODEL_UPDATE =2	'  更新模型数据
		MODEL_BOTH	 =3	'  包含上面两种方式		
	End Sub
End Class
%>