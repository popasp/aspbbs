<%
' +----------------------------------------------------------------------
' | POPASP [ ASP MVC ]
' +----------------------------------------------------------------------
' | Copyright (c) 2016 http://popasp.com All rights reserved.
' +----------------------------------------------------------------------
' | Author: popasp <1737025626@qq.com>
' +----------------------------------------------------------------------
'验证类
Class POPASP_AUTOVALIDATE
	'自动验证

	public [error]		'最近错误消息
	
	Public tableName	'表名
	Public modelType
	
	Private dReg		'正则匹配的Dictionary对象
	
    ' 操作状态
	Private VALUE_VALIDATE	'2 表单值不为空则验证
	Private MUST_VALIDATE	'1 必须验证
	Private EXISTS_VALIDATE	'0 表单存在字段则验证
	Public patchValidate	'是否批处理验证
	Private theMode

	' 自动验证，在使用db.Create(1/2/3)时会自动调用，验证条件要写到app/Model/modelname.asp文件中
	' 参数validate是验证条件的二维数组，详细写法需要查看帮助文件中的 模型/自动验证
	' data为要验证的数据，该数据为添加或修改数据库的数据，类型可以是一维Dictionary，也可以是request.form
	' mode分别为1(插入时验证),2(修改时验证),3(默认，前两种情况都验证)
	Public Function handle ( ByRef validate, ByVal data,ByRef mode )
		dim bValidate:bValidate = false
		
		if typename( data ) = "IRequestDictionary" then
			set data = POP_MVC.form2dict
		end if
		
		'属性验证
		if not isEmpty(validate) and isArray(validate) Then	' 如果设置了数据自动验证则进行数据验证
			if patchValidate Then [error] = array()
			for each row in validate
                ' 验证因子定义格式
                ' array(field,rule,message,condition,type,when,params)
                ' 判断是否需要执行验证
				bValidate = false
				bound = ubound( row )
				if bound < 5 Then
					bValidate = true
				elseif 3 = row(5) OR mode = row(5) Then
					bValidate = true
				end If
				theMode = mode
				if bValidate Then
					if bound<2 Then		'前三项是必须的
						Call POP_MVC.Exit( "自动验证的每条数组不能少于三项 , " & "POPASP_AUTOVALIDATE:47" )
					End if
					
					if bound < 3 Then
						Redim Preserve row(3)
						row(3) = EXISTS_VALIDATE	'默认为存在就验证
					End If
					
					if bound < 4 Then
						Redim Preserve row(4)
						row(4) = "regex"	'默认采用正则验证
					End If

					'判断验证条件
					select case row(3)
						case MUST_VALIDATE	' 1 必须验证
							if POP_MVC.Arr.iExists( Array("unique","exists","_unique","_exists","fieldexists","fieldunique") , row(4) ) then
								if False = validationField(data,row) Then
									if Not patchValidate then
										handle = false
										Exit Function
									end if
								end if
							elseif not data.Exists( row(0) ) then
								if patchValidate Then
									Redim Preserve [error]( ubound([error])+1 )
									[error]( ubound([error]) ) = row(2)
								Else				
									[error] = row(2)
									handle = false
									exit Function
								End If
							else
								if False = validationField(data,row) Then
									if Not patchValidate then
										handle = false
										Exit Function
									end if
								end if
							end if
						case VALUE_VALIDATE    '2  值不为空的时候才验证	
							if data.Exists( row(0) ) then
								if "" <> trim( data(row(0)) ) Then								
									if false = validationField(data,row) Then
										if Not patchValidate then
											handle = false
											Exit Function
										end if
									end if
								End If
							end if
						Case else	' 0 默认表单存在该字段就验证
							if data.Exists( row(0) ) Then								
								if False = validationField(data,row) Then
									if Not patchValidate then
										handle = false
										Exit Function
									end if
								end if							
							end if				
					end select
				end if
			next
		
			if isArray( [error] ) Then
				if ubound([error]) >=0 Then
					handle = false
					exit function
				end If				
			end If
		end If

		handle = true
	End Function
	
	' 验证表单字段 仅支持批量验证
	Private Function validationField(byref data,row)
		dim bound
		if not validationFieldItem(data,row) Then
			if patchValidate Then
				bound = ubound([error])+1				
				Redim Preserve [error]( bound )
				[error](bound) = row(2)
			Else				
				[error] = row(2)
				validationField = false
				exit Function
			End If
		End If
		validationField = true
	End Function
	
	' 根据验证因子验证字段
	Private Function validationFieldItem(byref data,row)		
		On Error Resume Next
		dim bound : bound = ubound(row)
		dim str,arr,model,replace_str,temp,i,dict
		row(4) = LCase( row(4) )

		if row(4) = "function" OR row(4) = "callback" Then
			if bound = 6 Then
				if not isArray( row(6) ) Then row(6) = array( row(6) )
				
				if ubound(row(6))>-1 then
					arr = Array()
					for i = 0 to ubound( row(6) )
						POP_MVC.Arr.push arr , "row(6)(" & i & ")"
					next
					
					POP_MVC.Arr.unshift arr,"data( row(0) )"
					
					str =  "validationFieldItem = " &  "REPLACE_STR" & "( " & join( arr , "," )  & " )"
				else
					str =  "validationFieldItem = " & "REPLACE_STR" & "(  data(row(0))  )"	
				end if
				
			else 
				str = "validationFieldItem = " & "REPLACE_STR" & "(  data(row(0))  )"		
			end if
		End If
		
		select case row(4)
			case "callback"				
				arr = split(row(1),".")
				if ubound( arr ) > 0 then
					model = arr(0)
					if POP_MVC.String.iEndsWith( model , "model" ) then
						model = left( model , len(model) - 5 )
					end if
					

					if modelType = "xml" then
						set model = XM_( model )
					else
						set model = POP_MVC.Model( model )
					end if
					
					
					replace_str = "model." & arr(1)
				elseif tableName <> "" then
					set model = POP_MVC.Model( tableName )
					replace_str = "model." & arr(0)
				else
					POP_MVC.warning(  typename(Me) & ".validationFieldItem 使用callback回调函数时应用XXXModel.Method这种格式" )
				end if
			case "function"	'使用函数进行验证
				replace_str = row(1)
			case "confirm"	'验证两个字段是否相同
				validationFieldItem = (data( row(0) ) = data( row(1) ))
			case "notconfirm","_confirm"	'验证两个字段是否不同
				row(4) = "confirm"
				validationFieldItem = (not validationFieldItem(data,row))
			case "unique","_exists"	'判断在表row(1)字段row(0)是否没有其它值了
				validationFieldItem = unique( data,row(0), row(1) )
			case "exists","_unique"	'判断在表row(1)字段row(0)是否还有其它值
				validationFieldItem = exists( data,row(0), row(1) )
			case "fieldunique"
				set dict = M_(row(1)).db.getTableStructure(M_(row(1)).db.tableName)
				dict = dict.Keys
				validationFieldItem = NOT POP_MVC.Arr.iExists( dict, LCase(row(0)) )
			case "fieldexists"
				validationFieldItem = (not validationFieldItem(data,row))
			case else
				validationFieldItem = check( data( row(0) ) , row(1) , row(4)  )		
		End select
		if row(4) = "function" OR row(4) = "callback" Then 			
			str = Replace( str , "REPLACE_STR" , replace_str )
			Execute( str )
			if row(4) = "function" and err.number = 13 then	'错误：错误类型不匹配，此时函数不存在
				validationFieldItem = true
			end if
			if row(4) = "callback" and err.number = 438 then	'错误：对象不支持此属性或方法，此时函数不存在
				validationFieldItem = true
			end if
		End if	

		call L_( "POPASP_AUTOVALIDATE.validationFieldItem" )
	End Function
	
	' 判断一维Dictonary对象中的某个键值对是否在表table_name的字段field中不存在对应值
	' data为要验证的数据，该数据为添加或修改数据库的数据，类型是一维Dictionary
	' table_name为要查询的表名
	' 比如 Data 为 { "username":"popasp" , "email": "popasp@163.com" , "qq":1737025626 }
	' field为email时,则查询表table_name的字段"email"是否有"popasp@163.com"值,有则返回false,无则返回true
	' 其本质查询的是cnt = B_(table_name).where( "email = 'popasp@163.com'" ).count，判断 cnt > 0
	' field也可以是多字段
	' 比如"email:qq"，则查询 cnt = B_(table_name).where( "email = 'popasp@163.com' and qq=1737025626" ).count
	' 在修改数据时，data一般含有对应的id，查询条件会自动添加 prikey <> id ，其中的prikey为主键
	' 比如 where( " id <> 1 and email = 'popasp@163.com'" )
	Public Function unique( data,field,table_name )		
		on error resume next
		dim item,temp2,map,model,rs,pk
		
		if TypeName( field ) = "String" Then
			'用英文逗号将字段分隔，此时验证条件必须为1
			if Instr( field , "," ) > 0 Then '可以用逗号分隔多个字段
				field = split( field , "," )
			End If
			set map = Server.CreateObject("Scripting.Dictionary")			

			if IsArray( field ) Then
				for each item in field
					if InStr( item,":" )>0 then
						temp2 = split( item,":" )
						map.add temp2(0),data( temp2(0) )
					else
						map.add item,data(item)
					end if							
				next
			else 

				if InStr( field,":" )>0 then
					temp2 = split( field,":" )					
					map.add temp2(0),data( temp2(0) )
					map( temp2(1) ) = null
				else
					map.add field,data( field )
				end if
			End If
				
			if modelType = "xml" then
				if table_name = "" then
					POP_MVC.error("XML作数据库时，unique验证的表名，即数组第二项不能为空。")
				end if
				set model = X_( table_name )
			else				
				if trim( table_name ) <> "" Then
					set model = B_( table_name )
				elseif not isEmpty(tableName) Then	
					set model = B_( tableName )
				End if
			end if

			if not isEmpty(model) Then
				'完善编辑的时候验证唯一
				pk = model.getPK()			
						
				if data.Exists( pk ) Then
					if not isEmpty(pk) then
						if POP_MVC.String.reg_test( data( pk ), "^[1-9]\d*$","i" ) then
							map.add pk,array( "neq" , data( pk ) )
						end if
					elseif not isEmpty( session(pk) ) and theMode = 2 and LCase(pk) <> "id" then
						if POP_MVC.String.reg_test( session(pk), "^[1-9]\d*$","i" ) then
							map.add pk,array( "neq" , session(pk) )
						end if
					end if
				End If

				if model.where(map).count > 0 Then
					unique = false
				Else 
					unique = true
				End If
	
				POP_MVC.unset(rs)
			else
				POP_MVC.error( typename(Me) & ".unique操作不存在的数据模型" )
			End if			
			set map = nothing
		End If	
	End Function
	
	' 判断一维Dictonary对象中的某个键值对是否在表table_name的字段field中存在对应值
	' 其使用正好与unique相反
	Public Function exists( data,field,table_name )
		exists = Not unique( data,field,table_name )
	End Function
	
	' 验证码的验证，不区分大小写
	Public Property get verify( value )
		verify = ( UCase(value) = UCase(session(POP_MVC.config( "SESSION_VERIFY" ))) )
	End Property
	
	' 判断数组arr中是否存在val值
	Private Function in_array( val,arr )
		dim item
		for each item in arr 			
			if item = val Then
				in_array = true
				exit function
			end if
		next
		in_array = false
	end Function
	
	'如果date1>date2返回正，否则返回负数
	Private Function dateCompare( date1,date2 )
		dateCompare = DateDiff("s",date2,date1)
	End Function
	
	' 验证数据
	' 判断value值，是否满足某个条件condition，rule为补充条件
	' 支持 in notin between notbetween equal(eq) iequal(ieq) notequal(neq) length date regex expire ip_allow ip_deny verify notempty ip gt egt lt elt notempty
	' 如 P_("AUTOVALIDATE").check( "popasp" , "6,30" , "length" )
	' 在model中，如 Array( 字段名, "6,30", "length" , 提示信息,"",验证时间1或2或3 )
	Function Check(value,rule,condition )		
		dim min,max,length
		condition = LCase( trim(condition) )

		select case condition
			case "in"	'此时的rule为range				
				if not isArray( rule ) Then
					rule = split( rule,"," )
				End if
				Check = in_array( value,rule )				
			case "notin","not in","_in"
				Check = ( Not Check(value,rule,"in" ) )
			case "between"	'验证数值是否在某个范围
				if not isArray( rule ) Then
					rule = split(rule,",")
				end if
				min = CSng( trim(rule(0)) )
				max = CSng( trim(rule(1)) )
				value = CSng( value )
				
				Check = ( value >= min AND value <= max )
			case "notbetween","not between","_between"
				Check = ( not Check(value,rule,"between") )
			case "eq","equal"
				Check = ( value = rule )
			case "gt"	'大于
				Check = ( value > rule )
			case "egt"	'大于或等于
				Check = ( value >= rule )
			case "lt"	'小于
				Check = ( value < rule )
			case "elt"	'小于或等于
				Check = ( value <= rule )
			case "ieq","iequal"
				Check = Check(LCase(value),LCase(rule),"eq")
			case "neq","notequal","_eq"
				Check = ( Not Check(value,rule,"eq" ) )
			case "_ieq"
				Check = ( Not Check(value,rule,"ieq" ) )
			case "length","ke_length","ue_length"
				length = len(value)
				if not isArray( rule ) Then
					rule = split(rule,",")
				end if			
				if ubound( rule ) > 0 Then	'长度区间
					Check = ( length >= CLng(trim(rule(0))) AND length <= CLng(trim(rule(1))) )
				else 		'指定长度
					Check = ( length = rule(0) )
				End if
			case "date"
				check = isDate( value )
			case "expire"
				if not isArray( rule ) Then
					rule = split(rule,",")
				end if
				min = CDate(rule(0))
				max = CDate(rule(1))
				if Not isDate( value ) Then
					value = CDate(value)
				end if
				value = CDate(value)
				Check = (dateCompare(value,min) >=0 AND dateCompare(value,max) <=0)
			case "ip_allow"
				Check = in_array( get_client_ip() , split(rule,",") )
			case "ip_deny"
				Check = ( Not Check("",rule,"ip_allow" ) )
			case "notempty"				
				Check = (trim(value) <> "")
			case POP_MVC.config( "SESSION_VERIFY" )
				if rule<> "" then
					Check = ( UCase(value) = UCase(session(rule)) )
				else
					Check = Me.verify( value )
				end if
			case "regex"
				Check = regex(value,rule)
			case "idcard"
				
			case else				
				Check = regex(value,rule)
		end Select	
	End Function
	

	' 使用正则验证数据
	' 已经写好的正则验证规则有 require email url currency number phone mobile zip qq ip integer   plusint minusint plusdecimal minusdecimal double english chinese db_id
	' 如果上述没有，需要自己写正则表达式
	Public Function regex( value,rule )		
		dim  pattern
		if dReg.Exists( LCase(rule) ) Then
			pattern = dReg(LCase(rule))			
		else 
			pattern = rule
		End If
		regex = POP_MVC.String.reg_test(value,pattern,"")
	End Function
	
	' 为了方便在js中使用相同规则与条件验证，js_output将验证条件生成json。
	' 该方法与js的popasp_check()时配合使用，且js_output会被自动调用。
	' 参数validate是验证条件的二维数组，详细写法需要查看帮助文件中的 模型/自动验证
	' mode分别为1(插入时验证),2(修改时验证),3(默认，前两种情况都验证)
	Function js_output( byref validate , byref mode )
		dim i,row,filter,arr,filter_row4
		filter_row4 = Array( "function","callback","unique","exists","ip_allow","ip_deny" )
		for i = 0 to ubound( validate )
			row = validate(i)
			filter = false
			if ubound( row ) > 4 then
				if mode <> 3 and mode<>row(5) then
					filter = true
				end if
			end if
			
			if not filter then
				if ubound( row ) > 3 then
					if  POP_MVC.Arr.Exists( filter_row3 , row(4) ) then
						filter = true
					end if
				end if
			end if
			
			if not filter then
				POP_MVC.Arr.push arr,row
			end if
		next
		js_output = arr
	end Function
	
	Private Sub Class_Terminate
		set dReg = nothing
	End Sub
	
	Private Sub Class_Initialize
		EXISTS_VALIDATE = 0
		MUST_VALIDATE = 1
		VALUE_VALIDATE = 2
		patchValidate = false	
		
		set dReg = Server.CreateObject("Scripting.Dictionary")
		dReg.add "require", ".+"
		dReg.add "email", "^\w+([-+.]\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*$"
		dReg.add "url","^http(s?):\/\/(?:[A-za-z0-9-]+\.)+[A-za-z]{2,4}(?:[\/\?#][\/=\?%\-&~`@[\]\':+!\.#\w]*)?$"
		dReg.add "currency","^\d+(\.\d+)?$"		
		dReg.add "number","^\d+$"
		dReg.add "integer","^[-\+]?\d+$"
		
		dReg.add "phone","^((\(\d{2,3}\))|(\d{3}\-))?(\(0\d{2,3}\)|0\d{2,3}-)?[1-9]\d{6,7}(\-\d{1,4})?$"
		dReg.add "mobile","^((\(\d{3}\))|(\d{3}\-))?1[345789]\d{9}$"
		
		dReg.add "zip","^\d{6}$"
		dReg.add "qq","[1-9][0-9]{4,}"
		dReg.add "ip","^((2[0-4]\d|25[0-5]|[01]?\d\d?)\.){3}(2[0-4]\d|25[0-5]|[01]?\d\d?)$"			
		
		dReg.add "plusint","^[+]?\d+$"
		dReg.add "minusint","^[-]\d+$"		
		dReg.add "plusdecimal","^[+]?\d+(\.\d+)?$"
		dReg.add "minusdecimal","^[-]\d+(\.\d+)?$"
		dReg.add "double","^[-\+]?\d+(\.\d+)?$"
		
		dReg.add "english","^[A-Za-z]+$"
		dReg.add "chinese","^[\u0391-\uFFE5]+$"	
		dReg.add "db_id","^[1-9]\d*$"	
		
	End Sub
End Class
%>