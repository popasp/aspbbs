<%
' +----------------------------------------------------------------------
' | POPASP [ ASP MVC ]
' +----------------------------------------------------------------------
' | Copyright (c) 2016 http://popasp.com All rights reserved.
' +----------------------------------------------------------------------
' | Author: popasp <1737025626@qq.com>
' +----------------------------------------------------------------------
Class POPASP_CACHE
	public homePath	'前台路径，必须首先指定
	Private isPageCache
	
	Public Function dir( arg )
		homePath = arg
		set dir = Me
	End Function
	
	'页面缓存网址
	'url本站的一个网址，比如：http://127.0.0.1:1234/50/index.asp
	'filename一般取入口文件名，比如index
	'id唯一标识符，可取空字符串
	Public Property Get cache( ByVal url , ByVal filename , ByVal id )
		dim content,sep,pos,path,dstname
		path = POP_MVC.rtrim( POP_MVC.rtrim( homePath , "/" ) , "\" )
		
		dstname = filename
		if id <> "" then
			dstname = dstname & "/" & POP_MVC.trim(POP_MVC.trim(id , "/" ) , "\")
		end if
		path = path & "/Runtime/Page/" & dstname & ".html"

		content = POP_MVC.import("POPASP_HTTP").get( url )
		sep = "<!--QQ_1737025626_POPASP_PAGE_TRACE_CACHE_QQ_GROUP_124648143-->"
		if len(content) > 0 then
			pos = instr( content,sep )
			if pos > 0 then
				content = left( content,pos-1 )
			end if
			content = POP_MVC.String.reg_replace(content , "" , "^\s+|\s+$" , "g" )
			Call POP_MVC.file_put_contents( path  , content )
		end if
	End Property	
	
	'获取页面缓存文件的路径
	'如果C_("PAGE_CACHE_FOLDER") = ""，则默认存储到Runtime/Page/文件夹下
	Public Property Get getPageCachePath( byval id )
		dim path,dstname

		'生成文件名
		dstname = POP_MVC.File.baseName( array( Request.ServerVariables( "SCRIPT_NAME" ) , true ) )
		if id <> "" then
			id = POP_MVC.String.reg_replace( id , "" , "[:|/\\*<>?""]" , "g" )
			dstname = dstname & "/" & POP_MVC.trim(POP_MVC.trim(id , "/" ) , "\")
		end if
		
		'得到存放路径
		'./home/Runtime/Page/index.html 或 ./home/Runtime/Page/index/aaa.html
		
		if POP_MVC.config("PAGE_CACHE_FOLDER") = "" Then		
			path = POP_MVC.appPath & "/Runtime/Page/" & dstname & ".html"
		else
			path = POP_MVC.rtrim( POP_MVC.rtrim( POP_MVC.config("PAGE_CACHE_FOLDER") , "/" ) , "\" ) & "\" & dstname & ".html"
		end if
		
		getPageCachePath = path
	End Property
End Class
%>