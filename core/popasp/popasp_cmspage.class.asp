<%
' +----------------------------------------------------------------------
' | POPASP [ ASP MVC ]
' +----------------------------------------------------------------------
' | Copyright (c) 2016 http://popasp.com All rights reserved.
' +----------------------------------------------------------------------
' | Author: popasp <1737025626@qq.com>
' +----------------------------------------------------------------------
Class POPASP_CMSPAGE	
	public total	'记录总个数	
	public totalPage	'总共多少页
	public page	'当前页
	public perpage	'每页显示多少条
	private showType,ID,keys
	private cmsClassName
	public leftPage
	public rightPage
	public seperator		'分隔符
	public ContentObj,SortObj
	
	'下面是可配置的	
	private matches
	private jump	'页面跳转，如果不需要页面跳转，可以 Call oPage.setConfig( "jump" , "" )
	private pagesize	'每页显示多少条
	private curpageLabel	'当前的包裹标签，默认为<em></em>
	Private linkType,linkPrefix,urlSuffix
	Private uri
	
	
	private aClass,currentClass	

	Private Sub Class_Initialize()
		seperator = ""
	
		if setting.IsDefault = 1 AND  then
			linkPrefix = "?"
		else
			linkPrefix = POP_MVC.config("INDEX_LINK_PREFIX")
		end if

		cmsClassName = C_("CMS_CLASS_NAME")	

		urlSuffix = P_(cmsClassName).urlSuffix
		currentClass = "page-num page-num-current"
 	End Sub
	
	Private Sub Class_Terminate
		set matches = nothing
	End Sub
	
	'初始化
	Public sub init( ByVal dTotal, ByVal dPerpage, ByVal dPage, ByVal dID, ByVal sLinkType, ByVal sShowType, ByVal sKeys )
		on error resume next
		total = dTotal
		perpage = Cint(dPerpage)
		Page = dPage
		ID = dID
		keys = sKeys
		showType = sShowType
		linkType = sLinkType
		
		if not isNumeric( page ) then
			page = 1
		end if
		
		if page < 1 then
			page = 1
		end if	
		
		page = Clng(page)
	
		totalPage = CLng(Abs(Int(- total / perpage)))

		
		if page > totalPage then
			page = totalPage
		end if			
	end sub
	
	function getUri
		if isEmpty(uri) then
			uri = POP_MVC.String.reg_replace( SELF__ ,"","&" & POP_MVC.config("VAR_PAGE") & "=[^&]+","ig")
			uri = POP_MVC.String.reg_replace(uri,"","\b" & POP_MVC.config("VAR_PAGE") & "=[^&]+","ig")
		end if
		getUri = uri
	end function
	
	'某一页链接
	Public Property Get PageLink( ByRef dPage )
		on error resume next
		dim ret,bool,sortObj
		if cmsClassName = "" then
			cmsClassName = C_("CMS_CLASS_NAME")
		end if
		
		if totalPage > 1 then
			'自定义模板
			if P_(cmsClassName).selfTpl <> "" then
				ret = POP_MVC.config("sitePath") & "/" & replace(replace(P_(cmsClassName).selfTpl , "#" , "/" ) , "." ,"_" & dPage & "." ) 
			elseif LCase(POP_MVC.get("a")) = "tagcontentlist" then
				ret = P_(cmsClassName).getTagContentListUrl( 0 , POP_MVC.get("TagID") , dPage )
			'适用于 DateContentList AuthorContentList UserContentList
			elseif POP_MVC.String.iEndsWith(POP_MVC.get("a") , "contentlist") then
				ret = linkPrefix & POP_MVC.get("a") & "_" & POP_MVC.get("SortType") & "_" & POP_MVC.get("ID") & "_" & dPage
			elseif LCase(POP_MVC.get("a")) = "looplist" then
				ret = linkPrefix & POP_MVC.get("a") & "_" & POP_MVC.get("template") & "_" & dPage
			'内容页
			elseif linkType="content" then
				
				bool = false
				if isObject( ContentObj ) then
					if ContentObj("ContentID") - ID = 0 then
						bool = true
					end if
				end if

				if bool then
					set sortObj = P_(cmsClassName).getSortObj( ContentObj("SortID") )
					if P_(cmsClassName).ClassType = "POPASP_IASPCMS" then
						ret = P_(cmsClassName).getContentLink(sortObj("SortType"),ContentObj("SortID"),ContentObj("ContentID"),ContentObj("GroupID"),sortObj("ContentFolder"),sortObj("ContentFileName"),ContentObj("AddTime"),ContentObj("PageFileName"),sortObj("GroupID"),dPage)
					elseif P_(cmsClassName).ClassType = "POPASP_PAPCMS" then
						ret = P_(cmsClassName).getContentLink4cms(sortObj("SortType"),ContentObj("SortID"),ContentObj("ContentID"),"",sortObj("ContentFolder"),"",ContentObj("AddTime"),"",sortObj("GroupID"),dPage)
					end if
				else
					ret = P_(cmsClassName).getContentLinkById( ID,dPage )
				end if	
			'列表页
			elseif linkType="list" or linkType="newslist" or linkType="productlist" or linkType="downlist" or linkType="piclist" or linkType="videolist" or linkType= "channellist" then				
				ret = P_(cmsClassName).getSortUrlById( ID,dPage )
			elseif linkType = "topicindexlist" then
				ret = P_(cmsClassName).getTopicIndexLink( "",dPage )
			elseif linkType = "topicpagelist" then
				ret = P_(cmsClassName).getTopicListLink( "", ID, "" ,dPage )
			elseif linkType = "gbooklist" then
				ret = linkPrefix & "gbook_" & dPage
			elseif linkType = "userbuylist" then
				ret = linkPrefix & "userbuylist_" & ID & "_" & dPage
			elseif linkType="searchlist" then
				ret = linkPrefix & "a=search&page=" & dPage & "&searchtype=" & ID & "&keys=" & keys
			elseif linkType="msearchlist" then	
				ret = getUri() & "&" & POP_MVC.config("VAR_PAGE") & "=" & dPage
			elseif linkType = "comment" then
				ret = "javascript:pager(" & ID & "," & dPage & ")"
			elseif linkType = "faq" then
				ret = "javascript:pager(" & dPage & ")"
			elseif showType="tags" then
				ret = linkPrefix & "tag_" & ID & "_" & dPage
			elseif showType="taglist" then
				ret = linkPrefix & "taglist_" & ID & "_" & dPage
			elseif linkType=P_(cmsClassName).CommonTable then
				ret = linkPrefix & showType & "_" & dPage
			else
				
				if config.runMode=0 then
					if ID <> 0 then
						ret = "?" & showType & "_" & ID & "_" & dPage & urlSuffix
					else
						ret = "?" & showType & "_"  & dPage & urlSuffix
					end if					
				elseif inStr( showType , "{page}" ) > 0 then
					ret = replace(showType, "{page}", dPage )&config.FileExt
				else
					ret = showType & "_" & dPage &config.FileExt
				end if	
			end if
		end if
		PageLink = ret
	End Property
	
	'第一页链接
	Public Property Get FirstLink
		dim ret,bool,sortObj
		if page = 1 then
			exit Property
		end if
		if totalPage > 1 then
			if P_(cmsClassName).selfTpl <> "" then
				FirstLink = POP_MVC.config("sitePath") & "/" & replace(P_(cmsClassName).selfTpl , "#" , "/" )
			elseif LCase(POP_MVC.get("a")) = "tagcontentlist" then
				ret = P_(cmsClassName).getTagContentListUrl( 0 , POP_MVC.get("TagID") , 1 )
			elseif POP_MVC.String.iEndsWith(POP_MVC.get("a") , "contentlist") then
				ret = linkPrefix & POP_MVC.get("a") & "_" & POP_MVC.get("SortType") & "_" & POP_MVC.get("ID") 
			elseif LCase(POP_MVC.get("a")) = "looplist" then
				ret = linkPrefix & POP_MVC.get("a") & "_" & POP_MVC.get("template") 
			elseif linkType="content" then
				bool = false
				if isObject( ContentObj ) then
					if ContentObj("ContentID") - ID = 0 then
						bool = true
					end if
				end if
				if bool then
					set sortObj = P_(cmsClassName).getSortObj( ContentObj("SortID") )
					if P_(cmsClassName).ClassType = "POPASP_IASPCMS" then
						ret = P_(cmsClassName).getContentLink(sortObj("SortType"),ContentObj("SortID"),ContentObj("ContentID"),ContentObj("GroupID"),sortObj("ContentFolder"),sortObj("ContentFileName"),ContentObj("AddTime"),ContentObj("PageFileName"),sortObj("GroupID"),1)
					elseif P_(cmsClassName).ClassType = "POPASP_PAPCMS" then
						ret = P_(cmsClassName).getContentLink4cms(sortObj("SortType"),ContentObj("SortID"),ContentObj("ContentID"),"",sortObj("ContentFolder"),"",ContentObj("AddTime"),"",sortObj("GroupID"),1)
					end if
				else
					ret = P_(cmsClassName).getContentLinkById( ID,1 )
				end if
			elseif linkType="list" or linkType="newslist" or linkType="productlist" or linkType="downlist" or linkType="piclist" or linkType="videolist" or linkType= "channellist" then
				ret = P_(cmsClassName).getSortUrlById( ID,1 )
			elseif linkType = "topicindexlist" then
				ret = P_(cmsClassName).getTopicIndexLink( "",1 )
			elseif linkType = "topicpagelist" then
				ret = P_(cmsClassName).getTopicListLink( "", ID, "" ,1 )
			elseif linkType = "gbooklist" then
				ret = linkPrefix & "gbook"
			elseif linkType = "userbuylist" then
				ret = linkPrefix & "userbuylist_" & ID
			elseif linkType="searchlist" then
				ret = linkPrefix & "a=search&searchtype=" & searchtype & "&keys=" & ID
			elseif linkType="msearchlist" then
				ret = getUri
			elseif linkType = "comment" then
				ret = "javascript:pager(" & ID & "," & 1 & ")"
			elseif linkType = "faq" then
				ret = "javascript:pager(1)"
			elseif showType="tags" then
				ret = linkPrefix & "tag_" & ID
			elseif showType="taglist" then
				ret = linkPrefix & "taglist_" & ID
			elseif linkType=P_(cmsClassName).CommonTable then
				ret = linkPrefix & showType
			else
				if config.runMode=0 then
					ret = "?" & showType & "_" & ID & urlSuffix
				else
					ret = replace(showType, "{page}", 1)&config.FileExt
				end if	
			end if
		end if
		FirstLink = ret
	End Property	
	
	'前页链接
	Public Property Get PrevLink
		if page = 2 then
			PrevLink = FirstLink
		elseif page > 2 then
			PrevLink = PageLink( page - 1 )	
		end if
	End Property
	
	'下一页链接
	Public Property Get NextLink
		if page < totalPage then
			NextLink = PageLink( page + 1 )	
		end if
	End Property		

	'尾页链接
	Public Property Get LastLink
		if totalPage > 1 and totalPage <> page then
			LastLink = PageLink( totalPage )
		end if
	End Property
	
	Public Property Get bar( pageCount )
		dim nav
		if total < 1 or totalPage < 2 Then
			bar = ""
			Exit Property
		end if
		nav = Array()
		if page > 1 or POP_MVC.config("isWap") = 1 then
			POP_MVC.Arr.push nav,"<a href='" & FirstLink & "'>首页</a>"
			POP_MVC.Arr.push nav,"<a href='" & PrevLink & "'>上一页</a>"
		end if
		
		if POP_MVC.config("isWap") = 1 then
			POP_MVC.Arr.push nav,SelectNumList( pageCount )
		else
			POP_MVC.Arr.push nav,NumList( pageCount )
		end if		
		
		if page < totalPage or POP_MVC.config("isWap") = 1 then
			POP_MVC.Arr.push nav,"<a href='" & NextLink & "'>下一页</a>"
			POP_MVC.Arr.push nav,"<a href='" & LastLink & "'>尾页</a>"
		end if		
	
		
		bar = join( nav, seperator )
	End Property
	
	Public Property Get selectbar(  )
		dim ret,i,pagestr
		ret = "<SELECT NAME=""select"" ONCHANGE=""var jmpURL=this.options[this.selectedIndex].value ; if(jmpURL!='') {window.location=jmpURL;} else {this.selectedIndex=0 ;}"" >"
		

		for i=1 to totalPage
			pagestr = ""
			if Page =i then pagestr=" selected='selected'"
			
			if config.runMode=0 then
				ret=ret&"<option value='" & linkPrefix & showType  & "_" & ID &"_"& i &"'  "& pagestr &">"& i &"</option>"
			else
				ret=ret&"<option value="&replace(showType, "{page}", i)&" "& i &">" & i &"</option>"
				
			end if	
		next
			ret=ret&"</SELECT>"		
		selectbar = ret
	End Property
	
	'页码链接
	Public Property Get SelectNumList( pageCount )
		dim dLeft,dRight,nav
		pageCount = Cint(pageCount)
		nav = array()
		
	
		
		if total < 1 or totalPage < 2 Then
			SelectNumList = ""
			Exit Property
		end if

		if page = 1 then
			POP_MVC.Arr.push nav,"<option selected='selected' value='" & FirstLink & "'>" & 1 & "</option>"
		else
			POP_MVC.Arr.push nav,"<option selected='selected' value='" & PageLink(page) & "'>" & page & "</option>"
		end if
		
		
		dLeft = page - 1
		dRight = page + 1
			
		
		do While dLeft >= 1 OR dRight <= totalPage		
			if UBound(nav) >= pageCount-1 then
				exit do
			end if		
			leftPage = dLeft
			rightPage = dRight
			if dLeft > 1 then
				POP_MVC.Arr.Unshift nav,"<option value='" & PageLink( dLeft ) & "'>" & dLeft & "</option>"	
				dLeft = dLeft - 1
			elseif dLeft = 1 then
				POP_MVC.Arr.Unshift nav,"<option value='" & FirstLink & "'>" & dLeft & "</option>"	
				dLeft = dLeft - 1
			end if
			
			if dRight <= totalPage then
				if dRight >= 1 then
					rightPage = dRight
					POP_MVC.Arr.Push nav,"<option value='" & PageLink( dRight ) & "'>" & dRight & "</option>"	
				end if
				
				dRight = dRight + 1
			end if
		Loop
		if leftPage < 1 then leftPage = 1
		if rightPage > totalPage then rightPage = totalPage
		

		SelectNumList = "<SELECT ONCHANGE=""var jmpURL=this.options[this.selectedIndex].value ; if(jmpURL!='') {window.location=jmpURL;} else {this.selectedIndex=0 ;}"" >" & join(nav,seperator) & "</SELECT>"
		

	End Property	
	
	'将当前页的起始与结束位以数组形式返回，如 Array( 10,19 )
	Public Function getRange(  )
		dim start_,end_
		start_ = (page-1) * perpage
		end_ = start_ + perpage -1
		if end_ > total - 1 then
			end_ = total -1
		end if
		
		getRange = Array( start_ , end_ )
	End Function
	

	'页码链接
	Public Property Get NumList( pageCount )
		dim dLeft,dRight,nav
		pageCount = Cint(pageCount)
		nav = array()

		if total < 1 or totalPage < 2 Then
			NumList = ""
			leftPage = 1
			rightPage = 1
			Exit Property
		end if

		if page = 1 then
			
			POP_MVC.Arr.push nav,"<a href='" & FirstLink & "' class='" & currentClass & "'>" & 1 & "</a>"
		else
			
			POP_MVC.Arr.push nav,"<a href='" & PageLink(page) & "' class='" & currentClass & "'>" & page & "</a>"
		end if
		
		
		dLeft = page - 1
		dRight = page + 1
			
		
		do While dLeft >= 1 OR dRight <= totalPage		
			if UBound(nav) >= pageCount-1 then
				exit do
			end if		
			leftPage = dLeft
			rightPage = dRight

			
			if dLeft > 1 then
				POP_MVC.Arr.Unshift nav,"<a href='" & PageLink( dLeft ) & "'>" & dLeft & "</a>"	
				dLeft = dLeft - 1
			elseif dLeft = 1 then
				POP_MVC.Arr.Unshift nav,"<a href='" & FirstLink & "'>" & dLeft & "</a>"	
				dLeft = dLeft - 1
			end if

			if dRight <= totalPage then
				if dRight >= 1 then
					rightPage = dRight
					POP_MVC.Arr.Push nav,"<a href='" & PageLink( dRight ) & "'>" & dRight & "</a>"	
				end if
				
				dRight = dRight + 1
			end if
		Loop
		if leftPage < 1 then leftPage = 1
		if rightPage > totalPage then rightPage = totalPage
		

		NumList = join(nav,seperator)
	End Property
	
End Class
%>