<%
' +----------------------------------------------------------------------
' | POPASP [ ASP MVC ]
' +----------------------------------------------------------------------
' | Copyright (c) 2016 http://popasp.com All rights reserved.
' +----------------------------------------------------------------------
' | Author: popasp <1737025626@qq.com>
' +----------------------------------------------------------------------
Class POPASP_CONTROLLER
	public template,success_tip,error_tip
	public success_wait,error_wait
	private tplShowTime,classType
	public dispatchJumpUrl,ajaxArgsDict
	
	'POPASP操作成功返回1，失败返回0
	'但有些UI，比如layui的fly成功返回0，失败返回1，此时就需要设置
	Public ajaxSuccessStatus,ajaxErrorStatus
	
	' 请求类型是否为GET
	Function isGet()
		isGet = POP_MVC.isGet
	End Function
	
	' 请求类型是否为POST
	Function isPost
		isPost = ( "post" = LCase( Request.ServerVariables("REQUEST_METHOD") ) )
	End Function
	
	' 是否为ajax请求
	Function isAjax()
		isAjax = POP_MVC.isAjax
	End Function
	
	'判断网址是否从本网站的一个链接而来，而非直接打开页面而来。
	Public Property Get isSelfOrigin()
		isSelfOrigin = POP_MVC.isSelfOrigin
	End Property
	
	'注销对象，可连贯操作
	Public Function u( ByRef obj )
		Call POP_MVC.Unset(obj)
		set u = Me
	End Function
	
	' 生成验证码
	Public Property Get Verify
		P_("VERIFY").output
	End Property

	
	'快速生成只有一个键值对的Dictionary对象
	Public Function dict( key ,value )
		dim d
		set d = D_
		if typename(value) = "IStringList" then
			d(key) = CStr(value)
		elseif isObject( value ) then
			set d(key) = value
		else
			d(key) = value
		end if
		set dict = d
	end Function
	
	'解析模板文件并显示
	Public Property get Display( arg )
		POP_MVC.dG_( "viewStartTime" ) = timer()
		set template = POP_MVC.import("POPASP_TEMPLATE" )
		template.Display(arg)
		POP_MVC.dG_( "viewEndTime" ) = timer()
	End Property
	
	'解析模板文件 c/a 并显示
	Public Property get Show( )
		on error resume next
		set template = POP_MVC.import("POPASP_TEMPLATE" )
		template.Display( POP_MVC.c & "/" & POP_MVC.a )
		call L_( classType & "Show" )
	End Property
	
	'获取解析内容
	Public Property get Fetch( tpl )
		set template = POP_MVC.import("POPASP_TEMPLATE" )
		Fetch = template.Fetch(tpl)
	End Property
	
    ' assigns values to template variables
    ' @param Dictionary|string tpl_var the template variable name(s)
    ' @param mixed value the value to assign
    public Sub assign( tpl_var, value )
		on error resume next		

		dim arr,stype,bool	'arr为dictionary对象
		dim dict,i,temp
		
		stype = typename(tpl_var)
		
		set arr = D_
		
		
		if stype = "Dictionary" Then	'直接分配Dictionary，此时Value值无效，赋值时随便给个什么
            set arr = POP_MVC.dict.clone( tpl_var )
		elseif stype = "Recordset" Then
			for i = 0 to tpl_var.fields.count-1
				arr( tpl_var.Fields(i).Name ) = tpl_var.Fields(i).Value 
			next			
        elseif stype = "String" Then	'既有变量名,又有变量值
			bool = false
			if isArray(value) then
				if ubound(value)>=0 then
					'如果第一个元素为Recordset，格式应为 Array( rs,需删除前缀的字符串,是否转小写 )
					if typename( value(0) ) = "Recordset" then
						POP_MVC.dict.edit arr,tpl_var,rs2dict(value)
						bool = true
					'如果Array(...,rs)
					elseif typename( value( ubound(value) ) ) = "Recordset" then	
						'如果第一个元素为1，则按 POP_MVC.rs2dict( array(rs) )处理
						if TypeName( value(0) ) = "Integer" then
							if value(0) = 1 then
								POP_MVC.dict.edit arr,tpl_var,POP_MVC.rs2dict( array( value( ubound(value) ) ) )
								bool = true
							end if
						else	'对应格式Array( 需删除前缀的字符串,是否转小写,rs )							
							set temp =  value( ubound(value) )
							call POP_MVC.Arr.Pop( value )
							call POP_MVC.Arr.unshift( value , temp )							
							POP_MVC.dict.edit arr,tpl_var,POP_MVC.rs2dict( value )
							bool = true
						end if						
					end if
				end if
			end if
			if not bool then
				select case typename(value)
					case "Recordset"
						POP_MVC.dict.edit arr,tpl_var,POP_MVC.rs2dict(value)
					case "Field"
						POP_MVC.dict.edit arr,tpl_var,value.value
					case else
						POP_MVC.dict.edit arr,tpl_var,value
				end select
			end if
        End If
		set POP_MVC.tpl_vars = POP_MVC.dict.merge(POP_MVC.tpl_vars,arr)
		
		set arr = nothing
		call L_("POPASP_CONTROLLER.assign")
    End Sub 'end Sub assign
	
	' 将Recordset结果集转化成Dictionary，每行记录的键名均为数字类型，且为Dictionary类型
	Public Function rs2dict(byref arg)
		on error resume next
		dim rs,prefix,bLcase,bound,i,k,m,key,dict,start,sql,j,bool
		j=0
		set rs2dict = Server.CreateObject("Scripting.Dictionary")
		
		
		'如果是数组，则第一个元素对应rs，第二个对应前缀，第三个对应是否将名称转为小写
		if isArray( arg ) then
			bound = ubound(arg)			
			set rs = arg(0)
			if bound > 1 then				
				prefix = arg(1)
				bLcase = arg(2)
			elseif bound > 0 then
				prefix = arg(1)
				bLcase = POP_MVC.config("TMPL_ASSIGN_RS_BLCASE")
			elseif bound = 0 then
				prefix = POP_MVC.config("TMPL_ASSIGN_RS_PREFIX")
				bLcase = POP_MVC.config("TMPL_ASSIGN_RS_BLCASE")
			else
				POP_MVC.Exit( "POPASP_MVC.rs2dict分配参数错误" )
			end if
		elseif typename(arg) = "Recordset" then
			set rs = arg
			prefix = ""
			bLcase = False
		end if
		
		if not isArray(prefix) then
			prefix = split(prefix,",")
		end if
		
		bound = ubound( prefix )
		
		'如果显示控制台，则进行计时
		if not is_empty( POP_MVC.config("SHOW_PAGE_TRACE") ) Then
			start = timer()	
		end if		
		
		bool = (LCase(POP_MVC.config("DB_TYPE")) <> "access")
		
		'如果匹配出top1 或者limit 1，则取出一条记录，返回一维的Dictionary对象
		if POP_MVC.String.reg_test(rs.Source,"^\s*SELECT\s+TOP\s+1\s+.+|^\s*SELECT\s+.*?LIMIT\s+1\s*;?$","gi") then
			set dict = Server.CreateObject("Scripting.Dictionary")					
			for i = 0 to rs.fields.count-1
				key = rs.Fields(i).Name
				
				'去掉前缀
				if bound>=0 then	
					for m = 0 to bound
						if POP_MVC.String.StartsWith(key, prefix(m) ) then
							key = mid(key, len( prefix(m)) +1 )
							exit for
						end if
					next
				end if
				
				'将键名转为小写
				if not is_empty(bLcase) then
					key = LCase(key)
				end if

				if  bool then
					err.clear
					Call typename( rs.Fields(i).Value )
					if err.number <> 0 then
						if typename(rs.Fields(i).Value) = "Long" then
							dict( key ) = CLng(rs.Fields(i).Value)
						else
							dict( key ) = rs.Fields(i).Value
							'POP_MVC.Exit( "发现了不能被ASP解析的数据类型，请联系POPASP作者，以解决此BUG" )
						end if						
					else
						dict(key) = rs.Fields(i).Value
					end if						
				else
					dict(key) = rs.Fields(i).Value
				end if	
			next						
			set rs2dict = dict
		else	'取出二维的Dictionary对象
			set rs2dict = Server.CreateObject("Scripting.Dictionary")
			if rs.recordCount > 0 then	
				for k = 1 to rs.pageSize		
					if not rs.BOF and not rs.EOF Then					
						set dict = Server.CreateObject("Scripting.Dictionary")
						for i = 0 to rs.fields.count-1
							key = rs.Fields(i).Name
							
							'去掉前缀
							if bound>=0 then	
								for m = 0 to bound
									if POP_MVC.String.StartsWith(key, prefix(m) ) then
										key = mid(key, len( prefix(m)) +1 )
										exit for
									end if
								next
							end if							
							
							'将键名转为小写
							if not is_empty(bLcase) then
								key = LCase(key)
							end if
							
							if bool then
								err.clear
								Call typename( rs.Fields(i).Value )
								if err.number <> 0 then
									if typename( rs.Fields(i).Value ) = "Long" then
										dict( key ) = CLng(rs.Fields(i).Value)
									else
										dict( key ) = rs.Fields(i).Value
										'POP_MVC.Exit( "发现了不能被ASP解析的数据类型，请联系POPASP作者，以解决此BUG" )
									end if										
								else
									dict( key ) = rs.Fields(i).Value
								end if						
							else
								dict( key ) = rs.Fields(i).Value
							end if	
						next
						rs2dict.add j,dict
						rs.MoveNext
						j = j+1
					end If
				next
			end if
		end if
		
		if not is_empty( POP_MVC.config("SHOW_PAGE_TRACE") ) Then	'保存sql信息
			set sql = D_
			sql("time") = round((timer() - start) * 1000,0)
			sql("sql") = rs.Source & " ; -- Recordset to Dictionary "
			set POP_MVC.dSql(POP_MVC.dSql.count + 1) = sql
		end if		
		set dict = nothing
		call L_("POPASP_CONTROLLER.rs2dict")
	End Function	
	
	'分配变量
	Public Property Let d( byval key, byref val )		
		dim dict
		set dict = POP_MVC.tpl_vars		
		POP_MVC.dict.edit dict,key,val
		set POP_MVC.tpl_vars = dict
	End Property
	
	Public Function url( byval url_, byval arg )
		dim ret,arr,matches
		if url_ = "" then
			ret = URL__ & POP_MVC.a
		elseif POP_MVC.String.reg_test( url_, "^\w+\/\w+$" , "" ) then
			arr = split( url_, "/" )
			ret = "?c=" & arr(0) & "&a=" & arr(1)
		elseif POP_MVC.String.reg_test( url_, "^\w+\/\w+&.*" , "" ) then
			set matches = POP_MVC.String.reg_exec( url_, "^(\w+)\/(\w+)(&.*)" , "" )
			ret = "?c=" & matches(0).subMatches(0) & "&a=" & matches(0).subMatches(1) & matches(0).subMatches(2)
		end if
		
		if arg <> "" then
			ret = ret & "&" & arg
		end if
		url = ret
	End Function
	
	'数据缓存是否过期，没有过期，自动加载数据
	Public Function expired(id)
		set template = POP_MVC.import("POPASP_TEMPLATE" )
		expired = template.expired(id)
	End Function
	
	'数据是否缓存
	Public Property Get is_cached( id )
		set template = POP_MVC.import("POPASP_TEMPLATE" )
		is_cached = template.is_cached(id)		
	end Property
	
	
	'是否缓存
	Public Property Get isCached( arg )
		set template = POP_MVC.import("POPASP_TEMPLATE" )
		isCached = template.isCached( arg )		
	end Property	
	
	'加载模板文件
	Public Property Get [Load]( arg )
		set template = POP_MVC.import("POPASP_TEMPLATE" )
		if isArray( arg ) then
			if ubound( arg ) = 0 then
				call template.Load( arg(0) ,""  )
			elseif ubound( arg ) > 0 then
				call template.Load( arg(0) ,arg(1) )
			else
				POP_MVC.Exit( "POPASP_CONTROLLER.Load参数分配错误！！" )
			end if
		else
			call template.Load( arg , "" )
		end if		
	End Property
	
	Public Property Get Cache()
		set template = POP_MVC.import("POPASP_TEMPLATE" )
		Call template.Cache()
	End Property
	
	'注销对象
	'在控制器中使用为 that.u(rs).u(dict)
	Public Sub Unset( ByRef obj )
		Call POP_MVC.Unset(obj)
	End Sub
	
	
	'获取来源网址，"http://127.0.0.1:2222/26/find1.asp"
	Function getReferer()
		getReferer = Request.ServerVariables("HTTP_REFERER")
	End Function
	
	'获取来源服务器，"http://127.0.0.1:2222"
	Function getOrigin()
		getOrigin = Request.ServerVariables("HTTP_ORIGIN")
	End Function
	
	' 页面跳转
	Public Property Get Redirect(args)
		if not isArray(args) then
			if args = "" then
				Response.redirect Me.Vars("HTTP_REFERER")
			else
				Call R_(args)
			end if
		else
			Call R_(args)
		end if
	End Property	

	
	' 根据键名获取 Request.QueryString中的值
	Public Property Get [Get]( key )
		[GEt] = POP_MVC.get( key )
	End Property
	
	' 根据键名获取 Request.Form中的值
	Public Property Get Form(key)
		Form = POP_MVC.Form( key )
	End Property
	
	' 根据键名获取 Request 中的值
	Public Property Get Req(key)
		Req = POP_MVC.Req( key )
	End Property
	
	' 获取来路
	Public Property Get Refer
		Dim ref
		ref=Me.Vars("REFERER")
		if isNul(ref) then
			ref=Request.ServerVariables("HTTP_REFERER")
		end if
		Refer = ref
	End Property
	
	'存在则返回Request.ServerVariables的键值，不存在返回Empty
	Public Property Get Vars( key )
		Vars = POP_MVC.Vars(key)
	End Property
	
	'存在则返回Request.ServerVariables的键值，不存在返回Empty
	Public Property Get var( key )
		var = Request.ServerVariables(key)
	End Property	
	
	'分配变量
	Public Property Let AjaxArgs( byval key, byref val )		
		ajaxArgsDict(key) = val
	End Property
	
	' Ajax返回
	' 分配变量时采用的步骤如下
	' 如果data是Recordset，函数会自动处理
	Public Sub AjaxReturn ( byRef data , ByRef info ,ByRef status )
		on error resume next
		dim args
		set args = D_	
		
		args.add ajaxArgsDict("data"),data
		args.add ajaxArgsDict("info"),info
		args.add ajaxArgsDict("status"),status

		if not isEmpty( dispatchJumpUrl ) then
			args.add ajaxArgsDict("action") , dispatchJumpUrl
		end if

		if TypeName( args("data") ) = "Recordset" Then
			args("recordCount") = args("data").RecordCount
			args("pageSize") = args("data").PageSize
			args("pageCount") = args("data").pageCount
			args("absolutePosition") = args("data").AbsolutePosition
			args("absolutePage") = args("data").AbsolutePage
			if is_empty( args("data") ) Then
				args("data") = array()
			else
				set args("data") = Me.rs2dict( args("data") )				
			end If
		End If
		
		call Response.AddHeader("content-type","application/json; charset=utf-8")
		Response.Write  js_encode( args )
		call L_("")
		if POP_MVC.config("SHOW_PAGE_TRACE") = "file" then
			Call POP_MVC.show_page_trace
		end if
		'POP_MVC.ajax( POP_MVC.runtime )
		POP_MVC.quit
	End Sub
	
	' Ajax返回
	' 分配变量时采用的步骤如下
	' 如果data是Recordset，函数会自动处理
	' 如果传入Dictionary对象，要传入分页值，需用数组　Array(dict,count)
	Public Sub LayuiTableAjax ( byRef data , ByRef info ,ByRef status )
		on error resume next
		dim args
		set args = D_	

		args.add "msg",info
		args.add "code",status			
		
		if TypeName( data ) = "Recordset" Then
			args.add "data",data
			args("count") = args("data").RecordCount
			if is_empty( args("data") ) Then
				args("data") = array()
			else
				set args("data") = Me.rs2dict( args("data") )				
			end If
		elseif isArray( data ) then
			if ubound( data ) = 1 then
				if typename( data(0) ) = "Dictionary" and is_numeric( data(1) ) then
					args.add "data",data(0)
					args.add "count",data(1)
				else
					args.add "data",data
				end if
			else
				args.add "data",data
			end if
		End If
		call Response.AddHeader("content-type","application/json; charset=utf-8")
		Response.Write  js_encode( args )
		call L_("")
		if POP_MVC.config("SHOW_PAGE_TRACE") = "file" then
			Call POP_MVC.show_page_trace
		end if
		'POP_MVC.ajax( POP_MVC.runtime )
		POP_MVC.quit
	End Sub
	
	' 自定义Ajax返回
	' 如果data是Recordset，函数会自动处理
	Public Property Get AjaxData ( byRef args )
		Call AjaxData0( args )
		POP_MVC.quit
	End Property
	
	' 自定义Ajax返回
	' 如果data是Recordset，函数会自动处理
	Public Property Get AjaxData0 ( byRef args )
		on error resume next
		call Response.AddHeader("content-type","application/json; charset=utf-8")
		if POP_MVC.isScalar(args) then
			Response.Write args
		else
			Response.Write  js_encode( args )
		end if		
		call L_("")
		if POP_MVC.config("SHOW_PAGE_TRACE") = "file" then
			Call POP_MVC.show_page_trace
		end if
		'POP_MVC.ajax( POP_MVC.runtime )
		Me.u(args)
	End Property
	
	'''
     ' 操作失败后跳转的快捷方法
     ' @param string message 提示信息
     ' @param string jump_url 页面跳转地址
     ' @param mixed ajax 是否为Ajax方式 当数字时指定跳转时间
     ' @return void
	'''
	Public Property Get [Error]( args )
		dim message,jump_url,ajax,bnd
		ajax = error_Wait
		if isArray( args ) then
			bnd = ubound( args )
			if bnd > -1 then message = args(0)
			if bnd > 0 then jump_url = args(1)
			if bnd > 1 then ajax = args(2)
		else
			message = args
		end if	
		call dispatchJump( message , ajaxErrorStatus , jump_url , ajax )
	End Property
	
	'''
     ' 操作成功跳转的快捷方法
     ' @param string message 提示信息
     ' @param string jump_url 页面跳转地址
     ' @param mixed ajax 是否为Ajax方式 当数字时指定跳转时间
     ' @return void
	'''
	Public Property Get Success( args )
		dim message,jump_url,ajax,bnd
		ajax = success_wait
		if isArray( args ) then
			bnd = ubound( args )
			if bnd > -1 then message = args(0)
			if bnd > 0 then jump_url = args(1)
			if bnd > 1 then ajax = args(2)
		else
			message = args
		end if	
		call dispatchJump( message , ajaxSuccessStatus , jump_url , ajax )
	End Property
	
	'操作处理
	Public Property Get Jump( arg )
		if not isArray( arg ) then
			if get_suc_bool( arg ) then
				Me.success( Me.success_tip )
			else
				Me.error( Me.error_tip )
			end if
		else
			dim bound : bound = ubound( arg )
			'如果只有一个元素
			if bound = 0 then
				Me.Jump( arg(0) )
				exit Property
			end if
			
			if get_suc_bool(arg(0)) then
				if not isNull( arg(1) ) then
					Me.Success( arg(1) )
				end if
			else
				if bound = 1 then
					Me.error( Me.error_tip )
				else
					if not isNull( arg(2) ) then
						Me.error( arg(2) )
					end if					
				end if
			end if			
		end if	
	End Property
	
	'如果 jumpUrl = -1 ， 则使用js返回上一页，且表单数据不清空
    Private sub dispatchJump(message,status,byval jumpUrl,ajax) 
		on error resume next	

		

		If NOt is_empty( POP_MVC.config("STOP_PAGE_JUMP") ) and status = ajaxSuccessStatus then
			Exit Sub
		End If
		 
		dim dict,tpl,waitSecond,tplContent,arr,matches
			
		if jumpUrl = -1 then
			jumpUrl = ""
		elseif isNul( jumpUrl ) Then
			jumpUrl = POP_MVC.vars("HTTP_REFERER")
		elseif POP_MVC.String.reg_test( jumpUrl , "^\w+$" , "" ) then
			jumpUrl = APP__ & "c=" & POP_MVC.c & "&a=" & jumpUrl
		elseif POP_MVC.String.reg_test( jumpUrl , "^\w+\/\w+$" , "" ) then
			arr = split( jumpUrl , "/" )
			jumpUrl = APP__ & "?c=" & arr(0) & "&a=" & arr(1)	
		elseif POP_MVC.String.reg_test( jumpUrl, "^\w+\/\w+&.*" , "" ) then
			set matches = POP_MVC.String.reg_exec( jumpUrl, "^(\w+)\/(\w+)(&.*)" , "" )
			jumpUrl = "?c=" & matches(0).subMatches(0) & "&a=" & matches(0).subMatches(1) & matches(0).subMatches(2)			
		end if
		
        if isAjax() then 'AJAX提交	
			dispatchJumpUrl = jumpUrl
			if status = ajaxSuccessStatus then
				call ajaxReturn("" , message ,  ajaxSuccessStatus )  
			else
				call ajaxReturn("" , message ,  ajaxErrorStatus )  
			end if
			exit sub
        End If		

		'ajax所代表的是等待秒数
		if is_numeric( ajax ) Then
			waitSecond = ajax
		else			
			if not is_empty( status ) Then
				waitSecond = success_wait
			else
				waitSecond = error_wait
			end if
		End if
		
		if waitSecond <1 then
			Response.redirect jumpUrl
			Response.end
		end if	
		

			
		if status = ajaxSuccessStatus Then	'操作正确时的提示
			if is_empty(POP_MVC.config("TMPL_ACTION_SUCCESS")) Then
				tpl = POP_MVC.mvc_dir & "Tpl/dispatch_jump.html"
			else 
				tpl = POP_MVC.config("TMPL_ACTION_SUCCESS")
			End If
		Else	'操作错误时的提示
			if is_empty(POP_MVC.config("TMPL_ACTION_ERROR")) Then
				tpl = POP_MVC.mvc_dir & "Tpl/dispatch_jump.html"
			else 
				tpl = POP_MVC.config("TMPL_ACTION_ERROR")
			End If
		End If
		
		if not POP_MVC.file.isExists( tpl ) then
			tpl = POP_MVC.mvc_dir & "Tpl/dispatch_jump.html"
		end if
		
		if not isEmpty( objCMS ) then
			objCMS.load(tpl)
			objCMS.content = getJumpContent( objCMS.content,status, message,jumpUrl, waitSecond )
			
			

			objCMS.parseHtml				
			objCMS.output
		else		
			tplContent = POP_MVC.file_get_contents( tpl )
			tplContent = getJumpContent( tplContent,status, message,jumpUrl, waitSecond )
			Response.write( tplContent )
		end if
		if POP_MVC.config("SHOW_PAGE_TRACE") = "file" then
			Call POP_MVC.show_page_trace
		end if
		Response.flush()		
		POP_MVC.quit
    End Sub
	
	Function getJumpContent( tplContent , status,  message , jumpUrl , waitSecond)	
		on error resume next

		dim dict,face,msgTitle
		if status = ajaxSuccessStatus Then	'操作正确时的提示				
			face = POP_MVC.config("TMPL_SUCCESS_FACE")
			msgTitle = "成功跳转"
		else
			face = POP_MVC.config("TMPL_ERROR_FACE")
			msgTitle = " 失败跳转"
		end if	
	
		
        '如果设置了关闭窗口，则提示完毕后自动关闭窗口
		if not is_empty(Me.get("closeWin")) Then
			jumpUrl = "window.close();"
		End If		
		set dict = D_
		dict.add "face",face
		dict.add "msgTitle",msgTitle
		dict.add "message",message
		dict.add "jumpUrl",jumpUrl
		dict.add "version",POP_MVC.version
		dict.add "waitSecond",waitSecond
		dict.add "status",status
		dict.add "ajaxSuccessStatus",ajaxSuccessStatus
		dict.add "ajaxErrorStatus",ajaxErrorStatus
		
		getJumpContent = replace(tplContent , "{$json$}" , js_encode(dict) )
		getJumpContent = replace(getJumpContent , "{$mvc_copyright$}" , POP_MVC.config("MVC_COPYRIGHT") )
		
		if NOT is_empty( POP_MVC.config("HIDE_COPYRIGHT") ) THEN
			getJumpContent = replace(getJumpContent , "{$copyright$}" , "none" )
		else
			getJumpContent = replace(getJumpContent , "{$copyright$}" , "block" )
		end if	
	End Function
	
	Public Property Get echo( str )
		Response.Write str
	End Property
	
	Private Sub Class_Initialize		
		success_tip = "操作成功！"
		error_tip	= "操作失败！"
		success_wait = 3
		error_wait = 3
		classType = typename(Me)
		set ajaxArgsDict = D_
		ajaxArgsDict("data") = "data"
		ajaxArgsDict("info") = "info"
		ajaxArgsDict("status") = "status"
		ajaxArgsDict("action") = "action"
		
		ajaxSuccessStatus = 1
		ajaxErrorStatus   = 0
	End Sub
	
	Private Sub Class_Terminate
		if isObject(template) Then
			set template = nothing
		End If		
		set ajaxArgsDict = nothing
	End Sub
	
	' 中止程序
	Public Sub [exit]()
		Response.End
	End Sub
	
	Public Sub Empty_
		Call A_( POP_MVC.config("EMPTY_MODULE") , POP_MVC.config("DEFAULT_ACTION") )
	end sub
	
	Private Function isTheRequest(key)
		isTheRequest = ( LCase(key) = LCase(getRequestMethod))
	End Function
	
	' 获取请求类型
	Private Function getRequestMethod()
		getRequestMethod = Request.ServerVariables("REQUEST_METHOD")
	End Function
	
	'使用了POPASP_AUTOVALIDATE.Check进行验证
	Public Function Check( value,rule )
		if isArray( rule ) then
			if ubound( rule ) = 0 then
				Check = POP_MVC.import("POPASP_AUTOVALIDATE" ).Check( value,rule(0) , "regex" )
			else
				Check = POP_MVC.import("POPASP_AUTOVALIDATE" ).Check( value,rule(0) , rule(1) )
			end if
		else
			Check = POP_MVC.import("POPASP_AUTOVALIDATE" ).Check( value,rule,"regex" )
		end if		
	End Function
	
	'仅对request.form数据进行mode=3时的验证,validate可以为数组，也可为数组所在文件
	Public Property Get asp_check( byval arg )
		dim bool
		bool = false
		if isArray( arg ) then
			dim validate
			validate = arg
			bool = true
		elseif typename( arg ) = "String" then			
			arg = replace( arg,"\" , "/" )
			dim file 
			file = POP_MVC.appPath & "/Check/" & POP_MVC.String.Ltrim(arg,"/")
			if POP_MVC.file.isFile( file ) then
				execute POP_MVC.asp_get_contents( file )
				bool = true
			end if
		end if

		if not bool then
			POP_MVC.exit( "验证条件错误，请网站开发者重新修订验证条件" )
		end if
		
		'自动验证有三个参数
		if not POP_MVC.import("POPASP_AUTOVALIDATE" ).handle( validate,request.form , 3 ) then
			Me.error( POP_MVC.import("POPASP_AUTOVALIDATE" ).error )
		end if	
	End Property
	
	'js验证
	'可以传入 M_("User").db.validate_
	'或 M_("User")
	'或 包含验证数组的文件名 文件名在 app/Check/文件名 
	'modeArg = Array( mode,fields ) 或者 modeArg = mode
	sub js_check_file( byval arg,byref modeArg , tplFile )		
		if isEmpty( arg ) then
			response.end
		end if
		dim validate,arr,mode,fields,i
		on error resume next
		
		fields = ""
		if isArray( modeArg ) then
			mode = modeArg(0)
			if ubound( modeArg ) > 0 then
				fields = modeArg( 1 )
			end if
		else
			mode = modeArg			
		end if
		
		
		if isArray( arg ) then			'直接以二维数组赋值验证条件	
			validate = arg
		elseif IsObject( arg ) then		'从B_("User")或M_("User")中获取验证数组
			if POP_MVC.String.iEndsWith(typename(arg),"model") then
				validate = arg.db.validate_
			else
				validate = arg.validate_
			end if
		elseif typename( arg ) = "String" then	'从文件中获取验证数组		
			arg = replace( arg,"\" , "/" )
			dim file 
			file = POP_MVC.appPath & "/Check/" & POP_MVC.String.Ltrim(arg,"/")
			if POP_MVC.file.isFile( file ) then
				execute POP_MVC.asp_get_contents( file )
				bool = true
			end if
		end if
	
		if is_empty( mode ) then
			mode = 3
		end if
		
		err.clear
		'从验证数组中剔除mode不匹配的验证
		arr = validate
		validate = array()
		for i = 0 to ubound( arr )
			if ubound(arr(i)) > 4 then
				if arr(i)(0) = "Password" then
					'var_export arr(i)(5)
				end if
				if arr(i)(5) = mode or arr(i)(5) = 3 then
					POP_MVC.Arr.push validate,arr(i) 
				end if				
			else
				POP_MVC.Arr.push validate,arr(i) 
			end if
		next
		
		if not isArray( fields ) then
			fields = split( fields, "," )
		end if		
		
		'如果fields不为空，则从validate中剔除fields中不存在的字段
		arr = Array()
		if ubound( fields ) > -1 then
			for i = 0 to ubound( validate )
				if POP_MVC.Arr.iExists( fields, validate(i)(0) ) then
					POP_MVC.Arr.push arr , validate(i)
				end if
			next
			validate = arr
		end if
	
		if ubound( validate ) <0  then
			POP_MVC.exit( "console.log('验证条件为空数组，请网站开发者重新修订验证条件');" )
		end if
		
		
		if POP_MVC.config("SHOW_PAGE_TRACE") <> "file" then
			'不显示控制台
			POP_MVC.config("SHOW_PAGE_TRACE") = 0
		end if			

		'修改头信息为js类型
		call Response.AddHeader("Content-Type","application/x-javascript; charset=UTF-8")
		
		'输出js文件
		if Me.get("type") = "" or Me.get("type") = "file" then
		
			'输出popasp_check.js文件内容
			Response.write POP_MVC.file_get_contents( tplFile )
		
			'换行
			Response.write vbcrlf
			
		end if
		
	
		'输出数据
		if isEmpty(Me.get("type")) or Me.get("type") = "data" then
			POP_MVC.dJS( "check_validate" ) = validate
			POP_MVC.dJS( "check_mode" ) = mode
			POP_MVC.dJS( "check_patchValidate" ) = POP_MVC.import("POPASP_AUTOVALIDATE" ).patchValidate
			POP_MVC.dJS( "SHOW_PAGE_TRACE" ) = POP_MVC.config( "SHOW_PAGE_TRACE" )
			POP_MVC.dJS( "DEBUG" ) = POP_MVC.config( "DEBUG" )
			
			Response.write "var POPASP_CHECK = " & js_encode( POP_MVC.dJS ) & ";"
		end if
		
		
		if POP_MVC.config("SHOW_PAGE_TRACE") = "file" then
			POP_MVC.show_page_trace
		end if			
		POP_MVC.quit
		
		'结束，以便不显示模板文件内容
		'Response.end
	End sub
	
	'js验证
	'可以传入 M_("User").db.validate_
	'或 M_("User")
	'或 包含验证数组的文件名 文件名在 app/Check/文件名 
	'modeArg = Array( mode,fields ) 或者 modeArg = mode
	Public sub js_check( byval arg,byref modeArg )		
		Call js_check_file( arg, modeArg, POP_MVC.mvc_dir & "Tpl/popasp_check.js"  )
	End sub
	
	'js验证
	Public sub weui_js_check( byref arg,byref mode )
		Call js_check_file( arg, modeArg, POP_MVC.mvc_dir & "weui/popasp_check.js"  )
	End sub	
	
	'js验证
	Public sub layui_js_check( byref arg,byref mode )
		Call js_check_file( arg, modeArg, POP_MVC.mvc_dir & "layui/popasp_check.js"  )
	End sub	
	
	Public Property Get isID( id )		
		if POP_MVC.String.reg_test( CStr(id) , "^[1-9]\d*$" , "" ) then
			isID = true
		else
			isID = false
		end if		
	End Property
	
	'js自动填充
	Public Property get js_auto( byref arg )
		if POP_MVC.config("SHOW_PAGE_TRACE") <> "file" then
			'不显示控制台
			POP_MVC.config("SHOW_PAGE_TRACE") = 0
		end if	

		'修改头信息为js类型
		call Response.AddHeader("Content-Type","application/x-javascript; charset=UTF-8")
		
		'输出js文件
		if Me.get("type") = "" or Me.get("type") = "file" then
		
			'输出popasp_check.js文件内容
			Response.write POP_MVC.file_get_contents(POP_MVC.mvc_dir & "Tpl/popasp_auto.js")
		
			'换行
			Response.write vbcrlf
			
		end if
		
	
		'输出数据
		if isEmpty(Me.get("type")) or Me.get("type") = "data" then
			if typename( arg ) = "Recordset" then
				set POP_MVC.dJS( "auto_data" ) = POP_MVC.rs2dict( arg )
			elseif typename( arg ) = "Dictionary" then
				set POP_MVC.dJS( "auto_data" ) = arg
			end if
			
			POP_MVC.dJS( "SHOW_PAGE_TRACE" ) = POP_MVC.config( "SHOW_PAGE_TRACE" )
			POP_MVC.dJS( "DEBUG" ) = POP_MVC.config( "DEBUG" )
			
			Response.write "var POPASP_AUTO = " & js_encode( POP_MVC.dJS ) & ";" & vbcrlf
			Response.write POP_MVC.file_get_contents(POP_MVC.mvc_dir & "Tpl/popasp_auto_jump.js")
		end if
		
		if POP_MVC.config("SHOW_PAGE_TRACE") = "file" then
			POP_MVC.show_page_trace
		end if
		
		'结束，以便不显示模板文件内容
		'Response.end
		
		POP_MVC.quit
	End Property
	
	function get_err_bool( condition )
		get_err_bool = false

		if isNull( condition ) then
			get_err_bool = false
		elseif typename( condition ) = "Dictionary" then 'Dictionary对象
			if condition.count < 1 then
				Me.u( condition )
				get_err_bool = true
			end if
		elseif typename( condition ) = "Recordset" then 'Recordset对象
			if condition.bof and condition.eof then
				Me.u( condition )
				get_err_bool = true
			end if		
		else
			get_err_bool = CBool(condition)
		end if
	End Function
	
	function get_suc_bool( ByRef condition )
		dim stype
		stype = typename( condition )
		get_suc_bool = false
		if isNull( condition ) then
			get_suc_bool = false
		elseif stype = "Dictionary" then 'Dictionary对象
			if condition.count > 0 then
				get_suc_bool = true
			end if
		elseif stype = "Recordset" then 'Recordset对象
			if not (condition.bof and condition.eof) then
				get_suc_bool = true
			end if		
		else
			get_suc_bool = CBool(condition)
		end if		
	End function
	
	'输出图像
	Property Get outputImg( file_path )
		Dim types,subfix,content_type
		set types = D_
		
		types(".tif") = "image/tiff"
		types(".fax") = "image/fax"
		types(".gif") = "image/gif"
		types(".ico") = "image/x-icon"
		types(".jfif") = "image/jpeg"
		types(".jpe") = "image/jpeg"
		types(".jpeg") = "image/jpeg"
		types(".jpg") = "image/jpeg"
		types(".net") = "image/pnetvue"
		types(".png") = "image/png"
		types(".rp") = "image/vnd.rn-realpix"
		types(".tiff") = "image/tiff"
		types(".wbmp") = "image/vnd.wap.wbmp"
		
		subfix = LCase(POP_MVC.String.rstr( file_path, "." ))
		
		If types.Exists( subfix ) then
			content_type = types( subfix )
		else
			content_type = "image/jpeg"
		end if
		
		'不显示控制台
		POP_MVC.config("SHOW_PAGE_TRACE") = 0
		
		'修改头信息为js类型
		call Response.AddHeader("Content-Type",content_type)
		
		Response.BinaryWrite POP_MVC.stream_get_contents( file_path )
		
		Response.End
	End Property
End Class
%>