<%
' +----------------------------------------------------------------------
' | POPASP [ ASP MVC ]
' +----------------------------------------------------------------------
' | Copyright (c) 2016 http://popasp.com All rights reserved.
' +----------------------------------------------------------------------
' | Author: popasp <1737025626@qq.com>
' +----------------------------------------------------------------------
Class POPASP_COOKIE
	Public Domain
	Public Expires
	Public Path
	Public Secure
	Public Unit
	
	Private Sub Class_Initialize
		call init
	End Sub
	
	'初始化
	Public Sub init
		Domain = POP_MVC.config("COOKIE_DOMAIN")
		Expires = POP_MVC.config("COOKIE_EXPIRES")
		Path = POP_MVC.config("COOKIE_PATH")
		Secure = CBool(POP_MVC.config("COOKIE_SECURE"))
		Unit = POP_MVC.config("COOKIE_EXPIRES_UNIT")
	end Sub
	
	'计算cookie个数
	Public Function Count()
		dim x,y
		Count = 0
		 for each x in Request.Cookies
		  if Request.Cookies(x).HasKeys then
			for each y in Request.Cookies(x)
			  Count = Count + 1
			next
		  else
			Count = Count + 1
		  end if
		next
	End Function
	
	'移除一个键
	Public Property Get Remove( byval key )
		on error resume next
		if isArray( key ) then
			key = key(0)
		end if
		if Exists(key) Then		
			Response.Cookies( key ).Expires = DateDiff( "d",1,now() )			
		end if
		Call L_("")
	end Property
	
	'清空cookie
	Public Property Get Clear()
		dim x
		for each x in Request.Cookies
			Remove(x)
		next
	End Property	
	
	
	'清空cookie，但是进行个别保留
	
	Public Property Get Destroy( ByVal arg )
		dim x
		if isNul( arg ) then
			Call Me.Clear
			Exit Property
		end if
		if not isArray(arg) then
			arg = split( arg, "," )
		end if
		for each x in Request.Cookies
			if not POP_MVC.Arr.iExists( arg , x ) then
				Response.cookies(x)=""
			end if
		next
	End Property
	
	
	'给cookie中设置参数
	' cookie.get(key)					Request.Cookies(key)
	' cookie.get(array(key))			Request.Cookies(key)
	' cookie.get(array(key1,key2))		Request.Cookies(key1)(key2)
	Public Function [Get]( byval key )
		if isArray( key ) then
			if ubound(key) = 0 then
				[Get] = Request.Cookies(key(0))
			else
				[Get] = Request.Cookies(key(0))(key(1))
			end if
		else
			[Get] = Request.Cookies(key)
		end if
	End Function
	
	
	' 判断某个键值是否存在于cookie中
	' cookie.Exists(key)					判断Request.Cookies(key)是否存在
	Public Function Exists( byval key )
		dim x,y
		Exists = false
		if isArray( key ) then
			if ubound(key) = 0 then
				key(0) = UCase( key(0) )
				for each x in Request.Cookies
					if UCase(x) = key(0) then
						Exists = True
						Exit Function
					end if
				next
			else
				key(0) = UCase( key(0) )
				key(1) = UCase( key(1) )
				for each x in Request.Cookies
					if Request.Cookies(x).HasKeys then
						if UCase(x) = key(0) then
							for each y in Request.Cookies(x)
								if UCase(y) = key(1) then
									Exists = True
									exit Function
								end if
							next
						end if
					end if
				next
			end if
		else
			key = UCase( key )
			for each x in Request.Cookies
				if UCase(x) = key then
					Exists = True
					Exit Function
				end if
			next
		end if
	End Function	
	
	Public Sub Assign( ByVal key,ByVal val )
		Me.Set key,val
	End Sub
	
	'将某个值各存cookie与session一份
	'cookieKey为cookie中的键名
	'sessionKey为session中的键名
	'nDay为存cookie多少天
	'value存值，不能为对象，因为对象无法存到cookie中
	Public Sub csSet( cookieKey,sessionKey, nDay , value )
		session(sessionKey) = value
		POP_MVC.config("COOKIE_EXPIRES_UNIT") = "d"
		POP_MVC.config("COOKIE_EXPIRES") = nDay
		Me.set cookieKey,value
	End Sub
	
	'csGet为csSet存取的取用
	'cookieKey为cookie中的键名
	'sessionKey为session中的键名
	'存值不能为对象，因为对象无法存到cookie中
	Public Function csGet( cookieKey,sessionKey )
		if Not isEmpty(session(sessionKey)) then
			csGet = session(sessionKey)
		elseif Me.exists(cookieKey) then
			csGet = Me.get(cookieKey)
		end if
	End Function


	'csDict取一组并分配为键值对，先取session，再取cookie
	'cookieKey1,sessionKey1对应键名
	'cookieKey2,sessionKey2对应值名
	'separate为分隔符
	Public Function csDict( cookieKey1,sessionKey1,cookieKey2,sessionKey2,separate )
		dim theKey,theValue,dict
		set csDict = D_
		theKey = Me.csGet( cookieKey1,sessionKey1 )
		if theKey <> "" then
			theValue = Me.csGet( cookieKey2,sessionKey2 )
			if theValue <> "" then
				theKey = split( theKey, separate )
				theValue = split( theValue, separate )
				set csDict = POP_MVC.Dict.combine( theKey,theValue )
			end if
		end if
	End Function	

	
	'给cookie中设置参数
	' cookie.set key,value					Response.Cookies(key) = value 
	' cookie.set array(key),value			Response.Cookies(key) = value 
	' cookie.set array(key1,key2),value		Response.Cookies(key1)(key2) = value 
	Public sub [Set]( byval key, byval val )
		'on error resume next
		if isArray( key ) then
			if ubound(key) = 0 then
				Response.Cookies( key(0) ) = val
				with Response.Cookies( key(0) )
					.Domain = Domain
					.Expires = DateAdd(Unit,Expires,now()) 
					.Secure = Secure
					.Path   = Path
				end with
			else				
				with Response.Cookies( key(0) )
					.Domain = Domain
					.Expires = DateAdd(Unit,Expires,now() )
					.Secure = Secure
					.Path   = Path
				end with
			end if
			Response.Cookies( key(0) )( key(1) ) = val
		else
			Response.Cookies( key ) = val
			with Response.Cookies( key )
				.Domain = Domain
				.Expires = DateAdd(Unit,Expires,now() )
				.Secure = Secure	
				.Path   = Path
			end with
		end if
	End sub
End Class
%>