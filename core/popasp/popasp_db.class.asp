<%
' +----------------------------------------------------------------------
' | POPASP [ ASP MVC ]
' +----------------------------------------------------------------------
' | Copyright (c) 2016 http://popasp.com All rights reserved.
' +----------------------------------------------------------------------
' | Author: popasp <1737025626@qq.com>
' +----------------------------------------------------------------------

'本类服务于数据库类,除非二次开发,请不要调用本类中的方法
Class POPASP_DB
	Public conn			'数据库连接对象
	Public version		'版本号
	Public db_type,access_type,db_path,db_host,db_user,db_name,db_pwd
	
	Public excelConnectMode '连接方式
	
	'根据SQL获取结果集
	Function getRS(sql)
		
		On error resume next
		
		Dim rs,dict,start
		if not is_empty( POP_MVC.config("SHOW_PAGE_TRACE") ) Then
			start = timer()
		end if		
		
		Set rs = CreateRS() 

		With rs
			.ActiveConnection = conn
			.CursorType = 1
			.LockType = 1
		End With
		rs.Source = sql
		rs.Open

		if err.number <> 0 Then
			Call Me.Exit( sql & ";--语句无法执行 ，" & "POPASP_DATABASE_TOOL.GetRS " & "原因:" & err.number & "," & err.description , "数据库查询错误，详情请查看日志！" )
		end if
		set getRS = rs
		if not is_empty( POP_MVC.config("SHOW_PAGE_TRACE") ) Then
			set dict = D_
			dict("time") = round((timer() - start) * 1000,0)
			dict("sql") = sql
			set dict("rs") = getRS
			set POP_MVC.dSql(POP_MVC.dSql.count + 1) = dict
			POP_MVC.num_db_query = POP_MVC.num_db_query + 1
			POP_MVC.num_recordset = POP_MVC.num_recordset + 1
		end if		
	End Function
	
	Sub [Exit]( str1,str2 )
		if  not is_empty(POP_MVC.config("APP_DEBUG")) Then
			POP_MVC.Exit( str1 )
		else				
			POP_MVC.Exit( str2 )
		end if		
	End Sub
	
	'测试数据库连接，参数跟B_参数类似，少了第一个表名
	Function testConnect(ByVal arg)
		Dim dbType,accessType,dbPath,dbHost,dbUser,dbName,dbPwd,dbPrefix,bnd
		Dim db_type2,access_type2,db_path2,db_host2,db_user2,db_name2,db_pwd2,db_prefix2
		Dim conn,connStr
		err.clear
		on error resume next
		Set conn = Server.CreateObject("ADODB.Connection")
		
		if err.number <> 0 then
			testConnect = err.description
			exit function
		end if
		
		'分配数据库相关的配置项
		accessType = access_type
		dbPath = db_path
		dbName = db_name
		dbHost = db_host
		dbUser = db_user
		dbPwd  = db_pwd
		dbPrefix  = db_prefix
		
		if not isArray( arg ) then
			dbType = arg
		else	
			dbType =  trim(lcase(arg(0)))
			'如果是数组的话，则按如下参数分配
			'access: db_type,db_path,access_type
			'excel: db_type,db_path
			'sqlite3: db_type,db_path
			'txt: db_type,db_path
			'mysql: db_type,db_name,db_host,db_user,db_pwd
			'sql server: db_type,db_name,db_host,db_user,db_pwd
			if ubound( arg ) > 0 then				
				bnd = ubound( arg )
				if dbType = "access" then
					if bnd > 0 then dbPath = arg(1)
					if bnd > 1 then accessType = arg(2)					
				ElseIf dbType = "excel" or  dbType = "sqlite3" or  dbType = "txt" then
					if bnd > 0 then dbPath = arg(1)
				ElseIf dbType = "mysql" or  dbType = "sqlserver" then
					if bnd > 0 then dbName = arg(1)
					if bnd > 1 then dbHost = arg(2)
					if bnd > 2 then dbUser = arg(3)
					if bnd > 3 then dbPwd  = arg(4)
					if bnd > 4 then dbPrefix  = arg(5)
				end if
			end if
		end if
		
		'临时保存配置项
		db_type2 = db_type
		access_type2 = access_type
		db_path2 = db_path
		db_name2 = db_name
		db_host2 = db_host
		db_user2 = db_user
		db_pwd2  = db_pwd
		db_prefix2  = db_prefix
		
		db_type  = dbType
		access_type = accessType
		db_path =  dbPath
		db_name  = dbName
		db_host  = dbHost
		db_user  = dbUser
		db_pwd   = dbPwd
		db_prefix   = dbPrefix
		
		if isDbType( "mysql" ) then
			conn.CursorLocation = 3			
		end if
		
		if isDbType( "sqlserver" ) or isDbType("mysql") then
			connStr = getSqlConnStr( db_host ,db_user,db_pwd,db_name)	
		else
			connStr = getConnStr(db_path,db_pwd)
		end if
		
		conn.open connStr
		
		If Err.number <> 0 Then
			'分配数据库相关的配置项
			db_type = db_type2
			access_type = access_type2
			db_path = db_path2
			db_name = db_name2
			db_host = db_host2
			db_user = db_user2
			db_pwd  = db_pwd2
			db_prefix  = db_prefix2
			testConnect = err.description
		End If
		conn.close
		set conn = nothing
	End Function
	
	'初始连接数据库
	Public Property Get initConn		
		if is_empty(conn) Then
			if isDbType( "sqlserver" ) or isDbType("mysql") then
				'getSqlConnStr( byval db_host,byval db_user,byval db_pwd,byval db_name )
				call connectDB( getSqlConnStr( Me.db_host ,Me.db_user,Me.db_pwd,Me.db_name) )
			else
				call connectDB( getConnStr(Me.db_path,Me.db_pwd) )
			end if
		End If
	End Property
	
	'根据连接字符串连接数据库
	'适用于文本型数据库与服务器型数据库
	Sub connectDB( conn_str )
		err.clear
		on error resume next
		dim startTime : startTime = timer()
		Set conn = POP_MVC.SCO("ADODB.Connection")
		
		if isDbType( "mysql" ) then
			conn.CursorLocation = 3			
		end if
		
		conn.open conn_str
		
		If Err.number <> 0 Then
			Call Me.Exit( "数据库连接失败，失败原因为:" & Err.Number & "，" & err.description , "数据库连接失败" )
		End If
		call POP_MVC.pushTime( startTime , "连接数据库 " & Me.db_type & " ")
		
		if isDbType( "mysql" ) AND  POP_MVC.config("MYSQL_CONNSTR") <> "" then
			'SET character_set_client = utf8;
			'SET character_set_results = utf8; 
			'SET character_set_connection = utf8; 
			conn.execute( "set names gbk" )
		end if
	End Sub
	
	'获取sqlserver或mysql连接数据库字符串
	Private Function getSqlConnStr( byval db_host,byval db_user,byval db_pwd,byval db_name )
		if isDbType("sqlserver") Then
			getSqlConnStr = "driver={sql server};database=" & db_name & ";server=" & db_host & iif( POP_MVC.config("DB_PORT") = "" , "" , "," & POP_MVC.config("DB_PORT") ) &  ";uid=" & db_user & ";pwd=" & db_pwd
		elseif isDbType("mysql") Then	'2.2版本中添加
			if POP_MVC.config("MYSQL_CONNSTR") = "" then	'如果为空，则采用3.51来连接
				getSqlConnStr = "driver={mysql odbc 3.51 driver};server=" & db_host & ";" & iif( POP_MVC.config("DB_PORT")="" , "" , "port=" & POP_MVC.config("DB_PORT") & ";" ) & "database=" & db_name & ";user name=" & db_user & ";password=" & db_pwd
			else	'否则，自己写连接字符串
				getSqlConnStr = POP_MVC.config("MYSQL_CONNSTR")
			end if
		end if	
	End Function
	
	'获取文本型数据库连接字符串，适合access、excel、sqlite3、txt
	Private Function getConnStr( byRef dbPath,ByRef password )
		dim ext,tDb	
		If Instr(dbPath,":")>0 Then : tDb = dbPath : Else : tDb = POP_MVC.realPath(dbPath) : End If		
		if isDbType("access") Then
			ext = getFileExt(dbPath)
			if ext = "mdb" Then				
				getConnStr = "Provider=Microsoft.Jet.OLEDB.4.0;Data Source=" & tDb & ";Jet OLEDB:Database Password="& password & ";"
			elseif ext = "accdb" Then
				getConnStr = "Provider=Microsoft.ACE.OLEDB.12.0;Mode=Share Exclusive;Data Source=" & tDb & ";"
			else
				select case Me.access_type
					case "2003","mdb"
						getConnStr = "Provider=Microsoft.Jet.OLEDB.4.0;Data Source=" & tDb & ";Jet OLEDB:Database Password="& password & ";"
					case "2007","accdb"
						getConnStr = "Provider=Microsoft.ACE.OLEDB.12.0;Mode=Share Exclusive;Data Source=" & tDb & ";"
					case else
						Call Me.Exit( "不支持的数据库类型 access 后缀:" & ext, "不支持的数据库类型" )
				end select
			End if		
		Elseif isDbType("sqlite3") Then
			getConnStr = "DRIVER={" & POP_MVC.config("SQLITE3_DRIVER_NAME") & "};Database=" & tDb & ";"	
		Elseif isDbType("excel") Then
			'IMEX的值，0为只写，1为只读，2为可写可读
			'HDR表示要把第一行作为数据还是作为列名,作为数据用HDR=no,作为列名用HDR=yes;
			'Provider=Microsoft.Jet.Oledb.4.0;data source=D:\\Data.xls;Extended Properties=Excel 8.0;
			if not is_empty( excelConnectMode ) then	' 查询用
				if POP_MVC.String.iEqual( POP_MVC.String.rstr( tDb , ".") , ".xls" ) Then
					getConnStr = "Provider=Microsoft.Jet.OLEDB.4.0;Data Source=" & tDb & ";Extended Properties='Excel 8.0;HDR=yes;IMEX=2';"
				elseif POP_MVC.String.iEqual( POP_MVC.String.rstr( tDb , ".") , ".xlsx" ) Then
					getConnStr = "Provider=Microsoft.ACE.OLEDB.12.0;Data Source=" & tDb & ";Extended Properties='Excel 12.0;HDR=yes;IMEX=2';"
				end if
			else	'遍历表名用
				if POP_MVC.String.iEqual( POP_MVC.String.rstr( tDb , ".") , ".xls" ) Then	'2003
					getConnStr = "Driver={Microsoft Excel Driver (*.xls)};DBQ=" & tDb & ";ReadOnly=true;Provider=MSDASQL" 
				elseif POP_MVC.String.iEqual( POP_MVC.String.rstr( tDb , ".") , ".xlsx" ) Then	'2007
					getConnStr = "Driver={Microsoft Excel Driver (*.xls, *.xlsx, *.xlsm, *.xlsb)};DBQ=" & tDb & ";ReadOnly=true;Provider=MSDASQL" 
				end if				
			end if
		Elseif isDbType("txt") Then
			getConnStr = "Driver={Microsoft Text Driver (*.txt; *.csv)};Dbq=" &  tDb & ";Extensions=asc,csv,tab,txt;"
		End if
	End Function
	
	' 执行sql语句，适用于update、delete，如果要添加数据，最好使用insert，可以返回最后生成的ID
	' arg如果是字符串则对应单条sql，如果是数组，分别为array( sql, 是否中断程序, 是否将sql记录在dN_  )
	Public Property Get Execute( arg )
		err.clear
		On Error Resume Next
		Dim cmd,dict,start,sql,bErr,bSql
		
		bErr = true
		bSql = true
		if isArray( arg ) then
			sql = arg( 0 )
			bErr = false
			if ubound( arg ) > 0 then
				bErr = arg( 1 )
			end if
			
			if ubound( arg ) > 1 then
				bSql = arg( 2 )
			end if
		else
			sql = arg
		end if
		
		if not is_empty( POP_MVC.config("SHOW_PAGE_TRACE") ) Then
			start = timer()	
		end if
		

		set cmd = CreateCMD( sql )
		cmd.execute Execute,,129
		set cmd = nothing

		if err.number <> 0 Then	
			if not is_empty(bErr) then
				Call Me.Exit( sql & ";--语句无法执行 ，" & "POPASP_DATABASE_TOOL.Execute " & "原因:" & err.number & "," & err.description & "数据库查询错误，详情请查看日志！" , "数据库查询错误，详情请查看日志！" )
			else
				Call L_( sql & ";--语句无法执行 ，" & "POPASP_DATABASE_TOOL.Execute " & "原因:" & err.number )
			end if
		else
			if not is_empty( POP_MVC.config("SHOW_PAGE_TRACE") ) Then
				set dict = D_
				dict("time") = round((timer() - start) * 1000,0)
				dict("sql") = sql
				set POP_MVC.dSql(POP_MVC.dSql.count + 1) = dict
				if not is_empty( bSql ) then
					POP_MVC.num_db_write = POP_MVC.num_db_write + 1
				end if
			end if
		end if
	End Property
	
	function safe( str )
		safe = POP_MVC.String.reg_replace(str,"''","'","g")
	end function
	
	Private Sub Class_Initialize
		db_type = POP_MVC.config("DB_TYPE")
		access_type = POP_MVC.config("ACCESS_TYPE")
		db_path	= POP_MVC.config("DB_PATH")	'文本型数据库路径
		db_pwd	= POP_MVC.config("DB_PWD")	'数据库密码
		db_name	= POP_MVC.config("DB_NAME")	'服务器型数据库名
		db_host	= POP_MVC.config("DB_HOST")	'服务器
		db_user	= POP_MVC.config("DB_USER")	'用户名

		version = POP_MVC.Version
		excelConnectMode = 1
	End Sub
	
	Private Sub Class_Terminate
		'在这里千万不能注销对象conn，否则会出错，conn的注销在popasp.asp文件中
	End Sub	
	
	'根据sql创建对象server.createobject("adodb.command")，使用完之后，一定要记得关闭
	Function CreateCMD( sql )
		dim cmd		
		call initConn	'连接数据库
		Set cmd=POP_MVC.SCO("adodb.command")
		cmd.ActiveConnection = conn
		cmd.CommandText = sql
		set CreateCMD = cmd
	End Function
	
	'创建对象server.createobject("adodb.recordset")，使用完之后，一定要记得关闭
	Function CreateRS()		
		call initConn	'连接数据库		
		set CreateRs = POP_MVC.SCO("adodb.recordset") 
	End Function
	
	'关闭recordset
	Sub closeRS( ByRef rs )		
		rs.close : set rs = nothing	
	End Sub
	
	'判断是否为该种数据库
	Private Function isDbType( mode )
		isDbType = POP_MVC.String.iEqual( Me.db_type , mode )
	End Function
	
	' 获得文件的后缀名
	Private Function getFileExt( file )
		getFileExt = mid(file,inStrRev(file,".")+1)
	End Function
End Class
%>