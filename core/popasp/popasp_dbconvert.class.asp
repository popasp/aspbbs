<%
' +----------------------------------------------------------------------
' | POPASP [ ASP MVC ]
' +----------------------------------------------------------------------
' | Copyright (c) 2016 http://popasp.com All rights reserved.
' +----------------------------------------------------------------------
' | Author: popasp <1737025626@qq.com>
' +----------------------------------------------------------------------
' 数据库转化类
Class POPASP_DBCONVERT
	'实现了mysql与access的互相转换
	'实现了 mysql/access 到 sqlite3 的转换
	'实现了 sqlite 到 mysql 的转换
	'转化数据由配置中的RS_2DICT_LIMIT值决定
	
	'清空 Access 的某个数据表
	Public Function TruncateAccTable(ByRef dbPath, ByVal tableName ,ByRef db_prefix )
		dim db_type,db_path
		dim db,conn,conn_str,pk
		db_type = POP_MVC.config("DB_TYPE")	
		db_path = POP_MVC.config("DB_PATH")	

		POP_MVC.config("DB_TYPE") = "access"
		POP_MVC.config("DB_PATH") = dbPath
		
		if not POP_MVC.String.iStartsWith( tableName , db_prefix ) then
			tableName = db_prefix & tableName
		end if		
		
		set db = B_(tableName)
		db.where("1=1").remove
		pk = db.getPK
		if not isEmpty(pk) then
			set db.tool.conn = nothing
			Set conn = CreateObject("ADODB.Connection")
			conn_str = db.tool.getConnStr( db.db_path , db.db_pwd )
			conn.open conn_str
			if POP_MVC.String.iEndsWith( db.db_path, ".mdb"  ) then
				conn.execute( "ALTER TABLE [" & tableName & "] ALTER LONG" )
			end if
			conn.execute( "ALTER TABLE [" & tableName & "] ALTER COLUMN [" & pk & "] COUNTER(1,1)" )
			set conn = nothing
		end if		
		set db.tool = nothing
		set db = nothing
		
		TruncateAccTable = tableName
		POP_MVC.config("DB_TYPE") = db_type
		POP_MVC.config("DB_PATH") = db_path
	End Function

	'清空 sqlite3 的某个数据表
	Public Function TruncateSqliteTable(ByRef dbPath, ByVal tableName ,ByRef db_prefix )
		dim db_type,db_path
		db_type = POP_MVC.config("DB_TYPE")	
		db_path = POP_MVC.config("DB_PATH")	

		POP_MVC.config("DB_TYPE") = "sqlite3"
		POP_MVC.config("DB_PATH") = dbPath
		
		if not POP_MVC.String.iStartsWith( tableName , db_prefix ) then
			tableName = db_prefix & tableName
		end if
		
		P_("db").execute( "DELETE FROM '" & tableName & "'" )
		P_("db").execute( "DELETE FROM sqlite_sequence WHERE name = '" & tableName & "'" )
		P_("db").execute( "UPDATE sqlite_sequence SET seq = 0 WHERE name = '" & tableName & "'" )
		
		TruncateSqliteTable = tableName
		POP_MVC.config("DB_TYPE") = db_type
		POP_MVC.config("DB_PATH") = db_path
	End Function

	'清空 mysql 的某个数据表
	Public Function TruncateMysqlTable( ByVal dbName, ByVal tableName ,ByRef db_prefix )
		dim db_type,db_name
		db_type = POP_MVC.config("DB_TYPE")	
		db_name = POP_MVC.config("DB_NAME")	

		POP_MVC.config("DB_TYPE") = "mysql"
		
		if not POP_MVC.String.iStartsWith( tableName , db_prefix ) then
			tableName = db_prefix & tableName
		end if

		P_("db").execute( "TRUNCATE TABLE " & tableName & "IF EXISTS" )
		TruncateMysqlTable = tableName
		POP_MVC.config("DB_TYPE") =  db_type
		POP_MVC.config("DB_NAME") = db_name
	End Function
	
	'清空 access 的所有数据表
	Public Property Get TruncateAccTables( ByRef dbPath )
		dim db_type,db_path,i,ret
		db_type = POP_MVC.config("DB_TYPE")	
		db_path = POP_MVC.config("DB_PATH")	

		POP_MVC.config("DB_TYPE") = "access"
		POP_MVC.config("DB_PATH") = dbPath
		set tables = P_("access").getTables
		tables = tables.items
		ret = Array()
		for i = 0 to ubound(tables)
			Call Me.TruncateAccTable( dbPath , tables(i)  )
			POP_MVC.Arr.push ret,tables(i)
		next
		TruncateAccTables = ret
		POP_MVC.config("DB_TYPE") =  db_type
		POP_MVC.config("DB_PATH") = dbPath
	End Property		

	'清空 mysql 的所有数据表
	Public Property Get TruncateMysqlTables( ByRef dbName )
		dim db_type,db_name,i
		db_type = POP_MVC.config("DB_TYPE")	
		db_name = POP_MVC.config("DB_NAME")	

		POP_MVC.config("DB_TYPE") = "mysql"
		POP_MVC.config("DB_NAME") = dbName
		set tables = P_("mysql").getTables
		tables = tables.items
		
		for i = 0 to ubound(tables)
			 P_("db").execute( "TRUNCATE TABLE " & tables(i) & " IF EXISTS" )
		next
		
		TruncateMysqlTables = tables
		
		POP_MVC.config("DB_TYPE") =  db_type
		POP_MVC.config("DB_NAME") = db_name
	End Property
	
	'清空 sqlite3 的所有数据表
	Public Property Get TruncateSqliteTables( ByRef dbPath )
		dim db_type,db_path,i,ret
		db_type = POP_MVC.config("DB_TYPE")	
		db_path = POP_MVC.config("DB_PATH")	

		POP_MVC.config("DB_TYPE") = "sqlite3"
		POP_MVC.config("DB_PATH") = dbPath
		set tables = P_("sqlite3").getTables
		tables = tables.items
		ret = Array()
		for i = 0 to ubound(tables)
			if NOT POP_MVC.String.iStartsWith( tables(i) , "sqlite_" ) then
				P_("db").execute( "DELETE FROM '" & tables(i) & "'" )
				P_("db").execute( "DELETE FROM sqlite_sequence WHERE name = '" & tables(i) & "'" )
				P_("db").execute( "UPDATE sqlite_sequence SET seq = 0 WHERE name = '" & tables(i) & "'" )
				POP_MVC.Arr.push ret,tables(i)
			end if
		next
		TruncateSqliteTables = ret
		POP_MVC.config("DB_TYPE") =  db_type
		POP_MVC.config("DB_PATH") = dbPath
	End Property	
	
	'删除 access 的所有数据表
	Public Property Get DropAccTables( ByRef dbPath )
		dim tables,i
		dim db_type,db_path
		db_type = POP_MVC.config("DB_TYPE")	
		db_path = POP_MVC.config("DB_PATH")	

		POP_MVC.config("DB_TYPE") = "access"
		POP_MVC.config("DB_PATH") = dbPath
		
		if not isEmpty(P_("access").getTables) then
			set tables = P_("access").getTables
			tables = tables.items
			for i = 0 to ubound(tables)
				P_("db").execute( "DROP TABLE " & tables(i) )		 
			next
			DropAccTables = tables
		else
			DropAccTables = Array()
		end if
		
		POP_MVC.config("DB_TYPE") =  db_type
		POP_MVC.config("DB_PATH") =  db_path
	End Property	
	
	'删除 access 的所有数据表
	Public Property Get DropAccTable( ByRef dbPath , table_name )
		on error resume next
		dim tables,i
		dim db_type,db_path
		db_type = POP_MVC.config("DB_TYPE")	
		db_path = POP_MVC.config("DB_PATH")	

		POP_MVC.config("DB_TYPE") = "access"
		POP_MVC.config("DB_PATH") = dbPath
		
		if not isEmpty(P_("access").getTables) then
			set tables = P_("access").getTables
			tables = tables.items
			if POP_MVC.Arr.iExists( tables,table_name ) then
				DropAccTable = P_("db").execute( "DROP TABLE " & table_name )
			end if
		end if

		POP_MVC.config("DB_TYPE") =  db_type
		POP_MVC.config("DB_PATH") =  db_path
	End Property
	
	
	'删除 mysql 的所有数据表
	Public Property Get DropMysqlTables( ByRef dbName )
		dim tables,i
		dim db_type,db_name
		db_type = POP_MVC.config("DB_TYPE")	
		db_name = POP_MVC.config("DB_NAME")

		POP_MVC.config("DB_TYPE") = "mysql"
		POP_MVC.config("DB_NAME") = dbName
		set tables = P_("mysql").getTables
		tables = tables.items
		
		for i = 0 to ubound(tables)
			 P_("db").execute( "DROP TABLE IF EXISTS " & tables(i) )
		next
		DropMysqlTables = tables
		POP_MVC.config("DB_TYPE") =  db_type
		POP_MVC.config("DB_NAME") = db_name
	End Property

	'删除 sqlite3 的所有数据表
	Public Property Get DropSqliteTables( ByRef dbPath )
		dim tables,i,ret
		dim db_type,db_path
		
		if not POP_MVC.File.isFile(dbPath) then
			exit Property
		end if
		
		db_type = POP_MVC.config("DB_TYPE")	
		db_path = POP_MVC.config("DB_PATH")	

		POP_MVC.config("DB_TYPE") = "sqlite3"
		POP_MVC.config("DB_PATH") = dbPath
		set tables = P_("sqlite3").getTables
		tables = tables.items
		ret = Array()
		for i = 0 to ubound(tables)
			if NOT POP_MVC.String.iStartsWith( tables(i) , "sqlite_" ) then
				P_("db").execute( "DELETE FROM '" & tables(i) & "'" )
				P_("db").execute( "DELETE FROM sqlite_sequence WHERE name = '" & tables(i) & "'" )
				P_("db").execute( "UPDATE sqlite_sequence SET seq = 0 WHERE name = '" & tables(i) & "'" )
				POP_MVC.Arr.push ret,tables(i)
			end if			 
		next
		DropSqliteTables = ret
		POP_MVC.config("DB_TYPE") =  db_type
		POP_MVC.config("DB_PATH") =  db_path
	End Property	

	'将 access 数据库转化成 sqlite3 数据库，结构连数据
	Function acc2sqliteWithData( ByVal accPath, ByVal sqlitePath , ByVal src_db_prefix , ByVal dst_db_prefix )
		Server.ScriptTimeOut=9999999
		on error resume next
		Call acc2sqlite4desc(  accPath, sqlitePath , src_db_prefix , dst_db_prefix )
		dim tables,i,cnt
		dim db_path,db_type
		
		'先保存配置信息
		db_path = POP_MVC.config("DB_PATH")
		db_type = POP_MVC.config("DB_TYPE")	
		if isNul(accPath) then
			accPath = POP_MVC.config("DB_PATH")
		end if

		'修改配置
		POP_MVC.config("DB_PATH") = accPath
		POP_MVC.config("DB_TYPE") = "access"
		
		set tables = P_("access").getTables
		
		tables = tables.items
		for i = 0 to ubound(tables)
			cnt = B_( array( tables(i) , "access" , accPath ) ).count
			if cnt > 0 then
				if isEmpty( acc2sqliteWithData ) then
					acc2sqliteWithData = 0
				end if
				acc2sqliteWithData = acc2sqliteWithData + cnt
				'Call acc2sqlite4desc4table(  tables(i), accPath, sqlitePath,src_db_prefix , dst_db_prefix )
				Call acc2sqliteWithData4table(  tables(i), accPath, sqlitePath,src_db_prefix , dst_db_prefix )
			end if		
		next
		
		'还原配置
		POP_MVC.config("DB_PATH") = db_path
		POP_MVC.config("DB_TYPE") = db_type
	End Function
	
	'将 mysql 数据库转化成 access 数据库，结构连数据
	Function mysql2accWithData( ByVal dbName,  ByVal dbPath, ByVal src_db_prefix , ByVal dst_db_prefix )
		mysql2accWithData = mysql2accOrSqliteWithData( dbName, "access" , dbPath, src_db_prefix , dst_db_prefix )
	End Function
	
	'将 mysql 数据库转化成 access 数据库，结构连数据
	Function mysql2sqliteWithData( ByVal dbName,ByVal dbPath,ByVal src_db_prefix,ByVal dst_db_prefix )
		mysql2sqliteWithData = mysql2accOrSqliteWithData( dbName, "sqlite3" , dbPath, src_db_prefix , dst_db_prefix )
	End Function
	
	'将 mysql 数据库转化成 access 数据库，结构连数据
	Function mysql2accOrSqliteWithData( ByVal dbName, ByVal dbType , ByVal dbPath, ByVal src_db_prefix , ByVal dst_db_prefix )
		Server.ScriptTimeOut=9999999
		on error resume next
		dim tables,i
		dim db_name,db_path,db_type,db_prefix,tableName,cnt
		
		Call mysql2accOrSqlite4desc( dbName ,  dbType , dbPath , src_db_prefix , dst_db_prefix )
		
		'先保存配置信息
		db_name = POP_MVC.config("DB_NAME")
		db_path = POP_MVC.config("DB_PATH")
		db_type = POP_MVC.config("DB_TYPE")	
		db_prefix = POP_MVC.config("DB_PREFIX")	
		if isNul(dbName) then
			dbName = POP_MVC.config("DB_NAME")
		end if
		
		if isNul(dbPath) then
			dbPath = POP_MVC.config("DB_PATH")
		end if		

		'修改配置
		POP_MVC.config("DB_NAME") = dbName
		POP_MVC.config("DB_TYPE") = "mysql"
		POP_MVC.config("DB_PREFIX") = src_db_prefix
		set tables = P_("mysql").getTables
		
		tables = tables.items
			
		for i = 0 to ubound(tables)
			tableName = tables(i)
			tableName = mid( tableName , len( src_db_prefix ) + 1 )
			POP_MVC.config("DB_PREFIX") = src_db_prefix
			cnt = B_( array( tableName , "mysql" , dbName ) ).count
			if cnt > 0 then
				if isEmpty( mysql2accOrSqliteWithData ) then
					mysql2accOrSqliteWithData = 0
				end if
				mysql2accOrSqliteWithData = mysql2accOrSqliteWithData + cnt
				Call mysql2accOrSqliteWithData4table( tableName,  dbName, dbType, dbPath, src_db_prefix ,dst_db_prefix )
			end if
		next
		
		'还原配置
		POP_MVC.config("DB_NAME") = db_name
		POP_MVC.config("DB_PATH") = db_path
		POP_MVC.config("DB_TYPE") = db_type
		POP_MVC.config("DB_PREFIX") = db_prefix
	End Function		
	
	'将 access 数据库转化成 mysql 数据库，结构连数据
	Function acc2mysqlWithData( ByVal accPath,ByVal dbName, ByVal src_db_prefix , ByVal dst_db_prefix )
		acc2mysqlWithData = accOrSqlite2mysqlWithData( "access" , accPath, dbName, src_db_prefix , dst_db_prefix )
	End Function
	
	'将 sqlite3 数据库转化成 mysql 数据库，结构连数据
	Function sqlite2mysqlWithData( ByVal sqlitePath,ByVal dbName , ByVal src_db_prefix , ByVal dst_db_prefix )
		sqlite2mysqlWithData = accOrSqlite2mysqlWithData( "sqlite3" , sqlitePath,  dbName, src_db_prefix , dst_db_prefix )
	End Function	
	
	
	'将acc数据库转化成mysql数据库，结构连数据
	Function accOrSqlite2mysqlWithData( ByRef dbType , ByVal dbPath, ByVal dbName, ByRef src_db_prefix , ByRef dst_db_prefix )
		Server.ScriptTimeOut=9999999
		on error resume next
		
		Call accOrSqlite2mysql4desc( dbType , dbPath, dbName, src_db_prefix , dst_db_prefix )

		dim tables,i,bool,cnt
		dim db_path,db_type,db_name

		bool = true
		
		'先保存配置信息
		db_name = POP_MVC.config("DB_NAME")
		db_path = POP_MVC.config("DB_PATH")
		db_type = POP_MVC.config("DB_TYPE")	
		if isNul(dbPath) then
			dbPath = POP_MVC.config("DB_PATH")
		else
			bool = false		
		end if
		
		if bool then
			'修改配置
			POP_MVC.config("DB_PATH") = dbPath
			POP_MVC.config("DB_TYPE") = dbType
		end if
		
		set tables = P_(dbType).getTables
		
		tables = tables.items
		
		POP_MVC.config( "DB_TYPE" ) = "mysql"	
		POP_MVC.config( "DB_NAME" ) = dbName
		P_("db").execute( "CREATE DATABASE IF NOT EXISTS " & dbName & " default character set utf8 COLLATE utf8_general_ci"  )
		
		POP_MVC.config("DB_TYPE") = dbType
		
		for i = 0 to ubound(tables)	
			if POP_MVC.config( "DB_TYPE" ) <> "sqlite3" OR NOT POP_MVC.String.iStartsWith( tables(i) , "sqlite_" ) then 
				cnt = B_( array( tables(i) , dbType , dbPath ) ).count
				if cnt > 0 then
					if isEmpty( accOrSqlite2mysqlWithData ) then
						accOrSqlite2mysqlWithData = 0
					end if
					accOrSqlite2mysqlWithData = accOrSqlite2mysqlWithData + cnt
					Call accOrSqlite2mysqlWithData4table( dbType , tables(i) , dbPath,dbName, src_db_prefix,dst_db_prefix )
				end if
			end if
		next
		
		'还原配置
		POP_MVC.config("DB_NAME") = db_name
		POP_MVC.config("DB_PATH") = db_path
		POP_MVC.config("DB_TYPE") = db_type
	End Function
	
	'将 access 数据库的某个表转化成 mysql 数据库，结构连数据
	Function acc2mysqlWithData4table( ByVal tableName,ByVal dbPath, ByVal dbName, ByVal src_db_prefix,ByVal dst_db_prefix)
		acc2mysqlWithData4table = accOrSqlite2mysqlWithData4table( "access" , tableName , dbPath ,dbName, src_db_prefix, dst_db_prefix )
	End Function
	
	'将 sqlite3 数据库的某个表转化成 mysql 数据库，结构连数据
	Function sqlite2mysqlWithData4table(ByVal tableName,ByVal dbPath,ByVal dbName, ByVal src_db_prefix,ByVal dst_db_prefix)
		sqlite2mysqlWithData4table = accOrSqlite2mysqlWithData4table( "sqlite3" , tableName , dbPath , dbName, src_db_prefix, dst_db_prefix )
	End Function	
	
	'将 access/sqlite3 数据库的某个表转化成 mysql 数据库，结构连数据
	Function accOrSqlite2mysqlWithData4table( ByRef dbType, ByVal tableName,ByVal dbPath,ByVal dbName, ByVal src_db_prefix,ByVal dst_db_prefix)
		Server.ScriptTimeOut=50000
		on error resume next
		
		if dbType = "access" then
			Call acc2mysql4desc4table( tableName, dbPath, dbName, src_db_prefix , dst_db_prefix )
		elseif dbType = "sqlite3" then
			Call sqlite2mysql4desc4table( tableName, dbPath, dbName, src_db_prefix , dst_db_prefix )
		end if

		dim data,db_path,db_name,db_type,db_prefix,page,pagesize
		
		'先保存配置信息
		db_name = POP_MVC.config("DB_NAME")
		db_path = POP_MVC.config("DB_PATH")
		db_type = POP_MVC.config("DB_TYPE")	
		db_prefix = POP_MVC.config("DB_PREFIX")	
		if isNul(dbPath) then
			dbPath = POP_MVC.config("DB_PATH")
		end if
		
		
		pagesize = 200
		page = 1			
		do 
			'修改配置
			POP_MVC.config("DB_PATH") = dbPath
			POP_MVC.config("DB_TYPE") = dbType			
			POP_MVC.config( "DB_PREFIX" ) = src_db_prefix	
			
			set data = B_( array( tableName , dbType , dbPath ) ).page( Array( page,pagesize ) ).getAll
			
			if data.count > 0 then
				POP_MVC.config("DB_TYPE") = "mysql"
				POP_MVC.config("DB_NAME") = dbName
				POP_MVC.config("DB_PREFIX") = dst_db_prefix
				
				B_( array( tableName , "mysql") ).prikey = ""
				B_( array( tableName , "mysql") ).exec("SET NAMES GBK")
				B_( array( tableName , "mysql") ).exec("SET SQL_MODE = ""NO_AUTO_VALUE_ON_ZERO""")
				if page = 1 then
					Call B_( array( tableName , "mysql")).data(data).multiAdd(1)
				else
					Call B_( array( tableName , "mysql")).data(data).multiAdd(0)
				end if				
			end if
			if data.count < pagesize then
				Exit Do
			end if
			page = page + 1
		loop
		
		'还原配置
		POP_MVC.config("DB_NAME") = db_name
		POP_MVC.config("DB_PATH") = db_path
		POP_MVC.config("DB_TYPE") = db_type
		POP_MVC.config("DB_PREFIX") = db_prefix
		set data = nothing
	End Function
	
	
	'将一个表中的数据导入到另一个表，要求表名前缀相同
	Function table2table( ByRef srcArr,ByRef srcPrefix, ByRef dstArr,ByRef dstPrefix)
		Server.ScriptTimeOut=50000
		on error resume next
		dim data,page,pagesize,dbType,prikey
		dbPrefix = POP_MVC.config("DB_PREFIX")
		pagesize = 200
		page = 1		

		prikey = B_( srcArr ).getPK		
		do 
			POP_MVC.config("DB_PREFIX") = srcPrefix
			
			if prikey = "" then
				set data = B_( srcArr ).page( Array( page,pagesize ) ).getAll
			else
				set data = B_( srcArr ).page( Array( page,pagesize ) ).order( prikey & " ASC" ).getAll
			end if
			
			if data.count > 0 then
				if dstArr(1) = "mysql" then
					B_( dstArr ).prikey = ""
					B_( dstArr ).exec("SET NAMES GBK")
					B_( dstArr ).exec("SET SQL_MODE = ""NO_AUTO_VALUE_ON_ZERO""")
				end if				

				POP_MVC.config("DB_PREFIX") = dstPrefix
				if page = 1 then
					Call B_( dstArr ).data(data).multiAdd(1)
				else
					Call B_( dstArr ).data(data).multiAdd(0)
				end if				
			end if
			if data.count < pagesize then
				Exit Do
			end if
			page = page + 1
		loop
		POP_MVC.config("DB_PREFIX") = dbPrefix
		set data = nothing
	End Function
	
	'将acc数据库转化成mysql数据库，结构连数据
	Function mysql2accWithData4table(ByVal tableName,ByVal dbName, ByVal dbPath,ByVal src_db_prefix,ByVal dst_db_prefix)
		mysql2accWithData4table = mysql2accOrSqliteWithData4table( tableName,dbName,"access", dbPath,src_db_prefix,dst_db_prefix)
	End Function
	
	'将acc数据库转化成mysql数据库，结构连数据
	Function mysql2sqliteWithData4table(ByVal tableName,ByVal dbName, ByVal dbPath,ByVal src_db_prefix,ByVal dst_db_prefix)
		mysql2sqliteWithData4table = mysql2accOrSqliteWithData4table( tableName,dbName,"sqlite3", dbPath,src_db_prefix,dst_db_prefix)
	End Function
	
	'将 mysql 数据库转化成 acc/sqlite3 数据库，结构连数据
	Function mysql2accOrSqliteWithData4table(ByVal tableName,ByVal dbName,ByVal dbType, ByVal dbPath,ByVal src_db_prefix,ByVal dst_db_prefix)
		Server.ScriptTimeOut=50000
		on error resume next

		dim data,db_name,db_path,db_type,db_prefix,tables,prefix,page,pagesize
		
		'先保存配置信息
		db_name = POP_MVC.config("DB_NAME")
		db_path = POP_MVC.config("DB_PATH")
		db_type = POP_MVC.config("DB_TYPE")	
		db_prefix = POP_MVC.config("DB_PREFIX")	
		if isNul(dbPath) then
			dbPath = POP_MVC.config("DB_PATH")
		end if
		
		if isNul(dbName) then
			dbName = POP_MVC.config("DB_NAME")
		end if	
		
		pagesize = 200
		page = 1			
		do 
			'修改配置
			POP_MVC.config("DB_NAME") = dbName
			POP_MVC.config("DB_TYPE") = "mysql"		
			POP_MVC.config( "DB_PREFIX" ) = src_db_prefix
			
			if POP_MVC.String.iStartsWith( tableName , src_db_prefix ) then
				tableName = mid( tableName , len( src_db_prefix ) + 1  )
			end if
			
			prefix = B_( array( tableName , "mysql" , dbName ) ).getPK
			if prefix = "" then
				set data = B_( array( tableName , "mysql" , dbName ) ).page( Array( page,pagesize ) ).getAll
			else
				set data = B_( array( tableName , "mysql" , dbName ) ).page( Array( page,pagesize ) ).order( prefix & " ASC" ).getAll
			end if

			if data.count > 0 then
				POP_MVC.config( "DB_PREFIX" ) = dst_db_prefix
				POP_MVC.config("DB_PATH") = dbPath
				POP_MVC.config("DB_TYPE") = dbType
				if not isEmpty(P_(dbType).getTables) then
					set tables = P_(dbType).getTables
					tables = tables.items
				else
					tables = Array()
				end if
				if  NOT POP_MVC.Arr.iExists( tables, dst_db_prefix & tableName ) then
					if dbType = "access" then
						P_("db").execute( getMysql2AccSql4table( tableName ,  dbName, src_db_prefix, dst_db_prefix ) )
					elseif dbType = "sqlite3" then
						P_("db").execute( getMysql2SqliteSql4table( tableName ,  dbName, src_db_prefix, dst_db_prefix ) )
					end if					
				end if
				POP_MVC.config( "DB_PREFIX" ) = dst_db_prefix
				if page = 1 then
					Call B_( array( tableName , dbType , dbPath)).data(data).multiAdd(1)
				else
					Call B_( array( tableName , dbType , dbPath)).data(data).multiAdd(0)
				end if
			end if
			if data.count < pagesize then
				exit do
			end if
			page = page + 1
		loop
		
		'还原配置
		POP_MVC.config("DB_PATH") = db_path
		POP_MVC.config("DB_TYPE") = db_type
		POP_MVC.config("DB_PREFIX") = db_prefix
		set data = nothing
	End Function
	
	'将 mysql 数据库转化成 access 数据库，只要结构，如果存在则不去创建
	Function mysql2acc4desc4table(ByVal tableName,ByVal dbName, ByVal dbType, ByVal dbPath,ByVal src_db_prefix,ByVal dst_db_prefix)
		mysql2acc4desc4table =  mysql2accOrSqlite4desc4table( tableName,dbName,"access",dbPath,src_db_prefix,dst_db_prefix )
	End Function
	
	'将 mysql 数据库转化成 sqlite3 数据库，只要结构，如果存在则不去创建
	Function mysql2sqlite4desc4table(ByVal tableName,ByVal dbName, ByVal dbType, ByVal dbPath,ByVal src_db_prefix,ByVal dst_db_prefix)
		mysql2sqlite4desc4table =  mysql2accOrSqlite4desc4table( tableName,dbName,"sqlite3",dbPath,src_db_prefix,dst_db_prefix )
	End Function
	
	
	'将 mysql 数据库转化成 access 或者 sqlite3 数据库，只要结构，如果存在则不去创建
	Function mysql2accOrSqlite4desc4table(ByVal tableName,ByVal dbName, ByVal dbType, ByVal dbPath,ByVal src_db_prefix,ByVal dst_db_prefix)
		Server.ScriptTimeOut=50000
		on error resume next

		dim data,db_name,db_path,db_type,db_prefix,tables
		
		'先保存配置信息
		db_path = POP_MVC.config("DB_PATH")
		db_type = POP_MVC.config("DB_TYPE")	
		db_prefix = POP_MVC.config("DB_PREFIX")	
		if isNul(dbName) then
			dbName = POP_MVC.config("DB_NAME")
		end if
		
		if isNul(dbPath) then
			dbPath = POP_MVC.config("DB_PATH")
		end if		
		
		if POP_MVC.String.iStartsWith( tableName , src_db_prefix ) then
			tableName = mid( tableName , len( src_db_prefix ) + 1  )
		end if

		POP_MVC.config( "DB_PREFIX" ) = dst_db_prefix
		POP_MVC.config("DB_PATH") = dbPath
		if not isEmpty(P_(dbType).getTables) then
			set tables = P_(dbType).getTables
			tables = tables.items
		else
			tables = Array()
		end if
		if  NOT POP_MVC.Arr.iExists( tables, dst_db_prefix & tableName ) then
			if dbType = "access" then
				P_("db").execute( getMysql2AccSql4table( tableName ,  dbName, src_db_prefix, dst_db_prefix ) )
			elseif dbType = "sqlite3" then
				P_("db").execute( getMysql2SqliteSql4table( tableName ,  dbName, src_db_prefix, dst_db_prefix ) )
			end if
		end if
		
		'还原配置
		POP_MVC.config("DB_PATH") = db_path
		POP_MVC.config("DB_TYPE") = db_type
		POP_MVC.config("DB_PREFIX") = db_prefix
	End Function
	
	'将acc数据库转化成mysql数据库，结构连数据
	Function acc2sqliteWithData4table( ByVal accTableName,ByVal accPath,ByVal sqlitePath, ByVal src_db_prefix,ByVal dst_db_prefix)
		Server.ScriptTimeOut=50000
		on error resume next
		
		Call acc2sqlite4desc4table( accTableName, accPath, sqlitePath,src_db_prefix , dst_db_prefix )

		dim data,db_path,db_type,db_prefix,page,pagesize
		
		'先保存配置信息
		db_path = POP_MVC.config("DB_PATH")
		db_type = POP_MVC.config("DB_TYPE")	
		db_prefix = POP_MVC.config("DB_PREFIX")	
		if isNul(accPath) then
			accPath = POP_MVC.config("DB_PATH")
		end if
		
		pagesize = 200
		page = 1			
		do 
			'修改配置
			POP_MVC.config("DB_PATH") = accPath
			POP_MVC.config("DB_TYPE") = "access"
			
			POP_MVC.config( "DB_PREFIX" ) = src_db_prefix
			
			set data = B_( array( accTableName , "access" , accPath ) ).page( Array( page,pagesize ) ).getAll

			if data.count > 0 then
				POP_MVC.config( "DB_PREFIX" ) = dst_db_prefix
				if page = 1 then
					Call B_( array( accTableName , "sqlite3" , sqlitePath)).data(data).multiAdd(1)
				else
					Call B_( array( accTableName , "sqlite3" , sqlitePath)).data(data).multiAdd(0)
				end if
			end if
			
			if data.count < pagesize then
				exit do
			end if
			
			page = page + 1
		loop
		
		'还原配置
		POP_MVC.config("DB_PATH") = db_path
		POP_MVC.config("DB_TYPE") = db_type
		POP_MVC.config("DB_PREFIX") = db_prefix
		set data = nothing
	End Function	

	'针对数据库，将acc数据库的某个表转化成mysql数据库的某个表
	'mysql的配置参数在配置中进行配置，并且要求mysql中已经有建好的数据库 DB_NAME
	'dbPath 为 access 的数据库路径，如果为空，则说明当前配置为 access ，并取当前数据库路径
	'src_db_prefix为acc的表名前缀
	'dst_db_prefix为mysql的表名前缀，如果为""，则不加前缀
	'如果src_db_prefix与dst_db_prefix都设置为空字符串，则可以将所有表同名复制
	Function acc2mysql4desc4table( ByVal tableName,ByVal dbPath, ByVal dbName, ByVal src_db_prefix , ByVal dst_db_prefix )
		acc2mysql4desc4table = sqliteOrAcc2mysql4desc4table( "access",tableName,dbPath,dbName,src_db_prefix,dst_db_prefix  )
	End Function

	'针对数据库，将 sqlite3 数据库的某个表转化成 mysql 数据库的某个表
	'mysql的配置参数在配置中进行配置，并且要求mysql中已经有建好的数据库 DB_NAME
	'dbPath 为 sqlite3 的数据库路径，如果为空，则说明当前配置为 sqlite3，并取当前数据库路径
	'src_db_prefix为acc的表名前缀
	'dst_db_prefix为mysql的表名前缀，如果为""，则不加前缀
	'如果src_db_prefix与dst_db_prefix都设置为空字符串，则可以将所有表同名复制
	Function sqlite2mysql4desc4table( ByVal tableName,ByVal dbPath, ByVal dbName ,ByVal src_db_prefix , ByVal dst_db_prefix )
		sqlite2mysql4desc4table = sqliteOrAcc2mysql4desc4table( "sqlite3",tableName,dbPath,dbName,src_db_prefix,dst_db_prefix  )
	End Function	
	
	
	'针对数据库，将 sqlite3 数据库的某个表转化成 mysql 数据库的某个表
	'mysql的配置参数在配置中进行配置，并且要求mysql中已经有建好的数据库 DB_NAME
	'dbPath 为 sqlite3 的数据库路径，如果为空，则说明当前配置为 sqlite3，并取当前数据库路径
	'src_db_prefix为acc的表名前缀
	'dst_db_prefix为mysql的表名前缀，如果为""，则不加前缀
	'如果src_db_prefix与dst_db_prefix都设置为空字符串，则可以将所有表同名复制
	Function sqliteOrAcc2mysql4desc4table(ByVal dbType, ByVal tableName,ByVal dbPath, ByVal dbName ,ByVal src_db_prefix , ByVal dst_db_prefix )
		dim db_name,db_path,db_type,sql
		
		'先保存配置信息
		db_name = POP_MVC.config("DB_NAME")
		db_path = POP_MVC.config("DB_PATH")
		db_type = POP_MVC.config("DB_TYPE")	
		if isNul(dbPath) then
			dbPath = POP_MVC.config("DB_PATH")
		end if			
		
		POP_MVC.config("DB_TYPE") = "mysql"
		POP_MVC.config("DB_NAME") = dbName
		
		'如果数据库不存在，则创建数据库
		P_("db").execute( "CREATE DATABASE IF NOT EXISTS " & dbName & " default character set utf8 COLLATE utf8_general_ci" )		
		P_("db").execute( "SET NAMES GBK" )		
		P_("db").execute( "USE " & dbName )		
		
		if dbType = "access" then
			sql = getAcc2MysqlSql4table( tableName, src_db_prefix , dst_db_prefix )
		elseif dbType = "sqlite3" then
			sql = getSqlite2MysqlSql4table( tableName, src_db_prefix , dst_db_prefix )
		end if
		
		P_("db").execute( sql )
		
		'还原配置
		POP_MVC.config("DB_NAME") = db_name
		POP_MVC.config("DB_PATH") = db_path
		POP_MVC.config("DB_TYPE") = db_type
	End Function		
	
	'针对数据库，将 access 数据库的某个表转化成 sqlite3 数据库的某个表
	'accPath为acc的数据库路径，如果为空，则说明当前配置为acc，并取当前数据库路径
	'src_db_prefix为acc的表名前缀
	'dst_db_prefix为mysql的表名前缀，如果为""，则不加前缀
	'如果src_db_prefix与dst_db_prefix都设置为空字符串，则可以将所有表同名复制
	Function acc2sqlite4desc4table( ByVal accTableName,ByVal accPath,ByVal sqlitePath, ByVal src_db_prefix , ByVal dst_db_prefix )
		dim db_path,db_type,sql
		
		'先保存配置信息
		db_path = POP_MVC.config("DB_PATH")
		db_type = POP_MVC.config("DB_TYPE")	
		if isNul(accPath) then
			accPath = POP_MVC.config("DB_PATH")
		end if		
		
		sql = getAcc2SqliteSql4table( accTableName, accPath, src_db_prefix , dst_db_prefix )
		
		POP_MVC.config("DB_TYPE") = "sqlite3"
		POP_MVC.config("DB_PATH") = sqlitePath
		P_("db").execute( sql )
		
		'还原配置
		POP_MVC.config("DB_PATH") = db_path
		POP_MVC.config("DB_TYPE") = db_type
	End Function
	
	'针对数据库，将acc数据库转化成mysql数据库
	'mysql的配置参数在配置中进行配置，并且要求mysql中已经有建好的数据库 DB_NAME
	'accPath为acc的数据库路径，如果为空，则说明当前配置为acc，并取当前数据库路径
	'src_db_prefix为acc的表名前缀
	'dst_db_prefix为mysql的表名前缀，如果为""，则不加前缀
	'如果src_db_prefix与dst_db_prefix都设置为空字符串，则可以将所有表同名复制
	Function acc2mysql4desc( ByVal accPath, ByVal dbName, ByRef src_db_prefix , ByRef dst_db_prefix )
		acc2mysql4desc = accOrSqlite2mysql4desc( "access" , accPath, dbName, src_db_prefix , dst_db_prefix )
	End Function
	
	'针对数据库，将 sqlite3 数据库转化成mysql数据库
	'mysql的配置参数在配置中进行配置，并且要求mysql中已经有建好的数据库 DB_NAME
	'sqlitePath为 sqlite3 的数据库路径，如果为空，则说明当前配置为 sqlite3 ，并取当前数据库路径
	'src_db_prefix为acc的表名前缀
	'dst_db_prefix为mysql的表名前缀，如果为""，则不加前缀
	'如果src_db_prefix与dst_db_prefix都设置为空字符串，则可以将所有表同名复制
	Function sqlite2mysql4desc( ByVal sqlitePath, ByVal dbName, ByRef src_db_prefix , ByRef dst_db_prefix )
		sqlite2mysql4desc = accOrSqlite2mysql4desc( "sqlite3" , sqlitePath,dbName, src_db_prefix , dst_db_prefix )
	End Function		
	
	
	'针对数据库，将 access/sqlite3 数据库转化成 mysql 数据库，只要结构不要数据
	'mysql的配置参数在配置中进行配置，如果 mysql 数据库不存在，则尝试创建
	'dbType 请选择 access 或者 sqlite3
	'dbPath 为 access/sqlite3 的数据库路径，如果为空，则说明当前配置为 access/sqlite3 ，并取当前数据库路径
	'src_db_prefix 为 access 的表名前缀
	'dst_db_prefix 为 mysql 的表名前缀，如果为""，则不加前缀
	'如果src_db_prefix与dst_db_prefix都设置为空字符串，则可以将所有表同名复制
	Function accOrSqlite2mysql4desc( ByRef dbType, ByVal dbPath,ByVal dbName, ByRef src_db_prefix , ByRef dst_db_prefix )
		dim db_name,db_path,db_type,sqls,i
		
		bool = true
		
		'先保存配置信息
		db_name = POP_MVC.config("DB_NAME")
		db_path = POP_MVC.config("DB_PATH")
		db_type = POP_MVC.config("DB_TYPE")	
		if isNul(dbPath) then
			dbPath = POP_MVC.config("DB_PATH")
		else
			bool = false		
		end if	

		if dbType = "access" then		
			sqls = getAcc2mysqlSql4Db( dbPath , src_db_prefix , dst_db_prefix  )
		elseif dbType = "sqlite3" then
			sqls = getSqlite2mysqlSql4Db( dbPath , src_db_prefix , dst_db_prefix  )
		end if

		POP_MVC.config("DB_TYPE") = "mysql"
		POP_MVC.config("DB_NAME") = dbName
		
		'如果数据库不存在，则创建数据库
		P_("db").execute( "CREATE DATABASE IF NOT EXISTS " & dbName & " default character set utf8 COLLATE utf8_general_ci" )
		P_("db").execute( "USE `" & dbName & "`" )
		P_("db").execute( "SET NAMES GBK" )
		
		for i = 0 to ubound( sqls )
			accOrSqlite2mysql4desc = P_("db").execute( sqls(i) )
		next
		
		'还原配置
		POP_MVC.config("DB_NAME") = db_name
		POP_MVC.config("DB_PATH") = db_path
		POP_MVC.config("DB_TYPE") = db_type
	End Function

	'针对数据库，将 mysql 数据库转化成 access 数据库
	'dbName 为 mysql 数据库真实存在的一个库名，mysql的其他配置参数在配置中进行配置
	'dbPath 为 access 的数据库路径，如果不存在，系统会自动创建
	'src_db_prefix为 mysql 的表名前缀
	'dst_db_prefix为 access 的表名前缀，如果为""，则不加前缀
	'如果src_db_prefix与dst_db_prefix都设置为空字符串，则可以将所有表同名复制
	Function mysql2acc4desc( ByVal dbName, ByVal dbPath , ByVal src_db_prefix , ByVal dst_db_prefix )
		mysql2acc4desc = mysql2accOrSqlite4desc( dbName, "access" , dbPath , src_db_prefix , dst_db_prefix )
	End Function
	
	'针对数据库，将 mysql 数据库转化成 sqlite3 数据库
	'dbName 为 mysql 数据库真实存在的一个库名，mysql的其他配置参数在配置中进行配置
	'dbPath 为 sqlite3 的数据库路径，如果不存在，系统会自动创建
	'src_db_prefix为 mysql 的表名前缀
	'dst_db_prefix为 sqlite3 的表名前缀，如果为""，则不加前缀
	'如果src_db_prefix与dst_db_prefix都设置为空字符串，则可以将所有表同名复制
	Function mysql2sqlite4desc( ByVal dbName, ByVal dbPath , ByVal src_db_prefix , ByVal dst_db_prefix )
		mysql2sqlite4desc = mysql2accOrSqlite4desc( dbName, "sqlite3" , dbPath , src_db_prefix , dst_db_prefix )
	End Function

	'针对数据库，将 mysql 数据库转化成 access 数据库
	'dbName为 mysql 数据库真实存在的一个库名，mysql的其他配置参数在配置中进行配置
	'dbPath 为 access/sqlite3 的数据库路径，如果不存在，系统会自动创建
	'src_db_prefix为 mysql 的表名前缀
	'dst_db_prefix为 access 的表名前缀，如果为""，则不加前缀
	'如果src_db_prefix与dst_db_prefix都设置为空字符串，则可以将所有表同名复制
	Function mysql2accOrSqlite4desc( ByVal dbName, ByVal dbType , ByVal dbPath , ByVal src_db_prefix , ByVal dst_db_prefix )
		dim db_name,db_path,db_type,sqls,tables
		
		bool = true
		
		'先保存配置信息
		db_name = POP_MVC.config("DB_NAME")
		db_path = POP_MVC.config("DB_PATH")
		db_type = POP_MVC.config("DB_TYPE")	
		if isNul(dbName) then
			dbName = POP_MVC.config("DB_NAME")
		else
			bool = false		
		end if		
		
		if dbType = "access" then
			sqls = getMysql2accSql4Db( dbName , src_db_prefix , dst_db_prefix  )
		elseif dbType = "sqlite3" then
			sqls = getMysql2SqliteSql4Db( dbName , src_db_prefix , dst_db_prefix  )
		end if
	
		if not POP_MVC.file.isFile( dbPath ) then
			if dbType = "access" then
				POP_MVC.file.CopyFile  "Tpl/access_empty.mdb" ,dbPath 
			elseif dbType = "sqlite3" then
				POP_MVC.file.CopyFile  "Tpl/sqlite3_empty.db" ,dbPath 
			end if
		end if		

		POP_MVC.config("DB_PATH") = dbPath
		POP_MVC.config("DB_TYPE") = dbType
		
		for i = 0 to ubound( sqls )
			mysql2accOrSqlite4desc = P_("db").execute( sqls(i) )
		next		
		
		'还原配置
		POP_MVC.config("DB_PATH") = db_path
		POP_MVC.config("DB_TYPE") = db_type
		POP_MVC.config("DB_NAME") = db_name
	End Function		


	'针对数据库，将acc数据库转化成sqlite3数据库
	'accPath为acc的数据库路径，如果为空，则说明当前配置为acc，并取当前数据库路径
	'sqlite3Path为sqlite3数据库路径，如果不存在则自动创建
	'src_db_prefix为acc的表名前缀
	'dst_db_prefix为sqlite3的表名前缀，如果为""，则不加前缀
	'如果src_db_prefix与dst_db_prefix都设置为空字符串，则可以将所有表同名复制
	Function acc2sqlite4desc( ByVal accPath, ByVal sqlitePath , ByVal src_db_prefix , ByVal dst_db_prefix )
		dim db_path,db_type,sql
		
		bool = true
		
		'先保存配置信息
		db_path = POP_MVC.config("DB_PATH")
		db_type = POP_MVC.config("DB_TYPE")	
		if isNul(accPath) then
			accPath = POP_MVC.config("DB_PATH")
		else
			bool = false		
		end if		
		

		
		if not POP_MVC.file.isFile( sqlitePath ) then
			POP_MVC.file.CopyFile "Tpl/sqlite3_empty.db" ,sqlitePath 
		end if
		
		sql = join(getAcc2SqliteSql4Db( accPath , src_db_prefix , dst_db_prefix  ) , vbcrlf )

		POP_MVC.config("DB_PATH") = sqlitePath
		POP_MVC.config("DB_TYPE") = "sqlite3"
		
		acc2sqlite4desc = P_("db").execute( sql )
		
		'还原配置
		POP_MVC.config("DB_PATH") = db_path
		POP_MVC.config("DB_TYPE") = db_type
	End Function	
	
	
	'针对数据库，将 access 数据库转化成 sqlite3 的建表语句
	'accPath为acc的数据库路径，如果为空，则说明当前配置为 access ，并取当前数据库路径
	'src_db_prefix为acc的表名前缀
	'dst_db_prefix为sqlite3的表名前缀，如果为""，则不加前缀	
	Function getAcc2SqliteSql4Db (  ByVal accPath , ByVal src_db_prefix , ByVal dst_db_prefix  )
		getAcc2SqliteSql4Db = getAcc2SqliteOrMysqlSql4Db( "sqlite3" ,accPath , src_db_prefix , dst_db_prefix  )
	End Function

	'针对数据库，将 access 数据库转化成 mysql 的建表语句
	'accPath为acc的数据库路径，如果为空，则说明当前配置为 access ，并取当前数据库路径
	'src_db_prefix为acc的表名前缀
	'dst_db_prefix为sqlite3的表名前缀，如果为""，则不加前缀
	Function getAcc2MysqlSql4Db (  ByVal accPath , ByVal src_db_prefix , ByVal dst_db_prefix  )
		getAcc2MysqlSql4Db = getAcc2SqliteOrMysqlSql4Db( "mysql" ,accPath , src_db_prefix , dst_db_prefix  )
	End Function		
	
	'针对数据库，将 access 数据库转化成 sqlite3/mysql 的建表语句
	'accPath为 access 的数据库路径，如果为空，则说明当前配置为 access，并取当前数据库路径
	'src_db_prefix为acc的表名前缀
	'dst_db_prefix为sqlite3的表名前缀，如果为""，则不加前缀		
	Function getAcc2SqliteOrMysqlSql4Db( ByRef dbType, ByVal accPath , ByVal src_db_prefix , ByVal dst_db_prefix )
		dim tables,i,bool,ret
		dim db_path,db_type
		bool = true
		ret = Array()
		
		'先保存配置信息
		db_path = POP_MVC.config("DB_PATH")
		db_type = POP_MVC.config("DB_TYPE")			
		if isNul(accPath) then
			accPath = POP_MVC.config("DB_PATH")
		else
			bool = false
		end if

		'修改配置
		POP_MVC.config("DB_PATH") = accPath
		POP_MVC.config("DB_TYPE") = "access"
		
		set tables = P_("access").getTables

		tables = tables.items
		if dbType = "sqlite3" then
			for i = 0 to ubound(tables)
				POP_MVC.arr.push ret, getAcc2SqliteSql4table( tables(i) , accPath, src_db_prefix, dst_db_prefix )
			next
		elseif dbType = "mysql" then
			for i = 0 to ubound(tables)
				POP_MVC.arr.push ret, getAcc2MysqlSql4table( tables(i) , accPath, src_db_prefix, dst_db_prefix )
			next
		end if
		getAcc2SqliteOrMysqlSql4Db = ret
		
		'还原配置
		POP_MVC.config("DB_PATH") = db_path
		POP_MVC.config("DB_TYPE") = db_type
	End Function
	
	
	'针对数据库，将 sqlite3 数据库转化成 mysql 的建表语句
	'sqlitePath 为 sqlite3 的数据库路径，如果为空，则说明当前配置为 sqlite3 ，并取当前数据库路径
	'src_db_prefix为acc的表名前缀
	'dst_db_prefix为sqlite3的表名前缀，如果为""，则不加前缀
	Function getSqlite2MysqlSql4Db( ByVal sqlitePath , ByVal src_db_prefix , ByVal dst_db_prefix )
		dim tables,i,bool,ret
		dim db_path,db_type
		bool = true
		ret = Array()
		
		'先保存配置信息
		db_path = POP_MVC.config("DB_PATH")
		db_type = POP_MVC.config("DB_TYPE")			
		if isNul(sqlitePath) then
			sqlitePath = POP_MVC.config("DB_PATH")
		else
			bool = false
		end if

		'修改配置
		POP_MVC.config("DB_PATH") = sqlitePath
		POP_MVC.config("DB_TYPE") = "sqlite3"
		
		set tables = P_("sqlite3").getTables

		tables = tables.items
		for i = 0 to ubound(tables)
			if NOT POP_MVC.String.iStartsWith( tables(i) , "sqlite_" ) then
				POP_MVC.arr.push ret, getSqlite2MysqlSql4table( tables(i) ,  sqlitePath, src_db_prefix, dst_db_prefix )
			end if
		next
		getSqlite2MysqlSql4Db = ret
		
		'还原配置
		POP_MVC.config("DB_PATH") = db_path
		POP_MVC.config("DB_TYPE") = db_type
	End Function
	
	'针对数据库，将 Mysql 数据库转化成 Access 的建表语句
	'dbName 为 Mysql 的数据库名，如果为空，则说明当前配置为 Mysql ，并取当前数据库名
	'src_db_prefix为 Mysql 的表名前缀
	'dst_db_prefix为 access 的表名前缀，如果为""，则不加前缀
	Function getMysql2AccSql4Db( ByVal dbName , ByVal src_db_prefix , ByVal dst_db_prefix )
		getMysql2AccSql4Db = getMysql2AccOrSqliteSql4Db( dbName , "access", src_db_prefix , dst_db_prefix )
	End Function
	
	'针对数据库，将 Mysql 数据库转化成 sqlite3 的建表语句
	'dbName 为 Mysql 的数据库名，如果为空，则说明当前配置为 Mysql ，并取当前数据库名
	'src_db_prefix为 Mysql 的表名前缀
	'dst_db_prefix为 sqlite3 的表名前缀，如果为""，则不加前缀
	Function getMysql2SqliteSql4Db( ByVal dbName , ByVal src_db_prefix , ByVal dst_db_prefix )
		getMysql2SqliteSql4Db = getMysql2AccOrSqliteSql4Db( dbName , "sqlite3", src_db_prefix , dst_db_prefix )
	End Function
	
	'针对数据库，将 Mysql 数据库转化成 Access 的建表语句
	'dbName 为 Mysql 的数据库名，如果为空，则说明当前配置为 Mysql ，并取当前数据库名
	'src_db_prefix为 Mysql 的表名前缀
	'dst_db_prefix为 access 的表名前缀，如果为""，则不加前缀
	Function getMysql2AccOrSqliteSql4Db( ByVal dbName , ByVal dbType, ByVal src_db_prefix , ByVal dst_db_prefix )
		dim tables,i,bool,ret
		dim db_name,db_type
		bool = true
		ret = Array()
		
		'先保存配置信息
		db_name = POP_MVC.config("DB_NAME")
		db_type = POP_MVC.config("DB_TYPE")			
		if isNul(dbName) then
			dbName = POP_MVC.config("DB_NAME")
		else
			bool = false
		end if

		'修改配置
		POP_MVC.config("DB_NAME") = dbName
		POP_MVC.config("DB_TYPE") = "mysql"
		
		set tables = P_("mysql").getTables

		tables = tables.items
		if dbType = "access" then
			for i = 0 to ubound(tables)
				POP_MVC.arr.push ret, getMysql2AccSql4table( tables(i) ,  dbName, src_db_prefix, dst_db_prefix )
			next
		elseif dbType = "sqlite3" then
			for i = 0 to ubound(tables)
				POP_MVC.arr.push ret, getMysql2SqliteSql4table( tables(i) ,  dbName, src_db_prefix, dst_db_prefix )
			next
		end if
		
		getMysql2AccOrSqliteSql4Db = ret
		
		'还原配置
		POP_MVC.config("DB_NAME") = db_name
		POP_MVC.config("DB_TYPE") = db_type
	End Function
	
	'从 access 数据库得到 mysql 数据库的建表语句，只针对一个表
	Function getAcc2MysqlSql4table( ByRef TableName , ByRef dbPath ,  ByRef src_db_prefix , ByRef dst_db_prefix )
		getAcc2MysqlSql4table = getAccOrSqlite2MysqlSql4table("access" ,TableName,dbPath,src_db_prefix,dst_db_prefix )
	End Function
	
	'从 sqlite3 数据库得到 mysql 数据库的建表语句，只针对一个表
	Function getSqlite2MysqlSql4table( ByRef TableName , ByRef dbPath ,  ByRef src_db_prefix , ByRef dst_db_prefix )
		getSqlite2MysqlSql4table = getAccOrSqlite2MysqlSql4table("sqlite3" ,TableName,dbPath,src_db_prefix,dst_db_prefix )
	End Function	
	
	'针对一个表，将 access/mysql 数据库转化成 sqlite3 的建表语句
	'srcTable 为 access/mysql 的某个表名
	'srcDb 为 access/mysql 数据库的路径或库名
	'src_db_prefix为acc的表名前缀
	'dst_db_prefix为sqlite3的表名前缀，如果为""，则不加前缀
	Function getAccOrMysql2SqliteSql4table(ByVal srcType , ByVal srcTable , ByRef srcDb , ByVal src_db_prefix , ByVal dst_db_prefix )
		dim dictFields,prikey,key,sql,table_name
		set dictFields = B_( Array( srcTable , srcType , srcDb ) ).getTableFields("")
		
		'判断表名前是否有前缀
		if POP_MVC.String.iStartsWith( srcTable , src_db_prefix ) then
			table_name = dst_db_prefix  & mid( srcTable , len( src_db_prefix ) + 1  ) 
		else
			table_name = dst_db_prefix & srcTable
		end if
		
		'比如：CREATE TABLE IF NOT EXISTS iAspCms_sort (
		sql = "CREATE TABLE IF NOT EXISTS " & table_name & " ("
		
		'加入主键
		prikey = B_( Array(srcTable , srcType , srcDb )).getPK
		if prikey <> "" then
			sql = sql & vbcrlf & """" & prikey & """" & " INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL,"
		end if
		
		'添加每个字段信息
		for each key in dictFields
			if LCase(key) <> LCase(prikey) then
				sql = sql & vbcrlf & """" & key & """" & " " & SqliteDataType( dictFields(key) ) & " ,"
			end if
		next
		
		'删除最后一个逗号
		sql = POP_MVC.rtrim( sql, "," )
		
		'加入 );
		sql = sql & vbcrlf & ");"
		getAccOrMysql2SqliteSql4table = sql
	End Function
	
	'针对一个表，将 access 数据库转化成 sqlite3 的建表语句
	'accTableName 为 access 的某个表名
	'accPath 为 access 数据库的路径
	'src_db_prefix为acc的表名前缀
	'dst_db_prefix为sqlite3的表名前缀，如果为""，则不加前缀
	Function getAcc2SqliteSql4table(ByVal accTableName , ByRef accPath , ByVal src_db_prefix , ByVal dst_db_prefix )
		getAcc2SqliteSql4table = getAccOrMysql2SqliteSql4table( "access" , accTableName , accPath , src_db_prefix , dst_db_prefix )
	End Function
	
	'针对一个表，将 mysql 数据库转化成 sqlite3 的建表语句
	'mysqlTableName 为 mysql 的某个表名
	'dbName 为 mysql 数据库的库名
	'src_db_prefix为 mysql 的表名前缀
	'dst_db_prefix为 sqlite3 的表名前缀，如果为""，则不加前缀
	Function getMysql2SqliteSql4table(ByVal mysqlTableName , ByRef dbName , ByVal src_db_prefix , ByVal dst_db_prefix )
		getMysql2SqliteSql4table = getAccOrMysql2SqliteSql4table( "mysql" , mysqlTableName , dbName , src_db_prefix , dst_db_prefix )
	End Function
	
	'针对一个表，将 mysql 数据库转化成 access 的建表语句
	'TableName 为 Mysql 的某个表名
	'dbName 为 Mysql 数据库的库名，其他参数应在配置中完成
	'src_db_prefix为acc的表名前缀
	'dst_db_prefix为 mysql 的表名前缀，如果为""，则不加前缀
	Function getMysql2AccSql4table(ByVal TableName , ByRef dbName , ByVal src_db_prefix , ByVal dst_db_prefix )		
		POP_MVC.config( "DB_PREFIX" ) = src_db_prefix
		getMysql2AccSql4table = B_( Array( TableName , "mysql" , dbName  ) ).toAccessTableSql( TableName,dst_db_prefix  )
	End Function		
	
	
	'从 access 或者 sqlite3 数据库得到 mysql 数据库的建表语句，只针对一个表
	Function getAccOrSqlite2MysqlSql4table(ByRef dbType,ByRef TableName , ByRef dbPath ,  ByRef src_db_prefix , ByRef dst_db_prefix )
		dim dictFields,prikey,key,sql,table_name

		set dictFields = B_( Array(TableName , dbType , dbPath )).getTableFields("")

		'判断表名前是否有前缀
		if POP_MVC.String.iStartsWith( TableName , src_db_prefix ) then
			table_name =dst_db_prefix  & mid( TableName , len( src_db_prefix ) + 1  ) 
		else
			table_name = dst_db_prefix & TableName
		end if
		
		'比如：CREATE TABLE IF NOT EXISTS iAspCms_sort (
		sql = "CREATE TABLE IF NOT EXISTS `" & table_name & "` ("
		
		'加入主键
		prikey = B_(Array(TableName , dbType , dbPath )).getPK
		if prikey <> "" then
			sql = sql & vbcrlf & "`" & prikey & "`" & " INT PRIMARY KEY AUTO_INCREMENT,"
		end if
		
		'添加每个字段信息
		for each key in dictFields
			if LCase(key) <> LCase(prikey) then
				sql = sql & vbcrlf & "`" & key & "`" & " " & MysqlDataType( dictFields(key) ) & " ,"
			end if
		next
		
		'删除最后一个逗号
		sql = POP_MVC.rtrim( sql, "," )
		
		'加入 );
		sql = sql & vbcrlf & ") ENGINE=MyISAM DEFAULT CHARSET=utf8 ;"

		getAccOrSqlite2MysqlSql4table = sql
	End Function	
	
	'数据类型转化
	Function SqliteDataType( ByVal num )
		dim ret
		Select case num			
			case 2	'SmallInt
				ret="SMALLINT"	
			case 3	'Int
				ret="INTEGER"
			case 4	'Real
				ret="FLOAT"
			case 5	'Float 
				ret="DOUBLE"
			case 6	'Currency
				ret = "REAL"
			case 7	'DateTime
				ret = "TEXT"	'Date/Time
			case 8	'String
				ret = "TEXT"
			case 11	'Bit 
				ret = "TINYINT" 	'Yes/No
			case 13 'TimeStamp
				ret = "INTEGER"
			case 17	'TinyInt
				ret = "TINYINT"
			case 72	'UniqueIdentifier
				ret = "TEXT"	'sqlite3里没有这个
			case 128	'Binary
				ret = "BLOB"
			case 129	'Char
				ret = "TEXT"
			case 130	'NChar
				ret = "TEXT"
			case 131	'Decimal
				ret = "REAL"
			case 133	'DateTime
				ret = "TEXT"
			case 135	'SmallDateTime
				ret = "TEXT"
			case 200	'VarChar
				ret = "TEXT"
			case 201	'Text
				ret = "TEXT"
			case 202	'VarChar
				ret = "TEXT"
			case 203	'备注
				ret = "TEXT(10000)"
			case 204	'Binary
				ret = "BLOB"
			case 205	'Image
				ret = "BLOB"	
			case else
				ret = "TEXT"
		end Select
		SqliteDataType = ret
	End Function	
	
	
	Function MysqlDataType( ByRef num )
		dim ret
		Select case num			
			case 2	'SmallInt
				ret="SMALLINT"	
			case 3	'Int
				ret="INT(11)"	'AutoNumber OR Number
			case 4	'Real
				ret="FLOAT"
			case 5	'Float 
				ret="DOUBLE"
			case 6	'Currency
				ret = "DOUBLE"	'Currency
			case 7	'DateTime
				ret = "DATETIME"	'Date/Time
			case 8	'String
				ret = "VARCHAR(255)"
			case 11	'Bit 
				ret = "TINYINT(1)" 	'Yes/No
			case 13 'TimeStamp
				ret = "TIMESTAMP"
			case 17	'TinyInt
				ret = "TINYINT(1)"
			case 72	'UniqueIdentifier
				ret = "INT(11)"
			case 128	'Binary
				ret = "BLOB"
			case 129	'Char
				ret = "CHAR(255)"
			case 130	'NChar
				ret = "CHAR(255)"
			case 131	'Decimal
				ret = "DOUBLE"
			case 133	'DateTime
				ret = "DATETIME"
			case 135	'SmallDateTime
				ret = "DATETIME"
			case 200	'VarChar
				ret = "VARCHAR(255)"
			case 201	'Text
				ret = "TEXT"
			case 202	'VarChar
				ret = "VARCHAR(255)"
			case 203	'备注
				ret = "TEXT"
			case 204	'Binary
				ret = "BLOB"
			case 205	'Image
				ret = "BLOB"	
			case else
				ret = "TEXT"			
		end Select
		MysqlDataType = ret
	End Function

	'Big Integer 、 Binary 、 Boolean 、 Byte 、 Char 、 Currency 、 Date/Time 、 Decimal 、 Double 、 Float 、 GUID 、 Integer 、 Long 、 Long Binary (OLE Object) 、 Memo 、 Numeric 、 Single 、 Text 、 Time 、 TimeStamp 以及 VarBinary。 
	Function AccessDataType( ByRef num )
		dim ret
		Select case num			
			case 2	'SmallInt
				ret="Short INTEGER"	
			case 3	'Int
				ret="Integer"	'AutoNumber OR Number
			case 4	'Real
				ret="Single"
			case 5	'Float 
				ret="Float"
			case 6	'Currency
				ret = "Currency"	'Currency
			case 7	'DateTime
				ret = "Date/Time"	'Date/Time
			case 8	'String
				ret = "TEXT(255)"
			case 11	'Bit 
				ret = "Boolean" 	'Yes/No
			case 13 'TimeStamp
				ret = "Integer"
			case 17	'TinyInt
				ret = "Byte"
			case 72	'UniqueIdentifier
				ret = "GUID"
			case 128	'Binary
				ret = "Binary"
			case 129	'Char
				ret = "CHAR"
			case 130	'NChar
				ret = "TEXT(255)"
			case 131	'Decimal
				ret = "FLOAT"
			case 133	'DateTime
				ret = "Date/Time"
			case 135	'SmallDateTime
				ret = "Date/Time"
			case 200	'VarChar
				ret = "TEXT(255)"
			case 201	'Text
				ret = "TEXT(255)"
			case 202	'VarChar
				ret = "TEXT(255)"
			case 203	'备注
				ret = "MEMO"
			case 204	'Binary
				ret = "Binary"
			case 205	'Image
				ret = "Binary"	
			case else
				ret = "TEXT"			
		end Select
		accessDataType = ret
	End Function	
End Class
%>