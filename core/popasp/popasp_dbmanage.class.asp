<%
' +----------------------------------------------------------------------
' | POPASP [ ASP MVC ]
' +----------------------------------------------------------------------
' | Copyright (c) 2016 http://popasp.com All rights reserved.
' +----------------------------------------------------------------------
' | Author: popasp <1737025626@qq.com>
' +----------------------------------------------------------------------
' 这个类还在完善中，不能使用……
Class POPASP_DBMANAGE
	'---- DataTypeEnum Values ----
	Public adEmpty
	Public adTinyInt
	Public adSmallInt
	Public adInteger
	Public adBigInt
	Public adUnsignedTinyInt
	Public adUnsignedSmallInt
	Public adUnsignedInt
	Public adUnsignedBigInt
	Public adSingle
	Public adDouble
	Public adCurrency
	Public adDecimal
	Public adNumeric
	Public adBoolean
	Public adError
	Public adUserDefined
	Public adVariant
	Public adIDispatch
	Public adIUnknown
	Public adGUID
	Public adDate
	Public adDBDate
	Public adDBTime
	Public adDBTimeStamp
	Public adBSTR
	Public adChar
	Public adVarChar
	Public adLongVarChar
	Public adWChar
	Public adVarWChar
	Public adLongVarWChar
	Public adBinary
	Public adVarBinary
	Public adLongVarBinary

	'---- FieldAttributeEnum Values ----
	Public adFldMayDefer
	Public adFldUpdatable
	Public adFldUnknownUpdatable
	Public adFldFixed
	Public adFldIsNullable
	Public adFldMayBeNull
	Public adFldLong
	Public adFldRowID
	Public adFldRowVersion
	Public adFldCacheDeferred

	'---- SchemaEnum Values ----
	Public adSchemaProviderSpecific 
	Public adSchemaAsserts
	Public adSchemaCatalogs
	Public adSchemaCharacterSets
	Public adSchemaCollations
	Public adSchemaColumns
	Public adSchemaCheckPublicraints
	Public adSchemaPublicraintColumnUsage
	Public adSchemaPublicraintTableUsage
	Public adSchemaKeyColumnUsage
	Public adSchemaReferentialPublicraints
	Public adSchemaTablePublicraints
	Public adSchemaColumnsDomainUsage
	Public adSchemaIndexes
	Public adSchemaColumnPrivileges
	Public adSchemaTablePrivileges
	Public adSchemaUsagePrivileges
	Public adSchemaProcedures
	Public adSchemaSchemata
	Public adSchemaSQLLanguages
	Public adSchemaStatistics
	Public adSchemaTables
	Public adSchemaTranslations
	Public adSchemaProviderTypes
	Public adSchemaViews
	Public adSchemaViewColumnUsage
	Public adSchemaViewTableUsage
	Public adSchemaProcedureParameters
	Public adSchemaForeignKeys
	Public adSchemaPrimaryKeys
	Public adSchemaProcedureColumns
	Public adSchemaDBInfoKeywords
	Public adSchemaDBInfoLiterals
	Public adSchemaCubes
	Public adSchemaDimensions
	Public adSchemaHierarchies
	Public adSchemaLevels
	Public adSchemaMeasures
	Public adSchemaProperties
	Public adSchemaMembers
	Public adSchemaTrustees
	Public adSchemaFunctions
	Public adSchemaActions
	Public adSchemaCommands
	Public adSchemaSets
	Public conn
	
	Private Sub Class_Initialize
		
		'---- DataTypeEnum Values ----
		adEmpty = 0
		adTinyInt = 16
		adSmallInt = 2
		adInteger = 3
		adBigInt = 20
		adUnsignedTinyInt = 17
		adUnsignedSmallInt = 18
		adUnsignedInt = 19
		adUnsignedBigInt = 21
		adSingle = 4
		adDouble = 5
		adCurrency = 6
		adDecimal = 14
		adNumeric = 131
		adBoolean = 11
		adError = 10
		adUserDefined = 132
		adVariant = 12
		adIDispatch = 9
		adIUnknown = 13
		adGUID = 72
		adDate = 7
		adDBDate = 133
		adDBTime = 134
		adDBTimeStamp = 135
		adBSTR = 8
		adChar = 129
		adVarChar = 200
		adLongVarChar = 201
		adWChar = 130
		adVarWChar = 202
		adLongVarWChar = 203
		adBinary = 128
		adVarBinary = 204
		adLongVarBinary = 205

		'---- FieldAttributeEnum Values ----
		adFldMayDefer = &H00000002
		adFldUpdatable = &H00000004
		adFldUnknownUpdatable = &H00000008
		adFldFixed = &H00000010
		adFldIsNullable = &H00000020
		adFldMayBeNull = &H00000040
		adFldLong = &H00000080
		adFldRowID = &H00000100
		adFldRowVersion = &H00000200
		adFldCacheDeferred = &H00001000

		'---- SchemaEnum Values ----
		adSchemaProviderSpecific = -1
		adSchemaAsserts = 0
		adSchemaCatalogs = 1
		adSchemaCharacterSets = 2
		adSchemaCollations = 3
		adSchemaColumns = 4
		adSchemaCheckPublicraints = 5
		adSchemaPublicraintColumnUsage = 6
		adSchemaPublicraintTableUsage = 7
		adSchemaKeyColumnUsage = 8
		adSchemaReferentialPublicraints = 9
		adSchemaTablePublicraints = 10
		adSchemaColumnsDomainUsage = 11
		adSchemaIndexes = 12
		adSchemaColumnPrivileges = 13
		adSchemaTablePrivileges = 14
		adSchemaUsagePrivileges = 15
		adSchemaProcedures = 16
		adSchemaSchemata = 17
		adSchemaSQLLanguages = 18
		adSchemaStatistics = 19
		adSchemaTables = 20
		adSchemaTranslations = 21
		adSchemaProviderTypes = 22
		adSchemaViews = 23
		adSchemaViewColumnUsage = 24
		adSchemaViewTableUsage = 25
		adSchemaProcedureParameters = 26
		adSchemaForeignKeys = 27
		adSchemaPrimaryKeys = 28
		adSchemaProcedureColumns = 29
		adSchemaDBInfoKeywords = 30
		adSchemaDBInfoLiterals = 31
		adSchemaCubes = 32
		adSchemaDimensions = 33
		adSchemaHierarchies = 34
		adSchemaLevels = 35
		adSchemaMeasures = 36
		adSchemaProperties = 37
		adSchemaMembers = 38
		adSchemaTrustees = 39
		adSchemaFunctions = 40
		adSchemaActions = 41
		adSchemaCommands = 42
		adSchemaSets = 43	
	End Sub	
	
	'返回字段类型函数
	Function getTypeName(field_type)


		'field_type = 字段类型值
		Select Case field_type
			case adEmpty:getTypeName = "Empty"
			case adTinyInt:getTypeName = "TinyInt"
			case adSmallInt:getTypeName = "SmallInt"
			case adInteger: getTypeName = "Integer"
			case adBigInt:getTypeName = "BigInt"
			case adUnsignedTinyInt:getTypeName = "TinyInt" 'UnsignedTinyInt
			case adUnsignedSmallInt:getTypeName = "UnsignedSmallInt"
			case adUnsignedInt:getTypeName = "UnsignedInt"
			case adUnsignedBigInt:getTypeName = "UnsignedBigInt"
			case adSingle:getTypeName = "Single" 'Single
			case adDouble:getTypeName = "Double" 'Double
			case adCurrency:getTypeName = "Money" 'Currency
			case adDecimal:getTypeName = "Decimal"
			case adNumeric:getTypeName = "Numeric" 'Numeric
			case adBoolean:getTypeName = "Bit" 'Boolean
			case adError:getTypeName = "Error"
			case adUserDefined:getTypeName = "UserDefined"
			case adVariant:getTypeName = "Variant"
			case adIDispatch:getTypeName = "IDispatch"
			case adIUnknown:getTypeName = "IUnknown"
			case adGUID:getTypeName = "GUID" 'GUID
			case adDATE:getTypeName = "DateTime" 'Date
			case adDBDate:getTypeName = "DBDate"
			case adDBTime:getTypeName = "DBTime"
			case adDBTimeStamp:getTypeName = "DateTime" 'DBTimeStamp
			case adBSTR:getTypeName = "BSTR"
			case adChar:getTypeName = "Char"
			case adVarChar:getTypeName = "VarChar"
			case adLongVarChar:getTypeName = "LongVarChar"
			case adWChar:getTypeName = "Text" 'WChar类型 SQL中为Text
			case adVarWChar:getTypeName = "VarChar" 'VarWChar
			case adLongVarWChar:getTypeName = "Text" 'LongVarWChar
			case adBinary:getTypeName = "Binary"
			case adVarBinary:getTypeName = "VarBinary"
			case adLongVarBinary:getTypeName = "LongBinary"'LongVarBinary
			case adChapter:getTypeName = "Chapter"
			case adPropVariant:getTypeName = "PropVariant"
			case else:getTypeName = "Unknown"
		end select
	End Function

	'返回字段类型列表
	Function fieldtypelist(n)
		dim strlist,str1,str2
		strlist = "<select name=""field_type"">"
		if session("dbtype") = "access" then
			strlist = strlist & "<option value=""VarChar"">文本</option>"
			strlist = strlist & "<option value=""Text"">备注</option>"
			strlist = strlist & "<option value=""Bit"">(是/否)</option>"
			strlist = strlist & "<option value=""TinyInt"">数字(字节)</option>"
			strlist = strlist & "<option value=""SmallInt"">数字(整型)</option>"
			strlist = strlist & "<option value=""Integer"">数字(长整型)</option>"
			strlist = strlist & "<option value=""Single"">数字(单精度)</option>"
			strlist = strlist & "<option value=""Double"">数字(双精度)</option>"
			strlist = strlist & "<option value=""Numeric"">数字(小数)</option>"
			strlist = strlist & "<option value=""GUID"">数字(同步ID)</option>"
			strlist = strlist & "<option value=""DateTime"">时间/日期</option>"
			strlist = strlist & "<option value=""Money"">货币</option>"
			strlist = strlist & "<option value=""Binary"">二进制</option>"
			strlist = strlist & "<option value=""LongBinary"">长二进制</option>"
			strlist = strlist & "<option value=""LongBinary"">OLE 对象</option>"
			
		else
			strlist = strlist & "<option value="""">选择类型</option>"
			strlist = strlist & "<option value=""BigInt"">bigint</option>"
			strlist = strlist & "<option value=""Binary"">binary(二进制数据类型)</option>"
			strlist = strlist & "<option value=""Bit"">bit(整型)</option>"
			strlist = strlist & "<option value=""Char"">char(字符型)</option>"
			strlist = strlist & "<option value=""DateTime"">datetime(日期时间型)</option>"
			strlist = strlist & "<option value=""Decimal"">decimal(精确数值型)</option>"
			strlist = strlist & "<option value=""Float"">float(近似数值型)</option>"
			strlist = strlist & "<option value=""Image"">image(二进制数据类型)</option>"
			strlist = strlist & "<option value=""Int"">int(整型)</option>"
			strlist = strlist & "<option value=""Money"">money(货币型)</option>"
			strlist = strlist & "<option value=""nchar"">nchar(统一编码字符型)</option>"
			strlist = strlist & "<option value=""ntext"">ntext(统一编码字符型)</option>"
			strlist = strlist & "<option value=""numeric"">numeric(精确数值型)</option>"
			strlist = strlist & "<option value=""nvarchar"">nvarchar(统一编码字符型)</option>"
			strlist = strlist & "<option value=""real"">real(近似数值型)</option>"
			strlist = strlist & "<option value=""smalldatetime"">Smalldatetime(日期时间型)</option>"
			strlist = strlist & "<option value=""smallint"">smallint(整型)</option>"
			strlist = strlist & "<option value=""smallmoney"">smallmoney(货币型)</option>"
			strlist = strlist & "<option value=""sql_variant"">sql_variant()</option>"
			strlist = strlist & "<option value=""text"">text(字符型)</option>"
			strlist = strlist & "<option value=""timestamp"">timestamp(特殊数据型)</option>"
			strlist = strlist & "<option value=""tinyint"">tinyint(整型)</option>"
			strlist = strlist & "<option value=""uniqueidentifier"">Uniqueidentifier(特殊数据型)</option>"
			strlist = strlist & "<option value=""varbinary"">varbinary(二进制数据类型)</option>"
			strlist = strlist & "<option value=""varchar"">varchar(字符型)</option>"
		end if
		str1 = """" & n & """"
		str2 = """" & n & """" & " selected"
		strlist = replace(strlist,str1,str2)
		strlist = strlist & "</select>"
		fieldtypelist = strlist
	End Function	
	
	
	'得到创建表的SQL语句
	sub getCreateTableSQL( tableName)
		dim sql,i,primarykey
		on error resume next
		sql = "CREATE TABLE ["& tableName &"] ("
		for i = 1 to request("i")
		   sql = sql & "[" & request("field_name")(i) & "] " & request("field_type")(i)
			  if request("field_size")(i) <> "" then
				  sql = sql & "(" & request("field_size")(i) & ")"
			  end if
			  if request("null")(i) = "NOT_NULL" then
				  sql = sql & " not null"
			  end if
			  if request("autoincrement")(i) = "自动编号" then
				  sql = sql & " identity"
			  end if
			  if request("primarykey")(i) = "primarykey" then
				  primarykey = request("field_name")(i)
			  end if
			'if primarykey <> "" then
			   sql = sql & ","
			'end if
		next
		if primarykey<>"" then
		   sql=sql&" primary key (["&primarykey&"]) "
		end if
		sql = sql & ")"
		sql = replace(sql,"()","")  '构建空表
		response.redirect "?key=sql&sql=" & sql 
	end sub	
	
	
	
End Class
%>