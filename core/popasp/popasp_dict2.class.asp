<%
' +----------------------------------------------------------------------
' | POPASP [ ASP MVC ]
' +----------------------------------------------------------------------
' | Copyright (c) 2016 http://popasp.com All rights reserved.
' +----------------------------------------------------------------------
' | Author: popasp <1737025626@qq.com>
' +----------------------------------------------------------------------
Class POPASP_DICT2
	Public sortByNumeric,sortByDate
	
	'从数据库中取出的数据为二维Dictionary对象，可以将其转化为Table表格
	public Property Get Table( ByRef dict )
		Table = P_("POPASP_HTML").Table( dict,"","","" )
	End Property	
	
	'从数据库中取出的数据为二维Dictionary对象，可以将其转化为Excel表格
	'1） 文本(text,t)：vnd.ms-excel.numberformat:@
	'2） 日期(date,d)：vnd.ms-excel.numberformat:yyyy/mm/dd hh:mm:ss
	'3） 数字(number,n)：vnd.ms-excel.numberformat:#,##0.00
	'4） 货币(currency,c,￥,$)：vnd.ms-excel.numberformat:￥#,##0.00
	'5） 百分比(percent,p)：vnd.ms-excel.numberformat: #0.00%	
	'style为数组，分别对应各字段的类型，默认空字符串""为文本类型
	public sub Excel( ByRef dict , ByVal style , ByVal filename )
		Server.ScriptTimeOut=9999999
		on error resume next
		POP_MVC.config("SHOW_PAGE_TRACE") = 0
		filename = POP_MVC.String.rtrim( filename,".xls" )
		filename = POP_MVC.String.rtrim( filename,".xlsx" )
		
		'如果要调试，请注释掉下面两行代码
		Response.ContentType = "application/excel"
		Response.AddHeader "Content-Disposition", "attachment;filename="""& filename &".xls"""
		
		
		dim html,i,k1,k2,item,j,temp,uniqname	'i用来计算行数,j计算列数
		
		html = "<html>" & VbCrLf
		html = html & "<head>" & VbCrLf
		html = html & "<meta name=ProgId content=Excel.Sheet>" & VbCrLf
		html = html & "<style>" & VbCrLf
		html = html & "table,th,td{border:0.5pt solid black;}" & VbCrLf
		html = html & "</style>" & VbCrLf
		html = html & "</head>" & VbCrLf
		html = html & "<body>" & VbCrLf
		html = html & "<table>"		
		i = 0	
		
		for each k1 in dict
			j = 0
			set item = dict(k1)
			if i = 0 then
				html = html & VbCrLf & vbTab & "<tr>"
				html = html & VbCrLf & vbTab & vbTab
				for each k2 in item
					html = html & "<th style='vnd.ms-excel.numberformat:@'>" & k2 & "</th>"
				next
				html = html & VbCrLf & vbTab & "</tr>"
				
				if not isArray(style) then
					style = split( style,"," )
				end if
				
				for j = 0 to ubound( style )
					if style(j) = "" then
						style(j) = "number"
					end if
				next
				
				for j = (ubound(style) + 1) to item.count
					POP_MVC.Arr.push style,"number"
				next
				
				if item.count > ubound( style ) + 1 then	'如果类型数组中的元素个数小于字段个数，则用数字类型代替
					temp = style( ubound(style) )
					for j = item.count - 1  to ubound( style ) step -1
						POP_MVC.Arr.Push style,temp
					next
				end if
			end if	
			html = html & VbCrLf & vbTab & "<tr>"
			html = html & VbCrLf & vbTab & vbTab
			j = 0

			for each k2 in item
				select case LCase(style(j))
					case "text","t"
						html = html & "<td style='vnd.ms-excel.numberformat:@'>" & item(k2) & "</td>"
					case "date","d"
						html = html & "<td style='vnd.ms-excel.numberformat:yyyy/mm/dd hh:mm:ss'>" & item(k2) & "</td>"
					case "number","n"
						'html = html & "<td style='vnd.ms-excel.numberformat:#,##0.00'>" & item(k2) & "</td>"
						html = html & "<td style='vnd.ms-excel.numberformat'>" & item(k2) & "</td>"
					case "currency","c","￥"
						html = html & "<td style='vnd.ms-excel.numberformat:￥#,##0.00'>" & item(k2) & "</td>"
					case "currency","c","$"
						html = html & "<td style='vnd.ms-excel.numberformat:$#,##0.00'>" & item(k2) & "</td>"
					case "percent","p"
						html = html & "<td style='vnd.ms-excel.numberformat: #0.00%	'>" & item(k2) & "</td>"
				End Select
				j = j + 1
			next
			html = html & VbCrLf & vbTab & "</tr>"
			i = i + 1
			
			if i > 0 and i mod 500 = 0 then
				if isEmpty( uniqname ) then
					uniqname = POP_MVC.String.Uniqid & ".html"
					uniqname = POP_MVC.String.rtrim(POP_MVC.appPath , "/" ) & "/Runtime/" & uniqname
				end if
				
				if not isEmpty( uniqname ) then
					Call POP_MVC.file_append_contents( uniqname, html )
					html = ""
				end if
			end if			
		next		
		html = html & VbCrLf & "</table>"
		html = html & VbCrLf & "</body>"
		html = html & VbCrLf & "</html>"
		
		if not isEmpty( uniqname ) then
			Call POP_MVC.file_append_contents( uniqname, html )
			html = POP_MVC.file_get_contents( uniqname )
			POP_MVC.file.remove( uniqname )		
		end if	
		
		set dict = nothing
		Response.write html
	End sub	
	
	Private Sub Class_Initialize
		sortByNumeric = True
		sortByDate = True
	End Sub
	
	''''''''''dict排序函数，全部是在dict自身上进行操作''''''''''	
	
	' 对二维dict根据项目field按照字符串值正向排序
	Sub sort( ByRef dict,ByRef field )
		call AscSortByFunc( dict , field, "POP_MVC.String.cmp")
	End Sub
	
	' 对二维dict根据项目field按照字符串值逆向排序
	Sub rsort( ByRef dict,ByRef field )
		call DescSortByFunc( dict , field, "POP_MVC.String.cmp")
	End Sub
	
	' 对二维dict根据项目field按照字符串值正向排序，并且忽略大小写
	Sub casesort( ByRef dict,ByRef field )
		call AscSortByFunc( dict , field, "POP_MVC.String.casecmp")
	End Sub
	
	' 对二维dict根据项目field按照字符串值逆向排序，并且忽略大小写
	Sub casersort( ByRef dict,ByRef field )
		call DescSortByFunc( dict , field, "POP_MVC.String.casecmp")
	End Sub
	
	' 用“自然排序”算法对二维dict根据项目field按照字符串值正向排序
	Sub natsort( ByRef dict,ByRef field )
		call AscSortByFunc( dict , field, "POP_MVC.String.natcmp")
	End Sub
		
	' 用“自然排序”算法对二维dict根据项目field按照字符串值逆向排序
	Sub natrsort( ByRef dict,ByRef field )
		call DescSortByFunc( dict ,field, "POP_MVC.String.natcmp")
	End Sub
	
	' 用“自然排序”算法对二维dict根据项目field按照字符串值正向排序，并且忽略大小写 
	Sub casenatsort( ByRef dict,ByRef field )
		call AscSortByFunc( dict ,field, "POP_MVC.String.casenatcmp")
	End Sub
	
	' 用“自然排序”算法对二维dict根据项目field按照字符串值逆向排序，并且忽略大小写 
	Sub casenatrsort( ByRef dict,ByRef field )
		call DescSortByFunc( dict ,field, "POP_MVC.String.casenatcmp")
	End Sub		
	
	'对二维dict根据项目field按照用户自定义的比较函数根据值进行排序
	Sub usort( ByRef dict,ByRef field ,ByRef funcComp )
		dim i,j,cnt,bool,arr
		arr = dict.keys
		cnt = ubound(arr)
		for i = 0 to cnt-1
			for j = i+1 to cnt			
				Execute "bool = " & funcComp & "( dict(arr(i)),dict(arr(j)) )"
				if bool > 0 then
					call POP_MVC.dict.kvswap(dict, arr(i),arr(j) )
					call POP_MVC.Arr.swap( arr,i,j )				
				end if
			next
		next	
	End Sub
	
	'对二维dict根据项目field按自定义函数进行升序排序，
	Public Sub AscSortByFunc( ByRef dict,ByRef field  , ByRef funcComp )
		call sortByFunc( dict , field, funcComp,false )		
	End Sub
	
	'对二维dict根据项目field按自定义函数进行升序排序，
	Public Sub DescSortByFunc( ByRef dict,ByRef field  , ByRef funcComp )
		call sortByFunc( dict , field, funcComp,true )		
	End Sub
	
	''''''''''''''其他函数	 
	
	'对二维dict根据项目field按字符串值，使用funcComp进行排序
	'isR是否为逆向，正向false,逆向true	
	Private Sub sortByFunc( ByRef dict,ByRef field  , ByRef funcComp, ByRef isR )
		dim i,j,cnt,bool,arr,p
		arr = dict.keys
		cnt = ubound(arr)
		
		for i = 0 to cnt-1
			p = i
			for j = i+1 to cnt
				bool = false
				if Not is_empty(sortByDate) And isDate( dict(arr(p))(field) ) And isDate( dict( arr(j) )(field) ) Then
					bool = ( DateDiff("s",dict(arr(j))(field),dict( arr(p) )(field)) >  0 )				
				elseif Not is_empty(sortByNumeric) And isNumeric( dict( arr(p) )(field) ) And isNumeric( dict( arr(j) )(field) ) Then
					bool = ( dict(arr(p))(field) > dict(arr(j))(field) )
				else
					Execute "bool = ( " & funcComp & "( dict(arr(p))(field),dict(arr(j))(field) )>0 )"
				end if
				
				if (not isR and bool) OR (isR and not bool) then
					p = j				
				end if				
			next
			
			if p <> i then
				call POP_MVC.dict.kvswap(dict, arr(i),arr(p) )
				call POP_MVC.Arr.swap( arr,i,p )	
			end if
		next	
	End Sub
End Class
%>