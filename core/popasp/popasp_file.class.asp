<%
' +----------------------------------------------------------------------
' | POPASP [ ASP MVC ]
' +----------------------------------------------------------------------
' | Copyright (c) 2016 http://popasp.com All rights reserved.
' +----------------------------------------------------------------------
' | Author: popasp <1737025626@qq.com>
' +----------------------------------------------------------------------
Class POPASP_FILE
	'下面几个方法来自于POP_MVC
	Private files_counter
	Private folders_counter
	
	'这几个属性用于 getFilesMap
	Private FilesMapStr,FilesLevel,FilesLastFolder,FilesRootPath
	
	'这几个属性跟POPASP_TEMPLATE_COMPILER密切相关
	Public is_use_tpl_compiler,tpl_nloopstr,tpl_repStr,tpl_l_d,tpl_loopCounter,tpl_bool,tpl_dict_label,tpl_start,tpl_filter,tpl_count
	
	Private Sub Class_Initialize
		files_counter = 0
		folders_counter = 0
	End Sub
	
	'返回文件的绝对路径
	'等同于POP_MVC.realPath
	'path不管是相对路径，还是绝对路径，都返回绝对路径
	'建议使用POP_MVC.realPath
	Function realPath( path )
		realPath = POP_MVC.realPath( path )
	End Function
	
	'获取文本文件内容
	'等同于POP_MVC.file_get_contents
	Function getContents( ByVal filePath )
		getContents = POP_MVC.file_get_contents( filePath )
	End Function
	
	' 给文件写入内容，如果文件不存在，则尝试创建
	'等同于POP_MVC.file_put_contents
	Function putContents(filePath,content)
		putContents = POP_MVC.file_put_contents(filePath,content)
	End Function
	
	'向文件尾部追加内容
	'等同于POP_MVC.file_append_contents
	Function appendContents( ByRef filePath , ByRef append_contents)
		appendContents = POP_MVC.file_append_contents(filePath,append_contents)
	End Function
	
	'自动生成文件夹路径
	'等同于POP_MVC.CreateFolder
	Public Property Get CreateFolder( ByVal path )
		CreateFolder = POP_MVC.CreateFolder( path )
	End Property
	
	'''''''''''''''''''''''''''
	
	'返回路径中的文件夹部分，仅根据filepath来获取，不会转化为真实路径
	Function dir( byval filepath )
		dim bool,suffix,pos
		bool = false
		
		filePath = replace( filePath,"\","/" )	'将路径中的\，用/来替换
		filePath = POP_MVC.String.rtrim( filePath, "/" )
		pos = inStrRev( filePath,"/" )
		if pos = 1 then
			dir = ""
		elseif pos>1 then	'如果路径中含有/			
			dir = POP_MVC.String.rstr( filePath ,"/" )	'找最后一个/			
			dir = mid(filePath,1,pos)
			
			if dir = "/" then
				dir = ""
			end if
		else
			dir = filePath
		end if
	End Function
	
	'返回路径中的文件夹部分，与dir不同，得到的是真实路径
	Function realDir( byval filepath )
		dim pos
		filepath = realPath( filepath )
		pos = inStrRev( filePath,"\" )
		if pos = 1 then
			realDir = ""
		elseif pos>1 then	'如果路径中含有/			
			realDir = POP_MVC.String.rstr( filePath ,"\" )	'找最后一个/			
			realDir = mid(filePath,1,pos-1)
			
			if realDir = "\" then
				realDir = ""
			end if
		else
			realDir = filePath
		end if
	End Function	
	
	'获取拷贝文件名
	Function getCopyName( byref filePath, byRef ext )
		dim fileExtName,ret
		fileExtName = extName( filePath )
		ret = left( filePath , len( filePath ) - len( fileExtName ) ) & ext & fileExtName
		getCopyName = ret	
	End Function

	'返回路径中的文件名部分	
	'如果参数为数组，Array( path,是否含文件后缀),第2个参数为真时不含后缀，默认取后缀
	Function baseName( byref arg )
		dim bool,suffix,filePath
		bool = false

		'处理参数
		if isArray( arg ) Then
			filePath = arg(0)
			if ubound(arg) > 0 then '含有两个参数
				bool = Not is_empty( arg(1) )
			end if
		else
			filePath = arg
		end if
	
		filePath = replace( filePath,"\","/" )	'将路径中的\，用/来替换
				
		if POP_MVC.String.Exists( filePath, "/" ) then	'如果路径中含有/
			filePath = POP_MVC.String.rstr( filePath ,"/" )	'找最后一个/			
			filePath = mid(filePath,2)
		end if

	
		baseName = filePath
		
		if bool then	'为真时，不含后缀
			if POP_MVC.String.Exists( filePath, "." ) then	'判断文件名中是否含有点
				suffix = POP_MVC.String.rstr( filePath ,"." )	
			end if

			if suffix <> "" Then
				baseName = mid(baseName,1,len(baseName) - len(suffix) )
			end if
		end if
	End Function
	
	'返回路径中的文件名的后缀，默认含.
	'如果参数为数组，Array( path,是否含第1个点),第2个参数为真时不取第1个点，默认含点
	Function extName( ByVal arg )	
		dim bool,filePath
		
		extName = ""
		bool = false
		
		'处理参数
		if isArray( arg ) Then
			filePath = arg(0)
			if ubound(arg) > 0 then '含有两个参数
				bool = (Not is_empty( arg(1) ))
			end if
		else
			filePath = arg
		end if
	
		filePath = baseName( filePath )
		if POP_MVC.String.Exists( filePath, "." ) then	'判断文件名中是否含有点
			extName = POP_MVC.String.rstr( filePath ,"." )	
		end if

		if bool then	'为真值时不含点
			extName = mid( extName,2 )
		end if
	End Function
	
	' 从类文件中取得内容，并且删除第一个与最后一个asp代码标记
	Function class_get_contents ( file )
		dim str,start_,end_
		str		= getContents(file)
		start_	= InStr(str,"<" & "%")
		end_	= InStrRev( str,"%" & ">" )
		str= mid(str,start_ + 2,end_-3) 
		class_get_contents = str
	End Function	
	
	
	' 检测提供的路径文件是否为文件
	Function isFile( path )
		isFile = POP_MVC.fso.FileExists( POP_MVC.realPath(path) )
	End Function
	
	'检测提供的路径文件夹是否存在
	Public Function isFolder(path)
		isFolder = POP_MVC.fso.FolderExists(POP_MVC.realPath(path))
	End Function
	
	'检测相对路径的文件或文件夹是否存在
	Public Function isExists(ByVal path)
		isExists = isFile(path) or isFolder(path)
	End Function
	
	'删除文件或文件夹
	Public Property Get remove( path )
		on error resume next
		if isFile( path ) then
			call POP_MVC.fso.deleteFile(POP_MVC.realPath(path),true)
		elseif isFolder( path ) then
			call POP_MVC.fso.deleteFolder(POP_MVC.realPath(path),true)
		end if
	End Property
	

	
	'从文件夹中删除文件
	'dir与path在判断从属关系时，不转化成绝对路径
	Public Function removeFromDir( ByVal dir, ByVal path )
		dim i,arr
		if isArray( path ) then
			for i = 0 to ubound( path )
				removeFromDir =  removeFromDir( dir, path(i) )
			next
		else
			if inStr( path, "," ) > 0 then
				path = split( path, "," )
				removeFromDir = removeFromDir( dir, path )
			else
				path = trim(path)
				if POP_MVC.String.iStartsWith( path, dir ) then
					remove(path)
					removeFromDir = true
				else
					dir  = POP_MVC.realPath( dir )
					path = POP_MVC.realPath( path )
					if POP_MVC.String.iStartsWith( path, dir ) then
						remove(path)
						removeFromDir = true
					else
						removeFromDir = path
						exit function
					end if
				end if
			end if
		end if
	End Function
	
	'判断文件path是否属于目录dir下的文件
	'判断从属关系时，将二者都先转成绝对路径
	Public Function belong( ByVal dir, ByVal path )
		belong = false
		if POP_MVC.String.iStartsWith( path, dir ) then
			belong = true
		else
			dir  = POP_MVC.realPath( dir )
			path = POP_MVC.realPath( path )
			if POP_MVC.String.iStartsWith( path, dir ) then
				belong = true
			end if
		end if
	End Function
	
	'判断文件path是否属于目录dir下的文件
	'belong的同名函数
	Public Function [in]( ByVal dir, ByVal path )
		[in] = belong( dir, path )
	End Function
	
	' 获取文件大小
	Function fileSize ( filename )
		dim file 
		If Instr(filename,">")>0 Then 
			if isFile( filename ) then
				set file = POP_MVC.fso.GetFile(filename)
			else
				set file = POP_MVC.fso.GetFolder(filename)
			end if
		else
			if isFile( filename ) then
				set file = POP_MVC.fso.GetFile(POP_MVC.realPath(filename))
			else
				set file = POP_MVC.fso.GetFolder(POP_MVC.realPath(filename))
			end if			
		end if	
		fileSize = file.size
		set file = nothing
	End Function

	' 获取某个文件的修改时间
	Function mtime ( filename )
		dim file 
		If Instr(filename,">")>0 Then 
			if isFile( filename ) then
				set file = POP_MVC.fso.GetFile(filename)
			else
				set file = POP_MVC.fso.GetFolder(filename)
			end if
			
		else
			if isFile( filename ) then
				set file = POP_MVC.fso.GetFile(POP_MVC.realPath(filename))
			else
				set file = POP_MVC.fso.GetFolder(POP_MVC.realPath(filename))
			end if
		end if	
		mtime = file.DateLastModified
		set file = nothing
	End Function
	
	' 获取某个文件的创建时间
	Function ctime ( filename )
		dim file 
		If Instr(filename,">")>0 Then 
			if isFile( filename ) then
				set file = POP_MVC.fso.GetFile(filename)
			else
				set file = POP_MVC.fso.GetFolder(filename)
			end if
			
		else
			if isFile( filename ) then
				set file = POP_MVC.fso.GetFile(POP_MVC.realPath(filename))
			else
				set file = POP_MVC.fso.GetFolder(POP_MVC.realPath(filename))
			end if
		end if	
		ctime = file.DateCreated
		set file = nothing
	End Function
	
	'把一个文件或文件夹从一个位置移动到另一个位置，用 MoveFile/MoveFolder ，失败返回错误(一般由于权限不够)
	Function rename( byval src , byval dst )
		err.clear
		on error resume next
		dim realSrc,realDst,errstr
		realSrc = POP_MVC.realPath( src )
		realDst = POP_MVC.realPath( dst )
		rename = false
		if realSrc<>realDst then
			if not Me.isExists( realDst ) then
				if Me.isFile( realSrc ) then
					POP_MVC.fso.movefile  realSrc, realDst
					rename = true
				elseif Me.isFolder( realSrc ) then
					POP_MVC.fso.moveFolder  realSrc, realDst
					rename = true
				end if
				if err.number <> 0 then
					rename = false
					if err.number = 70 then errstr = "，请确认是否已经关闭要重命名的文件(夹)"
					POP_MVC.exit( src & " -> " & dst &  "文件重命名失败，" & err.number & "," & err.description & errstr )
				end if
			else
				POP_MVC.Warning( dst & "文件已经存在，重命名失败！" )
			end if
		end if
	end Function
	
	'把一个文件或文件夹从一个位置移动到另一个位置，用 MoveFile/MoveFolder ，因权限不够不能移动时则采用 copy加remove的方法
	Function move( byval src , byval dst )
		err.clear
		on error resume next
		dim realSrc,realDst,errstr
		realSrc = POP_MVC.realPath( src )
		realDst = POP_MVC.realPath( dst )
		move = false
		if realSrc<>realDst then
			if not Me.isExists( realDst ) then
				if Me.isFile( realSrc ) then
					POP_MVC.fso.movefile  realSrc, realDst
					move = true
				elseif Me.isFolder( realSrc ) then
					POP_MVC.fso.moveFolder  realSrc, realDst
					move = true
				end if
				if err.number <> 0 then
					move = false
					if err.number = 70 then						
						Call Me.Copy( realSrc, realDst )
						Me.remove( realSrc )
					end if
				end if
			else
				POP_MVC.Warning( dst & "文件已经存在，重命名失败！" )
			end if
		end if
	end Function	
	
	'把一个文件从一个位置移动到另一个位置。
	function moveFile( byval src,byval dst )
		on error resume next
		moveFile = false
		if Me.isFile( src ) then
			Me.CopyFile src,dst
			if err.number = 0 then
				Call Me.remove( src )
				moveFile = true
			else
				POP_MVC.exit( L_("") )
			end if
		end if
	end function
	
	Sub Copy( src, dst )
		if Me.isFolder( src ) then
			Call CopyFolder( src,dst )
		elseif Me.isFile( src ) then
			Call CopyFile( src,dst )
		end if
	End Sub
	
	'将一个文件夹下的内容全部复制到另一个文件夹下
	Sub CopyFolder( src,dst )
		Server.ScriptTimeOut=99999
		If Not Me.isExists( dst ) Then
			POP_MVC.CreateFolder( dst )
			dim startTime : startTime = timer()
			POP_MVC.fso.CopyFolder POP_MVC.realPath( src )  , POP_MVC.realPath( dst ) , true
			call POP_MVC.pushTime( startTime , "复制文件夹从 " & src & " 到 " & dst )
		End If
	End Sub
	
	'复制一个文件
	Sub CopyFile( src,dst )		
		dim dirname
		dirname = Me.dir( POP_MVC.realPath( dst ) )
		if not POP_MVC.fso.FolderExists( dirname ) then POP_MVC.CreateFolder( dirname )
		POP_MVC.fso.CopyFile POP_MVC.realPath( src )  , POP_MVC.realPath( dst ) , true		
	End Sub
	
  '列出文件夹下的所有文件夹或文件
  Public Function List(ByVal folderPath, ByVal fileType)
    On Error Resume Next 'Do not delete or comment
    Dim f,fs,k,i,l,dict,ret
    folderPath = POP_MVC.realPath(folderPath) : i = 0
	
	If Not Me.isExists( folderPath ) Then
		set List = D_
		Exit Function
	End If
	
    Select Case LCase(fileType)
      Case "0","" l = 0
      Case "1","file" l = 1
      Case "2","folder" l = 2
      Case Else l = 0
    End Select
    Set f = POP_MVC.Fso.GetFolder(folderPath)
	set ret = D_
    If l = 0 Or l = 2 Then
      Set fs = f.SubFolders	  
      For Each k In fs		
		set dict = D_
		dict("Name") = k.Name
		dict("Size") = k.Size
		dict("DateLastModified") = k.DateLastModified
		dict("Attributes") = k.Attributes
		dict("Type") = k.Type
		dict("Name") = k.Name
		dict("Path") = k.Path
		set ret( ret.count + 1 ) = dict
      Next
    End If
	
    If l = 0 Or l = 1 Then
      Set fs = f.Files
      For Each k In fs
		set dict = D_
		dict("Name") = k.Name
		dict("Size") = k.Size
		dict("DateLastModified") = k.DateLastModified
		dict("Attributes") = k.Attributes
		dict("Type") = k.Type
		dict("Name") = k.Name
		dict("Path") = k.Path
		set ret( ret.count + 1 ) = dict
      Next
    End If
    Set fs = Nothing
    Set f = Nothing
	
    set List = ret
    Call L_("POPASP_FILE.List")
  End Function
  
	'获取子目录个数
	Public Property Get FolderCount( ByRef Path )
		on error resume next
		dim objFolder,objFiles
		set objFolder=POP_MVC.fso.GetFolder( POP_MVC.realPath(path) )
		set objFiles=objFolder.SubFolders
		FolderCount = objFiles.count
		set objFiles=nothing	
		set objFolder=nothing
		call L_( typename(Me) & ".FolderCount" )
	End Property
  
    '计算文件夹中文件（仅文件，不包含子文件夹）的数量
	Public Property Get FileCount( ByRef path )
		on error resume next
		dim objFolder,objFiles
		set objFolder=POP_MVC.fso.GetFolder( POP_MVC.realPath(path) )
		set objFiles=objFolder.Files
		FileCount = objFiles.count
		set objFiles=nothing	
		set objFolder=nothing
		call L_( typename(Me) & ".FileCount" )
	End Property
  
  
	'对文件夹(递归查找子文件夹)的所有文件与目录进行回调函数处理
	'既处理目录，又处理文件
	sub all_map( byref path , ByRef callback )
		on error resume next
		dim objFolder,file,objFiles,tplFilterBool
		if Me.isFolder( path ) then
			set objFolder=POP_MVC.fso.GetFolder( POP_MVC.realPath(path) )
			if tpl_start then
				if not is_use_tpl_compiler then
					Execute( "call " & callback & "(objFolder)" )
				else
					if Me.tpl_filter <> "" then
						Execute( "tplFilterBool = " & Me.tpl_filter )
						if tplFilterBool then
							Me.tpl_count = Me.tpl_count + 1
							Execute( "call " & callback & "(tpl_nloopstr,tpl_repStr,tpl_l_d,tpl_loopCounter,objFolder,tpl_bool,tpl_dict_label)" )
						end if
					else
						Me.tpl_count = Me.tpl_count + 1
						Execute( "call " & callback & "(tpl_nloopstr,tpl_repStr,tpl_l_d,tpl_loopCounter,objFolder,tpl_bool,tpl_dict_label)" )
					end if					
				end if
			end if
			tpl_start = false
			Set objFiles = objFolder.Files	  
			For Each file In objFiles			
				Set file= POP_MVC.fso.GetFile( POP_MVC.realPath(path) )
				if not is_use_tpl_compiler then
					Execute( "call " & callback & "(file)" )
				else
					if Me.tpl_filter <> "" then
						Execute( "tplFilterBool = " & Me.tpl_filter )
						if tplFilterBool then
							Me.tpl_count = Me.tpl_count + 1
							Execute( "call " & callback & "(tpl_nloopstr,tpl_repStr,tpl_l_d,tpl_loopCounter,file,tpl_bool,tpl_dict_label)" )
						end if
					else
						Me.tpl_count = Me.tpl_count + 1
						Execute( "call " & callback & "(tpl_nloopstr,tpl_repStr,tpl_l_d,tpl_loopCounter,file,tpl_bool,tpl_dict_label)" )
					end if						
				end if	
			Next
			Set objFiles = objFolder.SubFolders	  
			For Each file In objFiles
				if not is_use_tpl_compiler then
					Execute( "call " & callback & "(file)" )
				else
					if Me.tpl_filter <> "" then
						Execute( "tplFilterBool = " & Me.tpl_filter )
						if tplFilterBool then
							Execute( "call " & callback & "(tpl_nloopstr,tpl_repStr,tpl_l_d,tpl_loopCounter,file,tpl_bool,tpl_dict_label)" )
						end if
					else
						Execute( "call " & callback & "(tpl_nloopstr,tpl_repStr,tpl_l_d,tpl_loopCounter,file,tpl_bool,tpl_dict_label)" )
					end if
				end if
				call all_map( file.path, callback )
			Next
		end if
		call L_( typename(Me) & ".all_map" )
	end sub
  
  
	'仅对文件夹下的文件（不递归子文件夹）进行回调函数处理
	'只处理文件，不处理目录
	sub file_map( byref path , ByRef callback )	
		on error resume next
		dim objFolder,file,objFiles,tplFilterBool
		set objFolder=POP_MVC.fso.GetFolder( POP_MVC.realPath(path) )
		set objFiles=objFolder.Files
		for each file in objFiles
			if not is_use_tpl_compiler then
				Execute( "call " & callback & "(file)" )
			else	
				if Me.tpl_filter <> "" then
					Execute( "tplFilterBool = " & Me.tpl_filter )
					if tplFilterBool then
						Me.tpl_count = Me.tpl_count + 1
						Execute( "call " & callback & "(tpl_nloopstr,tpl_repStr,tpl_l_d,tpl_loopCounter,file,tpl_bool,tpl_dict_label)" )
					end if
				else
					Me.tpl_count = Me.tpl_count + 1
					Execute( "call " & callback & "(tpl_nloopstr,tpl_repStr,tpl_l_d,tpl_loopCounter,file,tpl_bool,tpl_dict_label)" )
				end if				
			end if	
		next 
		set objFiles=nothing	
		set objFolder=nothing
		call L_( typename(Me) & ".file_map" )
	end sub  
	
	
	'对文件夹(递归查找子文件夹)的所有文件进行回调函数处理
	'只处理文件，不处理目录
	sub files_map( byval path , ByRef callback )	
		on error resume next
		dim file
		if Me.isFile( path ) then
			dim tplFilterBool
			Set file= POP_MVC.fso.GetFile( POP_MVC.realPath(path) )
			if not is_use_tpl_compiler then				
				Execute( "call " & callback & "(file)" )			
			else
				if Me.tpl_filter <> "" then
					Execute( "tplFilterBool = " & Me.tpl_filter )
					if tplFilterBool then
						Me.tpl_count = Me.tpl_count + 1
						Execute( "call " & callback & "(tpl_nloopstr,tpl_repStr,tpl_l_d,tpl_loopCounter,file,tpl_bool,tpl_dict_label)" )
					end if
				else
					Me.tpl_count = Me.tpl_count + 1
					Execute( "call " & callback & "(tpl_nloopstr,tpl_repStr,tpl_l_d,tpl_loopCounter,file,tpl_bool,tpl_dict_label)" )
				end if
			end if	
		elseif Me.isFolder( path ) then
							
			dim objFolder,objFiles,i,fileList
			set objFolder=POP_MVC.fso.GetFolder( POP_MVC.realPath(path) )
		
			set objFiles=objFolder.Files
			
			for each file in objFiles
				call files_map( file.Path , callback )
			next 
			set objFiles=nothing				
			Set objFiles = objFolder.SubFolders	  
			For Each file In objFiles	
				Call files_map( file.Path , callback  )
			Next
			set objFiles=nothing	
			set objFolder=nothing
		end if
		call L_( typename(Me) & ".files_map" )
	end sub
	
	
	'仅对文件夹下的目录（不递归子目录）进行回调函数处理
	'只处理目录，不处理文件
	sub folder_map( byref path , ByRef callback )	
		on error resume next
		dim objFolder,file,objFiles
		set objFolder=POP_MVC.fso.GetFolder( POP_MVC.realPath(path) )
		Set objFiles = objFolder.SubFolders	  
		For Each file In objFiles			
			if not is_use_tpl_compiler then
				Execute( "call " & callback & "(file)" )
			else
				if Me.tpl_filter <> "" then
					Execute( "tplFilterBool = " & Me.tpl_filter )
					if tplFilterBool then
						Me.tpl_count = Me.tpl_count + 1
						Execute( "call " & callback & "(tpl_nloopstr,tpl_repStr,tpl_l_d,tpl_loopCounter,file,tpl_bool,tpl_dict_label)" )
					end if
				else
					Me.tpl_count = Me.tpl_count + 1
					Execute( "call " & callback & "(tpl_nloopstr,tpl_repStr,tpl_l_d,tpl_loopCounter,file,tpl_bool,tpl_dict_label)" )
				end if				
			end if			
		Next
		set objFiles = nothing
		set objFolder=nothing
		call L_( typename(Me) & ".folder_map" )
	end sub	
	
	'对文件夹(递归查找子目录)的所有目录进行回调函数处理
	'只处理目录，不处理文件
	sub folders_map( byref path , ByRef callback )
		on error resume next
		dim objFolder,file,objFiles,tplFilterBool
		set objFolder=POP_MVC.fso.GetFolder( POP_MVC.realPath(path) )
		Set objFiles = objFolder.SubFolders	  
		For Each file In objFiles			
			if not is_use_tpl_compiler then
				Execute( "call " & callback & "(file)" )
			else
				if Me.tpl_filter <> "" then
					Execute( "tplFilterBool = " & Me.tpl_filter )
					if tplFilterBool then
						Me.tpl_count = Me.tpl_count + 1
						Execute( "call " & callback & "(tpl_nloopstr,tpl_repStr,tpl_l_d,tpl_loopCounter,file,tpl_bool,tpl_dict_label)" )
					end if
				else
					Me.tpl_count = Me.tpl_count + 1
					Execute( "call " & callback & "(tpl_nloopstr,tpl_repStr,tpl_l_d,tpl_loopCounter,file,tpl_bool,tpl_dict_label)" )
				end if
			end if
			if file.SubFolders.count <> 0 then
				Call folders_map( file.Path , callback  )
			end if				
		Next
		set objFiles = nothing
		set objFolder=nothing
		call L_( typename(Me) & ".folders_map" )
	end sub
	
	
	
	'对文件夹(不递归查找子文件夹)的所有文件进行回调函数处理，并进行分页
	sub file_page_map( byref path , ByRef callback )	
		on error resume next
		if Not Me.isFolder(path) then
			Exit Sub
		end if
		dim objFolder,file,objFiles,oPage,arr,start_,end_
		set objFolder=POP_MVC.fso.GetFolder( POP_MVC.realPath(path) )		
		set objFiles=objFolder.Files

		if objFiles.count < 1 then
			exit sub
		end if
		
		if C_("PAGECLASS") <> "" then
			set oPage = P_(C_("PAGECLASS"))( objFiles.count )
		else
			set oPage = P_("page")( objFiles.count )
		end if
		
		arr = oPage.getRange
		start_ = arr(0)
		end_ = arr(1)
		For Each file In objFiles
			if files_counter >= start_ and files_counter <= end_ then
				if not is_use_tpl_compiler then
					Execute( "call " & callback & "(file)" )
				else
					if Me.tpl_filter <> "" then
						Execute( "tplFilterBool = " & Me.tpl_filter )
						if tplFilterBool then
							Me.tpl_count = Me.tpl_count + 1
							Execute( "call " & callback & "(tpl_nloopstr,tpl_repStr,tpl_l_d,tpl_loopCounter,file,tpl_bool,tpl_dict_label)" )
						end if
					else
						Me.tpl_count = Me.tpl_count + 1
						Execute( "call " & callback & "(tpl_nloopstr,tpl_repStr,tpl_l_d,tpl_loopCounter,file,tpl_bool,tpl_dict_label)" )
					end if					
				end if
			end if
			files_counter = files_counter + 1
		Next
		set objFiles = nothing
		set objFolder= nothing
		files_counter = 0
		Call L_( TyepName(Me) & ".file_page_map" )
	end sub  
  
	'对文件夹(不递归查找子目录)的所有目录进行回调函数处理
	'并进行分页处理
	sub folder_page_map( byref path , ByRef callback )	
		on error resume next
		dim objFolder,file,objFiles,oPage,arr,start_,end_
		set objFolder=POP_MVC.fso.GetFolder( POP_MVC.realPath(path) )		
		set objFiles=objFolder.SubFolders	

		if objFiles.count < 1 then
			exit sub
		end if
		
		if C_("PAGECLASS") <> "" then
			set oPage = P_(C_("PAGECLASS"))( objFiles.count )
		else
			set oPage = P_("page")( objFiles.count )
		end if
		
		arr = oPage.getRange
		start_ = arr(0)
		end_ = arr(1)		
			
		For Each file In objFiles
			if folders_counter >= start_ and folders_counter <= end_ then
				if not is_use_tpl_compiler then
					Execute( "call " & callback & "(file)" )
				else
					if Me.tpl_filter <> "" then
						Execute( "tplFilterBool = " & Me.tpl_filter )
						if tplFilterBool then
							Me.tpl_count = Me.tpl_count + 1
							Execute( "call " & callback & "(tpl_nloopstr,tpl_repStr,tpl_l_d,tpl_loopCounter,file,tpl_bool,tpl_dict_label)" )
						end if
					else
						Me.tpl_count = Me.tpl_count + 1
						Execute( "call " & callback & "(tpl_nloopstr,tpl_repStr,tpl_l_d,tpl_loopCounter,file,tpl_bool,tpl_dict_label)" )
					end if						
				end if
			end if
			folders_counter = folders_counter + 1
		Next
		set objFiles = nothing
		set objFolder= nothing
		folders_counter = 0
		Call L_( TyepName(Me) & ".folder_page_map" )
	end sub
	
	'从一个文件夹中随机取一个文件，返回文件名，而非路径
	'iaspcms后台登陆界面的随机图片使用了该函数
	function getRandFileName( ByRef dirPath )
		dim objFolder,objFiles,i,rnd,cnt,file
		set objFolder = POP_MVC.fso.GetFolder( realPath(dirPath) )
		Set objFiles = objFolder.Files	
		cnt = objFiles.count
		rnd = POP_MVC.String.Rand(  1 , cnt )
		i = 1
		for each file in objFiles
			if i = rnd then
				getRandFileName = file.name
			end if
			i = i + 1
		next
		set objFiles = nothing
		set objFolder = nothing
	end function
	
	'生成树状文件组织结构图
	'path:要展示文件树的目录
	'hideFolders:不显示子目录与文件的目录
	'showFilesCount:要显示的文件个数，负数取全部，0只显示目录不显示文件，正数则取N个文件
	'示例1: var_export POP_MVC.file.getFilesMap("./admin123/popasp" , Array( "./admin123/popasp/kindeditor" ) ,-1 )  该示例取全部文件，但是不显示kindeditor目录中的文件
	'var_export POP_MVC.file.getFilesMap("./admin123/popasp" , Array( "./admin123/popasp/kindeditor" ) ,2 ) 该示例只取一个文件夹中前2个文件
	function getFilesMap( byval path , ByVal hideFolders, ByVal showFilesCount )
		dim i
		path = realPath(path)
		FilesMapStr = baseName( path )
		FilesLevel	= ubound( split( path , "\") ) 
		FilesLastFolder = Array()
		FilesRootPath = lcase(path)
		

		if typename( hideFolders ) = "String" then
			hideFolders = split( hideFolders , "," )
		end if
		if isArray( hideFolders ) then
			for i = 0 to ubound( hideFolders ) 
				hideFolders(i) = realPath( hideFolders(i) )
			next
		end if

		Call map4getFilesMap( path , hideFolders , showFilesCount )
		getFilesMap = FilesMapStr
	end function
	
	
	'生成树状文件组织结构图的核心函数
	sub map4getFilesMap( ByVal path , ByRef hideFolders, ByRef showFilesCount )
		on error resume next
		dim objFolder,file,objFiles,objFolders,i,cnt,level,str,j,temp,str2,level2,bool

		if NOT POP_MVC.Arr.iExists(  hideFolders , path  ) then
			set objFolder = POP_MVC.fso.GetFolder(path)
			Set objFiles = objFolder.Files	
			Set objFolders = objFolder.SubFolders
			
			if showFilesCount <> 0 then
				i = 1
				cnt = objFolders.count
				For Each file In objFolders
					if i = cnt then					
						POP_MVC.Arr.push FilesLastFolder , file.path			
					end if
					i = i + 1
				Next 
				
				cnt = objFiles.count
				i = 1
				str = ""
				bool = false
				For Each file In objFiles
					str2 = ""				
					if i = 1 then
						level = ubound( split( file.path , "\" ) )
						
						if LCase(realDir(file.path)) <> FilesRootPath then
							temp = file.path
							
							for j = FilesLevel to level-1  step 1					
								temp = realDir(temp)
								if lcase(temp) = LCase(FilesLastFolder(0)) then
									bool = true
									str =  "    " & str
								elseif j = FilesLevel and POP_MVC.Arr.Exists( FilesLastFolder , temp ) then
									str =  "    " & str
								elseif POP_MVC.Arr.Exists( FilesLastFolder , temp ) and lcase(temp) <> FilesRootPath then
									str =  "    " & str
								else
									str =  "│   " & str
								end if						
							next
							if bool then
								str = mid( str , 5 )
							else
								str = mid( str , 1 , 4 ) & mid( str , 9 )
							end if
						else
							str = ""
						end if
					end if				
				
					if i = showFilesCount + 1 and objFolders.count = 0 then
						str2 = str & "└── "
					elseif i = cnt then	
						if objFolders.count = 0 then
							str2 = str & "└─ "
						else
							str2 = str & "├─ "
						end if					
					else
						str2 = str & "├─ "
					end if
					
					if showFilesCount > 0 then
						if i = showFilesCount + 1 then
							FilesMapStr = FilesMapStr & vbcrlf & str2 & "[ 尚有 " & ( cnt - i + 1 ) & "/" & cnt & " 个文件按要求未显示 ]" 
							exit for
						else
							FilesMapStr = FilesMapStr & vbcrlf & str2 & file.name
						end if
					else
						FilesMapStr = FilesMapStr & vbcrlf & str2 & file.name
					end if				
								
					i = i + 1
				Next 
			end if
			
			i = 1
			cnt = objFolders.count
			str = ""
			For Each file In objFolders
				str2 = ""
				if i = 1 then
					level2 = ubound( split( file.path , "\" ) )
					temp = file.path
					for j = FilesLevel to level2 - 2 step 1						
						temp = realDir(temp)
						
						if POP_MVC.Arr.Exists( FilesLastFolder , temp ) and lcase(temp) <> FilesRootPath then
							str =  "    " & str
						else
							str =  "│   " & str
						end if					
					next
				end if
				
				if i = cnt then					
					str2 = str & "└─ "
					POP_MVC.Arr.push FilesLastFolder , file.path
				else
					str2 = str & "├─ "					
				end if
				FilesMapStr = FilesMapStr & vbcrlf & str2 & file.name 
				Call map4getFilesMap( file.path , hideFolders , showFilesCount )
				i = i + 1
			Next 			
			
			
			set objFiles = nothing			
			set objFolders = nothing
		else
			FilesMapStr = FilesMapStr & " [ 按要求不展开 ]"
		end if
		call L_( typename(Me) & ".map4getFilesMap" )
	end sub
	
	'获取一个目录下面的所有文件属性(不递归)，如果仅一个属性则回一维数组，如果是多个属性则返回二维数组
	'getFileArr2后面的2仅指二维数组的意思
	'folder为目录
	'attrs为文件属性，比如"name,path,size"，或者Array( "name" , "path" , "size" )
	Function getFileArr2( Byval folder , ByVal attrs , ByRef filter )
		dim filePath,objFolder,file,objFiles,i,fileList,filterBool
				
		filePath= POP_MVC.realPath(folder)
		
		set objFolder=POP_MVC.fso.GetFolder(filePath)
		set objFiles=objFolder.Files
		
		if isArray( attrs ) then attrs = join( attrs,"," )
		
		attrs = replace( attrs , " " , "" )
		attrs = POP_MVC.String.trim( attrs , "," )
		attrs = replace( attrs , "," , ",file." )
		attrs = "file." & attrs
		
		if inStr( attrs, "," ) > 0 then
			attrs = "Array(" & attrs & ")"
		end if
		
		redim  fileList(0)
		i=0
		if isNul( filter ) then
			for each file in objFiles	
				ReDim Preserve fileList(i)
				execute( "fileList(i) = " & attrs  )
				i=i + 1
			next
		else
			for each file in objFiles	
				Execute( "filterBool = " & filter )
				if filterBool then
					ReDim Preserve fileList(i)
					execute( "fileList(i) = " & attrs  )
					i=i + 1
				end if
			next
		end if
		
		set objFiles=nothing
		set objFolder=nothing
		getFileArr2=fileList
		Call L_("POPASP_FILE.getFileArr2")
	End Function
	
	
	'获取一个目录下面的所有文件文件属性(不递归)，返回二维Dictionary对象
	'getFileDict2后面的2仅指二Dictionary的意思
	'folder为目录
	'attrs为文件属性，比如"name,path,size"
	Function getFileDict2( Byval folder , ByVal attrs , ByRef filter )
		on error resume next
		dim filePath,objFolder,file,objFiles,i,fileList,dict,cnt,filterBool

		filePath= POP_MVC.realPath(folder)
		
		set objFolder=POP_MVC.fso.GetFolder(filePath)
		set objFiles=objFolder.Files
		
		if typename( attrs ) = "String" then
			attrs = replace( attrs , " " , "" )
			attrs = split( attrs, "," )
		end if
		
		i=0
		set fileList = POP_MVC.SCD		
		cnt = ubound( attrs )
		
		if isNul( filter ) then
			for each file in objFiles
				set dict = POP_MVC.SCD
		
				for i = 0 to cnt
					execute( "dict.add attrs(i) , file." & attrs(i) )
				next
				fileList.add fileList.count,dict
			next 
		else
			for each file in objFiles
				Execute( "filterBool = " & filter )
				if filterBool then
					set dict = POP_MVC.SCD
					for i = 0 to cnt
						execute( "dict.add attrs(i) , file." & attrs(i) )
					next

					fileList.add fileList.count,dict
				end if
			next 
		end if

		set objFiles=nothing
		set objFolder=nothing
		set getFileDict2 = fileList
		Call L_("POPASP_FILE.getFileDict2")
	End Function	
	
	
	'获取一个目录下面的所有文件名，并返回一维数组
	Function getFileNames( ByRef folder )
		getFileNames = getFileArr2( folder , "name" , "" )
	End Function
	
	'获取一个目录下面的所有子文件名，并返回一维数组
	Function getFolderNames( ByRef cDir )
		dim filePath,objFolder,objSubFolder,objSubFolders,i
		i=0
		redim  folderList(0)
		filePath= POP_MVC.realPath(cDir)
		
		set objFolder=	POP_MVC.fso.GetFolder(filePath)
		set objSubFolders=objFolder.Subfolders
			for each objSubFolder in objSubFolders
			ReDim Preserve folderList(i)
			
			With objSubFolder
				folderList(i)=.name
			End With
			i=i + 1 
		next 
		set objFolder=nothing
		set objSubFolders=nothing
		getFolderNames=folderList
	End Function
	
    '得到一个目录下面的所有文件，不递归
	'返回Array( "文件名,大小,修改时间,路径" ,... )
	Function getFileList(Byval cDir)
		dim filePath,objFolder,objFile,objFiles,i,fileList
		i=0
		redim  fileList(0)
		filePath= POP_MVC.realPath(cDir)
		set objFolder=POP_MVC.fso.GetFolder(filePath)
		set objFiles=objFolder.Files
		for each objFile in objFiles			
			ReDim Preserve fileList(i)
			With objFile						
				fileList(i)=.name&","&Mid(.name, InStrRev(.name, ".") + 1)&","&.size/1000&"KB,"&.DateLastModified&","&cDir&"/"&.name
			End With
			i=i + 1 
		next 
		set objFiles=nothing
		set objFolder=nothing
		getFileList=fileList
	End Function
	
	'获取一个目录下面所有文件的路径
	Function getFilesPath(  byref path  )	
		on error resume next
		dim file
		if Me.isFile( path ) then
			if isEmpty( getFilesPath ) then
				getFilesPath = POP_MVC.realPath(path)
			else
				getFilesPath = getFilesPath & "," & POP_MVC.realPath(path)
			end if			
		elseif Me.isFolder( path ) then
			dim objFolder,objFiles,i,fileList
			set objFolder=POP_MVC.fso.GetFolder( POP_MVC.realPath(path) )
			set objFiles=objFolder.Files
			for each file in objFiles
				if isEmpty( getFilesPath ) then
					getFilesPath = file.Path
				else
					getFilesPath = getFilesPath & "," & file.Path
				end if
			next 
			set objFiles=nothing				
			Set objFiles = objFolder.SubFolders	  
			For Each file In objFiles		
				getFilesPath = getFilesPath & "," & getFilesPath( file.Path )
			Next
			set objFiles=nothing	
			set objFolder=nothing
		end if
		call L_( typename(Me) & ".getFilesPath" )
	end Function	
	
    '得到一个目录下面的所有目录，不递归
	'返回Array( "目录名,大小,修改时间,路径" ,... )
	Function getFolderList(Byval cDir)
		dim filePath,objFolder,objSubFolder,objSubFolders,i
		i=0
		redim  folderList(0)
		filePath= POP_MVC.realPath(cDir)
		
		set objFolder=	POP_MVC.fso.GetFolder(filePath)
		set objSubFolders=objFolder.Subfolders
			for each objSubFolder in objSubFolders
			ReDim Preserve folderList(i)
			With objSubFolder
				folderList(i)=.name&",文件夹,"&.size/1000&"KB,"&.DateLastModified&","&cDir&"/"&.name
			End With
			i=i + 1 
		next 
		set objFolder=nothing
		set objSubFolders=nothing
		getFolderList=folderList
	End Function	
	
	
	'压缩access数据库
	Public Property Get AccessCompress( ByVal dbPath )
		on error resume next
		dim key,item
		
		Call POP_MVC.CloseMvcDbClass
		for each key in POP_MVC.dClass
			if instr( key, "POPASP_DATABASE_TOOL" ) > 0 OR instr( key, "POPASP_ACCESS" ) > 0 then
				set POP_MVC.dClass( key ) = nothing
				POP_MVC.dClass.remove(key)
			end if
		next
		
		if dbPath = "" or isNull( dbPath ) then dbPath = C_("DB_PATH")	
	
		if NOT POP_MVC.isInstall( "JRO.JetEngine" ) Then
			POP_MVC.exit( "服务器上缺少组件，不能完成压缩" )
		end if
		dim obj,tempDbPath
		set obj = POP_MVC.SCO( "JRO.JetEngine" )
		tempDbPath = "./__popasp_temp_db_popasp__.asp"
			
		obj.CompactDatabase "Provider=Microsoft.Jet.OLEDB.4.0;Data Source=" & POP_MVC.realPath(dbPath),"Provider=Microsoft.Jet.OLEDB.4.0;Data Source="& POP_MVC.realPath(tempDbPath)
	
		if err.number <> 0 then
			POP_MVC.exit( L_("") )
		end if
		Call Me.moveFile( tempDbPath,dbPath )

		set obj = nothing
	End Property
	
	'备份数据库
	'仅适用于文件型数据库
	Public Sub AccessBackup( ByVal dbPath , ByVal backFolder )
		dim key,item
		Call POP_MVC.CloseMvcDbClass
		for each key in POP_MVC.dClass
			if instr( key, "POPASP_DATABASE_TOOL" ) > 0 OR instr( key, "POPASP_ACCESS" ) > 0 then
				set POP_MVC.dClass( key ) = nothing
			end if
		next	
		if dbPath = "" or isNull( dbPath ) then dbPath = C_("DB_PATH")		
		dim dirname,backname,extname		
		extname = Me.extname( dbPath )
		dirname = Me.dir( dbPath )
		if backFolder = "" or isNull( backFolder ) then backFolder = dirname
		backFolder = POP_MVC.realPath( backFolder )
		backname = backFolder & "\" & POP_MVC.formatdate( now() , "YYYYMMDDHHIISS" ) & "_bak" & extname
		
		Call Me.copyfile( dbPath, backname )	
	End Sub	
	
	'获取文件类型，目前只能判断图片文件img与文本文件txt
	Function getFileType(Byref path)
		dim filetype,imgFileStr,pageFileStr
		filetype=lcase(mid(path,instrrev(path,".")))
		imgFileStr=".jpg|.jpeg|.gif|.bmp|.png"
		pageFileStr =".html|.htm|.js|.css|.txt|.xml|.wml"
		if instr(imgFileStr,filetype)>0 then getFileType="img" : Exit Function
		if instr(pageFileStr,filetype)>0 then getFileType="txt" : Exit Function
	End Function
	
	'获取文件类型，返回中文名
	Function getFileType4cn( Byval filename )
		dim filetype,imgFileStr,ret
		filename = LCase( filename )
		filetype = mid(filename,instrrev(filename,"."))
		
		imgFileStr=".jpg|.jpeg|.gif|.bmp|.png"
		if inStr(imgFileStr,filetype) > 0 then 
			ret = "图片文件"
		elseif filetype = ".css" then
			ret = "样式文件"
		elseif filetype = ".js" then
			ret = "js脚本文件"
		elseif filetype = ".xml" then
			ret = "xml文档"
		elseif filetype = ".wml" then
			ret = "手机网页"
		elseif filetype = ".html" or filetype = ".htm" then
			ret = "静态页面文件"		
		elseif filetype = ".asp" then
			ret = "asp脚本文件"
		else
			ret = "其他文件"
		end if		
		getFileType4cn = ret
	End Function
	
	'制作桌面url文件
	Function makeDesktopUrl( url, filename , iconUrl )
		dim Shortcut
		Shortcut = "[InternetShortcut] " & vbCrLf
		Shortcut = Shortcut & "URL=" & url & vbCrLf
		Shortcut = Shortcut & "IDList=" & vbCrLf
		if not isNul( iconUrl ) then
			Shortcut = Shortcut & "IconFile=" & iconUrl & vbCrLf
		end if
		Shortcut = Shortcut & "[{000214A0-0000-0000-C000-000000000046}] " & vbCrLf
		Shortcut = Shortcut & "Prop3=19,2 " & vbCrLf
		Shortcut = Shortcut & " " & vbCrLf
		Response.AddHeader "Content-Disposition", "attachment;filename=" & filename & ".url;"
		Response.ContentType = "application/octet-stream"
		Response.Write Shortcut
		response.end
	End Function
End Class
%>