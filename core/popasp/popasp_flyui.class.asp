<%
' +----------------------------------------------------------------------
' | POPASP [ ASP MVC ]
' +----------------------------------------------------------------------
' | Copyright (c) 2016 http://popasp.com All rights reserved.
' +----------------------------------------------------------------------
' | Author: popasp <1737025626@qq.com>
' +----------------------------------------------------------------------
Class POPASP_FLYUI
	public separator
	public ExtendClass
	public inputDisplay
	private totalHtml
	public hrLine
	
	Private Sub Class_Initialize	
		separator = "$"
		hrLine = "<hr class=""layui-border-orange"" />"
		inputDisplay = "inline"
	end sub
	
	
	
	'提示头
	public property get title(str)
		title = "<blockquote class=""layui-elem-quote"">" & str & "</blockquote>"	
	end property
	
	'表单input，仅作提示用，<input type="text" name="字段名" id="ID值" />
	public function inputTip( byVal title,byVal tip )
		dim html
		html = getFormItem( ExtendClass )
        html = html & getLabel( title )
        html = html & "	<div class=""layui-input-block"">" & vbcrlf & vbtab & vbtab & vbtab
        html = html & "		<input readonly autocomplete=""off"" type=""text"" "
		html = html & "value=""" & tip & """ class=""layui-input"">" & vbcrlf & vbtab & vbtab & vbtab		
		html = html & "	</div>" & vbcrlf & vbtab & vbtab & vbtab
        html = html & "</div>"  & vbcrlf
		inputTip = html
	End function
	
	'返回按钮
	Public Property Get GoBack()
		GoBack = "<button type='button' class=""layui-btn layui-btn-primary"" onclick=""history.go(-1)""" & ">返回</button>"	
	End Property
	
	'保存用html
	Public Function SubmitBack( ByVal title, ByVal filter)
		dim html
		html = getFormItem( "" )
        html = html & "    <div class=""layui-input-block"">" & vbcrlf
        html = html & "        <button type='submit' class=""layui-btn layui-btn-normal"" alert=""alert"" lay-submit " & iif( not isNul(filter) , "lay-filter=""" & filter & """" , "lay-filter=""*""" ) & ">" & title  & "</button>" & vbcrlf 
        html = html & "        <button type='button' class=""layui-btn layui-btn-primary"" onclick=""history.go(-1)""" & ">返回</button>" & vbcrlf 
        html = html & "    </div>" & vbcrlf
        html = html & "</div>" & vbcrlf
		SubmitBack = html
	End Function
	
	'保存用html
	Public Function Submit( ByVal title, ByVal filter)
		dim html
		html = getFormItem( "" )
        html = html & "    <div class=""layui-input-block"">" & vbcrlf
        html = html & "        <button type='submit' class=""layui-btn layui-btn-normal"" alert=""alert"" lay-submit " & iif( not isNul(filter) , "lay-filter=""" & filter & """" , "lay-filter=""*""" ) & ">" & title  & "</button>" & vbcrlf 
        html = html & "    </div>" & vbcrlf
        html = html & "</div>" & vbcrlf
		Submit = html
	End Function

	'保存用html
	Public Property Get Save( ByVal filter)
		Save = Submit( "确认保存" ,filter )
	End Property
	
	'已阅用html
	Public Property Get Read( ByVal filter)
		Read = Submit( "已阅" , filter )
	End Property

	
	'操作按钮
	Public Function btnDeal( ByVal title,ByVal page,  ByVal pageID, ByVal pageFor  )
		dim icon
		if isNul( pageFor ) then
			pageFor = "id"
		end if
		
		if title = "复制" then
			icon = "<i class=""fa fa-files-o"" aria-hidden=""true""></i>"
			btnDeal = "<a class=""layui-btn layui-btn-xs layui-btn-primary cms-deal-line"" data-href='" & URL__ & page & "&" & pageFor & "=" & pageID & "' title=""" & title & """"">" & icon & "</a>"
		elseif title = "添加" then
			icon = "<i class=""layui-icon layui-icon-add-circle""></i>"
			btnDeal = "<a class=""layui-btn layui-btn-xs layui-btn-primary cms-deal-line"" data-href='" & URL__ & page & "&" & pageFor & "=" & pageID & "' title=""" & title & """"">" & icon & "</a>"
		elseif title = "恢复此备份" then
			icon = "<i class=""fa fa-files-o"" aria-hidden=""true""></i>"
			btnDeal = "<a class=""layui-btn layui-btn-xs layui-btn-primary cms-deal-line"" data-href='" & URL__ & page & "&" & pageFor & "=" & pageID & "' title=""" & title & """"">" & icon & "</a>"
		elseif title = "删除" or title = "移除" then
			icon = "<i class=""fa fa-remove""></i>"
			btnDeal = "<a class=""layui-btn layui-btn-xs layui-btn-danger cms-remove-line"" data-href='" & URL__ & page & "&" & pageFor & "=" & pageID & "' title=""" & title & """"">" & icon & "</a>"	
		elseif title = "完全删除" then
			icon = "<i class=""fa fa-recycle"" aria-hidden=""true""></i>"
			btnDeal = "<a class=""layui-btn layui-btn-xs layui-btn-danger cms-remove-line"" data-href='" & URL__ & page & "&" & pageFor & "=" & pageID & "' title=""" & title & """"">" & icon & "</a>"	
		end if

	End Function
	
	'添加按钮
	Public Function btnAdd( title, tip  )
		if isNul( title ) then
			title = "添加"
		end if
		
		if isNul( tip ) then
			tip = "添加"
		end if
		btnAdd = "<button class=""layui-btn layui-btn-normal layui-btn-sm"" lay-event=""add"" data-href=""" & URL__ & "add"" title=""" & title & """> <i class=""layui-icon layui-icon-add-circle""></i> " & tip & " </button>"
	End Function
	
	'全部删除按钮html
	Public Function btnAllRemove( title, tip  )
		if isNul( title ) then
			title = "删除"
		end if
		
		if isNul( tip ) then
			tip = "删除"
		end if
		btnAllRemove = "<button class=""layui-btn layui-btn-sm layui-btn-danger data-delete-btn"" lay-event=""delete"" data-href="""  & URL__ & "do_remove"" title=""" & title & """ > <i class=""layui-icon layui-icon-delete""></i> " & tip & " </button>"
	End Function
	
	'全部删除按钮html
	Public Function btnAllDel( url ,title )
		if isNul( title ) then
			title = "删除"
		end if
		btnAllDel = "<button class=""layui-btn layui-btn-sm layui-btn-danger data-delete-btn"" lay-event=""delete"" data-href="""  & URL__ & url & """ title=""" & title & """ > <i class=""layui-icon layui-icon-delete""></i> " & title & " </button>"
	End Function
	
	'行的删除html
	Public Function btnRemove( ByVal pageID, ByVal pageFor  )
		dim icon
		if isNul( pageFor ) then
			pageFor = "id"
		end if
		btnRemove = "<a class=""layui-btn layui-btn-xs layui-btn-danger cms-remove-line"" data-href='" & URL__ & "do_remove&" & pageFor &  "=" & pageID & "' title=""删除""><i class=""fa fa-remove""></i></a>"	
	End Function	
	
	'搜索用html
	Public Property Get Search( ByVal filter)
		dim html
        html = html & "    <div class=""layui-inline"">" & vbcrlf
        html = html & "        <button class=""layui-btn layui-btn-primary"" alert=""alert"" lay-submit " & iif( not isNul(filter) , "lay-filter=""" & filter & """" , "lay-filter=""*""" ) & ">搜索</button>" & vbcrlf 
        html = html & "    </div>" & vbcrlf
		Search = html
	End Property
	
	'搜索用html
	Public Function url( title, uri )
		dim html
        html = html & "    <div class=""layui-inline"">" & vbcrlf
        html = html & "        <button class=""layui-btn layui-btn-primary"" alert=""alert"" onclick=""location.href='?" & uri & "'"" " & ">" & title & "</button>" & vbcrlf 
        html = html & "    </div>" & vbcrlf
		url = html
	End Function
	
	'__URL__switch_status链接
	Public Function SwitchStatus( ByVal status, ByRef id )
		dim html
		
		if typename(status) = "Boolean" then
			if status then
				status = 1
			else
				status = 0
			end if
		end if
		
		if status = 1 then
			html= "<a style=""cursor:pointer"" class=""cms-switchStatus-line"" data-status=1 data-href=""" & URL__ & "switch_status&id="" data-id=""" & id & """><i style=""color:#5FB878"" class=""layui-icon layui-icon-ok"" title=""启用""></i></a>"
		else
			html= "<a style=""cursor:pointer"" class=""cms-switchStatus-line"" data-status=0 data-href=""" & URL__ & "switch_status&id="" data-id=""" & id & """><i style=""color:#FF5722"" class=""layui-icon layui-icon-close"" title=""禁用""></i></a>"
		end if
		SwitchStatus = html
	End Function
	
	'status
	Public Function Status( ByVal statusValue, ByRef id )
		dim html
		
		if typename(statusValue) = "Boolean" then
			if statusValue then
				statusValue = 1
			else
				statusValue = 0
			end if
		end if
		
		if statusValue = 1 then
			html= "<i style=""color:#5FB878"" class=""layui-icon layui-icon-ok-circle""></i>"
		else
			html= "<i style=""color:#FF5722"" class=""layui-icon layui-icon-close-fill""></i>"
		end if
		Status = html
	End Function
	
	'页面标题头
	'每项之间用||分隔，项目内标题与链接用|分隔，如下所示
	'添加场景||场景列表|?Scene
	public property get TabTitle(  byVal arg  )
		dim arr,html,item
		if not isArray(arg) then
			arg = split(arg, "||")
		end if
		
		html = html & "<ul class=""layui-tab-title"">"
		
		for i = 0 to ubound(arg)
			item = arg(i)
			
			if i = 0 then
				html = html & "<li class=""layui-this"">" & item & "</li>"
			else			
				if inStr( item, "|" ) > 0 then
					arr  = split( item , "|" )
					html = html & "<li><a href=""" & arr(1) & """>" & arr(0) & "</a></li>"
				else					
					html = html & "<li>" & item & "</li>"				
				end if			
			end if
		next	
		
		html = html & "</ul>"
		TabTitle = html
	end property
	
	
	
	'表单input，<input type="text" name="字段名" id="ID值" />
	'arg可传入四个参数
	'字段名$ID值$标题$说明$readonly$value
	public property get input( byVal arg )
		dim html,title,fieldName,id,tip,readonly,value
		readonly = 0
		if not isArray(arg) then
			arg = split(arg, separator)
		end if
		
		value = ""
		
		
		if ubound( arg ) > -1 then fieldName = arg(0)
		if ubound( arg ) > 0 then id = arg(1)
		if ubound( arg ) > 1 then title = arg(2)
		if ubound( arg ) > 2 then tip = arg(3)
		if ubound( arg ) > 3 then readonly = arg(4)
		if ubound( arg ) > 4 then value = arg(5)
		

		html = getFormItem( ExtendClass )
        html = html & getLabel( title )
        html = html & "	<div class=""layui-input-" & inputDisplay & """>" & vbcrlf & vbtab & vbtab & vbtab
        html = html & "		<input autocomplete=""off"" type=""text"" "
		if fieldName <> "" then
			html = html & "name=""" & fieldName & """ "
		end if
		if id<>"" then
			html = html & "id=""" & id & """ " 
		end if
		
		if isNul(readonly) then
			readonly = 0
		end if
		
		if readonly -1 =0 then
			html = html & "readonly" 
		end if
		html = html & " value=""" & value & """ class=""layui-input"">" & vbcrlf & vbtab & vbtab & vbtab
		
		html = html & "	</div>" & vbcrlf & vbtab & vbtab & vbtab
		html = html & getTip( title , tip )
        html = html & "</div>"  & vbcrlf
		if tip <> "" then
			html = html & hrLine
		end if
		input = html
	End property
	
	'表单input，<input type="text" name="字段名"  />
	'arg可传入四个参数
	'字段名$颜色值$标题$说明$readonly
	public property get color( byVal arg )
		dim html,title,fieldName,colorValue,tip,readonly
		readonly = 0
		if not isArray(arg) then
			arg = split(arg, separator)
		end if
		
		
		if ubound( arg ) > -1 then fieldName = arg(0)
		if ubound( arg ) > 0 then colorValue = arg(1)
		if ubound( arg ) > 1 then title = arg(2)
		if ubound( arg ) > 2 then tip = arg(3)
		if ubound( arg ) > 3 then readonly = arg(4)
		html = getFormItem( ExtendClass )
        html = html & getLabel( title )
        html = html & "	<div class=""layui-input-inline"">" & vbcrlf & vbtab & vbtab & vbtab
        html = html & "		<input autocomplete=""off"" type=""text"" "
		if fieldName <> "" then
			html = html & "name=""" & fieldName & """ "
		end if

		if readonly -1 =0 then
			html = html & "readonly" 
		end if
		html = html & "value=""" & colorValue & """ class=""layui-input"">" 
		
		html = html & "	</div>" & vbcrlf & vbtab & vbtab & vbtab
		
		html = html & "<div class='layui-input-inline'><span class='layui-btn layui-btn-primary select-color-" & fieldName & "' style='padding:0 12px;min-width:45px;background-color: " & colorValue & ";'></span></div>" & vbcrlf & vbtab & vbtab & vbtab
		html = html & getTip( title, tip )
        html = html & "</div>"  & vbcrlf
		if tip <> "" then
			html = html & hrLine
		end if
		html = html & "<script>" & vbcrlf
		html = html & "$(function () {" & vbcrlf
		html = html & "	$('.select-color-" & fieldName & "').paigusu({" & vbcrlf
		html = html & "       color: $('input[name=""" & fieldName & """]').val()," & vbcrlf
		html = html & "   }, function (event, obj) {" & vbcrlf
		html = html & "       $(event).css('background-color', '#' + obj.hex);" & vbcrlf
		html = html & "       $('input[name=""" & fieldName & """]').val('#' + obj.hex);" & vbcrlf
		html = html & "   });" & vbcrlf
		html = html & "});" & vbcrlf 
		html = html & "</script>" & vbcrlf
		color = html
	End property
	
	'表单input，<input type="text" name="字段名"  />
	'arg可传入四个参数
	'字段名$初始值$标题$说明
	public property get star( byVal arg )
		dim html,title,fieldName,initValue,tip

		if not isArray(arg) then
			arg = split(arg, separator)
		end if
		
		
		if ubound( arg ) > -1 then fieldName = arg(0)
		if ubound( arg ) > 0 then initValue = arg(1)
		if ubound( arg ) > 1 then title = arg(2)
		if ubound( arg ) > 2 then tip = arg(3)
		html = getFormItem( ExtendClass )
        html = html & getLabel( title )
        html = html & "	<div class=""layui-input-inline"">" & vbcrlf & vbtab & vbtab & vbtab
        html = html & "		<input type=""hidden"" "
		if fieldName <> "" then
			html = html & "name=""" & fieldName & """ "
		end if

		html = html & "value=""" & initValue & """>" 
		
		html = html & "	</div>" & vbcrlf & vbtab & vbtab & vbtab
		
		html = html & "<div class='layui-inline' id='" & fieldName & "-Star'></div>" & vbcrlf & vbtab & vbtab & vbtab
		html = html & getTip( title, tip )
        html = html & "</div>"  & vbcrlf
		if tip <> "" then
			html = html & hrLine
		end if
		html = html & "<script>" & vbcrlf
		html = html & "layui.use(['rate'], function(){" & vbcrlf
		html = html & "  var $ = layui.jquery;" & vbcrlf
		html = html & "  var rate = layui.rate;" & vbcrlf
		html = html & "  rate.render({" & vbcrlf
		html = html & "    elem: '#" & fieldName & "-Star'" & vbcrlf
		html = html & "    ,length: 7" & vbcrlf
		html = html & "    ,value: " & initValue & vbcrlf
		html = html & "	,choose: function(value){" & vbcrlf
		html = html & "		$('input[name=""" & fieldName & """]').val(value);" & vbcrlf
		html = html & "	}" & vbcrlf
		html = html & "  });" & vbcrlf
		html = html & "});"  & vbcrlf
		html = html & "</script>" & vbcrlf
		star = html
	End property
	
	
	'表单date，<input type="date" name="字段名" id="ID值" />
	'arg可传入四个参数
	'字段名$初始值$标题$说明
	public property get uidate( byVal arg )
		dim html,title,fieldName,initValue,tip
		if not isArray(arg) then
			arg = split(arg, separator)
		end if
		
		if ubound( arg ) > -1 then fieldName = arg(0)
		if ubound( arg ) > 0 then initValue = arg(1)
		if ubound( arg ) > 1 then title = arg(2)
		if ubound( arg ) > 2 then tip = arg(3)
		
		if isNul( initValue ) then
			initValue = ""
		end if

		html = getFormItem( ExtendClass )
        html = html & getLabel( title )
        html = html & "	<div class=""layui-input-" & inputDisplay & """>" & vbcrlf & vbtab & vbtab & vbtab
        html = html & "		<input type=""text"" placeholder=""" & POP_MVC.config( "WDATE_DATE_FORMAT" ) & """ "
		if fieldName <> "" then
			html = html & "name=""" & fieldName & """ "
		end if

			html = html & "id=""" & fieldName & """ " 
		
			html = html & "value=""" & POP_MVC.FormatDate(initValue,C_( "WDATE_DATE_FORMAT" )) & """ class=""layui-input"" autocomplete=""off"">" & vbcrlf & vbtab & vbtab & vbtab
			html = html & "	</div>" & vbcrlf & vbtab & vbtab & vbtab
			html = html & getTip( title, tip )
			html = html & "</div>"  & vbcrlf
			
			html = html & "<script>" & vbcrlf
			html = html & "$(function(){" & vbcrlf
			html = html & "layui.use('laydate', function(){" & vbcrlf
			html = html & "	var laydate = layui.laydate;" & vbcrlf
			html = html & "	laydate.render({" & vbcrlf
			html = html & "	  elem: '#" & fieldName & "'" & vbcrlf
			html = html & "	});" & vbcrlf
			html = html & "});" & vbcrlf
			html = html & "});" & vbcrlf			
			html = html & "</script>" & vbcrlf			

		if tip <> "" then
			html = html & hrLine
		end if
		uidate = html
	End property
	
	'表单input，<input type="text" name="字段名" id="ID值" />
	'arg可传入四个参数
	'字段名$ID值$标题$说明
	public property get number( byVal arg )
		dim html,title,fieldName,id,tip
		if not isArray(arg) then
			arg = split(arg, separator)
		end if
		
		if ubound( arg ) > -1 then fieldName = arg(0)
		if ubound( arg ) > 0 then id = arg(1)
		if ubound( arg ) > 1 then title = arg(2)
		if ubound( arg ) > 2 then tip = arg(3)
		html = getFormItem( ExtendClass )
        html = html & getLabel( title )
        html = html & "	<div class=""layui-input-" & inputDisplay & """>" & vbcrlf & vbtab & vbtab & vbtab
        html = html & "		<input type=""text"" "
		if fieldName <> "" then
			html = html & "name=""" & fieldName & """ "
		end if
		if id<>"" then
			html = html & "id=""" & id & """ " 
		end if
		html = html & "value="""" class=""layui-input"">" & vbcrlf & vbtab & vbtab & vbtab
		html = html & "	</div>" & vbcrlf & vbtab & vbtab & vbtab
		html = html & getTip( title, tip )
        html = html & "</div>"  & vbcrlf
		number = html
	End property
	
	'表单input，<input type="text" name="字段名" id="ID值" />
	'arg可传入四个参数
	'字段名$ID值$标题$说明
	public property get password( byVal arg )
		dim html,title,fieldName,id,tip
		if not isArray(arg) then
			arg = split(arg, separator)
		end if
		
		if ubound( arg ) > -1 then fieldName = arg(0)
		if ubound( arg ) > 0 then id = arg(1)
		if ubound( arg ) > 1 then title = arg(2)
		if ubound( arg ) > 2 then tip = arg(3)
		html = getFormItem( ExtendClass )
        html = html & getLabel( title )
        html = html & "	<div class=""layui-input-" & inputDisplay & """>" & vbcrlf & vbtab & vbtab & vbtab
        html = html & "		<input type=""password"" "
		if fieldName <> "" then
			html = html & "name=""" & fieldName & """ "
		end if
		if id<>"" then
			html = html & "id=""" & id & """ " 
		end if
		html = html & "value="""" class=""layui-input"">" & vbcrlf & vbtab & vbtab & vbtab
		html = html & "	</div>" & vbcrlf & vbtab & vbtab & vbtab
		html = html & getTip( title, tip )
        html = html & "</div>"  & vbcrlf
		password = html
	End property
	
	'arg可传入四个参数
	'字段名$ID值$标题$说明$readonly
	public property get textarea( byVal arg )
		dim html,title,fieldName,id,tip,readonly
		readonly = 0
		if not isArray(arg) then
			arg = split(arg, separator)
		end if
		
		if ubound( arg ) > -1 then fieldName = arg(0)
		if ubound( arg ) > 0 then id = arg(1)
		if ubound( arg ) > 1 then title = arg(2)
		if ubound( arg ) > 2 then tip = arg(3)
		if ubound( arg ) > 3 then readonly = arg(4)
		html = html & "<div class=""layui-form-item layui-form-text"">" & vbcrlf & vbtab & vbtab & vbtab
        html = html & getLabel( title )
        html = html & "	<div class=""layui-input-block"">" & vbcrlf & vbtab & vbtab & vbtab
        html = html & "		<textarea class=""layui-textarea"" "
		if fieldName <> "" then
			html = html & "name=""" & fieldName & """ "
		end if
		if id<>"" then
			html = html & "id=""" & id & """ " 
		end if
		if readonly -1 =0 then
			html = html & "readonly " 
		end if
		html = html & " style=""height: 80px;""></textarea>" & vbcrlf & vbtab & vbtab & vbtab
		html = html & "	</div>" & vbcrlf & vbtab & vbtab & vbtab
		html = html & getTip( title ,  tip )
        html = html & "</div>"  
		textarea = html
	End property
	
	
	'上传
	'arg可传入四个参数
	'字段名$上传类型$标题$说明
	public Function TextareaSelect( ByVal url, byVal arg )
		dim html,title,fieldName,tip
		if not isArray(arg) then
			arg = split(arg, separator)
		end if
		
		if ubound( arg ) > -1 then fieldName = arg(0)
		if ubound( arg ) > 0 then title = arg(1)
		if ubound( arg ) > 1 then tip = arg(2)
		html = getFormItem( ExtendClass )
        html = html & getLabel( title )

		html = html & "		<button type='button' class='layui-btn cms-list-select' data-href='?" & url & "&target=" & fieldName & "' data-for='" & fieldName & "'>" 
	
		html = html & "选择"
		html = html & "</button>" & vbcrlf & vbtab & vbtab & vbtab
		
	
        html = html & "	<div class=""layui-block"">" & vbcrlf & vbtab & vbtab & vbtab
        html = html & "		<textarea class=""layui-textarea"" "
		if fieldName <> "" then
			html = html & "name=""" & fieldName & """ "
		end if
		if id<>"" then
			html = html & "id=""" & id & """ " 
		end if
		if readonly -1 =0 then
			html = html & "readonly " 
		end if
		html = html & "></textarea>" & vbcrlf & vbtab & vbtab & vbtab
        'html = html & "	<div class=""layui-input-inline"">" & vbcrlf & vbtab & vbtab & vbtab

		html = html & getTip( title ,  tip )
		'html = html & "	</div>" & vbcrlf & vbtab & vbtab & vbtab
		html = html & "	</div>" & vbcrlf & vbtab & vbtab & vbtab
        html = html & "</div>" & vbcrlf

			html = html & hrLine

		TextareaSelect = html	
	End Function
	
	'标签选择器
	public property get TagSelect( byVal arg )
		TagSelect = TextareaSelect( "c=Tag&a=SelectList" ,  arg )
	End property
	
	'标签选择器
	public property get RelatedContentSelect( byVal arg )
		RelatedContentSelect = TextareaSelect( "c=Content&a=SelectList" ,  arg )
	End property
	
	public property get UserSelect( byVal arg )
		dim html,title,fieldName,tip
		if not isArray(arg) then
			arg = split(arg, separator)
		end if
		
		if ubound( arg ) > -1 then fieldName = arg(0)
		if ubound( arg ) > 0 then title = arg(1)
		if ubound( arg ) > 1 then tip = arg(2)
		html = getFormItem( ExtendClass )
        html = html & getLabel( title )
        html = html & "	<div class=""layui-input-inline"">" & vbcrlf & vbtab & vbtab & vbtab
        html = html & "		<input type=""text"" "
		if fieldName <> "" then
			html = html & "name=""" & fieldName & """ "
		end if

		html = html & "id=""" & fieldName & """ " 

		html = html & "value="""" class=""layui-input"">" & vbcrlf & vbtab & vbtab & vbtab
		html = html & getTip( title ,  tip )
		html = html & "	</div>" & vbcrlf & vbtab & vbtab & vbtab
        'html = html & "</div>"  & vbcrlf & vbtab & vbtab & vbtab
		

		'html = getFormItem( ExtendClass )
        'html = html & "	<label class=""layui-form-label"" title=""上传" & title & """>" & title & "</label>" & vbcrlf & vbtab & vbtab & vbtab
		html = html & "	  <div class='layui-input-inline layui-btn-container' style='width: auto;'>"
		html = html & "		<button type='button' class='layui-btn cms-list-select' data-href='?c=User&a=SelectList&target=" & fieldName & "' >"
	
		html = html & "选择"
		html = html & "		</button>"		
		html = html & "	  </div>"
        html = html & "</div>" & vbcrlf

		html = html & hrLine

		UserSelect = html	
	End property
	
	'上传
	'arg可传入四个参数
	'字段名$上传类型$标题$说明
	public property get upload( byVal arg )
		dim html,title,fieldName,saveType,tip
		if not isArray(arg) then
			arg = split(arg, separator)
		end if
		
		if ubound( arg ) > -1 then fieldName = arg(0)
		if ubound( arg ) > 0 then saveType = arg(1)
		if ubound( arg ) > 1 then title = arg(2)
		if ubound( arg ) > 2 then tip = arg(3)
		html = getFormItem( ExtendClass )
        html = html & getLabel( title )
        html = html & "	<div class=""layui-upload"">" & vbcrlf & vbtab & vbtab & vbtab
        html = html & "	<label for='L_" & fieldName & "' class='layui-form-label' title=""" & title & """>" & title & "</label>" & vbcrlf & vbtab & vbtab & vbtab
        html = html & "		<button type=""button"" class=""layui-btn iaspcms-siteconfig-upload"">上传图片</button>"
        html = html & "		<input type=""hidden"" name=""" & fieldName & """ class=""iaspcms-siteconfig-input"" value="""" />"
        html = html & "		<div class=""layui-upload-list"">"
        html = html & "		<img class=""layui-upload-img"" src="""" style=""width: 230px; margin: 0 10px 10px 0;"">"
		html = html & "</div>" & vbcrlf & vbtab & vbtab & vbtab
		html = html & getTip( title ,  tip )
        html = html & "</div>" & vbcrlf
		upload = html	
	End property
	
	'上传
	'arg可传入四个参数
	'字段名$上传类型$标题$说明
	public property get uploads( byVal arg )
		dim html,title,fieldName,saveType,tip
		if not isArray(arg) then
			arg = split(arg, separator)
		end if
		
		if ubound( arg ) > -1 then fieldName = arg(0)
		if ubound( arg ) > 0 then saveType = arg(1)
		if ubound( arg ) > 1 then title = arg(2)
		if ubound( arg ) > 2 then tip = arg(3)
		html = getFormItem( ExtendClass )
        html = html & getLabel( title )
        html = html & "	<div class=""layui-upload"">" & vbcrlf & vbtab & vbtab & vbtab
        html = html & "	<label for='L_" & fieldName & "' class='layui-form-label' title=""" & title & """>" & title & "</label>" & vbcrlf & vbtab & vbtab & vbtab
        html = html & "		<button type=""button"" class=""layui-btn iaspcms-siteconfig-uploads"">上传图片</button>"
        html = html & "		<textarea placeholder="""" name=""" & fieldName & """ autocomplete=""off"" class=""layui-textarea iaspcms-siteconfig-textarea"" style=""height: 80px;""></textarea>"
		html = html & "</div>" & vbcrlf & vbtab & vbtab & vbtab
		html = html & getTip( title ,  tip )
        html = html & "</div>" & vbcrlf
		upload = html	
	End property
	
	'上传
	'arg可传入四个参数
	'字段名$上传类型$标题$说明
	public property get CodeUpload( byVal arg )
		dim html,title,fieldName,saveType,tip,id
		if not isArray(arg) then
			arg = split(arg, separator)
		end if
		
		if ubound( arg ) > -1 then fieldName = arg(0)
		if ubound( arg ) > 0 then saveType = arg(1)
		if ubound( arg ) > 1 then title = arg(2)
		if ubound( arg ) > 2 then tip = arg(3)
		html = getFormItem( ExtendClass )
        html = html & getLabel( title )

		id = fieldName

        'html = html & "</div>"  & vbcrlf & vbtab & vbtab & vbtab

		html = html & "		<button type='button' class='layui-btn layui-btn-primary cms-upload' data-type='" & saveType & "' data-for='" & fieldName & "'>"
		html = html & "		  <i class='layui-icon'>&#xe67c;</i>上传"
		html = html & "</button>" & vbcrlf & vbtab & vbtab & vbtab
		if not checkWap() then
			html = html & "		<button type='button' class='layui-btn layui-btn-primary cms-list-select' data-href='?c=File&a=upload_view&target=" & fieldName & "' data-for='" & fieldName & "'>" 
		
			html = html & "选择"
			html = html & "</button>" & vbcrlf & vbtab & vbtab & vbtab
		
		
			html = html & "		<button type='button' class='layui-btn layui-btn-primary cms-upload-view' data-for='" & fieldName & "'>查看图片</button >" & vbcrlf & vbtab & vbtab & vbtab
		
			html = html & "		<button type='button' class='layui-btn layui-btn-primary' onClick=""insertAtCursor( document.getElementById('" & id & "'), '$$$')"">" 
		
			html = html & "插入随机符（$$$）"
			html = html & "</button>" & vbcrlf & vbtab & vbtab & vbtab
		end if 
		
        html = html & "	<div class=""layui-block"">" & vbcrlf & vbtab & vbtab & vbtab
        html = html & "		<textarea class=""layui-textarea"" "
		if fieldName <> "" then
			html = html & "name=""" & fieldName & """ "
		end if
		if id<>"" then
			html = html & "id=""" & id & """ " 
		end if
		if readonly -1 =0 then
			html = html & "readonly " 
		end if
		html = html & "></textarea>" & vbcrlf & vbtab & vbtab & vbtab
        html = html & "	<div class=""layui-input-inline"">" & vbcrlf & vbtab & vbtab & vbtab

		html = html & getTip( title ,  tip )
		html = html & "	</div>" & vbcrlf & vbtab & vbtab & vbtab
		html = html & "	</div>" & vbcrlf & vbtab & vbtab & vbtab
        html = html & "</div>" & vbcrlf

			html = html & hrLine

		CodeUpload = html	
	End property
	
	'arg可传入四个参数
	'字段名$ID值$选项参数$标题$说明
	public property get [select]( byVal arg )
		dim html,title,fieldName,id,tip,options
		if not isArray(arg) then
			arg = split(arg, separator)
		end if
		
		if ubound( arg ) > -1 then fieldName = arg(0)
		if ubound( arg ) > 0 then id = arg(1)
		if ubound( arg ) > 1 then options = arg(2)
		if ubound( arg ) > 2 then title = arg(3)
		if ubound( arg ) > 3 then tip = arg(4)
		html = getFormItem( ExtendClass )
        html = html & getLabel( title )
        html = html & "	<div class=""layui-input-" & inputDisplay & """>" & vbcrlf & vbtab & vbtab & vbtab
        html = html & "		<select "
		if fieldName <> "" then
			html = html & "name=""" & fieldName & """ "
		end if
		
		if id<>"" then
			html = html & "id=""" & id & """ " 
		end if
		
		html = html & " lay-filter=""" & fieldName & """>" & vbcrlf & vbtab & vbtab & vbtab
		html = html & options & vbcrlf & vbtab & vbtab & vbtab
		html = html & "</select>" & vbcrlf & vbtab & vbtab & vbtab
		html = html & "	</div>" & vbcrlf & vbtab & vbtab & vbtab
		html = html & getTip( title ,  tip )
        html = html & "</div>"  & vbcrlf
		[select] = html
	End property
	
	'arg可传入四个参数
	'字段名$ID值$标题$说明
	Public Property get tplDirNames( arg )
		dim html,title,fieldName,id,tip,arr2
		if not isArray(arg) then
			arg = split(arg, separator)
		end if
		
		if ubound( arg ) > -1 then fieldName = arg(0)
		if ubound( arg ) > 0 then id = arg(1)
		if ubound( arg ) > 1 then title = arg(2)
		if ubound( arg ) > 2 then tip = arg(3)
		totalHtml = "<option>请选择</option>"
		Call POP_MVC.File.folder_map( "../templates" ,"P_(""layui"").getFileNameOptions" )
		
		POP_MVC.arr.push arr2, fieldName
		POP_MVC.arr.push arr2, id
		POP_MVC.arr.push arr2, totalHtml
		POP_MVC.arr.push arr2, title
		POP_MVC.arr.push arr2, tip
		
		tplDirNames = [select](arr2)
	End property	
	
	public function getFileNameOptions(file)
		getFileNameOptions = "<option value='" & file.name & "'>" & file.name & "</option>" &   vbcrlf & vbtab
		totalHtml = totalHtml & getFileNameOptions
	End Function
	
	
	
	'arg可传入四个参数
	'字段名$为1时提示$为0时提示$标题$说明
	'字段名$提示1|值1$提示2|值2$标题$说明
	public property get radio( byVal arg )
		dim html,title,fieldName,trueTip, falseTip ,tip,options,readonly,trueValue,falseValue,arr
		readonly = 0
		if not isArray(arg) then
			arg = split(arg, separator)
		end if
		
		trueValue = 1
		falseValue = 0
		
		
		if ubound( arg ) > -1 then fieldName = arg(0)
		if ubound( arg ) > 0 then trueTip = arg(1)
		if ubound( arg ) > 1 then falseTip = arg(2)
		if ubound( arg ) > 2 then title = arg(3)
		if ubound( arg ) > 3 then tip = arg(4)
		if ubound( arg ) > 4 then readonly = arg(5)
		
		if inStr( trueTip, "|" ) > 0 then
			arr = split( trueTip, "|" )
			trueTip = arr(0)
			trueValue = arr(1)
		end if
		
		if inStr( falseTip, "|" ) > 0 then
			arr = split( falseTip, "|" )
			falseTip = arr(0)
			falseValue = arr(1)
		end if
		
		html = getFormItem( ExtendClass )
        html = html & getLabel( title )
        html = html & "	<div class=""layui-input-inline"">" & vbcrlf & vbtab & vbtab & vbtab
        html = html & " <input type=""radio"" name=""" & fieldName & """ value=""" & trueValue & """ lay-filter=""" & fieldName & """ title=""" & trueTip & """ />"
        html = html & " <input type=""radio"" name=""" & fieldName & """ value=""" & falseValue & """ lay-filter=""" & fieldName & """ title=""" & falseTip & """ />"
		html = html & "	</div>" & vbcrlf & vbtab & vbtab & vbtab
		html = html & getTip( title ,  tip )
        html = html & "</div>"  & vbcrlf
		if tip <> "" then
			html = html & hrLine
		end if
		radio = html
	End property
	
	'开关
	'arg可传入四个参数
	'字段名$为1时提示$为0时提示$标题$说明
	public property get switch( byVal arg )
		dim html,title,fieldName,trueTip, falseTip ,tip,options,trueValue,flaseValue,arr
		if not isArray(arg) then
			arg = split(arg, separator)
		end if
		
		trueValue = 1
		falseValue = 0
		
		if ubound( arg ) > -1 then fieldName = arg(0)
		if ubound( arg ) > 0 then trueTip = arg(1)
		if ubound( arg ) > 1 then falseTip = arg(2)
		if ubound( arg ) > 2 then title = arg(3)
		if ubound( arg ) > 3 then tip = arg(4)
		
		html = getFormItem( ExtendClass )
        html = html & getLabel( title )
        html = html & "	<div class=""layui-input-inline"">" & vbcrlf & vbtab & vbtab & vbtab
        html = html & " <input type=""checkbox"" name=""" & fieldName & """ value=""1"" lay-skin=""switch"" lay-text=""" & iif(isNul(trueTip) , "ON" , trueTip ) & "|" & iif(isNul(trueTip) , "OFF" , falseTip ) & """ />"
		html = html & "	</div>" & vbcrlf & vbtab & vbtab & vbtab
		html = html & getTip( title ,  tip )
        html = html & "</div>"  & vbcrlf
		switch = html
	End property
	
	public property Get PageConfigTop( GroupIDS )
		dim rs,i,html
		set rs = B_("ConfigGroup").where( GroupIDS ).field("GroupName,GroupID").top(20).select
		i = 0
		do while not rs.eof
			html = html & "<li class=""" & iif(i=0,"layui-this","") & """ lay-id=""Group-" & rs("GroupID") & """>" & rs("GroupName") & "</li>"
			i = i + 1
			rs.MoveNext
		loop
		POP_MVC.unset( rs )
		PageConfigTop = html
	End property
	
	Public Function getConfigHtml( ConfigName , ConfigTitle , TrueText , FalseText , SelectOptions , ConfigInput , ConfigTip  )
		dim html
		if ConfigInput = "radio" then
			html = html & radio( Array( ConfigName , TrueText , FalseText , ConfigTitle , ConfigTip  ) )				
		elseif ConfigInput = "input" then
			html = html & input( Array( ConfigName , "" , ConfigTitle , ConfigTip  ) )				
		elseif ConfigInput = "number" then
			html = html & number( Array( ConfigName , "" , ConfigTitle , ConfigTip  ) )							
		elseif ConfigInput = "password" then
			html = html & password( Array( ConfigName , "" , ConfigTitle , ConfigTip  ) )	
		elseif ConfigInput = "upload" then
			html = html & upload( Array( ConfigName , "17" , ConfigTitle , ConfigTip  ) )
		elseif ConfigInput = "textarea" then
			html = html & textarea( Array( ConfigName , "" , ConfigTitle , ConfigTip  ) )
		elseif ConfigInput = "select" then
			html = html & [select]( Array( ConfigName , "" , SelectOptions , ConfigTitle , ConfigTip  ) )
		end if
		getConfigHtml = html
	End Function
	
	'配置表config中的配置
	public property Get configs( ConfigName )
		dim html,rs,where
		dim ConfigTitle,TrueText,FalseText,SelectOptions,ConfigInput,ConfigTip,GroupID
		set where = D_
		where("ConfigName") = ConfigName
		'html = html & "<div class=""layui-tab-item" & "" & """>"
		set rs = B_("Config").where(where).field("ConfigName,ConfigTitle,TrueText,FalseText,SelectOptions,ConfigInput,ConfigTip").find

		ConfigName = rs("ConfigName")
		ConfigTitle = rs("ConfigTitle")
		TrueText = rs("TrueText")
		FalseText = rs("FalseText")
		SelectOptions = rs("SelectOptions")
		ConfigInput = rs("ConfigInput")
		ConfigTip =  rs("ConfigTip")
		
		html = html & getConfigHtml( ConfigName , ConfigTitle , TrueText , FalseText , SelectOptions , ConfigInput , ConfigTip  )

		configs = html		
	End property
	
	'根据分类ID取取多项配置
	public property Get PageConfigContent( GroupIDS )
		dim arr,i,html,rs,where
		dim ConfigName,ConfigTitle,TrueText,FalseText,SelectOptions,ConfigInput,ConfigTip,GroupID,BeforeGroup
		set where = D_
		where("GroupID") = Array( "IN" , GroupIDS )
		
		arr = B_("ConfigGroup").where( GroupIDS ).field("GroupID").top(20).get1Arr
		
		for i = 0 to ubound(arr)
			GroupID = arr(i)
			html = html & "<div class=""layui-tab-item" & iif( BeforeGroup = "" ," layui-show", "" ) & """>"
			where("GroupID") = GroupID
			set rs = B_("Config").where(where).field("ConfigName,ConfigTitle,TrueText,FalseText,SelectOptions,ConfigInput,ConfigTip").select

			do while not rs.eof
				ConfigName = rs("ConfigName")
				ConfigTitle = rs("ConfigTitle")
				TrueText = rs("TrueText")
				FalseText = rs("FalseText")
				SelectOptions = rs("SelectOptions")
				ConfigInput = rs("ConfigInput")
				ConfigTip =  rs("ConfigTip")

				html = html & getConfigHtml( ConfigName , ConfigTitle , TrueText , FalseText , SelectOptions , ConfigInput , ConfigTip  )
				
				rs.MoveNext
			loop
				'if BeforeGroup <> GroupID then
					html = html & Save("")
					html = html & "</div>"

				'end if
			BeforeGroup = GroupID
		next
		POP_MVC.unset(rs)
		PageConfigContent = html
		
	End property
	
	'根据分类ID取取多项配置
	public property Get ConfigSearch( keyword )
		dim arr,i,html,rs,where
		dim ConfigName,ConfigTitle,TrueText,FalseText,SelectOptions,ConfigInput,ConfigTip,BeforeGroup

		html = html & "<div class=""layui-tab-item layui-show"">"
			
		if not isNul( keyword ) then	
		
			set rs = B_("Config").field("ConfigName,ConfigTitle,TrueText,FalseText,SelectOptions,ConfigInput,ConfigTip").search( keyword , "ConfigTitle,ConfigName,ConfigValue,ConfigTip,SelectOptions" )

			do while not rs.eof
				ConfigName = rs("ConfigName")
				ConfigTitle = rs("ConfigTitle")
				TrueText = rs("TrueText")
				FalseText = rs("FalseText")
				SelectOptions = rs("SelectOptions")
				ConfigInput = rs("ConfigInput")
				ConfigTip =  rs("ConfigTip")

				html = html & getConfigHtml( ConfigName , ConfigTitle , TrueText , FalseText , SelectOptions , ConfigInput , ConfigTip  )				
				rs.MoveNext
			loop
			html = html & Save("")
		end if
		html = html & "</div>"

		POP_MVC.unset(rs)
		ConfigSearch = html
	End property
	
	Public property get getTip( ConfigTitle , tip )
		getTip = "		<div class=""layui-form-mid layui-word-aux layui-show-xs layui-hide-md"">" & ConfigTitle & iif( isNul(tip) , "" ,  ":" & tip) & "</div>" & vbcrlf & vbtab & vbtab & vbtab
		getTip = getTip & "		<div class=""layui-form-mid layui-word-aux layui-hide-xs layui-show-md"">" & tip & "</div>" & vbcrlf & vbtab & vbtab & vbtab
	end property
	
	Public property get getLabel ( title )
		getLabel = "	<label class=""layui-form-label"" title=""" & title & """>" & title & "</label>" & vbcrlf & vbtab & vbtab & vbtab
	end property
	
	Public property get getFormItem( theClass )
		getFormItem = "<div class=""layui-form-item" & theClass & """>" & vbcrlf & vbtab & vbtab & vbtab
	end property
	
End Class
%>