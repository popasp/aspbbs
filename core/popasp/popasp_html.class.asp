<%
' +----------------------------------------------------------------------
' | POPASP [ ASP MVC ]
' +----------------------------------------------------------------------
' | Copyright (c) 2016 http://popasp.com All rights reserved.
' +----------------------------------------------------------------------
' | Author: popasp <1737025626@qq.com>
' +----------------------------------------------------------------------
Class POPASP_HTML
	public basePath
	
	private sub class_initialize
		basePath = ""
	end sub

	'从数据库中取出的数据为二维Dictionary对象，可以将其转化为Table表格
	'dict也可以是二维数组
	public Function Table( ByRef data , ByRef caption,ByRef topTr,ByRef bottomTr )
		dim html,i,k1,k2,item,bool,cnt,dict,tp,j
		html = "<table class='popasp_table'>"
		if caption <> "" then
			html = html & VbCrLf & vbTab & "<caption>" & caption & "</caption>"
		end if
		i = 0
		tp = typename( data )
		
		
		if tp = "Recordset" then
			if topTr <> "" then
				html = html & VbCrLf & vbTab & "<tr><td colspan=" & data.fields.count & ">" & topTr & "</td></tr>"
			end if
			j = 1 
			do while not data.bof and not data.eof
				if j > data.pageSize then Exit Do
				
				if j = 1 then
					html = html & VbCrLf & vbTab & "<tr>"
					html = html & VbCrLf & vbTab & vbTab
					for i = 0 to data.fields.count-1
						html = html & "<th>" & data.fields(i).Name & "</th>"
					next
					html = html & VbCrLf & vbTab & "</tr>"
				end if
				if data.absolutePosition mod 2 = 1 then
					html = html & VbCrLf & vbTab & "<tr class='popasp_even_tr'>"
				else
					html = html & VbCrLf & vbTab & "<tr>"
				end if
				html = html & VbCrLf & vbTab & vbTab
				for i = 0 to data.fields.count-1
					html = html & "<td>" & data.fields(i).value & "</td>"
				next
				html = html & VbCrLf & vbTab & "</tr>"
				data.moveNext : j = j + 1
			loop
			if bottomTr <> "" then
				html = html & VbCrLf & vbTab & "<tr><td colspan='" & data.fields.count & "'>" & bottomTr & "</td></tr>"
			end if
		elseif tp = "Dictionary" then
			i = 0
			for each k1 in data
				set item = data(k1)
				
				if i = 0 then
					if topTr <> "" then
						html = html & VbCrLf & vbTab & "<tr><td colspan=" & item.count & ">" & topTr & "</td></tr>"
					end if
					html = html & VbCrLf & vbTab & "<tr>"
					html = html & VbCrLf & vbTab & vbTab
					for each k2 in item
						html = html & "<th>" & k2 & "</th>"
					next
					html = html & VbCrLf & vbTab & "</tr>"
				end if
				if i mod 2 = 1 then
					html = html & VbCrLf & vbTab & "<tr class='popasp_even_tr'>"
				else
					html = html & VbCrLf & vbTab & "<tr>"
				end if
				html = html & VbCrLf & vbTab & vbTab
				for each k2 in item
					html = html & "<td>" & item(k2) & "</td>"
				next
				html = html & VbCrLf & vbTab & "</tr>"
				if i = data.count - 1 then
					if bottomTr <> "" then
						html = html & VbCrLf & vbTab & "<tr><td colspan='" & item.count & "'>" & bottomTr & "</td></tr>"
					end if
				end if
				i = i + 1
			next
			set item = nothing
		elseif isArray(data) then
			for i = 0 to ubound(data)
				if i = 0 then
					if topTr <> "" then
						html = html & VbCrLf & vbTab & "<tr><td colspan=" & ubound( data(i) +1 ) & ">" & topTr & "</td></tr>"
					end if
				end if
				if i mod 2 = 1 then
					html = html & VbCrLf & vbTab & "<tr class='popasp_even_tr'>"
				else
					html = html & VbCrLf & vbTab & "<tr>"
				end if
				
				html = html & VbCrLf & vbTab & vbTab
				for each k2 in data(i)
					html = html & "<td>" & k2 & "</td>"
				next
				html = html & VbCrLf & vbTab & "</tr>"
				
				if i = ubound(data) then
					if bottomTr <> "" then
						html = html & VbCrLf & vbTab & "<tr><td colspan='" & ubound( data(i) +1 ) & "'>" & bottomTr & "</td></tr>"
					end if
				end if
			next	
		else
			POP_MVC.exit( "POPASP_HTML.TableInfo不支持" & tp & "该数据类型" )			
		end if
		html = html & VbCrLf & "</table>"
		Table = html
	End Function
	
	public Function TableInfo( ByRef data , ByRef caption,ByRef topTr,ByRef bottomTr )
		on error resume next
		dim html,i,key,item,tp
		html = "<table class='popasp_table' style='width:auto;'>"
		if caption <> "" then
			html = html & VbCrLf & vbTab & "<caption>" & caption & "</caption>"
		end if
		if topTr <> "" then
			html = html & VbCrLf & vbTab & "<tr><td colspan='2'>" & topTr & "</td></tr>"
		end if
		
		tp = typename( data )
		if isArray( data ) then
			for i = 0 to ubound( data )
				key = i + 1
				item = data(i)
				if i mod 2 = 1 then
					html = html & VbCrLf & vbTab & "<tr class='popasp_even_tr'>"
				else
					html = html & VbCrLf & vbTab & "<tr>"
				end if
				html = html & "<td style='text-align:center;'>" & key & "</td>" & "<td style='text-align:center;'>" & item & "</td>"
				html = html & VbCrLf & vbTab & "</tr>"			
			next
		elseif tp = "Dictionary" then
			i = 0
			for each key in data
				item = data(key)
				if i mod 2 = 1 then
					html = html & VbCrLf & vbTab & "<tr class='popasp_even_tr'>"
				else
					html = html & VbCrLf & vbTab & "<tr>"
				end if
				html = html & "<td style='text-align:center;'>" & key & "</td>" & "<td style='text-align:center;'>" & item & "</td>"
				html = html & VbCrLf & vbTab & "</tr>"
				i = i + 1
			next
		elseif tp = "Recordset" then
			if not data.BOF and not data.EOF then
				for i = 0 to data.fields.count-1			
					'如果是mysql数据库，自增ID的数据类型不被支持，产生如下错误
					'变量使用了一个 VBScript 中不支持的 Automation 类型
					if LCase(POP_MVC.config("DB_TYPE")) <> "access" then	
						err.clear
						Call typename( data.Fields(i).Value )
						if err.number <> 0 then
							if typename(data.Fields(i).Value) = "Long" then
								item = CLng(data.Fields(i).Value)
							else
								item = data.Fields(i).Value
								'Me.Exit( "发现了不能被ASP解析的数据类型，请联系POPASP作者，以解决此BUG" )
							end if							
						else
							item = data.Fields(i).Value
						end if						
					else
						item = data.Fields(i).Value
					end if
					key = data.Fields(i).Name
					if i mod 2 = 1 then
						html = html & VbCrLf & vbTab & "<tr class='popasp_even_tr'>"
					else
						html = html & VbCrLf & vbTab & "<tr>"
					end if
					html = html & "<td style='text-align:center;'>" & key & "</td>" & "<td style='text-align:center;'>" & item & "</td>"
					html = html & VbCrLf & vbTab & "</tr>"
				next
			end if	
		else
			POP_MVC.exit( "POPASP_HTML.TableInfo不支持" & tp & "该数据类型" )
		end if

		if bottomTr <> "" then
			html = html & VbCrLf & vbTab & "<tr><td colspan='2'>" & bottomTr & "</td></tr>"
		end if
		set item = nothing
		html = html & VbCrLf & "</table>"
		TableInfo = html
	End Function
	
	Public Property Get Css( byval path )
		Response.write "<link rel=""stylesheet"" type=""text/css"" href=""" &  Me.basePath & path & """ />"
	End Property
	
	Public Property Get Js( byval path )
		Response.write "<script type=""text/javascript"" src=""" & Me.basePath & path & """></script>"
	End Property
	
	Public Property Get [Load]( byval path )
		if POP_MVC.String.iEndsWith( path , ".css" ) then
			Me.Css( path )
		else
			Me.Js( path )
		end if
	End Property
	
	Public Property Get pageCss()
		pageCss = POP_MVC.file_get_contents(POP_MVC.mvc_dir & "Tpl/popasp_page.css") 
	End Property
	
	Public Property Get tableCss()
		tableCss = POP_MVC.file_get_contents(POP_MVC.mvc_dir & "Tpl/popasp_table.css") 
	End Property
	
	'获取视频格式的html
	'target为视频源，stype为视频类型
	Function getAviHtml( target, ByVal stype )
		dim ret
		if isNul(stype) then
			stype = LCase( POP_MVC.file.extname( Array( target, false ) ) )
		end if
		select case stype
			case "swf"
				ret = "<object codebase=""http://download.macromedia.com/pub/shockwave/cabs/flash/swflash.cab#version=7,0,0,0"" classid=""clsid:D27CDB6E-AE6D-11cf-96B8-444553540000"" width=""400"" height=""300""><param name=""movie"" value=""" & target & """ /><param name=""quality"" value=""high"" /><param name=""AllowScriptAccess"" value=""never"" /><embed src=""" & target & """ quality=""high"" pluginspage=""http://www.macromedia.com/go/getflashplayer"" type=""application/x-shockwave-flash"" width=""400"" height=""300"" /></object>"
			case "mp3","wma"
				ret = "<object classid=""CLSID:22d6f312-b0f6-11d0-94ab-0080c74c7e95""  id=""MediaPlayer"" width=""450"" height=""70""><param name=""howStatusBar"" value=""-1""><param name=""AutoStart"" value=""False""><param name=""Filename"" value=""" & target & """></object>"
			case "rm","rmvb"
				ret = "<object classid=""clsid:CFCDAA03-8BE4-11cf-B84B-0020AFBBCCFA"" width=""400"" height=""300""><param name=""SRC"" value=""" & target & """ /><param name=""CONTROLS"" VALUE=""ImageWindow"" />"& vbCrLf
				ret = ret & "<param name=""CONSOLE"" value=""one"" /><param name=""AUTOSTART"" value=""true"" /><embed src=""" & target & """ nojava=""true"" controls=""ImageWindow"" console=""one"" width=""400"" height=""300""></object><br/>"& vbCrLf
				ret = ret & "<object classid=""clsid:CFCDAA03-8BE4-11cf-B84B-0020AFBBCCFA"" width=""400"" height=""32"" /><param name=""CONTROLS"" value=""StatusBar"" /><param name=""AUTOSTART"" value=""true"" /><param name=""CONSOLE"" value=""one"" />"& vbCrLf&_
				"<embed src=""" & target & """ nojava=""true"" controls=""StatusBar"" console=""one"" width=""400"" height=""24"" /></object><br/><object classid=""clsid:CFCDAA03-8BE4-11cf-B84B-0020AFBBCCFA"" width=""400"" height=""32"" />" & vbCrLf 
				ret = ret & "<param name=""CONTROLS"" value=""ControlPanel"" /><param name=""AUTOSTART"" value=""true"" /><param name=""CONSOLE"" value=""one"" /><embed src=""" & target & """ nojava=""true"" controls=""ControlPanel"" console=""one"" width=""400"" height=""24"" autostart=""true"" loop=""false"" /></object>"
			case "ra"
				ret = "<object classid=""clsid:CFCDAA03-8BE4-11CF-B84B-0020AFBBCCFA"" id=""RAOCX"" width=""450"" height=""60""><param name=""_ExtentX"" value=""6694""><param name=""_ExtentY"" value=""1588""><param name=""AUTOSTART"" value=""true""><param name=""SHUFFLE"" value=""0""><param name=""PREFETCH"" value=""0"">"& vbCrLf&_
				"<param name=""NOLABELS"" value=""0""><param name=""SRC"" value=""" & target & """><param name=""CONTROLS"" value=""StatusBar,ControlPanel""><param name=""LOOP"" value=""0""><param name=""NUMLOOP"" value=""0""><param name=""CENTER"" value=""0""><param name=""MAINTAINASPECT"" value=""0""><param name=""BACKGROUNDCOLOR"" value=""#000000""><embed src=""" & target & """ width=""450"" autostart=""true"" height=""60""></embed></object>"
			case "asf","avi","wmv"
				ret = "<object classid=""clsid:22D6F312-B0F6-11D0-94AB-0080C74C7E95"" codebase=""http://activex.microsoft.com/activex/controls/mplayer/en/nsmp2inf.cab#Version=6,0,02,902"" type=""application/x-oleobject"" standby=""Loading..."" width=""400"" height=""300"">"& vbCrLf
				ret = ret & "<param name=""FileName"" VALUE=""" & target & """ /><param name=""ShowStatusBar"" value=""-1"" /><param name=""AutoStart"" value=""true"" /><embed type=""application/x-mplayer2"" pluginspage=""http://www.microsoft.com/Windows/MediaPlayer/"" src=""" & target & """ autostart=""true"" width=""400"" height=""300"" /></object>"
		end select
		getAviHtml = ret 
	End Function
End Class
%>