<%
' +----------------------------------------------------------------------
' | POPASP [ ASP MVC ]
' +----------------------------------------------------------------------
' | Copyright (c) 2016 http://popasp.com All rights reserved.
' +----------------------------------------------------------------------
' | Author: popasp <1737025626@qq.com>
' +----------------------------------------------------------------------
Class POPASP_JMAIL

  Private s_SMTPServer, s_FromMail, s_FromName, s_MailServerUserName, s_MailServerPassword, s_Charset,s_Attchment
  private s_ToMail, s_CC, s_BCC, s_Subject, s_Body
  Private Sub Class_Initialize()
		's_SMTPServer = "smtp.qq.com"
		's_FromMail = "1737025626@qq.com"
		's_FromName = "POPASP"
		's_MailServerUserName = "1737025626@qq.com"
		's_MailServerPassword = ""		'163为帐户密码，qq为授权码
		s_Charset = "utf-8"
  End Sub  
  
  '邮箱服务器
  Public Property Let SMTPServer(ByVal value)
    s_SMTPServer = value
  End Property
  
  Public Property Get SMTPServer()
	SMTPServer = s_SMTPServer
	if SMTPServer = "" then
		SMTPServer = "smtp." & POP_MVC.String.after( User, "@" )
	end if
  End Property 
  
  '登陆帐户
  Public Property Let User(ByVal value)
    s_MailServerUserName = value
  End Property
  
  '登陆密码
  Public Property Let Password(ByVal value)
    s_MailServerPassword = value
  End Property  
  
  '发件人邮箱
  Public Property Let FromMail(ByVal value)
    s_FromMail = value
  End Property
  
  Public Property Get FromMail() 
    FromMail = s_FromMail
	if FromMail = "" then
		FromMail = User
	end if
  End Property    
  
  '发件人称呼  
  Public Property Let FromName(ByVal value )
	s_FromName = value
  End Property
  
  Public Property Get FromName()
    FromName = s_FromName
	if FromName = "" then
		FromName = POP_MVC.String.before( User, "@" )
	end if
  End Property 
  
  '收件人邮箱
  Public Property Let ToMail(ByVal arg )
	s_ToMail = getEmail( arg )
  End Property
  
  '抄送邮箱
  Public Property Let CC(ByVal arg )
	s_CC = getEmail( arg )
  End Property
  
  '密送邮箱，多次测试发现163不能进行密送，而qq可以
  Public Property Let BCC(ByVal arg )
	s_BCC = getEmail( arg )
  End Property

	Private Function getEmail ( arg )
		dim email
		email = ""
		if typename( arg ) = "Dictionary" then	'键名是邮箱，值是用户名		
			for each key in arg
				email = email & arg(key) & "<" & key & ">;"
			next
		elseif typename( arg ) = "Recordset" then	'第一个是用户名，第二个是邮箱
			Do While Not arg.BOF And Not arg.EOF
				email = email & arg.Fields(0).Value & "<" & arg.Fields(1).value	 & ">;"
				arg.MoveNext
			Loop
			arg.close : set arg = nothing		
		elseif isArray( arg ) then
			email = "<" & join( arg, ">;<" ) & ">"
		else
			email = arg			
		end if
		getEmail = email
	end Function
  
  '邮件主题
  Public Property Let Subject(ByVal value)
    s_Subject = value
  End Property
  
  '邮件内容
  Public Property Let Body(ByVal value)
    s_Body = value
  End Property
  
  '附件,多个用;分开
  Public Property Let Attchment(ByVal value)
    s_Attchment = value
  End Property 
  
  '编码
  Public Property Let Charset(ByVal value)
    s_Charset = value
  End Property 
  
  Public Property Get Subject()
    Subject = s_Subject
  End Property
  
  Public Property Get Body()
	Body = s_Body
  End Property
        
  
  Public Property Get User() : User = s_MailServerUserName : End Property        
  Public Property Get Password() : Password = s_MailServerPassword : End Property               
  Public Property Get ToMail() : ToMail = s_ToMail : End Property    
  Public Property Get Attchment() : Attchment = s_Attchment : End Property
  Public Property Get Charset() : Charset = s_Charset : End Property

  '发送邮件，返回状态1 ，554为垃圾邮件。0为发送成功
  Public Function Send()    
	On Error Resume Next
	dim startTime : startTime = timer()
	dim arr,i
    Set jmail = Server.CreateObject("JMAIL.Message")   '建立发送邮件的对象
    If Err.Number <> 0 Then
        Send = 1
        Exit Function
    End If
	with jmail
		.silent = True    '屏蔽例外错误，返回FALSE跟TRUE两值
		.logging = False   '启用邮件日志
		.Charset = s_Charset     '邮件的文字编码GB2312为中文 UTF-8为英文
		.ISOEncodeHeaders = False '防止邮件标题乱码
		if s_Attchment = "" then
			.ContentType  = "text/html"    '邮件的格式为HTML格式 
		end if
		.From = FromMail  '发件人的E-MAIL地址
		.FromName = FromName   '发件人姓名
		.MailServerUserName = User    '登录邮件服务器所需的用户名
		.MailServerPassword = Password     '登录邮件服务器所需的密码
		.Subject = Subject    '邮件的标题 
		.Body = Body      '邮件的内容
		.Priority = 1      '邮件的紧急程序，1 为最快，5 为最慢， 3 为默认值
		if not isEmpty( s_BCC ) then
			arr = split( s_BCC , ";" )
			for i = 0 to ubound( arr )
				if arr(i) <> "" then
					.AddRecipientBCC arr(i)    '密件收件人的地址
				end if
			next
		end if
		if not isEmpty( s_CC ) then
			arr = split( s_CC , ";" )
			for i = 0 to ubound( arr )
				if arr(i) <> "" then
					.AddRecipientCC arr(i)    '邮件抄送者的地址
				end if
			next	
		end if
		arr = split( s_ToMail , ";" )
		for i = 0 to ubound( arr )
			if arr(i) <> "" then
				.AddRecipient arr(i)    '邮件收件人的地址
			end if
		next		
		if s_Attchment <> "" then
			.ContentType "text/plain" '发送附件时需要使用这个
			arr = split( s_Attchment, ";" )
			for i = 0 to ubound( arr )
				.AddAttachment(POP_MVC.realPath(arr(i)))    '邮件附件
			next
		end if
    end with 


	jmail.Send(SMTPServer)     '执行邮件发送（通过邮件服务器地址）	
    Send = jmail.ErrorCode
	if jmail.ErrorCode = 0 then
		call POP_MVC.pushTime( startTime , "使用jmail发送主题为 " & subject & " 的邮件耗时")
	else
		'Call L_( Typename(Me) & ".Send:" & jmail.ErrorCode )
	end if
	jmail.Close()   '关闭对象   
	
  End Function
End Class
%>