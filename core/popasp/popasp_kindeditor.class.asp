<%
' +----------------------------------------------------------------------
' | POPASP [ ASP MVC ]
' +----------------------------------------------------------------------
' | Copyright (c) 2016 http://popasp.com All rights reserved.
' +----------------------------------------------------------------------
' | Author: popasp <1737025626@qq.com>
' +----------------------------------------------------------------------
Class POPASP_KINDEDITOR
	Private editorIndex,srcPath,dstPath,isIncludeJS,uploadIndex
	
	Public jsPath
	
	'默认模式
	'http://kindeditor.net/ke4/examples/default.html
	Public Property Get KE_Default( textarea )
		editorIndex = editorIndex + 1
		KE_Default = IncludeJS
		KE_Default = KE_Default & POP_MVC.file_get_contents( jsPath & "KE_Default.txt" )
		KE_Default = Replace( KE_Default , "{TEXTAREA}" , POP_MVC.String.jsonEncode(textarea) )
		KE_Default = Replace( KE_Default , "{EDITOR}" , "editor" & editorIndex )
		KE_Default = Replace( KE_Default , "{AUTO_FOLDER_NAME}" , POP_MVC.config("AUTO_FOLDER_NAME") )
	End Property
	
	Public Property Get CMS_Default( textarea )
		editorIndex = editorIndex + 1
		KE_Default = IncludeJS
		KE_Default = KE_Default & POP_MVC.file_get_contents( jsPath & "CMS_Default.txt" )
		KE_Default = Replace( KE_Default , "{TEXTAREA}" , POP_MVC.String.jsonEncode(textarea) )
		KE_Default = Replace( KE_Default , "{EDITOR}" , "editor" & editorIndex )
		KE_Default = Replace( KE_Default , "{AUTO_FOLDER_NAME}" , POP_MVC.config("AUTO_FOLDER_NAME") )
	End Property
	
	'简单模式
	'http://kindeditor.net/ke4/examples/simple.html
	Public Property Get KE_Simple( textarea )
		editorIndex = editorIndex + 1
		KE_Simple = IncludeJS
		KE_Simple = KE_Simple & POP_MVC.file_get_contents( jsPath & "KE_Simple.txt" )
		KE_Simple = Replace( KE_Simple , "{TEXTAREA}" , POP_MVC.String.jsonEncode(textarea) )
		KE_Simple = Replace( KE_Simple , "{EDITOR}" , "editor" & editorIndex )
		KE_Simple = Replace( KE_Simple , "{AUTO_FOLDER_NAME}" , POP_MVC.config("AUTO_FOLDER_NAME") )
	End Property
	
	'仿QQ邮箱风格
	'http://kindeditor.net/ke4/examples/qqstyle.html
	Public Property Get KE_QQStyle( textarea )
		editorIndex = editorIndex + 1
		KE_QQStyle = IncludeJS
		if KE_QQStyle <> "" then
			KE_QQStyle = POP_MVC.String.reg_replace( KE_QQStyle , "" , "\<link.*?\>" , "i" )
		else
			POP_MVC.exit("仿QQ邮箱风格的编辑器不能与其他风格的混用")
		end if
		KE_QQStyle = KE_QQStyle & POP_MVC.file_get_contents( jsPath & "KE_QQStyle.txt" )
		KE_QQStyle = Replace( KE_QQStyle , "{TEXTAREA}" , POP_MVC.String.jsonEncode(textarea) )
		KE_QQStyle = Replace( KE_QQStyle , "{EDITOR}" , "editor" & editorIndex )
		KE_QQStyle = Replace( KE_QQStyle , "{AUTO_FOLDER_NAME}" , POP_MVC.config("AUTO_FOLDER_NAME") )
	End Property
	
	'取色器
	'http://kindeditor.net/ke4/examples/colorpicker.html
	Public Property Get ColorPicker( button )
		ColorPicker = IncludeJS
		ColorPicker = ColorPicker & POP_MVC.file_get_contents( jsPath & "ColorPicker.txt" )
		ColorPicker = Replace( ColorPicker , "{INPUT}" , POP_MVC.String.jsonEncode(button) )
	End Property
	
	'上传按钮
	'http://kindeditor.net/ke4/examples/uploadbutton.html
	'stype可以选flash media file image
	Public Function UploadButton( button , inputUploadPath, inputSrcName,style )
		uploadIndex = uploadIndex + 1
		UploadButton = IncludeJS
		UploadButton = UploadButton & POP_MVC.file_get_contents( jsPath & "UploadButton.txt" )
		UploadButton = Replace( UploadButton , "{AUTO_FOLDER_NAME}" , POP_MVC.config("AUTO_FOLDER_NAME") )
		UploadButton = Replace( UploadButton , "{BUTTON}" , POP_MVC.String.jsonEncode(button) )
		UploadButton = Replace( UploadButton , "{INPUT}" , POP_MVC.String.jsonEncode(inputUploadPath) )
		UploadButton = Replace( UploadButton , "{SRCNAME}" , POP_MVC.String.jsonEncode(inputSrcName) )
		UploadButton = Replace( UploadButton , "{INDEX}" , uploadIndex )
		UploadButton = Replace( UploadButton , "{TYPE}" , style )
	End Function
	
	'远程管理器
	'http://kindeditor.net/ke4/examples/file-manager.html
	'stype可以选flash media file image	
	Public Function FileManage( button , input, style )
		editorIndex = editorIndex + 1
		FileManage = IncludeJS
		FileManage = FileManage & POP_MVC.file_get_contents( jsPath & "FileManage.txt" )
		FileManage = Replace( FileManage , "{EDITOR}" , "editor" & editorIndex )
		FileManage = Replace( FileManage , "{AUTO_FOLDER_NAME}" , POP_MVC.config("AUTO_FOLDER_NAME") )
		FileManage = Replace( FileManage , "{BUTTON}" , POP_MVC.String.jsonEncode(button) )
		FileManage = Replace( FileManage , "{INPUT}" , POP_MVC.String.jsonEncode(input) )
		FileManage = Replace( FileManage , "{TYPE}" , style )
	End Function
	
	'远程管理器
	'http://kindeditor.net/ke4/examples/file-manager.html
	'stype可以选flash media file image	
	Public Function FileManageRemove( button , url )
		editorIndex = editorIndex + 1
		FileManageRemove = IncludeJS
		FileManageRemove = FileManageRemove & POP_MVC.file_get_contents( jsPath & "FileManageRemove.txt" )
		FileManageRemove = Replace( FileManageRemove , "{EDITOR}" , "editor" & editorIndex )
		FileManageRemove = Replace( FileManageRemove , "{AUTO_FOLDER_NAME}" , POP_MVC.config("AUTO_FOLDER_NAME") )
		FileManageRemove = Replace( FileManageRemove , "{MANAGE_ASP}" , "popasp_file_manager_json" )
		FileManageRemove = Replace( FileManageRemove , "{BUTTON}" , POP_MVC.String.jsonEncode(button) )
		FileManageRemove = Replace( FileManageRemove , "{URL}" , POP_MVC.String.jsonEncode(url) )
	End Function
	
	'远程管理器
	'http://kindeditor.net/ke4/examples/file-manager.html
	'pathID只能是由英文与数字组成的标识符
	Public Function ManageRemoveByFile( button , url, Path, pathID )
		editorIndex = editorIndex + 1
		ManageRemoveByFile = IncludeJS
		
		dim jsonAsp,content
		srcPath = POP_MVC.mvc_dir & "kindeditor/kindeditor/asp/popasp_file_manager_json.asp"
		jsonAsp = "./" & POP_MVC.config("AUTO_FOLDER_NAME") & "/kindeditor/asp/popasp_file_manager_json_" & pathID & ".asp"
		

		If Not POP_MVC.file.isExists( jsonAsp ) Then			
			content = POP_MVC.file_get_contents( srcPath )
		
			
			content = replace( content, "{MVC_DIR}" , "../../../" & POP_MVC.String.ltrim(POP_MVC.String.ltrim(POP_MVC.mvc_dir , "./" ) , ".\" ) )
			content = replace( content, "{APP_PATH}" , "../../../" & POP_MVC.String.ltrim(POP_MVC.String.ltrim(POP_MVC.appPath , "./" ) , ".\" ) )
			content = replace( content, "{UPLOAD_PATH}" , path )
			Call POP_MVC.file_put_contents( jsonAsp , content )
		end if

		ManageRemoveByFile = ManageRemoveByFile & POP_MVC.file_get_contents( jsPath & "FileManageRemove.txt" )
		ManageRemoveByFile = Replace( ManageRemoveByFile , "{EDITOR}" , "editor" & editorIndex )
		ManageRemoveByFile = Replace( ManageRemoveByFile , "{AUTO_FOLDER_NAME}" , POP_MVC.config("AUTO_FOLDER_NAME") )
		ManageRemoveByFile = Replace( ManageRemoveByFile , "{MANAGE_ASP}" , "popasp_file_manager_json_" & pathID )
		ManageRemoveByFile = Replace( ManageRemoveByFile , "{BUTTON}" , POP_MVC.String.jsonEncode(button) )
		ManageRemoveByFile = Replace( ManageRemoveByFile , "{URL}" , POP_MVC.String.jsonEncode(url) )
	End Function
	
	'本地图片上传
	'http://kindeditor.net/ke4/examples/image-dialog.html
	'stype可以选flash media file image	
	Public Function UploadLocal( button , inputUploadPath, style )
		editorIndex = editorIndex + 1
		UploadLocal = IncludeJS
		UploadLocal = UploadLocal & POP_MVC.file_get_contents( jsPath & "UploadDialog.txt" )
		UploadLocal = Replace( UploadLocal , "{EDITOR}" , "editor" & editorIndex )
		UploadLocal = Replace( UploadLocal , "{AUTO_FOLDER_NAME}" , POP_MVC.config("AUTO_FOLDER_NAME") )
		UploadLocal = POP_MVC.String.reg_replace( UploadLocal , "showRemote : false ," , "showRemote\s*:\s*\w+\s*," , "ig" )
		UploadLocal = Replace( UploadLocal , "{BUTTON}" , POP_MVC.String.jsonEncode(button) )
		UploadLocal = Replace( UploadLocal , "{INPUT}" , POP_MVC.String.jsonEncode(inputUploadPath) )
		UploadLocal = Replace( UploadLocal , "{INDEX}" , uploadIndex )
		UploadLocal = Replace( UploadLocal , "{TYPE}" , style )
	End Function
	
	'网络图片
	'http://kindeditor.net/ke4/examples/image-dialog.html
	'stype可以选flash media file image	
	Public Function UploadRemote( button , inputUploadPath, style )
		editorIndex = editorIndex + 1
		UploadRemote = IncludeJS
		UploadRemote = UploadRemote & POP_MVC.file_get_contents( jsPath & "UploadDialog.txt" )
		UploadRemote = Replace( UploadRemote , "{EDITOR}" , "editor" & editorIndex )
		UploadRemote = Replace( UploadRemote , "{AUTO_FOLDER_NAME}" , POP_MVC.config("AUTO_FOLDER_NAME") )
		UploadRemote = POP_MVC.String.reg_replace( UploadRemote , "showLocal : false ," , "showRemote\s*:\s*\w+\s*," , "ig" )
		UploadRemote = Replace( UploadRemote , "{BUTTON}" , POP_MVC.String.jsonEncode(button) )
		UploadRemote = Replace( UploadRemote , "{INPUT}" , POP_MVC.String.jsonEncode(inputUploadPath) )
		UploadRemote = Replace( UploadRemote , "{INDEX}" , uploadIndex )
		UploadRemote = Replace( UploadRemote , "{TYPE}" , style )
	End Function
	
	'网络图片 + 本地上传
	'http://kindeditor.net/ke4/examples/image-dialog.html
	'stype可以选flash media file image	
	Public Function Upload( button , inputUploadPath, style )
		editorIndex = editorIndex + 1
		Upload = IncludeJS
		Upload = Upload & POP_MVC.file_get_contents( jsPath & "UploadDialog.txt" )
		Upload = Replace( Upload , "{EDITOR}" , "editor" & editorIndex )
		Upload = Replace( Upload , "{AUTO_FOLDER_NAME}" , POP_MVC.config("AUTO_FOLDER_NAME") )
		Upload = Replace( Upload , "{BUTTON}" , POP_MVC.String.jsonEncode(button) )
		Upload = Replace( Upload , "{INPUT}" , POP_MVC.String.jsonEncode(inputUploadPath) )
		Upload = Replace( Upload , "{INDEX}" , uploadIndex )
		Upload = Replace( Upload , "{TYPE}" , style )
	End Function
	
	'网络图片 + 本地上传
	'http://kindeditor.net/ke4/examples/image-dialog.html
	'stype可以选flash media file image	
	Public Function MultiUpload( button , inputUploadPath, style )
		editorIndex = editorIndex + 1
		MultiUpload = IncludeJS
		MultiUpload = MultiUpload & POP_MVC.file_get_contents( jsPath & "MultiUpload.txt" )
		MultiUpload = Replace( MultiUpload , "{EDITOR}" , "editor" & editorIndex )
		MultiUpload = Replace( MultiUpload , "{AUTO_FOLDER_NAME}" , POP_MVC.config("AUTO_FOLDER_NAME") )
		MultiUpload = Replace( MultiUpload , "{BUTTON}" , POP_MVC.String.jsonEncode(button) )
		MultiUpload = Replace( MultiUpload , "{INPUT}" , POP_MVC.String.jsonEncode(inputUploadPath) )
		MultiUpload = Replace( MultiUpload , "{INDEX}" , MultiUploadIndex )
		MultiUpload = Replace( MultiUpload , "{TYPE}" , style )
	End Function
	

	Private Sub Class_Initialize
		Me.Install
		editorIndex = 0
		uploadIndex = 0
		isIncludeJS = false
	End Sub

	Public Property Get Install
		srcPath = POP_MVC.mvc_dir & "kindeditor/"
		dstPath = "./" & POP_MVC.config("AUTO_FOLDER_NAME") & "/kindeditor/"
		if jsPath = "" then
			jsPath = srcPath & "inc/"
		end if
		If Not POP_MVC.file.isExists( dstPath ) Then
			dim jsonAsp,content
			POP_MVC.file.CopyFolder srcPath & "kindeditor/" , dstPath
			jsonAsp = "./" & POP_MVC.config("AUTO_FOLDER_NAME") & "/kindeditor/asp/popasp_upload_json.asp"
			content = POP_MVC.file_get_contents( jsonAsp )
			content = replace( content, "{MVC_DIR}" , "../../../" & POP_MVC.String.ltrim(POP_MVC.String.ltrim(POP_MVC.mvc_dir , "./" ) , ".\" ) )
			content = replace( content, "{APP_PATH}" , "../../../" & POP_MVC.String.ltrim(POP_MVC.String.ltrim(POP_MVC.appPath , "./" ) , ".\" ) )
			Call POP_MVC.file_put_contents( jsonAsp , content )
			
			jsonAsp = "./" & POP_MVC.config("AUTO_FOLDER_NAME") & "/kindeditor/asp/popasp_file_manager_json.asp"
			content = POP_MVC.file_get_contents( jsonAsp )
			content = replace( content, "{MVC_DIR}" , "../../../" & POP_MVC.String.ltrim(POP_MVC.String.ltrim(POP_MVC.mvc_dir , "./" ) , ".\" ) )
			content = replace( content, "{APP_PATH}" , "../../../" & POP_MVC.String.ltrim(POP_MVC.String.ltrim(POP_MVC.appPath , "./" ) , ".\" ) )
			content = replace( content, "{UPLOAD_PATH}" , iif( Not POP_MVC.config.Exists("UPLOAD_SAVE_PATH") , "/upload/" , POP_MVC.config.Exists("UPLOAD_SAVE_PATH") ) )
			Call POP_MVC.file_put_contents( jsonAsp , content )
			
			jsonAsp = "./" & POP_MVC.config("AUTO_FOLDER_NAME") & "/kindeditor/kindeditor-all-min.js"
			content = POP_MVC.file_get_contents( jsonAsp )
			if POP_MVC.config.exists("content_page_lable") then
				content = replace( content, "<hr style=""page-break-after: always;"" class=""ke-pagebreak"" />" , POP_MVC.config("content_page_lable") )
				Call POP_MVC.file_put_contents( jsonAsp , content )
			end if			
		end if
	End Property	
	
	Public Property Get IncludeJS()
		if Not isIncludeJS then
			IncludeJS = POP_MVC.file_get_contents( jsPath & "IncludeJS.txt" )
			IncludeJS = Replace( IncludeJS , "{AUTO_FOLDER_NAME}" , POP_MVC.config("AUTO_FOLDER_NAME") ) & VbCrLf
			isIncludeJS = true
		End if
	End Property
	
	Public Property Get CurrentEditor()
		CurrentEditor = "editor" & editorIndex
	End Property
	
	
  '列出文件夹下的所有文件夹或文件
  Public Function FileList(ByVal folderPath )
    On Error Resume Next 'Do not delete or comment

    Dim f,fs,k,i,l,dict,ret,fileTypes,fileExt
	'图片扩展名
	fileTypes = "gif,jpg,jpeg,png,bmp"
    folderPath = POP_MVC.realPath(folderPath) : i = 0

	If Not POP_MVC.file.isExists( folderPath ) Then
		set FileList = D_
		Exit Function
	End If
	
    Set f = POP_MVC.Fso.GetFolder(folderPath)
	set ret = D_

	Set fs = f.SubFolders	  
	For Each k In fs		
		set dict = D_
		dict("is_dir") = true
		dict("has_file") = (k.Files.count > 0)
		dict("filesize") = k.Size
		dict("is_photo") = false
		dict("filetype") = ""
		dict("filename") = k.Name
		dict("datetime") = FormatDate(k.DateLastModified)
		set ret( ret.count + 1 ) = dict
	Next

      Set fs = f.Files
      For Each k In fs
		set dict = D_
		fileExt = lcase(mid(k.name, InStrRev(k.name, ".") + 1))
		dict("is_dir") = false
		dict("has_file") = false
		dict("filesize") = k.Size
		dict("is_photo") = (instr(lcase(fileTypes), fileExt) > 0)
		dict("filetype") = fileExt
		dict("filename") = k.Name
		dict("datetime") = FormatDate(k.DateLastModified)
		set ret( ret.count + 1 ) = dict		
      Next

    Set fs = Nothing
    Set f = Nothing
	
    set FileList = ret
    Call L_( typename(Me) & ".FileList" )
  End Function
  
	Public Function FormatDate(datetime)
		FormatDate = POP_MVC.FormatDate( datetime, "yyyy-mm-dd hh:ii:ss" )
	End Function 
	
	
End Class
%>