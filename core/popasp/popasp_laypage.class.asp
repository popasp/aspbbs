<%
' +----------------------------------------------------------------------
' | POPASP [ ASP MVC ]
' +----------------------------------------------------------------------
' | Copyright (c) 2016 http://popasp.com All rights reserved.
' +----------------------------------------------------------------------
' | Author: popasp <1737025626@qq.com>
' +----------------------------------------------------------------------

'''常规举例
' var_export P_("PAGE")( 55 ).show

'''字符串参数例子
' var_export P_("PAGE")( array(55, "name=张三&ange=18") ).show

'''二维Dictionary参数例子
' dim dict : set dict = D_
' dict("name") = "张三"
' dict("age") = 18
' set dict("hobby") = D_	'一维Dictionary参数例子可以去掉dict("hobby")这一项
' dict("hobby")(0) = "篮球"
' dict("hobby")(1) = "足球"
' dict("hobby")(2) = "看电影"
' var_export P_("PAGE")( array(  55, dict ) ).show
' var_export "<br />"
' var_export request.querystring	'{ "page": "4", "hobby": "篮球, 足球, 看电影", "name": "张三", "age": "18" }
Class POPASP_LAYPAGE	
	public total	'记录总个数	
	public totalPage	'总共多少页
	public page	'当前页
	public perpage	'每页显示多少条

	
	private uri,uri2
	private parameter	'网址中的额外参数
	private route
	Private divClass	
	'下面是可配置的
	private header	'"条记录"
	private first	'"首页"
	private prev	'"上一页"
	private [next]	'"下一页"
	private last	'"尾页"
	private noRecordTip		'无数据的提示
	private pageCount		'分页中显示的页码个数
	private seperator		'分隔符
	private pageScale		'类似 5/30页
	private perpageTip		'每页显示
	private matches
	private jump	'页面跳转，如果不需要页面跳转，可以 Call oPage.setConfig( "jump" , "" )
	private pagesize	'每页显示多少条
	private curpageLabel	'当前的包裹标签，默认为<em></em>
	private aClass,currentClass,prevClass,nextClass,firstClass,lastClass

	Private Sub Class_Initialize
		dim arr,pos
		header = "条"
		first = "<i class='fa fa-angle-double-left' aria-hidden='true'></i>"
		prev = "<i class='fa fa-angle-left' aria-hidden='true'></i>"
		[next] = "<i class='fa fa-angle-right' aria-hidden='true'></i>"
		last = "<i class='fa fa-angle-double-right' aria-hidden='true'></i>"
		perpageTip = "每页显示"
		pageCount = 12
		perpage = POP_MVC.config("PAGE_PAGESIZE")
		noRecordTip = ""
		seperator = ""
		pageScale = false
		curpageLabel = "em"
		aClass = ""
		currentClass = ""
		divClass = "popasp_page"

		page =  POP_MVC.Get( POP_MVC.config("VAR_PAGE") )

		if not isNumeric( page ) then
			page = 1
		end if
		
		if page < 1 then
			page = 1
		end if
		
		total = 0
		
		page = CLng( page )
 	End Sub
	
	Private Sub Class_Terminate
		set matches = nothing
	End Sub
	
	Public Property Get config(attr)
		Execute "config = " & attr
	end Property
	
	Public Property Let config(attr,value)
		if LCase( attr ) = "parameter" then
			Call setParameter( value )
			Exit Property
		end if
		
		if LCase( attr ) = "perpage" then
			perpage = value
			Exit Property
		end if
		
		if attr = "pagesize" then
			pagesize = value
			Exit Property
		end if
		
		if isNumeric(value) then
			Execute attr & " = " & value
		else			
			Execute attr & " = """ & value & """"
		end if
	End Property
	
	Private Sub setParameter( arg )
		if typename( arg ) = "Dictionary" then	'Dictionary对象
			dim k1,k2
			parameter = ""
			for each k1 in arg
				if typename( arg(k1) ) = "Dictionary" then
					for each k2 in arg(k1)
						parameter = parameter & "&" & k1 & "=" & cstr( arg(k1)(k2) )
					next
				else
					parameter = parameter & "&" & k1 & "=" & cstr( arg(k1) ) 
				end if
			next
			parameter = POP_MVC.ltrim( parameter , "&" )
		else
			parameter = arg
		end if
	End Sub 
	
	'使用时有两种方法，一种是传入Recordset对象，一种是传入总数
	'1. that.assign "page" , P_("PAGE")(rs).show
	'2. that.assign "page" , P_("PAGE")( total ).show，total为总数
	'url其它参数传入时，可以使用P_("PAGE")( Array( rs/total, parameter , route ) )，parameter可以是字符串也可以是最多二维的Dictionary对象
	Public Default Function Constructor(byref args)
		dim rs
		totalPage = 0
		if isArray(args) then
			if typename( args(0) ) = "Recordset" then
				set rs = args(0)
			else
				if POP_MVC.String.reg_test(POP_MVC.get( POP_MVC.config("VAR_PAGESIZE") ),"^[1-9]\d*$","") then
					perpage = POP_MVC.get( POP_MVC.config("VAR_PAGESIZE") )
				end if
				total = args(0)
			end if
			if ubound( args ) > 0 then
				if not isNul( args(1) ) then
					Call setParameter( args(1) )
				end if
			end if
			
			if ubound( args ) > 1 then
				route = args(2)
			end if
		else
			if typename( args ) = "Recordset" then
				set rs = args
			else
				if POP_MVC.String.reg_test(POP_MVC.get( POP_MVC.config("VAR_PAGESIZE") ),"[1-9]\d*","") then
					perpage = POP_MVC.get( POP_MVC.config("VAR_PAGESIZE") )
				end if
				total = args
			end if
			parameter = ""
		end if
		
		if not isEmpty( rs ) then
			total = rs.recordCount			
			totalPage = rs.pageCount
			perpage = rs.pageSize
		end if

		totalPage = CLng(Abs(Int(- total / perpage)))

		page = CLng(page)
		
		if page > totalPage then
			page = totalPage
		end if		
		
		set Constructor = Me
	End Function
	
	Public Function GetUri
		if isEmpty( uri ) then
			if C_("URL_MODE") <> 0 then
				uri = "?" & POP_MVC.config("VAR_MODULE") & "=" & POP_MVC.c & "&" & POP_MVC.config("VAR_ACTION") & "=" & POP_MVC.a
				dim key
				for each key in POP_MVC.dQuery
					if key <> POP_MVC.config("VAR_MODULE") and key <> POP_MVC.config("VAR_ACTION") and key <> POP_MVC.config("VAR_PAGE") then
						uri = uri & "&" & key & "=" & POP_MVC.dQuery(key)
					end if
				next				
			else
				uri = SELF__
			end if

			
			if parameter <> "" AND inStr( POP_MVC.urldecode(uri),POP_MVC.urldecode(parameter) ) < 1 then
				if POP_MVC.Get("") = "" then
					uri = uri & "?" & parameter
				else
					uri = uri & "&" & parameter
				end if
			end if

			uri = POP_MVC.String.reg_replace(uri,"","&" & POP_MVC.config("VAR_PAGE") & "=[^&]+","ig")			

			uri = POP_MVC.String.reg_replace(uri,"","\b" & POP_MVC.config("VAR_PAGE") & "=[^&]+","ig")

			
			if instr( uri , "?" ) > 0 then
				if inStr( uri , "&" ) > 1 or not POP_MVC.String.EndsWith( uri , "?" ) then
					uri = uri & "&"
				end if
			else
				uri = uri & "?"
			end if			
		end if

		getUri = uri
	End Function
	
	'得到页码的链接html
	'如 <a href="clist.asp?ShowId=24&page=2">2</a>
	Public Function getUrl(dPage,sTxt,ByVal className)
		if route = "" then
			uri = getUri
			getUrl = "<a href='" & uri & POP_MVC.config("VAR_PAGE") & "=" & dPage & "' class='" & className & "'>" & sTxt & "</a>"	
		else
			getUrl = "<a href='" & replace( route, POP_MVC.config("VAR_PAGE"),dPage ) & "' class='" & className & "'>" & sTxt & "</a>"	
		end if
	End Function
	
	'得到页码的链接html
	'如 <a href="clist.asp?ShowId=24&page=2">2</a>
	Public Function getUrl2(dsize,sTxt)
		dim temp
		if route = "" then
			if isEmpty( uri2 ) then
				if isEmpty(uri) then Call getUrl(1,1,firstClass)
				uri2 = uri
				uri2 = POP_MVC.String.reg_replace(uri2,"","&" & POP_MVC.config("VAR_PAGESIZE") & "=[^&]+","ig")
				uri2 = POP_MVC.String.reg_replace(uri2,"","\b" & POP_MVC.config("VAR_PAGESIZE") & "=[^&]+","ig")

				if instr( uri2 , "?" ) > 0 then
					if not POP_MVC.String.EndsWith( uri2 , "?" ) then
						uri2 = uri2 & "&"
					else
						uri2 = uri2 & "&"
					end if
				else
					uri2 = uri2 & "?"
				end if	
				
				uri2 = replace(uri2,"&&","&")
			end if
			uri2 = replace(uri2,"?&","?")

			getUrl2 = "<a href='" & uri2 & POP_MVC.config("VAR_PAGESIZE") & "=" & dsize & "'>" & sTxt & "</a>"
		else
			getUrl2 = "<a href='" & replace( route , POP_MVC.config("VAR_PAGESIZE") , dsize ) & "'>" & sTxt & "</a>"
		end if
	End Function
	
	Public Function getUrl3(dsize)
		dim temp
		if route = "" then
			if isEmpty( uri2 ) then
				if isEmpty(uri) then Call getUrl(1,1,firstClass)
				uri2 = uri
				uri2 = POP_MVC.String.reg_replace(uri2,"","&" & POP_MVC.config("VAR_PAGESIZE") & "=[^&]+","ig")
				uri2 = POP_MVC.String.reg_replace(uri2,"","\b" & POP_MVC.config("VAR_PAGESIZE") & "=[^&]+","ig")

				if instr( uri2 , "?" ) > 0 then
					if not POP_MVC.String.EndsWith( uri2 , "?" ) then
						uri2 = uri2 & "&"
					else
						uri2 = uri2 & "&"
					end if
				else
					uri2 = uri2 & "?"
				end if	
				
				uri2 = replace(uri2,"&&","&")
			end if
			uri2 = replace(uri2,"?&","?")

			getUrl3 = uri2 & POP_MVC.config("VAR_PAGESIZE") & "=" & dsize
		else
			getUrl3 = replace( route , POP_MVC.config("VAR_PAGESIZE") , dsize )
		end if
	End Function	
	
	'将当前页的起始与结束位以数组形式返回，如 Array( 10,19 )
	Public Function getRange(  )
		dim start_,end_
		start_ = (page-1) * perpage
		end_ = start_ + perpage -1
		if end_ > total - 1 then
			end_ = total -1
		end if
		
		getRange = Array( start_ , end_ )
	End Function
	
	'首页链接地址
	Public Property Get FirstLink
		if page > 1 then 
			if route = "" then
				uri = getUri
				FirstLink = uri & POP_MVC.config("VAR_PAGE") & "=" & 1	
			else
				FirstLink = replace( route, POP_MVC.config("VAR_PAGE"),1 )
			end if				
		end if
	End Property
	
	'上一页链接地址
	Public Property Get PrevLink
		if page > 1 then 
			if route = "" then
				uri = getUri
				PrevLink = uri & POP_MVC.config("VAR_PAGE") & "=" & (page-1)	
			else
				PrevLink = replace( route, POP_MVC.config("VAR_PAGE"),page-1 )
			end if				
		end if
	End Property
	
	'下一页链接地址
	Public Property Get NextLink
		if page < totalPage then 
			if route = "" then
				uri = getUri
				NextLink = uri & POP_MVC.config("VAR_PAGE") & "=" & (page+1)	
			else
				NextLink = replace( route, POP_MVC.config("VAR_PAGE"), CStr(page+1) )
			end if	
		end if		
	End Property
	
	'最后一页链接地址
	Public Property Get LastLink
		if totalPage > 1 then 
			if route = "" then
				uri = getUri
				LastLink = uri & POP_MVC.config("VAR_PAGE") & "=" & totalPage	
			else
				LastLink = replace( route, POP_MVC.config("VAR_PAGE"),totalPage )
			end if				
		end if
	End Property
	
	Public Property Get NumList
		dim dLeft,dRight,nav
		nav = array()
		if total < 1 Then
			NumList = Array()
			Exit Property
		end if

		If totalPage > 1 then
			POP_MVC.Arr.push nav,"<span class='layui-laypage-curr'><em class='layui-laypage-em'></em><em>" & page & "</em></span>"
		End If
		
		dLeft = page - 1
		dRight = page + 1
			
		
		do While dLeft >= 1 OR dRight <= totalPage		
			if UBound(nav) >= pageCount-1 then
				exit do
			end if		
			
			if dLeft>= 1 then	
				POP_MVC.Arr.Unshift nav,getUrl(dLeft,dLeft,aClass)				
				dLeft = dLeft - 1
			end if
			
			if dRight <= totalPage then
				if dRight >= 1 then
					POP_MVC.Arr.Push nav,getUrl(dRight,dRight,aClass)
				end if
				dRight = dRight + 1
			end if
		Loop
		NumList = nav
	End Property

	
	'返回页面字符串
	'如果总数为0，则提示无数据
	'如果总页数不足一页，则输出空字符串
	Public Function Show()
		dim nav,temp,i,html
		
		if total < 1 and not isNul(noRecordTip) then
			Show = noRecordTip
			exit Function
		end if
	
		nav = NumList
		
		if page > 1 and totalPage > pageCount then 
			if not is_empty(prev) 	then  POP_MVC.Arr.Unshift nav,getUrl(page-1,prev,prevClass)
			if not is_empty(first) 	then  POP_MVC.Arr.Unshift nav,getUrl(1,first,firstClass)				
		end if
		
		if page < totalPage and totalPage > pageCount then
			if not is_empty([next]) then  POP_MVC.Arr.Push nav,getUrl( page+1,[next] , nextClass )
			if not is_empty(last) 	then  POP_MVC.Arr.Push nav,getUrl( totalPage,last,lastClass )		
		end if		
		if not is_empty(pageScale) then POP_MVC.Arr.Unshift nav,page & "/" & totalPage & "页" & "&nbsp;"

		
		temp = page
		if temp < totalPage then
			temp = temp + 1
		elseif temp = totalPage then
			temp = 1
		end if		
		
		if totalPage > 1 and isEmpty( jump ) and totalPage > pageCount then		
			jump = "<span class='layui-laypage-skip'>　到第<input type='text' id='popasp_jump_page' class='layui-input' onkeydown=""javascript:if(event.keyCode==13){var page=(this.value>" & totalPage & ")?" & totalPage & ":this.value;location='"& uri & POP_MVC.config("VAR_PAGE") & "='+page;}"" value='" & temp & "' />页<button type='button' class='layui-laypage-btn' onclick=""var page=(document.getElementById('popasp_jump_page').value>" & totalPage & ")?" & totalPage & ":document.getElementById('popasp_jump_page').value;location='"& uri & POP_MVC.config("VAR_PAGE") & "='+page;"" value='" & temp & "'>确定</button></span>"
			
			POP_MVC.Arr.Push nav,jump
		end if
		
		if not is_empty(header) then POP_MVC.Arr.push nav,"<span class='layui-laypage-count'>共 " & total & " " & header & "</span>"	
		
		if not isEmpty( pagesize ) then
			if not isArray(pagesize) then
				pagesize = split(pagesize,",")
			end if
			
				if total > CInt(pagesize(0)) then
					if seperator = "" then
						POP_MVC.Arr.Push nav,"&nbsp;&nbsp;" & perpageTip
					else
						POP_MVC.Arr.Push nav,perpageTip
					end if
					html = "<span class='layui-laypage-limits'><select lay-ignore='' ONCHANGE=""location.href=this.value"">"
					for i = 0 to ubound(pagesize)			
						if Cint(pagesize(i)) - perpage =0 then
							html = html & "<option selected='' value='" & getUrl3( pagesize(i) ) & "'> " & pagesize(i) &  "条/页 </option>"
						else
							html = html & "<option value='" & getUrl3( pagesize(i) ) & "'> " & pagesize(i) & "条/页 </option>"
						end if
						if CInt(pagesize(i)) > total then
							exit for							
						end if
					next
					html = html & "</select></span>"
					POP_MVC.Arr.Push nav,html
				end if		
		end if

		Show = "<div class='layui-table-page'><div id='layui-table-page1'><div class='layui-box layui-laypage layui-laypage-default'>" & join( nav,seperator ) & "</div></div></div>"
	End Function
	
End Class
%>