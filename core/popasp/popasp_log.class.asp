<%
' +----------------------------------------------------------------------
' | POPASP [ ASP MVC ]
' +----------------------------------------------------------------------
' | Copyright (c) 2016 http://popasp.com All rights reserved.
' +----------------------------------------------------------------------
' | Author: popasp <1737025626@qq.com>
' +----------------------------------------------------------------------
Class POPASP_LOG
	public  logSize
	private logFile
	private sqlFile
	private url,ip
	
	private sub Class_Initialize
		logFile = POP_MVC.appPath & "/runtime/log/curr.log"
		sqlFile = POP_MVC.appPath & "/runtime/log/curr.sql"
		logSize = 1024*1024*5
		ip = get_client_ip()
		url = GetLocationURL
	End Sub
	
	public property get write( byval contents )	
		dim str,key		
				
		if isArray( contents ) then
			str = ""
			for i = 0 to ubound( contents )
				str = str & "IP:" & ip & " | time:" & now() & " | uri:" & url & " | " & contents(i) & VbCrLf
			next
		else
			str = "IP:" & ip & " | time:" & now() & " | uri:" & url & " | " & contents & VbCrLf
		end if
		
		'如果文件不存在，则创建该文件
		if not POP_MVC.file.isExists( logFile ) then
			Call POP_MVC.file_put_contents( logFile, str  )
			Exit property
		end if		
		
		call prepare( logFile,"log" )
		
		Call POP_MVC.file_append_contents( logFile , str )
	end property
	
	
	private sub prepare( file ,stype )
		dim bak_file,temp,theDate		
		
		if POP_MVC.file.fileSize( file ) < logSize then
			exit sub
		end if		
		
		theDate = now()
		
		bak_file = Year( theDate )	'年			
		
		temp = Month( theDate )	'月
		bak_file = bak_file & iif( temp<10 , "0" & temp , temp )		
		
		temp = Day( theDate )	'日
		bak_file = bak_file & iif( temp<10 , "0" & temp , temp )
		
		temp = Hour( theDate )	'时
		bak_file = bak_file & iif( temp<10 , "0" & temp , temp )
		
		temp = Minute( theDate )	'时
		bak_file = bak_file & iif( temp<10 , "0" & temp , temp )
		
		bak_file = POP_MVC.appPath & "/runtime/log/" & bak_file & "." & stype
		
		POP_MVC.file.rename file,bak_file
	end sub
	
	Private Function GetLocationURL() 
		Dim Url 
		Dim ServerPort,ServerName,ScriptName,QueryString 
		ServerName = Request.ServerVariables("SERVER_NAME") 
		ServerPort = Request.ServerVariables("SERVER_PORT") 
		ScriptName = Request.ServerVariables("SCRIPT_NAME") 
		QueryString = Request.ServerVariables("QUERY_STRING") 
		Url= ServerName 
		If ServerPort <> "80" Then Url = Url & ":" & ServerPort 
		Url=Url & ScriptName 
		If QueryString <>"" Then Url=Url&"?"& QueryString 
		GetLocationURL=Url 
	End Function 
End Class
%>