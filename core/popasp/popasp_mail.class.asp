<%
' +----------------------------------------------------------------------
' | POPASP [ ASP MVC ]
' +----------------------------------------------------------------------
' | Copyright (c) 2016 http://popasp.com All rights reserved.
' +----------------------------------------------------------------------
' | Author: popasp <1737025626@qq.com>
' +----------------------------------------------------------------------
Class POPASP_MAIL

  Private s_SMTPServer, s_FromMail, s_FromName, s_MailServerUserName, s_MailServerPassword, s_Charset,s_Attchment
  private s_ToMail, s_CC, s_BCC, s_Subject, s_Body
  Private Sub Class_Initialize()
		's_SMTPServer = "smtp.qq.com"
		's_FromMail = "1737025626@qq.com"
		's_FromName = "POPASP"
		's_MailServerUserName = "1737025626@qq.com"
		's_MailServerPassword = ""		'163为帐户密码，qq为授权码
		s_Charset = "utf-8"
  End Sub  
  
  '邮箱服务器
  Public Property Let SMTPServer(ByVal value)
    s_SMTPServer = value
  End Property
  
  Public Property Get SMTPServer()
	SMTPServer = s_SMTPServer
	if SMTPServer = "" then
		SMTPServer = "smtp." & POP_MVC.String.after( User, "@" )
	end if
  End Property 
  
  '登陆帐户
  Public Property Let User(ByVal value)
    s_MailServerUserName = value
  End Property
  
  '登陆密码
  Public Property Let Password(ByVal value)
    s_MailServerPassword = value
  End Property  
  
  '发件人邮箱
  Public Property Let FromMail(ByVal value)
    s_FromMail = value
  End Property
  
  Public Property Get FromMail() 
    FromMail = s_FromMail
	if FromMail = "" then
		FromMail = User
	end if
  End Property    
  
  '发件人称呼  
  Public Property Let FromName(ByVal value )
	s_FromName = value
  End Property
  
  Public Property Get FromName()
    FromName = s_FromName
	if FromName = "" then
		FromName = POP_MVC.String.before( User, "@" )
	end if
  End Property 
  
  '收件人邮箱
  Public Property Let ToMail(ByVal arg )
	s_ToMail = getEmail( arg )
  End Property
  
  '抄送邮箱
  Public Property Let CC(ByVal arg )
	s_CC = getEmail( arg )
  End Property
  
  '密送邮箱，多次测试发现163不能进行密送，而qq可以
  Public Property Let BCC(ByVal arg )
	s_BCC = getEmail( arg )
  End Property

	Private Function getEmail ( arg )
		dim email
		email = ""
		if typename( arg ) = "Dictionary" then	'键名是邮箱，值是用户名		
			for each key in arg
				email = email & arg(key) & "<" & key & ">;"
			next
		elseif typename( arg ) = "Recordset" then	'第一个是用户名，第二个是邮箱
			Do While Not arg.BOF And Not arg.EOF
				email = email & arg.Fields(0).Value & "<" & arg.Fields(1).value	 & ">;"
				arg.MoveNext
			Loop
			arg.close : set arg = nothing		
		elseif isArray( arg ) then
			email = "<" & join( arg, ">;<" ) & ">"
		else
			email = arg			
		end if
		getEmail = email
	end Function
  
  '邮件主题
  Public Property Let Subject(ByVal value)
    s_Subject = value
  End Property
  
  '邮件内容
  Public Property Let Body(ByVal value)
    s_Body = value
  End Property
  
  '附件,多个用;分开
  Public Property Let Attchment(ByVal value)
    s_Attchment = value
  End Property 
  
  '编码
  Public Property Let Charset(ByVal value)
    s_Charset = value
  End Property 
  
  Public Property Get Subject()
    Subject = s_Subject
  End Property
  
  Public Property Get Body()
	Body = s_Body
  End Property
        
  
  Public Property Get User() : User = s_MailServerUserName : End Property        
  Public Property Get Password() : Password = s_MailServerPassword : End Property               
  Public Property Get ToMail() : ToMail = s_ToMail : End Property    
  Public Property Get Attchment() : Attchment = s_Attchment : End Property
  Public Property Get Charset() : Charset = s_Charset : End Property

  '发送邮件，返回状态1 ， 2 ， 3   状态1为检测不到JMAIL组件  554为垃圾邮件。0为发送成功
  Public Function Send()
	  on error resume next
	  Dim objConfig,objMessage,Fields
	  dim startTime : startTime = timer()
	  Set objConfig = POP_MVC.SCO("CDO.Configuration") 
	  Set Fields = objConfig.Fields 
	  dim arr,i
	 
	  dim schema : schema = "http://schemas.microsoft.com/cdo/configuration/"
	  With Fields 
	  .Item( schema & "sendusing" ) = 2	'发送邮件端口 
	  .Item( schema &  "smtpserver" ) = SMTPServer
	  .Item( schema &  "smtpserverport") = 25	'SMTP服务器端口                
	  .Item( schema &  "smtpconnectiontimeout") = 10     
	  if instr( POP_MVC.Vars( "SERVER_PROTOCOL" ) , "HTTPS" ) > 0 or  POP_MVC.Vars( "HTTPS" ) = "on" then
		.Item( schema &  "smtpusessl") = true    '是否使用ssl 
	  .Item( schema &  "smtpserverport") = 465	'SMTP服务器端口                
	  .Item( schema &  "smtpconnectiontimeout") = 60
	  else
	  .Item( schema &  "smtpserverport") = 25	'SMTP服务器端口                
	  .Item( schema &  "smtpconnectiontimeout") = 10 
	  end if
	  .Item( schema &  "smtpauthenticate") = 1
	  .Item( schema &  "sendusername" ) = User		'邮箱登陆帐户名
	  .Item( schema & "sendpassword") = Password	'邮箱登陆密码
	  .Update 
	  End With 	  
	  
	  Set objMessage = POP_MVC.SCO("CDO.Message") 
	  Set objMessage.Configuration = objConfig 
	  
	  With objMessage
		.BodyPart.Charset = "utf-8"
		.To = ToMail
		if not isEmpty( s_CC ) then
			.Cc = s_CC '抄送
		end if
		if not isEmpty( s_BCC ) then
			.Bcc = s_BCC '密送
		end if
		.From=  FromName & "<" & FromMail & ">"
		.Subject= Subject
		.HtmlBody= Body
	   '.TextBody = Trim(POP_MVC.Form("m_neirong")) '文本正文
	  End With 
	  
		'添加附件
		if  not isEmpty(s_Attchment) then
			if isArray( s_Attchment ) then
				arr = s_Attchment
			else
				arr = split( s_Attchment, ";" )
			end if
			for i = 0 to ubound( arr )
				Call objMessage.AddAttachment( POP_MVC.realPath(arr(i))  )    '邮件收件人的地址
			next
		end if	

		
		
	objMessage.send
	Send = err.number 

	  Set Fields = Nothing 
	  Set objMessage = Nothing 
	  Set objConfig = Nothing 
	  
	  if err.number = 0 then
		call POP_MVC.pushTime( startTime , "使用CDO.Message发送主题为 " & subject & " 的邮件耗时")
	  else
		Call L_( Typename(Me) & "Send" )
	  end if
  End Function
End Class
%>