<%
' +----------------------------------------------------------------------
' | POPASP [ ASP MVC ]
' +----------------------------------------------------------------------
' | Copyright (c) 2016 http://popasp.com All rights reserved.
' +----------------------------------------------------------------------
' | Author: popasp <1737025626@qq.com>
' +----------------------------------------------------------------------
Class POPASP_MODEL
	Private Function dateCompare( date1,date2 )
		dateCompare = DateDiff("s",date2,date1)
	End Function
	
	'创建一个Model文件
	Function CreateModelFile( db_type,db_path,tableName,prikey,ignoreExists )
		dim dirName,filePath,model_str,bool,modelPath
		CreateModelFile = False
		
		dirName = POP_MVC.File.baseName( array( db_path,1 ) )
		if dirName = "" Then dirName = "db"
		filePath = POP_MVC.appPath + "/Model/" + LCase(db_type) + "/" + dirName + "/" + POP_MVC.String.UCFirst( LCase(tableName) ) + ".asp"	'如./home/Model/excel/15251成绩/1学期.asp
		
		bool = false
		if not is_empty(ignoreExists) then	'忽略文件的存在性
			bool = true
		elseif POP_MVC.file.isExists( filePath ) then	'如果目标文件不存在
			bool = true
		end if
		
		if bool then
			'创建目标文件				
			model_str = POP_MVC.asp_get_contents( POP_MVC.mvc_dir + "Tpl/model2.html" )
			model_str = Replace(model_str,"TABLENAME" ,tableName ,1,1,0)
			model_str = Replace(model_str,"TABLENAME" ,tableName ,1,1,0)
			model_str = Replace(model_str,"PRIKEY" ,prikey ,1,1,0)
			call POP_MVC.file_put_contents( filePath, "<" + "%" + trim(model_str) + "%" + ">" )	
		end if
		
		if POP_MVC.file.isExists( filePath ) then	'如果目标文件不存在
			CreateModelFile = true
		end if
	End Function
	

	'创建Model对象，可以用 M_("表名") 来创建
	Function Create(ByVal arg)
		
		on error resume next			
		Dim model_path,dst_path,model_str,bReload,key,tableName,runtime_model,runtime_arg
		
		Dim db_type,access_type,db_path,db_host,db_user,db_name,db_pwd,db_prefix,bnd
		Dim ctrl_name , ExtendName , trueCtrl , extendFile
		
		'分配数据库相关的配置项
		db_type = POP_MVC.config("DB_TYPE")
		access_type = POP_MVC.config("ACCESS_TYPE")
		db_path = POP_MVC.config("DB_PATH")
		db_name = POP_MVC.config("DB_NAME")
		db_host = POP_MVC.config("DB_HOST")
		db_user = POP_MVC.config("DB_USER")
		db_pwd  = POP_MVC.config("DB_PWD")
		db_prefix  = POP_MVC.config("DB_PREFIX")
		
		if not isArray( arg ) then
			tableName = arg
		else
			tableName = arg(0)
			'如果是数组的话，则按如下参数分配
			'access: tableName,db_type,db_path,access_type
			'excel: tableName,db_type,db_path
			'sqlite3: tableName,db_type,db_path
			'mysql: tableName,db_type,db_name,db_host,db_user,db_pwd
			'sql server: tableName,db_type,db_name,db_host,db_user,db_pwd
			if ubound( arg ) > 0 then
				db_type = trim(lcase(arg(1)))
				bnd = ubound( arg )
				if db_type = "xml" then
					set Create = CreateXMLModel(arg)
					exit function
				elseif db_type = "access" then
					if bnd > 1 then db_path = arg(2)
					if bnd > 2 then access_type = arg(3)					
				ElseIf db_type = "excel" or  db_type = "sqlite3" or  db_type = "txt" then
					if bnd > 1 then db_path = arg(2)
				ElseIf db_type = "mysql" or  db_type = "sqlserver" then
					if bnd > 1 then db_name = arg(2)
					if bnd > 2 then db_host = arg(3)
					if bnd > 3 then db_user = arg(4)
					if bnd > 4 then db_pwd  = arg(5)
					if bnd > 5 then db_prefix  = arg(6)
				end if
			end if
		end if
		
		If tableName = "" Then tableName = POP_MVC.c
		
		if db_type = "access" or db_type = "excel" or  db_type = "sqlite3" then
			runtime_arg = array( tableName , db_type , db_path )
		else
			runtime_arg = array( tableName , db_type , db_name )
		end if		
		
		runtime_model = POP_MVC.getModelRunName( runtime_arg )
		
		key = runtime_model + "Model"	
		
		if POP_MVC.dModel.Exists(key) then
			if typeName( POP_MVC.dModel(key) ) = key Then
				set Create = POP_MVC.dModel(key)
				Exit Function
			end if
		end if

		bReload = false		

		Call POP_MVC.calExtend( tableName , trueCtrl , ExtendName  )	

		
		if ExtendName <> "" then
			tableName = trueCtrl
			extendFile = POP_MVC.rtrim(POP_MVC.config("EXTEND_PATH") , "/" ) + "/" + ExtendName + "/" + trueCtrl + ".asp"
			runtime_arg = Array(extendFile , db_type , db_name )
			if POP_MVC.file.isFile( extendFile ) then
				model_path = POP_MVC.getModelPath( runtime_arg )
			end if			
		else
			model_path = POP_MVC.getModelPath( runtime_arg )
		end if
	
		dst_path   = POP_MVC.getModelDstPath( runtime_arg )

		IF model_path <> "" Then	'如果已经定义了model
			If Not POP_MVC.file.isFile(dst_path) Then	'如果目标文件不存在					
				bReload = True
			ElseIf dateCompare(POP_MVC.file.mtime(POP_MVC.mvc_dir + "popasp_model.class.asp"),POP_MVC.file.mtime(dst_path)) > 0 Then	' 如果popasp_model.class.asp进行了最新修改				
				bReload = True
			ElseIf dateCompare(POP_MVC.file.mtime(model_path),POP_MVC.file.mtime(dst_path)) > 0 Then ' 如果 模型文件 进行了最新修改
				bReload = True			
			Else
				'获取文件内容
				bReload = false
				model_str = POP_MVC.asp_get_contents( dst_path )
			End If
			

			
			if bReload Then
				model_str = POP_MVC.asp_get_contents( model_path )	
				model_str =  POP_MVC.String.reg_replace( model_str, "Class [" + runtime_model + "Model]"  + vbCrLf + vbTab + "Public db", "^\s*Class\s+\[?" + tableName + "\]?","im" )
				call POP_MVC.file_put_contents( dst_path, "<" + "%" + vbCrLf + trim(model_str) + vbCrLf + "%" + ">" )	
			End If			
		Else	'没有定义model	
			If POP_MVC.file.isFile( dst_path )	Then '如果解析后的文件存在
				'获取文件内容
				model_str = POP_MVC.asp_get_contents( dst_path )
			Else
				'创建目标文件				
				model_str = POP_MVC.asp_get_contents( POP_MVC.mvc_dir + "Tpl/model1.html" )
				model_str = Replace(model_str,"TABLENAME" ,runtime_model ,1,1,0)
				model_str = Replace(model_str,"TABLENAME" ,tableName ,1,1,0)
	
				call POP_MVC.file_put_contents( dst_path, "<" + "%" + vbCrLf + trim(model_str) + vbCrLf + "%" + ">" )	
			end If			
		End If

		'实例化类
		Execute model_str

		if err.number <> 0 then
			if  is_empty(POP_MVC.config("APP_DEBUG")) then
				call L_("")				
			else 
				call L_( POP_MVC.UCFirst(tableName) + "Model 类出错：" )
				call POP_MVC.quit
			end if			
		end if
		
		Execute "set POP_MVC.dModel(""" + key + """) = new " + key	
		
		'给对象中的db赋值
		if db_type = "access" or db_type = "excel" or db_type = "sqlite3" then
			db_path = POP_MVC.realPath( db_path )
			set POP_MVC.dModel(key).db = P_( array("POPASP_" + UCase(db_type) , LCase(tableName) + "#" + db_path) )
		else
			set POP_MVC.dModel(key).db = P_( array("POPASP_" + UCase(db_type) , LCase(tableName) + "#" + db_name + "#" + db_host) )
		end if
		
		POP_MVC.dModel(key).db.db_type = db_type
		POP_MVC.dModel(key).db.access_type = access_type
		POP_MVC.dModel(key).db.db_path = db_path
		POP_MVC.dModel(key).db.db_name = db_name
		POP_MVC.dModel(key).db.db_host = db_host
		POP_MVC.dModel(key).db.db_user = db_user
		POP_MVC.dModel(key).db.db_pwd  = db_pwd
		POP_MVC.dModel(key).db.db_prefix  = db_prefix
		Call POP_MVC.dModel(key).db.assignTool( db_type,access_type,db_path,db_host,db_user,db_name,db_pwd , db_prefix )

		Call POP_MVC.dModel(key).init
		
		if isEmpty(POP_MVC.dModel(key).db.tableName) then
			POP_MVC.dModel(key).db.tableName = tableName
		end if
		
		if POP_MVC.config( UCase( db_type ) + "_PREFIX"  ) <> "" then
			if Not POP_MVC.String.iStartsWith( POP_MVC.dModel(key).db.tableName , POP_MVC.config( UCase( db_type ) + "_PREFIX"  ) )  then
				POP_MVC.dModel(key).db.tableName = POP_MVC.config( UCase( db_type ) + "_PREFIX"  ) + POP_MVC.dModel(key).db.tableName
			end if
		elseif db_prefix <> "" then
			if Not POP_MVC.String.iStartsWith( POP_MVC.dModel(key).db.tableName , db_prefix )  then
				POP_MVC.dModel(key).db.tableName = db_prefix + POP_MVC.dModel(key).db.tableName
			end if
		end if
		
		set Create = POP_MVC.dModel(key)
		Call L_("")
	End Function
	
	
	'创建Model对象，可以用K_(模型名) 来创建
	Function CreateMethodModel(ByVal modelName)		
		on error resume next			
		Dim model_path,dst_path,model_str,bReload,key,runtime_model,runtime_arg
		
		Dim ctrl_name , ExtendName , trueCtrl , extendFile
		
		If modelName = "" Then modelName = POP_MVC.c

		Call POP_MVC.calExtend( modelName , trueCtrl , ExtendName  )
		
		if ExtendName <> "" then
			modelName = trueCtrl
			extendFile = POP_MVC.rtrim(POP_MVC.config("EXTEND_PATH") , "/" ) + "/" + ExtendName + "/" + trueCtrl + ".asp"
			runtime_arg = Array(extendFile , db_type , db_name )		
		else
			runtime_arg = array( modelName , "" , "" )
		end if
		
		runtime_model = POP_MVC.getModelRunName( runtime_arg )
		
		key = LCase(runtime_model) + "Model"
		
		if POP_MVC.dModel.Exists(key) then			
			set CreateMethodModel = POP_MVC.dModel(key)
			Exit Function
		end if

		bReload = false	
		model_path = POP_MVC.getModelPath( runtime_arg )	
		dst_path   = POP_MVC.getModelDstPath( runtime_arg )
		
		if not POP_MVC.file.isFile( model_path ) then
			POP_MVC.Warning( modelName + "插件文件不存在" )
			set CreateMethodModel = nothing
			Exit Function
		end if
		
		If Not POP_MVC.file.isFile(dst_path) Then	'如果目标文件不存在					
			bReload = True
		ElseIf dateCompare(POP_MVC.file.mtime(POP_MVC.mvc_dir + "popasp_model.class.asp"),POP_MVC.file.mtime(dst_path)) > 0 Then	' 如果popasp_model.class.asp进行了最新修改				
			bReload = True
		ElseIf dateCompare(POP_MVC.file.mtime(model_path),POP_MVC.file.mtime(dst_path)) > 0 Then ' 如果 模型文件 进行了最新修改
			bReload = True			
		Else
			'获取文件内容
			bReload = false
			model_str = POP_MVC.asp_get_contents( dst_path )
		End If
		
		if POP_MVC.String.iEndsWith( modelName, ".asp" ) then
			modelName = POP_MVC.file.basename( array(modelName,1) )
		end if
		
		if bReload Then
			model_str = POP_MVC.asp_get_contents( model_path )	
			model_str =  POP_MVC.String.reg_replace( model_str, "Class [" + runtime_model + "Model]" , "^\s*Class\s+\[?" + modelName&"\]?","im" )
			call POP_MVC.file_put_contents( dst_path, "<" + "%" + vbCrLf + trim(model_str) + vbCrLf + "%" + ">" )	
		End If	
		

		'实例化类
		Execute model_str	
		
		if err.number <> 0 then
			if  is_empty(POP_MVC.config("APP_DEBUG")) then
				call L_("")				
			else 
				call L_( POP_MVC.UCFirst(modelName) + "Model 类出错：" )
				call POP_MVC.quit
			end if			
		end if
		
		Execute "set POP_MVC.dModel(""" + key + """) = new [" + key + "]"	
		
		set CreateMethodModel = POP_MVC.dModel(key)
		
		Call L_("")
	End Function
	
	'创建Model对象，可以用 B_("表名") 来创建
	Function CreateDb(ByVal arg)
		on error resume next			
		Dim tableName,runtime_arg,db
		
		Dim db_type,access_type,db_path,db_host,db_user,db_name,db_pwd,db_prefix,bnd
		
		'分配数据库相关的配置项
		db_type = POP_MVC.config("DB_TYPE")
		access_type = POP_MVC.config("ACCESS_TYPE")
		db_path = POP_MVC.config("DB_PATH")
		db_name = POP_MVC.config("DB_NAME")
		db_host = POP_MVC.config("DB_HOST")
		db_user = POP_MVC.config("DB_USER")
		db_pwd  = POP_MVC.config("DB_PWD")
		db_prefix  = POP_MVC.config("DB_PREFIX")
		
		if not isArray( arg ) then
			tableName = arg
		else
			tableName = arg(0)
			'如果是数组的话，则按如下参数分配
			'access: tableName,db_type,db_path,access_type
			'excel: tableName,db_type,db_path
			'sqlite3: tableName,db_type,db_path
			'txt: tableName,db_type,db_path
			'mysql: tableName,db_type,db_name,db_host,db_user,db_pwd
			'sql server: tableName,db_type,db_name,db_host,db_user,db_pwd
			if ubound( arg ) > 0 then
				db_type = trim(lcase(arg(1)))
				bnd = ubound( arg )
				if db_type = "xml" then
					set CreateDb = CreateXML(arg)
					exit function
				elseif db_type = "access" then
					if bnd > 1 then db_path = arg(2)
					if bnd > 2 then access_type = arg(3)					
				ElseIf db_type = "excel" or  db_type = "sqlite3" or  db_type = "txt" then
					if bnd > 1 then db_path = arg(2)
				ElseIf db_type = "mysql" or  db_type = "sqlserver" then
					if bnd > 1 then db_name = arg(2)
					if bnd > 2 then db_host = arg(3)
					if bnd > 3 then db_user = arg(4)
					if bnd > 4 then db_pwd  = arg(5)
					if bnd > 5 then db_prefix  = arg(6)
				end if
			end if
		end if
		
		If tableName = "" Then tableName = POP_MVC.c
		
		if POP_MVC.config( UCase( db_type ) + "_PREFIX"  ) <> "" then
			if Not POP_MVC.String.iStartsWith( tableName , POP_MVC.config( UCase( db_type ) + "_PREFIX"  ) )  then
				tableName = POP_MVC.config( UCase( db_type ) + "_PREFIX"  ) + tableName
			end if
		elseif db_prefix <> "" then
			if Not POP_MVC.String.iStartsWith( tableName , db_prefix )  then
				tableName = db_prefix + tableName
			end if
		end if
		
		if db_type = "access" or db_type = "excel" or  db_type = "sqlite3" or db_type = "txt" then
			runtime_arg = array( tableName , db_type , db_path )
		else
			runtime_arg = array( tableName , db_type , db_name )
		end if	
		

		'给对象中的db赋值
		if db_type = "access" or db_type = "excel" or db_type = "sqlite3" or db_type = "txt" then
			db_path = POP_MVC.realPath( db_path )
			set db = P_( array("POPASP_" + UCase(db_type) , LCase(tableName) + "#" + db_path) )
		else
			set db = P_( array("POPASP_" + UCase(db_type) , LCase(tableName) + "#" + db_name + "#" + db_host) )
		end if	
	
		db.db_type = db_type
		db.access_type = access_type
		db.db_path = db_path
		db.db_name = db_name
		db.db_host = db_host
		db.db_user = db_user
		db.db_pwd  = db_pwd
		Call db.assignTool( db_type,access_type,db_path,db_host,db_user,db_name,db_pwd , db_prefix )
		db.tableName = tableName

		set CreateDb = db
		Call L_("")
		
	End Function
	
	'创建XML的Model对象，可以用 X_("路径") 来创建
	Function CreateXML(ByVal arg)
		on error resume next			
		Dim path,db		
		Dim db_type,db_path
		
		'分配数据库相关的配置项
		db_type = POP_MVC.config("XML_TYPE")
		db_path = POP_MVC.config("XML_PATH")
		
		if not isArray( arg ) then
			path = arg
		else
			path = arg(0)
			'如果是数组的话，则按如下参数分配
			'path,db_path,db_type
			if ubound( arg ) > 0 then
				db_path = trim(lcase(arg(1)))
				if ubound( arg )  > 1 then db_type = arg(2)
			end if
		end if

		set db = P_( array("POPASP_XMLDB" , path + "#" + db_path + "#" + db_type) )

		db.db_type = db_type
		db.db_path = db_path
		Call db.path( path )
		set CreateXML = db
		Call L_("")		
	End Function
	
	'创建XMLDB的Model对象，可以用 XM_("路径") 来创建
	Function CreateXMLModel(ByVal arg)		
		on error resume next			
		Dim model_path,model_str,key,runtime_model,runtime_arg,tableName
		
		Dim db_type,db_path
		Dim ctrl_name , ExtendName , trueCtrl
		
		'分配数据库相关的配置项
		db_type = POP_MVC.config("XML_TYPE")
		db_path = POP_MVC.config("XML_PATH")
		
		if not isArray( arg ) then
			tableName = arg
		else
			tableName = arg(0)
			'如果是数组的话，则按如下参数分配
			'path,db_path,db_type
			if ubound( arg ) > 0 then
				db_path = trim(lcase(arg(1)))
				if ubound( arg )  > 1 then db_type = arg(2)
			end if
		end if
		

		
		runtime_arg = array( tableName , "xml" , db_path + db_type)		
		runtime_model = POP_MVC.getModelRunName( runtime_arg )
		key = LCase(runtime_model) + "Model"
		

		
		if POP_MVC.dModel.Exists(key) then			
			set CreateXMLModel = POP_MVC.dModel(key)
			POP_MVC.dModel(key).db.db_type = db_type
			POP_MVC.dModel(key).db.db_path = db_path
			Call POP_MVC.dModel(key).init
			Call POP_MVC.dModel(key).db.path(POP_MVC.dModel(key).path)
			Exit Function
		end if

		Call POP_MVC.calExtend( tableName , trueCtrl , ExtendName  )	

		if ExtendName <> "" then
			model_path = POP_MVC.rtrim(POP_MVC.config("EXTEND_PATH") , "/" ) + "/" + ExtendName + "/" + trueCtrl + ".asp"	
		else
			model_path = POP_MVC.appPath + "/Model/" + tableName + ".asp"
		end if

		IF POP_MVC.file.isFile(model_path) Then	'如果已经定义了model
			model_str = POP_MVC.asp_get_contents( model_path )		

			model_str =  POP_MVC.String.reg_replace( model_str, "Class [" + runtime_model + "Model]"  + vbCrLf + vbTab + "Public db,path" , "^\s*Class\s+\[?" + trueCtrl + "\]?","im" )
		else
			POP_MVC.quit( "未定义" + trueCtrl + "的model类" )
		End If


		'实例化类
		Execute model_str

		if err.number <> 0 then
			if  is_empty(POP_MVC.config("APP_DEBUG")) then
				call L_("")				
			else 
				call L_( POP_MVC.UCFirst( tableName ) + "Model 类出错：" )
				call POP_MVC.quit
			end if			
		end if
		
				
		
		Execute "set POP_MVC.dModel(""" + key + """) = new " + key	


		'给对象中的db赋值
		set POP_MVC.dModel(key).db = P_( array("POPASP_XMLDB" , key) )
		

		
		POP_MVC.dModel(key).db.db_type = db_type
		POP_MVC.dModel(key).db.db_path = db_path
		Call POP_MVC.dModel(key).init
		
		
		if not isNul(path) then
			Call POP_MVC.dModel(key).db.path(POP_MVC.dModel(key).path)
		end if
		set CreateXMLModel = POP_MVC.dModel(key)
		Call L_("")
	End Function
End Class
%>