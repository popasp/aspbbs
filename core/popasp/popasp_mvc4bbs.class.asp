<%
' +----------------------------------------------------------------------
' | POPASP [ ASP MVC ]
' +----------------------------------------------------------------------
' | Copyright (c) 2016 http://popasp.com All rights reserved.
' +----------------------------------------------------------------------
' | Author: popasp <1737025626@qq.com>
' +----------------------------------------------------------------------
Class POPASP_MVC4BBS
	'在本类中不能使用array函数

	'这组变量对外使用
	public appPath,applicationOn
	'项目路径、是否将框架程序与控制器文件写入application、是否将配置文件写入appliction
	public mvc_dir,root_path,config		'框架路径， 根路径， 项目路径， 配置参数(Dictionary对象)
	public Version	'版本号
	
	' FileSystemObject、RegExp对象
	Public fso,reg
	
	' 具体的类 Application、POPASP_DICTIONARY、POPASP_FILE、POPASP_URL,POPASP_MOLIBUPLOAD,POPASP_ARR、POPASP_STRING
	Public Apt,Dict,File,Url,Uploader,Arr,[String]	
	Public c,a		' 控制器与方法名
	
	'这组变量二次开发应了解其用法
	Public dRejection	' 反射类的寄存器(二维Dictionary对象)
	Public dSql,dTpl,dFlow,dTime,dMd5		' 存储sql、模板引擎信息、流程、性能(耗时)、md5
	Public dClass		' 存储实例化的类
	Public dCMS			' cms过程信息
	Public dPlugin		' 存储工具类
	Public dModel		' 存储实例化的model	
	Public dDict		' 存储实例化的Dictionary对象
	public dG_,dL_	'对应获取函数G_、N_、L_、T_
	public dJS			'存储js用的json
	Public tpl_vars		'模板引擎中要用到的变量
	Public num_db_write,num_db_query,num_recordset
	
	Public page_trace_include	'popasp的include行
	Public dObj			'创建对象记录
	Public dAuto			'创建对象记录
	Public runtimeOn	
	
	'本类的私有变量
	Private objExtendsClass,action_count,is_init_popasp		' 继承类的实例化
	Private isExit,dAction
	Public isExecuteExtends		'是否在使用 Action_ 方法时，执行继承类的方法 initialize
	private spent_limit	'超时时间，单位ms
	private isPageCache,ctrl_son,method_exists,temp_ctrl
	Private dClassContent,s_httpHost,isLoadExtendConfig,objReject
	Public dQuery,CurExtend,startTime

	'POPASP的初始化
	'早于POP_MVC.run或POP_MVC.start
	'调用run或start时，会自动调用init。
	'init多次调用时，仅一次有效。
	public Sub init
		on error resume next
		dim str,path,apt_key
		startTime = timer()		
		if isEmpty( mvc_dir ) then
			mvc_dir = get_mvc_dir()	'得到框架路径
		end if
		
		if not is_init_popasp Then
			set dG_ = CreateDict()
			dG_("REQUEST_TIME") = now()
			dG_("beginTime") = timer()	
			if isEmpty( Me.appPath ) then	'可以指定项目路径
				Me.appPath = Replace(APP_PATH,"\" , "/" )	'得到项目路径
			End if
			
			if right(Me.appPath,1) = "/" then
				Me.appPath = mid(Me.appPath,1,len(Me.appPath)-1 )
			end if 
			
			'加载 POPASP_STRING 类
			'set [String] = import("POPASP_STRING")
			set [String] = new POPASP_STRING
			
			'加载 POPASP_DICTIONARY 类
			'set Dict = import("POPASP_DICTIONARY")
			set Dict = new POPASP_DICTIONARY
			
			'加载 POPASP_ARR 类
			'set Arr = import("POPASP_ARR")
			set Arr = new POPASP_ARR
			
			' 加载项目配置文件
			path = Me.appPath + "/Conf/config.asp"
			Call loadConfig( path , 0 )
			
			'加载额外的配置文件，一般为整个开发项目的配置文件
			if not is_empty( Me.config("EXT_CONFIG") ) then
				path = Me.config("EXT_CONFIG") 
				Call loadConfig( path , 0 )
			end if
		
			' 记录加载文件时间
			Me.dG_( "loadTime" ) = timer()
		
			'反射类的寄存器
			set dRejection = CreateDict()
			
			' 加载 POPASP_FILE 类
			'set File = import("POPASP_FILE")
			set File = new POPASP_FILE
			
			' 加载 POPASP_URL 类
			'set Url = import("POPASP_URL")
			set Url = new POPASP_URL
			
			'初始化完成标识
			is_init_popasp = true
			
			call Url.set_tmpl_config
		end If
	End Sub
	
	'加载配置文件
	Public Sub loadConfig( path ,stype )
		dim apt_key,str
		' 加载项目配置文件
		If Me.fso.FileExists( Me.realPath(path) ) Then
			apt_key = Me.realPath( path )
			if not is_empty(Me.applicationOn) and not isEmpty( Application.contents( apt_key ) ) then			
				Execute( Application.contents( apt_key ) )			
			else
				str = file_get_contents( path )
				if not is_empty( str ) then
					str = Replace( str , "<" + "%" , "" )
					str = Replace( str , "%" + ">" , "" )
					if stype = 0 then
						Execute(str)
					else
						ExecuteGlobal(str)
					end if
					if err.number <> 0 then
						Me.exit( path  + "文件代码有误:" + err.number + "," + err.description )
					end if
					if not is_empty(Me.applicationOn) then
						Application.contents( apt_key ) = str
					end if
				end if
			end if
		end if	
	End Sub
	
	'得到运行时间
	Public Property Get runtime( )	
		'runtime = FormatNumber((timer() - Me.dG_("beginTime"))*1000,0,-1,0,0)
		runtime = FormatNumber(timer() - startTime,3,-1,0,0)
	End Property
	
	'生成提示消息errStr，推向控制台或日志文件(如果日志功能开启)
	Public Property Get Notice( errStr )
		on error resume next
		Err.Raise 1000,"POPASP Notice",errStr
		Call L_("")
	End Property
	
	'使用js的alert进行弹窗，消息提示
	'arg或为消息字符串，或为一个数组Array(提示消息,跳转url)
	Public Sub Alert( arg )
		dim str,url
		if isArray(arg) then
			str = arg(0)
			if ubound( arg ) > 0 then url = arg(1)
		else
			str = arg
		end if	
		
		if cstr(url) = "" then
			url = ME.Vars("HTTP_REFERER")
		end if
		
		if cstr(url) = "-1" then
			url = "history.back(-1);"
		else
			url="location.href='"&url&"';"
		end if		
		if not isNul(str) then str ="alert('"&str&"');"
		Response.write("<script>"&str&url&"</script>")
		response.End
	End Sub
	
	'生成警告消息errStr，推向控制台或日志文件(如果日志功能开启)
	Public Property Get Warning( errStr )
		on error resume next
		Err.Raise 1001,"POPASP Warning",errStr
		Call L_("")
	End Property
	
	'生成错误消息errStr，推向控制台或日志文件(如果日志功能开启)
	Public Property Get [Error]( errStr )
		on error resume next
		Err.Raise 1001,"POPASP Error",errStr
		Call L_("")
	End Property
	
	'生成js的window.open字符串
	'参数分别为：url地址、title标题、窗口宽度、窗口高度
	Public Function OpenWindow( url,title,width,height )
		OpenWindow = "javascript:window.open('" + url + iif( config( "URL_MODE" ) = 0,"&token=" & timer , "" ) + "','" & title & "','width=" & width & ",height=" & height & ",left='+(window.screen.availWidth - " & width & " )/2+',top='+(window.screen.availHeight - " & height & " )/2+',status=yes,toolbar=no,menubar=no,location=no')"
	End Function
	
	'返回服务器的地址，带端口
	'如http://www.popasp.com
	Public Property Get http_host( )
		if isEmpty( s_httpHost ) then
			if instr( Me.Vars( "SERVER_PROTOCOL" ) , "HTTPS" ) < 1 or  Me.Vars( "HTTPS" ) = "off" then
				s_httpHost = "http://" +  Me.Vars("HTTP_HOST")
			else
				s_httpHost = "https://" +  Me.Vars("HTTP_HOST")
			end if
		end if
		http_host = s_httpHost
	End Property
	
	'返回服务器的地址，不带端口
	'如http://www.popasp.com
	Public Property Get http_host2( )
		dim url,pos
		url = http_host
		pos = inStrRev( url,":" )
		if pos > 0 then
			http_host2 = left( url , pos -1 )
		else
			http_host2 = url
		end if
	End Property
	
	'记录开销耗时
	Public Function pushTime( startTime , text )
		if is_init_popasp and config("APP_DEBUG") = 1  Then
			dim spent
			spent = FormatNumber( (timer() - startTime ) * 1000 ,0,-1,0,0)
			spent = CInt( spent )
			if not isEmpty( dTime ) then
				if spent > spent_limit then
					dTime.add dTime.count , text & " " & spent & " ms"
				end if
			end if
			pushTime = spent		
		end if
	end Function
	
	'记录开销耗时间，用于POPASP_CMS类
	Public Function cmsPushTime( startTime , text )
		dim spent
		if is_init_popasp Then
			
			if not isEmpty( dCMS ) then
				if startTime > 0 then
					spent = FormatNumber( (timer() - startTime ) *1000,0,-1,0,0)
					spent = CInt( spent )
					if spent > spent_limit then
						dCMS.add dCMS.count , text & " " & spent & "ms"
					end if
				else
					dCMS.add dCMS.count , text					
				end if
			end if
			cmsPushTime = spent		
		end if
	end Function
	
	'记录开销耗时，用于模板类与模板解析类
	Public Function tplPushTime( startTime , text )
		if not is_init_popasp or not config("APP_DEBUG") = 1 Then
			dim spent
			spent = FormatNumber( (timer() - startTime ) *1000,0,-1,0,0)
			spent = CInt( spent )
			if not isEmpty( dTpl ) then
				if dTpl.exists( text ) then						
					dTpl( text ) = array(  dTpl( text )(0) + 1 , dTpl( text )(1) + spent )
				else
					dTpl.add text,array( 1 , spent )
				end if				
			end if
			tplPushTime = spent		
		end if
	end Function
	
	'向控制台AUTO菜单中记录自动化消息，主要用于POPASP_AUTO类
	Public Property Get pushAuto( msg )
		If config("APP_DEBUG") = 1 Then
			Me.dAuto( Me.dAuto.count + 1 ) = msg
		End If
	End Property
	
	'返回POPASP的logo图片base64字符串
	Public Function Logo()
		Logo = file_get_contents( mvc_dir + "Tpl/logo.txt" )
	End Function
	
	' 使用该方法与D_函数创建的Dictionary对象，程序结束时均可自动销毁
	Public Function CreateDict()
		dim count
		count = dDict.Count
		set dDict( count + 1 ) = Server.CreateObject("Scripting.Dictionary")
		Set CreateDict = dDict( count + 1 )
	End Function
	
	' 创建Dictionary对象，但不会自动销毁
	Public Function SCD()
		set SCD = Server.CreateObject("Scripting.Dictionary")
	End Function

	
	'检测组件是否安装
	Public Function IsInstall(Byval s)
		On Error Resume Next : Err.Clear()
		IsInstall = False
		Dim obj : Set obj = Server.CreateObject(s)
		If Err.Number = 0 Then IsInstall = True
		Set obj = Nothing : Err.Clear()
	End Function
	
	'返回系统数据库的model对象，使用了M_创建对象
	'set rs = POP_MVC.sysModel(POPASP系统自带数据库的表名)
	Public Property Get sysModel( tableName )
		set sysModel = M_( Array( tableName , "access" , Me.mvc_dir + "Module/popasp.mdb" ) )
	End Property
	
	'返回系统数据库的M_或B_中的参数，结果为数组
	'set rs = B_( POP_MVC.systemModel(POPASP系统自带数据库的表名) ).select
	Public Property Get systemModel( arg )
		systemModel = Array( arg , "access" , Me.mvc_dir + "Module/popasp.mdb" )
	End Property
	
	'创建对象，并返回
	'如果创建失败，系统强制退出
	Public Function SCO( arg )
		on error resume next
		dim objName,bool
		if isArray( arg ) then
			objName = arg(0)
			bool = false
		else
			objName = arg
			bool = true
		end if
		
		dim key : key = LCase( objName )
		
		if dObj.Exists(key) then
			dObj(key) = dObj(key) + 1
		else
			dObj(key) = 1
		end if
		set SCO = Server.CreateObject(objName)
		if bool then
			if err.number = -2147221005 then
				Me.Exit("不支持" + objName + "创建对象，程序无法继续运行：" + err.description )
			end if
		end if
	End Function
	
	'使用POP_MVC.SCO创建Adodb.Stream对象
	Public function CreateStream
		on error resume next
		set CreateStream = Me.SCO("Adodb.Stream")
	end function
	
	'返回网站根路径
	function get_site_path()
		dim script_name,pos,arr_,i
		'设置模板替换变量
		script_name = Me.trim(Request.ServerVariables("SCRIPT_NAME") , "/")
		arr_ = split( script_name , "/" )
 
		if ubound( arr_ ) > 0 then
			redim preserve arr_( ubound(arr_) - 1)
			get_site_path = join( arr_ , "/" )
		end if
		
		if get_site_path <> "" then
			get_site_path = "/" + get_site_path
		end if
	end function
	
	'返回网站根路径
	private function get_root_path()
		dim script_name,pos
		'设置模板替换变量
		script_name = Request.ServerVariables("SCRIPT_NAME")		
		pos = instrrev(script_name,"/")
		if pos > 0 then
			get_root_path = left(script_name,pos-1)
		else
			get_root_path = ""
		end if
	end function
	
	'返回框架目录
	private function get_mvc_dir()
		dim file,contents,matches
		file = Request.ServerVariables("SCRIPT_NAME")
		contents = file_get_contents(file)
		reg.Global = false : reg.IgnoreCase = true : reg.MultiLine = false
		reg.Pattern = "<!--#include\s+file\s*=\s*\W(.*)popasp.asp\W\s*-->"
		
		if reg.test(contents) Then
			set matches = reg.execute( contents )
			get_mvc_dir = matches(0).subMatches(0)
			page_trace_include = matches(0)
		else
			reg.Pattern = "<!--#include\s+virtual\s*=\s*\W(.*)popasp.asp\W\s*-->"
			if reg.test(contents) Then
				set matches = reg.execute( contents )
				get_mvc_dir = matches(0).subMatches(0)
				get_mvc_dir = replace( get_mvc_dir,"/","\" )
				if left( get_mvc_dir,1 ) <> "\" then
					get_mvc_dir = "\" + get_mvc_dir 
				end if
				page_trace_include = matches(0)
			else
				call [Exit]("找不到popasp目录")
			end if
		End if
		set matches = nothing
	End Function
	
	'将Request.QueryString的参数返回Dictionary对象
	'使用方法为 Execute POP_MVC.extract_get
	Public Property Get extract_get()
		on error resume next
		dim key,str
		str = ""
		for each key in Request.QueryString
			if Me.String.reg_test( key,"[a-z]\w*","i" ) then
				str = str + "Dim " + key + vbCrLf
				str = str + key + " = " + chr(34) & getReqValue( key,Request.QueryString(key) ) & chr(34) + vbCrLf
			else
				str = str + "Dim " + "[" + key + "]" + vbCrLf
				str = str + "[" + key + "]" + " = " + chr(34) & getReqValue( key,Request.QueryString(key) ) & chr(34) + vbCrLf
			end if			
		next
		extract_get = str
		Me.notice("")
	end Property
	
	
	'将Request.Form的参数返回Dictionary对象
	'使用方法为Execute POP_MVC.extrac_form
	Public Property Get extract_form()
		on error resume next
		dim key,str
		str = ""
		for each key in Request.Form
			if Me.String.reg_test( key,"[a-z]\w*","i" ) then
				str = str + "Dim " + key + vbCrLf
				str = str + key + " = " + chr(34) & Me.Form(key) & chr(34) + vbCrLf
			else
				str = str + "Dim " + "[" + key + "]" + vbCrLf
				str = str + "[" + key + "]" + " = " + chr(34) & Me.Form(key) & chr(34) + vbCrLf
			end if
		next
		extract_form = str
		Me.notice("")
	end Property	
	
	'POPASP框架内部方法，不对外使用
	'获取runtime/model下的model名称
	Function getModelRunName( arg )
		dim tableName,db_type,db_name	'如果为文件型数据库db_name实为db_path，即路径
		dim str
		tableName = arg(0)
		db_type = arg(1)
		db_name = arg(2)
		
		
		str = db_name + "_" + tableName
		if db_type = "access" or db_type = "excel" or  db_type = "sqlite3" or db_type = "xml" then
			str = Replace( str, "." , "" )
			str = Replace( str, "/" , "" )
			str = Replace( str, "\" , "" )
			If Me.String.reg_test( str , "\W" , "" ) then
				getModelRunName = db_type + "_" + Me.String.reg_replace( Me.realPath(db_name) + "_" + LCase(tableName) , "" , "\W" , "g"  )
			Else
				getModelRunName = db_type + "_" + str
			End If
		else
			If Me.String.reg_test( str , "\W" , "" ) then
				getModelRunName = db_type + "_" + Me.String.reg_replace( Me.realPath(db_name) + "_" + LCase(tableName) , "" , "\W" , "g"  )
			Else
				getModelRunName = db_type + "_" + str
			End If
		end if
	End Function
	
	'获取 Table.asp 的路径
	'先找 /home/Model/access/db/Table.asp
	'再找 /home/Model/access/Table.asp
	'再找 /home/Model/db/Table.asp
	'最后找 /home/Model/Table.asp
	'找不到返回空字符串
	Function getModelPath( byval arg )
		on error resume next
		
		dim modelPath,db_type,db_path,ext,tableName,model_path
		tableName = arg(0)
		db_type = arg(1)
		db_path = arg(2)
		
		if Me.String.iEndsWith( tableName , ".asp" ) then
			getModelPath = tableName
			exit Function
		end if
		
		if db_type = "access" or db_type = "excel" or  db_type = "sqlite3" then
			db_path = Me.File.baseName( LCase( Me.config("DB_PATH") ) )
			ext = Me.File.extName( LCase( Me.config("DB_PATH") ) )
			db_path = mid( db_path,1,len(db_path) - len(ext) )
		end if
		
		if not isEmpty( Me.config("MODEL_SUFFIX") ) then
			tableName = UCFirst(LCase(tableName)) + Me.config("MODEL_SUFFIX") + ".asp"
		else
			tableName = UCFirst(LCase(tableName)) + ".asp"
		end if
		
		getModelPath = ""
		
		if Not isEmpty( Me.config( "MODEL_PATH" ) ) then
			model_path = Me.config( "MODEL_PATH" )
		else
			model_path = Me.appPath + "/Model/"
		end if		
		
		if db_type <> "" then
		
			modelPath = model_path + db_type + "/" + db_path + "/" + tableName '如./home/Model/excel/15251成绩/1学期.asp		
			
			if Me.file.isFile( modelPath ) then
				getModelPath = modelPath : exit Function
			end if
			
			modelPath = model_path + db_type  + "/" + tableName '如./home/Model/excel/1学期.asp

			if Me.file.isFile( modelPath ) then
				getModelPath = modelPath : exit Function
			end if
		end if
		
		if db_path <> "" then
		
			modelPath = model_path + db_path + "/" + tableName '如./home/Model/15251成绩/1学期.asp
			
			if Me.file.isFile( modelPath ) then
				getModelPath = modelPath : exit Function
			end if
		
		end if
		
		modelPath = model_path  + tableName '如./home/Model/1学期.asp

		if Me.file.isFile( modelPath ) then
			getModelPath = modelPath : exit Function
		end if		
	End Function
	
	'返回model的目标路径
	Function getModelDstPath( ByVal arg )	
		getModelDstPath = Me.appPath + "/Runtime/Class/" + getModelRunName( arg ) + "Model.class.asp"
	End Function
  
	'类的初始化
	Private Sub Class_Initialize
		on error resume next
		dim tempStr
		Version = "3.6.0"		
		spent_limit = 0
		applicationOn = False
		runtimeOn = False
	
		'Application.Contents.RemoveAll		
		set dDict = Server.CreateObject("Scripting.Dictionary")
		
		if err.number = -2147221005 then
			Response.write "不支持Scripting.Dictionary，无法使用POPASP!"
			Response.end
		end if
		
		'存储所有使用的POPASP类
		set dClass = Server.CreateObject("Scripting.Dictionary")
		'set dClass("POPASP_MVC") = Me
		
		set dObj  = CreateDict()
		set dAuto  = CreateDict()
		set dPlugin = CreateDict()
		set dModel = CreateDict()
		set tpl_vars = CreateDict()
		set dL_ = CreateDict()
		set dJs = CreateDict()
		
		num_db_write = 0
		num_db_query = 0
		num_recordset = 0
		
		set reg = new RegExp		
		
		root_path = get_root_path()	
		
		tempStr = "Scri"
		tempStr = tempStr + "pting.File"
		tempStr = tempStr + "Sys"
		tempStr = tempStr + "temObject"
		
		set fso = Me.SCO(tempStr)
		
		if err.number = -2147221005 then
			Me.exit( "不支持" + tempStr + "，无法使用POPASP!" )
		end if		
		
		set config = CreateDict()
		reg.Global = true : reg.IgnoreCase = true : reg.MultiLine = true
		reg.Pattern = ""

		isExecuteExtends = True
		
		set dSql = CreateDict()
		set dTpl = CreateDict()
		set dFlow = CreateDict()
		set dCMS = CreateDict()
		set dTime = CreateDict()
		set dMd5 = CreateDict()
		is_init_popasp = false
		
		isExit = true
	End Sub	
	
	'类的销毁
	Private Sub Class_Terminate
		on error resume next
		isExit = false
		dim key
		set fso = nothing
		set reg = nothing
		set config = nothing
		set dG_ = nothing
		set dL_ = nothing
		set dJs = nothing
		set Apt = nothing
		set Dict = nothing
		set File = nothing
		set Url = nothing
		set objReject = nothing
		set Uploader = nothing
		set objExtendsClass = nothing
		set dAction = nothing
		call quit
	End Sub
	
	'仅将字符串首字母改为大写，其他字符不处理
	Function UCFirst(ByVal str)
		UCFirst = Me.String.UCFirst(str)		
	End Function 	
	
	'采用递归，删除str左边出现的字符串find
	function [ltrim](str,find)
		if  left( str, len(find) ) = find Then
			[ltrim] = Me.[ltrim]( mid(str,len(find)+1) , find )
		else
			[ltrim] = str
		End If
	End Function
	
	'采用递归，删除str右边出现的字符串find
	function [rtrim](str,find)
		if  right( str, len(find) ) = find Then
			[rtrim] = Me.[rtrim]( left(str,len(str) - len(find) ) , find )
		else
			[rtrim] = str
		End If
	End Function
	
	'采用递归，删除str两端出现的字符串find
	function [trim](str,find)
		[trim] = Me.rtrim(Me.ltrim(str,find),find)
	End Function
	
	'对server.urlencode函数的解码
	function URLDecode(ByVal input)
		URLDecode =  Me.String.URLDecode(input)
	End function
	
	'自动生成上传文件名，并返回
	'yyyymmddhhiiss + 五位随机数
	'20190215183010 + 五位随机数	
	Function GetUploadName()
		Randomize
		dim r
		r = CInt(Rnd() * 10000)
		r = right("0000" + r,4)
		GetUploadName = FormatDate( now() , "yyyymmddhhiiss" ) + r
	End Function
	
	' 文件采用自动命名的方式，自动上传到"/Upload"文件夹下
	Public Property Get Upload(formName)
		err.clear		
		on error resume next
		dim File,SavePath
		if Not Me.config.Exists("UPLOAD_SAVE_PATH") then
			SavePath = "/Upload/" + FormatDate( now() , "yyyymmdd" ) + "/"	
		else
			SavePath = Me.config("UPLOAD_SAVE_PATH")
			SavePath = replace( SavePath, "\" , "/" )
			SavePath = Me.rtrim( SavePath, "/" ) + "/"
		end if		

		If isEmpty( Uploader ) Then			
			set Uploader = import("POPASP_MOLIBUPLOAD")
			
			Uploader.exe = Me.config("UPLOAD_ALLOW_TYPES")
			Uploader.TotalSize = Me.config("UPLOAD_MAX_FILESIZE")
			Uploader.MaxSize = Me.config("UPLOAD_MAX_SIZE")			
			Uploader.GetData()			
			if Uploader.errorID > 0 then 
				Exit Property
			end if
		End If			

		Me.CreateFolder( SavePath )	
		
		Set File = Uploader.files( formName )	
		
		if file is nothing then
			Uploader.ErrorID = 6
			Exit Property
		end if
		
		Uploader.SrcName = file.filename
		
		call file.saveToFile( Me.realPath(savePath), 0, true)
		
		upload = SavePath + file.filename
		
		set file = nothing
	
		if err.number <> 0 then
			Uploader.description = L_("")
		end if
	End Property
	
	
	'传入图片路径，自动添加水印
	'水印采用文本或图片、水印位置可配置
	Public Property Get WaterMark(ByVal imgPath)		
		dim sAllowMarkExt:sAllowMarkExt = ".jpg,.png,.gif,.jpeg,.bmp"
		dim errPrefix,fileExt,arr
		
		fileExt = Mid(imgPath, InStrRev(imgPath, ".")+1, Len(imgPath))
		
		errPrefix = "在使用POP_MVC.WaterMark对图片" + imgPath + "进行水印处理时，" 		
		
		'图片不存在
		if Not Me.file.isFile( imgPath ) then
			Me.error( errPrefix + "发现图片不存在！" )
			Exit Property
		end if
		
		'图片格式不正确
		If InStr(sAllowMarkExt, Mid(imgPath, InStrRev(imgPath, "."), Len(imgPath))) = 0 Then
			Me.error( errPrefix + "发现图片格式不正确！" )
			Exit Property
		End If
		
		'未安装Persits.Jpeg
		If Not IsInstall("Persits.Jpeg") Then 
			Me.error( errPrefix + "发现未安装Persits.Jpeg！" )
			Exit Property
		end if
		
		If Me.config("WATERMARK_PATH") = "" Then 
			Call Me.Warning( errPrefix + "未指定水印图片或水印文字" )
			Exit Property
		end if

		dim jpegObj : set jpegObj = Server.CreateObject("Persits.Jpeg")	
		
		'打开背景图片
		jpegObj.Open Me.realPath(imgPath)
		
		on error resume next
		
		if err.number <> 0 then
			Me.error(  errPrefix + err.description  )
			Exit Property
		end if
		
		' Image cannot be wider than X pixels (for this demo only.) Resize if necessary
		If jpegObj.OriginalWidth > Me.config("WATERMARK_WIDTH") Then
			jpegObj.PreserveAspectRatio = True ' Height will be calculated automatically
			jpegObj.Width =  Me.config("WATERMARK_WIDTH")
		End If
		
		' Image cannot be heighter than X pixels (for this demo only.) Resize if necessary
		If jpegObj.Height > Me.config("WATERMARK_HEIGHT") Then
			jpegObj.PreserveAspectRatio = True ' Width will be calculated automatically
			jpegObj.Height =  Me.config("WATERMARK_HEIGHT")
		End If
		
		jpegObj.ToRGB ' to handle non-RGB images without an error
		
		if Me.file.isFile(  Me.config("WATERMARK_PATH") ) then  '为图片加入图片水印功能
			dim Logobox,ImageWidth,ImageHeight,ImageMode			
			Set Logobox = Server.CreateObject("Persits.Jpeg")	
			Logobox.Open Me.realPath( Me.config("WATERMARK_PATH")  )
			 
			ImageWidth		= Logobox.Width 
			ImageHeight		= Logobox.Height
			
			If jpegObj.OriginalWidth < Cint(ImageWidth) or jpegObj.Originalheight<Cint(ImageHeight) Then
				Me.notice( errPrefix + "图片宽/高度小于水印宽/高度，故不做水印处理" )
				Set jpegObj = Nothing
				set Logobox = Nothing
				Exit Property
			end if
			
			ImageMode = "jpg"
			IF ImageMode<>"" and FileExt<>"gif" Then 			
				jpegObj.Canvas.Pen.Width  = 1   
				jpegObj.Canvas.Brush.Solid = False  
				'jpegObj.DrawImage 5 , jpegObj.height-markPicHeight-5, Logobox, markPicAlpha
				arr = WaterMarkPosition( jpegObj.width,jpegObj.height,Logobox.Width,Logobox.Height,Me.config("WATERMARK_POSITION") )
				jpegObj.DrawImage arr(0) , arr(1), Logobox, 100
				jpegObj.Canvas.Bar 0, 0, jpegObj.Width, jpegObj.Height
				jpegObj.Save Me.realPath(imgPath)  
			end if
			'jpegObj.Sharpen 1, 120			
			Set Logobox=Nothing				
		else  ''文字水印处理
			dim strWidth,strHeight,waterStr
			waterStr	= Me.config("WATERMARK_PATH")
			strHeight	= Me.config("WATERMARK_HEIGHT") - 0
			strWidth	= len( waterStr )*strHeight * 0.85
			
			if jpegObj.width < strWidth OR jpegObj.height<strHeight then 
				Me.notice( errPrefix + "图片宽/高度小于水印宽/高度，故不做水印处理" )
				Exit Property
			end if
			
			'为图片加入文字水印功能
			jpegObj.Canvas.Font.Color = &HFF0000 ' 颜色,这里是设置成:黑 
			jpegObj.Canvas.Font.Family = "方正隶变简体"  ' 设置字体 
			jpegObj.Canvas.Font.Bold = False '是否设置成粗体 
			jpegObj.Canvas.Font.Size = strHeight '字体大小 
			jpegObj.Canvas.Font.Quality = 4 ' 文字清晰度
			jpegObj.Canvas.Font.ShadowColor = &H00FF00 '阴影色彩 
			jpegObj.Canvas.Font.ShadowYOffset = 1 
			jpegObj.Canvas.Font.ShadowXOffset = 1 
			jpegObj.Canvas.Brush.Solid = TRUE 
			arr = WaterMarkPosition( jpegObj.width,jpegObj.height,strWidth,strHeight,Me.config("WATERMARK_POSITION") )
			jpegObj.Canvas.Print arr(0) , arr(1) , waterStr
			jpegObj.Save Server.MapPath(imgPath)    ' 保存文件
		end if
		set jpegObj=Nothing
	End Property
	
	
	' 根据参数返回水印坐标位置的数组
	Public Function WaterMarkPosition(ByVal source_w,ByVal source_h,ByVal width,ByVal height,ByVal pos)
		Dim t_Position(2)		
		select case pos '水印位置  
		 case 1 '顶部居左
		  t_Position(0) = 10
		  t_Position(1) = 10
		 case 2 '顶部居中   
		  t_Position(0) = (source_w - width) / 2
		  t_Position(1) = 10
		 case 3    '顶部居右   
		  t_Position(0) = source_w - width - 10
		  t_Position(1) = 10
		 case 4    '中部居左   
		  t_Position(0) = 10
		  t_Position(1) = (source_h - height) / 2 
		 case 5    '中心位置   
		  t_Position(0) = (source_w - width) / 2  
		  t_Position(1) = (source_h - height) / 2  
		 case 6    '中部居右   
		  t_Position(0) = (source_w - width) / 2 -10  
		  t_Position(1) = (source_h - height) / 2  
		 case 7    '底部居左   
		  t_Position(0) = 10
		  t_Position(1) = source_h - height  - 10
		 case 8    '底部居中   
		  t_Position(0) = (source_w - width) / 2  
		  t_Position(1) = source_h - height  - 10
		 case 9    '底部居右 
		  t_Position(0) = source_w - width - 10
		  t_Position(1) = source_h - height  - 10
		 case else   '随机位置   
		  Randomize
		  t_Position(0) = Int(source_w - width + 1) * Rnd
		  Randomize
		  t_Position(1) = Int(source_h - height + 1) * Rnd 
		end select
		WaterMarkPosition = t_Position		
	End Function
	
	'判断当前请求是否采用get方式
	Function isGet()
		isGet = ( "get" = LCase( Request.ServerVariables("REQUEST_METHOD") ) )
	End Function
	
	'判断当前请求是否采用post方式
	Function isPost
		isPost = ( "post" = LCase( Request.ServerVariables("REQUEST_METHOD") ) )
	End Function
	
	'提供服务器文件的下载
	'参数Array(文件路径，文件名)
	Public Property Get Download( byval arg )
		dim Ado,i,r,path,filename
		Set Ado = Me.CreateStream 'Ado对象
		i = 0    '计数器
		r = 1024 '每次读取大小(byte)
		if IsArray( arg ) then
			path = arg(0)
			if ubound( arg ) > 0 then
				filename = arg(1)
			end if
		else
			path = arg '文件路径			
		end if
		
		path = Me.realPath( path )
		if isEmpty( filename ) then
			filename = Me.fso.GetFile(path).name
		end if
		
		Ado.Mode = 3 '1 读，2 写，3 读写。
		Ado.Type = 1 '1 二进制，2 文本。
		Ado.Open
		Ado.LoadFromFile(path) '载入文件
		session.CodePage = 936  '设置为gb2312编码输出
		Response.AddHeader "Content-Disposition", "attachment; filename=" + filename '文件名
		Response.AddHeader "Content-Length", Ado.size '通知浏览器接收的文件大小
		Response.ContentType = "application/octet-stream" '通知浏览器接受的文件类型(可自己定义,很多种,但一般都用这个.)
		While i < Ado.Size '循环读取直到读完为止
			Response.BinaryWrite Ado.Read(r) '输出二进制数据流
			Response.Flush '立即发送(要求至少256字节)，不加的话可能提示超过缓存区。
			i = i + r '累加计数器
		Wend
		Download = iif( (err.number=0) ,true,false )
		Ado.Close '关闭文件对象
		Response.End
	End Property
	
	'判断是否是POPASP_ 开头的类名
	Function isPopasp( className )
		If TypeName( className ) = "String" then
			isPopasp = ( UCase(Left( className , 7)) = "POPASP_" )
		End If
	End Function
	
	'判断是否是Action结尾的类名
	Function isAction( classname )
		If TypeName( className ) = "String" then
			isAction = ( UCase(Right( className , 6)) = "ACTION" )
		End If
	End Function
	
	'判断是否是Model结尾的类名
	Function isModel( className )
		isModel = ( UCase(Right( className , 5)) = "MODEL" AND UCase(className) <> "POPASP_MODEL" )
	End Function
	
	'将 Request.QueryString 中的数据转化成dict对象
	'不同于Form2Dict，键名全是小写
	Function get2Dict()
		dim key
		set get2Dict = Me.dict.Create()
		if Me.config("URL_MODE") = 0 then
			for each key in Request.QueryString
				if isArray( Request.QueryString(key) ) then
					Me.dict.edit get2Dict,LCase(key),Request.QueryString(key)
				else
					Me.dict.edit get2Dict,LCase(key),getReqValue( key,Request.QueryString(key) )
				end if
			next
		end if
	End Function
	
	'将 Request.Form中的数据转化成dict对象
	Function Form2Dict()
		dim key
		set Form2Dict = Me.dict.Create()
		for each key in Request.Form			
			if isArray( Request.Form(key) ) then
				Me.dict.edit Form2Dict,key,Request.Form(key)
			else
				Me.dict.edit Form2Dict,key,getReqValue( key,Request.Form(key) )
			end if
		next
	End Function
	
	'将 Request.Form中的数据转化成dict对象
	'去掉后缀
	Function Form2Dict4suffix( byVal suffix )
		dim key,k,m
		set Form2Dict4suffix = Me.dict.Create()
		m = len(suffix)
		for each key in Request.Form			
			if isArray( Request.Form(key) ) then
				Me.dict.edit Form2Dict4suffix,key,Request.Form(key)
			else
				k = key
				if Me.String.iEndsWith( k , suffix ) then
					k = left( k, len(key) - m )
				end if
				Me.dict.edit Form2Dict4suffix,k, getReqValue( key,Request.Form(key) )
			end if
		next
	End Function
	
	
	'取得包装后的req传值
	Function getReqValue( ByVal key, ByVal value )	
		getReqValue = value
		key = "," + LCase(key) + ","
		if isNumeric( value ) then
			exit function
		end if
		
		if Config("REQ_FILTER_JS") <> "" then
			if Config("REQ_IGNORE_JS") <> "" then
				if inStr("," + LCase( Config("REQ_IGNORE_JS") ) + "," , key) < 1 then
					getReqValue = Me.String.strip4script( getReqValue )
				end if
			else
				getReqValue = Me.String.strip4script( getReqValue )
			end if			
		end if
		
		if Config("REQ_FILTER_IFRAME") <> "" then
			if Config("REQ_IGNORE_IFRAME") <> "" then
				if inStr("," + LCase( Config("REQ_IGNORE_IFRAME") ) + "," , key) < 1 then
					getReqValue = Me.String.strip4iframe( getReqValue )
				end if
			else
				getReqValue = Me.String.strip4iframe( getReqValue )
			end if			
		end if
		
		if Config("REQ_FILTER_HTML") <> "" then
			if Config("REQ_IGNORE_HTML") <> "" then
				if inStr("," + LCase( Config("REQ_IGNORE_HTML") ) + "," , key) < 1 then
					getReqValue = Me.String.strip4html( getReqValue )
				end if
			else
				getReqValue = Me.String.strip4html( getReqValue )
			end if			
		end if
		
		if Config("REQ_FILTER_ASP") <> "" then
			if Config("REQ_IGNORE_ASP") <> "" then
				if inStr("," + LCase( Config("REQ_IGNORE_ASP") ) + "," , key) < 1 then
					getReqValue = Me.String.strip4asp( getReqValue )
				end if
			else
				getReqValue = Me.String.strip4asp( getReqValue )
			end if
		end if
	End Function
	
	
	' 得到控制器名称
	Public Function get_ctrl_name(url)
		dim arr : arr = split(url,"/")
		if ubound(arr) < 0 then
			get_ctrl_name = Me.ucfirst( LCase( Me.config("DEFAULT_MODULE") ) )
		else
			get_ctrl_name = Me.ucfirst( LCase(arr(0)) )
		end if
	End Function
  
	' 得到操作名称
	Public Function get_action_name(url)
		dim arr : arr = split(url,"/")		
		if ubound(arr) < 1 then
			get_action_name = Me.File.basename( Array(Request.ServerVariables("SCRIPT_NAME") , 1 ) )
		else
			get_action_name = Me.ucfirst( LCase(arr(1)) )
		end if
	End Function
	
	'采用多文件入口时的必须方法
	'简化写法为Call A_("控制器名/方法名")
	'或者Call A_("方法名")，此时采用默认控制器，一般为Index
	Sub Action( byval arg )
		Me.dG_( "actionStartTime" ) = timer()
		dim c_,a_,key,url
		
		if isArray(arg) then
			Me.config("EXEC_PARENT_SAME_ACTION") = not not is_empty( Me.config("EXEC_PARENT_SAME_ACTION") )
			url = arg(0)
		else
			url = arg
		end if
		
		c_ = get_ctrl_name(url)
		a_ = get_action_name(url)
		
		if isEmpty( c ) then 
			c = c_
			Execute( "const CTRL__ = """ + APP__ + "?" + Me.config("VAR_MODULE") + "=" + c + """" )
		end if
		
		if isEmpty( a ) then
			a = a_
			Execute("const ACT__ = """ + CTRL__ + "&" + Me.config("VAR_ACTION") + "=" + a + """")
			Execute("const URL__ = """ + CTRL__ + "&" + Me.config("VAR_ACTION") + "=" + """")
		end if

		if isEmpty(dAction) then
			set dAction = Me.CreateDict()
		end if
		
		key = LCase(c_ + "/" + a_)
		
		if not dAction.Exists( key ) then
			if Me.Dict.Search(dAction,c_) <> "" then
				isExecuteExtends = false
			else
				isExecuteExtends = true
			end if		
			method_exists = false
			temp_ctrl = c_
			action_count = 0
			
			call Action__(c_,a_)
			action_count = 0

			isExecuteExtends = False
			dAction( key ) = c_
		else
			Call F_( UCFirst( LCase(c_) ) + "Action." + a_ + "已经执行过了，只能执行一次" )
		end if
		if isEmpty( Me.dG_( "viewStartTime" ) ) then
			Me.dG_( "viewStartTime" ) = timer()
		end if
	End Sub
	
	'生成model对象
	'M_的原生方法，一般不直接使用
	'set mUser = POP_MVC.Model("user")
	'set mUser = M_("user")
	Function Model(ByRef tableName)	
		set Model = import("POPASP_MODEL").Create(tableName)
	End Function
	
	'K_函数的原生方法，一般不直接使用
	Function MethodModel(ByRef modelName)	
		set MethodModel = import("POPASP_MODEL").CreateMethodModel(modelName)
	End Function
	
	'B_函数的原生方法，一般不直接使用
	Function Db(ByRef tableName)	
		set Db = import("POPASP_MODEL").CreateDb(tableName)
	End Function
	
	'获取mvc_dir路径，可直接使用POP_MVC.mvc_dir
	Function getMvcDir()
		getMvcDir = mvc_dir
	End Function
	
	'清空Application.Contents
	Sub apt_clear
		Application.Contents.RemoveAll
	End Sub
  
	'设置配置参数
	Public Property Set setConfig( byval key, byref val )
		dict.edit config,key,val
	End Property
  
	'返回键名key的配置参数
	Public Property Get getConfig(byval key)
		dim arr,i,str
		if isNull(key) Then
			set getConfig = config		
		Else 			
			if inStr( key , "." ) > 0 Then				
				arr = split(key,".")
				str = "POP_MVC.config"				
				for i = 0 to ubound(arr)
					str = str + "(""" + arr(i) + """)"
				next
				Execute "getConfig = " + str			
			elseif config.Exists( key ) Then				
				If isObject( config(key) ) Then				
					set getConfig = config( key )
				else
					getConfig = Me.config(key)
				End if
			Else
				getConfig = Empty
			End If			
		End if    
	End Property
	
	'注销变量
	'如POP_MVC.Unset(rs)
	Public Property Get Unset( ByRef obj )
		dim temp
		if isObject( obj ) Then
			if TypeName(obj) = "Recordset" Then
				obj.close
			End If
			if Me.String.iStartsWith(TypeName( obj ) , "popasp_" ) then
				temp = TypeName( obj )
				set dClass( temp ) = nothing
				dClass.remove( temp )
			else
				set obj = nothing
			end if
		elseif TypeName(obj) = "String" Then
			'
		End if
	End Property
	
	Public Function FetchSqls
		dim ret,k
		for each k in dSql
			Me.Arr.push ret,dSql(k)("sql")
		next
		FetchSqls = ret
	End Function
	
	Public Function FetchMemRS( ByVal sql )
		on error resume next
		dim rs,k
		for each k in dSql
			if dSql(k)("sql") = sql then
				if typename( dSql(k)("rs") ) = "Recordset"  then
					if rs.State = 1 then
						set rs = dSql(k)("rs")
						if rs.recordCount > 0 then
							rs.moveFirst
						end if
						set FetchMemRS = rs
						Me.notice( sql + ";--的结果集重复使用1次" )
						exit Function
					end if
				end if				
			end if
		next
		set FetchMemRS = nothing
		Call L_( "" )
	End Function
	
	Public Property Get ClearSqlRS( ByRef keyword )
		dim key,bool
		for each key in dSql
			bool = false
			if keyword = "" or keyword = "*" then
				bool = true
			else
				if Me.String.iExists( dSql(key)("sql") , keyword ) then
					bool = true
				end if
			end if
			
			if bool then
				dSql(key)("rs").close
				set dSql(key)("rs") = nothing
				dsql(key).remove("rs")
			end if
		next
	End Property
	
	
	'给一个关键词，释放popasp中的类
	Public Property Get removePopaspClass( ByRef keyword )
		dim key
		for each key in dClass
			if Me.String.iExists( key, keyword ) then
				set dClass( key ) = nothing
				dClass.remove( key )
			end if
		next
	End Property
	
	'是否为标量
	'数值 字符串 布尔值为标量
	Function isScalar( byref opt )
		dim label : label = TypeName(opt)
		if isEmpty(opt) OR isNull(opt) OR isDate(opt) OR IsNumeric(opt) then
			isScalar = true
		else		
			isScalar = (label = "String" OR label = "Integer" OR label = "Long" OR label = "Double" OR label = "Boolean")
		end if
	End Function
  
	'返回控制器文件的路径
	'二次开发时有可能使用到，仅开发项目用不到
	Function get_ctrl_path( byVal arg )
		dim ctrl_name , ExtendName , trueCtrl
		
		if arg = Me.config("SYSTEM_MODULE") Then
			get_ctrl_path = mvc_dir + "Tpl/" + arg + "Action.class.asp"		
		else
			ctrl_name = arg
			
			dim ctrlFile
			if not isEmpty( Me.config("CTRL_SUFFIX") ) then
				ctrlFile = ctrl_name + Me.config("CTRL_SUFFIX") + ".asp"
			else
				ctrlFile = ctrl_name + ".asp"
			end if
			'CURRENT_CTRL_PATH 当前控制器的路径，常用于插件
			if not isEmpty( Me.config("CURRENT_CTRL_PATH") ) then				
				get_ctrl_path = Me.config("CURRENT_CTRL_PATH") + ctrlFile	
			else			
				Call calExtend( arg , trueCtrl , ExtendName  )
				if ExtendName <> "" then
					get_ctrl_path = rtrim(Me.config("EXTEND_PATH") , "/" ) + "/" + ExtendName + "/" + trueCtrl + ".class.asp"
				else
					'get_ctrl_path = Me.appPath & "/Controller/" & ctrl_name & "Action.class.asp"
					get_ctrl_path = Me.appPath + "/Controller/" + ctrlFile
				end if
			end if
		end if
	End Function

	'将input变量，输出到当前文件夹下的__ajax__.txt中
	Public Property Get ajax( byval input )
		dim fileName 
		fileName = "/__ajax__.txt"
		if Not Me.fso.FileExists( Me.realPath( fileName ) ) Then
			Call Me.file_put_contents( fileName,""  )
		End If
		Call file_var_export( filename, input )
	End Property
  
	'控制器文件不存在时的处理
	Private Sub ctrl_file_not_exists_handler
		dim c,a
		c = Me.config("EMPTY_MODULE")
		a = Me.config("DEFAULT_ACTION")
		url.setConfig "ctrlName",c
		url.setConfig "actionName",a
		if Me.File.isFile( get_ctrl_path( c ) ) Then
			Call Action_(c,a)
		else
			Me.exit( "没有找到控制器 " + c + " 对应的类文件 " )
		End If
	End Sub
  
	'控制器的类在文件中找不到到的处理
	Private Sub ctrl_not_exists_handler
		call [Exit]("在控制器文件中并未定义相应的控制器")
	End Sub
  
	'方法在控制器中找不到的处理
	Private Sub method_not_exists_handler
		Select case Err.number
			case "438"
				echo "未定义的方法"
			case else
				echo Err.number + Err.Source + Err.Description 
		end Select
	End Sub
  
	'控制器代码写错时的处理
	Private Sub ctrl_code_error_handler
		echo Err.number + Err.Source + Err.Description 
	End Sub
  
	'输出
	Private Sub echo( str )
		Response.write str + "<br />"
	End Sub
	
	'向文件尾部追加内容
	Function file_append_contents( ByRef filePath , ByRef append_contents)
		dim content
		content = Me.file_get_contents( filePath ) + append_contents
		file_append_contents = file_put_contents( filePath,content )	
	End Function
	
	'向文件尾部追加内容
	Function file_append_contents_without_bom( ByRef filePath , ByRef append_contents)
		dim content
		content = Me.file_get_contents( filePath ) + append_contents
		file_append_contents_without_bom = file_put_contents_without_bom( filePath,content )	
	End Function
	
	'检测文件编码类型
	'只能检测带头的utf-8与unicode两种
	function detect_encoding(ByVal path) 
		dim objstream,bintou
		set objstream=Me.CreateStream
		objstream.Type=1 
		objstream.mode=3 
		objstream.open 
		objstream.Position=0 
		objstream.loadfromfile realPath(path) 
		bintou=objstream.read(2) 
		
		'带头的utf-8编码
		If AscB(MidB(bintou,1,1))=&HEF And AscB(MidB(bintou,2,1))=&HBB Then
			detect_encoding="utf-8"
		elseIf AscB(MidB(bintou,1,1))=&HFF And AscB(MidB(bintou,2,1))=&HFE Then 
			detect_encoding="unicode" 
		End If 
		objstream.close 
		set objstream=nothing 
	end function
  
	'获取文本文件内容
	Function file_get_contents( ByVal filePath )
		file_get_contents = file_get_contents_charset( filePath, "UTF-8" )
	End Function
	

	
	'获取文本文件内容
	Function file_get_contents_charset( ByVal filePath , s_charset )
		on error resume next
		dim startTime : startTime = timer()
		Dim o_strm, tmpStr,p

		filePath = Replace( filePath, "/" , "\" )
		p = realPath(filePath)
		  Set o_strm = Me.CreateStream
		  With o_strm
			.Type = 2
			.Mode = 3
			.Open
			.LoadFromFile p
			.Charset = s_charset
			.Position = 2
			tmpStr = .ReadText
			.Close
		  End With
		  
		  Set o_strm = Nothing
		  if s_charset = "utf-8" then
			tmpStr = Replace(tmpStr, "^\uFEFF", "" , 1,1,0)
			end if
		file_get_contents_charset = tmpStr
		call pushTime( startTime , "read file " + filePath )
		call L_("读取文件 " + filePath + " 失败")
	End Function	
	
	'获取二进制文件内容
	Function stream_get_contents( ByVal filePath )
		on error resume next
		dim startTime : startTime = timer()
		Dim o_strm, tmpStr,p
		
		filePath = Replace( filePath, "/" , "\" )
		p = realPath(filePath)		
		  Set o_strm = Me.CreateStream
		  With o_strm
			.Type = 1	'1 二进制，2 文本
			.Mode = 3	'1 读，2 写，3 读写
			.Open
			.LoadFromFile p
			.Position = 0
			tmpStr = .Read
			.Close
		  End With
		  Set o_strm = Nothing
		stream_get_contents = tmpStr
		call pushTime( startTime , "read file " + filePath )
		call L_("读取文件 " + filePath + " 失败")
	End Function
	
	Function file_put_contents_charset(ByVal filePath,content,s_charset)
		on error resume next
		dim oStream , startTime : startTime = timer()
		'content = Replace(content, "^\uFEFF", "" , 1,1,0)
		
		filePath = Replace( filePath, "/" , "\" )
		file_put_contents_charset = CreateFolder( Left(filePath,InstrRev(filePath,"\")-1) )

		if file_put_contents_charset Then
			Set oStream = Me.CreateStream
			With oStream
				.Type = 2	'以本模式读取
				.mode = 3
				.Open
				.Charset = s_charset
				.Position = 0

				.WriteText(content)
				.SaveToFile realPath(filePath),2

				.Flush
				.Close
			End With
			Set oStream=Nothing
			if err.number <> 0 then
				Call L_( "“" + filePath + "”文件写入失败" )
			else
				file_put_contents_charset = true
			end if				
		end if
		call pushTime( startTime , "write file " + filePath)
	End Function
	
	' 给文件写入内容，如果文件不存在，则尝试创建，文件为有BOM的utf-8编码
	Function file_put_contents(ByVal filePath,content)
		file_put_contents = file_put_contents_charset( filePath,content,"utf-8" )
	end Function
	
	' 给文件写入内容，如果文件不存在，则尝试创建，文件为元BOM的utf-8编码
	Function file_put_contents_without_bom(filePath,content)
		on error resume next
		dim startTime : startTime = timer()
		'content = Replace(content, "^\uFEFF", "" , 1,1,0)
		
		filePath = Replace( filePath, "/" , "\" )
		file_put_contents_without_bom = CreateFolder( Left(filePath,InstrRev(filePath,"\")-1) )

		if file_put_contents_without_bom Then
			dim stm:set stm= Me.CreateStream
			with stm
				.Type=2 '以文本模式读取  
				.mode=3  
				.charset="utf-8"  
				.open  
				.Writetext (content)  
				.Position = 3 
			end with
			dim newStream:Set newStream = Me.CreateStream
			With newStream
				.Mode = 3  
				.Type = 1  
				.Open
			End With  
			stm.CopyTo(newStream)  
			newStream.SaveToFile realPath(filePath),2  
			stm.flush  
			stm.Close  
			Set stm = Nothing  
			Set newStream = Nothing  
			if err.number <> 0 then
				Call L_( "“" + filePath + "”文件写入失败" )
			else
				file_put_contents_without_bom = true
			end if				
		end if
		call pushTime( startTime , "write file " + filePath)
	end Function
	
	' 将二进制内容写入文件
	' bytes必须是Byte()类型
	' 如果文件不存在，则尝试创建
	Function stream_put_contents(filePath,bytes)
		on error resume next
		dim oStream , startTime : startTime = timer()
		
		filePath = Replace( filePath, "/" , "\" )
		stream_put_contents = CreateFolder( Left(filePath,InstrRev(filePath,"\")-1) )

		if stream_put_contents Then
			Set oStream = Me.CreateStream
			With oStream
				.Type = 1	'以本模式读取
				.mode = 3
				.Open
				.Position = 0
				.Write(bytes)
				.SaveToFile realPath(filePath),1
				.Flush
				.Close
			End With
			Set oStream=Nothing

			stream_put_contents = iif((Err.Number = 0),True,False)		
		end if
		call pushTime( startTime , "write file " + filePath)
		call L_("写入文件 " + filePath + " 失败")
	end Function
	
	
	' 将base64内容写入文件
	' 如果文件不存在，则尝试创建
	Function base64_put_contents(filePath,byVal str)
		on error resume next
		dim xml ,stm, xmlstr ,startTime,arr_t
		startTime = timer()
		
		filePath = Replace( filePath, "/" , "\" )
		base64_put_contents = CreateFolder( Left(filePath,InstrRev(filePath,"\")-1) )
		
		if base64_put_contents then
			if len(str)>0 then
				if instr(str,";base64,")>0 then
					arr_t=split(str,";base64,")
					str=arr_t(1)
				end if
			end if
		
			xmlstr="<data>"&str&"</data>"
			Set xml=CreateObject("MSXML2" + ".DOMDocument")
			Set stm=CreateObject("ADODB"  + ".Stream")
			xml.resolveExternals=False
			xml.loadxml(xmlstr)
			xml.documentElement.setAttribute "xmlns:dt","urn:schemas-microsoft-com:datatypes"
			xml.documentElement.dataType = "bin.base64"
			stm.Type= 1'adTypeBinary
			stm.Open
			stm.Write xml.documentElement.nodeTypedValue
			stm.SaveToFile realpath(filePath)
			stm.Close
			Set xml=Nothing
			Set stm=Nothing
		end if
		
		call pushTime( startTime , "write file " + filePath)
		call L_("写入文件 " + filePath + " 失败")
	end Function
	
	'自动生成文件夹路径
	'如果有不存在的路径，则尝试创建
	Public Property Get CreateFolder( ByVal path )
		on error resume next
		Dim i , folder
		CreateFolder = True
		'//看看是否已经存在目录
		path = realPath(path)
		If fso.FolderExists(path) Then
			Exit Property
		Else
			path = Replace(path,"/","\")
			path = Split(path,"\")
			For i = 0 To Ubound(path)
				folder = folder + path(i) + "\"	
				If Not fso.folderExists(folder) Then
					fso.CreateFolder(folder)
				End If
			Next
			
			if Err.Number <> 0 then
				Me.Exit( "创建" + path +   "目录失败：" & Err.number & "," + Err.Description  )
			end if
			CreateFolder = True
		End If
	End Property
	
	'查找字符串的最后一次出现，返回字符串的一部分或者 Empty（如果未发现）	
	Function rstr( ByRef input ,ByRef find )
		Dim pos
		pos = inStrRev( input,find )
		if pos>0 then rstr = mid( input,pos ) 
	End Function
	
	'返回popasp的类的内容
	Function asp_get_popasp ( arg )
		dim str,start_,end_,modules,i,content,run_path,bool,bCompress,file
		
		bCompress = true
		if isArray( arg ) then
			file = arg(0)
			if ubound(arg) > 0 then bCompress = arg(1)
		else
			file = arg
		end if

		run_path =  mvc_dir + "Runtime" + Me.rstr( file,"/" )	
		bool = Me.fso.FileExists( Me.realPath(run_path) )
		if  bool then
			str = file_get_contents( run_path )
		else
			str = file_get_contents( file )
		end if
			
		start_	= InStr(str,"<" + "%")
		end_	= InStrRev( str,"%" + ">" )				
		str= mid(str,start_ + 2,end_-4) 				
		asp_get_popasp = str

		if bool then
			Exit Function
		end if
		
		if LCase(right(file,len( "popasp_string.class.asp" ))) = "popasp_string.class.asp" then
			if not Me.fso.FileExists(mvc_dir + "Runtime/popasp_mvc.class.asp") then
				Call asp_get_popasp( mvc_dir + "popasp_mvc.class.asp" )
			end if
			if not Me.fso.FileExists(mvc_dir + "Runtime/popasp.func.asp") then
				Call asp_get_popasp( mvc_dir + "popasp.func.asp" )
			end if
			if not Me.fso.FileExists(mvc_dir + "Runtime/popasp.convention.asp") then
				Call asp_get_popasp( mvc_dir + "popasp.convention.asp" )
			end if			
		end if
		
		if  LCase(right(file,len( "popasp_access.class.asp" ))) = "popasp_access.class.asp" OR LCase(right(file,len( "popasp_sqlite3.class.asp" ))) = "popasp_sqlite3.class.asp" OR LCase(right(file,len( "popasp_excel.class.asp" ))) = "popasp_excel.class.asp" OR LCase(right(file,len( "popasp_sqlserver.class.asp" ))) = "popasp_sqlserver.class.asp" OR LCase(right(file,len( "popasp_mysql.class.asp" ))) = "popasp_mysql.class.asp" OR LCase(right(file,len( "popasp_txt.class.asp" ))) = "popasp_txt.class.asp" Then
			asp_get_popasp = Replace( asp_get_popasp , "'POPASP_REPLACE_CLASS_CODE_POPASP'" , file_get_contents( mvc_dir + "Tpl/code_db.txt" ) )
			asp_get_popasp = Replace( asp_get_popasp , "'POPASP_REPLACE_CLASS_PARSEWHERE_POPASP'" , file_get_contents( mvc_dir + "Tpl/code_db_parsewhere.txt" ) )	
		end if

		if  LCase(right(file,len( "popasp_iaspcms.class.asp" ))) = "popasp_iaspcms.class.asp" OR LCase(right(file,len( "popasp_papcms.class.asp" ))) = "popasp_papcms.class.asp" Then
			asp_get_popasp = Replace( asp_get_popasp , "'POPASP_REPLACE_CLASS_CODE_POPASP'" , asp_get_contents( mvc_dir + "Tpl/code_cms.asp" ) )
		end if
		
		If Me.config("AUTO_EXTENDS_PATH") = "" Then
			Me.config("AUTO_EXTENDS_PATH") = Me.mvc_dir + "Module/"
		End If
		
		If LCase(right(file,len( "popasp_auto.class.asp" ))) = "popasp_auto.class.asp" then		

			If Me.config( "AUTO_EXTENDS_MODULE" ) <> "" Then							
				modules = split( Me.config( "AUTO_EXTENDS_MODULE" ) , "," )
				
				content = ""
				for i = 0 to ubound( modules )
					content = content + Me.file_get_contents( Me.config("AUTO_EXTENDS_PATH") + modules(i) + "/auto.txt" ) + VbCrLf
				next
				asp_get_popasp = Replace( asp_get_popasp , "End Class" , content + "End Class" )
			End If
			
			
		end if
		
		if bCompress then
			asp_get_popasp = Me.removeAspComments( asp_get_popasp )	
		end if
		
	
		'如果重生成类的压缩文件，请注释下面这行
		'Call file_put_contents_without_bom( run_path,"<" & "%" & asp_get_popasp & "%" & ">" )
		if LCase(right(file,len( "popasp.func.asp" ))) = "popasp.func.asp" then
			'Call file_append_contents_without_bom( mvc_dir & "Runtime/inc_list.asp"  , "<!--#include file=""" & mid(Me.rstr( file,"/" ),2) & """-->" & "<" & "% " & "Dim POP_MVC : Set POP_MVC = New POPASP_MVC4BBS : POP_MVC.runtimeOn = True" & " %" & ">" )
		else
			'Call file_append_contents_without_bom( mvc_dir & "Runtime/inc_list.asp"  , "<!--#include file=""" & mid(Me.rstr( file,"/" ),2) & """-->" )
		end if
	End Function
	
	'删除asp代码中的解析与空白行
	Function removeAspComments( str )
		Me.reg.IgnoreCase = true
		Me.reg.Global = true
		Me.reg.MultiLine = true
		Me.reg.Pattern = "[\r\n]\s*'[^\r\n]*"
		removeAspComments = Me.reg.Replace( str , "" )
		Me.reg.Pattern = "\s*'(?!extends\s+)[^""\r\n]*$"
		removeAspComments = Me.reg.Replace( removeAspComments , "" )
		Me.reg.Pattern = "[\r\n]\s*"
		removeAspComments = Me.reg.Replace( removeAspComments , VbCrLf )	
	End Function
	
	'根据编码获取内容
	Function asp_get_contents_charset ( file , s_charset )
		dim str,start_,end_,modules,i,content
		str		= file_get_contents_charset(file,s_charset)
		
		start_	= InStr(str,"<" + "%")
		end_	= InStrRev( str,"%" + ">" )
		
		str= mid(str,start_ + 2,end_-4) 
		
		asp_get_contents_charset = str
	End Function
	
	' 从asp文件中取得内容，并且删除第一个与最后一个出现在asp代码标记
	Function asp_get_contents ( file )
		asp_get_contents = asp_get_contents_charset( file,"utf-8" )
	End Function
	
	' 引入文件 popasp_application.class.asp
	Private Sub import_application		
		Dim apt_key
		if isEmpty( apt ) Then	'如果还没有加载application类
			if is_empty( Me.runtimeOn ) then
				if not is_empty( Me.applicationOn ) then
					' 取得键名，必须与 apt.getKey() 获取的相一致
					apt_key = Me.config("APPLICATION_PREFIX") + get_class_in_apt_key("POPASP_APPLICATION")
					
					' 如果没有存到 Application中，则先存至 Application中
					if isEmpty( Application.Contents(apt_key) ) Then
						Application.Contents(apt_key) = Me.asp_get_popasp( get_mvc_class_path("POPASP_APPLICATION") )
					End If
					Execute Application.Contents(apt_key)
				else 
					Execute  Me.asp_get_popasp( get_mvc_class_path("POPASP_APPLICATION") )
				End If
			
				set apt = new POPASP_APPLICATION
				set dClass("POPASP_APPLICATION") = apt
			else
				set apt = Me.import("POPASP_APPLICATION")
			end if
		End if		
	End Sub
	
	'显示控制台
	Public sub show_page_trace
		dim arr,tpl,content,url

		if Me.isAjax() then
			exit sub
		end if
		
		if NOT isEmpty(isPageCache) and not isEmpty( session("POPASP_PAGE_CACHE_URL") ) then 
			tpl = Me.mvc_dir + "Tpl/page_cache.html"
			content = Me.file_get_contents( tpl )
			content = replace( content,"__CACHE_URL__",isPageCache )
			Response.write( content )
		end if
		if isEmpty( Me.dG_( "viewEndTime" ) ) then
			Me.dG_( "viewEndTime" ) = timer()
		end if
		if is_empty( Me.config("SHOW_PAGE_TRACE") ) then
			'if not is_empty( Me.config("APP_DEBUG") ) and is_localhost() and not Me.isAjax() then
			'	Response.Write Chr(13) & Chr(10) & "<!-- POPASP MVC : http://www.popasp.com  |  Spent: " & G_( array("beginTime","viewEndTime") ) & " s " & "-->"
			'end if
			exit sub
		end if

		Call import("POPASP_PAGETRACE").show()	
	end sub
	
	'获取框架基类路径
	Private Function get_mvc_class_path(class_name)	
		get_mvc_class_path = mvc_dir + LCase(class_name) + ".class.asp"
		if left(class_name , 18 ) = "POPASP_SELF_OBJECT" then
			Exit Function
		end if
		if not Me.fso.FileExists( Me.realPath(get_mvc_class_path) ) then
			get_mvc_class_path = mvc_dir + "Plugin/" + LCase(class_name) + ".class.asp"
			if not Me.fso.FileExists( Me.realPath(get_mvc_class_path) ) then
				Me.exit(class_name + "类文件不存在!!")
			end if
		end if
	End Function
	
	Private Function get_class_in_apt_key( class_name )
		get_class_in_apt_key = Me.Version + "_" + LCase(class_name) + ".class.asp"
	End Function
  
	'实例化popasp基类
	'使用方法: POP_MVC.import("page")
	'一般使用P_简化方法: P_("page")
	Function import(class_name)
		on error resume next
		dim startTime,run_path
		
		'dim startTime : startTime = timer()
		'将每个生成的对象都保存到dClass中，如果dClass存在这个对象，则直接取出
		if dClass.Exists(class_name) Then
			set import = dClass(class_name)
			Exit Function
		end if
		
		if not is_empty(Me.runtimeOn) then	'如果使用压缩路径
			run_path =  mvc_dir + "Runtime/" + LCase(class_name) + ".class.asp"
			if Me.fso.FileExists( Me.realPath(run_path) ) then
				startTime = timer()
				Execute "Set dClass(""" + class_name + """) = New " + class_name
				set import = dClass(class_name)
				call pushTime( startTime , "New " + class_name )
				Exit Function
			end if
		end if

		'如果不存在，则第一次实例化该对象，达到单例模式的目的
		Execute get4asp(class_name)
		if err.number <> 0 Then
			if not is_empty(Me.config("APP_DEBUG")) Then
				call [Exit]( class_name + "类中代码有误:" & err.number & "," + err.description)
			end if
		end if
		startTime = timer()
		Execute "Set dClass(""" + class_name + """) = New " + class_name
		set import = dClass(class_name)
		call pushTime( startTime , "New " + class_name )
	End Function
	
	' 引入类文件或控制器文件，但是比import高级一些，带有键名，通过P_函数创建时，键名前总是加一个*号
	Function import_with_key(class_name,class_key)		
		on error resume next
		dim key,run_path
		dim startTime : startTime = timer()
		key = class_name + class_key
		'将每个生成的对象都保存到dClass中，如果dClass存在这个对象，则直接取出
		if dClass.Exists(key) Then
			set import_with_key = dClass(key) : exit Function
		end if

		if Me.runtimeOn then
			run_path =  mvc_dir + "Runtime/" + LCase(class_name) + ".class.asp"
			if Me.fso.FileExists( Me.realPath(run_path) ) then
				Execute "Set dClass(""" + key + """) = New " + class_name
				set import_with_key = dClass(key)
				Exit Function
			end if
		end if
		
		'如果不存在，则第一次实例化该对象，达到单例模式的目的
		IF isEmpty( dClassContent ) then
			set dClassContent = Me.CreateDict
		End If
		
		IF Not dClassContent.Exists( class_name ) then
			dClassContent( class_name ) = get4asp(class_name)
		end if
		execute dClassContent( class_name )
		if err.number <> 0 Then
			if not is_empty(C("APP_DEBUG")) Then
				call [Exit]( class_name + "类中代码有误:" & err.number & "," & err.description)
			end if
		end if

		Execute "Set dClass(""" + key + """) = New " + class_name

		set import_with_key = dClass(key)
		call pushTime( startTime , "New " + key )
	End Function
	
	' 引入插件类
	Function import_plugin(class_name)
		on error resume next		
		
		dim file
		'将每个生成的对象都保存到dClass中，如果dClass存在这个对象，则直接取出
		if dPlugin.Exists(class_name) Then
			set import_plugin = dPlugin(class_name) : exit Function
		end if
		
		'插件类文件
		file = Me.String.Rtrim(Me.config("PLUGIN_PATH"),"/") 
		file = Me.String.Rtrim(file,"\") 
		file = file + "/" + class_name + ".class.asp" '插件必须是类，且文件名格式必须是tool.class.asp	
		
		if NOt Me.fso.FileExists( Me.realPath( file ) ) then
			Me.Exit( "插件文件 " + file + " 不存在" )
		end if
		
		'如果不存在，则第一次实例化该对象，达到单例模式的目的
		Execute get4asp(file)		
		
		if err.number <> 0 Then
			if not is_empty(C("APP_DEBUG")) Then
				call [Exit]( class_name + "类中代码有误:" & err.number & "," & err.description)
			end if
		end if		
		
		Execute "Set dPlugin(""" + class_name + """) = New " + class_name

		set import_plugin = dPlugin(class_name)
	End Function
	
	'带提示退出程序
	Public Property Get [exit]( byref str )
		if not isEmpty(config) and not isEmpty( dClass ) then
			if not is_empty( Me.config("ERROR_LOG") ) then
				P_("POPASP_LOG").write( str )
			end if
		end if
		Response.write str
		call quit
	End Property
	
	'不带提示直接退出程序
	Public Property Get quit
		err.clear
		on error resume next
		dim key,key2	
	
		if isObject( dSql ) Then
			'自动关闭Recordset资源
			for each key in  dSql
				if dSql(key).exists("rs") Then
					 dSql(key)("rs").close : set  dSql(key)("rs") = nothing
				end if
			next		
		End if	
		
		if isObject( dModel ) Then
			for each key in dModel
				if isObject( dModel(key) ) then
					if dModel(key).Exists("db") then
						set dModel(key).db = nothing
					end if
				end if
			next
			set dModel = nothing
		end if
		
		if isObject( dClass ) then
			for each key in dClass
				if isObject( dClass(key) ) then
					if key = "POPASP_DATABASE_TOOL" Then
						'自动关闭Recordset资源
						for each key2 in  dSql
							if dSql(key2).exists("rs") Then
								 dSql(key2)("rs").close : set  dSql(key2)("rs") = nothing
							end if
						next
					end if					
					
					
					set dClass(key) = Nothing
				end if	
			next
			set dClass = Nothing
		end if
		
		if isObject( dPlugin ) then
			for each key in dPlugin
				if isObject( dPlugin(key) ) then
					set dPlugin(key) = Nothing
				end if	
			next
			set dPlugin = Nothing
		end if
		
		for each key in dDict
			set dDict(key) = nothing
		next
		set dDict = nothing
		
		if isExit then
			Response.end
		end if
	End Property
	
	' 获取引入asp文件的内容
	Function get4asp(class_name)		
		dim key
		if not is_empty( Me.applicationOn ) Then	'开启了 Application 缓存
			call import_application		
			if class_name <> "POPASP_APPLICATION" Then				
				if isPopasp(class_name) Then	'如果是框架的类
					key = get_class_in_apt_key(class_name)
					if Not apt.Exists( key ) Then					
						apt.set key,asp_get_popasp( get_mvc_class_path(class_name) )
					End If				
					get4asp = apt.get(key)		
				Else '不是框架的类，则应该提供路径
					key = realPath(class_name)
					if Not apt.Exists( key ) Then					
						apt.set key,asp_get_contents( class_name )
					End If				
					get4asp = apt.get(key)					
				End if	
			Else
				get4asp = apt.get( get_class_in_apt_key("POPASP_APPLICATION") )
			end If
		else	'没有开启 Application 缓存
			if isPopasp(class_name) then	'如果是框架的类
				get4asp = asp_get_popasp( get_mvc_class_path(class_name) )
			else	'不是框架的类，则应该提供路径
				get4asp = asp_get_contents( class_name )
			end if
		end if
	End Function
	
	'转换时间,可以随意处理时间格式，非常方便
	'比如2020-02-09 08:05:07
	'yyyy,yy,y(yyyy)年份，对应2020,20,2020
	'mm,m月份，对应02，2
	'dd,d日，对应09,9
	'hh,h小时，对应08,8
	'ii,i分钟，对应05,5
	'ss,s秒，对应07,7
	'wwww,www,ww,w为周几，分别对应:Sunday,Sun,日,1
	Function FormatDate(Byval t,Byval ftype)
		if isNul(t)	then t = now()
		If Not IsDate(t)  Then Exit Function
		dim yyyy,yy,y,mm,m,dd,d,hh,h,ii,i,ss,s,wwww,www,ww,w
		dim arr,vArr,j,weekArr,wArr,wcnArr
		weekArr = Array( "Sunday" , "Monday" , "Tuesday" , "Wednesday" , "Thursday" ,"Friday" , "Saturday" )
		wArr=Array( "Sun" , "Mon" , "Tue" , "Wed" , "Thur" , "Fri" , "Sat" )
		wcnArr=Array( "日" , "一" , "二", "三", "四", "五", "六" )
		yyyy=cstr(year(t)) : yy = right( yyyy,2 ) : y = yyyy
		m=cstr(month(t)) : mm = m
		If len(mm)=1 Then mm="0" & mm
		d=cstr(day(t)) : dd = d
		If len(dd)=1 Then dd="0" & dd
		h = cstr(hour(t)) : hh = h
		If len(hh)=1 Then hh="0" & hh
		i = cstr(minute(t)) : ii = i
		If len(ii)=1 Then ii="0" & ii
		s = cstr(second(t)) : ss = s
		If len(ss)=1 Then ss="0" & ss
		w = weekday( t , 0 ) -1
		wwww  = weekArr( w )
		www   = wArr( w )
		ww    = wcnArr( w )
		w	  = w + 1
		ftype = LCase(ftype)
		arr = array( "yyyy" , "yy" , "y" , "mm" , "m" , "dd" , "d" , "hh" , "h" , "ii" , "i" , "ss" , "s" , "wwww","www","ww","w" )
		vArr = Array(yyyy, yy, y, mm, m, dd, d, hh, h, ii, i, ss, s, wwww, www, ww, w)
		t = CStr(ftype)
		for j = 0 to ubound(arr)
			if InStr( t, arr(j) ) > 0 then
				t = replace( t , arr(j) , vArr(j) )
				'Execute( "t = replace( t, """ & arr(j) & """ , " & arr(j) & " )" )
			end if
		next
		FormatDate = t
	End Function
	
	'关闭POPASP_DATABASE_TOOL类
	Public Property Get CloseMvcDbClass
		dim i,keys
		keys = dClass.keys
		for i = 0 to ubound( keys )
			if Me.String.StartsWith( keys(i) , "POPASP_DATABASE_TOOL" ) then
				dClass( keys(i) ).conn.close
				set dClass( keys(i) ).conn = nothing						
				set dClass( keys(i) ) = nothing
				dClass.remove( keys(i) )
			end if
		next
	End Property
	
	'将日期转化成中国农历日期，也可以按FormateDate格式化
	'比如2020-02-22
	'n农历年份，对应 庚子
	'v农历月份，对应 正
	'r农历日，对应 廿九
	'z生肖，对应 鼠
	Function FormatCnDate(ByRef t,Byval ftype)		
		if isNul(t)	then t = now()
		If Not IsDate(t)  Then Exit Function
		
		dim n,v,r,z,temp,arr,vArr,j		
		
		call import("POPASP_CNDAY").Action(t,n,v,r,z)
		
		arr = array( "n" , "v" , "r" , "z" )
		vArr = Array(n, v, r, z)
		temp = t
				
		ftype = LCase(ftype)		
		t = CStr(ftype)
		for j = 0 to ubound(arr)
			if InStr( t, arr(j) ) > 0 then
				t = Replace(t, arr(j), vArr(j))
			end if
		next
		ftype = t
		t = temp

		FormatCnDate = FormatDate( t, ftype )
	End Function
	
	'一般消息类通知信息在拿到时间后，会更贴近发布该内容多久时间，比如：刚刚、十分钟前、两小时前、两天前等
	'如果超过一定的时间后，则显示发布的日期。对于用户来说，更加友好地显示时间。
	Function DateStr( ByVal t )
		dim days,minutes
		if not isDate(t) then
			DateStr = "" : Exit Function
		end if
		days = datediff( "d" , t , now() )
		if days > 7 then
			DateStr = FormatDate( t, "YYYY-MM-DD" ) : Exit Function
		elseif days > 2 then
			DateStr = days & "天前" : Exit Function
		elseif days = 2 then
			DateStr = "前天" : Exit Function
		elseif days = 1 then
			DateStr = "昨天" : Exit Function
		end if		
		minutes = datediff( "n" , t , now() )
		
		if minutes > 60 then
			DateStr = CInt(minutes/60) & "小时前" : Exit Function
		elseif  minutes > 10 then
			DateStr = minutes & "分钟前" : Exit Function
		end if
		DateStr = "刚刚"
	End Function

	'格式化时间
	Function FormatTime(ByRef t)
		 if DateDiff("n",t,now)<5 then
		  FormatTime="刚刚"
		 elseif DateDiff("n",t,now)<60 then
		  FormatTime=DateDiff("n",t,now) & " 分钟前"
		 elseif DateDiff("h",t,now)<5 Then
		  FormatTime=DateDiff("h",t,now) & " 小时前"
		 else
		  FormatTime=formatdatetime(t,2)
		 end if
	End Function
	
	'将Recordset类型转化为Dictionary
	'如果参数为数组，则Array( rs, 需要去掉的字段的前缀, 是否将字段名转为小写 )
	Public Function rs2dict(arg)		
		on error resume next
		'bLcase是否转为小写，默认为false
		dim rs,prefix,bLcase,bound,i,k,m,key,dict,start,sql,j,bool
		j=0
		set rs2dict = CreateDict()
		
		'如果是数组，则第一个元素对应rs，第二个对应前缀，第三个对应是否将名称转为小写
		if isArray( arg ) then
			bound = ubound(arg)			
			set rs = arg(0)
			if bound > 1 then				
				prefix = arg(1)
				bLcase = arg(2)
			elseif bound > 0 then
				prefix = arg(1)
				bLcase = Me.config("TMPL_ASSIGN_RS_BLCASE")
			elseif bound = 0 then
				prefix = Me.config("TMPL_ASSIGN_RS_PREFIX")
				bLcase = Me.config("TMPL_ASSIGN_RS_BLCASE")
			else
				Me.Exit( "POPASP_MVC.rs2dict分配参数错误" )
			end if
		elseif typename(arg) = "Recordset" then
			set rs = arg
			prefix = ""
			bLcase = False	
		end if
		
		if rs.BOF and rs.EOF then Exit Function
		
		'可以替换多个前缀，可为数组，也可为用逗号分隔的字符串
		if not isArray(prefix) then
			prefix = split(prefix,",")
		end if
		
		bound = ubound( prefix )
		
		start = timer()	

		reg.Global = true : reg.MultiLine = False : reg.IgnoreCase = True
		reg.Pattern = "^\s*SELECT\s+TOP\s+1\s+.+|^\s*SELECT\s+.*?LIMIT\s+1\s*;?$"		

		bool = (LCase(Me.config("DB_TYPE")) <> "access")
		if reg.test(rs.Source) then
			set dict = CreateDict()
			for i = 0 to rs.fields.count-1			
				key = rs.Fields(i).Name
				
				'去掉前缀
				if bound>=0 then	
					for m = 0 to bound
						if Me.String.StartsWith(key, prefix(m) ) then
							key = mid(key, len( prefix(m)) +1 )
							exit for
						end if
					next
				end if
				
				'将键名转为小写
				if not is_empty(bLcase) then
					key = LCase(key)
				end if

				'如果是mysql数据库，自增ID的数据类型不被支持，产生如下错误
				'变量使用了一个 VBScript 中不支持的 Automation 类型
				if bool then
					err.clear				
					Call typename( rs.Fields(i).Value )
					if err.number <> 0 then
						if typename( rs.Fields(i).Value ) = "Long" then
							dict( key ) = CLng(rs.Fields(i).Value)
						else
							dict( key ) = rs.Fields(i).Value
							'Me.Exit( "发现了不能被ASP解析的数据类型，请联系POPASP作者，以解决此BUG" )
						end if						
					else
						dict( key ) = rs.Fields(i).Value
					end if						
				else
					dict( key ) = rs.Fields(i).Value
				end if	
			next						
			set rs2dict = dict
		else			
			if rs.recordCount <> 0 then
				rs.moveFirst
			end if
			
			if rs.recordCount > Me.config("RS_2DICT_LIMIT") then
				Me.Error( "POPASP_MVC.rs2dict最多可以容纳" + Me.config("RS_2DICT_LIMIT") + "行记录，可以修改RS_2DICT_LIMIT配置项一次性获取更多记录" )
			end if			

			Do While Not rs.BOF And Not rs.EOF
				if j > Me.config("RS_2DICT_LIMIT") then Exit Do
				set dict = CreateDict()
				for i = 0 to rs.fields.count-1
					key = rs.Fields(i).Name
					
					'去掉前缀
					if bound>=0 then	
						for m = 0 to bound
							if Me.String.StartsWith(key, prefix(m) ) then
								key = mid(key, len( prefix(m)) +1 )
								exit for
							end if
						next
					end if							
					
					'将键名转为小写
					if not is_empty(bLcase) then
						key = LCase(key)
					end if
					
					'如果是mysql数据库，自增ID的数据类型不被支持，产生如下错误
					'变量使用了一个 VBScript 中不支持的 Automation 类型				
					if bool then
						err.clear
						Call typename( rs.Fields(i).Value )
						if err.number <> 0 then	'自增ID产生的bug
							if typename( rs.Fields(i).Value ) = "Long" then
								dict( key ) = CLng(rs.Fields(i).Value)
							else
								dict( key ) = rs.Fields(i).Value
								'Me.Exit( "发现了不能被ASP解析的数据类型，请联系POPASP作者，以解决此BUG" )
							end if							
						else
							dict(key) = rs.Fields(i).Value
						end if						
					else
						dict(key) = rs.Fields(i).Value
					end if					
				next
				
				rs2dict.add j,dict
				rs.MoveNext
				j = j+1
			Loop
		end if
		
		if config("APP_DEBUG") = 1 Then
			set sql = D_
			sql("time") = round((timer() - start) * 1000,0)
			sql("sql") = rs.Source + " ; -- Recordset to Dictionary "
			set dSql( dSql.count + 1 ) = sql
		end if
		set dict = nothing
		call L_("POPASP_MVC.rs2dict")
	End Function
	
	
	' 计算个数
	' 字符串计算长度，数值、日期、货币、布尔值返回1
	' Recordset对象返回记录个数
	' Dictionary、Files、IVariantDictionary、IRequestDictionary返回实际个数
	' session返回Session.Contents.Count
	' 其他返回0
	Public Function count( var )
		if isArray(var) Then
			count = ubound(var) + 1
		else
			select case TypeName(var)
				case "String"
					count = Len(var)
				case "Integer","Long","Single","Double","Currency","Decimal","Date","Boolean"
					count = 1
				case "Dictionary","Files","IVariantDictionary","IRequestDictionary"
					count = var.Count
				case "Recordset"
					count = var.RecordCount
				case "ISessionObject"
					count = var.Contents.Count
				case "Empty","Null","Unknown","Nothing","Error"
					count = 0
				case else
					count = 0
			End Select
		End If
	End Function
	
	'返回文件的绝对路径
	'path不管是相对路径，还是绝对路径，都返回绝对路径
	Function realPath( path )
		if isNul( path ) then
			exit function
		end if
		on error resume next
		if inStr(path,":") > 0 then
			realPath = path
		else
			realPath =Server.mapPath( Replace(  path  ,"/" , "\" ) )
		end if
		Call L_("POP_MVC.realPath " + path )
	End Function
	
	sub calExtend(ByRef c , ByRef trueCtrl , ByRef ExtendName )
		dim arr
		'扩展
		ExtendName = ""
		trueCtrl = c
		if Me.config("EXTEND_PATH") <> "" then
			if inStr( c , Me.config("EXTEND_SEPARATOR") ) > 1 then
				arr = split( c, Me.config("EXTEND_SEPARATOR") ,2 )
				ExtendName = arr(0)
				trueCtrl = arr(1)			
			end if
			'如果指定了默认的插件名
			if  Me.config("DEFAULT_EXTEND") <> "" and ExtendName = "" then
				ExtendName = Me.config("DEFAULT_EXTEND")
				c = ExtendName + Me.config("EXTEND_SEPARATOR") + c
			end if
		end if	
		CurExtend = ExtendName
	end sub
	
	' for aspbbs
	Private Sub Action__(ByVal c,ByRef a)
		on error resume next
		dim className,ctrl_action,ctrl_str,rej,startTime,trueCtrl,ExtendName,dict
		dim ExtendCtrl,EmptyCtrl,ctrlPath,rejFile,rejContent
		
		rejFile =  POP_MVC.appPath + "/Runtime/Class/" + c + ".rejection.asp"
		rejFile = realPath(rejFile)
		
		Call calExtend( c , trueCtrl , ExtendName )
		
		if ExtendName <> "" and isEmpty( isLoadExtendConfig ) then
			'继续默认插件的配置
			if LCase(ExtendName) <> LCase(config("DEFAULT_EXTEND")) and config("INHERIT_DEFAULT_EXTEND") = 1 then
				Call loadConfig( rtrim(Me.config("EXTEND_PATH") , "/" ) + "/" + config("DEFAULT_EXTEND") + "/config.inc.asp" , 1 )
			end if
			isLoadExtendConfig = rtrim(Me.config("EXTEND_PATH") , "/" ) + "/" + ExtendName + "/config.inc.asp"
			Call loadConfig( isLoadExtendConfig , 1 )
		end if		
		ctrlPath = get_ctrl_path( c )
		if Me.File.isFile( ctrlPath ) Then	'判断控制器文件是否存在
			ctrl_str = get4asp( ctrlPath )


			if inStr( ctrl_str , "'Extends bbs--Top" ) > 0 then	
				set ExtendCtrl = new Top
				Call ExtendCtrl.initialize

			elseif inStr( ctrl_str , "'Extends bbs--Common" ) > 0 then
				set ExtendCtrl = new Common
				Call ExtendCtrl.initialize
			end if
			
			className = UCFirst( LCase( trueCtrl ) ) + "Action"		'标准的控制器名称
			
			
			ctrl_str = String.reg_replace( ctrl_str, "Class " + className , "Class\s+\[?" + trueCtrl & "\]?" , "i"  )
			Execute ctrl_str	'运行类	
			if config("APP_DEBUG") = 1 or not Me.fso.FileExists( realPath( rejFile ) )  then
				set objReject = new POPASP_REJECTION
				set rej = objReject.ParseContent( ctrl_str )	'解析控制器的映射	
				if config("APP_DEBUG") = 0 then
					Call objReject.saveParse( rejFile , rej )
				end if
			elseif  Me.fso.FileExists( realPath( rejFile ) ) then
				rejContent = file_get_contents( rejFile ) 
				set dict = CreateDict
				Execute rejContent
				set rej = dict
			end if
			

		
			If Err.number <> 0 Then
				if config("APP_DEBUG") = 1 Then
					call L_( "控制器" + c + "Action" + "中代码有误" )
					call quit
				else
					Call ctrl_code_error_handler
				end if
			End If
			
			'实例化类，如果没有该类进行处理
			Execute "set ctrl_action = new [" + className + "]"
		
			if isObject( ExtendCtrl ) then
				set ExtendCtrl.son = ctrl_action
			end if
	
	
			' 第一次执行 intialize 方法
			if Me.Arr.iExists(rej("public_method"),Me.config("INITIALIZE_ACTION")) Then
				Execute "call ctrl_action." + Me.config("INITIALIZE_ACTION")
				Call ctrl_error( trueCtrl, Me.config("INITIALIZE_ACTION") )
			End If	
		
			' 前面已经加载过一次initialize了
			'isExecuteExtends = false
			if LCase(a) <> Me.config("INITIALIZE_ACTION") Then
						
				'加载方法，如果有该方法进行处理
				if Me.Arr.iExists(rej("public_method"),LCase(a)) Then	
				
					Call F_("开始执行 " + className + "." + a )
					Execute "call ctrl_action." & a						
					Call F_("执行结束 " + className + "." + a )

					method_exists = true
					Call ctrl_error( trueCtrl, a )
				else 
					if isObject( ExtendCtrl ) then
						set ExtendCtrl.son = ctrl_action
						
						execute "ExtendCtrl." + a
						if err.number = 438 then
							if Me.Arr.iExists(rej("public_method"),config("EMPTY_ACTION")) Then	
								Execute ClassName + "." + config("EMPTY_ACTION")
							else
								set EmptyCtrl = new Empty__
								call EmptyCtrl.index
							End If
						end if
					else
						set EmptyCtrl = new Empty__
						call EmptyCtrl.index						
					end if
				end if
			End If
		Else		
			set EmptyCtrl = new Empty__
			call EmptyCtrl.index
		End if
		set ctrl_action = nothing
		set EmptyCtrl = nothing
		if isObject( objReject ) then
			set objReject = nothing
		end if
	End Sub
	
	' 执行控制器
	Private Sub Action_(ByVal c,ByRef a)
		on error resume next
		dim className,ctrl_action,template_file,ctrl_str,rej,bool,fc,fd,f1,dstname,key,inc_file,inc_content,trueCtrl,ExtendName
		'防止无穷尽循环，如果超过10次，则退出
		if action_count > 10 Then Exit Sub
		action_count = action_count + 1
		
		if isEmpty( objReject ) then
			set objReject = new POPASP_REJECTION
		end if
		
		Call calExtend( c , trueCtrl , ExtendName )
		
		if ExtendName <> "" and isEmpty( isLoadExtendConfig ) then
			'继续默认插件的配置
			if LCase(ExtendName) <> LCase(config("DEFAULT_EXTEND")) and config("INHERIT_DEFAULT_EXTEND") = 1 then
				Call loadConfig( rtrim(Me.config("EXTEND_PATH") , "/" ) + "/" + config("DEFAULT_EXTEND") + "/config.inc.asp" , 1 )
			end if
			isLoadExtendConfig = rtrim(Me.config("EXTEND_PATH") , "/" ) + "/" + ExtendName + "/config.inc.asp"
			Call loadConfig( isLoadExtendConfig , 1 )
		end if
		
						
		Call F_("准备执行 " + c + "Action." & a)
		'根据控制器文件是否存在进行相应的处理
		if Me.File.isFile( get_ctrl_path( c ) ) Then	'判断控制器文件是否存在
		
			' 记录应用初始化时间
			startTime = timer()			
			Me.dG_( "initTime" ) = startTime
			
			bool = false
			dstname = Me.appPath & "/Runtime/Class/" & c & "Action.class.asp"
			
			inc_content = ""	
			
			if Me.fso.FileExists( Me.realPath( dstname ) ) then	'如果目标文件存在	

				if DateDiff("s" , Me.File.mtime( get_ctrl_path( c ) ) ,Me.File.mtime( dstname ) ) >= 0 then
					bool = true
				end if
			
				inc_file = appPath & "/Runtime/Class/" & c & "Action.inc.asp"				
			
				If fso.FileExists( Me.realPath( inc_file  ) ) then '如果包含文件存在
					if DateDiff("s" , Me.File.mtime( dstname ), Me.File.mtime( inc_file ) )   >= 0 then
						bool = false
						inc_content = Me.asp_get_contents( inc_file )
					elseif DateDiff("s" ,Me.File.mtime( inc_file ) , Me.File.mtime( get_ctrl_path( c ) ) ) >=0 then
						bool = false
						inc_content = Me.asp_get_contents( inc_file )
					end if
				End If
			end if

			if bool then
				ctrl_str = get4asp( dstname )
			else			
				ctrl_str = Me.asp_get_contents(get_ctrl_path( c ))	'得到控制器文件的内容				

				ctrl_str =  Me.String.reg_replace( ctrl_str, "Class [" & trueCtrl & "Action]$1"  & vbCrLf & vbTab & "Public " & Me.config("VAR_CONTROLLER") & ",parent,son" & VbCrLf & inc_content, "^\s*Class\s+\[?" & trueCtrl & "\]?(.*)","im" )
				
				ctrl_str = Me.removeAspComments( ctrl_str )
				
				Call Me.file_put_contents( dstname,"<" & "%" & VbCrLf & ctrl_str & "%" & ">" )	
			
				if not is_empty( Me.applicationOn ) Then	'开启了 Application 缓存									
					Me.Apt.set dstname,ctrl_str
				end if
				Me.File.remove( Me.appPath & "/Runtime/Class/" & c & "Action.rejection.asp" )
			End if

			className = UCFirst( LCase( trueCtrl ) ) & "Action"		'标准的控制器名称	
			
			Execute ctrl_str	'运行类
			
			If Err.number <> 0 Then
				if config("APP_DEBUG") = 1 Then
					call L_( "控制器" & c & "Action" & "中代码有误" )
					call quit
				else
					Call ctrl_code_error_handler
				end if
			End If
			
			'实例化类，如果没有该类进行处理
			Execute "set ctrl_action = new [" & className & "]"
			
			if ExtendName <> "" then
				call pushTime( startTime , "New " & ExtendName & Me.config("EXTEND_SEPARATOR") & className )
			else
				call pushTime( startTime , "New " & ExtendName & "" & className )
			end if
			

			If isEmpty(ctrl_action) <> 0 Then
				Call ctrl_not_exists_handler
			End If
			
			If Err.number <> 0 Then
				call L_( "控制器" & c & "Action" & "中代码有误" )
			End If			

			if ExtendName <> "" then
				set rej = objReject.ParseStr( ctrl_str , c )	'解析控制器的映射
			else
				set rej = objReject.Parse( className )	'解析控制器的映射	
			end if
			
			Execute("set ctrl_action." & Me.config("VAR_CONTROLLER") & " = import(""POPASP_CONTROLLER"")")	'实例化控制器父类
			
			if not isEmpty( ctrl_son ) then
				set ctrl_action.son = ctrl_son
			end if
			
			' 如果有继承关系，则执行父类方法 initialize
			if isExecuteExtends AND rej.Exists("extends") Then
				set ctrl_son = ctrl_action
				Call F_("发现了继承类" & rej("extends") & "，准备执行继承类") 
				Call Action_( rej("extends") , Me.config("INITIALIZE_ACTION") )
				set ctrl_action.parent = objExtendsClass
			End If	
			
			set objExtendsClass = ctrl_action		

			' 第一次执行 intialize 方法
			if Me.Arr.iExists(rej("public_method"),Me.config("INITIALIZE_ACTION")) Then
				Call F_("开始执行 " & className & "." & Me.config("INITIALIZE_ACTION") )
				Execute "call ctrl_action." & Me.config("INITIALIZE_ACTION")
				Call F_("执行结束 " & className & "." & Me.config("INITIALIZE_ACTION") )
				
				Call ctrl_error( trueCtrl, Me.config("INITIALIZE_ACTION") )
			End If			
			
			' 前面已经加载过一次initialize了
			'isExecuteExtends = false
			if LCase(a) <> Me.config("INITIALIZE_ACTION") Then
				'前置操作
				if Me.Arr.iExists(rej("public_method"),Me.config("BEFORE_ACTION_PREFIX") & LCase(a)) Then
					method_exists = true				
					Call F_("发现了前置操作 " & className & "." & Me.config("BEFORE_ACTION_PREFIX") & a)
					Call F_("开始执行 " & className & "." & Me.config("BEFORE_ACTION_PREFIX") & a)
					Execute "call ctrl_action." & Me.config("BEFORE_ACTION_PREFIX") & a						
					Call F_("执行结束 " & className & "." & Me.config("BEFORE_ACTION_PREFIX") & a)				
				end if
				
				Call ctrl_error( trueCtrl, Me.config("BEFORE_ACTION_PREFIX") & a )
						
				'加载方法，如果有该方法进行处理
				if Me.Arr.iExists(rej("public_method"),LCase(a)) Then	
				
					Call F_("开始执行 " & className & "." & a )
					Execute "call ctrl_action." & a						
					Call F_("执行结束 " & className & "." & a )
					
					method_exists = true
					Call ctrl_error( trueCtrl, a )
					
					'后置操作
					if Me.Arr.iExists(rej("public_method"),Me.config("AFTER_ACTION_PREFIX") & LCase(a)) Then
						Call F_("发现了后置操作 " & className & "." & Me.config("AFTER_ACTION_PREFIX") & a)
						Call F_("开始执行 " & className & "." & Me.config("AFTER_ACTION_PREFIX") & a)
						Execute "call ctrl_action." & Me.config("AFTER_ACTION_PREFIX") & a						
						Call F_("执行结束 " & className & "." & Me.config("AFTER_ACTION_PREFIX") & a)				
					end if

					Call ctrl_error( trueCtrl, Me.config("AFTER_ACTION_PREFIX") & a )
				Else
					'如果方法不存在，但是模板文件存在，也让其显示
					if ExtendName <> "" then
						template_file = rtrim(Me.config("EXTEND_PATH") , "/" ) & "/" & ExtendName & "/" & trueCtrl & "/" & a & getConfig("TMPL_TEMPLATE_SUFFIX")
					elseif Me.config.exists( "TPL_PATH" ) then
						template_file = Me.config( "TPL_PATH" ) & trueCtrl & "/" & a & getConfig("TMPL_TEMPLATE_SUFFIX")
					else
						template_file = Me.appPath & "/TPL/" & trueCtrl & "/" & a & getConfig("TMPL_TEMPLATE_SUFFIX")
					end if
					if Me.fso.FileExists( Me.realPath(template_file))  Then
						method_exists = true
						Call F_( "找到模板文件 "  & template_file & Me.config("TMPL_TEMPLATE_SUFFIX") & " ，进行渲染" )
						Err.clear
						'Me.import("POPASP_CONTROLLER").load("")
						'Me.import("POPASP_CONTROLLER").show

						Me.import("POPASP_CONTROLLER").display( c & "/" & a )
					elseif not is_empty( Me.Config("EXEC_PARENT_SAME_ACTION") ) AND isExecuteExtends AND rej.Exists("extends") Then  ' 如果有继承关系，则执行父类同名方法	
						if isExecuteExtends then
							Call Action_( rej("extends") , config("EMPTY_ACTION") )	
						else
							isExecuteExtends = False
							method_exists = true
							Call F_( "没有方法 "  & className & "." & a )
							Call F_("尝试执行父类的方法 " & rej("extends") & "." & a ) 
							Call Action_( rej("extends") , a )	
							isExecuteExtends = true
						end if
					else
						if UCase(c) = "AJAX__" then
							Me.exit("未找到方法" & c & "." & a )
						else
							Call Action_( rej("extends") , config("EMPTY_ACTION") )	
						end if						
					End If
				End If
				
				if err.number<>0 Then				
					Me.exit( L_("") )
				End If
				
				if not method_exists and c = temp_ctrl Then
					
					Call F_( "没有方法 "  & className & "." & a )
					if Me.Arr.iExists(rej("public_method"),Me.config("EMPTY_ACTION"))  Then
						isExecuteExtends = false
						Call Action_( c,Me.config("EMPTY_ACTION")  )
					else
						Call F_( "没有方法 "  & c & "." & Me.config("EMPTY_ACTION") )
					End If
				End If
			End If
		ElseIf Me.File.isFile( Me.appPath & "/Controller/" & c & "Action.class.asp" ) Then
			Response.write("你目前使用的是POPASP" & Me.Version & "版本，但是你在控制器命名方面仍然使用的是2.3版本以前的命名方式")
			Me.quit
		Else	'没有找到类文件		
			' 记录应用初始化时间
			Me.dG_( "initTime" ) = timer()
			Call F_( "没有找到控制器 " & c & " 对应的类文件" )
			Call ctrl_file_not_exists_handler
		End if	
		Call L_("")
	End Sub
	
	private sub ctrl_error( c,a )
		if err.number<>0 Then		
			response.write( L_( "控制器中的方法 " + c & "." + a + " 代码有误，不能成功执行" ) )
			if config("APP_DEBUG") = 1 then 
				Response.write "<pre style='font-size:16px;'>请谨慎书写代码，并使用下面的代码来调试错误。" + vbCrLf + vbCrLf + "Sub " + a  +  vbCrLf + "　on error resume next"  + vbCrLf + "　'这里是你的代码"  + vbCrLf + "　Call L_("""")"  + vbCrLf + "End Sub" + vbCrLf + vbCrLf + "如果不能解决问题，请加入QQ群124648143，跟popasp爱好者一起讨论解决。</pre>"
			end if
		End If		
	end sub
	
	'不缓存页面信息
	Public Sub NoCache()
		Response.Expires = 0
		Response.ExpiresAbsolute = Now() - 1
		Response.CacheControl = "no-cache"
		Response.AddHeader "Expires",Now() - 1
		Response.AddHeader "Pragma","no-cache"
		Response.AddHeader "Cache-Control","private, no-cache, must-revalidate"
	End Sub
	
	' 根据键名获取 Request.QueryString 中的值
	' 如果使用了路由，则须使用POP_MVC.get
	Public Property Get [Get]( key )
		if key="" then
			[Get] = Request.QueryString
		else
			If Me.config("URL_MODE") <> 0 then
				if isEmpty( dQuery ) then
					set dQuery = CreateDict
					dim params,i,query,values,j,rules,rule,addParams,line
					rules = Me.config( "URL_RULE" )
					query = request.querystring
					if Me.config( "URL_SUFFIX" ) <> "" and  Me.String.iEndswith(query, Me.config( "URL_SUFFIX" ) ) then
						query = left( query , len(query) - len(Me.config( "URL_SUFFIX" )) )
					end if
					if isArray( rules ) then
						for i = lbound( rules ) to ubound( rules )						
							rule = rules(i)(0)
							if String.reg_test( query, rule , "i" ) then
								if ubound(rules(i)) > 2 then
									values = split( query,rules(i)(3) ) 
									params = split( rules(i)(1) , rules(i)(3) )
								else
									values = split( query,Me.config("URL_DEPR") ) 
									params = split( rules(i)(1) , Me.config("URL_DEPR") )
								end if
								
								'添加参数
								for j = lbound( params ) to ubound( params )
									dQuery( LCase(params(j) ) ) =  values(j)
								next
								'第3个参数为附加参数  附加参数格式 key1=value1&key2=value2
								'每组之间用&分隔，键值之间用=分隔
								if ubound( rules(i) ) > 1 then
									'附加参数
									addParams = rules(i)(2)	
									'将每组分开
									addParams = split( addParams , "&" )	
									for j = lbound(addParams) to ubound(addParams)
										'将键值对分开
										line = split( addParams(j) , "=" )  
										if ubound( line ) > 0 then
											dQuery( LCase(line(0)) ) = line(1)
										end if
									next
								end if
								exit for
							end if
						next
					end if
				end if
				if dQuery.exists( lcase(key) ) then
					[Get] = dQuery( LCase( key ) )
				elseif Me.config("URL_MODE") = -1 then
					if  Me.dQuery.count = 0 then
						Me.config("URL_MODE") = 0
					end if
					[Get] = Request.QueryString(key)		
				end if 
			else
				if isEmpty( dQuery ) then
					[Get] = getReqValue(key,Request.QueryString(key))
				else
					if dQuery.exists( lcase(key) ) then
						[Get] = dQuery( LCase(key) )
					else
						[Get] = getReqValue(key,Request.QueryString(key))
					end if
				end if
			end if
		end if
	End Property
	
	'如果使用了路由，则还可以使用POP_MVC.get设置值
	'POP_MVC.get(键名) = 值
	Public Property Let [Get]( key , value )
		if isEmpty( dQuery ) then
			set dQuery = Me.CreateDict
		end if
		if isObject( value ) then
			set dQuery( LCase(key) ) = value
		else
			dQuery( LCase(key) ) = value
		end if
	End Property

	' 根据键名获取 Request.Form中的值
	Public Property Get Form(key)
		if key = "" then
			Form = Request.Form
			exit Property
		end if
		if not isEmpty(Request.Form(key)) Then
			if TypeName(Request.Form(key)) = "IStringList" Then
				Form =  getReqValue( key,CStr( Request.Form(key) ) )
			Else
				Form = getReqValue( key,Request.Form(key) )
			End If			
		End If
		if not isNumeric( Form ) then
			Form=replace(Form,CHR(34),"&quot;")
			Form=replace(Form,CHR(39),"&apos;")
		end if
	End Property
	
	' 根据键名获取 Request 中的值
	Public Property Get Req(key)
		Req = Me.get(key)
		if not isEmpty( Req ) then
			Exit Property
		else
			if not isEmpty(Request.Form(key)) Then
				if TypeName(Request.Form(key)) = "IStringList" Then
					Req = getReqValue( key, CStr( Request.Form(key) ) )
				Else
					Req = getReqValue( key,Request.Form(key) )
				End If			
			End If	
		end if
	End Property
	
	' 根据键名获取 Request.ServerVariables 中的值
	Public Property Get Vars(key)
		vars = Request.ServerVariables(key)
	End Property
	
	' 获取全局变量request.ServerVariables("HTTP_USER_AGENT")
	Public Property Get Agent()
		Agent=request.ServerVariables("HTTP_USER_AGENT")
	End Property

	' 获取REFERER
	Public Property Get Refer()
		Dim ref
		ref=Me.req("REFERER")
		if isNul(ref) then
			ref=Request.ServerVariables("HTTP_REFERER")
		end if
		if not isnul( ref ) then
			Refer=Server.URLEncode(ref)
		end if		
	End Property

	Public Property Get Servername()
		Servername=request.ServerVariables("server_name")
	End Property
	
	'判断请求网址是否从本网站的一个链接而来，而非直接打开页面而来。
	Public Property Get isSelfOrigin()
		isSelfOrigin = isSameOrigin( "", "" ) 
	End Property
	
	'判断请求网址是否从本网站的一个链接而来，而非直接打开页面而来。
	Public Property Get isSameOrigin( originUrl, testUrl )
		dim str1,str2,firstChar
		
		if isNul(testUrl) then 
			str2 = Request.ServerVariables("HTTP_HOST")
		else
			str2 = testUrl
			
			firstChar = left(str2,1)
		
			if firstChar = "/" or firstChar = "?" or firstChar = "#" then
				isSameOrigin = true
				exit Property
			end if
		end if
		
		str2 = Me.String.reg_replace( str2 , "" , "^https?://" , "i" )
		str2 = Me.String.Before(str2,":")		
		str2 = Me.String.Before(str2,"/")
		str2 = Me.String.Before(str2,"?")
		str2 = Me.String.Before(str2,"#")
		
		if isNul( originUrl ) then
			str1 = Request.ServerVariables("HTTP_REFERER")			
		else
			str1 = originUrl			
		end if
		
		str1 = Me.String.reg_replace( str1 , "" , "^https?://" , "i" )
		str1 = Me.String.Before(str1,":")
		str1 = Me.String.Before(str1,"/")
		str1 = Me.String.Before(str1,"?")
		str1 = Me.String.Before(str1,"#")
		
		if not Me.String.iStartsWith( str1 , "www." ) and Me.String.iStartsWith( str2 , "www." ) then
			str1 = "www." + str1 
		end if
		isSameOrigin = Me.String.iStartsWith( str1 ,  str2 ) 
	End Property
	
	' 判断请求网址是否为ajax请求
	Public Property Get isAjax()
		on error resume next
		isAjax = False
		if Request.ServerVariables("HTTP_X_REQUESTED_WITH") = "XMLHttpRequest" then
			isAjax = True : Exit Property
		Elseif Not isEmpty( request( Me.config("VAR_AJAX_SUBMIT") ) ) Then
			if err.number = 0 then isAjax = True : Exit Property			
		End If
	End Property
  
    '进行项目部署时，使用start
	public Property get start
		'dim struct
		
		call init
		'set struct = import("POPASP_STRUCTRUE")
		'set struct = new POPASP_STRUCTRUE
		'call struct.handle				
		
		c = Url.get_ctrl_name()	
		Call calExtend( c , "" , "" )		
		
		a = Url.get_action_name()
		
		'防止通过URL参数来进行前置操作或后置操作的调用
		if Me.String.iStartsWith( a , Me.config("BEFORE_ACTION_PREFIX") ) then
			if is_empty(Me.config( "APP_DEBUG" )) then
				a = Me.config("EMPTY_ACTION")
			else
				Response.write("禁止通过URL调用前置操作，可以在程序中使用Call A_(""" + c + "/" + a + """)来调用前置操作")
				Me.quit
			end if
		elseif Me.String.iStartsWith( a , Me.config("AFTER_ACTION_PREFIX") ) then
			if is_empty(Me.config( "APP_DEBUG" )) then
				a = Me.config("EMPTY_ACTION")
			else
				Response.write("禁止通过URL调用后置操作，可以在程序中使用Call A_(""" + c + "/" + a + """)来调用后置操作")
				Me.quit
			end if
		end if		

		Execute("const CTRL__ = """ + APP__ + "?" + Me.config("VAR_MODULE") + "=" + c + """" )
		Execute("const ACT__ = """ + CTRL__ + "&" + Me.config("VAR_ACTION") + "=" + a + """")
		Execute("const URL__ = """ + CTRL__ + "&" + Me.config("VAR_ACTION") + "=" + """")	
		
		if isEmpty( isExecuteExtends ) then		
			isExecuteExtends = True	
		end if
		
		Call Action( c + "/" + a )
		isExecuteExtends = False		
		'set struct = nothing
		
		if c = Me.config("SYSTEM_MODULE") OR c = Me.config("EMPTY_MODULE") then
			if is_empty( Me.config("SHOW_PAGE_TRACE") ) then
				Call quit
			end if
		end if
	end Property 

	'项目部署完之后，使用run
	public Property get run		
		if Me.Get( Me.config("VAR_MODULE") ) <> "" then
			call Me.start
		else
			call init				
			isExecuteExtends = True		
		end if
	end Property  	
End Class
%>