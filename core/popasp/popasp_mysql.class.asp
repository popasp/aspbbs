<%
' +----------------------------------------------------------------------
' | POPASP [ ASP MVC ]
' +----------------------------------------------------------------------
' | Copyright (c) 2016 http://popasp.com All rights reserved.
' +----------------------------------------------------------------------
' | Author: popasp <1737025626@qq.com>
' +----------------------------------------------------------------------

'在本类中已经使用了Replace作为方法名，所以在本类中不能再使用Replace函数了，请考虑使用正则的Replace方法吧。
Class POPASP_MYSQL
	'经测试发现sqlite3的列字段类型为blob时会出错。

	'为了减轻数据库操作类的重量，特将相同代码写到了另外一个文件
	'POPASP_REPLACE_CLASS_CODE_POPASP'
	
	' 获取 mysql 数据表的字段类型
	' [ { "Field": "SortID", "Type": "int(11)", "Null": "NO", "Key": "PRI", "Default": null, "Extra": "auto_increment" }, { "Field": "LanguageID", "Type": "int(11)", "Null": "YES", "Key": "", "Default": null, "Extra": "" }, ... ]
	Function getFieldsType( ByVal table_name )
		on error resume next
		dim rs
		table_name = getTrueTableName(table_name)
		
		lastSql = "DESC " & table_name 
		set rs = Me.getRS( lastSql )

		set getFieldsType = P_("POPASP_RECORDSET").rs2data(rs,"",isAccess)
		Call tool.closeRS(rs)
		Call L_( typename(Me) & ".getFieldsType" )
	End Function
	
	Function toAccessTableSql( ByVal table_name , ByVal access_prefix )
		dim sql , dict
		set dict = Me.getFieldsType( table_name )
		
		'判断表名前是否有前缀
		if POP_MVC.String.iStartsWith( table_name , db_prefix ) then
			table_name = access_prefix  & mid( table_name , len( db_prefix ) + 1  ) 
		else
			table_name = access_prefix & table_name
		end if	
		
		'比如：CREATE TABLE IF NOT EXISTS iAspCms_sort (
		sql = "CREATE TABLE " & table_name & " ("
	
		'添加每个字段信息
		for each key in dict
			if dict(key)("Key") = "PRI" then
				sql = sql & vbcrlf & "[" & dict(key)("Field") & "]" & " COUNTER PRIMARY KEY  NOT NULL,"
			else
				sql = sql & vbcrlf & "[" & dict(key)("Field") & "]" & " " & mysql2accessFieldType( dict(key)("Type") ) & " ,"
			end if
		next
		
		'删除最后一个逗号
		sql = POP_MVC.rtrim( sql, "," )		
		
		'加入 );
		sql = sql & vbcrlf & ")"		
		toAccessTableSql = sql		
	End Function
	
	Function mysql2accessFieldType( ByVal fieldType )
		dim num
		fieldType = Lcase( fieldType )
		if inStr( fieldType , "tinyint" ) then
			mysql2accessFieldType = "BYTE"
		elseif inStr( fieldType , "smallint" ) then
			mysql2accessFieldType = "SHORT"
		elseif inStr( fieldType , "int" ) then	
			mysql2accessFieldType = "INTEGER"
		elseif inStr( fieldType , "float" ) then
			mysql2accessFieldType = "single"
		elseif inStr( fieldType , "double" ) then
			mysql2accessFieldType = "float"
		elseif inStr( fieldType , "decimal" ) then
			mysql2accessFieldType = "DECIMAL"
		elseif inStr( fieldType , "datetime" ) then
			mysql2accessFieldType = "DATE/TIME"
		elseif inStr( fieldType , "date" ) then
			mysql2accessFieldType = "DATE/TIME"
		elseif inStr( fieldType , "time" ) then
			mysql2accessFieldType = "DATE/TIME"
		elseif inStr( fieldType , "timestamp" ) then
			mysql2accessFieldType = "TIMESTAMP"
		elseif inStr( fieldType , "year" ) then
			mysql2accessFieldType = "SHORT"
		elseif inStr( fieldType , "char" ) > 0 and inStr( fieldType , "varchar" ) <1 then
			mysql2accessFieldType = fieldType
		elseif inStr( fieldType , "tinyblob" ) then
			mysql2accessFieldType = replace(fieldType , "tinyblob" , "varchar" )
		elseif inStr( fieldType , "tinytext" ) then
			mysql2accessFieldType = replace(fieldType , "tinytext" , "varchar" )	
		elseif inStr( fieldType , "varchar" ) then
			num = POP_MVC.String.reg_fetch( fieldType , "\d+" , "" )			
			if isEmpty( num ) then
				mysql2accessFieldType = "MEMO"
			else
				num = CInt( num )
				if num > 255 then
					mysql2accessFieldType = "MEMO"
				else
					mysql2accessFieldType = fieldType
				end if				
			end if
		elseif inStr( fieldType , "blob" ) then
			mysql2accessFieldType = "Binary"
		elseif inStr( fieldType , "TEXT" ) then
			mysql2accessFieldType = "MEMO"
		else
			mysql2accessFieldType = "CHAR(255)"
		end if
	End Function
	
	' 获取表的所有字段与字段类型
	Function getTableFields( ByVal table_name )
		table_name = getTrueTableName(table_name)
		set getTableFields = tool.getTableFields("SELECT * FROM `" & table_name	& "` LIMIT 1")
	End Function
	
	Function InsertByExecute( table,sql )
		InsertByExecute = tool.Execute( sql )		
		if InsertByExecute > 0 Then
			InsertByExecute = mysql_last_insert_id( table )
		End if
	End Function
	
	' 向数据表中插入数据，data为Dictionary对象，其键名与字段名相对应，如果data含主键，须手动删除
	Public Function Insert(ByVal table,ByVal data)
		table = getTableFromInput(table)
		call handlerData( table,data , 1 ) '剔除不存在的字段		
		lastSql = tool.getInsertSql( table, data , getTableStructure( table ) )
		Insert = InsertByExecute( table , lastSql )
	End Function
	
	' 向数据表中添加数据，采用sql添加
	Public Property Get Add()
		call parseOptions	'解析参数		
		if is_empty(parsedOptions("table")) then 
			Call setPoptsTable( parsedOptions )	
		end if
		
		if isEmpty( parsedOptions("data") ) Then
			set parsedOptions("data") = POP_MVC.Form2Dict()
		end If
		
		if ( not is_empty( parsedOptions("data") ) ) Then
			'剔除不存在的字段
			call handlerData( parsedOptions("table"),parsedOptions("data") , 1 )			
			
			lastSql = getInsertSql(parsedOptions)	
			Add = InsertByExecute( parsedOptions("table") , lastSql )
		else 
			POP_MVC.error( "POPASP_MYSQL.Add" )
		end If		
	End Property
	
	' Replace，无则添加，有则修改，此时where无效，返回结果是最后添加或修改的ID
	Public Property Get [Replace]()
		On Error Resume Next
		dim popts,pk,max,findCount	
		call parseOptions	'解析参数		
		set popts = parsedOptions
		
		if is_empty(popts("table")) then
			Call setPoptsTable( popts )	
		end if
		
		pk = getPrikey( popts("table") )	'得到主键
		if popts("data").Exists( pk ) Then	'如果数据中存在主键，则需要从是否为最大ID与是否存在该ID两方面判断
			set rs = getRS( "SELECT MAX(" & pk & ") AS theResult FROM `" & popts("table") & "` LIMIT 1")
			rs.moveFirst
			max = rs("theResult")	'得到最大ID
			tool.closeRS rs
			set rs = getRS( "SELECT * FROM `" & popts("table") & "` WHERE " & pk & " = " & popts("data")(pk) & " LIMIT 1" )
			findCount = rs.RecordCount	'以该ID查找一行记录，找到为1，找不到为0
			tool.closeRS rs
			if max >= popts("data")(pk) then	'数据ID <= 最大ID 
				if findCount > 0 then	'如果能找到，则修改
					call options.Remove("where")	
					[Replace] = popts("data")(pk)	'返回实际修改的ID
					if Save() = 0 then
						[Replace] = 0
					end if
				else	'如果找不到，插入到数据ID的位置上。返回该数据ID
					[Replace] = Add()
				end if
			else	' 超出最大ID，则添加，如果数据表被全部删除，此时生成的ID并不是1
				call options("data").Remove(pk)
				[Replace] = Add()
			end if			
		else	'不存在主键，直接添加
			[Replace] = Add()
		end if		
		call L_( Me.db_type & " Replace" )
	End Property
	
	' 得到数据库中的所有表名，返回一个对象
	' 例： {"0":"about", "1":"category", "2":"contact", "3":"feedback", "4":"post", "5":"swiper", "6":"user"}
	Public Function getTables()		
		on error resume next
		dim arr,rs,content
		dim filename
		
		' 如果已经保存了数据，直接返回
		if not isEmpty(dTables) Then
			set getTables = dTables
			Exit Function
		End If
		
		'如果对应文件不存在，则须先将数据生成json保存到文件中
		filename = tool.getDataFileName("_tables_")
		arr = Array()
		if Not is_empty( POP_MVC.config("APP_DEBUG") ) OR Not tool.file_exists( filename ) Then
			call tool.initConn		
			Set rs = tool.getRS( "SHOW TABLES" )
			Do While Not rs.EOF 
				POP_MVC.Arr.Push arr,rs("Tables_in_" & POP_MVC.config("DB_NAME"))
				rs.MoveNext
			Loop
			tool.closeRS rs
			call tool.file_put_contents(filename, join(arr,",") )
		else		'从文件中取出数据
			content = POP_MVC.file_get_contents(tool.getFilePath(filename))
			content = mid(content,2)
			arr = split(content,",")
		End If		
		set dTables = POP_MVC.Arr.toDict(arr)
		set getTables = dTables		
		Call L_("POPASP_MYSQL.getTables")
	End Function
	
	'获取数据表结构
	Public Function getTableStructure( ByVal tableName )
		call getTables
		tableName = getTrueTableName(tableName)
		if dTables.count = 0 then
			Call getTables()
		end if
		set getTableStructure = tool.getTableStructure( tableName,dTables,dTS,"SELECT * FROM `" & tableName	& "` LIMIT 1" )
	End Function
	
	Function mysql_last_insert_id( ByVal tableName )
		dim rs
		set rs = getRS( "SELECT LAST_INSERT_ID() as theResult" )
		mysql_last_insert_id = CSTR(rs("theResult"))	'将数值转化成字符串比较可靠
		call tool.closeRS(rs)
	End Function
	
	Function Explain( ByRef sql )
		set Explain = getRS( "Explain " & sql)
	End Function
	
	Sub SetNames( charset )
		tool.conn.execute( "set names " & charset )
	end sub
	
	'清空mysql的所有数据表
	Public Property Get Truncate( ByVal table_name )
		on error resume next
		dim dbType,dbName
		table_name = getTrueTableName( table_name )
		dbType = POP_MVC.config("DB_TYPE")	
		dbName = POP_MVC.config("DB_NAME")	

		POP_MVC.config("DB_TYPE") = "mysql"
		POP_MVC.config("DB_NAME") = db_name
		P_("db").execute( "TRUNCATE TABLE " & table_name )
		POP_MVC.config("DB_TYPE") =  dbType
		POP_MVC.config("DB_PATH") = dbName
	End Property	

	'将 mysql 数据库转化成 access 数据库，只要结构不要数据，如果mysql中已经存在同名表，则不去创建
	'mysql的配置参数在配置中进行配置，并且要求mysql中已经有建好的数据库 DB_NAME
	Public Property Get toAcc4desc( byVal dbPath )
		toAcc4desc = P_("POPASP_DBCONVERT").mysql2acc4desc( db_name, dbPath ,"","")
	End Property
	
	'将 mysql 数据库转化成 sqlite3 数据库，只要结构不要数据，如果mysql中已经存在同名表，则不去创建
	'mysql的配置参数在配置中进行配置，并且要求mysql中已经有建好的数据库 DB_NAME
	Public Property Get toSqlite4desc( byVal dbPath )
		toSqlite4desc = P_("POPASP_DBCONVERT").mysql2sqlite4desc( db_name, dbPath ,"","")
	End Property
	
	'将 mysql 数据库转化成 access 数据库，结构连数据
	Public Property Get toAccWithData( byVal dbPath )
		toAccWithData = P_("POPASP_DBCONVERT").mysql2accWithData(db_name,dbPath,"","")
	End Property
	
	'将 mysql 数据库转化成 access 数据库，结构连数据
	Public Property Get toSqliteWithData( byVal dbPath )
		Call P_("POPASP_DBCONVERT").DropSqliteTables( dbPath )
		toSqliteWithData = P_("POPASP_DBCONVERT").mysql2sqliteWithData(db_name,dbPath,"","")
	End Property
	
	Public Function  toAccWithData4table( ByVal dbPath, ByVal mysql_prefix , ByVal access_prefix )
		dim table_name
		table_name = MID( tableName, len( db_prefix ) + 1 )
		Call P_("POPASP_DBCONVERT").TruncateAccTable( dbPath, table_name,access_prefix )
		toAccWithData4table = P_("POPASP_DBCONVERT").mysql2accWithData4table(table_name ,db_name , dbPath , mysql_prefix , access_prefix )
	End Function	

	Public Function  toSqliteWithData4table( ByVal dbPath, ByVal mysql_prefix , ByVal sqlite_prefix )
		dim table_name
		table_name = MID( tableName, len( db_prefix ) + 1 )
		Call P_("POPASP_DBCONVERT").TruncateSqliteTable( dbPath, table_name,sqlite_prefix )
		toSqliteWithData4table = P_("POPASP_DBCONVERT").mysql2sqliteWithData4table(table_name ,db_name , dbPath , mysql_prefix , sqlite_prefix )
	End Function	
End Class
%>