<%
' +----------------------------------------------------------------------
' | POPASP [ ASP MVC ]
' +----------------------------------------------------------------------
' | Copyright (c) 2016 http://popasp.com All rights reserved.
' +----------------------------------------------------------------------
' | Author: popasp <1737025626@qq.com>
' +----------------------------------------------------------------------
'功能尚在开发中……
Class POPASP_PAGECACHE
	public homePath	'前台路径，必须首先指定
	
	Public Function dir( arg )
		homePath = arg
		set dir = Me
	End Function
	
	Public Property Get cache( ByVal url , ByVal filename , ByVal id )
		dim content,sep,pos,path,dstname
		path = POP_MVC.rtrim( POP_MVC.rtrim( homePath , "/" ) , "\" )
		
		dstname = filename
		if id <> "" then
			dstname = dstname & "/" & POP_MVC.trim(POP_MVC.trim(id , "/" ) , "\")
		end if
		path = path & "/Runtime/Page/" & dstname & ".html"

		content = P_("POPASP_HTTP").get( url )
		sep = "<!--QQ_1737025626_POPASP_PAGE_TRACE_CACHE_QQ_GROUP_124648143-->"
		if len(content) > 0 then
			pos = instr( content,sep )
			if pos > 0 then
				content = left( content,pos-1 )
			end if
			content = POP_MVC.String.reg_replace(content , "" , "^\s+|\s+$" , "g" )
			Call POP_MVC.file_put_contents( path  , content )
		end if
	End Property
End Class
%>