<%
' +----------------------------------------------------------------------
' | POPASP [ ASP MVC ]
' +----------------------------------------------------------------------
' | Copyright (c) 2016 http://popasp.com All rights reserved.
' +----------------------------------------------------------------------
' | Author: popasp <1737025626@qq.com>
' +----------------------------------------------------------------------
Class POPASP_PAGETRACE
	public options
	private dataFileName,show_page_trace
	
	Private Sub Class_Initialize
		If IsEmpty( POP_MVC.config("TRACE_HANDLER_LINK") ) Then POP_MVC.config("TRACE_HANDLER_LINK") = "<a href='HREF' target='_blank' style='color:red;text-decoration:none;' title='TITLE'>CONTENT</a>"
		If IsEmpty( POP_MVC.config("TRACE_READ_SHOW") ) Then POP_MVC.config("TRACE_READ_SHOW") = "&#8801;"	
		show_page_trace = POP_MVC.config("SHOW_PAGE_TRACE")
	End Sub
	
	function handlerLink( action,param,title,content )
		dim href
		href = U_( array(POP_MVC.config("SYSTEM_MODULE") & "/" & action, param )  )
		handlerLink = POP_MVC.config("TRACE_HANDLER_LINK")
		handlerLink = replace( handlerLink,"HREF",href )
		handlerLink = replace( handlerLink,"TITLE",title )
		handlerLink = replace( handlerLink,"CONTENT",content )		
	end function
	
	
	'读取文件
	function readFile( file , title )		
		readFile = " " & handlerLink( "readFile","file=" & file ,title,POP_MVC.config("TRACE_READ_SHOW") )
	end function
	
	'读取popasp_class结构
	function readPOPASPClass( sClass  )	
		dim temp
		' if show_page_trace <> "file" then
			' readPOPASPClass = " " & handlerLink( "readPOPASPClass","class=" & sClass ,sClass," " & lcase(sClass) & " " )
		' else
			temp = sClass
			temp = lcase(POP_MVC.String.before(sClass,"*"))
			if inStr(temp , "self_object") > 0 then
				temp = "self_object"
			end if
			readPOPASPClass = " " & "<a title='点击查看popasp_" & temp & "类的详细使用说明' target='_blank' style='color:red;' href='http://www.popasp.com/?fileDetail/popasp_" & temp & "'>" & LCase(sClass) & "</a> "
		'end if
	end function
	
	sub show4file
		err.clear
		on error resume next
		dim tpl,content,fileName,pathName
		Call Assign()
		tpl = POP_MVC.mvc_dir & "Tpl/page_trace_file.html"
		
		content = POP_MVC.file_get_contents( tpl )
		'Randomize
		fileName = "./QQ_1737025626_POPASP_PAGE_TRACE_" & "" & "_QQ_GROUP_124648143.asp"
		
		
		
		pathName = Request.ServerVariables("SCRIPT_NAME")
		if Request.ServerVariables("QUERY_STRING") <> "" Then
			pathName = pathName & "?" & Request.ServerVariables("QUERY_STRING")
		end if

		'包含框架路径
		if not isEmpty( POP_MVC.page_trace_include ) then
			content = replace( content,"<!--__POPASP_INCLUDE__-->",POP_MVC.page_trace_include )
		elseif POP_MVC.String.StartsWith(POP_MVC.mvc_dir , "/" ) or POP_MVC.String.StartsWith(POP_MVC.mvc_dir , "\" ) then
			content = replace( content,"<!--__POPASP_INCLUDE__-->","<!--#include virtual=""" & POP_MVC.mvc_dir & "popasp.asp""-->" )		
		else
			content = replace( content,"<!--__POPASP_INCLUDE__-->","<!--#include file=""" & POP_MVC.mvc_dir & "popasp.asp""-->" )
		end if
		content = replace( content,"__APP_PATH__",POP_MVC.appPath )
		content = replace( content,"__MVC_DIR__",POP_MVC.mvc_dir )
		content = replace( content,"__DATA_PATH__",dataFileName )
		content = replace( content,"__ASP_PATH__",fileName )
		
		if POP_MVC.file_put_contents( fileName,content ) then
			Response.write "<!--QQ_1737025626_POPASP_PAGE_TRACE_CACHE_QQ_GROUP_124648143-->"
			Response.write "<QQ_1737025626_POPASP_PAGE_TRACE_CACHE_QQ_GROUP_124648143 id='QQ_1737025626_POPASP_HIDE_QQ_GROUP_124648143' style='display:none'><script type='text/x-popasp-template' style='display:none;'><![CDATA["

			Server.Execute fileName

			'call POP_MVC.file.remove(fileName)
			'call POP_MVC.file.remove(dataFileName)
		end if
	end sub
	
	sub show()
		if show_page_trace = "file" then
			call show4file
			exit sub
		end if
		err.clear
		on error resume next
		dim tpl,content,fileName
		Call Assign()
		tpl = POP_MVC.mvc_dir & "Tpl/page_trace.html"
		content = POP_MVC.file_get_contents( tpl )
		'Randomize
		fileName = "./QQ_1737025626_POPASP_PAGE_TRACE_" & "" & "_QQ_GROUP_124648143.asp"
		
		
		'包含框架路径
		if not isEmpty( POP_MVC.page_trace_include ) then
			content = replace( content,"<!--__POPASP_INCLUDE__-->",POP_MVC.page_trace_include )
		elseif POP_MVC.String.StartsWith(POP_MVC.mvc_dir , "/" ) or POP_MVC.String.StartsWith(POP_MVC.mvc_dir , "\" ) then
			content = replace( content,"<!--__POPASP_INCLUDE__-->","<!--#include virtual=""" & POP_MVC.mvc_dir & "popasp.asp""-->" )		
		else
			content = replace( content,"<!--__POPASP_INCLUDE__-->","<!--#include file=""" & POP_MVC.mvc_dir & "popasp.asp""-->" )
		end if
		content = replace( content,"__APP_PATH__",POP_MVC.appPath )
		content = replace( content,"__MVC_DIR__",POP_MVC.mvc_dir )
		content = replace( content,"__DATA_PATH__",dataFileName )
		if POP_MVC.file_put_contents( fileName,content ) then
			Response.write "<!--QQ_1737025626_POPASP_PAGE_TRACE_CACHE_QQ_GROUP_124648143-->"
			Response.write "<QQ_1737025626_POPASP_PAGE_TRACE_CACHE_QQ_GROUP_124648143 id='QQ_1737025626_POPASP_HIDE_QQ_GROUP_124648143' style='display:none'><script type='text/x-popasp-template' style='display:none;'><![CDATA["

			Server.Execute fileName

			'call POP_MVC.file.remove(fileName)
			'call POP_MVC.file.remove(dataFileName)
		end if
	end sub
	
	sub Assign()
		err.clear
		on error resume next
		dim base,temp,vars,key,sql,arr,keys,errors,i,item,matches,filePath,dict,totalSpent,url,dCMS
		set base = Server.CreateObject("Scripting.Dictionary")
		set vars = Server.CreateObject("Scripting.Dictionary")
		temp = Request.ServerVariables("SERVER_PROTOCOL") & " " &  Request.ServerVariables("REQUEST_METHOD") & " : "
		
		url = Request.ServerVariables("SCRIPT_NAME")
		if Request.ServerVariables("QUERY_STRING") <> "" Then
			url = url & "?" & Request.ServerVariables("QUERY_STRING")
		end if
		
		temp = temp & url
		
		if show_page_trace = "file" then
			temp = temp & "　IP:" & get_client_ip  & " Time:" & POP_MVC.dG_("REQUEST_TIME")
		end if
		
		base.add "请求信息",temp
		
		' 添加运行时间
		base.add "运行时间",G_( array("beginTime","viewEndTime") ) & "s ( Load:" & G_( array("beginTime","loadTime") ) & "s" & " action:" & G_( array("actionStartTime", "viewStartTime") )  & "s"  & " template:" & G_( array("viewStartTime", "viewEndTime") )  & "s ) " 
		
		' 添加使用到的类
		set dict = POP_MVC.Arr.toDict(POP_MVC.dClass.keys)
		set dict = POP_MVC.Dict.flip( dict )


		
		for each key in dict
			temp = POP_MVC.ltrim(key,"POPASP_")
			if temp <> key then
				dict(key) = readPOPASPClass( temp )
			else
				dict(key) = key
			end if
		next
		
		'
		base.add "P_" & " (" & (POP_MVC.dClass.Count + 1) & "个)",Replace(join(dict.items,","),"POPASP_","")  
		
		if POP_MVC.dPlugin.count > 0 then
			if POP_MVC.dPlugin.Count < 10 then
				base.add "T_" & "　(" & (POP_MVC.dPlugin.Count) & "个)",join( POP_MVC.dPlugin.keys," , " )
			else
				base.add "T_" & " (" & (POP_MVC.dPlugin.Count) & "个)",join( POP_MVC.dPlugin.keys," , " )
			end if
		end if
		
		' 数据库读取次数
		temp = POP_MVC.num_db_query & " queries "
		
		' 数据库写入次数
		temp = temp & POP_MVC.num_db_write & " writes"
		
		base.add "查询信息", temp
		
		temp = POP_MVC.dDict.count & " Dictionaries " ' 用到的 Dictionaries 个数
		temp = temp &  POP_MVC.num_recordset & " Recordsets"

		base.add "自动销毁",  temp
		
		' 配置项个数
		base.add "配置加载", POP_MVC.config.count
		
		' session
		base.add "会话信息", "Session.SessionID=" & Session.SessionID 
		
		' cookie 与 application
		base.add "Cookies",   P_("POPASP_COOKIE").Count & " Application: " & Application.Contents.Count & " Session: " & Session.Contents.Count
		
		
		' 数据库查询语句
		set sql = Server.CreateObject("Scripting.Dictionary")
		totalSpent = 0
		for each key in POP_MVC.dSql
			sql.add key,POP_MVC.dSql(key)("sql") & "; -- " &  POP_MVC.dSql(key)("time") & "ms"
			totalSpent = POP_MVC.dSql(key)("time") + totalSpent
		next
		
		if POP_MVC.dSql.count> 0 then
			POP_MVC.dict.unshift sql , 0 , "-- <font color='red'>" & POP_MVC.config("DB_TYPE") & "</font> 数据库 共执行 <font color='red'>" & POP_MVC.dSql.count & "</font> 次查询，耗时 <font color='red'>" & FormatNumber(totalSpent/1000,2,-1,0,0) & "</font> 秒，占总运行时间 <font color='red'>" & FormatNumber(( totalSpent/G_( array("beginTime","viewEndTime") )/10),2,-1,0,0) & "%</font>"
		end if
		

		' 错误信息
		set errors = Server.CreateObject("Scripting.Dictionary")
		
		set errors = POP_MVC.dict.merge( errors , POP_MVC.dL_ )
		
		for each key in POP_MVC.dTime
			item = POP_MVC.dTime(key)
			item = Replace( item , "\t" , "\\tt" )
			if POP_MVC.String.reg_test( item,"^(read|write)\s+file\s+(.*?)\s+" , "i" ) then
				set matches = POP_MVC.reg.Execute(item)
				filePath = matches(0).SubMatches(1)
				if show_page_trace <> "file" then
					item = item & readFile( filePath, "查看文件" )
				end if
			end if
			POP_MVC.dTime(key) = item
		next
		set matches = nothing
		vars.add "基本",base
		if sql.count > 0 then vars.add "SQL",sql
		vars.add "SCO",POP_MVC.dObj
		if POP_MVC.dAuto.count > 0 then vars.add "自动化",POP_MVC.dAuto
		if POP_MVC.dCMS.count > 0 then 
			totalSpent = 0 
			set dCMS = POP_MVC.dCMS
			for each key in dCMS
				temp = dCMS(key)
				temp = POP_MVC.String.reg_find( temp, "(\d+)ms" , 1 , "" )
				if temp <> "" then
					totalSpent = CInt( temp ) + totalSpent
				end if
			next
			POP_MVC.dict.unshift dCMS , -1 , "-- <font color='red'>" & POP_MVC.config("CMS_NAME") & "</font> 解析类 至少解析 <font color='red'>" & dCMS.count & "</font> 次，耗时 <font color='red'>" & FormatNumber(totalSpent/1000,2,-1,0,0) & "</font> 秒，占总运行时间 <font color='red'>" & FormatNumber(( totalSpent/G_( array("beginTime","viewEndTime") )/10),2,-1,0,0) & "%</font>"
			vars.add "CMS(" & dCMS.count & ")",dCMS
		end if
		if POP_MVC.dTpl.count > 0 then 	
			dim tplCount
			tplCount = 0
			for each key in POP_MVC.dTpl
				tplCount = tplCount + POP_MVC.dTpl(key)(0)
				POP_MVC.dTpl(key) = "共替换" & POP_MVC.dTpl(key)(0) & "次 耗时" & POP_MVC.dTpl(key)(1) & "ms"
			next
			POP_MVC.dTpl.add "合计" , tplCount & "次 总耗时" & P_("POPASP_TEMPLATE_COMPILER").runtime & "ms"
			vars.add "TPL",POP_MVC.dTpl
		end if
		vars.add "流程",POP_MVC.dFlow
		vars.add "性能(" & POP_MVC.dTime.count & ")",POP_MVC.dTime	
		if errors.count > 0 then
			vars.add "错误(" & errors.count & ")",errors
		end if
		
		vars.add "runtime",G_( array("beginTime","viewEndTime") ) & "s" 
		
		if show_page_trace = "file" then
			vars.add "url",url
		end if

		'Randomize
		dataFileName = "./QQ_1737025626_POPASP_PAGE_TRACE_" & "" & "_QQ_GROUP_124648143.dat"
		call POP_MVC.file_put_contents( dataFileName ,js_encode(vars) )	
		set base = nothing
		set vars = nothing
		set sql = nothing
		set errors = nothing
	end sub
End Class
%>