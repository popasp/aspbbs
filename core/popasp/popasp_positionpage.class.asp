<%
' +----------------------------------------------------------------------
' | POPASP [ ASP MVC ]
' +----------------------------------------------------------------------
' | Copyright (c) 2016 http://popasp.com All rights reserved.
' +----------------------------------------------------------------------
' | Author: popasp <1737025626@qq.com>
' +----------------------------------------------------------------------
Class POPASP_POSITIONPAGE	
	private total	'记录总个数	
	private position	'当前页
	private uri
	private parameter	'网址中的额外参数
	
	'下面是可配置的
	private noRecordTip		'无数据的提示
	private header	'"条记录"
	private first	'"首页"
	private prev	'"前进"
	private [next]	'"后退"
	private last	'"尾页"
	private seperator		'分隔符
	private jump	'页面跳转，如果不需要页面跳转，可以 Call oPage.setConfig( "jump" , "" )

	Private Sub Class_Initialize
		dim arr,pos
		header = "条记录"
		first = "首页"
		prev = "上一页"
		[next] = "下一页"
		last = "尾页"
		noRecordTip = "没有可载入的数据"
		seperator = "&nbsp;"

		position =  POP_MVC.Get( POP_MVC.config("VAR_POSITION") )
		
		if not isNumeric( position ) then
			position = 1
		end if
		
		if position < 1 then
			position = 1
		end if
		
		position = CLng( position )
 	End Sub
	
	Public Property Get config(attr)
		Execute "config = " & attr
	end Property
	
	Public Property Let config(attr,value)
		if LCase( attr ) = "parameter" then
			Call setParameter( value )
			Exit Property
		end if
		
		if isNumeric(value) then
			Execute attr & " = " & value
		else			
			Execute attr & " = """ & value & """"
		end if
	End Property
	
	Private Sub setParameter( arg )
		if typename( arg ) = "Dictionary" then	'Dictionary对象
			dim k1,k2
			parameter = ""
			for each k1 in arg
				if typename( arg(k1) ) = "Dictionary" then
					for each k2 in arg(k1)
						parameter = parameter & "&" & k1 & "=" & cstr( arg(k1)(k2) )
					next
				else
					parameter = parameter & "&" & k1 & "=" & cstr( arg(k1) ) 
				end if
			next
			parameter = POP_MVC.ltrim( parameter , "&" )
		else
			parameter = arg
		end if
	End Sub 
	
	'使用时有两种方法，一种是传入Recordset对象，一种是传入总数
	'1. that.assign "page" , P_("PAGE")(rs).show
	'2. that.assign "page" , P_("PAGE")( total ).show，total为总数
	'url其它参数传入时，可以使用P_("PAGE")( Array( rs/total, parameter ) )，parameter可以是字符串也可以是最多二维的Dictionary对象
	Public Default Function Constructor(byref args)
		dim rs
		if isArray(args) then
			if typename( args(0) ) = "Recordset" then
				set rs = args(0)
			else
				total = args(0)
			end if
			if ubound( args ) > 0 then
				Call setParameter( args(1) )
			end if
		else
			if typename( args ) = "Recordset" then
				set rs = args
			else
				total = args
			end if
			parameter = ""
		end if
		
		if not isEmpty( rs ) then
			total = rs.recordCount
		end if
		
		if position > total then
			position = total
		end if

		set Constructor = Me
	End Function
	
	'得到页码的链接html
	'如 <a href="clist.asp?ShowId=24&page=2">2</a>
	Public Function getUrl(dPos,sTxt)
		if isEmpty( uri ) then
			uri = SELF__
			if parameter <> "" AND inStr( POP_MVC.urldecode(uri),POP_MVC.urldecode(parameter) ) < 1 then
				if POP_MVC.Get("") = "" then
					uri = uri & "?" & parameter
				else
					uri = uri & "&" & parameter
				end if
			end if
			uri = POP_MVC.String.reg_replace(uri,"","&" & POP_MVC.config("VAR_POSITION") & "=[^&]+","ig")
			uri = POP_MVC.String.reg_replace(uri,"","\b" & POP_MVC.config("VAR_POSITION") & "=[^&]+","ig")

			if instr( uri , "?" ) > 1 then
				if inStr( uri , "&" ) > 1 or not POP_MVC.String.EndsWith( uri , "?" ) then
					uri = uri & "&"
				end if
			else
				uri = uri & "?"
			end if			
		end if
		getUrl = "<a href='" & uri & POP_MVC.config("VAR_POSITION") & "=" & dPos & "'>" & sTxt & "</a>"
	End Function

	'返回页面字符串
	'如果总数为0，则提示无数据
	'如果总页数不足一页，则输出空字符串
	Public Function Show()
		on error resume next
		dim nav,sFirst,sPrev,sNext,sLast,temp
		nav = array()
		if total < 1 Then
			Show = noRecordTip
			Exit Function
		end if
		
		if position > 1 then 
			if not is_empty(prev)   then POP_MVC.Arr.Unshift nav,getUrl(position-1,prev)
			if not is_empty(first) 	then POP_MVC.Arr.Unshift nav,getUrl(1,first)
		end if
		
		if position < total then
			if not is_empty([next]) then  POP_MVC.Arr.Push nav,getUrl( position+1,[next] )
			if not is_empty(last) 	then  POP_MVC.Arr.Push nav,getUrl( total,last )
		end if

		if not is_empty(header) then POP_MVC.Arr.Unshift nav,"共" & position & "/" & total & header
		
		temp = position
		if temp < total then
			temp = temp + 1
		elseif temp = total then
			temp = 1
		end if
		
		if total > 1 and isEmpty( jump ) then		
			jump = "　<input type='text' class='popasp_page_input_type' onkeydown='javascript:if(event.keyCode==13){var position=(this.value>" & total & ")?" & total & ":this.value;location="""& uri & POP_MVC.config("VAR_POSITION") & "=""+position;}' value='" & temp & "' /><input type='button' class='popasp_page_button_type' onclick='javascript:var position=(this.previousElementSibling.value>" & total & ")?"  & total & ":this.previousElementSibling.value;location="""  & uri & POP_MVC.config("VAR_POSITION") & "=""+position;'" & " value='GO' />"
			
			POP_MVC.Arr.Push nav,jump
		end if

		Show = "<div class='popasp_page'>" & join( nav,seperator ) & "</div>"
	End Function
End Class
%>