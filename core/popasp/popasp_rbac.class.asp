<%
' +----------------------------------------------------------------------
' | POPASP [ ASP MVC ]
' +----------------------------------------------------------------------
' | Copyright (c) 2016 http://popasp.com All rights reserved.
' +----------------------------------------------------------------------
' | Author: popasp <1737025626@qq.com>
' +----------------------------------------------------------------------
Class POPASP_RBAC

	'认证方法，返回Recordset对象
	Public Function authenticate( byval map, byval model )
		if is_empty( model ) then model = POP_MVC.config("USER_AUTH_MODEL")
		set authenticate = M_(model).db.where( map ).find()
	End Function

	'用于检测用户权限的方法,并保存到Session中
	Public Property Get saveAccessList( authID )
		if authID = "" then
			authID = session( POP_MVC.config("USER_AUTH_KEY") )
		end if
        ' 如果使用普通权限模式，保存当前用户的访问权限列表
        ' 对管理员开放所有权限

		if POP_MVC.config("USER_AUTH_TYPE") <> 2 AND is_empty( session(POP_MVC.config("ADMIN_AUTH_KEY")) ) then
			set session( "_ACCESS_LIST" ) = Me.getAccessList( authID )
		end if
	End Property

	'登录检查
	Public Property Get checkLogin
		'检查当前操作是否需要认证		
		if Me.checkAccess then
			'检查认证识别号
			if is_empty( session( POP_MVC.config("USER_AUTH_KEY") ) ) then
				if not is_empty( POP_MVC.config("GUEST_AUTH_ON") ) then
					
					'开启游客授权访问
					if is_empty( session( "_ACCESS_LIST" ) ) then
						Me.saveAccessList( POP_MVC.config("GUEST_AUTH_ID") )
					end if
				else
					'禁止游客访问跳转到认证网关
					Response.redirect( POP_MVC.config("USER_AUTH_GATEWAY") )
				end if
			end if
		end if
		
		checkLogin = true
	end Property
	
	'权限认证的过滤器方法
	Public Property Get accessDecision( ByVal appName )
		on error resume next
		if appName = "" then
			appName = POP_MVC.config("APP_NAME")
		end if
		if Me.checkAccess then
			dim accessGuid,accessList,key
			accessGuid = appName & "-" & POP_MVC.c & "-" & POP_MVC.a
			if is_empty( session( POP_MVC.config("ADMIN_AUTH_KEY") ) ) then
				
				if POP_MVC.config( "USER_AUTH_TYPE" ) = 2 then
                    '加强验证和即时验证模式 更加安全 后台权限修改可以即时生效
                    '通过数据库进行访问检查
					set accessList = Me.getAccessList( session( POP_MVC.config("USER_AUTH_KEY") ) )
				else
					'如果是管理员或者当前操作已经认证过，无需再次认证
					if not is_empty( session( accessGuid ) ) then
						accessDecision = true : Exit Property
					end if
					'登录验证模式，比较登录后保存的权限访问列表
					set accessList = session("_ACCESS_LIST")
				end if
				
				'判断是否为组件化模式，如果是，验证其全模块名
				key = UCase( appName ) & "." & UCase( POP_MVC.c ) & "." & UCase( POP_MVC.a )
				if not POP_MVC.Dict.KeyExists(accessList , key , "." ) then
					session( accessGuid ) = false : accessDecision = false : exit Property
				else
					session( accessGuid ) = true
				end if
			else
				accessDecision = true
			end if			
		end if
		accessDecision = true
	End Property
	
	'检查当前操作是否需要认证
	Public Property Get checkAccess()
		if not is_empty( POP_MVC.config("USER_AUTH_ON") ) then
			dim yesCtrl,yesAction,noCtrl,noAction
			yesCtrl = Array()	:	yesAction = Array()
			noCtrl = Array()	:	noAction = Array()
			
			if ("" <> POP_MVC.config( "REQUIRE_AUTH_MODULE" )) then
				yesCtrl = split( UCase(POP_MVC.config("REQUIRE_AUTH_MODULE")) ,"," )
			else
				noCtrl = split( UCase(POP_MVC.config("NOT_AUTH_MODULE")) ,"," )
			end if
			'检查当前模块是否需要认证			
			if not POP_MVC.Arr.Exists( noCtrl , UCase( POP_MVC.c )) OR POP_MVC.Arr.Exists( yesCtrl , UCase( POP_MVC.c ) ) then

				if "" <> POP_MVC.config("REQUIRE_AUTH_ACTION") then
					yesAction = split( UCase( POP_MVC.config("REQUIRE_AUTH_ACTION")  ), "," )
				else
					noAction = split( UCase( POP_MVC.config("NOT_AUTH_ACTION")  ) , ",")
				end if

				
				'检查当前操作是否需要认证
				if not POP_MVC.Arr.Exists( noAction , UCase( POP_MVC.a ) ) OR POP_MVC.Arr.Exists( yesAction , UCase( POP_MVC.a ) ) then
					checkAccess = True : exit Property
				else
					checkAccess = False : exit Property
				end if
			else
				
				checkAccess = False : exit Property
			end if			
		end if
		checkAccess = False
	End Property

	'返回Dictionary对象
	Public Function getAccessList( ByVal authID )
		dim access	'接收器，Dictionary对象
		dim sql,rs,key
		dim tblRole,tblUser,tblAccess,tblNode		
		tblRole =	POP_MVC.config( "RBAC_ROLE_TABLE" )
		tblUser =	POP_MVC.config( "RBAC_USER_TABLE" )
		tblAccess =	POP_MVC.config( "RBAC_ACCESS_TABLE" )
		tblNode	=	POP_MVC.config( "RBAC_NODE_TABLE" )

		'项目 1，控制器 2，操作 3
		'level = 1
		'status为状态，如果数据库设为布尔值，那么Access数据库中为真时为-1，假时为0，
		sql = "select tbl_node.id,tbl_node.name from " & tblRole & " as tbl_role," & tblUser & " as tbl_user," & tblAccess & " as tbl_access ," & tblNode & " as tbl_node where tbl_user.user_id=" & authID & " and tbl_user.role_id=tbl_role.id and ( tbl_access.role_id=tbl_role.id or (tbl_access.role_id=tbl_role.pid and tbl_role.pid<>0 ) ) and tbl_role.status and tbl_access.node_id=tbl_node.id and tbl_node.level=1 and tbl_node.status"
		set access = D_
		
		dim appList,appKey,appID,appName
		dim ctrlList,ctrlKey,ctrlID,ctrlName
		dim actList,actKey,actID,actName
		dim action,publicAction
		dim arr
		arr = Array()
		if not is_empty( POP_MVC.config( "NOT_AUTH_MODULE" ) ) then
			arr = split( POP_MVC.config( "NOT_AUTH_MODULE" ) , ","  )
		end if
		
		set rs = M_( tblRole ).db.getRS( sql ) : set appList = POP_MVC.rs2dict( rs ) : Me.unset( rs )
		
		for each appKey in appList
			appID = appList( appKey )( "id" )
			appName = appList( appKey )( "name" )
			set access( UCase( appName ) ) = D_
			
			'level=2对应控制器
			sql = "select tbl_node.id,tbl_node.name from " & tblRole & " as tbl_role," & tblUser & " as tbl_user," & tblAccess & " as tbl_access ," & tblNode & " as tbl_node where tbl_user.user_id=" & authID & " and tbl_user.role_id=tbl_role.id and ( tbl_access.role_id=tbl_role.id or (tbl_access.role_id=tbl_role.pid and tbl_role.pid<>0 ) ) and tbl_role.status and tbl_access.node_id=tbl_node.id and tbl_node.level=2 and tbl_node.pid = " & appID & " and tbl_node.status"
			
			
			set rs = M_( tblRole ).db.getRS( sql ) : set ctrlList = POP_MVC.rs2dict( rs ) : Me.unset( rs )			

			
			'判断是否存在公共模块的权限
			set publicAction = D_
			for each ctrlKey in ctrlList
				ctrlID = ctrlList( ctrlKey )( "id" )
				ctrlName = ctrlList( ctrlKey )( "name" )
				if POP_MVC.Arr.iExists( arr, ctrlName ) then					
					sql = "select tbl_node.id,tbl_node.name from " & tblRole & " as tbl_role," & tblUser & " as tbl_user," & tblAccess & " as tbl_access ," & tblNode & " as tbl_node where tbl_user.user_id=" & authID & " and tbl_user.role_id=tbl_role.id and ( tbl_access.role_id=tbl_role.id or (tbl_access.role_id=tbl_role.pid and tbl_role.pid<>0 ) ) and tbl_role.status and tbl_access.node_id=tbl_node.id and tbl_node.level=3 and tbl_node.pid = " & ctrlID & " and tbl_node.status"
					
					
					set rs = M_( tblRole ).db.getRS( sql ) : set actList = POP_MVC.rs2dict( rs ) : Me.unset( rs )
					for each key in actList
						publicAction( UCase(actList(key)("name")) ) = actList(key)("id")
					next
					ctrlList.remove( ctrlKey )
				end if
			next
				
			'依次读取模块的操作权限
			for each ctrlKey in ctrlList
				ctrlID = ctrlList( ctrlKey )( "id" )
				ctrlName = ctrlList( ctrlKey )( "name" )
					
				sql = "select tbl_node.id,tbl_node.name from " & tblRole & " as tbl_role," & tblUser & " as tbl_user," & tblAccess & " as tbl_access ," & tblNode & " as tbl_node where tbl_user.user_id=" & authID & " and tbl_user.role_id=tbl_role.id and ( tbl_access.role_id=tbl_role.id or (tbl_access.role_id=tbl_role.pid and tbl_role.pid<>0 ) ) and tbl_role.status and tbl_access.node_id=tbl_node.id and tbl_node.level=3 and tbl_node.pid = " & ctrlID & " and tbl_node.status"
					
				set rs = M_( tblRole ).db.getRS( sql ) : set actList = POP_MVC.rs2dict( rs ) : Me.unset( rs )
				
				set action = D_
				for each key in actList
					action( UCase(actList(key)("name")) ) = actList(key)("id")
				next
				set action = POP_MVC.Dict.Merge( action,publicAction )
				set actList = nothing			
				
				set access( UCase( appName ) )( UCase( ctrlName ) ) = action
				set action = nothing
			next			
		next
		POP_MVC.unset( appList ) : POP_MVC.unset( ctrlList ) : POP_MVC.unset( actList ) : POP_MVC.unset( publicAction )
		
		set getAccessList = access
	End Function
	
	'读取模块(控制器)所属的记录访问权限
	Public Property Get getModuleAccessList( ByVal authID ,ByVal ctrl )
		dim access	'接收器，Dictionary对象
		dim sql,rs
		tblRole =	POP_MVC.config( "RBAC_ROLE_TABLE" )
		tblUser =	POP_MVC.config( "RBAC_USER_TABLE" )
		tblAccess =	POP_MVC.config( "RBAC_ACCESS_TABLE" )
		tblNode	=	POP_MVC.config( "RBAC_NODE_TABLE" )
		
		'项目 1，控制器 2，操作 3
		'level = 1
		'status为状态，如果数据库设为布尔值，那么Access数据库中为真时为-1，假时为0，
		sql = "select tbl_node.id,tbl_node.name from " & tblRole & " as tbl_role," & tblUser & " as tbl_user," & tblAccess & " as tbl_access ," & tblNode & " as tbl_node where tbl_user.user_id=" & authID & " and tbl_user.role_id=tbl_role.id and ( tbl_access.role_id=tbl_role.id or (tbl_access.role_id=tbl_role.pid and tbl_role.pid<>0 ) ) and tbl_role.status and tbl_access.module = '" & ctrl & "'"
		set rs = M_( tblRole ).db.getRS( sql ) : access = array()
		do while not rs.eof
			POP_MVC.Arr.push access,rs("node_id")
		loop
		Me.unset( rs )
		getModuleAccessList = access
	End Property
	
	Public Property Get unset( byref arg )
		Call POP_MVC.unset( arg )
	End property
	
	
	'将一个控制器文件夹中的所有控制器分析一遍
	Public Function ParseApp( dir )
		dim fd,fc,f1,dict,ret,oPage,i,arr
		'创建filesystemobject对象并得到文件夹对象
		
		set fd = POP_MVC.fso.getFolder( POP_MVC.realpath( dir ) )
		
		set fc = fd.files
		
		i = 0
		set dict = D_
		for each f1 in fc
			if POP_MVC.String.iEndsWith( f1.name, ".asp" ) then
				dict( POP_MVC.file.basename( array(f1.name,".asp") ) ) = ParseMethod( f1.path )
			end if
			i = i + 1
		next		
		set fc = nothing
		set fd = nothing
		set ParseApp = dict
	End Function
	
	' 从类的文本内容中获取解析后的方法，只含Public方法
	' 返回的是Dictionary对象。{"private_method":{"小写的方法名称":"Function或者Sub"},"public_method":{...}}
	Public Function ParseMethod( path )
		dim content
		content = POP_MVC.file_get_contents( path )		
		ParseMethod = ParseMethod_( "^\s*(?:public)?(?:\s+default)?\s+(sub|function|property\s+let|property\s+get|property\s+set)\s+\[?(\w+)\]?\s*(''.+)?" ,content  )
	End Function
	
	'解析类中的方法，pattern为正则匹配，content为类的文本内容
	Private Function ParseMethod_( byref pattern ,byref content ) 'className,method
		dim matches,i,name,title		
		if POP_MVC.String.reg_test(content,pattern,"gim") Then
			set matches = POP_MVC.reg.Execute ( content )
			for i = 0 to matches.count-1
				name = matches(i).submatches(1)
				title = POP_MVC.string.reg_Replace( mid(matches(i).submatches(2),3),""  ,  "^\s+|\s+$","g")
				if title = "" then
					title = name
				end if
				POP_MVC.Arr.push arr,array( name,title )
			next
			set matches = nothing
			ParseMethod_ = arr
		else
			ParseMethod_ = Array()
		End If
	End Function
End Class
%>