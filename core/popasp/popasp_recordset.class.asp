<%
' +----------------------------------------------------------------------
' | POPASP [ ASP MVC ]
' +----------------------------------------------------------------------
' | Copyright (c) 2016 http://popasp.com All rights reserved.
' +----------------------------------------------------------------------
' | Author: popasp <1737025626@qq.com>
' +----------------------------------------------------------------------

'处理RECORDSET的类
Class POPASP_RECORDSET
	'获取rs的字段名，返回数组
	Function getFields( ByRef rs )
		dim arr
		arr = array()
		if Not rs.BOF And Not rs.EOF then	
			for i = 0 to rs.fields.count-1
				POP_MVC.Arr.push arr,rs.fields(i).name
			next
		end if
		getFields = arr
	End Function
	
	
	'将查询数据下载为Excel表格
	'style为数组或用英文逗号分隔的字符串，代表各字段类型
	'filename为文件名，如果给""，则取当前表名
	'1） 文本(text,t)：vnd.ms-excel.numberformat:@
	'2） 日期(date,d)：vnd.ms-excel.numberformat:yyyy/mm/dd hh:mm:ss
	'3） 数字(number,n)：vnd.ms-excel.numberformat:#,##0.00
	'4） 货币(currency,c,￥,$)：vnd.ms-excel.numberformat:￥#,##0.00
	'5） 百分比(percent,p)：vnd.ms-excel.numberformat: #0.00%	
	'style为数组，分别对应各字段的类型，默认空字符串""为文本类型
	'rs需要生成excel的recordset对象，isAccess是否为access数据库(True/False)
	Public Sub Excel( rs , style , filename , isAccess )
		Server.ScriptTimeOut=9999999
		dim uniqname,arr
		on error resume next
		POP_MVC.config("SHOW_PAGE_TRACE") = 0
		
		'如果要调试，请注释掉下面两行代码
		Response.ContentType = "application/excel"
		Response.AddHeader "Content-Disposition", "attachment;filename="""& filename &".xls"""
		
		dim html,i,k1,k2,item,j,temp	'i用来计算行数,j计算列数
		
		html = "<html>" & VbCrLf
		html = html & "<head>" & VbCrLf
		html = html & "<meta name=ProgId content=Excel.Sheet>" & VbCrLf
		html = html & "<style>" & VbCrLf
		html = html & "table,th,td{border:0.5pt solid black;}" & VbCrLf
		html = html & "</style>" & VbCrLf
		html = html & "</head>" & VbCrLf
		html = html & "<body>" & VbCrLf
		html = html & "<table>"		
		i = 0	
		do while not rs.eof
			j = 0
			if i = 0 then
				html = html & VbCrLf & vbTab & "<tr>"
				html = html & VbCrLf & vbTab & vbTab
				
				for k2 = 0 to rs.fields.count-1
					html = html & "<th style='vnd.ms-excel.numberformat:@'>" & rs.Fields(k2).Name & "</th>"
				next				

				html = html & VbCrLf & vbTab & "</tr>"
				
				if not isArray(style) then
					style = split( style,"," )
				end if
				
				for j = 0 to ubound( style )
					if style(j) = "" then
						style(j) = "number"
					end if
				next
				
				for j = (ubound(style) + 1) to rs.fields.count
					POP_MVC.Arr.push style,"number"
				next
				
				if rs.fields.count > ubound( style ) + 1 then	'如果类型数组中的元素个数小于字段个数，则用数字类型代替
					temp = style( ubound(style) )
					for j = rs.fields.count - 1  to ubound( style ) step -1
						POP_MVC.Arr.Push style,temp
					next
				end if
			end if	
			html = html & VbCrLf & vbTab & "<tr>"
			html = html & VbCrLf & vbTab & vbTab
			j = 0

			for k2 = 0 to rs.fields.count-1
				if not isAccess then
					err.clear
					call typename( rs(rs.fields(k2).name).value )

					if err.number <> 0 then	'自增ID产生的bug
						err.clear
						if typename(rs.Fields(k2).Value) = "Long" then
							item = CLng(rs.Fields(k2).Value)
						elseif isNumeric(cstr(rs.Fields(k2).Value)) then
							item = CLng( CStr(rs.Fields(k2).Value) )
						else
							item = rs.Fields(k2).Value
						end if	
					else
						item = rs.Fields(k2).Value
					end if	
				else
					item = rs.Fields(k2).Value
				end if
			
				select case LCase(style(j))
					case "text","t"
						html = html & "<td style='vnd.ms-excel.numberformat:@'>" & item & "</td>"
					case "date","d"
						html = html & "<td style='vnd.ms-excel.numberformat:yyyy/mm/dd hh:mm:ss'>" & item & "</td>"
					case "number","n"
						'html = html & "<td style='vnd.ms-excel.numberformat:#,##0.00'>" & item & "</td>"
						html = html & "<td style='vnd.ms-excel.numberformat'>" & item & "</td>"
					case "currency","c","￥"
						html = html & "<td style='vnd.ms-excel.numberformat:￥#,##0.00'>" & item & "</td>"
					case "currency","c","$"
						html = html & "<td style='vnd.ms-excel.numberformat:$#,##0.00'>" & item & "</td>"
					case "percent","p"
						html = html & "<td style='vnd.ms-excel.numberformat: #0.00%	'>" & item & "</td>"
				End Select
				j = j + 1
			next
			html = html & VbCrLf & vbTab & "</tr>"
			i = i + 1
			rs.movenext
			
			if i > 0 and i mod 1000 = 0 then
				uniqname = POP_MVC.String.Uniqid & ".html"
				uniqname = POP_MVC.String.rtrim(POP_MVC.appPath , "/" ) & "/Runtime/" & uniqname
				Call POP_MVC.file_put_contents( uniqname, html )
				html = ""
				POP_MVC.Arr.push arr,uniqname
			end if
		'next	
		loop
		
		html = html & VbCrLf & "</table>"
		html = html & VbCrLf & "</body>"
		html = html & VbCrLf & "</html>"
		
		if not isEmpty( uniqname ) then
			Call POP_MVC.file_append_contents( uniqname, html )
		end if	

		if isArray( arr ) then
			for i = 0 to ubound(arr)
				response.write POP_MVC.file_get_contents( arr(i) )
				POP_MVC.file.remove( arr(i) )
			next
		else
			Response.write html
		end if		
	End Sub
	
	Function rs2xml( ByRef rs ,ByRef packageName , ByRef needNull )
		on error resume next
		dim i,j,start,temp,str,ConfigValue,ConfigName,ret
		
		'if Not rs.BOF then
		'	rs.moveFirst
		'end if
		if not is_empty( POP_MVC.config("SHOW_PAGE_TRACE") ) Then
			start = timer()	
		end if

		j = 0
		ret = ""
		Do While Not rs.BOF And Not rs.EOF		
			if j = POP_MVC.config("RS_2DICT_LIMIT") then
				Exit DO
			end if
			
			str = ""
			for i = 0 to rs.fields.count-1
				configName = rs.fields(i).name				
				ConfigValue = rs(rs.fields(i).name)
				ConfigValue =repnull(ConfigValue)
				if needNull or  not isNul(configValue) then
					str = str & "<" & ConfigName & ">" 
					if inStr( ConfigValue , "<" ) > 0 or inStr( ConfigValue , ">" ) > 0 or inStr( ConfigValue , "&" ) > 0 then
						str = str & "<![CDATA[" & ConfigValue & "]]>"
					elseif configValue = true then
						str = str & 1
					elseif configValue = false then 
						str = str & 0
					else
						str = str & configValue
					end if
					str = str & "</" & ConfigName & ">"
				end if
			next
			ret = ret & "<" & packageName & ">"  & str  &   "</" & packageName & ">"
			j = j + 1
			rs.MoveNext
		Loop

		rs2xml = ret
		Call pushDebug( rs, start ,  "Recordset to xml" )
	End Function

	Function rs2data( ByRef rs ,prikey , isAccess , isUsePage )
		on error resume next
		dim i,j,dict,key,item,start,sql,bool,temp
		
		'if Not rs.BOF then
		'	rs.moveFirst
		'end if
		
		set rs2data = D_
		
		if not is_empty( POP_MVC.config("SHOW_PAGE_TRACE") ) Then
			start = timer()	
		end if

		j = 0
		
		'如果是mysql数据库，自增ID的数据类型不被支持，产生如下错误
		'变量使用了一个 VBScript 中不支持的 Automation 类型				
		if not isaccess then			
			Do While Not rs.BOF And Not rs.EOF
				if isUsePage = 1 and j = rs.pagesize then
					exit do
				end if
				if j = POP_MVC.config("RS_2DICT_LIMIT") then
					Exit DO
				end if
				set dict = Server.CreateObject("Scripting.Dictionary")
				
				for i = 0 to rs.fields.count-1
					err.clear
					call typename( rs(rs.fields(i).name).value )

					if err.number <> 0 then	'自增ID产生的bug
						err.clear
						if typename(rs.Fields(i).Value) = "Long" then
							dict(rs.Fields(i).Name) = CLng(rs.Fields(i).Value)
						elseif isNumeric(cstr(rs.Fields(i).Value)) then
							dict(rs.Fields(i).Name) = CLng( CStr(rs.Fields(i).Value) )
						else
							dict(rs.Fields(i).Name) = rs.Fields(i).Value
							'POP_MVC.Exit( "发现了不能被ASP解析的数据类型，请联系POPASP作者，以解决此BUG" )
						end if	
					else
						dict(rs.Fields(i).Name) = rs.Fields(i).Value
					end if
				next
				err.clear
				
				if prikey = "" then
					set data(j) = dict 
					rs2data.add j,dict
				else
					temp = rs(prikey).value
					rs2data.add temp,dict
				end if
				rs.MoveNext
				j = j+1
			Loop						
		else
			Do While Not rs.BOF And Not rs.EOF
				if isUsePage = 1 and j = rs.pagesize then
					exit do
				end if
			
				if j = POP_MVC.config("RS_2DICT_LIMIT") then
					Exit DO
				end if
				set dict = Server.CreateObject("Scripting.Dictionary")
				
				for i = 0 to rs.fields.count-1
					dict(rs.Fields(i).Name) = rs(rs.fields(i).name)
				next
				
				if prikey = "" then
					rs2data.add j,dict
				else	
					temp = rs(prikey).value
					rs2data.add temp,dict
				end if
				rs.MoveNext
				j = j+1
			Loop
		end if
		Call pushDebug( rs, start ,  "Recordset to Dictionary" )
	End Function
	
	Public Sub pushDebug( rs , start , info ) 
		dim sql
		if not is_empty( POP_MVC.config("SHOW_PAGE_TRACE") ) Then
			set sql = Server.CreateObject("Scripting.Dictionary")
			sql("time") = round((timer() - start) * 1000,0)
			sql("sql") = rs.Source & " ; -- " & info
			set POP_MVC.dSql(POP_MVC.dSql.count + 1) = sql
			set sql = nothing
		end if	
	End Sub
	
	'获取字符值,如果fields为""，则取field方法fieldRev中的值
	'只将当前游标对应的rs转成成一条记录的Dictionary对象
	Public Function rs2dict( ByRef rs,isAccess )
		on error resume next
		dim dict,i,sql,start
		set dict = Server.CreateObject("Scripting.Dictionary")
		if not is_empty( POP_MVC.config("SHOW_PAGE_TRACE") ) Then
			start = timer()	
		end if
		if not rs.BOF and not rs.EOF then
			for i = 0 to rs.fields.count-1			
				if not isAccess then
					Call typename( rs.Fields(i).Value )
					if err.number <> 0 then	
						err.clear
						if typename(rs.Fields(i).Value) = "Long" then
							dict(rs.Fields(i).Name) = CLng(rs.Fields(i).Value)
						else
							dict(rs.Fields(i).Name) = rs.Fields(i).Value
							'POP_MVC.Exit( "发现了不能被ASP解析的数据类型，请联系POPASP作者，以解决此BUG" )
						end if							
					else
						dict( rs.Fields(i).Name ) = rs.Fields(i).Value
					end if						
				else
					dict( rs.Fields(i).Name ) = rs.Fields(i).Value
				end if
			next
			err.clear
		end if
		set rs2dict = dict
		set dict = nothing
		Call pushDebug( rs, start ,  "Recordset to Dictionary" )
	End Function
	
	'将第1个字段作为键名，将第2个字段的值作为单一值形成的Dictionary对象
	Function rs2dict2( ByRef rs )
		dim start,sql
		
		set rs2dict2 = Server.CreateObject("Scripting.Dictionary")
		
		if not is_empty( POP_MVC.config("SHOW_PAGE_TRACE") ) Then
			start = timer()	
		end if
			j = 0
		Do While Not rs.BOF And Not rs.EOF
			'如果值为Null，则转为空
			if isNull(  rs.Fields(1).value ) then
				rs2dict2.add cstr(rs.Fields(0).Value), ""
			else
				rs2dict2.add cstr(rs.Fields(0).Value), rs.Fields(1).value
			end if
			rs.MoveNext
		Loop	
		Call pushDebug( rs, start ,  "Recordset to Dictionary" )
	End Function
	
	'将主键值作为键名，将第2个字段的值作为单一值形成的object对象
	Function rs2object( ByRef rs , isAccess )
		dim start,sql,str,str2,stype,dict,temp,className,i,obj,temp2,arr,bool,str3
		
		if rs.eof then
			set rs2object = Nothing
			exit Function
		end if
		
		if not is_empty( POP_MVC.config("SHOW_PAGE_TRACE") ) Then
			start = timer()	
		end if	

		className = POP_MVC.config("POPASP_SELF_OBJECT_PREFIX") & POP_MVC.dClass.count
		str = "Class " & className & vbcrlf	
	
		if not rs.BOF and not rs.EOF then
			str2 = ""
			temp = ""
			temp2 = ""
			str = str & "Public vars__" & vbcrlf
			str = str & "Public "
			'通过find来获取的，将字段名作为属性名
			if POP_MVC.String.reg_test( rs.Source , "^\s*SELECT\s+TOP\s+1\s+.+|^\s*SELECT\s+.*?LIMIT\s+1\s*;?$", "i" ) then
				bool = true
				for i = 0 to rs.fields.count-1	
							temp = temp & "[" & rs.Fields(i).Name & "],"
							temp2 =  temp2 & """" & rs.Fields(i).Name & ""","			
					if not isAccess then
						Call typename( rs.Fields(i).Value )
						if err.number <> 0 then		
							err.clear
							if typename(rs.Fields(i).Value) = "Long" then
								str2 = str2 & "[" & rs.Fields(i).Name & "]=" & "CLng(rs.Fields(" & i & ").Value)" & vbcrlf
							else
								str2 = str2 & "[" & rs.Fields(i).Name & "]=" & "rs.Fields(" & i & ").Value" & vbcrlf
								'POP_MVC.Exit( "发现了不能被ASP解析的数据类型，请联系POPASP作者，以解决此BUG" )
							end if							
						else
							str2 = str2 & vbTab & "[" & rs.Fields(i).Name & "]=" & "rs.Fields(" & i & ").Value" & vbcrlf
						end if						
					else
						str2 = str2 & vbTab & "[" & rs.Fields(i).Name & "]=" & "rs.Fields(" & i & ").Value" & vbcrlf
					end if
				next
				err.clear
			else
			'通过select来获取的，将记录的第一个值作为属性名，将记录第二个值作为值
				bool = false
				i = 0
				do while not rs.eof
					temp = temp & "[" & rs.Fields(0).value & "],"
					temp2 =  temp2 & """" & rs.Fields(0).value & ""","			
					POP_MVC.arr.push arr,rs.Fields(1).Value	
					str2 = str2 & vbTab & "[" & rs.Fields(0).value & "]=" & "arr__(" & i & ")" & vbcrlf
					i = i + 1
					rs.movenext
				loop
			end if

			temp = left( temp , len(temp) - 1 )
			temp2 = left( temp2 , len(temp2) - 1 )
			str = str & temp & vbcrlf
			if bool then
				str = str & "Public Sub Init__(ByRef rs)" & vbcrlf
			else
				str = str & "Public Sub Init__(ByRef arr__)" & vbcrlf
			end if 
			str = str & vbTab & "vars__ = Array(" & temp2 & ")" & vbcrlf
			str = str & str2
			str = str & "End Sub" & vbcrlf
			str = str & "Property Get Exists__(ByRef key__)" & vbcrlf
			str = str & vbTab & "Exists__=POP_MVC.Arr.iExists(vars__,key__)"  & vbcrlf
			str = str & "End Property" & vbcrlf
			str = str & "Property Get Get__(ByRef key__)" & vbcrlf
			str = str & vbTab & "Ex" & Chr("101") & "cute(""Get__ = "" &  key__ )"  & vbcrlf
			str = str & vbTab & "if isNull(Get__) Then Get__ = """""  & vbcrlf
			str = str & "End Property" & vbcrlf
		end if	
		str = str & "End Class" & vbcrlf
		


		'var_Export str : response.end
		execute( str )
		str3 = "set POP_MVC.dClass(className) = NEW " & className
		execute( str3 )
		if bool then
			POP_MVC.dClass(className).init__(rs)
		else
			POP_MVC.dClass(className).init__(arr)
		end if
		set rs2object = POP_MVC.dClass(className)

		Call pushDebug( rs, start ,  "Recordset to " & className )
	End Function
	

End Class
%>