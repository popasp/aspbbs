<%
' +----------------------------------------------------------------------
' | POPASP [ ASP MVC ]
' +----------------------------------------------------------------------
' | Copyright (c) 2016 http://popasp.com All rights reserved.
' +----------------------------------------------------------------------
' | Author: popasp <1737025626@qq.com>
' +----------------------------------------------------------------------
' 反射类，用于分析 POPASP_XXX文件或者 XXXAction控制器文件或者 XXXMODEL 数据模型文件的变量、方法、继承
Class POPASP_REJECTION
	private reg
	
	' 判断className类中是否有variable属性
	Public Function Variable_Exists( className,variable )
		dim dict
		set dict = Parse( className )
		Variable_Exists  = dict("public_vars").Exists( LCase(variable) )
	End Function
	
	' 判断className类中是否有method方法
	Public Function Method_Exists( ByVal className,ByVal method )
		dim dict
		set dict = Parse( className )
		Method_Exists  = POP_MVC.Arr.iExists(dict("public_method") , method )
	End Function
	
	
	' 解析类的变量与方法及继承关系
	' 如果要解析Model的话，className = Array( tableName , db_type , db_name/db_path )
	Public Function ParseStr( ByVal content , ByVal className )
		on error resume next
		dim rejFile,dict,classKey
		classKey = LCase( className )
		if POP_MVC.dRejection.Exists( classKey ) Then
			Set ParseStr =  POP_MVC.dRejection(classKey)
		Else
			rejFile = className		

			rejFile = POP_MVC.appPath & "/Runtime/Class/" & rejFile & ".rejection.asp"
			
			if POP_MVC.file.isExists( rejFile ) AND is_empty(POP_MVC.config("APP_DEBUG")) Then				
				set dict = Server.CreateObject("Scripting.Dictionary")
				on error resume next
				Execute(POP_MVC.file_get_contents(rejFile))
				set ParseStr = dict
				set POP_MVC.dRejection( classKey ) = ParseStr				
			Else				
				set dict = ParseContent( content )
				
				set ParseStr = dict		
				call saveParse(rejFile,dict)
				set POP_MVC.dRejection( classKey ) = ParseStr
				set dict = nothing
			End If		
		End If
		
		Call L_("POPASP_REJECTION.ParseStr")		
	End Function

	' 解析类的变量与方法及继承关系
	' 如果要解析Model的话，className = Array( tableName , db_type , db_name/db_path )
	Public Function Parse( ByVal className )
		on error resume next
		

		
		dim content,m,modelFile,rejFile,dict,dstname,srcname
		if POP_MVC.dRejection.Exists( className ) Then
			Set Parse =  POP_MVC.dRejection(className)
		Else
			rejFile = className		

			if NOT POP_MVC.isPopasp(className) AND NOT POP_MVC.isAction(className) Then
				if isArray( className ) then
					rejFile = POP_MVC.getModelRunName(className) & "Model"
				else
					rejFile = className
				end if
			End If		

			rejFile = POP_MVC.appPath & "/Runtime/Class/" & rejFile & ".rejection.asp"
			
			if POP_MVC.file.isExists( rejFile ) AND is_empty(POP_MVC.config("APP_DEBUG")) Then				
				set dict = Server.CreateObject("Scripting.Dictionary")
				on error resume next
				Execute(POP_MVC.file_get_contents(rejFile))
				set Parse = dict
				set POP_MVC.dRejection( className ) = Parse				
			Else
				if POP_MVC.isPopasp( className ) Then	'核心类					
					content = POP_MVC.get4asp(className)		
				Elseif POP_MVC.isAction(className) Then	'控制器类						
					if className = POP_MVC.config("SYSTEM_MODULE") & "Action" Then
						content = POP_MVC.asp_get_contents( POP_MVC.mvc_dir & "Tpl/" & className & ".class.asp" )
					else	
						dstname = POP_MVC.appPath & "/Runtime/Class/" & left(className , len(className)-6 ) & "Action.class.asp"						
						srcname = POP_MVC.appPath & "/Controller/" & left(className , len(className)-6 ) & ".asp" 
						
						If POP_MVC.file.isExists( dstname ) then
							If DateDiff("s" , POP_MVC.File.mtime( srcname ) ,POP_MVC.File.mtime( dstname ) ) >= 0 then
								content = POP_MVC.asp_get_contents( dstname )
							else
								content = POP_MVC.asp_get_contents( srcname )
							end if
						else						
							content = POP_MVC.asp_get_contents( srcname )
						End If
					end if					
				else 	'默认情况下为模型类，array( tableName,db_type,db_name )				
					If isArray( className ) then
						modelFile = POP_MVC.getModelDstPath(className)
					else
						modelFile = POP_MVC.appPath & "/Runtime/Class/" & className & ".class.asp"
					end if
					
					if Not POP_MVC.file.isExists(  modelFile  ) Then
						set m = M_(className)
						set m = nothing
					End If
					if POP_MVC.file.isExists(  modelFile  ) Then
						content = POP_MVC.asp_get_contents(modelFile)
					End If
				end If
				
				set dict = ParseContent( content )
				
				set Parse = dict		
				call saveParse(rejFile,dict)
				set POP_MVC.dRejection( className ) = Parse
				set dict = nothing
			End If		
		End If
		
		Call L_("POPASP_REJECTION.Parse")		
	End Function
	
	sub saveParse( file,dict )
		dim key,item,str,temp
		str = ""
		for each key in dict
			item = dict(key)
			if isArray( item ) then
				if ubound(item) >=0 then					
					temp = "Array(""" & join( item,""",""" ) & """)"
					temp = "dict("""& key & """)=" & temp
				else
					temp = "dict("""& key & """) = Array()"
				end if				
			else
				temp = "dict("""& key &""") = " & """" & item & """"
			end if
			str = str & ":" & temp
		next
		str = mid( str ,2 )
		call POP_MVC.file_put_contents( file,str )
	End Sub
	
	' 从类的文本内容中获取解析内容
	' 包括{"private_vars":{"小写的变量名":"实际变量名"},"public_vars":{...},"private_method":{"小写的方法名称":"Function或者Sub"},"public_method":{...}}
	Public Function ParseContent( content )
		dim dict,extends
		set dict = D_
		set dict = ParseVariables( content )
		
		set dict = POP_MVC.dict.Merge(dict , ParseMethod( content ) )
		
		extends = ParseExtends(content)
		if Not isEmpty(extends) Then dict("extends") =  ParseExtends(content)
		set ParseContent = dict		
		set dict = Nothing
	End Function
	
	' 从类的文本内容中获取解析后的继承类名，比如后台控制器多继承 Common
	Public Function ParseExtends( content )
		dim matches	
		if POP_MVC.String.reg_test(content , "^\s*Class\s+.+\s*'\s*Extends\s+([\w-]+)" , "im") Then
			set matches = POP_MVC.reg.Execute( content )
			ParseExtends = matches(0).SubMatches(0)
			set matches = nothing
		End If			
	End Function
	
	' 从类的文本内容中获取解析后的方法，及其说明、类型
	' 返回二维Dictionary对象
	Public Function ParsePublicMethod( content )
		dim dict,pattern,matches,i,key,args,info
		set dict = D_
		pattern = "((?:^\s*\'(?!\s*\+).*?$)*)^\s*?(?:public\s+)?(?:default\s+)?(sub|function|property\s+let|property\s+get|property\s+set)\s+\[?(\w+)\]?\s*(\s+|\(.*?\))"
		
		if POP_MVC.String.reg_test(content,pattern,"gim") Then
			set matches = POP_MVC.reg.Execute ( content )
			
			for i = 0 to matches.count-1
				key = matches(i).submatches(2)
				set dict( key ) = D_
				dict(key)("name") = key 
				dict(key)("type") = LCase(matches(i).submatches(1))
				args = POP_MVC.String.rtrim(POP_MVC.String.ltrim(trim(LCase(matches(i).submatches(3))),"("),")")
				args  = POP_MVC.String.reg_replace( args,"", "byref\s+|byval\s+|\s+" , "gi" )
				info = matches(i).submatches(0)
				info  = POP_MVC.String.reg_replace( info ,"", "^\s*\'+" , "gim" )
				info  = POP_MVC.String.reg_replace( info ,"", "\s*$" , "gi" )
				info  = POP_MVC.String.encodeHtml(info)
				info  = POP_MVC.String.reg_replace( info,"<br />", "\s*$" , "gim" )
				info = POP_MVC.String.trim( info,"<br />" )
				dict(key)("args") = args
				dict(key)("info") = info
			next
			set matches = nothing
		End If	
		set ParsePublicMethod = dict
		set dict = nothing
	End Function
	
	' 从类的文本内容中获取解析后的方法，及其说明、类型
	' 返回二维Dictionary对象
	Public Function getNotePadAutoComplete( )
		dim pattern,matches,i,key,args,info,methodList,classList,path,folder,className,j,k,methodName,str
		pattern = "((?:^\s*\'(?!\s*\+).*?$)*)^\s*?(?:public\s+)?(?:default\s+)?(sub|function|property\s+let|property\s+get|property\s+set)\s+\[?(\w+)\]?\s*(\s+|\(.*?\))"
		redim  methodList(0)
		k = 0 
		
		folder = POP_MVC.mvc_dir
		classList = POP_MVC.file.getFileArr2( folder,"name", "POP_MVC.String.EndsWith(file.name,"".class.asp"")" )				
		for i = 0 to ubound( classList )
			className = split( classList( i ) , "." , "2" )(0)
			if not POP_MVC.arr.exists( Array("popasp_mysql","popasp_sqlserver","popasp_sqlite3","popasp_excel","popasp_txt","popasp_database_tool,popasp_database_tool,popasp_templte_compiler") , className ) then
				path = folder & classList( i )
				content = POP_MVC.asp_get_popasp( array(path,false) )				
				set matches = POP_MVC.string.reg_exec( content,pattern,"gim" )
				for j = 0 to matches.count-1
					ReDim Preserve methodList(k)
					methodName = matches(j).submatches(2)
					args = POP_MVC.String.rtrim(POP_MVC.String.ltrim(trim(LCase(matches(j).submatches(3))),"("),")")
					args  = POP_MVC.String.reg_replace( args,"", "byref\s+|byval\s+|\s+" , "gi" )
					args = trim( args )
					select case className
						case "popasp_mvc"
							methodList(k) = "POP_MVC." & methodName
						case "popasp_string"
							methodList(k) = "POP_MVC.String." & methodName
						case "popasp_arr"
							methodList(k) = "POP_MVC.Arr." & methodName
						case "popasp_dictionary"
							methodList(k) = "POP_MVC.Dict." & methodName
						case "popasp_file"
							methodList(k) = "POP_MVC.File." & methodName
						case "popasp_application"
							methodList(k) = "POP_MVC.apt." & methodName
						case "popasp_access"
							methodList(k) = "B_(tableName)." & methodName
						case else
							methodList(k) = "P_(""" & split( className,"_" , 2 )(1) & """)." & methodName					
					end select
					if args <> "" then
						methodList(k) = methodList(k) & "(" & args & ")"
					end if
					k = k + 1
					if className = "popasp_controller" then
						redim  methodList(k)
						methodList(k) = "that." & methodName & "(" & args & ")"
						k = k + 1
					end if
				next
				set matches = nothing
			end if
		next
		
		folder = POP_MVC.mvc_dir & "plugin/"
		classList = POP_MVC.file.getFileArr2( folder,"name", "POP_MVC.String.EndsWith(file.name,"".class.asp"")" )
		for i = 0 to ubound( classList )
			className = split( classList( i ) , "." , "2" )(0)
			path = folder & classList( i )
			content = POP_MVC.asp_get_popasp( array(path,false) )			
			set matches = POP_MVC.string.reg_exec( content,pattern,"gim" )
			for j = 0 to matches.count-1
				ReDim Preserve methodList(k)
				methodName = matches(j).submatches(2)
				args = POP_MVC.String.rtrim(POP_MVC.String.ltrim(trim(LCase(matches(j).submatches(3))),"("),")")
				args  = POP_MVC.String.reg_replace( args,"", "byref\s+|byval\s+|\s+" , "gi" )
				args = trim( args )
				select case className
					case "popasp_mvc"
						methodList(k) = "POP_MVC." & methodName
					case "popasp_string"
						methodList(k) = "POP_MVC.String." & methodName
					case "popasp_arr"
						methodList(k) = "POP_MVC.Arr." & methodName
					case "popasp_dictionary"
						methodList(k) = "POP_MVC.Dict." & methodName
					case "popasp_file"
						methodList(k) = "POP_MVC.File." & methodName
					case "popasp_access"
						methodList(k) = "B_(tableName)." & methodName
					case else
						methodList(k) = "P_(""" & split( className,"_" , 2 )(1) & """)." & methodName					
				end select
				if args <> "" then
					methodList(k) = methodList(k) & "(" & args & ")"
				end if
				k = k + 1
			next
			set matches = nothing			
		next
		
		path = POP_MVC.mvc_dir & "popasp.func.asp"
		content = POP_MVC.file_get_contents( path )			
		set matches = POP_MVC.string.reg_exec( content,pattern,"gim" )
		for j = 0 to matches.count-1
			ReDim Preserve methodList(k)
			methodName = matches(j).submatches(2)
			args = POP_MVC.String.rtrim(POP_MVC.String.ltrim(trim(LCase(matches(j).submatches(3))),"("),")")
			args  = POP_MVC.String.reg_replace( args,"", "byref\s+|byval\s+|\s+" , "gi" )
			args = trim( args )
			methodList(k) = methodName
			if args <> "" then
				methodList(k) = methodList(k) & "(" & args & ")"
			end if
			k = k + 1
		next
		set matches = nothing			
		
		Call POP_MVC.Arr.sort( methodList )
		
		str = ""
		
		for i = 0 to ubound( methodList )
			str = str & "<KeyWord name='" & methodList( i ) & "' />" & vbcrlf
		next
		
		getNotePadAutoComplete = str
	End Function
	
	' 从类的文本内容中获取解析后的方法，包括Private方法与Public方法
	' 返回的是Dictionary对象。{"private_method":array(小写的方法名),"public_method":array(小写的方法名)}
	Public Function ParseMethod( content )
		dim dict
		set dict = D_
		dict("private_method") = ParseMethod_( "^\s*(?:private\s+)(sub|function|property\s+let|property\s+get|property\s+set)\s+\[?(\w+)\]?\s*" ,content )
		dict("public_method") = ParseMethod_( "^\s*(?:public\s+)?(?:default\s+)?(sub|function|property\s+let|property\s+get|property\s+set)\s+\[?(\w+)\]?\s*" ,content  )
		set ParseMethod = dict
		set dict = nothing
	End Function
	
	' 从类的文本内容中获取解析后的变量，包括Private变量与Public变量
	' 返回的是Dictionary对象。{"private_vars":array(属性名),"public_vars":array(属性名)}
	Public Function ParseVariables( content )
		dim dict
		set dict = D_
		dict("private_vars") = ParseVariables_( "^\s*(?:private\s+)(?!sub|property|function)([\w ,\[\]]+)\s*('.*)?$" ,content )
		dict("public_vars") = ParseVariables_( "^\s*(?:public\s+)(?!sub|property|function)([\w ,\[\]]+)\s*('.*)?$" ,content )
		set ParseVariables = dict
		set dict = nothing
	End Function
	
	'解析类中的类变量，pattern为正则匹配，content为类的文本内容
	Private Function ParseVariables_( byref pattern,byRef content ) 'className,method
		dim matches,i,j,str,arr,temp,key,cnt,ret
		ParseVariables_ = Array()
		if POP_MVC.String.reg_test(content,pattern,"gim") Then			
			set matches = POP_MVC.reg.Execute( content  )

			for i = 0 to matches.count-1
				str = matches(i).submatches(0)
				arr = split( str, "," )
				cnt = ubound(arr)
				for j = 0 to cnt
					temp = arr(j)
					key = temp
					key = POP_MVC.ltrim( key, "[" )
					key = POP_MVC.ltrim( key, "]" )
					key = LCase(key)
					POP_MVC.Arr.Push ret,temp
				next
			next
			set matches = nothing
			ParseVariables_ = ret
		End If		
	End Function
	
	'解析类中的方法，pattern为正则匹配，content为类的文本内容
	Private Function ParseMethod_( byref pattern ,byref content ) 'className,method
		dim matches,i
		dim arr
		ParseMethod_ = Array()
		if POP_MVC.String.reg_test(content,pattern,"gim") Then
			set matches = POP_MVC.reg.Execute ( content )
			for i = 0 to matches.count-1				
				POP_MVC.Arr.push arr,matches(i).submatches(1)
			next
			set matches = nothing
			ParseMethod_ = arr
		End If				
	End Function	
	
	Private Sub Class_Initialize
		set reg = POP_MVC.reg
	End Sub
	
	Private sub Class_Terminate
		'set reg = Nothing
	End Sub
End Class
%>