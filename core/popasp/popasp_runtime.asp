<%@LANGUAGE="VBSCRIPT" CODEPAGE="65001"%>
<%
Option Explicit
Server.ScriptTimeOut = 300
Response.Buffer = True  '设置输出缓冲区
Response.Charset = "utf-8"  '设置输出缓冲区
Response.Expires = 0
Response.ExpiresAbsolute = Now() - 1
Response.CacheControl = "no-cache"
Response.AddHeader "Expires",Now() - 1
Response.AddHeader "Pragma","no-cache"
Response.AddHeader "Cache-Control","private, no-cache, must-revalidate"
%><!--#include file="Runtime/inc_list.asp"--><script language="vbscript" runat="server">If TypeName(POP_MVC) = "POPASP_MVC4BBS" Then Call POP_MVC.show_page_trace() : Set POP_MVC = Nothing</script>