<%
' +----------------------------------------------------------------------
' | POPASP [ ASP MVC ]
' +----------------------------------------------------------------------
' | Copyright (c) 2016 http://popasp.com All rights reserved.
' +----------------------------------------------------------------------
' | Author: popasp <1737025626@qq.com>
' +----------------------------------------------------------------------
Class POPASP_SAFE
	'检测某个目录CheckDir下面是否含有木马文件
	'
	private CheckFileExtNameArr,CheckPatternArr,CheckReturnArr,CheckFirst
	private SumFileExtNameArr
	
	Private PhpPatternArr,AspPatternArr,JspPatternArr,HtmlPatternArr
	
	Private Sub Class_Initialize
		'Html扫描正则匹配表达式
		HtmlPatternArr = Array("\<script[^>]+JScript\.Encode[^>]*\>","\<body[^>]+onload\s*\=\s*\W*(window\.)?location=\W*[^>]+>","\<iframe[^>]+src\s*=\s*(\'|\""|)*http(s?):\/\/(?:[A-za-z0-9-]+\.)+[A-za-z]{2,4}(?:[\/\?#][\/=\?%\-&~`@[\]\':+!\.#\w]*)?(\'|\""|)*[^>]*\>")		
	
		'PHP语言扫描正则匹配表达式
		PhpPatternArr = Array( "function\_exists\s*\(\s*[\'|\""](popen|exec|proc\_open|system|passthru)+[\'|\""]\s*\)","(exec|shell\_exec|system|passthru)+\s*\(\s*\$\_(GET|POST|COOKIE|SERVER|SESSION)+\[(.*)\]\s*\)","(udp\:\/\/(.*)\;)+","preg\_replace\s*\((.*)\/e(.*)\,\s*\$\_(.*)\,(.*)\)","preg\_replace\s*\((.*)\(base64\_decode\(\$","(eval|assert|include|require)+\s*\((.*)(base64\_decode|file\_get\_contents|php\:\/\/input)+","(eval|assert|include|require|array\_map)+\s*\(\s*\$\_(GET|POST|COOKIE|SERVER|SESSION)+\[(.*?)\]\s*\)","\$\_(GET|POST|COOKIE|SERVER|SESSION)+(.*?)(eval|assert|include|require)+\s*\(\s*\$(\w+)\s*\)","\$\_(GET|POST|COOKIE|SERVER|SESSION)+\[(.*)\]\(\s*\$(.*)\)","\(\s*\$\_FILES\[(.*)\]\[(.*)\]\s*\,\s*\$\_FILES\[(.*)\]\[(.*)\]\s*\)","(fopen|fwrite|fpust|file\_put\_contents)+\s*\((.*)\$\_(GET|POST|COOKIE|SERVER|SESSION)+\[(.*)\](.*)\)","echo\s*curl\_exec\s*\(\s*\$(\w+)\s*\)","new com\s*\(\s*[\'|\""]shell(.*)[\'|\""]\s*\)","\$(.*)\s*\((.*)\/e(.*)\,\s*\$\_(.*)\,(.*)\)","\$\_\=(.*)\$\_","str_rot13\s*(.*?)")
		
		PhpPatternArr = POP_MVC.Arr.Merge( PhpPatternArr , HtmlPatternArr )
		
		'ASP+ASPX语言扫描正则匹配表达式
		AspPatternArr = array("(	(vbscript|jscript|javascript)\.Encode|WScript\.shell|Shell\.Application|Scripting\.FileSystemObject)+","S[""\s&]*c[""\s&]*r[""\s&]*i[""\s&]*p[""\s&]*t[""\s&]*i[""\s&]*n[""\s&]*g[""\s&]*\.[""\s&]*F[""\s&]*i[""\s&]*l[""\s&]*e[""\s&]*S[""\s&]*y[""\s&]*s[""\s&]*t[""\s&]*e[""\s&]*m[""\s&]*O[""\s&]*b[""\s&]*j[""\s&]*e[""\s&]*c[""\s&]*t","(eval|execute)+(.*)(request|session)+\s*\((.*)\)","(eval|execute)+(.*)request.item\s*\[(.*)\]","request\s*\((.*)\)(.*)(eval|execute)+\s*\((.*)\)","\<script\s*runat\s*\=(.*)server(.*)\>(.*)\<\/script\>","Load\s*\((.*)Request","StreamWriter\(Server\.MapPath(.*)\.Write\(Request")
		
		AspPatternArr = POP_MVC.Arr.Merge( AspPatternArr , HtmlPatternArr )
		
		'JSP语言扫描正则匹配表达式
		JspPatternArr = Array("(eval|execute)+(.*)(request|session)+\s*\((.*)\)","(eval|execute)+(.*)request.item\s*\[(.*)\]","request\s*\((.*)\)(.*)(eval|execute)+\s*\((.*)\)","Runtime\.getRuntime\(\)\.exec\((.*)\)","FileOutputStream\(application\.getRealPath(.*)request")
		
		JspPatternArr = POP_MVC.Arr.Merge( JspPatternArr , HtmlPatternArr )
		
		CheckReturnArr = Array()
	End Sub
	
	Public Function HtmlCheck( CheckDir , ByVal ExtName , NeedFirst )
		HtmlCheck = WebCheck( "Html" , CheckDir , ExtName , NeedFirst )	
	End Function
	
	Public Function AspCheck( CheckDir , ByVal ExtName , NeedFirst )
		AspCheck = WebCheck( "Asp" , CheckDir , ExtName , NeedFirst )	
	End Function
	
	Public Function PhpCheck( CheckDir , ByVal ExtName , NeedFirst )
		PhpCheck = WebCheck( "Php" , CheckDir , ExtName , NeedFirst )	
	End Function
	
	Public Function JspCheck( CheckDir , ByVal ExtName , NeedFirst )
		JspCheck = WebCheck( "Jsp" , CheckDir , ExtName , NeedFirst )	
	End Function
	
	
	'Lang为语言类型，共3种, "Asp" , "Php" , "Jsp"
	'CheckDir为扫描目录
	'ExtName为文件后缀，"asp","asp,css,js","php","jsp"
	'NeedFirst为1时只取第1个，为0时取所有
	'取所有的时候返回二维数组 Array( Array( 第1项为路径，第2项为数组Array( 匹配项1,匹配项2,... ) ) ,  )
	'只取第1个时为一维数组Array( "路径|匹配项" ,"路径|匹配项",...  )
	Public Function WebCheck( Lang, CheckDir , ByVal ExtName , NeedFirst )
		Execute( "CheckPatternArr = " & Lang & "PatternArr" )
		CheckReturnArr = Array()
		if not isArray( ExtName ) then
			CheckFileExtNameArr = split( ExtName , "," )
		else		
			CheckFileExtNameArr = ExtName
		end if
		
		for i = 0 to ubound( CheckFileExtNameArr )
			CheckFileExtNameArr(i) = LCase( CheckFileExtNameArr(i) )
		next		
	
		CheckFirst = NOT is_empty(NeedFirst)
		
		call POP_MVC.File.files_map(CheckDir , "P_(""SAFE"").WebCheck_")

		WebCheck = CheckReturnArr
	End Function
	
	
	'回调函数
	sub WebCheck_( file )
		on error resume next
		dim path,extname,content,ret
		path = file.path
		path = replace(path,"\" , "/")
		extname = LCase(Mid(file.name, InStrRev(file.name, ".") + 1))
	
		if NOT POP_MVC.Arr.Exists( CheckFileExtNameArr , extname ) then
			Exit Sub
		end if
		
		content = POP_MVC.file_get_contents( path )
		

		for i = 0 to ubound( CheckPatternArr )
			if CheckFirst then
				ret = POP_MVC.String.reg_fetch( content, CheckPatternArr(i) , "i"  )
				if ret <> "" then
					POP_MVC.Arr.push CheckReturnArr , path & "|" & left(ret,200)
					exit for
				end if			
			else
				ret = POP_MVC.String.reg_array( content, CheckPatternArr(i) , "", "i"  )
				if ubound(ret) > -1 then
					POP_MVC.Arr.push CheckReturnArr ,Array( path,ret )
				end if			
			end if
		next
	end sub	
	
	'检测asp单个文件是否挂马
	Function CheckAspFile( ByVal path  )
		on error resume next
		dim extname,content,ret
		path = replace(path,"\" , "/")
		extname = LCase(Mid(path, InStrRev(path, ".") + 1))
		content = POP_MVC.file_get_contents( path )
		for i = 0 to ubound( AspPatternArr )
			ret = POP_MVC.String.reg_fetch( content, AspPatternArr(i) , "i"  )
			if ret <> "" then
				exit for
			end if	
		next
		CheckAspFile = ret
	end Function	
	
	'检测php单个文件是否挂马
	Function CheckPhpFile( ByVal path  )
		on error resume next
		dim extname,content,ret
		path = replace(path,"\" , "/")
		extname = LCase(Mid(path, InStrRev(path, ".") + 1))
		content = POP_MVC.file_get_contents( path )
		for i = 0 to ubound( PhpPatternArr )
			ret = POP_MVC.String.reg_fetch( content, AspPatternArr(i) , "i"  )
			if ret <> "" then
				exit for
			end if	
		next
		CheckPhpFile = ret
	end Function	
	
	'检测html或xml单个文件是否挂马
	Function CheckHtmlFile( ByVal path  )
		on error resume next
		dim extname,content,ret
		path = replace(path,"\" , "/")
		extname = LCase(Mid(path, InStrRev(path, ".") + 1))
		content = POP_MVC.file_get_contents( path )
		for i = 0 to ubound( HtmlPatternArr )
			ret = POP_MVC.String.reg_fetch( content, HtmlPatternArr(i) , "i"  )
			if ret <> "" then
				exit for
			end if	
		next
		CheckHtmlFile = ret
	end Function

	'汇总某个目录下面所有的文件类型，即看有多少种后缀名，不区分大小写
	Function SumFileExtName( CheckDir )
		SumFileExtNameArr = Array()
		call POP_MVC.File.files_map(CheckDir , "P_(""SAFE"").SumFileExtName_")
		SumFileExtName = SumFileExtNameArr
	End Function
	
	Sub SumFileExtName_(file)
		dim extname
		extname = Mid(file.name, InStrRev(file.name, ".") + 1)
		extname = LCase(extname)
		if extname <> "" then
			if not POP_MVC.Arr.Exists(SumFileExtNameArr,extname) then
				POP_MVC.Arr.push SumFileExtNameArr,extname
			end if
		end if
	End sub
End Class
%>