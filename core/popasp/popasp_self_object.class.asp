<%
' +----------------------------------------------------------------------
' | POPASP [ ASP MVC ]
' +----------------------------------------------------------------------
' | Copyright (c) 2016 http://popasp.com All rights reserved.
' +----------------------------------------------------------------------
' | Author: popasp <1737025626@qq.com>
' +----------------------------------------------------------------------

'处理RECORDSET的类
Class POPASP_SELF_OBJECT
	Private className
	

	Function object2dict( ByRef obj )
		dim i,dict
		set dict = POP_MVC.CreateDict
		for i = 0 to ubound( obj.vars__ )
			dict.add obj.vars__(i),obj.get__( obj.vars__(i) )
		next
		set object2dict = dict
	End Function
	

	'将主键值作为键名，将第2个字段的值作为单一值形成的object对象
	Function dict2object( ByRef dict )
		on error resume next
		dim start,str,str2,temp,className,i,obj,temp2,arr,bool,key
		
		if typename( dict ) <> "Dictionary" then
			set dict2object = Nothing
			exit Function
		end if
		
		if dict.count < 0 then
			set dict2object = Nothing
			exit Function
		end if

		className = POP_MVC.config("POPASP_SELF_OBJECT_PREFIX") & POP_MVC.dClass.count
		str = "Class " & className & vbcrlf	
	
			str2 = ""
			temp = ""
			temp2 = ""
			str = str & "Public vars__" & vbcrlf
			str = str & "Public "
			i = 0
			
			for each key in dict
				temp = temp & "[" & key & "],"
				temp2 =  temp2 & """" & key & ""","			
				POP_MVC.arr.push arr,dict(key)
				str2 = str2 & vbTab & "[" & key & "]=" & "dict(keys__(" & i & "))" & vbcrlf
				i = i + 1
			next

			temp = left( temp , len(temp) - 1 )
			temp2 = left( temp2 , len(temp2) - 1 )
			str = str & temp & vbcrlf
			str = str & "Public Sub Init__(ByRef dict)" & vbcrlf
			str = str & vbTab & "dim keys__" & vbcrlf
			str = str & vbTab & "keys__ = dict.keys" & vbcrlf
			str = str & vbTab & "vars__ = Array(" & temp2 & ")" & vbcrlf
			str = str & str2
			str = str & "End Sub" & vbcrlf
			str = str & "Property Get Exists__(ByRef key__)" & vbcrlf
			str = str & vbTab & "Exists__=POP_MVC.Arr.iExists(vars__,key__)"  & vbcrlf
			str = str & "End Property" & vbcrlf
			str = str & "Property Get Get__(ByRef key__)" & vbcrlf
			str = str & vbTab & "Execute(""Get__ = "" &  key__ )"  & vbcrlf
			str = str & vbTab & "if isNull(Get__) Then Get__ = """""  & vbcrlf
			str = str & "End Property" & vbcrlf

		str = str & "End Class" & vbcrlf

		'var_Export str : response.end
		execute( str )
		
		if err.number <> 0 then
			POP_MVC.error( L_( className & ".dict2object") )
		end if
		execute( "set POP_MVC.dClass(className) = NEW " & className )
		POP_MVC.dClass(className).init__(dict)
		set dict2object = POP_MVC.dClass(className)
	End Function
	
	'将主键值作为键名，将第2个字段的值作为单一值形成的object对象
	Function dict2class( ByRef className , ByRef dict )
		on error resume next
		dim start,str,str2,temp,i,obj,temp2,arr,bool,key
		
		if typename( dict ) <> "Dictionary" then
			set dict2object = Nothing
			exit Function
		end if
		
		if dict.count < 0 then
			set dict2object = Nothing
			exit Function
		end if

		str = "Class " & className & vbcrlf	
	
			str2 = ""
			temp = ""
			temp2 = ""
			str = str & "Public vars__" & vbcrlf
			str = str & "Public "
			i = 0
			
			for each key in dict
				temp = temp & "[" & key & "],"
				temp2 =  temp2 & """" & key & ""","			
				POP_MVC.arr.push arr,dict(key)
				str2 = str2 & vbTab & "[" & key & "]=""" & POP_MVC.String.vbsEncode(dict(key))  & """" & vbcrlf
				i = i + 1
			next

			temp = left( temp , len(temp) - 1 )
			temp2 = left( temp2 , len(temp2) - 1 )
			str = str & temp & vbcrlf
			str = str & "Private Sub Class_Initialize" & vbcrlf
			str = str & vbTab & "vars__ = Array(" & temp2 & ")" & vbcrlf
			str = str & str2
			str = str & "End Sub" & vbcrlf
			str = str & "Property Get Exists__(ByRef key__)" & vbcrlf
			str = str & vbTab & "Exists__=POP_MVC.Arr.iExists(vars__,key__)"  & vbcrlf
			str = str & "End Property" & vbcrlf
			str = str & "Property Get Get__(ByRef key__)" & vbcrlf
			str = str & vbTab & "Execute(""Get__ = "" &  key__ )"  & vbcrlf
			str = str & vbTab & "if isNull(Get__) Then Get__ = """""  & vbcrlf
			str = str & "End Property" & vbcrlf

		str = str & "End Class" & vbcrlf

		'var_Export str : response.end
		'execute( str )
		
		dict2class = str
	End Function

	'将主键值作为键名，将第2个字段的值作为单一值形成的object对象
	Function rs2object( ByRef rs , isAccess )
		dim start,sql,str,str2,stype,dict,temp,className,i,obj,temp2,arr,bool
		
		if rs.eof then
			set rs2object = Nothing
			exit Function
		end if
		
		if not is_empty( POP_MVC.config("SHOW_PAGE_TRACE") ) Then
			start = timer()	
		end if	

		className = POP_MVC.config("POPASP_SELF_OBJECT_PREFIX") & POP_MVC.dClass.count
		str = "Class " & className & vbcrlf	
	
		if not rs.BOF and not rs.EOF then
			str2 = ""
			temp = ""
			temp2 = ""
			str = str & "Public vars__" & vbcrlf
			str = str & "Public "
			'通过find来获取的，将字段名作为属性名
			if POP_MVC.String.reg_test( rs.Source , "^\s*SELECT\s+TOP\s+1\s+.+|^\s*SELECT\s+.*?LIMIT\s+1\s*;?$", "i" ) then
				bool = true
				for i = 0 to rs.fields.count-1	
							temp = temp & "[" & rs.Fields(i).Name & "],"
							temp2 =  temp2 & """" & rs.Fields(i).Name & ""","			
					if not isAccess then
						Call typename( rs.Fields(i).Value )
						if err.number <> 0 then	
							if typename(rs.Fields(i).Value) = "Long" then
								str2 = str2 & "[" & rs.Fields(i).Name & "]=" & "CLng(rs.Fields(" & i & ").Value)" & vbcrlf
							else
								str2 = str2 & "[" & rs.Fields(i).Name & "]=" & "rs.Fields(" & i & ").Value" & vbcrlf
								'POP_MVC.Exit( "发现了不能被ASP解析的数据类型，请联系POPASP作者，以解决此BUG" )
							end if							
							err.clear
						else
							str2 = str2 & vbTab & "[" & rs.Fields(i).Name & "]=" & "rs.Fields(" & i & ").Value" & vbcrlf
						end if						
					else
						str2 = str2 & vbTab & "[" & rs.Fields(i).Name & "]=" & "rs.Fields(" & i & ").Value" & vbcrlf
					end if
				next
			else
			'通过select来获取的，将记录的第一个值作为属性名，将记录第二个值作为值
				bool = false
				i = 0
				do while not rs.eof
					temp = temp & "[" & rs.Fields(0).value & "],"
					temp2 =  temp2 & """" & rs.Fields(0).value & ""","			
					POP_MVC.arr.push arr,rs.Fields(1).Value	
					str2 = str2 & vbTab & "[" & rs.Fields(0).value & "]=" & "arr__(" & i & ")" & vbcrlf
					i = i + 1
					rs.movenext
				loop
			end if

			temp = left( temp , len(temp) - 1 )
			temp2 = left( temp2 , len(temp2) - 1 )
			str = str & temp & vbcrlf
			if bool then
				str = str & "Public Sub Init__(ByRef rs)" & vbcrlf
			else
				str = str & "Public Sub Init__(ByRef arr__)" & vbcrlf
			end if 
			str = str & vbTab & "vars__ = Array(" & temp2 & ")" & vbcrlf
			str = str & str2
			str = str & "End Sub" & vbcrlf
			str = str & "Property Get Exists__(ByRef key__)" & vbcrlf
			str = str & vbTab & "Exists__=POP_MVC.Arr.iExists(vars__,key__)"  & vbcrlf
			str = str & "End Property" & vbcrlf
			str = str & "Property Get Get__(ByRef key__)" & vbcrlf
			str = str & vbTab & "Execute(""Get__ = "" &  key__ )"  & vbcrlf
			str = str & vbTab & "if isNull(Get__) Then Get__ = """""  & vbcrlf
			str = str & "End Property" & vbcrlf
		end if	
		str = str & "End Class" & vbcrlf

		'var_Export str : response.end
		execute( str )
		execute( "set POP_MVC.dClass(className) = NEW " & className )
		if bool then
			POP_MVC.dClass(className).init__(rs)
		else
			POP_MVC.dClass(className).init__(arr)
		end if
		set rs2object = POP_MVC.dClass(className)

		Call pushDebug( rs, start ,  "Recordset to " & className )
	End Function
	
	'记录信息
	Private Sub pushDebug( rs , start , info ) 
		dim sql
		if not is_empty( POP_MVC.config("SHOW_PAGE_TRACE") ) Then
			set sql = Server.CreateObject("Scripting.Dictionary")
			sql("time") = round((timer() - start) * 1000,0)
			sql("sql") = rs.Source & " ; -- " & info
			set POP_MVC.dSql(POP_MVC.dSql.count + 1) = sql
			set sql = nothing
		end if	
	End Sub	
	
	Private Sub Class_Initialize
		className = TypeName( Me )
	End Sub
End Class
%>