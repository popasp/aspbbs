<%
' +----------------------------------------------------------------------
' | POPASP [ ASP MVC ]
' +----------------------------------------------------------------------
' | Copyright (c) 2016 http://popasp.com All rights reserved.
' +----------------------------------------------------------------------
' | Author: popasp <1737025626@qq.com>
' +----------------------------------------------------------------------

'处理 版本 的类
Class POPASP_SERVICE
	'获取CPU使用率
	function getCpuPerc
		dim objProc
		Set objProc  = GetObject("winmgmts://./root/cimv2:win32_processor='cpu0'")
		'var_export "CPU Load Percentage: " & objProc.LoadPercentage & "%"		
		getCpuPerc = "CPU:" &  objProc.LoadPercentage & "%"
		set objProc = nothing
	end function
	
	function getMemory
		dim strComputer,objWMI,colOS,objOS,strReturn
		strComputer = "."
		set objWMI = GetObject("winmgmts:\\" & strComputer & "\root\cimv2")
		set colOS = objWMI.InstancesOf("Win32_OperatingSystem")
		for each objOS in colOS
		'strReturn = "内存总数: " &  round(objOS.TotalVisibleMemorySize / 1024) & " MB" & vbCrLf &"内存可用数: " & round(objOS.FreePhysicalMemory / 1024) & " MB" & vbCrLf &"内存使用率 :" & Round(((objOS.TotalVisibleMemorySize-objOS.FreePhysicalMemory)/objOS.TotalVisibleMemorySize)*100) & "%"
		strReturn = round(objOS.FreePhysicalMemory / 1024)  /  round(objOS.TotalVisibleMemorySize / 1024) 
		getMemory = "内存:" &  FormatNumber( strReturn *100,0,-1,0,0) & "%"
		next
	end function
End Class
%>