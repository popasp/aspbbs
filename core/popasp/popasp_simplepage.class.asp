<%
' +----------------------------------------------------------------------
' | POPASP [ ASP MVC ]
' +----------------------------------------------------------------------
' | Copyright (c) 2016 http://popasp.com All rights reserved.
' +----------------------------------------------------------------------
' | Author: popasp <1737025626@qq.com>
' +----------------------------------------------------------------------
Class POPASP_SIMPLEPAGE	
	public total	'记录总个数	
	private totalPage	'总共多少页
	private page	'当前页
	private uri
	private parameter	'网址中的额外参数
	private seperator		'分隔符
	
	'下面是可配置的
	private perpage	'每页显示多少条
	private noRecordTip		'无数据的提示

	Private Sub Class_Initialize
		dim arr,pos
		perpage = POP_MVC.config("PAGE_PAGESIZE")
		noRecordTip = "没有可载入的数据"

		page =  POP_MVC.Get( POP_MVC.config("VAR_PAGE") )
		
		if not isNumeric( page ) then
			page = 1
		end if
		
		if page < 1 then
			page = 1
		end if
		
		page = CLng( page )
 	End Sub
	
	Public Property Get config(attr)
		Execute "config = " & attr
	end Property
	
	Public Property Let config(attr,value)
		if LCase( attr ) = "parameter" then
			Call setParameter( value )
			Exit Property
		end if
		
		if LCase( attr ) = "perpage" then
			perpage = value
			Exit Property
		end if
		
		if attr = "pagesize" then
			pagesize = value
			Exit Property
		end if
		
		if isNumeric(value) then
			Execute attr & " = " & value
		else			
			Execute attr & " = """ & value & """"
		end if
	End Property
	
	Private Sub setParameter( arg )
		if typename( arg ) = "Dictionary" then	'Dictionary对象
			dim k1,k2
			parameter = ""
			for each k1 in arg
				if typename( arg(k1) ) = "Dictionary" then
					for each k2 in arg(k1)
						parameter = parameter & "&" & k1 & "=" & cstr( arg(k1)(k2) )
					next
				else
					parameter = parameter & "&" & k1 & "=" & cstr( arg(k1) ) 
				end if
			next
			parameter = POP_MVC.ltrim( parameter , "&" )
		else
			parameter = arg
		end if
	End Sub 
	
	'使用时有两种方法，一种是传入Recordset对象，一种是传入总数
	'1. that.assign "page" , P_("PAGE")(rs).show
	'2. that.assign "page" , P_("PAGE")( total ).show，total为总数
	'url其它参数传入时，可以使用P_("PAGE")( Array( rs/total, parameter , route ) )，parameter可以是字符串也可以是最多二维的Dictionary对象
	Public Default Function Constructor(byref args)
		dim rs
		if isArray(args) then
			if typename( args(0) ) = "Recordset" then
				set rs = args(0)
			else
				if POP_MVC.String.reg_test(POP_MVC.get( POP_MVC.config("VAR_PAGESIZE") ),"^[1-9]\d*$","") then
					perpage = POP_MVC.get( POP_MVC.config("VAR_PAGESIZE") )
				end if
				total = args(0)
			end if
			if ubound( args ) > 0 then
				if not isNul( args(1) ) then
					Call setParameter( args(1) )
				end if
			end if
			
			if ubound( args ) > 1 then
				route = args(2)
			end if
		else
			if typename( args ) = "Recordset" then
				set rs = args
			else
				if POP_MVC.String.reg_test(POP_MVC.get( POP_MVC.config("VAR_PAGESIZE") ),"[1-9]\d*","") then
					perpage = POP_MVC.get( POP_MVC.config("VAR_PAGESIZE") )
				end if
				total = args
			end if
			parameter = ""
		end if
		
		
		if not isEmpty( rs ) then
			total = rs.recordCount			
			totalPage = rs.pageCount
			perpage = rs.pageSize
		else
			totalPage = CLng(Abs(Int(- total / perpage)))
		end if


		page = CLng(page)
		
		if page > totalPage then
			page = totalPage
		end if
		
		
		
		set Constructor = Me
	End Function
	
	Public Function GetUri
		if isEmpty( uri ) then
			if C_("URL_MODE") <> 0 then
				uri = "?" & POP_MVC.config("VAR_MODULE") & "=" & POP_MVC.c & "&" & POP_MVC.config("VAR_ACTION") & "=" & POP_MVC.a
				dim key
				for each key in POP_MVC.dQuery
					if key <> POP_MVC.config("VAR_MODULE") and key <> POP_MVC.config("VAR_ACTION") and key <> POP_MVC.config("VAR_PAGE") then
						uri = uri & "&" & key & "=" & POP_MVC.dQuery(key)
					end if
				next				
			else
				uri = SELF__
			end if

			
			if parameter <> "" AND inStr( POP_MVC.urldecode(uri),POP_MVC.urldecode(parameter) ) < 1 then
				if POP_MVC.Get("") = "" then
					uri = uri & "?" & parameter
				else
					uri = uri & "&" & parameter
				end if
			end if
			
			uri = POP_MVC.String.reg_replace(uri,"","&" & POP_MVC.config("VAR_PAGE") & "=[^&]+","ig")
			uri = POP_MVC.String.reg_replace(uri,"","\b" & POP_MVC.config("VAR_PAGE") & "=[^&]+","ig")
			
			if instr( uri , "?" ) > 0 then
				if inStr( uri , "&" ) > 1 or not POP_MVC.String.EndsWith( uri , "?" ) then
					uri = uri & "&"
				end if
			else
				uri = uri & "?"
			end if			
		end if
		getUri = uri
	End Function
	
	'得到页码的链接html
	'如 <a href="clist.asp?ShowId=24&page=2">2</a>
	Public Function getUrl(dPage)
		if isEmpty( uri ) then
			uri = getUri		
		end if
		getUrl = uri & POP_MVC.config("VAR_PAGE") & "=" & dPage
	End Function
	
	'将当前页的起始与结束位以数组形式返回，如 Array( 10,19 )
	Public Function getRange(  )
		dim start_,end_
		start_ = (page-1) * perpage
		end_ = start_ + perpage -1
		if end_ > total - 1 then
			end_ = total -1
		end if
		
		getRange = Array( start_ , end_ )
	End Function
	
	Public Function getScale
		if total < 1 then
			getScale = ""
		end if
		
		getScale = page & "/" & totalPage
	End Function
	
	'获取上一页的链接，得不到返回空字符串
	Public Function getPrev		
		if total < 1 then
			getPrev = noRecordTip
			Exit Function
		end if		
		if page < 2 OR totalPage < 1 then
			getPrev = ""
			Exit Function
		end if		
		getPrev = getUrl( page -1 )
	End Function
	
	'获取下一页的链接，得不到返回空字符串
	Public Function getNext
		if total < 1 then
			getNext = noRecordTip
			Exit Function
		end if	
	
		if page > (totalPage - 1)  OR totalPage < 1 then
			getNext = ""
			Exit Function
		end if
		getNext = getUrl( page + 1 )
	End Function	
	
	
	
	Public Property Get NumList
		dim dLeft,dRight,nav
		nav = array()
		if total < 1 Then
			NumList = Array()
			Exit Property
		end if

		If totalPage > 1 then
			POP_MVC.Arr.push nav,"<option value='" & page & "' selected='selected'>当前第" & page & "页</option>"
		End If
		
		dLeft = page - 1
		dRight = page + 1
			
		
		do While dLeft >= 1 OR dRight <= totalPage		
			if UBound(nav) >= pageCount-1 then
				exit do
			end if		
			
			if dLeft>= 1 then	
				POP_MVC.Arr.Unshift nav, "<option value='"& getUrl(dLeft) &"'>第" & dLeft & "页</option>"
				dLeft = dLeft - 1
			end if
			
			if dRight <= totalPage then
				if dRight >= 1 then
					POP_MVC.Arr.Push nav, "<option value='"& getUrl(dRight) &"'>第" & dRight & "页</option>"
				end if
				dRight = dRight + 1
			end if
		Loop
		NumList = nav
	End Property
	
	Public Property Get Show
		if total < 1 then
			Show = noRecordTip
			exit Property
		end if
	
		dim ret,pre,nex,lists
		pre = getPrev
		if pre<> "" then
			ret = "<a href='" & pre & "' class='page-prev'>上一页</a>"
		else
			ret = "共" & totalPage & "页"
		end if
		
		ret = ret & seperator
		
		lists = NumList
		if ubound(lists) > 0 then
			ret = ret & "<select onchange=""location.href=this.value"" class='page-select'>" & join(lists , vbcrlf) & "</select>"
		else
			if totalPage > 1 then
				ret = ret & "共" & totalPage & "页"
			end if
		end if
		
		ret = ret & seperator
		
		nex = getNext
		if nex<> "" then
			ret = ret & "<a href='" & nex & "' class='page-next'>下一页</a>"
		else
			ret = ret & "" & total & "条"
		end if
		Show = ret
	End Property
End Class
%>