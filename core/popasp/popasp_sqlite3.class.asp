<%
' +----------------------------------------------------------------------
' | POPASP [ ASP MVC ]
' +----------------------------------------------------------------------
' | Copyright (c) 2016 http://popasp.com All rights reserved.
' +----------------------------------------------------------------------
' | Author: popasp <1737025626@qq.com>
' +----------------------------------------------------------------------

'在本类中已经使用了Replace作为方法名，所以在本类中不能再使用Replace函数了，请考虑使用正则的Replace方法吧。
Class POPASP_SQLITE3
	'经测试发现sqlite3的列字段类型为blob时会出错。

	'为了减轻数据库操作类的重量，特将相同代码写到了另外一个文件
	'POPASP_REPLACE_CLASS_CODE_POPASP'
	
	' 获取表的所有字段与字段类型
	Function getTableFields( ByVal table_name )
		table_name = getTrueTableName( table_name ) 
		set getTableFields = tool.getTableFields("SELECT * FROM '" & table_name	& "' LIMIT 1")
	End Function
	
	Function InsertByExecute( table,sql )
		InsertByExecute = tool.Execute( sql )
		if InsertByExecute > 0 Then
			InsertByExecute = sqlite_last_insert_rowid( table )
		End if
	End Function
	
	' 向数据表中插入数据，data为Dictionary对象，其键名与字段名相对应，如果data含主键，须手动删除
	Public Function Insert(ByVal table,ByVal data)
		table = getTableFromInput(table)
		call handlerData( table,data , 0 ) '剔除不存在的字段		
		lastSql = tool.getInsertSql( table, data , getTableStructure( table ) )
		Insert = InsertByExecute( table , lastSql )
	End Function
	
	' 向数据表中添加数据，采用sql添加
	Public Property Get Add()
		'dim pk
		call parseOptions	'解析参数		
		if is_empty(parsedOptions("table")) then 
			Call setPoptsTable( parsedOptions )	
		end if
		
		if isEmpty( parsedOptions("data") ) Then
			set parsedOptions("data") = POP_MVC.Form2Dict()

		end If
		
		if ( not is_empty( parsedOptions("data") ) ) Then
			'注意sqlite3在插入数据的时候，可以插入主键
			call handlerData( parsedOptions("table"),parsedOptions("data") , 0 )
			lastSql = getInsertSql(parsedOptions)	
			Add = InsertByExecute( parsedOptions("table") , lastSql )
		else 
			POP_MVC.error( "POPASP_SQLITE3.Add" )
		end If		
	End Property
	
	' Replace，无则添加，有则修改，此时where无效，返回结果是最后添加或修改的ID
	Public Property Get [Replace]()
		On Error Resume Next
		dim popts,pk,max,findCount	
		call parseOptions	'解析参数		
		set popts = parsedOptions
		
		if is_empty(popts("table")) then
			Call setPoptsTable( popts )	
		end if
		
		pk = getPrikey( popts("table") )	'得到主键
		if popts("data").Exists( pk ) Then	'如果数据中存在主键，则需要从是否为最大ID与是否存在该ID两方面判断
			set rs = getRS( "SELECT MAX(" & pk & ") AS theResult FROM `" & popts("table") & "` LIMIT 1")
			rs.moveFirst
			max = rs("theResult")	'得到最大ID
			tool.closeRS rs
			set rs = getRS( "SELECT * FROM `" & popts("table") & "` WHERE " & pk & " = " & popts("data")(pk) & " LIMIT 1" )
			findCount = rs.RecordCount	'以该ID查找一行记录，找到为1，找不到为0
			tool.closeRS rs
			if max >= popts("data")(pk) then	'数据ID <= 最大ID 
				if findCount > 0 then	'如果能找到，则修改
					call options.Remove("where")	
					[Replace] = popts("data")(pk)	'返回实际修改的ID
					if Save() = 0 then
						[Replace] = 0
					end if
				else	'如果找不到，插入到数据ID的位置上。返回该数据ID
					[Replace] = Add()
				end if
			else	' 超出最大ID，则添加，如果数据表被全部删除，此时生成的ID并不是1
				call options("data").Remove(pk)
				[Replace] = Add()
			end if			
		else	'不存在主键，直接添加
			[Replace] = Add()
		end if		
		call L_( Me.db_type & " Replace" )
	End Property
	
	' 得到数据库中的所有表名，返回一个对象
	' 例： {"0":"about", "1":"category", "2":"contact", "3":"feedback", "4":"post", "5":"swiper", "6":"user"}
	Public Function getTables()
		on error resume next
		dim arr,rs,content
		dim filename
		' 如果已经保存了数据，直接返回
		if not isEmpty(dTables) Then
			set getTables = dTables
			Exit Function
		End If
		'如果对应文件不存在，则须先将数据生成json保存到文件中
		filename = tool.getDataFileName("_tables_")
		if Not is_empty( POP_MVC.config("APP_DEBUG") ) OR Not tool.file_exists( filename ) Then
			call tool.initConn		
			Set rs = tool.getRS( "select name from sqlite_master WHERE type = 'table'" )	
			POP_MVC.Arr.Push arr,LCase("sqlite_master")
			Do While Not rs.EOF 
				POP_MVC.Arr.Push arr,rs("name")
				rs.MoveNext
			Loop
			tool.closeRS rs
			call tool.file_put_contents(filename, join(arr,",") )
		else		'从文件中取出数据
			content = POP_MVC.file_get_contents(tool.getFilePath(filename))
			content = mid(content,2)
			arr = split(content,",")
		End If		
		set dTables = POP_MVC.Arr.toDict(arr)
		set getTables = dTables		
		Call L_("POPASP_SQLITE3.getTables")
	End Function
	
	'获取数据表结构
	Public Function getTableStructure( ByVal tableName )
		call getTables
		tableName = getTrueTableName(tableName)
		
		if dTables.count = 0 then
			Call getTables()
		end if	
		
		set getTableStructure = tool.getTableStructure( tableName,dTables,dTS,"SELECT * FROM `" & tableName	& "` LIMIT 1" )
	End Function
	
	Function sqlite_last_insert_rowid( ByVal tableName )
		call ResumeOpts
		sqlite_last_insert_rowid = table(tableName).getField("last_insert_rowid()")
		call ResumeOpts
	End Function
	
	Function Explain( ByRef sql )
		set Explain = getRS( "Explain " & sql)
	End Function
	
	
	'清空mysql的所有数据表
	Public Property Get Truncate( ByVal table_name )
		on error resume next
		dim dbType,dbPath
		table_name = getTrueTableName( table_name )
		dbType = POP_MVC.config("DB_TYPE")	
		dbPath = POP_MVC.config("DB_PATH")	

		POP_MVC.config("DB_TYPE") = "sqlite3"
		POP_MVC.config("DB_PATH") = db_path
		P_("db").execute( "DELETE FROM " & table_name )
		P_("db").execute( "DELETE FROM sqlite_sequence WHERE name = '" & table_name & "'" )
		P_("db").execute( "UPDATE sqlite_sequence SET seq = 0 WHERE name = '" & table_name & "'" )
		POP_MVC.config("DB_TYPE") =  dbType
		POP_MVC.config("DB_PATH") = dbPath
	End Property	
	
	'将 sqlite 数据库转化成mysql数据库，只要结构不要数据，如果mysql中已经存在同名表，则不去创建
	'mysql的配置参数在配置中进行配置，并且要求mysql中已经有建好的数据库 DB_NAME
	Public Property Get toMysql4desc( ByVal dbName )
		toMysql4desc = P_("POPASP_DBCONVERT").sqlite2mysql4desc(db_path,dbName,"","")
	End Property
	
	'将 sqlite 数据库转化成mysql数据库，结构连数据，如果mysql中已经存在同名表
	'mysql的配置参数在配置中进行配置，并且要求mysql中已经有建好的数据库 DB_NAME
	Public Property Get toMysqlWithData( ByVal dbName )
		Call P_("POPASP_DBCONVERT").DropMysqlTables( dbName )
		toMysqlWithData = P_("POPASP_DBCONVERT").sqlite2mysqlWithData(db_path,dbName,"","")
	End Property
	
	Public Function  toMysqlWithData4table( Byval dbName, ByVal sqlite_prefix , ByVal mysql_prefix )
		dim table_name
		table_name = MID( tableName, len( db_prefix ) + 1 )
		Call P_("POPASP_DBCONVERT").TruncateMysqlTable( table_name,mysql_prefix )
		toMysqlWithData4table = P_("POPASP_DBCONVERT").sqlite2mysqlWithData4table(table_name ,db_path , db_name, sqlite_prefix , mysql_prefix )
	End Function	
End Class
%>