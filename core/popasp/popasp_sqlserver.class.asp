<%
' +----------------------------------------------------------------------
' | POPASP [ ASP MVC ]
' +----------------------------------------------------------------------
' | Copyright (c) 2016 http://popasp.com All rights reserved.
' +----------------------------------------------------------------------
' | Author: popasp <1737025626@qq.com>
' +----------------------------------------------------------------------

'在本类中已经使用了Replace作为方法名，所以在本类中不能再使用Replace函数了，请考虑使用正则的Replace方法吧。
Class POPASP_SQLSERVER
	'为了减轻数据库操作类的重量，特将相同代码写到了另外一个文件
	'POPASP_REPLACE_CLASS_CODE_POPASP'
	
	' 获取表的所有字段与字段类型
	Function getTableFields( ByVal table_name )
		table_name = getTrueTableName(table_name)
		set getTableFields = tool.getTableFields("SELECT TOP 1 * FROM [" & table_name & "]" )
	End Function
	
	Function InsertByTable( table,data )
		lastSql = tool.getInsertSql( table, data , getTableStructure( table ) )
		InsertByTable = tool.InsertByTable( table , data , getPrikey( table ) )
	End Function
	
	' 向数据表中插入数据，data为Dictionary对象，其键名与字段名相对应，如果data含主键，须手动删除
	Public Function Insert(ByVal table,ByVal data)
		table = getTableFromInput(table)
		call handlerData( table,data , 1 ) '剔除不存在的字段与主键
		Insert = InsertByTable( table , data )
	End Function
	
	' 向数据表中添加数据
	Public Property Get Add()
		call parseOptions	'解析参数

		if is_empty(parsedOptions("table")) then 
			Call setPoptsTable( parsedOptions )	
		end if
		
		if isEmpty( parsedOptions("data") ) Then
			set parsedOptions("data") = POP_MVC.Form2Dict()
		end If
		
		if ( not is_empty( parsedOptions("data") ) ) Then			
			'剔除不存在的字段与主键
			if isEmpty( Me.prikey ) then				
				call handlerData( parsedOptions("table"),parsedOptions("data") , 0 )
			else
				call handlerData( parsedOptions("table"),parsedOptions("data") , 1 )
			end if
			
			Add = InsertByTable( parsedOptions("table") , parsedOptions("data") )
		else 
			POP_MVC.error( "POPASP_SQLSERVER.Add" )
		end If		
		call ResumeOpts
	End Property
	
	' Replace，无则添加，有则修改，此时where无效，返回结果是最后添加或修改的ID
	Public Property Get [Replace]()
		On Error Resume Next
		dim popts,pk,max,findCount	
		call parseOptions	'解析参数		
		set popts = parsedOptions
		
		if is_empty(popts("table")) then
			Call setPoptsTable( popts )	
		end if
		
		pk = getPrikey( popts("table") )	'得到主键		
		if popts("data").Exists( pk ) Then	'如果数据中存在主键，则需要从最大ID与是否存在该ID两方面判断
			set rs = getRS( "SELECT TOP 1 MAX(" & pk & ") AS theResult FROM [" & popts("table") & "]")
			rs.moveFirst
			max = rs("theResult")	'得到最大ID
			
			tool.closeRS rs
			set rs = getRS( "SELECT TOP 1 * FROM [" & popts("table") & "] WHERE [" & pk & "] = " & popts("data")(pk) )
			findCount = rs.RecordCount	'以该ID查找一行记录，找到为1，找不到为0			
			tool.closeRS rs
			if max >= popts("data")(pk) then	'数据ID <= 最大ID 			
				if findCount > 0 then	'如果能找到，则修改
					call options.Remove("where")	
					[Replace] = popts("data")(pk)	'返回实际修改的ID
					if Save() = 0 then						
						[Replace] = 0
					end if
				else	'如果找不到，插入到数据ID的位置上。返回该数据ID
					[Replace] = Add()
				end if
			else	' 超出最大ID，则添加，如果数据表被全部删除，此时生成的ID并不是1
				call options("data").Remove(pk)
				[Replace] = Add()
			end if			
		else	'不存在主键，直接添加
			[Replace] = Add()
		end if		
		call L_( Me.db_type & " Replace" )
	End Property
	
	' 得到数据库中的所有表名，返回一个对象
	' 例： {"0":"about", "1":"category", "2":"contact", "3":"feedback", "4":"post", "5":"swiper", "6":"user"}
	Public Function getTables()
		on error resume next
		dim arr,rs,content
		dim filename
		' 如果已经保存了数据，直接返回
		if not isEmpty(dTables) Then
			set getTables = dTables
			Exit Function
		End If
		'如果对应文件不存在，则须先将数据生成json保存到文件中
		filename = tool.getDataFileName("_tables_")
		if Not is_empty( POP_MVC.config("APP_DEBUG") ) OR Not tool.file_exists( filename ) Then
			call tool.initConn		
			Set rs = tool.getRS( "SELECT [Name] FROM sysobjects" )	
			Do While Not rs.EOF 
				POP_MVC.Arr.Push arr,rs("Name")
				rs.MoveNext
			Loop
			tool.closeRS rs
			call tool.file_put_contents(filename, join(arr,",") )
		else		'从文件中取出数据
			content = POP_MVC.file_get_contents(tool.getFilePath(filename))
			content = mid(content,2)
			arr = split(content,",")
		End If		
		set dTables = POP_MVC.Arr.toDict(arr)
		set getTables = dTables		
		Call L_("POPASP_SQLSERVER.getTables")
	End Function	
	
	'获取数据表结构
	Public Function getTableStructure( ByVal tableName )
		call getTables
		tableName = getTrueTableName(tableName)
		
		if dTables.count = 0 then
			Call getTables()
		end if
		
		set getTableStructure = tool.getTableStructure( tableName,dTables,dTS,"SELECT TOP 1 * FROM [" & tableName & "]")
	End Function
	
	'清空mysql的所有数据表
	Public Property Get Truncate( ByVal table_name )
		on error resume next
		dim dbType,dbName
		table_name = getTrueTableName( table_name )
		dbType = POP_MVC.config("DB_TYPE")	
		dbName = POP_MVC.config("DB_NAME")	

		POP_MVC.config("DB_TYPE") = "sqlserver"
		POP_MVC.config("DB_NAME") = db_name
		P_("db").execute( "TRUNCATE TABLE " & table_name )
		POP_MVC.config("DB_TYPE") =  dbType
		POP_MVC.config("DB_PATH") = dbName
	End Property	
	
	'获取数据表的建表语句
	Public Function getCreateSQL( ByVal tableName )
		on error resume next
		dim primarykey,sql,obj,typs,attrib
		primarykey = getPrikey( tableName )
		set obj = P_("dbmanage")
		sql = "CREATE TABLE [" & getTrueTableName(tableName) & "] ( " & chr(10)
		Set rs = tool.conn.execute("SELECT * FROM " & getTrueTableName(tableName) )
		if err = 0 then
			for i = 0 to rs.fields.count-1
			   sql = sql & vbTab & "[" & rs(i).name & "] "
			   typs = obj.getTypeName(rs(i).type)
			   if typs = "VARCHAR" or typs = "BINARY" or typs = "CHAR" then
				 sql = sql & typs & "(" & rs(i).definedsize & ")"
			   else
				 sql = sql & typs & " "
			   end if
			   attrib = rs(i).attributes
			   if (attrib and obj.adFldIsNullable) = 0 then
				 sql = sql&" NOT NULL"
			   end if
			   if rs(i).Properties("ISAUTOINCREMENT") = True then
				 sql = sql & " IDENTITY"
			   end if
			   sql = sql & "," & chr(10)
			next
			if primarykey <> "" then
				sql = sql & vbTab & "PRIMARY KEY ([" & primarykey & "])" & chr(10)
				sql = sql & ");"
			else
				sql = Mid(sql,1,Len(sql)-2)
				sql = sql & ");"
			end if
		else
			sql = "CREATE TABLE [" & table & "];"
		end if
		rs.close : set rs = nothing
		getCreateSQL = getCreateSQL & sql		
	End Function		
End Class
%>