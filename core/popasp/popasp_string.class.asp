<%
' +----------------------------------------------------------------------
' | POPASP [ ASP MVC ]
' +----------------------------------------------------------------------
' | Copyright (c) 2016 http://popasp.com All rights reserved.
' +----------------------------------------------------------------------
' | Author: popasp <1737025626@qq.com>
' +----------------------------------------------------------------------
Class POPASP_STRING	
	''''''''''''''修改函数
	
	'采用递归，删除str左边出现的字符串find
	function [ltrim](str,find)
		[ltrim] = POP_MVC.ltrim( str,find )
	End Function
	
	'采用递归，删除str右边出现的字符串find
	function [rtrim](str,find)
		[rtrim] = POP_MVC.rtrim( str,find )
	End Function
	
	'采用递归，删除str两端出现的字符串find
	function [trim](str,find)
		[trim] = Me.rtrim(Me.ltrim(str,find),find)
	End Function
	
	'使用另一个字符串填充字符串为指定长度，两侧填充
	'input: 输入字符串。
	'pad_length: 如果 pad_length 的值是负数，小于或者等于输入字符串的长度，不会发生任何填充。
	'pad_string: 如果填充字符的长度不能被 pad_string 整除，那么 pad_string 可能会被缩短。
	function pad(ByVal input,ByRef pad_length,ByVal pad_string)
		pad = padding( input,pad_length,pad_string,"both" )
	end Function
	
	'使用另一个字符串填充字符串为指定长度，右填充
	'参数同pad
	function rpad(ByVal input,ByRef pad_length,ByVal pad_string)
		rpad = padding( input,pad_length,pad_string,"right" )
	end Function
	
	'使用另一个字符串填充字符串为指定长度，左填充
	'参数同pad
	function lpad(ByVal input,ByRef pad_length,ByVal pad_string)
		lpad = padding( input,pad_length,pad_string,"left" )
	end Function
	
	'返回字符串，此字符串与指定字符串顺序相反。
	Function Reverse( ByRef str )
		reverse = StrReverse(str)
	End Function

	' 重复一个字符串
	' input: 待操作的字符串 
	' multiplier : input 被重复的次数
	' multiplier 必须大于或等于0。如果 multiplier 被设置为 0，函数返回空字符串
	Function Repeat( ByRef input,ByRef multiplier )
		dim i
		if multiplier < 0 then
			Repeat = "" : Exit Function
		end if
		for i = 1 to multiplier
			Repeat = Repeat & input
		next
	End Function
	
	'生成当前时间戳
	Function TimeStamp()
        TimeStamp = DateDiff("s", "1970-1-1 8:00:00", Now())
    End Function
	
	'删除html标签
	'filtertype为html或jsiframe或自定义正则表达式
	'如果为html，删除所有html标签。如果是jsiframe，则只删除js与iframe标签
	'如果是自定义正则表达式，则采用gim模式进行正则替换为空。
	Function strip_tags(Byval str,Byval filtertype)
		if isNul(str) then  strip_tags="" : Exit Function
		dim pattern
		Select case filtertype
			case "html"	
				pattern="<\!--\[if [\s\S]+?<\!\[endif\]-->"
				strip_tags = Me.reg_replace( str, "" , pattern,"gim" )
				pattern="<[\/]?[^>]+?>"
				strip_tags = Me.reg_replace( strip_tags, "" , pattern,"gim" )
			case "jsiframe"
				pattern="<\!--\[if [\s\S]+?<\!\[endif\]-->"
				strip_tags = Me.reg_replace( str, "" , pattern,"gim" )
				pattern="<[\/]?(script|iframe)[^>]*?>"
				strip_tags = Me.reg_replace( strip_tags, "" , pattern,"gim" )
		end Select		
	End Function
	
	'去掉 html 标签及内容
	Function strip4html( ByVal str )
		strip4html = strip_tags( str,"html" )
	End Function
	
	
	'去掉 script 标签及内容
	Function strip4script( ByVal str )
		strip4script = Me.reg_replace( str, "" , "<script.*?<\/script\s*>" , "gim" )
	End Function
	
	'去掉 iframe 标签及内容
	Function strip4iframe( ByVal str )
		strip4iframe = Me.reg_replace( str, "" , "<iframe.*?<\/iframe\s*>" , "gim" )
	End Function	
	
	'去掉 asp 标签及内容
	Function strip4asp( ByVal str )
		strip4asp = Me.reg_replace( str, "" , "<" & "%.+?" & "%" & ">" , "gim" )
	End Function
	
	'获取区间内容，先找左侧，再找右侧
	'str为需要截取的字符串
	'leftLabel为左侧区间标签，为空的话，从起始位置截取
	'rightLabel为右侧区间标签，为空的话，截取到最后
	Function getInStr( ByRef str, ByRef leftLabel, ByRef rightLabel )
		dim left_start,right_start,startLen,startTime
		startTime = timer
		if leftLabel = "" then
			left_start = 0
		else
			left_start = inStr( str, leftLabel )
			if left_start < 1 then
				exit function
			end if
		end if		
		startLen = len( leftLabel )
		
		

		if rightLabel = "" then
			getInStr = mid( str, startLen + left_start  )
			Exit Function
		else
		
			if left_start + startLen = 0 then
				startLen = 1
			end if
			
			right_start = inStr(left_start + startLen, str,  rightLabel  )

			if right_start < 1 then
				exit function
			else
				getInStr = mid( str, startLen + left_start , right_start -  startLen - left_start )
			end if
		end if	
		call POP_MVC.pushTime( startTime , "查找string.getInStr " & leftLabel & "..." & rightLabel )
	End Function
	
	'获取区间内容，先找右侧，再找左侧
	'str为需要截取的字符串
	'leftLabel为左侧区间标签，为空的话，从起始位置截取
	'rightLabel为右侧区间标签，为空的话，截取到最后
	Function getInStrRev( ByRef str, ByRef leftLabel, ByRef rightLabel )
		dim left_start,right_start,startLen	,startTime
		startTime = timer
		if rightLabel = "" then
			right_start = len(str)
		else
			right_start = inStrRev( str, rightLabel )
			if right_start < 1 then
				exit function
			end if
		end if		
		startLen = len( leftLabel )
		
		if leftLabel = "" then
			getInStrRev = mid( str, 1, right_start - 1  )
			Exit Function
		else
			left_start = inStrRev(  str,  leftLabel , right_start )
			getInStrRev = mid( str, startLen + left_start , right_start -  startLen - left_start )
		end if	
		call POP_MVC.pushTime( startTime , "查找string.getInStrRev " & leftLabel & "..." & rightLabel )
	End Function

	
    '与javascript中的unescape()等效
    Function UnEscape(str)
        Dim x
        x=InStr(str,"%") 
        Do While x>0
            UnEscape=UnEscape & Mid(str,1,x-1)
            If LCase(Mid(str,x+1,1))="u" Then
                UnEscape=UnEscape & ChrW(CLng("&H"&Mid(str,x+2,4)))
                str=Mid(str,x+6)
            Else
                UnEscape=UnEscape & Chr(CLng("&H"&Mid(str,x+1,2)))
                str=Mid(str,x+3)
            End If
            x=InStr(str,"%")
        Loop
        UnEscape=UnEscape & str
    End Function	
	

	
	'对server.urlencode函数的解码
	function URLDecode(ByVal input)
		URLDecode = ""
		Dim sl: sl = 1
		Dim tl: tl = 1
		Dim key: key = "%"
		Dim kl: kl = Len(key)
		sl = InStr(sl, input, key, 1)
		Do While sl>0
		If (tl=1 And sl<>1) Or tl<sl Then
		URLDecode = URLDecode & Mid(input, tl, sl-tl)
		End If
		Dim hh, hi, hl
		Dim a
		Select Case UCase(Mid(input, sl+kl, 1))
		Case "U":'Unicode URLEncode
		a = Mid(input, sl+kl+1, 4)
		URLDecode = URLDecode & ChrW("&H" & a)
		sl = sl + 6
		Case "E":'UTF-8 URLEncode
		hh = Mid(input, sl+kl, 2)
		a = Int("&H" & hh)'ascii码
		If Abs(a)<128 Then
		sl = sl + 3
		URLDecode = URLDecode & Chr(a)
		Else
		hi = Mid(input, sl+3+kl, 2)
		hl = Mid(input, sl+6+kl, 2)
		a = ("&H" & hh And &H0F) * 2 ^12 Or ("&H" & hi And &H3F) * 2 ^ 6 Or ("&H" & hl And &H3F)
		If a<0 Then a = a + 65536
		URLDecode = URLDecode & ChrW(a)
		sl = sl + 9
		End If
		Case Else:'Asc URLEncode
		hh = Mid(input, sl+kl, 2)'高位
		a = Int("&H" & hh)'ascii码
		If Abs(a)<128 Then
		sl = sl + 3
		Else
		hi = Mid(input, sl+3+kl, 2)'低位
		a = Int("&H" & hh & hi)'非ascii码
		sl = sl + 6
		End If
		URLDecode = URLDecode & Chr(a)
		End Select
		tl = sl
		sl = InStr(sl, input, key, 1)
		Loop
		URLDecode = URLDecode & Mid(input, tl)
	End function
	
	
	'正则匹配结果，返回IMatchCollection2对象
	'input为需要匹配的字符串，pattern为正则匹配表达式，mode为匹配模式(gim三个字母的组合，可以为空)
	'g代表全局，即匹配所有，i代表不区分大小写，m代表可以换行。
	Function reg_exec( ByRef input, ByRef pattern, ByRef mode )
		on error resume next
		
		Call setReg( pattern,mode )	

		dim startTime : startTime = timer()		
		set reg_exec = POP_MVC.reg.Execute( input )
		if not is_empty( POP_MVC.config("SHOW_PAGE_TRACE") ) then
			call POP_MVC.pushTime( startTime , "正则匹配reg_exec " & Me.encodeHtml(pattern) )
		end if

		Call L_( "String.reg_exec: " & pattern )
	End Function
	
	'正则匹配个数
	'input为需要匹配的字符串，pattern为正则匹配表达式，mode为匹配模式(gim三个字母的组合，可以为空)
	'g代表全局，即匹配所有，i代表不区分大小写，m代表可以换行。
	Function reg_count( ByRef input, ByRef pattern, ByRef mode )
		on error resume next
		dim matches
		Call setReg( pattern,mode )	
		dim startTime : startTime = timer()		
		set matches = POP_MVC.reg.Execute( input )
		reg_count = matches.count
		set matches = nothing
		if not is_empty( POP_MVC.config("SHOW_PAGE_TRACE") ) then
			call POP_MVC.pushTime( startTime , "正则匹配reg_exec " & Me.encodeHtml(pattern) )
		end if
		Call L_( "String.reg_count" )
	End Function
	
	'获取某个字符串中第一个图片路径
	'常用来获取文章内容中的第一张图片将其作为封面图
	Function fetch_img( ByRef str )
		dim rule,Matches
		rule = "<img[^>]*?\s+src\s*=\s*((?:"")([^""]+)(?:"")|(?:')([^']+)(?:')|([^\s>]+))[^>]*>"

		If Me.reg_test(str, rule , "i") Then
		  Set Matches = Me.reg_exec(str, rule , "i")
		  fetch_img = Matches(0).subMatches(1)
		  set Matches = nothing
		End If
	End Function
	
	'获取某个字符串中第一个下载路径<a href="路径" download="文件名"></a>
	'常用来获取文章内容中的第一张图片将其作为封面图
	Function fetch_download( ByRef str )
		dim rule,Matches
		rule = "<a[^>]*?\s+download\s*=\s*((?:"")([^""]+)(?:"")|(?:')([^']+)(?:')|([^\s>]+))[^>]*>"

		If Me.reg_test(str, rule , "i") Then
		  Set Matches = Me.reg_exec(str, rule , "i")
		  fetch_download = Matches(0).subMatches(1)
		  set Matches = nothing
		End If
	End Function
	

  '为html字符串<img src="imgPath">中的imgPath添加域名
  'str一般为文章内容
  'hasTag为是否需要<img>标签
  Public Function AddHttpHost4Img(ByVal str, ByVal httpHost)
	Dim s_rule, Matches, match, s_img, s_src
	if isNul( httpHost ) then
		httpHost = POP_MVC.http_host
	end if
	httpHost = POP_MVC.Rtrim(httpHost , "/") & "/"
	'匹配img标签的正则
	s_rule = "<img[^>]*?\s+src\s*=\s*((?:"")([^""]+)(?:"")|(?:')([^']+)(?:')|([^\s>]+))[^>]*>"
	If Me.reg_test(str, s_rule , "gim") Then
	  '正则匹配所有的img标签
	  Set Matches = Me.reg_exec(str, s_rule , "gim")
	  '取出每个img标签
	  For Each match In Matches
		'取出图片标签
		s_img = match.Value
		'取出图片地址
		s_src = match.SubMatches(0)		
		'更新标签中的src地址
		if not P_("POPASP_AUTOVALIDATE").regex( s_src , "url" ) then
			s_src = POP_MVC.Trim(POP_MVC.Trim(s_src,""""),"'")
			s_img = replace( s_img, s_src , httpHost & POP_MVC.Ltrim(s_src , "/") )
			str = replace( str , match.Value , s_img )
		end if
	  Next
	End If
	AddHttpHost4Img = str
  End Function	

	'返回第一个正则匹配的结果
	'input为需要匹配的字符串，pattern为正则匹配表达式，mode为匹配模式(im两个字母的组合，g不起作用，可以为空)
	Function reg_fetch( ByRef input, ByRef pattern , mode)
		on error resume next
		if isNul( input ) then
			exit function
		end if
		dim matches
		mode = replace( mode,"g","" )
		Call setReg( pattern,mode )	
		dim startTime : startTime = timer()		
		set matches = POP_MVC.reg.Execute( input )
		if matches.count > 0 then
			reg_fetch = matches(0).value
		end if
		if not is_empty( POP_MVC.config("SHOW_PAGE_TRACE") ) then
			call POP_MVC.pushTime( startTime , "正则匹配reg_fetch " & Me.encodeHtml(pattern) )
		end if

		Call L_( "String.reg_fetch:" & pattern )
	End Function	
	
	'返回第一个正则匹配的结果，num为括号匹配项，为1,2,……	'input为需要匹配的字符串，pattern为正则匹配表达式，num为匹配项，mode为匹配模式(im两个字母的组合，g不起作用，可以为空)
	Function reg_find( ByRef input, ByRef pattern , ByRef num , mode)
		on error resume next
		dim matches
		mode = replace( mode,"g","" )
		Call setReg( pattern,mode )	
		dim startTime : startTime = timer()		
		set matches = POP_MVC.reg.Execute( input )
		reg_find = ""

		if matches.count > 0 then		
			if matches(0).subMatches.count >= num then
				reg_find = matches(0).subMatches(num-1)
			end if
		end if
		if not is_empty( POP_MVC.config("SHOW_PAGE_TRACE") ) then
			call POP_MVC.pushTime( startTime , "正则匹配 reg_find " & Me.encodeHtml(pattern) )
		end if
		Call L_( "String.reg_find " & pattern )
	End Function
	
	'以数组形式返回匹配的结果
	'input为需要匹配的字符串，pattern为正则匹配表达式，num为匹配项，mode为匹配模式(gim字母的组合)
	Function reg_array( ByRef input, ByRef pattern , ByRef num , mode)
		on error resume next
		dim matches,match,ret
		Call setReg( pattern,mode )	
		dim startTime : startTime = timer()		
		set matches = POP_MVC.reg.Execute( input )
		ret = Array()
		
		if matches.count > 0 then	
			for each match in matches
				if isNul( num ) then
					POP_MVC.Arr.push ret,match.value
				else
					POP_MVC.Arr.push ret,match.subMatches(num-1)
				end if
			next
		end if
		if not is_empty( POP_MVC.config("SHOW_PAGE_TRACE") ) then
			call POP_MVC.pushTime( startTime , "正则匹配 reg_array " & Me.encodeHtml(pattern) )
		end if
		
		reg_array = ret
		Call L_( "String.reg_array" )
	End Function	
	
	'返回是否正则匹配
	'input为需要匹配的字符串，pattern为正则匹配表达式，mode为匹配模式(gim三个字母的组合)
	Function reg_test( ByRef input, ByRef pattern, ByRef mode )
		on error resume next
		Call setReg( pattern,mode )	
		dim startTime : startTime = timer()
		reg_test = POP_MVC.reg.Test( input )
		if not is_empty( POP_MVC.config("SHOW_PAGE_TRACE") ) then 
			call POP_MVC.pushTime( startTime , "正则查找 reg_test " & Me.encodeHtml(pattern))
		end if
		Call L_( "String.reg_test" )
	End Function
	
	'进行正则替换
	'input为需要匹配的字符串，sReplace为以何字符串代替，pattern为正则匹配表达式，mode为匹配模式(gim三个字母的组合，可以为空)
	Function reg_replace( ByRef input, Byref sReplace ,ByRef pattern, ByRef mode)
		on error resume next
		Call setReg( pattern,mode )		
		dim startTime : startTime = timer()		
		reg_replace = POP_MVC.reg.Replace( input, CStr(sReplace) )
		if not is_empty( POP_MVC.config("SHOW_PAGE_TRACE") ) then 
			call POP_MVC.pushTime( startTime , "正则替换 reg_replace " & Me.encodeHtml(pattern))
		end if
		Call L_( "String.reg_replace" )
	End Function
	
	'正则分割，返回一个数组
	'input为需要匹配的字符串，pattern为正则匹配表达式，mode为匹配模式(im三个字母的组合，g不起作用，可以为空，但仍会自动加m)
	Function reg_split( ByVal input, ByVal pattern , ByRef mode)
		on error resume next
		dim startTime : startTime = timer()	
		dim uniqStr
		
		mode = replace( mode,"g","" )
		mode = mode & "g"	
		
		uniqStr = "PoPaSp" & uniqid & "pOpAsP"		
		input = Me.reg_replace( input , uniqStr, pattern, mode )

		reg_split = split( input , uniqStr )
		call POP_MVC.pushTime( startTime , "正则分割 reg_split " & pattern)
		Call L_( "String.reg_split" )
	End Function
	
	'替换正则表达式编组
	'说明：按正则表达式的规则替换一个字符串中某个捕获编组的内容
	'示例：reg_replace2("photo-3.html", "^(\w+)-(\d+)\.html$", "$2", "4")
	'     返回： photo-4.html
	Public Function reg_replace2(ByVal str, ByVal rule, ByVal group, ByVal replaceWith)
		
		If Not Me.reg_test(str, rule,"gim") Then
		  '如果规则不匹配则直接返回字符串
		  reg_replace2 = str
		  Exit Function
		End If
		Dim o_match, i, j, s_match, i_pos, s_left, s_tmp,matches
		'获取编组号
		
		i = Int(Mid(group,2))-1
		set matches = Me.reg_exec(str,rule,"gim")		
		'取得正则编组
		Set o_match = matches(0)		
		
		'循环编组查找匹配项
		For j = 0 To o_match.SubMatches.Count-1
		  s_match = o_match.SubMatches(j)		  
		  '取得当前组的字符开始位置
		  i_pos = Instr(str,s_match)
		  If i_pos > 0 Then
			'把字符串按当前组的位置分为两部分
			s_tmp = Left(str,i_pos-1)
			str = Mid(str,Len(s_tmp)+1)		
			'如果找到匹配的编组号则仅替换本组中的字符串
			If i = j Then
			  '把替换后的字符串和前一部分组合起来
			  reg_replace2 = s_left & s_tmp & Replace(str,s_match,replaceWith,i_pos-len(s_tmp),1,0)
			  Exit For
			End If
			'如果没有找到匹配则把当前组的字符串换到前一部分中去
			s_left = s_left & s_tmp & s_match
			'在后面部分的字符串中继续下一次扫描匹配
			str = Mid(str, Len(s_match)+1)
		  End If
		Next
		
		Set o_match = Nothing
	End Function	
	'''''''''''''''判断函数
	
	'判断字符串是否存在另一字符串中存在
	Function Exists( input,find )
		Exists = (inStr( input,find ) > 0)
	End Function
	
	'判断字符串是否存在另一字符串中存在，忽略大小写
	Function iExists( input,find )
		iExists = (inStr( LCase(input),LCase(find) ) > 0)
	End Function
	
	'判断某字符串是否以另一字符串开头
	Function StartsWith( ByVal input,ByVal find )
		StartsWith =  (cmp( left(input,len( find )) , find ) = 0)
	End Function
	
	'判断某字符串是否以另一字符串开头，忽略大小写
	Function iStartsWith( ByVal input,ByVal find )
		iStartsWith =  (casecmp( left(input,len( find )) , find ) = 0)
	End Function
	
	'判断某字符串是否以另一字符串结尾
	Function EndsWith( ByVal input,ByVal find )
		EndsWith =  (cmp( right(input,len( find )) , find ) = 0)
	End Function
	
	
	'判断某字符串是否以另一字符串结尾，忽略大小写
	Function iEndsWith( ByVal input,ByVal find )
		iEndsWith =  (casecmp( right(input,len( find )) , find ) = 0)
	End Function
	
	'''''''''''''''比较函数
	
	'判断两字符串是否相同
	Function Equal( ByVal str1,ByVal str2 )
		Equal =  (cmp( str1,str2 ) = 0)
	End Function
	
	'判断两字符串是否相同，忽略大小写
	Function iEqual( ByVal str1,ByVal str2 )
		iEqual =  (casecmp( str1,str2 ) = 0)
	End Function
	
	'将字符串的首字母转换为大写
	Function UCFirst(ByRef str)
		Dim first
		if str="" Then
			UCFirst = ""
		Else
			first = Mid(str,1,1)
			UCFirst = UCase(first) & Mid(str,2)
		End If		
	End Function
	
	'将字符串的首字母转换为小写
	Function LCFirst(ByRef str)
		Dim first
		if str="" Then
			LCFirst = ""
		Else
			first = Mid(str,1,1)
			LCFirst = LCase(first) & Mid(str,2)
		End If		
	End Function
	


	'查找字符串的首次出现，返回字符串的一部分或者 Empty（如果未发现）
	Function str( ByRef input ,ByRef find )
		Dim pos
		pos = inStr(input,find)
		if pos>0 then str = mid( input,pos )
	End Function

	'查找字符串的最后一次出现，返回字符串的一部分或者 Empty（如果未发现）	
	Function rstr( ByRef input ,ByRef find )
		Dim pos
		pos = inStrRev( input,find )
		if pos>0 then rstr = mid( input,pos ) 
	End Function
	
	'input必须是IP
	Function markIp( ByVal input )
		dim arr,i
		if isNul(input) then exit Function
		if inStr(input,".") < 1 then markIp = input : exit function
		arr = split( input , "." )
		markIp = arr( 0 ) & "." & arr(1) & ".***.***"
	End Function

	
	'使用另一个字符串填充字符串为指定长度
	'input: 输入字符串。
	'pad_length: 如果 pad_length 的值是负数，小于或者等于输入字符串的长度，不会发生任何填充。
	'pad_string: 如果填充字符的长度不能被 pad_string 整除，那么 pad_string 可能会被缩短。
	'action: 填充方向，可为left、right、both
	Private function padding(ByVal input,ByRef pad_length,ByVal pad_string,ByRef action)
		dim input_length,padding_length
		input = CStr(input)
		input_length = len(input)
		if pad_length <= input_length Then
			padding = input : exit function
		end if
		
		pad_string = CStr(pad_string)
		if pad_string = "" then pad_string = " "		
		padding_length = len( pad_string )
		
		'循环填充
		do until input_length >=  pad_length
			if action = "left" then
				input = pad_string & input				
			else
				input = input & pad_string	
			end if
			
			input_length = input_length + padding_length
			
			'两侧填充时，得实时检测其长度
			if action = "both" and input_length < pad_length then
				input = pad_string & input
				input_length = input_length + padding_length				
			end if
		loop
		
		'填充长了，还得修剪
		if input_length > pad_length then
			if action = "left" then
				input = right( input,pad_length )				
			else
				input = left( input,pad_length )
			end if
		end if
		padding = input
	end Function
	
	'对正则匹配模式进行设置
	Private Sub setReg(ByRef pattern, ByRef mode)
		'忽略大小写
		If iExists(mode,"i") Then 
			POP_MVC.reg.IgnoreCase = true
		Else
			POP_MVC.reg.IgnoreCase = false 
		End If
		
		'全部
		If iExists(mode,"g") Then 
			POP_MVC.reg.Global = true
		Else
			POP_MVC.reg.Global = false 
		End If
		
		'多行
		If iExists(mode,"m") Then 
			POP_MVC.reg.MultiLine = true
		Else
			POP_MVC.reg.MultiLine = false 
		End If
		
		POP_MVC.reg.Pattern = pattern
	End Sub	
	
	
	'server.urlencode的改写
	Function URLEncode(ByVal input)
		URLEncode = Server.URLEncode(input)
	End Function
	
	Function decodeHtml(Byval str)
		IF isNull(str) then 
			decodeHtml = ""
			exit function
		end if
			str=replace(str,"&lt;","<")
			str=replace(str,"&gt;",">")
			str=replace(str,"&quot;",CHR(34))
			str=replace(str,"&apos;",CHR(39))
			str=replace(str,"&#160;"," ")
			decodeHtml=str
	End Function
	
	Function encodeHtml(Byval str)
		if isNul( str ) then 
			encodeHtml = ""
			exit function
		end if
			str=replace(str,"<","&lt;")			
			str=replace(str,">","&gt;")
			str=replace(str,CHR(34),"&quot;")
			str=replace(str,CHR(39),"&apos;")
			encodeHtml=str
	End Function
	
	'将HTML文本转换为HTML代码
	'比如将<替换为&lt;
	Public Function HtmlDecode(ByVal str)
		If len(str) > 0 Then
			str = Replace(str, CHR(39),"&apos;")
			str = Replace(str, CHR(34),"&quot;")
			str = Replace(str, "<" , "&lt;")
			str = Replace(str, ">" , "&gt;")
			str = Replace(str, " " , Chr(32))
		End If
		HtmlDecode = str
	End Function
	
	'对字符串进行json的字符替换，比如将\替换为\\，将/替换为\/等等
	Function jsonEncode(ByVal val)
		val = Replace(val, "\", "\\")
		val = Replace(val, """", "\""")
		'val = Replace(val, "/", "\/")
		val = Replace(val, Chr(8), "")
		val = Replace(val, Chr(12), "")
		val = Replace(val, Chr(10), "")
		val = Replace(val, Chr(13), "")
		val = Replace(val, Chr(9), "")
		jsonEncode = Me.Trim(val," ")
	End Function

	'对字符串进行vbs的字符替换
	Function vbsEncode(ByVal val)
		if typename(val) <> "String" then
			vbsEncode = val
			Exit Function
		end if
		val = Replace(val, """", """""")
		'val = Replace(val, "/", "\/")
		val = Replace(val, Chr(8), "")
		val = Replace(val, Chr(12), "")
		val = Replace(val, Chr(10), "")
		val = Replace(val, Chr(13), "")
		val = Replace(val, Chr(9), "")
		vbsEncode = Me.Trim(val," ")
	End Function	
  
	'''''''''''''''排序函数
	
	'将字符串中的字符按升序排列
	Function sort(ByRef str)
		dim arr		
		arr = explode(str)
		call POP_MVC.Arr.sort( arr )
		sort = join(arr,"")		
	End Function
	
	'将字符串中的字符按降序排列
	Function rsort(ByRef str)
		dim arr		
		arr = explode(str)
		call POP_MVC.Arr.rsort( arr )
		rsort = join(arr,"")		
	End Function
	
	'按自定义函数将字符串中的字符排列
	Function usort( ByRef str , ByRef FuncComp)
		dim arr		
		arr = explode(str)		
		call POP_MVC.Arr.usort( arr , funcComp)	
		usort = join(arr,"")	
	End Function	
	
	''''''''''''''其他函数
	
	'将字符串按每N(默认为1)个字符炸成一个数组
	Function explode( ByRef mixed )
		dim str,separator,iBound,i,length,arr
		
		separator = 1
		if isArray( mixed ) then	'如果为数组
			ibound = ubound( mixed )
			if ibound < 0 then	'数组中无元素，则返回该空数组
				explode = mixed
				exit Function
			end if
			str = mixed(0)
			if ibound > 0 then	'默认第二个元素为步长
				separator = mixed(1)
			end if
		else	'默认为字符串，如果传递其他类型的值可能会出错
			str = mixed
		end if
		length = len(str)
		
		arr = Array()
		if isNumeric( separator ) then
			for i = 1 to length step separator
				POP_MVC.Arr.push arr,mid(str,i,separator)
			next
		elseif typename( separator ) = "String" then
			arr = split( str,separator )
		end if
		explode = arr
	End Function
	
	'计算字符串的 MD5 散列值，使用了Easp的插件
	Function md5(ByRef str)		
		err.clear
		on error resume next
		dim startTime	
		startTime = timer()
		With CreateObject("MSXML.DOMDocument").createElement("a")
			.dataType = "bin.hex"
			.nodeTypedvalue = CreateObject("System.Security.Cryptography.MD5CryptoServiceProvider").ComputeHash_2(CreateObject("System.Text.UTF8Encoding").GetBytes_4(str))
			md5 = .text
		End With
		if err.number <> 0 then
			md5 = P_("POPASP_MD5").To32( str )
		end if
		call POP_MVC.pushTime( startTime , "MD5 " & md5 & " ( length : " & byte2size(len(str)) & ")" )	
	End Function
	
	'从left_到right_中随机取一整数
	Function Rand( ByRef left_,ByRef right_ )
		if right_ > left_ then
			Randomize
			Rand = Int( ( ( right_ - left_ + 1 ) * Rnd ) + 1 )
		end if
	End Function
	
    ' 产生随机字串
    ' @param string randLen 长度
    ' @param string randType 字串类型
    ' 0 字母 1 数字 2 大写字母 3 小写字母 4 汉字 其它 大小写字母与数字混合
    ' @return string	
	Function Random( ByRef randLen,ByRef randType )
		dim chars_len,chars,i
		
		if randLen < 1 then
			Random = ""
			Exit Function
		end if
		
		Select case randType
			case 0	'字母
				chars = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz"
			case 1	'数字
				chars = Repeat(  "0123456789" , 3 )
			case 2	'大写字母
				chars = "ABCDEFGHIJKLMNOPQRSTUVWXYZ"
			case 3 ' 小写字母
				chars = "abcdefghijklmnopqrstuvwxyz"
			case 4
				chars = POP_MVC.file_get_contents( POP_MVC.mvc_dir & "Tpl/commonChineseCharacter.txt" )
			case else
				chars = "ABCDEFGHIJKMNPQRSTUVWXYZabcdefghijkmnpqrstuvwxyz23456789"
		End Select	

		if randLen > 10 then	'位数过长重复字符串一定次数
			if randType = 1 then
				chars = Repeat( chars,randLen )
			else
				chars = Repeat( chars, 5 )
			end if
		end if
		
		if randType <> 4 then
			chars = Shuffle( chars )
			Random = mid( chars,1,randLen )
		else
			Random = ""
			chars_len = len( chars ) + 1			
			for i = 1 to randLen
				Randomize
				Random = Random & mid( chars, int(Rnd() * chars_len),1 )
			next
		end if
	End Function	
	
	'移除字符串中重复的字符
	Function unique(ByRef str)
		dim arr		
		arr = explode(str)
		arr = POP_MVC.Arr.unique( arr )		
		unique = join(arr,"")		
	End Function
	
	'将字符串中的字符打乱
	Function Shuffle( ByRef str )
		dim arr
		arr = explode(str)
		call POP_MVC.Arr.Shuffle( arr )
		Shuffle = join(arr,"")
	End Function
	
	'取得一个唯一ID
	Function Uniqid( )
		dim theTime
		theTime = now
		Uniqid = right(Year( theTime ),2) & Me.lpad(Month( theTime) , 2 , "0" ) & Me.lpad(Day( theTime) , 2 , "0" ) & Me.lpad(Int(timer) , 5 , "0" )
		Randomize
		Uniqid = Uniqid & Me.lpad(Int( rnd * 10000),4,"0")
	End Function
	
	'取得一个唯一ID
	Function Dateid( )
		dim theTime
		theTime = now
		Dateid = right(Year( theTime ),2) & Me.lpad(Month( theTime) , 2 , "0" ) & Me.lpad(Day( theTime) , 2 , "0" ) & Me.lpad(Int(timer) , 5 , "0" )
	End Function
	
	' 二进制安全比较字符串大小
	function cmp( ByRef str1,ByRef str2 )
		cmp = strComp( str1, str2 )
	End Function
	
	' 二进制安全比较字符串大小，忽略大小写
	function casecmp( ByRef str1,ByRef str2 )
		casecmp = cmp( LCase(str1), LCase(str2) )
	End Function
	
	'进行版本号比较，仅比较数值
	'vStr1与vStr2是含有版本号的字符串
	'如果正则表达式pattern为空，则取"\d+(\.\d+){1,3}"
	function versioncmp( ByVal vStr1, ByVal vStr2 , pattern )
		dim arr1,arr2,cnt1,cnt2,i,cnt,bool,v1,v2
		if isNul(pattern) then
			pattern = "\d+(\.\d+){1,3}"
		end if
		vStr1 = Me.reg_fetch( vStr1, pattern , "" )
		vStr2 = Me.reg_fetch( vStr2, pattern , "" )		
		arr1 = split( vStr1, "." )
		arr2 = split( vStr2, "." )
		cnt1 = ubound( arr1 )
		cnt2 = ubound( arr2 )

		if cnt1 > cnt2 then
			cnt = cnt1
			bool = 1
		elseif cnt1 < cnt2 then 
			cnt = cnt2
			bool = -1
		elseif cnt1 = cnt2 then 
			cnt = cnt1
			bool = 0
		end if
		
		versioncmp = 0 
		for i = 0 to cnt
			if bool = 1 then
				v1 = arr1( i )
				if i > cnt2 then
					v2 = 0
				else
					v2 = arr2( i )					
				end if
			elseif bool = -1 then
				v2 = arr2( i )
				if i > cnt1 then
					v1 = 0
				else
					v1 = arr1( i )
				end if
			else
				v1 = arr1( i )
				v2 = arr2( i )
			end if
			
			versioncmp = v1 - v2
			if versioncmp <> 0 then
				exit for
			end if			
		next
	End Function
	
	'比较当前版本是否在低版与高版之间[lowVer,highVer)
	function verBetween( curVer, lowVer, highVer )
		if versioncmp(curVer , lowVer , "" ) >=0 and versioncmp(curVer , highVer , "" ) < 0 then
			verBetween = true
		else
			verBetween = false
		end if
	End Function
	
	'进行版本号比较，仅比较数值
	'其实就是省略了第三个参数的versioncmp
	function vercmp( ByVal vStr1, ByVal vStr2 )		
		vercmp = versioncmp( vStr1 ,vStr2 , "" )
	end function
	
	'二进制安全比较字符串开头的若干个字符
	function ncmp( ByRef str1,ByRef str2,ByRef length)
		ncmp = cmp( left(str1,length) , left(str2,length) )
	end Function
	
	'二进制安全比较字符串开头的若干个字符，忽略大小写
	function ncasecmp( ByRef str1,ByRef str2, ByRef length)
		ncasecmp = ncmp( LCase(str1) , LCase(str2),length )
	end Function
	
	'使用自然排序算法比较字符串，忽略大小写
	function casenatcmp(ByVal left,ByVal right )
		casenatcmp = natcmp(LCase(left),LCase(right) )
	End Function

	'使用自然排序算法比较字符串
	function natcmp(ByVal left,ByVal right) 
		dim reg
		dim lTest,rTest,matches,test
		set reg = POP_MVC.reg
		reg.Global = false : reg.IgnoreCase = true : reg.MultiLine = false
		while (len(left) > 0) and (len(right) > 0)			
			reg.Pattern = "^([^0-9]*)([0-9].*)$"
			'第一步，以数字为分界，将字符串分成两部分，先比较第一部分			
			if reg.test(left) then			
				set matches = reg.execute( left )
				lTest = matches(0).subMatches(0)
				left = matches(0).subMatches(1) 
			else
			  lTest = left
			  left =  ""
			end if

			if reg.test(right) then
				set matches = reg.execute( right )
				rTest = matches(0).subMatches(0)
				right = matches(0).subMatches(1) 		
			else
				rTest = right
				right = ""
			end if
			
			
			'进行字符串比较，如果值不为0，则返回该值
			test = cmp( lTest , rTest )
			If test<>0 Then
			  natcmp = test
			  Exit Function
			End If
			
			
			'第二步，将分出来的第二部分再用数字进行分割，并比较数字部分
			reg.Pattern = "^([0-9]+)([^0-9].*)?$"			
			if reg.test(left) Then
				set matches = reg.execute(left)
				lTest = CLng( matches(0).subMatches(0) )
				left =  matches(0).subMatches(1)
			else
				lTest = 0
			End if
			
			if reg.test(right) then
				set matches = reg.execute(right)
				rTest = CLng( matches(0).subMatches(0) )
				right = matches(0).subMatches(1)
			else
			  rTest = 0
			end if
			'var_expot lTest & "-" & rTest & vbcrlf
			'进行数字比较，如果值不为0，则返回该值
			test = lTest - rTest			
			if test<> 0 Then
				natcmp = test
				exit Function
			end if
		Wend
		natcmp = cmp( left,right )
		set matches = nothing
	End Function

	'获取A:B中的A
	Function Before( byref str,byref separator )
		dim pos
		pos = inStr( str , separator )
		if pos > 0 then
			Before = mid(str,1,pos-len(separator))
		else 
			Before = str
		end if
	End Function
	
	
	'获取A:B中的B
	Function After( byref str,byref separator )
		dim pos
		pos = inStr( str , separator )
		if pos > 0 then
			After = mid(str,pos + len(separator) )
		else 
			After = str
		end if
	End Function
	
	'返回字符串的长度，中文算两个字符
	Function cnlen(str)
		Dim i,n : n = 0
		For i = 1 To Len(str)
			if Abs(Ascw(Mid(str,i,1)))>255 then
				n = n + 2
			else
				n = n + 1
			end if
		Next
		cnlen = n
	End Function
	
	'过滤HTML标签
	Public Function HtmlFilter(ByVal str)
		if str <> "" then
			str = Me.reg_replace(str,"","<[^>]+>","gm")
			str = Replace(str, ">", "&gt;")
			str = Replace(str, "<", "&lt;")
		End If
		HtmlFilter = str
	End Function
	
	'压缩html
	Public Function HtmlCompress(ByVal content)	
		content = Me.reg_replace(content,"","[\t]","gm")
		content = Me.reg_replace(content,"","^(\s*)$\n\r","gm")
		'content = Me.reg_replace(content,"","($\s*$)|(^\s*^)|(\s(?=\s))","gm")
		'content = Me.reg_replace(content,vbCrLf,"($\s+^)|(^\s+$)","gm")
		content = Me.reg_replace(content,"> <",">\s+<","gm")
		content = Me.reg_replace(content,">",">(\s+\n|\r)","gm")	
		HtmlCompress = content
	End Function
	
	'截取长字符串的指定长度并以...代替
	'半角字符以半个字符计，返回的字符串最大长度为strlen
	'因为字体不同，该函数并不能保证所截出的字符串长短一致，最好还是使用CSS进行截取
	Public Function Cut(ByVal s, ByVal strlen)
		if isNul(s) then Cut = "" : Exit Function
	
		'去除html标签、换行和制表符
		s = Me.HtmlFilter(s)
		s = replace(s, vbCrLf,"")
		s = replace(s, vbTab, "")
		
		if is_empty( strlen ) then Cut = s : Exit Function
		
		if strlen < 1 then
			strlen = len(s) * strlen
		end if
		
		strlen = Cint(strlen)
	
		Dim suffix , i, n 
		
		suffix = "..."

		'解决中英文混排长度
		n = 0 
		Cut = s
		For i = 1 To Len(s)			
			if Abs(Ascw(Mid(s,i,1)))>255 then
				n = n + 2
				If n > strlen-1 Then
					Cut = Left(s, i) & suffix
					Exit For
				End If
			else
				n = n + 1
				If n > strlen Then
					Cut = Left(s, i) & suffix
					Exit For
				End If
			end if
		Next
	End Function
	
	'反向截取长字符串的指定长度并以...代替
	'半角字符以半个字符计，返回的字符串最大长度为strlen
	'因为字体不同，该函数并不能保证所截出的字符串长短一致，最好还是使用CSS进行截取
	Public Function rCut(ByVal s, ByVal strlen)
		if isNul(s) then Cut = "" : Exit Function
	
		'去除html标签、换行和制表符
		s = Me.HtmlFilter(s)
		s = replace(s, vbCrLf,"")
		s = replace(s, vbTab, "")
		
		if is_empty( strlen ) then rCut = s : Exit Function
		
		strlen = Cint(strlen)
	
		Dim suffix , i, n 
		
		suffix = "..."

		'解决中英文混排长度
		n = 0 
		rCut = s
		For i = Len(s) To 1 step -1			
			if Abs(Ascw(Mid(s,i,1)))>255 then
				n = n + 2
				If n > strlen-1 Then
					rCut =   suffix & right(s, len(s)-i)
					Exit For
				End If
			else
				n = n + 1
				If n > strlen Then
					rCut =   suffix & right(s, len(s)-i)
					Exit For
				End If
			end if
		Next
	End Function
	
	'查找字符串find在str中出现的次数
	Function findCount( str,find )
		if isNul(str) then findCount = 0 : exit function
		if inStr(str,find)<1 then findCount = 0 : exit function
		dim i,pos,curPos,findLen,strlen
		i = 0
		curPos = 1
		findLen = len(find)
		strlen = len(str)
		
		do
			pos = inStr( curPos , str , find )
			curPos = pos + findLen 
			if pos > 0 then			
				i = i + 1
			else 
				exit do
			end if
		loop while pos > 0 and curPos < strlen
		findCount = i
	End Function
	
	'返回带前缀prefix的字符串str
	'前缀iAspCms,则iAspCms_Sort与Sort都返回iAspCms_Sort
	Function getPrefixStr( ByRef str ,ByRef prefix )
		if not Me.iStartsWith( str , prefix ) then
			getPrefixStr  = prefix & str
		else
			getPrefixStr = str
		end if
	End Function
	  
	  
	'将Byte()类型转化为字符串
	Function byte2str(bytes, charset) 
		on error resume next
		dim oStrm	
		set oStrm = POP_MVC.CreateStream
		With oStrm
		  .Type = 1
		  .Mode =3
		  .Open
		  .Write bytes
		  .Position = 0
		  .Type = 2
		  .Charset = charset
		  byte2str = .ReadText
		  .Close
		End With		
		set oStrm = nothing
		Call L_("POPASP_STRING.byte2str")
	End Function
	
	'===============
	'加密
	'===============
	Function vbsEscape(str)
		 dim i,s,c,a
		 s=""
		 For i=1 to Len(str)
			 c=Mid(str,i,1)
			 a=ASCW(c)
			 If (a>=48 and a<=57) or (a>=65 and a<=90) or (a>=97 and a<=122) Then
				 s = s & c
			 ElseIf InStr("@*_+-./",c)>0 Then
				 s = s & c
			 ElseIf a>0 and a<16 Then
				 s = s & "%0" & Hex(a)
			 ElseIf a>=16 and a<256 Then
				 s = s & "%" & Hex(a)
			 Else
				 s = s & "%u" & Hex(a)
			 End If
		 Next
		 vbsEscape = s
	End Function
	
	'===============
	'解密
	'===============
	Function vbsUnEscape(str)
		 dim i,s,c
		 s=""
		 For i=1 to Len(str)
			 c=Mid(str,i,1)
			 If Mid(str,i,2)="%u" and i<=Len(str)-5 Then
				 If IsNumeric("&H" & Mid(str,i+2,4)) Then
					 s = s & CHRW(CInt("&H" & Mid(str,i+2,4)))
					 i = i+5
				 Else
					 s = s & c
				 End If
			 ElseIf c="%" and i<=Len(str)-2 Then
				 If IsNumeric("&H" & Mid(str,i+1,2)) Then
					 s = s & CHRW(CInt("&H" & Mid(str,i+1,2)))
					 i = i+2
				 Else
					 s = s & c
				 End If
			 Else
				 s = s & c
			 End If
		 Next
		 vbsUnEscape = s
	End Function
	
	'十三加密解密
	Function ShiSan(input)
		dim i,newStr
		input = Replace(input, "╁", """")
		For i = 1 To Len(input)
			If Mid(input, i, 1) <> "╋" Then
			newStr = Mid(input, i, 1) & newStr
		Else
			newStr = vbCrLf & newStr
		End If
		Next
		ShiSan = newStr
	end Function
End Class
%>