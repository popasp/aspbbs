<%
' +----------------------------------------------------------------------
' | POPASP [ ASP MVC ]
' +----------------------------------------------------------------------
' | Copyright (c) 2016 http://popasp.com All rights reserved.
' +----------------------------------------------------------------------
' | Author: popasp <1737025626@qq.com>
' +----------------------------------------------------------------------
' 项目布置类
Class POPASP_STRUCTRUE
	private appPath
	private sub Class_Initialize		
		appPath = POP_MVC.appPath
	end sub
	
	sub handle
		if not folder_exists(appPath) Then
			call mkDir( appPath )		
			call mkDir( appPath & "/Common" )		
			call mkDir( appPath & "/Conf" )		
			call mkDir( appPath & "/Controller" )		
			call mkDir( appPath & "/Model" )		
			call mkDir( appPath & "/Runtime" )		
			call mkDir( appPath & "/Tpl" )		
			call mkDir( appPath & "/Runtime/Cache" )	
			call mkDir( appPath & "/Runtime/Class" )	
			call mkDir( appPath & "/Runtime/Compile" )	
			call mkDir( appPath & "/Runtime/Data" )
			call mkDir( appPath & "/Runtime/Log" )
			
			call copyFile( POP_MVC.mvc_dir & "Tpl/default_index.html" , appPath & "/Controller/Index.asp" )
			call copyFile( POP_MVC.mvc_dir & "Tpl/default_config.html" , appPath & "/Conf/config.asp" )
			call copyFile( POP_MVC.mvc_dir & "Tpl/default_common.html" , appPath & "/Common/common.asp" )
		end if
	End sub	
	
	sub copyFile( src,dst )
		if file_exists(src) and not file_exists(dst) Then
			POP_MVC.fso.copyfile POP_MVC.realPath(src),POP_MVC.realPath(dst)
		end if
	end Sub
	
	sub mkDir( folderPath )
		if not folder_exists( folderPath ) then
			POP_MVC.CreateFolder( folderPath )
		end if
	end Sub
	
	Private Function file_exists( filePath )
		file_exists = POP_MVC.fso.FileExists( POP_MVC.realPath(filePath))
	end Function
	
	Private Function folder_exists( folderPath )
		folder_exists = POP_MVC.fso.FolderExists( POP_MVC.realPath(folderPath) )
	end Function
End Class
%>