<%
' +----------------------------------------------------------------------
' | POPASP [ ASP MVC ]
' +----------------------------------------------------------------------
' | Copyright (c) 2016 http://popasp.com All rights reserved.
' +----------------------------------------------------------------------
' | Author: popasp <1737025626@qq.com>
' +----------------------------------------------------------------------
Class POPASP_TEMPLATE
	' The name of the diretory where templates are located
	' @var string
	public template_dir
	
    ' The name of the directory for cache files.
    ' @var string
    public cache_dir
	
    ' The left delimiter used for the template tags.
    '	用于Simtpl_Compiler类
    ' @var string
    public left_delimiter

    ' The right delimiter used for the template tags.
    '	用于Simtpl_Compiler类
    ' @var string
    public right_delimiter
	
    ' This is the number of seconds cached content will persist.
    ' <ul>
    '  <li>-1 = 永久缓存</li>
    '  <li> 0 = 不缓存</li>
    '  <li> N = 缓存N秒钟</li>	 
    ' </ul>
    ' @var integer
    public cache_lifetime
	
	' 模板文件名的后缀
	public tpl_suffix
	
	
	' 是否使用带有控制器样式的tpl名称
	public use_ctrl_tpl
	
	' 文件是否过期的数字表现形式，-1是重新生成，0为过期，1为在缓存期内
	Private is_cached__
	
	Private Pub_tpl,Pub_id,classType

	

	
    ' test to see if valid cache exists for this template
    ' @param string id
	' if cached return false , or true
	Public function expired( byval id )
		dim tpl
		if cache_lifetime <= 0 then
			expired = true
			exit function
		end if
		
		'模板文件为入口文件名
		tpl = POP_MVC.File.baseName( array( Request.ServerVariables( "SCRIPT_NAME" ) , true ) )	
		
		'过滤掉不能为文件名的字符
		if id <> "" then
			id = POP_MVC.String.reg_replace( id , "" , "[:|/\\*<>?""]" , "g" )
		end if
		
		Pub_tpl = tpl
		Pub_id = id
		
		expired = ( not is_data_cached( tpl,id ) )
		
		call data_cache_get_contents( tpl ,id )
	End Function
	
    ' test to see if valid cache exists for this template
    ' @param string id
	' if cached return true , or false
	Public function is_cached( byval id )
		dim tpl
		
		'模板文件为入口文件名
		tpl = POP_MVC.File.baseName( array( Request.ServerVariables( "SCRIPT_NAME" ) , true ) )	
		
		'过滤掉不能为文件名的字符
		if id <> "" then
			id = POP_MVC.String.reg_replace( id , "" , "[:|/\\*<>?""]" , "g" )
		end if
		
		is_cached = is_data_cached( tpl,id )
	End Function
	
	
    ' test to see if valid cache exists for this template
    ' @param string id
	' if cached return true , or false
	Public function isCached( byval arg )
		dim tpl,id,bnd,context
		
		id = ""
		if isArray( arg ) then
			bnd = ubound(arg)
			if bnd > -1 then tpl = arg(0)
			if bnd > 0  then id  = arg(1)
		else
			tpl = arg
		end if
		if tpl = "" then
			tpl = POP_MVC.c & "/" & POP_MVC.a
		end if
		Pub_tpl = tpl
		Pub_id  = id
		isCached = is_page_cached( tpl,id )
	End Function	
	

	
	'清空与模板引擎相关的文件夹， cache_dir
	Public Sub clear()
		if POP_MVC.config("DATA_CACHE_FOLDER") <> "" then
			call POP_MVC.file.remove( POP_MVC.config("DATA_CACHE_FOLDER") )
		else
			call POP_MVC.file.remove( cache_dir )
		end if
	End Sub
	
	'加载模板文件
	Public Sub [Load]( byval tpl,byval id )
		if tpl = "" Then
			tpl = POP_MVC.Url.get_action_name()
		End If
		call data_cache_get_contents( tpl ,id )
	End Sub	
	
	'显示模板文件
	Public Property Get Display ( arg )

		on error resume next
		dim tpl,id,bnd,context,bool,startTime,cache_file
		startTime = timer
		id = ""
		if isArray( arg ) then
			bnd = ubound(arg)
			if bnd > -1 then tpl = arg(0)
			if bnd > 0  then id  = arg(1)
		else
			tpl = arg
		end if
		if tpl = "" then
			tpl = POP_MVC.c & "/" & POP_MVC.a
		end if
		
		if not isEmpty( Pub_tpl ) then
			tpl = Pub_tpl
		end if
		
		if not isEmpty( Pub_id ) then
			id = Pub_id
		end if
		
		cache_file = get_page_cache_file(tpl,id)
		
		if is_cached__ > 0 then
			context = POP_MVC.file_get_contents( cache_file )	
			if not is_empty( POP_MVC.config("SHOW_PAGE_TRACE") ) then
				call POP_MVC.tplPushTime( startTime , "从缓存文件" & cache_file & "读出数据" )
			end if
		else		
				
			context = get_parse_context(tpl)			
			if POP_MVC.config("TPL_COMPRESS") then
				context = POP_MVC.String.htmlCompress( context )
			end if		
		end if
		
		Response.Write context
		if is_empty(POP_MVC.config("SHOW_PAGE_TRACE")) then
			response.flush
		end if
		if not isEmpty( is_cached__ ) then
			if is_cached__ <= 0 then
				if not is_empty( POP_MVC.config("SHOW_PAGE_TRACE") ) then
					if is_cached__ = 0 then
						call POP_MVC.tplPushTime( startTime , "更新缓存文件" & cache_file )
					else
						call POP_MVC.tplPushTime( startTime , "第一次生成缓存文件" & cache_file )
					end if
				end if
				Call POP_MVC.file_put_contents(get_page_cache_file(tpl,id),context)
			end if
		end if

		call L_( classType & "Display" )
	End Property
  
	'获取模板文件的路径
	Function get_template_file( ByVal tpl )	
		Dim template_file
				
		
		if POP_MVC.file.isFile(tpl) then			
			get_template_file = tpl
			exit function
		end if			
	
		if POP_MVC.config("EXTEND_PATH") <> "" then
			if inStr( tpl , POP_MVC.config("EXTEND_SEPARATOR") ) > 0 then
				get_template_file = POP_MVC.rtrim(POP_MVC.config("EXTEND_PATH") , "/" ) & "/" & replace(tpl , POP_MVC.config("EXTEND_SEPARATOR") , "/" ) & tpl_suffix
				exit function
			end if
		end if

	
		if POP_MVC.String.iEndsWith( template_dir , "popasp/Tpl/" ) then '如果用的是系统跳转
			template_file = template_dir & POP_MVC.file.basename(tpl) & tpl_suffix
		else
			tpl = get_tpl_name( tpl )
			
			template_file = template_dir  & tpl	
		end if
	
		template_file = replace( template_file,"/","\" )

		if file_exists(template_file) Then	'如果模板文件不存在
			get_template_file = template_file
		else
			POP_MVC.error( "未找到模板文件" & template_file )
		End If
	End Function
  
    ' 获取tpl的完整名称
	' 为了得到这样的index/header.html，可以采用下列几种方式输入
    ' index/header.html
    ' index/header
	' header
	function get_tpl_name( ByVal tpl )		
		dim c,a
		tpl = Replace(tpl, tpl_suffix, "" )
		if use_ctrl_tpl Then	
			c = get_ctrl_name(tpl)
			a = get_action_name(tpl)
			tpl = c & "/" & a		
		End If		
		tpl = tpl & tpl_suffix
		get_tpl_name = tpl
	End Function
  
	'获得复杂的文件名
	Function get_poptpl_file_name ( tpl , id )
		dim str,length,ret
		
		str = replace( id, " " , "" )
		str = replace( str, VBCRLF , "" )
		if str = "" then
			ret = Replace(Replace(tpl,"/","_"),TPL_SUFFIX,"")
		elseif POP_MVC.String.reg_test( str,"^[^:|/\\*<>?""]{1,128}$","" ) then 'windows下完全限定文件名必须少于260个字符，目录名必须小于248个字符。
			ret = Replace(Replace(tpl,"/","_"),TPL_SUFFIX,"") & "_" & str
		else
			ret = Replace(Replace(tpl,"/","_"),TPL_SUFFIX,"") & "_" & md5(str)
		end if
		get_poptpl_file_name = ret
	End Function

	Function filemtime(filename)
		filemtime = POP_MVC.File.mtime(filename)
	End Function
  
	'如果date1>date2返回正，否则返回负数
	Function dateCompare( date1,date2 )
		dateCompare = DateDiff("s",date2,date1)
	End Function

	'判断文件或目录是否存在
	Function file_exists( fileName )
		file_exists = POP_MVC.File.isExists(fileName)
	End Function		

	' 将数据缓存中的文件内容,与POP_MVC.tpl_vars合并,后分配的会覆盖模板中已经有的
	Private sub data_cache_get_contents( byRef tpl,byRef id)
		if file_exists(  get_data_cache_file(tpl,id) ) Then
			dim temp
			set temp = js_decode(POP_MVC.file_get_contents( get_data_cache_file(tpl,id) ))
			set POP_MVC.tpl_vars = POP_MVC.Dict.Merge( POP_MVC.tpl_vars,temp )
		End If			
	End Sub 'end Sub data_cache_get_contents	
	
	
    '判断是否数据缓存
    ' @param string $tpl
    ' @如果开启数据缓存,且数据缓存有效,则返回 True
	Private function is_data_cached( byval tpl,byval id)
		if cache_lifetime <= 0 then
			is_data_cached = false
			exit function
		end if	
	
		dim data_cache_file			

		data_cache_file = get_data_cache_file(tpl,id)	'得到数据缓存文件路径
		
		'		缓存文件不存在
		if Not file_exists(data_cache_file)  Then
			is_data_cached = false
			is_cached__ = -1	'需要重新生成数据文件
			Exit Function
		'				缓存文件过期
		ElseIf dateCompare(now(),filemtime(data_cache_file)) > cache_lifetime Then	
			is_data_cached = false
			is_cached__ = 0		'需要更新数据文件中的数据
			Exit Function
		End If
		
		is_data_cached = true
		is_cached__ = 1
	End Function
	
	
    '判断是否数据缓存
    ' @param string $tpl
    ' @如果开启页面缓存,且页面缓存有效,则返回 True
	function is_page_cached( byval tpl,byval id)
		if tpl = "" then 
			tpl = POP_MVC.c & "/" & POP_MVC.a
		end if
	
		if cache_lifetime <= 0 then
			is_page_cached = false
			exit function
		end if	
	
		dim page_cache_file			

		page_cache_file = get_page_cache_file(tpl,id)	'得到数据缓存文件路径

		'		缓存文件不存在
		if Not file_exists(page_cache_file)  Then
			is_page_cached = false
			is_cached__ = -1	'需要重新生成缓存文件
			Exit Function
		'				缓存文件过期
		ElseIf dateCompare(now(),filemtime(page_cache_file)) > cache_lifetime Then	
			is_page_cached = false
			is_cached__ = 0		'需要更新数据文件中的数据
			Exit Function
		End If
		
		is_page_cached = true
		is_cached__ = 1
	End Function
	
	
	' 得到数据缓存文件路径
	Private function get_data_cache_file( byVal tpl , byVal id )
		dim dstname,path
		
		dstname = tpl
		
		if id <> "" then
			dstname = dstname & "/" & POP_MVC.trim(POP_MVC.trim(id , "/" ) , "\")
		end if	
		
		if POP_MVC.config("DATA_CACHE_FOLDER") = "" Then		
			path = POP_MVC.appPath & "/Runtime/Cache/" & dstname & ".asp"
		else
			path = POP_MVC.rtrim( POP_MVC.rtrim( POP_MVC.config("DATA_CACHE_FOLDER") , "/" ) , "\" ) & "\" & dstname & ".asp"
		end if

		get_data_cache_file = path
	End Function 'end function get_data_cache_file
	
	
	' 得到数据缓存文件路径
	function get_page_cache_file( byVal tpl , byVal id )
		dim dstname,path
		
		dstname = tpl & id
		
		dstname = POP_MVC.String.reg_replace( dstname , "_" , "\W" , "gi" )
		
		if POP_MVC.config("PAGE_CACHE_FOLDER") = "" Then		
			path = POP_MVC.appPath & "/Runtime/Cache/" & dstname & tpl_suffix
		else
			path = POP_MVC.rtrim( POP_MVC.rtrim( POP_MVC.config("PAGE_CACHE_FOLDER") , "/" ) , "\" ) & "\" & dstname & tpl_suffix
		end if

		get_page_cache_file = path
	End Function 'end function get_data_cache_file
	
	' 获取字典对象中的元素个数
	Private Function count( byref dict ) 
		count = dict.Count
	End Function
	
	
	function get_tpl_context( tpl )
		dim template_file,context
		
		template_file =  get_template_file(tpl)	'得到模板文件路径
		
		context = POP_MVC.file_get_contents(template_file)	'得到模板文件内容

		get_tpl_context = context
	end function
	
	function fetch( tpl )
		fetch = get_parse_context( tpl )
	end function

  
    ' sets PHP tag to the compiled source
    ' @param string $tpl(template file)
	private function get_parse_context ( tpl ) 
		on error resume next
		POP_MVC.dG_( "compileStartTime" ) = timer()
		dim template_file,context,newtext
		dim compiler
		
		template_file =  get_template_file(tpl)	'得到模板文件路径


		' 下面要使用解析器对模板文件进行解析了		
		set compiler = P_("TEMPLATE_COMPILER")
		compiler.init left_delimiter,right_delimiter
		compiler.template = get_tpl_name(tpl)
		compiler.template_file = template_file

		newtext = compiler.compile

		get_parse_context = newtext
		POP_MVC.dG_( "compileEndTime" ) = timer()
		call L_("POPASP_TEMPLATE.get_parse_context")
	End Function 'end function get_parse_context
  
	'从模板名中得到控制器名称
	Private Function get_ctrl_name(tpl_name)
		get_ctrl_name = POP_MVC.get_ctrl_name(tpl_name)
	End Function
  
	'从模板名中得到操作名称
	Private Function get_action_name(tpl_name)
		get_action_name = POP_MVC.get_action_name(tpl_name)
	End Function
	
	Public Sub Cache()
		if cache_lifetime <= 0 then
			exit sub
		end if
		if not isEmpty( is_cached__ ) then
			if is_cached__ <= 0 then
				if typename(POP_MVC.tpl_vars) = "Dictionary" then
					Call POP_MVC.file_put_contents(get_data_cache_file(Pub_tpl,Pub_id),js_encode(POP_MVC.tpl_vars))
				end if
			end if
		end if
	end sub
	
	Private Sub Class_Initialize
		' The name of the diretory where templates are located
		template_dir = "./templates/"

		' The name of the directory for cache files.
		cache_dir		=	"./cache/"

		' The left delimiter used for the template tags.
		left_delimiter  =  "{"

		' The right delimiter used for the template tags.
		right_delimiter =  "}"

		' This is the number of seconds cached content will persist.
		cache_lifetime  =  3600	

		' 模板文件名的后缀
		tpl_suffix = ".html"
		
		' 是否使用带有控制器样式的tpl名称
		use_ctrl_tpl = false
		
		classType = typename(Me)
		
		call init
	End Sub
	
	Private Sub init()
		dim key,temp
		
		cache_lifetime	= 86400
		left_delimiter = "{" 
		right_delimiter = "}" 
		tpl_suffix = ".html"
		
		If Not IsEmpty( POP_MVC.config("TMPL_CACHE_LIFETIME") ) Then cache_lifetime	= POP_MVC.config("TMPL_CACHE_LIFETIME")			
		If Not IsEmpty( POP_MVC.config("TMPL_L_DELIM") ) 		Then left_delimiter = POP_MVC.config("TMPL_L_DELIM")
		If Not IsEmpty( POP_MVC.config("TMPL_R_DELIM") )		Then right_delimiter = POP_MVC.config("TMPL_R_DELIM")
		If Not IsEmpty( POP_MVC.config("TMPL_TEMPLATE_SUFFIX")) Then tpl_suffix = POP_MVC.config("TMPL_TEMPLATE_SUFFIX")
		if POP_MVC.config.exists( "TPL_PATH" ) then
			template_dir	= POP_MVC.config( "TPL_PATH" )
		else
			template_dir	= POP_MVC.appPath & "/Tpl/"	
		end if
		
		cache_dir	 		= POP_MVC.appPath & "/Runtime/Cache/"		
	End sub
End Class
%>