<%
' +----------------------------------------------------------------------
' | POPASP [ ASP MVC ]
' +----------------------------------------------------------------------
' | Copyright (c) 2016 http://popasp.com All rights reserved.
' +----------------------------------------------------------------------
' | Author: popasp <1737025626@qq.com>
' +----------------------------------------------------------------------
Class POPASP_TEMPLATE_COMPILER
	'是否启用模板布局
	Public layout_on
	
	'模板布局文件
	Public layout_file
	
	'模板布局替换标签
	Public layout_label
	
	'不需要模板布局的替换标签
	Public nolayout_label


    ' The left delimiter used for the template tags.
    '	用于Simtpl_Compiler类
    ' @var string
    private left_delimiter

    ' The right delimiter used for the template tags.
    '	用于Simtpl_Compiler类
    ' @var string
    private right_delimiter
	
	Public content
	
	Private loopCounter
	
	Private cache_lifetime
	
	Private classType
	
	Private tplDict,cacheDict,test_bool,is_loopdb_dict,is_loopdb_arr,dictKeys
	private b_if,b_select,b_loop,b_loopdb,b_looparr,b_loopfile,b_loopfield,b_loopxml
	private b_exp,b_three_assign,b_two_assign,b_one_assign,b_nocache,b_escape,b_nocachefile,b_default_output
	Private zuo_delimiter,you_delimiter,escapeDict,escapeDict2,nocacheDict,nocacheDict2
	Public s_template,template_file,nocache_file,file_label
	private b_LJ,b_RJ,b_LF,b_RF,b_WH,b_MH,b_three_output,b_two_output,b_output_exp,b_flow,b_digui
	private ruleVar,recurDict,file_index,attrRule
	private is_test,is_loop,digui_count
	
	Public runtime
	
	' 核心方法，解析模板文件内容
	' 该类仅此方法可对外使用，若非二次开发，其他方法不需使用
	Public Function compile( )		
		on error resume next
		dim dict_label,l_d,r_d,key,item,nloopstr,temp,startTime,start_time,bReload,compile_file,context
		start_time = timer
		dict_label = ""
		set tplDict(dict_label) = CreateDict

		bReload = false
		compile_file = POP_MVC.appPath & "/Runtime/Compile/" & file_label & ".txt"
		
		if not is_empty( POP_MVC.config("APP_DEBUG") ) then
			bReload = true
		else
			if not POP_MVC.file.isExists( compile_file ) then
				bReload = true
			elseif template_file <> "" then			
				if dateCompare(filemtime(template_file),filemtime(compile_file)) > 0 then
					bReload = true
				end if
			end if
		end if
		
		if bReload then
			'如果模板文件不为空，从模板文件中获取内容
			if template_file <> "" then		
				context = POP_MVC.file_get_contents( template_file )
			else
			'否则，从content中获取内容，content应为初始时给的需要解析的字符串
				context = content
			end if			
			context = parseBlock( context )
			context = parseInclude( context )
			context = parseLayout( context )			
			Call POP_MVC.file_put_contents_without_bom( compile_file, context )
		else
			startTime = timer
			context = POP_MVC.file_get_contents( compile_file )
			call POP_MVC.tplPushTime( startTime , "从 Compile/" & file_label & ".txt" & " 中读入解析文件内容" )
		end if
		
		b_escape = inStr( context , "/escape" )
		
		if b_escape then
			context = parseEscape( context,left_delimiter , right_delimiter,1 )
		end if
		
		b_nocache = inStr( context , "/nocache" )
		if b_nocache then
			context = parseNocache( context,left_delimiter , right_delimiter,1 )
		end if		
		
		if not ( b_nocache and b_nocachefile ) then		
			call context_replace( context, dict_label )					
		end if
		
		if b_nocache and not b_nocachefile then
			nloopstr = context
			temp = context
			for each key in nocacheDict
				context = nocacheDict(key)
				temp = replace( temp,key,nocacheDict2(key), 1,1,0 )					
				call context_replace( context, dict_label )				
				nloopstr = replace( nloopstr,key,context, 1,1,0 )								
			next
			context = nloopstr
			startTime = timer
			
			if b_escape then
				for each key in escapeDict
					temp = replace( temp , key , escapeDict(key) ,1,1,0)
				next			
			end if			
			call POP_MVC.file_put_contents_without_bom( nocache_file , temp )
			call POP_MVC.tplPushTime( startTime , "写入缓存文件" & nocache_file )
		end if	
		
		if b_escape then
			for each key in escapeDict
				context = replace( context , key , escapeDict(key) ,1,1,0)
			next			
		end if		
		
		runtime = FormatNumber( (timer() - start_time ) *1000,0,-1,0,0)
		compile = context
		Call L_( classType & "compile" )
	End Function	
	
	Function context_replace( byref context,dict_label)
		on error resume next
		dim l_d,r_d,key
		
		'模板替换
		if dict_label = "" then
			if isObject( POP_MVC.config("TMPL_PARSE_STRING") ) then
				for each key in POP_MVC.config("TMPL_PARSE_STRING")
					context = replace( context , key , POP_MVC.config("TMPL_PARSE_STRING")(key) )
				next
			end if
		end if
		
		'css与js替换
		if inStr( left_delimiter & "js " ) > 0 or inStr( left_delimiter & "css " ) > 0 then
			context = parseCSSJS( context )
		end if		
	
		' 一行注释的替换 {//注释内容 }
		context = POP_MVC.String.reg_replace( context , "", left_delimiter & "//.*?" & right_delimiter , "gim")
		
		' 多注释的替换 {/*注释内容*/}
		context = POP_MVC.String.reg_replace( context , "" , left_delimiter & "/\*[\s\S]*?\*/" & right_delimiter , "gim")	

		b_LJ = inStr( context , "__LJ" ) > 0
		b_RJ = inStr( context , "__RJ" ) > 0
		b_LF = inStr( context , "__LF" )  > 0
		b_RF = inStr( context , "__RF" ) > 0
		b_WH = inStr( context , "__WH" ) > 0
		b_MH = inStr( context , "__MH" ) > 0
		b_flow = inStr( context , "/flow" ) > 0
		b_if = inStr( context , "/if" ) > 0
		b_select = inStr( context , "/select" ) > 0
		b_loop = inStr( context , "/loop" ) > 0
		b_loopdb = inStr( context , "/loopdb" ) > 0
		b_loopxml = inStr( context , "/loopxml" ) > 0
		b_looparr = inStr( context , "/looparr" ) > 0
		b_loopfile = inStr( context , "/loopfile" ) > 0
		b_loopfield = inStr( context , "/loopfield" ) > 0
		b_default_output = inStr( context , "|default=" ) > 0
		b_digui = inStr( context , "{digui|" ) > 0
	
		
		b_exp = inStr( context , "{:" ) > 0 or  inStr( context , "[:" ) > 0 
		b_one_assign = POP_MVC.String.reg_test( context, "({|\[)\s*[\:]\s*\$(" & ruleVar & ")\s*(\=\:?)([^\?]+?)(\]|})" , "i" )	
		b_three_assign = POP_MVC.String.reg_test( context, "({|\[)\s*[\:]\s*\$(" & ruleVar & ")\s*(\=\:?)(.+?)(\?\:*)(.+?)([\:]+)(.*?)(\]|})" , "i" )		
		b_two_assign = POP_MVC.String.reg_test( context, "({|\[)\s*[\:]\s*\$(" & ruleVar & ")\s*(\=\:?)(.+?)(\?\:*)(.+?)\s*(\]|})" , "i" )
		b_three_output = POP_MVC.String.reg_test( context, "({|\[)\s*(\=\:?)(.+?)(\?\:*)(.+?)([\:]+)(.*?)(\]|})" , "i" )
		b_two_output = POP_MVC.String.reg_test( context, "({|\[)\s*(\=\:?)(.+?)(\?\:*)(.+?)(\]|})" , "i" )
		b_output_exp = POP_MVC.String.reg_test( context, "({|\[)[ ]*(\=\:?)([^\?]+?)\s*(\]|})" , "i" )	
		context = compile_(context , left_delimiter , right_delimiter , "$" ,1, null ,  dict_label )
		l_d = "\["
		r_d = "\]"
		if b_one_assign then context = parseOneAssign( context,l_d , r_d , "$"  , null, 1,dict_label )		
		if b_three_assign then context = parseThreeAssign( context,l_d , r_d , "$"  , null, 1,dict_label )
		if b_two_assign then context = parseTwoAssign( context,l_d , r_d , "$"  , null, 1,dict_label )
		if b_exp then context = parseExecExp( context,l_d , r_d , "$" , null, 1,dict_label )
		context = baseReplace_after( context , l_d , r_d ,"$" , null ,1, dict_label )
		context_replace = context
		Call L_( classType & "context_replace" )
	end Function	
	
	'执行函数并输出
	private Function compile_ (byval context , l_d , r_d  , var_prefix, loopCounter , collect , byval dict_label )	
		if b_flow then context = parseFlow( context, l_d , r_d ,collect,loopCounter , dict_label )
		if b_one_assign then context = parseOneAssign( context,l_d , r_d , var_prefix , collect, loopCounter,dict_label )
		if b_three_assign then context = parseThreeAssign( context,l_d , r_d , var_prefix , collect, loopCounter,dict_label )
		if b_two_assign then context = parseTwoAssign( context,l_d , r_d , var_prefix , collect, loopCounter,dict_label )
		if b_exp then context = parseExecExp( context,l_d , r_d , var_prefix , collect, loopCounter,dict_label )		
		if b_if then context = parseIf( context ,"" , l_d , r_d , collect , loopCounter , dict_label )
		if b_select then context = parseSelect( context,l_d , r_d,collect ,loopCounter , dict_label )
		if b_loop and b_loopdb then context = parseDBLoop( context, l_d , r_d ,collect ,loopCounter , dict_label )		
		if b_loop and b_looparr then context = parseArrLoop( context, l_d , r_d ,collect,loopCounter , dict_label )
		if b_loop and b_loopfile then context = parseFileLoop( context, l_d , r_d ,collect,loopCounter , dict_label )
		if b_loop and b_loopxml then context = parseXmlLoop( context, l_d , r_d ,collect ,loopCounter , dict_label )	
		if b_loop and b_loopfield then context = parseFieldLoop( context , l_d , r_d , loopCounter , dict_label )
		context = baseReplace_after( context , l_d , r_d ,var_prefix , collect ,loopCounter, dict_label )
		compile_ = context
	end Function

	'''''''''''''''''''''''''关键函数执行开始'''''''''''''''''''''''''''
	
	Private Function parseBlock( content )		
		dim match,matches,startTime,bool,base_file,base_context,rulePrefix,ruleSuffix,labelRule,cmatch,cmatches,pattern,block_name
		on error resume next
		parseBlock = content
		startTime = timer
		if inStr(content, left_delimiter & "extend" ) < 1 then exit function		
		pattern = left_delimiter & "\s*extend\s+(?:file\s*=\s*)?(""|" & chr(39) & "|)(.*?)\1\s*" & right_delimiter
		set matches = POP_MVC.String.reg_exec( content , pattern, "i" )
		base_file = matches(0).subMatches(1)
		base_file = P_("POPASP_TEMPLATE").get_template_file(base_file)	'得到 base 文件路径
		base_context =  POP_MVC.file_get_contents(base_file)	'得到 base 文件路径
		set matches = nothing
		
		'如果含“{include”,加载包含文件
		if inStr( base_context , left_delimiter & "include" ) then
			base_context = parseInclude( base_context )
		end if
		
		'如果未发现“{block”,则进行警告提示 
		if inStr(base_context, left_delimiter & "block" ) < 1 then
			Call POP_MVC.Warning( "在模板继承文件 " & base_file & " 中未找到block标签 " )
			exit function
		end if

		Call parseRuleFix( rulePrefix,ruleSuffix,left_delimiter,right_delimiter,1)
		labelRule = "((" & rulePrefix & "\s*block)\s*=\s*(\W)(" & ruleVar & ")\W\s*(" & ruleSuffix & "))" & "([\s\S]*?)" & rulePrefix & "\s*\/block(.*?)" & ruleSuffix
		
		set matches = POP_MVC.String.reg_exec( base_context ,  labelRule, "gim" )
		for each match in matches
			block_name = match.subMatches(3)
			pattern = "((" & rulePrefix & "\s*block)\s*=\s*(\W)(" & block_name & ")\W\s*(" & ruleSuffix & "))" & "([\s\S]*?)" & rulePrefix & "\s*\/block(.*?)" & ruleSuffix
			set cmatches = POP_MVC.String.reg_exec( content, pattern, "i" )
			if cmatches.count > 0 then
				base_context = replace( base_context , match.value, cmatches(0).SubMatches(5) )
				content = replace(  content , cmatches(0).value , "" , 1,1,0 )				
			else
				base_context = replace( base_context , match.value, match.SubMatches(5) ,1,1,0 )
				Call POP_MVC.notice( "在模板文件 " & template_file & " 中未找到" & POP_MVC.String.encodeHtml(  match.subMatches(0) ) & "标签 " )
			end if
			call POP_MVC.tplPushTime( startTime , POP_MVC.String.encodeHtml( match.subMatches(0) )  )			
			startTime = timer
		next
		set matches = nothing
		set cmatches = nothing
		parseBlock = base_context
		call L_( classType & ".parseBlock" )
	end Function	
	
	
	Private Function parseLayout( content )		
		dim matches,pattern,layout_on_,layout_file_,layout_context,j,bool,startTime,temp
		on error resume next
		parseLayout = content
	
		'发现了nolayout_label，则不进行解析
		if inStr( content,nolayout_label ) > 0 then
			content = replace( content,nolayout_label,"" )	'替换 nolayout_label
			content = replace( content,layout_label,"" )	'替换 layout_label
			parseLayout = content
			exit function
		end if	
		
		bool = false
		startTime = timer()
		'含有layout_label,或者配置中的 TMPL_LAYOUT_ON 为真时
		if inStr(content, left_delimiter & "layout" ) > 0 then	
			pattern = left_delimiter & "\s*layout\s+(?:file|name)\s*=\s*?(\W?)(.*?)\1\s*" & right_delimiter
			set matches = POP_MVC.String.reg_exec( content , pattern, "i" )
			layout_file_ = matches(0).SubMatches(1)
			layout_file_ = prefixReplace( layout_file_,left_delimiter,right_delimiter, null )
			if not POP_MVC.String.reg_test( layout_file_, "^[a-zA-Z]\w*$" , "" ) then
				Execute( "layout_file_ = " & layout_file_ )
			end if
			content = replace( content,matches(0).value , ""  )
			'set matches = nothing
			bool = true
		elseif layout_on then
			layout_file_ = layout_file
			bool = true
		end if		

		'开始解析
		if bool then
			temp = layout_file_
			layout_file_ = P_("template").get_template_file(layout_file_)	'得到layout文件路径	
			if not isEmpty( layout_file_ ) then		'文件存在
				layout_context = POP_MVC.file_get_contents(layout_file_)	'得到layout文件内容
				if inStr( layout_context , left_delimiter & "include"  ) > 0 then
					layout_context = parseInclude(layout_context)
				end if
				if InStr( layout_context , layout_label ) > 0 then
					content = replace( layout_context,layout_label,content )	'替换{__CONTENT__}
					call POP_MVC.tplPushTime( startTime , POP_MVC.String.encodeHtml( layout_label )  )
				else
					content = "<!-- 未找到layout文件:" & temp & " -->" & content
					Call POP_MVC.Warning( "在模板布局文件 " & temp & " 中未找到替换标签 " & layout_label )
				end if
			else
				content = "<!-- 未找到layout文件:" & temp & " -->" & content
				Call POP_MVC.Warning( "布局文件 """ & temp & """ 不存在")	
			end if
			set matches = nothing
		end if

		parseLayout = content
		call L_( classType & ".parseLayout" )
	end Function	
	
	'解析include
	Private Function parseInclude( byval content )
		dim match,matches,startTime,bool,include_file,include_context,temp
		on error resume next
		parseInclude = content
		pattern = left_delimiter & "\s*include\s+(?:file|name)\s*=\s*?(\W?)(.*?)\1\s*" & right_delimiter
		do while ( inStr(content, left_delimiter & "include" ) > 0 and j<100 )
			set matches = POP_MVC.String.reg_exec( content , pattern, "gi" )
			for each match in matches
				startTime = timer()
				include_file = match.SubMatches(1)
				include_file = prefixReplace( include_file,left_delimiter,right_delimiter, null )
				if not POP_MVC.String.reg_test( include_file, "^[a-zA-Z]\w*$" , "" ) then
					Execute( "include_file = " & include_file )
				end if
				temp = include_file				
				include_file = P_("POPASP_TEMPLATE").get_template_file(include_file)	'得到 include 文件路径
				if not isEmpty( include_file ) then
					include_context = POP_MVC.file_get_contents(include_file)	'得到 include 文件内容					
				else
					content = replace( content,match,"<!-- 未找到include文件:" & temp & " -->" )	'替换
					Call POP_MVC.Warning( "include 文件 " & temp & " 不存在")
				end if
				content = replace( content,match,include_context )	'替换				
				call POP_MVC.tplPushTime( startTime , POP_MVC.String.encodeHtml( match.value )  )
			next
			j = j+1			
		loop
		set matches = nothing
		parseInclude = content
		call L_( classType & ".parseInclude" )
	end Function	
	
	'解析escape
	Private Function parseEscape( byval content , l_d,r_d , loopCounter )	
		dim match,matches,labelRule,rulePrefix,ruleSuffix,escape_label,i
		on error resume next		
		Call parseRuleFix( rulePrefix,ruleSuffix,l_d,r_d,loopCounter)		
		labelRule = "((" & rulePrefix & "\s*escape)\s*(.*?)\s*(" & ruleSuffix & "))" & "([\s\S]*?)" & rulePrefix & "\s*\/escape(.*?)" & ruleSuffix
		set matches = POP_MVC.String.reg_exec( content,labelRule , "gim" )
		set escapeDict = CreateDict
		i = 0
		for each match in matches
			bool = false
			if match.subMatches(2) <> "" then
				if POP_MVC.String.startsWith( match.subMatches(2) , "=:" ) then
					bool = mid(  match.subMatches(2) , 3 )
				else
					bool = mid(  match.subMatches(2) , 2 )
					bool = prefixReplace( bool,left_delimiter,right_delimiter, null )
				end if
				Execute( "bool = " & bool )
				bool = CBool(bool)
			end if
			escape_label = "#QQ_1737025626_POPASP_ESCAPE_QQ_GROUP_124648143_" & i & "#"
			content = replace( content , match , escape_label,1,1,0 )
			if bool then
				escapeDict( escape_label ) = match.value				
			else
				escapeDict( escape_label ) = match.subMatches(4)
			end if			
			i = i + 1
		next
		set match = nothing : set matches = nothing
		parseEscape = content
		Call L_( classType & ".parseEscape" )
	End Function	
	
	'解析nocache
	Private Function parseNocache( byval content , l_d,r_d , loopCounter )	
		dim match,matches,labelRule,lifeTime,i,startTime
		dim rulePrefix,ruleSuffix
		dim cache_label,nocache_label
		on error resume next		
		cache_label = "nocache"
		nocache_file = POP_MVC.appPath & "/Runtime/Compile/" & cache_label & "#" & file_label & ".txt"
		if POP_MVC.file.isFile( nocache_file ) then			
			lifeTime = dateCompare(now(),filemtime(nocache_file))
			If dateCompare(filemtime(template_file) ,filemtime(nocache_file)) < 0 and lifeTime > 0 and lifeTime < cache_lifetime Then	
				startTime = timer()
				b_nocachefile = true
				parseNocache = POP_MVC.file_get_contents( nocache_file )				
				b_escape = inStr( parseNocache , "/escape" )				
				if b_escape then
					parseNocache = parseEscape( parseNocache,l_d , r_d,1 )
				end if				
				parseNocache = POP_MVC.String.reg_replace( parseNocache,  "" , zuo_delimiter & "\/?#QQ_1737025626_POPASP_NOCACHE_QQ_GROUP_124648143_\d+#" & you_delimiter, "gim")
				call POP_MVC.tplPushTime( startTime , "从缓存文件" & nocache_file & "中取得缓存内容"  )
				exit function
			end if
		end if		
		Call parseRuleFix( rulePrefix,ruleSuffix,l_d,r_d,loopCounter)		
		labelRule = "(" & rulePrefix & "\s*nocache\s*" & ruleSuffix & ")" & "([\s\S]*?)" & rulePrefix & "\s*\/nocache\s*" & ruleSuffix
		set matches = POP_MVC.String.reg_exec( content,labelRule , "gim" )
		set nocacheDict = CreateDict
		set nocacheDict2 = CreateDict
		i = 0		
		for each match in matches
			nocache_label = "#QQ_1737025626_POPASP_NOCACHE_QQ_GROUP_124648143_" & i & "#"
			content = replace( content , match , nocache_label,1,1,0 )
			nocacheDict( nocache_label ) = match.subMatches(1)
			nocacheDict2( nocache_label ) = zuo_delimiter & nocache_label & you_delimiter & match.subMatches(1) & zuo_delimiter & "/" & nocache_label & you_delimiter
			i = i + 1
		next
		set match = nothing : set matches = nothing
		parseNocache = content
		Call L_( classType & ".parseNocache" )
	End Function	
	
	'解析赋值表达式
	Private Function parseOneAssign( byval context , byval l_d, byval r_d , var_prefix , t_  , loopCounter ,byval dict_label )
		on error resume next
		dim str,i,matches,match,condition,str_true,str_false,pattern,repStr,startTime,execStr,varName
		parseOneAssign = context
		Call loopcntReplace( l_d,r_d,loopCounter,dict_label )

		
		'变量赋值，属于表达式运算的一种特例
		': $var_name = value
		'one_assign
		if b_one_assign  then
			if l_d = left_delimiter then
				pattern = l_d & "\s*[\:]\s*\$(" & ruleVar & ")\s*(\=\:?)([^\?]+?)" & r_d
			else	
				pattern = "(?:" & l_d & ")"  & "{1}" &  "(?!\[+)"  &  "\s*[\:]\s*\$(" & ruleVar & ")\s*(\=\:?)([^\?\]]+?)\s*" & "(?:" & r_d & ")" & "{1}"  &  "(?!\])"
			end if		
			set matches = POP_MVC.String.reg_exec(context, pattern  ,"gim")
			for each match in matches	

			
				startTime = timer()
				str = match.subMatches(2)
				
				if match.subMatches(1) = "=" then
					str =  prefixReplace( str ,l_d,r_d  , t_ )
				elseif match.subMatches(1) = "=:" then					
					'不做任何修改
				end if				
				varName = match.subMatches(0)				
				Call varNameReplace( execStr , str, varName )	

				
				Execute(  execStr )	
				if err.number <> 0 then
					Call POP_MVC.Warning( "使用parseOneAssign解析" & POP_MVC.String.encodeHtml( match.value ) & "为" &  POP_MVC.String.encodeHtml( execStr ) & "时，不能被正确Execute:" & err.description   )
				end if
			
				context = Replace(context,match.value, "" )
				
				call POP_MVC.tplPushTime( startTime , POP_MVC.String.encodeHtml( match.value )  )
			next
			set matches = nothing
		end if

		parseOneAssign = context	
		Call L_( classType & ".parseOneAssign" )
	end function	
	
	'三元赋值
	Private Function parseThreeAssign( byval context , byval l_d, byval r_d , var_prefix , t_  , loopCounter ,byval dict_label )
		err.clear
		on error resume next
		dim str,i,matches,match,condition,str_true,str_false,pattern,repStr,startTime,execStr,varName
		
		parseThreeAssign = context
		Call loopcntReplace( l_d,r_d,loopCounter,dict_label )
		
		'三目赋值
		': $var = ( V_("toggle") = 1 ) ? "开启" : "关闭"	
		'three_exp
		

		if b_three_assign  then
			if l_d = left_delimiter then
				pattern = l_d & "\s*[\:]\s*\$(" & ruleVar & ")\s*(\=\:?)(.+?)(\?\:*)(.+?)([\:]+)(.*?)\s*" & r_d
			else		
				pattern = "(?:" & l_d & ")"  & "{1}" &  "(?!\[+)"  &  "\s*[\:]\s*\$(" & ruleVar & ")\s*(\=\:?)([^\[\]]+?)(\?\:*)([^\[\]]+?)([\:]+)([^\[\]]*?)\s*" & "(?:" & r_d & ")" & "{1}"  &  "(?!\])"
			end if		
			set matches = POP_MVC.String.reg_exec(context, pattern ,"gim")

			for each match in matches
				startTime = timer()
				str = match.subMatches(2)
				
				if match.subMatches(1) = "=" then	'如果是=:则原样输出
					str =  prefixReplace( str,l_d,r_d  , t_ )
				elseif match.subMatches(1) = "=:" then
					'不做任何修改
				end if	
				if str = "" then
					condition = false
				else
					Execute("condition = " & str )		
				end if
				
				if condition then
					str = match.subMatches(4)	
					if match.subMatches(3) = "?" then
						str =  prefixReplace( str,l_d,r_d  , t_ )
					else
						str =  delimiterReplace( str,l_d,r_d )
					end if
				else
					str = match.subMatches(6)					
					if match.subMatches(5) = ":" then
						str =  prefixReplace( str,l_d,r_d  , t_ )
					else
						str =  delimiterReplace( str,l_d,r_d )
					end if
				end if	
				varName = match.subMatches(0)				
				Call varNameReplace( execStr , str, varName )				
				Execute(  execStr )
				context = Replace(context,match.value,"" )
				call POP_MVC.tplPushTime( startTime , POP_MVC.String.encodeHtml( match.value )  )
			next
			set matches = nothing
		end if
		parseThreeAssign = context	
		Call L_( classType & ".parseThreeAssign" )
	end function	
	
	'二元赋值
	Private Function parseTwoAssign( byval context , byval l_d, byval r_d , var_prefix , t_  , loopCounter ,byval dict_label )
		err.clear
		on error resume next
		dim str,i,matches,match,condition,str_true,str_false,pattern,repStr,startTime,execStr,varName
		
		parseTwoAssign = context
		Call loopcntReplace( l_d,r_d,loopCounter,dict_label )
		
		'双目赋值
		': $var = ( V_("toggle") = 1 ) ? "开启"
		'two_assign
			if l_d = left_delimiter then
				pattern = l_d & "\s*[\:]\s*\$(" & ruleVar & ")\s*(\=\:?)(.+?)(\?\:*)(.+?)\s*" & r_d
			else		
				pattern = "(?:" & l_d & ")"  & "{1}" &  "(?!\[+)"  &  "\s*[\:]\s*\$(" & ruleVar &  ")\s*(\=\:?)([^\[\]]+?)(\?\:*)([^\[\]]+?)\s*" & "(?:" & r_d & ")" & "{1}"  &  "(?!\])"
			end if		
			set matches = POP_MVC.String.reg_exec(context, pattern ,"gi")
			
			for each match in matches			
				startTime = timer()
				str = match.subMatches(2)
				if match.subMatches(1) = "=" then	'如果是两个::则原样输出
					str =  prefixReplace( str,l_d,r_d  , t_ )
				elseif match.subMatches(1) = "=:" then
					'不做任何修改
				end if
				if str = "" then
					condition = false
				else
					Execute("condition = " & str )	
				end if
				if condition then
					str = match.subMatches(4)	
					if match.subMatches(3) = "?" then
						str =  prefixReplace( str,l_d,r_d  , t_ )
					else
						str =  delimiterReplace( str,l_d,r_d )
					end if
					varName = match.subMatches(0)				
					Call varNameReplace( execStr , str, varName )	
					Execute(  execStr )		
				end if				
				context = Replace(context,match.value,"" )
				call POP_MVC.tplPushTime( startTime , POP_MVC.String.encodeHtml( match.value )  )
			next
			set matches = nothing


		parseTwoAssign = context	
		Call L_( classType & ".parseTwoAssign" )
	end function

	Function parseExecExp( byval context , byval l_d, byval r_d , var_prefix , t_  , loopCounter ,byval dict_label )
		err.clear
		on error resume next
		dim str,matches,match,condition,str_true,str_false,pattern,repStr,startTime,pattern1,pos,str1
		
		parseExecExp = context
		Call loopcntReplace( l_d,r_d,loopCounter,dict_label )
		
		if (not isEmpty(tplDict( dict_label )( "exec_exp" )) and  tplDict( dict_label )( "exec_exp" ) = false) then
			exit function	
		end if
		
		if l_d <> left_delimiter then
			if inStr( context , "{:" ) < 1 and  inStr( context , "[:" ) < 1	 then
				tplDict( dict_label )( "exec_exp" ) = false
				exit Function
			else
				tplDict( dict_label )( "exec_exp" ) = true
			end if
		end if			
				
		'表达式运算
		': POP_MVC.String.range( 5,100 )
		'exec_exp
		if b_exp or ( l_d <> left_delimiter and  tplDict( dict_label )( "exec_exp" )) then	

			if l_d = left_delimiter then
				pattern = l_d & "\s*([\:]+)([\s\S]+?)" & r_d
				pattern1 = l_d & "\s*[\:]\$(" & ruleVar & ")\|remove\s*" & r_d
			else	
				pattern = "(?:" & l_d & ")"  & "{1}" &  "(?!\[+)" & "\s*([\:]+)([^\[\]]+?)\s*" & "(?:" & r_d & ")" & "{1}"  &  "(?!\])"
				pattern1 = "(?:" & l_d & ")"  & "{1}" &  "(?!\[+)" & "\s*[\:]\$(" & ruleVar & ")\|remove\s*" & "(?:" & r_d & ")" & "{1}"  &  "(?!\])"
			end if
			if POP_MVC.String.reg_test( context, pattern1 , "" ) then
				set matches = POP_MVC.String.reg_exec(context, pattern1  ,"gmi")
				if matches.count = 0 and not is_loop then
					tplDict(dict_label)(l_d & var_prefix & "exec_exp") = false
				end if	
				for each match in matches
					startTime = timer()
					pos = inStrRev(  match.subMatches(0), "." )
					if pos then
						str = mid(match.subMatches(0),pos+1)
						str1 = mid(match.subMatches(0),1,pos-1)
						str1= replace( str1, ".",""")("""  )
						str1= "(""" & str1 & """)"
						str = "POP_MVC.tpl_vars" & str1 & ".remove(""" & str & """)"
					else
						str = match.subMatches(0)
						str = "POP_MVC.tpl_vars.remove(""" & str & """)"
					end if
					Execute(  str )
					context = Replace(context,match.value, "" )
					call POP_MVC.tplPushTime( startTime , POP_MVC.String.encodeHtml( match.value )  )
				next
			else
				set matches = POP_MVC.String.reg_exec(context, pattern  ,"gm")
				if matches.count = 0 and not is_loop then
					tplDict(dict_label)(l_d & var_prefix & "exec_exp") = false
				end if	
				for each match in matches
					startTime = timer()
					str = match.subMatches(1)	
					if match.subMatches(0) = ":" then
						str =  prefixReplace( str,l_d,r_d  , t_ )
					elseif match.subMatches(0) = "::" then
						'不做任何修改
					end if
					Execute(  str )
					if err.number <> 0 then
						POP_MVC.error( classType & ".parseExecExp:" & "解析 " & match.value & "为" & POP_MVC.String.encodeHtml( str ) & "时不能被Execute,错误：" & err.description )
					end if
					context = Replace(context,match.value, "" )
					call POP_MVC.tplPushTime( startTime , POP_MVC.String.encodeHtml( match.value )  )
				next
			end if
			set matches = nothing
		end if
		parseExecExp = context	
		Call L_( classType & ".parseExecExp" )
	end function	
	
	'解析if
	Private Function parseIf(byval content,byval x, byval l_d, byval r_d,byref t_, byval loopCounter , byval dict_label )
		on error resume next
		parseIf = content

		dim matchIf,matchesIf,strIf,strThen,strThen1,strElse1,labelRule2,labelRule3,startTime
		dim ifFlag,elseIfArray,elseIfSubArray,elseIfArrayLen,resultStr,elseIfLen,strElseIf,strElseIfThen,elseIfFlag
		dim matches,match,repStr,bool
		dim cacheBool,cache_label,fileBool,is_cached,cachePath,outputBool,needParse
		
		dim rulePrefix,ruleSuffix,loop_cnt
		Call parseRuleFix( rulePrefix,ruleSuffix,l_d,r_d,loopCounter)
	
		labelRule= "(" & rulePrefix & "\s*if\s*"&x&"(\:+)([\s\S]+?)" & ruleSuffix & ")" & "([\s\S]*?)" & rulePrefix & "\s*\/if"&x&"(.*?)" & ruleSuffix 		
		labelRule2= rulePrefix & "\s*elseif"&x&"\s*(\:+)([\s\S]+?)" &  ruleSuffix & "([\s\S]*?)(?=" & rulePrefix & "\s*(elseif|else|\/if).*?" & ruleSuffix & ")"
		labelRule3= rulePrefix & "\s*else"&x&"\s*" & ruleSuffix & "([\s\S]*)"
		elseIfFlag=false
		set matchesIf=POP_MVC.String.reg_exec(content,labelRule,"gim")

		for each matchIf in matchesIf		
			startTime = timer()
			dict_label = CStr(  matchif.subMatches(0) )
			call parseCacheArgs( matchif.SubMatches(4),cacheBool,cache_label,fileBool,is_cached,cachePath,outputBool,needParse,matchif,content,dict_label,startTime)			
			
			'如果未缓存
			if needParse then	
				repStr = ""
				strIf=matchIf.SubMatches(2):strThen=matchIf.SubMatches(3)

				if matchIf.SubMatches(1) = ":" then
					strIf = prefixReplace( strIf,l_d,r_d  , t_ )
				elseif matchIf.SubMatches(1) = "::" then
					'不做任何处理
				end if	
				
				

				Execute("if ("&strIf&") then ifFlag=true else ifFlag=false")	

				if ifFlag then
					repStr = strThen
					if inStr( strThen , "<!--" & POP_MVC.String.repeat( replace(l_d , "\" , "")  , loopCounter ) & "else" ) > 0 or inStr( strThen , "<!--" & POP_MVC.String.repeat( replace(l_d , "\" , "")  , loopCounter ) & "/if" ) > 0  then						
						set matches = POP_MVC.String.reg_exec( strThen, "([\s\S]*?)" & rulePrefix & "\s*(?:else|\/if).*?" & ruleSuffix , "im" )
						if matches.count > 0 then
							repStr = matches(0).subMatches(0)
						end if
						set matches = nothing		
					end if	
				else		
					'匹配esleif并进行替换
					bool = false
					if inStr( strThen , "<!--" & POP_MVC.String.repeat( replace(l_d , "\" , "")  , loopCounter ) & "elseif"  ) > 0  then
						set matches = POP_MVC.String.reg_exec( matchIf.value, labelRule2 , "gim" )
						for each match in matches 
							strIf=match.SubMatches(1)
							if match.SubMatches(0) = ":" then
								strIf = prefixReplace( strIf, l_d,r_d  ,t_ )
							elseif match.SubMatches(0) = "::" then
								'不做任何修改
							end if
							Execute("if "&strIf&" then ifFlag=true else ifFlag=false")				

							if ifFlag then
								repStr = match.SubMatches(2)
								
								'content=replace(content,matchIf.value,match.SubMatches(1) )
								bool = true
								exit for
							end if
						next
						set matches = nothing
					end if					
					
					
					'匹配esleif失败，则进行else匹配并替换
					if not bool then
						if inStr( strThen, "{else" ) > 0 or inStr( strThen, "[else" ) > 0 then
							set matches = POP_MVC.String.reg_exec( strThen, labelRule3 , "im" )					
							if matches.count > 0 then
								repStr = matches(0).subMatches(0)
								'content=replace(content,matchIf.value,matches(0).subMatches(0) )
								bool = true
							end if
						end if
					end if
					
					if not bool then
						repStr = ""
					end if	
				end if
				
				if repStr <> "" then
					if isNull( t_ ) then
						repStr = compile__( repStr , l_d , loopCounter , t_ , false , dict_label )
					else
						repStr = compile__( repStr , l_d , loopCounter , t_ , true , dict_label )
					end if
				end if
				Call parseCacheOutput(cacheBool,cache_label,repStr,fileBool,is_cached,cachePath,outputBool,matchIf,content)
				call POP_MVC.tplPushTime( startTime , POP_MVC.String.encodeHtml( dict_label )  )
			end if
		next
		parseIf = content
		Call L_( classType & ".parseIf" )
	End Function
	
	'select替换
	Private Function parseSelect( byval content , l_d,r_d , byref t_ , loopCounter , byval dict_label )	
		dim match,matches,caseMatch,caseMatches,labelRule,startTime,str
		dim condition,caseRule,innerStr,bool,repStr,rulePrefix,ruleSuffix,caseElseRule
		dim cacheBool,cache_label,fileBool,is_cached,cachePath,outputBool,needParse
		on error resume next
		parseSelect = content

		Call parseRuleFix( rulePrefix,ruleSuffix,l_d,r_d,loopCounter)
		
		labelRule = "(" & rulePrefix & "\s*select\s*(\:+)([\s\S]+?)" & ruleSuffix & ")" & "([\s\S]*?)" & rulePrefix & "\/select(.*?)" & ruleSuffix
		caseRule =  rulePrefix & "\s*case\s*(\:+)([\s\S]*?)" & ruleSuffix & "(.*)" 
		caseElseRule =  rulePrefix & "\s*caseelse\s*" & ruleSuffix & "(.*)" 
		'caseRule =  rulePrefix & "\s*case\s*(\:+)([\s\S]*?)" & ruleSuffix & "([\s\S]*?)" & rulePrefix & "\s*/case\s*" & ruleSuffix
		'caseElseRule =  rulePrefix & "\s*caseelse\s*" & ruleSuffix & "([\s\S]*?)" & rulePrefix & "\s*/caseelse\s*" & ruleSuffix
		set matches = POP_MVC.String.reg_exec( content,labelRule , "gim" )

		for each match in matches
			startTime = timer()
			dict_label = CStr(match.subMatches(0))
			call parseCacheArgs( match.SubMatches(4),cacheBool,cache_label,fileBool,is_cached,cachePath,outputBool,needParse,match,content,dict_label,startTime)
			
			'如果缓存了，
			if needParse then
				str = match.subMatches(2)
				if match.subMatches(1) = ":" then
					str = prefixReplace( str,l_d,r_d  , t_ )
				elseif  match.subMatches(1) = "::" then
					'不做任何修改
				end if
				'得到条件
				execute("condition = " & str )	
				
				innerStr = match.subMatches(3)
				bool = false
				set caseMatches = POP_MVC.String.reg_exec( innerStr,caseRule , "gim" )

				for each caseMatch in caseMatches
					str = caseMatch.subMatches(1)
					if caseMatch.subMatches(1) = ":" then
						str = prefixReplace( str,l_d,r_d  , t_ )
					elseif  caseMatch.subMatches(1) = "::" then
						'不做任何修改
					end if					
					execute( "bool = ( condition = " & str & " )" )
					if bool then
						execute( "repStr = caseMatch.subMatches(2)" )
						exit for
					end if
				next

				'[=:值] 最后一个相当于 case else
				if not bool then
					set caseMatches = POP_MVC.String.reg_exec( innerStr,caseElseRule , "gim" )
					if caseMatches.count > 0 then
						repStr = caseMatches(0).subMatches(0)
						bool = true
					else
						repStr = ""
					end if
				end if
				Call parseCacheOutput(cacheBool,cache_label,repStr,fileBool,is_cached,cachePath,outputBool,match,content)
				call POP_MVC.tplPushTime( startTime , POP_MVC.String.encodeHtml( dict_label )  )
			end if
		next
		set match = nothing : set matches = nothing

		parseSelect = content
		Call L_( classType & ".parseSelect" )
	End Function	

	'输出用select、getAll、search取出的二维数据块
	'<!--{loopdb="表名:方法名:标识符" 连贯操作或参数}-->...<!--{/loopdb="缓存名:是否立即输出:文件缓存时长"}-->
	'<ul>
	'<!--{loopdb="links:select" where="1=1"}-->   ':select可以省略，默认取select
	'<li>[@LinkUrl]</li>
	'<!--{/loopdb}-->
	'</ul>
	'如果有参数，使用arg_1="...",arg_2="..."来表示
	Private Function parseDBLoop( byval content ,byval l_d,byval r_d,byref t_,byval loopCounter , byval dict_label  )
		dim match,matches,amatch,amatches,attrRule,labelRule,separator,attrstr,opt_key,opt_value,opt_equal,loopstr,nloopstr
		dim tableName,method,rs,model,arr,arg_str,arg_arr,i,bnd,key,item,temp,tpl_label,rulePrefix,ruleSuffix,bool,repStr,startTime
		dim cacheBool,cache_label,fileBool,is_cached,cachePath,outputBool,needParse,is_assign_page
		on error resume next
		parseDBLoop = content
		
		Call parseRuleFix( rulePrefix,ruleSuffix,l_d,r_d,loopCounter)
		
		labelRule = "(" & rulePrefix & "\s*loopdb\s*\=\s*(\W+)(.+?)\2([\s\S]*?)" & ruleSuffix & ")" & "([\s\S]*?)" & rulePrefix & "\s*\/loopdb(.*?)" & ruleSuffix

		If NOt POP_MVC.String.reg_test( content , labelRule , "i" ) then
			 Exit Function
		end if	
		
		set matches = POP_MVC.String.reg_exec( content,labelRule , "gim" )
		is_loop = true
		for each match in matches			
			startTime = timer()
			dict_label = CStr(match.subMatches(0))
			call parseCacheArgs( match.SubMatches(5),cacheBool,cache_label,fileBool,is_cached,cachePath,outputBool,needParse,match,content,dict_label,startTime)
			'如果缓存了，
			if needParse then
				tpl_label = ""
				loopstr = match.SubMatches(4)
				'用:炸开loop"表名:方法名:标识符"
				arr = split( match.SubMatches(2) , ":" )				
				'分配标识符
				if ubound( arr ) > 1 then				
					tpl_label = arr(2)
				end if	

				if tpl_label="" or not POP_MVC.tpl_vars.exists( tpl_label ) then			
					tableName = arr(0)
					
					tableName = trim( tableName )
					
					'如果表名为空，跳出循环
					if tableName <> "" then
					
						if tpl_label = "" then
							tpl_label = tableName
						end if
						
					
						'分配方法名
						if ubound( arr ) > 0 then				
							method = arr(1)
						end if

						if method= "" then
							'默认使用select方法
							method = "select"					
						end if
						
						'得到连贯操作与参数字符串
						attrstr = match.SubMatches(3)
						separator = match.SubMatches(1)
						
						attrRule = "(\w+)\s*(=:?)\s*" & "\" & separator & "(.+?)" & "\" & separator
						
						set model = M_(tableName).db
						
						arg_str = ""
						
						bool = true
						
						'替换字段
						If POP_MVC.String.reg_test( attrstr , attrRule , "gim" ) then
							arg_arr = array()
							set amatches = POP_MVC.String.reg_exec( attrstr,attrRule , "gim" )
							for each amatch in amatches
								opt_key = amatch.SubMatches(0)		'连贯操作如where、field、group等
								opt_equal = amatch.SubMatches(1)		'=或=:
								opt_value = amatch.SubMatches(2)	'连贯操作where等的值，可含{$var}等
								
											
								if opt_equal = "=" then
									opt_value = prefixReplace( opt_value,l_d,r_d  , t_ )
									if not POP_MVC.String.reg_test( opt_value, "^[a-zA-Z]\w*$" , "" ) then
										Execute( "if isObject(" & opt_value & ") then set opt_value = " & opt_value & " else opt_value = " & opt_value )
									end if
								elseif match.subMatches(1) = "=:" then
									'不做任何修改
								end if
								'如果要传参，传参的前缀是arg_，按次序分配值。如getRS(sql)，此时可以 arg_1="sql"
								if not POP_MVC.String.iStartsWith( opt_key , "arg_" ) then
									if LCase(opt_key) = "page" then
										is_assign_page = true
									end if
									execute( "model." & opt_key & "(opt_value)" )
								else
									POP_MVC.arr.push arg_arr,opt_value
								end if
							
							next
							bnd = ubound( arg_arr )
							
							if bnd > -1 then
								for i = 0 to bnd
									if i <> bnd then
										arg_str = arg_str & "arg_arr(" & i &"),"
									else
										arg_str = arg_str & "arg_arr(" & i &")"
									end if
								next
							end if
						end if
						execute( "set rs = model." & method & "( " & arg_str & " )" ) 
						'给POP_MVC.tpl_vars分配rs，可以使用V_(tpl_label)调用
						set POP_MVC.tpl_vars( tpl_label ) = rs
					end if
				else
					set rs = POP_MVC.tpl_vars( tpl_label )
					if POP_MVC.String.reg_test( match.SubMatches(3) , "page\s*=\s*(\W)0\1","i" ) then
						is_assign_page = false
					else
						is_assign_page = true
					end if	
				end if

				nloopstr = ""
				i = 0 
				if typename( rs ) = "Recordset" then	'如果是Recordset，使用loop循环，只输出当前页数据
					if rs.state then
						do while not rs.eof and not rs.bof
							if is_assign_page then
								if i = rs.pageSize then exit do
							else
								if i = 500 then exit do
							end if
							POP_MVC.tpl_vars( "__index" ) = i
							if not isEmpty( tpl_label ) then
								POP_MVC.tpl_vars( tpl_label & "__" & "index" ) = i
							end if		
					
							temp = compile__( loopStr , l_d , loopCounter , rs , true , dict_label )
							nloopstr = nloopstr & temp 				
							i = i + 1
							rs.movenext
						loop
					end if
				elseif typename( rs ) = "Dictionary" then	'如果是Dictionary，使用for each循环，输出所有数据
					is_loopdb_dict = true
					for each key in rs
						if not is_assign_page and i = 500 then exit for
						POP_MVC.tpl_vars( "__index" ) = i
						if not isEmpty( tpl_label ) then
							POP_MVC.tpl_vars( tpl_label & "__" & "index" ) = i
							set POP_MVC.tpl_vars( tpl_label & "__" & "value" ) = rs(key)
						end if	
						set item = rs(key)
						if isEmpty( dictKeys ) then
							dictKeys = item.keys
						end if
						temp = compile__( loopStr , l_d , loopCounter , item ,true , dict_label )					
						nloopstr = nloopstr & temp					
						i = i + 1
					next
					dictKeys = Empty
					is_loopdb_dict = false
				end if		
				
				Call parseCacheOutput(cacheBool,cache_label,nloopstr,fileBool,is_cached,cachePath,outputBool,match,content)
				
				call POP_MVC.tplPushTime( startTime , POP_MVC.String.encodeHtml( dict_label )  )
			end if
			is_assign_page = false
		next
		is_loop = false
		set match = nothing : set matches = nothing
		parseDBLoop = content
		Call L_( classType & ".parseDBLoop" )
	End Function
	
	'输出数组
	'<!--{looparr="arr:theStart:theEnd:theStep"}-->...<!--{/looparr="缓存名:是否立即输出:文件缓存时长"}-->
	'$__index,$__key,$__value
	'arr_loop
	Function parseArrLoop( byval content ,byval l_d,byval r_d,byref t_,byval loopCounter ,byval dict_label )	
		dim match,matches,attrRule,labelRule,separator,attrstr,startTime,loopstr,nloopstr,tpl_label
		dim tableName,method,arr,arg_str,arg_arr,i,bnd,key,item,temp,arr_label,rulePrefix,ruleSuffix
		dim cacheBool,cache_label,fileBool,is_cached,cachePath,outputBool,needParse
		dim start_,end_,step_,theArr,j
		on error resume next
		parseArrLoop = content
		
		Call parseRuleFix( rulePrefix,ruleSuffix,l_d,r_d,loopCounter)
		
		labelRule = "(" & rulePrefix & "\s*looparr\s*\=\s*(\W+)(.+?)\2([\s\S]*?)" & ruleSuffix & ")" & "([\s\S]*?)" & rulePrefix & "\s*\/looparr(.*?)" & ruleSuffix

		If NOt POP_MVC.String.reg_test( content , labelRule , "i" ) then
			 Exit Function
		end if	

		set matches = POP_MVC.String.reg_exec( content,labelRule , "gim" )
		
		is_loop = true
		for each match in matches	
			startTime = timer()				
			dict_label = CStr(match.subMatches(0))
			call parseCacheArgs( match.SubMatches(5),cacheBool,cache_label,fileBool,is_cached,cachePath,outputBool,needParse,match,content,dict_label,startTime)
		
			'如果需要解析
			if needParse then		
				'用:炸开loop"数组名:i=start:end值:step值"
				arr = split( match.SubMatches(2) , ":" )
				arr_label = arr(0)	
				start_ = ""
				end_ = ""
				if arr_label <> "" then
					theArr = prefixReplace( arr_label ,l_d,r_d  , t_ )
					if not POP_MVC.String.reg_test( theArr, "^[a-zA-Z]\w*$" , "" ) then
						Execute( "theArr = " & theArr )
					end if
				else
					theArr = Empty
				end if
				step_ = 1				
				if ubound( arr ) > 0 then start_ = prefixReplace(arr(1),l_d,r_d,t_) : Execute( "start_ = " & start_ )
				if ubound( arr ) > 1 then end_ = prefixReplace(arr(2),l_d,r_d,t_) : Execute( "end_ = " & end_ )
				if ubound( arr ) > 2 then step_ =  prefixReplace(arr(3),l_d,r_d,t_) : Execute( "step_ = " & step_ )
				if POP_MVC.String.reg_test( arr_label , "^(?:\$|\@)(\w+)$" , "" ) then
					tpl_label = mid(arr_label,2)
				end if
				nloopstr = ""				
				i = 0
				
				if arr_label <> "" and end_ = "" then
					if end_ = "" then end_ = ubound( theArr )
					if start_ = "" then start_ = lbound( theArr )
				end if

				for j = start_ to end_ step step_					
					POP_MVC.tpl_vars( "__index" ) = i					
					loopstr = match.SubMatches(4)
					temp = loopstr	
					POP_MVC.tpl_vars( tpl_label & "__" & "index" ) = i
					POP_MVC.tpl_vars( "__key" ) = j						
					if arr_label <> "" then
						if isObject( theArr(j) ) then
							set item = theArr(j)
							set POP_MVC.tpl_vars( "__value" ) = item
							set POP_MVC.tpl_vars( tpl_label & "__" & "value" ) = item
							temp = compile__( temp , l_d , loopCounter , item , true , dict_label )
						else
							item = theArr(j)	
							POP_MVC.tpl_vars( "__value" ) = item	
							POP_MVC.tpl_vars( tpl_label & "__" & "value" ) = item	
							temp = compile__( temp , l_d , loopCounter , item , false , dict_label )
						end if	
					else
						temp = compile__( temp , l_d , loopCounter , null , false , dict_label )
					end if
					nloopstr = nloopstr & temp
					i = i + 1
				next			
				
				Call parseCacheOutput(cacheBool,cache_label,nloopstr,fileBool,is_cached,cachePath,outputBool,match,content)
				call POP_MVC.tplPushTime( startTime , POP_MVC.String.encodeHtml( dict_label )  )
			end if
		next
		is_loop = false
		set match = nothing : set matches = nothing
		parseArrLoop = content
		Call L_( classType & ".parseArrLoop" )
	End Function
	
	'<!--{loopxml="filePath/domStr:path:类型:循环标志符"}-->...<!--{/loopxml="缓存名:是否立即输出:文件缓存时长"}-->
	'第1个参数是文件路径，若是$path，则从分配变量找,若是类似"../xml/1.xml"则直接加载文件内容
	'第1参数是节点集，即IXMLDOMSelection类型的$nodes，则直接遍历
	'类型有三种，对应第1个参数 xmlfile(默认,xml文件路径)、xmldocument(dom文本)、transfer(网址)
	'$__index,$__key,$__node
	'xml_loop	
	Function parseXmlLoop( byval content ,byval l_d,byval r_d,byref t_,byval loopCounter ,byval dict_label )	
		dim match,matches,attrRule,labelRule,separator,attrstr,startTime,loopstr,nloopstr,tpl_label,xmlPath,path,page,perpage
		dim arr,arg_str,arg_arr,i,bnd,key,item,temp,rulePrefix,ruleSuffix,is_assign_page
		dim cacheBool,cache_label,fileBool,is_cached,cachePath,outputBool,needParse
		dim start_,end_,step_,j
		dim xmlobj,nodeLen,stype,nodes,loop_label
		on error resume next
		parseXmlLoop = content
	

		if isEmpty( tplDict(dict_label) ) then set tplDict(dict_label) = CreateDict		
		if not isEmpty(tplDict( dict_label )( "xml_loop" )) and  tplDict( dict_label )( "xml_loop" ) = false  then
			exit function	
		end if

		if l_d <> left_delimiter then
			if inStr( content, "/loopxml" ) < 1 then
				tplDict( dict_label )( "xml_loop" ) = false
				exit Function
			else
				tplDict( dict_label )( "xml_loop" ) = true
			end if
		end if		
		
		Call parseRuleFix( rulePrefix,ruleSuffix,l_d,r_d,loopCounter)
		
		labelRule = "(" & rulePrefix & "\s*loopxml\s*\=\s*(\W+)(.+?)\2([\s\S]*?)" & ruleSuffix & ")" & "([\s\S]*?)" & rulePrefix & "\s*\/loopxml(.*?)" & ruleSuffix

		If NOt POP_MVC.String.reg_test( content , labelRule , "i" ) then
			 Exit Function
		end if	

		set matches = POP_MVC.String.reg_exec( content,labelRule , "gim" )
		if matches.count > 0  then
			tplDict(dict_label)("xml_loop") = true
		end if
		

		
		is_loop = true
		for each match in matches	
			startTime = timer()				
			dict_label = CStr(match.subMatches(0))
			call parseCacheArgs( match.SubMatches(5),cacheBool,cache_label,fileBool,is_cached,cachePath,outputBool,needParse,match,content,dict_label,startTime)
		
			'如果缓存了，
			if needParse then	
				'用:炸开loop"数组名:i=start:end值:step值"
				arr = split( match.SubMatches(2) , ":" )
				
				tpl_label = ""

				xmlPath = arr(0)	'文件夹路径
				
				
				if ubound( arr ) > 0 then
					path = arr(1)
				end if
				
				stype = "xmlfile"
				
				if ubound( arr ) > 1 then
					stype = lcase(arr(2))
				end if
				
				if ubound( arr ) > 2 then
					loop_label = lcase(arr(3))
				end if
				

				if POP_MVC.String.reg_test( xmlPath , "^(?:\$|\@)(\w+)$" , "" ) then
					xmlPath = mid(xmlPath,2)
					if typename( POP_MVC.tpl_vars(xmlPath) ) = "IXMLDOMSelection" then
						set nodes = POP_MVC.tpl_vars(xmlPath)	'实为nodes
					elseif typename( POP_MVC.tpl_vars(xmlPath) ) = "String" then
						xmlPath = POP_MVC.tpl_vars(xmlPath)	
					end if					
					tpl_label = xmlPath
				end if
				
				if POP_MVC.String.reg_test( path , "^(?:\$|\@)(\w+)$" , "" ) then
					path = mid(path,2)
					path = POP_MVC.tpl_vars(path)	
				end if				
				
				nloopstr = ""				
				i = 0
				
				if typename( POP_MVC.tpl_vars(tpl_label) ) <> "IXMLDOMSelection" then
					set  xmlobj = P_("xml")
					Call xmlobj.load( xmlPath ,stype)					
					set  nodes = xmlobj.getNodes(path)				
				end if
				
				nodeLen = nodes.Length
				
				'分页处理
				if POP_MVC.String.reg_test( match.SubMatches(3) , "page\s*=\s*(\W)(.*?)\1","i" ) then
					set oPage = P_("page")( nodeLen )
					arg_str = POP_MVC.String.reg_find( match.SubMatches(3) , "page\s*=\s*(\W)(.*?)\1",2,"i" )
					arg_arr = split( arg_str , "," )
					
					'第一个参数为当前页，第二个参数为每页多少条
					page = arg_arr(0)
					
					'每页多少条
					if ubound( arg_arr ) > 0 then
						perpage = arg_arr(1)
						oPage.perpage = CLng(perpage)
					end if
					
					'当前页为空，取传参的page
					if page = "" then
						POP_MVC.req( POP_MVC.config("VAR_PAGE") )
					end if
					
					if not POP_MVC.String.reg_test( page , "^[1-9]\d*$" , "" ) then
						page = 1
					end if
					oPage.page = CLng(page)
					
					is_assign_page = true
				else
					is_assign_page = false
				end if	
				
				

				if is_assign_page then
					arg_arr = oPage.getRange
					start_ = arg_arr(0)
					end_ = arg_arr(1)
				else
					start_ = 0
					end_ = nodeLen - 1
				end if
				
				step_ = 1

				for j = start_ to end_ step step_					
					POP_MVC.tpl_vars( "__index" ) = i					
					loopstr = match.SubMatches(4)
					temp = loopstr	
					POP_MVC.tpl_vars( loop_label & "__" & "index" ) = i
					POP_MVC.tpl_vars( "__key" ) = j						
					set item = nodes.item(j)
					POP_MVC.tpl_vars( "__value" ) = item.text	
					POP_MVC.tpl_vars( "__tagname" ) = item.BaseName	
					
					set POP_MVC.tpl_vars( "__node" ) = item
					set POP_MVC.tpl_vars( loop_label & "__" & "node" ) = item
					temp = compile__( temp , l_d , loopCounter , item , true , dict_label )
					nloopstr = nloopstr & temp
					i = i + 1
				next			
				
				Call parseCacheOutput(cacheBool,cache_label,nloopstr,fileBool,is_cached,cachePath,outputBool,match,content)
				call POP_MVC.tplPushTime( startTime , POP_MVC.String.encodeHtml( dict_label )  )
			end if
		next
		is_loop = false
		set match = nothing : set matches = nothing
		parseXmlLoop = content
		Call L_( classType & ".parseXmlLoop" )
	End Function		
	
	'输出用select、getAll、search取出的二维数据块
	'<!--{loopfield="表名:方法名:标识符" 连贯操作或参数}-->...<!--{/loopfield="缓存名:是否立即输出:文件缓存时长"}-->
	'如果有参数，使用arg_1="...",arg_2="..."来表示
	Function parseFieldLoop( byval content , l_d , r_d , loopCounter , byval dict_label )	
		dim match,matches,amatch,attrRule,amatches,labelRule,separator,attrstr,opt_key,opt_value,opt_equal,rs,model,startTime,loopstr,nloopstr
		dim tableName,method,arr,arg_str,arg_arr,i,bnd,key,item,temp,tpl_label,rulePrefix,ruleSuffix,bool,repStr
		dim cacheBool,cache_label,fileBool,is_cached,cachePath,outputBool,needParse,stype
		on error resume next
		parseFieldLoop = content
		if isEmpty( tplDict(dict_label) ) then set tplDict(dict_label) = CreateDict		
		
		if not isEmpty(tplDict( dict_label )( "field_loop" )) and  tplDict( dict_label )( "field_loop" ) = false then
			exit function	
		end if
		
		if l_d <> left_delimiter then
			if inStr( content, "/loopfield" ) < 1 then
				tplDict( dict_label )( "field_loop" ) = false
				exit Function
			else
				tplDict( dict_label )( "field_loop" ) = true
			end if
		end if		
		
		
		Call parseRuleFix( rulePrefix,ruleSuffix,l_d,r_d,loopCounter)
	
		labelRule =  "(" & rulePrefix & "\s*loopfield\s*\=\s*(\W+)(.+?)\2([\s\S]*?)" & ruleSuffix & ")" & "([\s\S]*?)" & rulePrefix & "\s*\/loopfield(.*?)" & ruleSuffix
		
		If NOt POP_MVC.String.reg_test( content , labelRule , "i" ) then
			 Exit Function
		end if	
		
		set matches = POP_MVC.String.reg_exec( content,labelRule , "gim" )
		if matches.count > 0  then
			tplDict(dict_label)("field_loop") = true
		end if
		is_loop = true
		

		for each match in matches
			startTime = timer()			
			dict_label = CStr(match.subMatches(0))
			call parseCacheArgs( match.SubMatches(5),cacheBool,cache_label,fileBool,is_cached,cachePath,outputBool,needParse,match,content,dict_label,startTime)
			
			'如果缓存了，
			if needParse then
				tpl_label = ""
				loopstr = match.SubMatches(4)				
				'用:炸开loop"表名:方法名:标识符"
				arr = split( match.SubMatches(2) , ":" )
				'分配标识符
				if ubound( arr ) > 1 then				
					tpl_label = arr(2)
				end if
	
				if lcase(tpl_label) = "request.querystring" or lcase(tpl_label) = "request.form" or lcase(tpl_label) = "request.servervariables" then
					Execute( "set rs = " & tpl_label )
				elseif tpl_label="" or not POP_MVC.tpl_vars.exists( tpl_label ) then				
					tableName = arr(0)
					
					'默认使用select方法
					method = "find"
					
					'分配方法名
					if ubound( arr ) > 0 then				
						method = arr(1)
					end if
					
					'得到连贯操作与参数字符串
					attrstr = match.SubMatches(3)
					separator = match.SubMatches(1)					
					
					attrRule = "(\w+)\s*(=:?)\s*" & "\" & separator & "(.+?)" & "\" & separator
					set model = M_(tableName).db					
					arg_str = ""							
					
					'替换字段
					If POP_MVC.String.reg_test( attrstr , attrRule , "gim" ) then
						arg_arr = array()
						set amatches = POP_MVC.String.reg_exec( attrstr,attrRule , "gim" )
						for each amatch in amatches
							opt_key = amatch.SubMatches(0)		'连贯操作如where、field、group等
							opt_equal = amatch.SubMatches(1)		'=或=:
							opt_value = amatch.SubMatches(2)	'连贯操作where等的值，可含{$var}等	
							
							if opt_equal = "=" then
								opt_value = prefixReplace( opt_value,l_d,r_d  , t_ )
								if not POP_MVC.String.reg_test( opt_value, "^[a-zA-Z]\w*$" , "" ) then
									Execute( "if isObject(" & opt_value & ") then set opt_value = " & opt_value & " else opt_value = " & opt_value )
								end if
							elseif match.subMatches(1) = "=:" then
								'不做任何修改
							end if
							
							'如果要传参，传参的前缀是arg_，按次序分配值。如getRS(sql)，此时可以 arg_1="sql"
							if not POP_MVC.String.iStartsWith( opt_key , "arg_" ) then
								execute( "model." & opt_key & "(opt_value)" )
							else
								POP_MVC.arr.push arg_arr,opt_value
							end if
						next
						bnd = ubound( arg_arr )
						
						if bnd > -1 then
							for i = 0 to bnd
								if i <> bnd then
									arg_str = arg_str & "arg_arr(" & i &"),"
								else
									arg_str = arg_str & "arg_arr(" & i &")"
								end if
							next
						end if
					end if
					
					execute( "set rs = model." & method & "( " & arg_str & " )" ) 
					
					'给POP_MVC.tpl_vars分配rs，可以使用V_(tpl_label)调用
					set POP_MVC.tpl_vars( tpl_label ) = rs
				
				else
					set rs = POP_MVC.tpl_vars( tpl_label )
				end if
				
				nloopstr = ""
				i = 0
				temp = loopstr
				
				bool = true
				stype = typename( rs )
				if stype = "Recordset" then	'如果是Recordset
					if not rs.BOF and not rs.EOF then					
						for i = 0 to rs.fields.count-1
							POP_MVC.tpl_vars( "__index" ) = i					
							POP_MVC.tpl_vars( tpl_label & "__" & "index" ) = i
							call ParseKVloopstr( i+1 , rs.Fields(i).Name, rs.Fields(i).Value,tpl_label,repStr,bool,temp,l_d,loopCounter,dict_label,nloopstr,rs  )	
						next
					end if					
				elseif stype = "Dictionary" or stype = "IRequestDictionary" then
				'如果是Dictionary，使用for each循环，输出所有数据
				

					for each key in rs
						POP_MVC.tpl_vars( "__index" ) = i					
						POP_MVC.tpl_vars( tpl_label & "__" & "index" ) = i
						call ParseKVloopstr( i , key, rs(key),tpl_label,repStr,bool,temp,l_d,loopCounter,dict_label,nloopstr,rs  )
						i = i + 1
					next
				elseif POP_MVC.String.startswith( stype, "POPASP_SELF_OBJECT" ) then
					for i = 0 to  ubound(rs.vars__)					
						POP_MVC.tpl_vars( "__index" ) = i					
						POP_MVC.tpl_vars( tpl_label & "__" & "index" ) = i
						call ParseKVloopstr( i , rs.vars__(i), rs.Get__(rs.vars__(i)),tpl_label,repStr,bool,temp,l_d,loopCounter,dict_label,nloopstr,rs  )
					next
				end if	
			
				Call parseCacheOutput(cacheBool,cache_label,nloopstr,fileBool,is_cached,cachePath,outputBool,match,content)
				call POP_MVC.tplPushTime( startTime , POP_MVC.String.encodeHtml( dict_label )  )
			end if
		next
		is_loop = false
		set match = nothing : set matches = nothing
		parseFieldLoop = content
		Call L_( classType & ".parseFieldLoop" )
	End Function	

	'<!--{loopfile="文件夹路径:遍历方法名:标识符" }-->...<!--{/loopfile="缓存名:是否立即输出:文件缓存时长"}-->
	'遍历方法，可以使用 file_map,folder_map,file_page_map,folder_page_map,files_map,folders_map,all_map
	'file_loop
	Function parseFileLoop( byval content ,byval l_d,byval r_d,byref t_,byval loopCounter , byval dict_label  )
		dim match,matches,attrRule,labelRule,separator,attrstr,opt_key,opt_value,opt_equal,loopstr,startTime,amatches,amatch
		dim dirPath,method,arr,arg_str,arg_arr,i,tpl_label,rulePrefix,ruleSuffix,bool
		dim cacheBool,cache_label,fileBool,is_cached,cachePath,outputBool,needParse
		on error resume next
		parseFileLoop = content
		
		if isEmpty( tplDict(dict_label) ) then set tplDict(dict_label) = CreateDict	

		if not isEmpty(tplDict( dict_label )( "file_loop" )) and  tplDict( dict_label )( "file_loop" ) = false  then
			exit function	
		end if
		
		if l_d <> left_delimiter then
			if inStr( content, "/loopfile" ) < 1 then
				tplDict( dict_label )( "file_loop" ) = false
				exit Function
			else
				tplDict( dict_label )( "file_loop" ) = true
			end if
		end if		
		
	
		Call parseRuleFix( rulePrefix,ruleSuffix,l_d,r_d,loopCounter)
		
		labelRule = "(" & rulePrefix & "\s*loopfile\s*\=\s*(\W+)(.+?)\2([\s\S]*?)" & ruleSuffix & ")" & "([\s\S]*?)" & rulePrefix & "\s*\/loopfile(.*?)" & ruleSuffix

		If NOt POP_MVC.String.reg_test( content , labelRule , "i" ) then
			 Exit Function
		end if	
		
		set matches = POP_MVC.String.reg_exec( content,labelRule , "gim" )
		if matches.count > 0  then
			tplDict(dict_label)("file_loop") = true
		end if
		
		is_loop = true
		for each match in matches	
			file_index = 0
			startTime = timer()
			dict_label = CStr(match.subMatches(0))
			call parseCacheArgs( match.SubMatches(5),cacheBool,cache_label,fileBool,is_cached,cachePath,outputBool,needParse,match,content,dict_label,startTime)
			
			'如果缓存了，
			if needParse then			
				tpl_label = ""
				loopstr = match.SubMatches(4)
				'用:炸开loop"表名:方法名:标识符"
				arr = split( match.SubMatches(2) , ":" )
				tpl_label = ""
				'分配标识符
				if ubound( arr ) > 1 then				
					tpl_label = arr(2)
				end if
				
				dirPath = arr(0)	'文件夹路径
				if dirPath  <> "" then
					dirPath = prefixReplace( dirPath,l_d,r_d  , t_ )
					if not POP_MVC.String.reg_test( dirPath, "^[a-zA-Z]\w*$" , "" ) then
						Execute( "dirPath = " & dirPath )
					end if
				end if
				
				if tpl_label <> "" and POP_MVC.tpl_vars.exists(tpl_label) then
					dirPath = POP_MVC.tpl_vars(tpl_label)
				end if
				
				if POP_MVC.file.isFolder( dirPath ) then
					
					'分配方法名
					if ubound( arr ) > 0 then	'POP_MVC.file.method中的method名称	
						method = prefixReplace( arr(1),l_d,r_d  , t_ )					
						if not POP_MVC.String.reg_test( method, "^[a-zA-Z]\w*$" , "" ) then
							Execute( "method = " & method )
						end if
					end if	
					


					if method = "" then
						'默认使用select方法
						method = "file_map"				
					end if
					
					'得到连贯操作与参数字符串
					attrstr = match.SubMatches(3)
					separator = match.SubMatches(1)
					
					attrRule = "(\w+)\s*(=:?)\s*" & "\" & separator & "(.+?)" & "\" & separator
					
					POP_MVC.file.tpl_filter = ""
				
					If POP_MVC.String.reg_test( attrstr , attrRule , "gim" ) then
						set amatches = POP_MVC.String.reg_exec( attrstr,attrRule , "gi" )
							
						
						for each amatch in amatches
							opt_key = amatch.SubMatches(0)		'
							opt_equal = amatch.SubMatches(1)	'
							opt_value = amatch.SubMatches(2)	'
			
							if opt_equal = "=" then
								opt_value = prefixReplace( opt_value,l_d,r_d  , t_ )
								execute( "opt_value = " & opt_Value )
							elseif match.subMatches(1) = "=:" then
								'不做任何修改
							end if
							
							if LCase( opt_key ) = "filter" then
								POP_MVC.file.tpl_filter = opt_value
							end if
						next
					end if					

					POP_MVC.file.is_use_tpl_compiler = true
					bool = true
					POP_MVC.file.tpl_repStr = loopStr
					POP_MVC.file.tpl_l_d = l_d
					POP_MVC.file.tpl_loopCounter = loopCounter
					POP_MVC.file.tpl_bool = bool
					POP_MVC.file.tpl_start = true
					POP_MVC.file.tpl_count = 0
					POP_MVC.file.tpl_dict_label = dict_label			
					
					Execute "Call POP_MVC.file." & method & "( dirPath, ""P_(""""POPASP_TEMPLATE_COMPILER"""").compile_callback"" )"
				end if

				Call parseCacheOutput(cacheBool,cache_label,POP_MVC.file.tpl_nloopstr,fileBool,is_cached,cachePath,outputBool,match,content)
				
				POP_MVC.tpl_vars( "__count"  ) = POP_MVC.file.tpl_count
				
				if cache_label <> "" then
					POP_MVC.tpl_vars( cache_label & "__count"  ) = POP_MVC.file.tpl_count
				end if
				
				POP_MVC.file.tpl_nloopstr = ""
				POP_MVC.file.tpl_count = 0
				POP_MVC.file.tpl_filter = ""
				call POP_MVC.tplPushTime( startTime , POP_MVC.String.encodeHtml( dict_label )  )
			end if
		next

		is_loop = false
		set match = nothing : set matches = nothing
		parseFileLoop = content
		Call L_( classType & ".parseFileLoop" )
	End Function
	
	'<!--{flow 递归初始值}-->...<!--{/flow="缓存名:是否立即输出:文件缓存时长"}-->
	'file_loop
	Function parseFlow( byval content ,byval l_d,byval r_d,byref t_,byval loopCounter , byval dict_label  )
		dim match,matches,labelRule,startTime,rulePrefix,ruleSuffix,loopstr,nloopstr,temp
		dim amatches,amatch,attrstr,attrRule,varName,str,execStr
		dim cacheBool,cache_label,fileBool,is_cached,cachePath,outputBool,needParse
		on error resume next
		parseFlow = content
		
		Call parseRuleFix( rulePrefix,ruleSuffix,l_d,r_d,loopCounter)
		
		labelRule = "((" & rulePrefix & "\s*flow)\s*(.*?)(" & ruleSuffix & "))" & "([\s\S]*?)" & rulePrefix & "\s*\/flow(.*?)" & ruleSuffix

		If NOt POP_MVC.String.reg_test( content , labelRule , "i" ) then
			 Exit Function
		end if	
		
		set matches = POP_MVC.String.reg_exec( content,labelRule , "gim" )
	
		attrRule = "(\w+)\s*(=:?)\s*(\W)(.+?)\3"
		for each match in matches	
			startTime = timer()			
			attrstr = match.subMatches(2)
			'替换字段
			If POP_MVC.String.reg_test( attrstr , attrRule , "gim" ) then
				set amatches = POP_MVC.String.reg_exec( attrstr,attrRule , "gim" )
				for each amatch in amatches
					varName = amatch.SubMatches(0)		'如 ParentID="0" 
					opt_equal = amatch.SubMatches(1)		'=或=:
					str = amatch.SubMatches(3)	'ParentID等的值，可含$var等					
					if opt_equal = "=" then
						str = prefixReplace( str,l_d,r_d  , t_ )								
					elseif opt_equal = "=:" then
						'不做任何修改
					end if					
					call varNameReplace(execStr,str,varName)
					Execute(  execStr )	
				next
			end if			
			
			dict_label = CStr(match.subMatches(0))
			call parseCacheArgs( match.SubMatches(5),cacheBool,cache_label,fileBool,is_cached,cachePath,outputBool,needParse,match,content,dict_label,startTime)
			nloopstr = ""
			'如果缓存了，

			if needParse then			
				loopstr = match.SubMatches(4)
				nloopstr = compile__( loopstr , l_d , loopCounter , null , false , dict_label )	

				if b_digui and not isNull(t_) then			
					if digui_count > 500 then exit function
					if instr(loopstr,"{digui|") then
						set amatches = POP_MVC.String.reg_exec( nloopstr,"{digui\|(.+?)}" , "gim" )		
						for each amatch in amatches
							temp = amatch.value
							temp = compile__(  temp , l_d , loopCounter-1 , t_ , true , dict_label )	
							nloopstr = replace( nloopstr, amatch.value , temp)
						next 
					end if
				end if
				if b_digui then
					if digui_count > 500 then exit function
					if instr(nloopstr,"{digui|") then
						set amatches = POP_MVC.String.reg_exec( nloopstr,"{digui\|(.+?)}" , "gim" )	
						for each amatch in amatches
							str = match.SubMatches(1) & " " &  amatch.subMatches(0) &  match.SubMatches(3) & match.SubMatches(4) & replace(match.SubMatches(1),"flow","/flow") & match.SubMatches(3)
							str = parseFlow( str , l_d, r_d, t_, loopCounter , dict_label  )
							nloopstr = replace( nloopstr , amatch.value , str )
						next				
					end if
					digui_count = digui_count + 1
				end if
				Call parseCacheOutput(cacheBool,cache_label,nloopstr,fileBool,is_cached,cachePath,outputBool,match,content)			
				call POP_MVC.tplPushTime( startTime , POP_MVC.String.encodeHtml( dict_label )  )
			end if
		next
		set match = nothing : set matches = nothing
		parseFlow = content
		Call L_( classType & ".parseFlow" )
	End Function
	
	'解析css与js的引入
	Function parseCSSJS( byval content )		
		dim match,matches,pattern,startTime,str,arr,href,repStr,i,bnd,label
		on error resume next
		pattern = left_delimiter & "\s*(js|css)\s+(?:file|href|src)\s*=\s*(\W?)(.*?)\2\s*" & right_delimiter
		set matches = POP_MVC.String.reg_exec( content , pattern, "gi" )
		startTime = timer
		for each match in matches
			label = match.SubMatches(0)
			str = match.SubMatches(2)
			arr = split( str , ",")
			repStr = ""
			bnd = ubound( arr )
			for i = 0 to bnd
				href = arr(i)
				href = prefixReplace( href,left_delimiter,right_delimiter, null )
				'if not POP_MVC.String.reg_test( href, "^[a-zA-Z]\w*$" , "" ) then
					Execute( "href = " & href )
					if err.number = 424 then err.clear
				'end if
				if label = "css" then
					repStr = repStr & "<LINK href=""" & href & """ type=""text/css"" rel=""stylesheet"">"
				elseif label = "js" then
					repStr = repStr & "<script type=""text/javascript"" src=""" & href & """></script>"
				end if
				if i <> bnd then
					repStr = repStr & vbCrLf
				end if
			next
			content = replace( content,match.value , repStr )
			call POP_MVC.tplPushTime( startTime , POP_MVC.String.encodeHtml( match.value )  )
			startTime = timer
		next
		parseCSSJS = content
		call L_( classType & ".parseCSSJS" )
	end Function		
	
	Private Function baseReplace_after( byval context , byval l_d, byval r_d ,var_prefix ,t_  ,loopCounter ,byval dict_label )
		on error resume next
		dim str,i,matches,match,condition,str_true,str_false,pattern,repStr,startTime

		Call loopcntReplace( l_d,r_d,loopCounter,dict_label )
		
		'三元输出运算符
		'=( V_("toggle") = 1 ) ? "开启" : "关闭"	
		'three_output
		
		if b_three_output then
			if l_d = left_delimiter then
				pattern = l_d & "[ ]*(\=\:?)([^\}]+?)(\?\:*)([^\}]+?)([\:]+)([^\}]+?)" & r_d
			else		
				pattern = "(?:" & l_d & ")"  & "{1}" &  "(?!\[+)" &  "[ ]*(\=\:?)([^\[\]]+?)(\?\:*)([^\[\]]+?)([\:]+)([^\[\]]*?)[ ]*" & "(?:" & r_d & ")" & "{1}"  &  "(?!\])"
			end if	
			
			set matches = POP_MVC.String.reg_exec(context, pattern ,"gi")
	
			
			for each match in matches
				startTime = timer()
				str = match.subMatches(1)
				
				if match.subMatches(0) = "=" then	'如果是两个::则使用原样
					str =  prefixReplace( str,l_d,r_d  , t_ )
					Execute( "str = " & str )
				elseif match.subMatches(0) = "=:" then
					'不做任何修改
				end if	
				
				
			
				if isNul(str) then
					condition = false
				else
					Execute("condition = " & str )	
				end if

				if err.number <> 0 then
					if var_prefix = "@" then
						POP_MVC.error( classType & ".baseReplace_after:" & "在字符串 " & match.value & "中 " & typename(t_) & "中查找键" & str & "时，发现错误：" & err.description )
					else
						POP_MVC.error( classType & ".baseReplace_after:" & "输出 " & match.value & " 时发现错误：" & err.description )
					end if
				else
					call POP_MVC.tplPushTime( startTime , POP_MVC.String.encodeHtml( match.value )  )
				end if	


				if condition and CStr(condition) <> "0" then
					str = match.subMatches(3)	
					if match.subMatches(2) = "?" then
						str =  prefixReplace( str,l_d,r_d  , t_ )
						Execute( "str = " & str )	
					else
						str =  delimiterReplace( str,l_d,r_d )
					end if
				else
					str = match.subMatches(5)
					if match.subMatches(4) = ":" then
						str =  prefixReplace( str,l_d,r_d  , t_ )
						Execute( "str = " & str )	
					else
						str =  delimiterReplace( str,l_d,r_d )
					end if
				end if
				if isNull( str ) then str = ""
				context = Replace(context,match.value,str)
				call POP_MVC.tplPushTime( startTime , POP_MVC.String.encodeHtml( match.value )  )
			next
			set matches = nothing
		end if

		'二元输出
		'=( V_("toggle") = 1 ) ? "开启"
		'two_output
		if b_two_output then
			
			if l_d = left_delimiter then
				pattern = l_d & "[ ]*(\=\:?)([^\}]+?)(\?\:*)(.+?)[ ]*" & r_d
			else		
				pattern = "(?:" & l_d & ")"  & "{1}" &  "(?!\[+)" &  "[ ]*(\=\:?)([^\[\]]+?)(\?\:*)([^\[\]]+?)[ ]*" & "(?:" & r_d & ")" & "{1}"  &  "(?!\])"
			end if		
			set matches = POP_MVC.String.reg_exec(context, pattern ,"gi")
			for each match in matches
				
				startTime = timer()
				str = match.subMatches(1)
				
				if match.subMatches(0) = "=" then
					str =  prefixReplace( str,l_d,r_d  , t_ )						
					Execute( "str = " & str )
				elseif match.subMatches(0) = "=:" then
					'不做任何修改
				end if	

				if str = "" then
					condition = false
				else
					Execute("condition = " & str )
				end if
				if err.number <> 0 then
					if var_prefix = "@" and err.number = 1001 then
						POP_MVC.error( classType & ".baseReplace_after:" & "在字符串 " & match.value & "中 " & typename(t_) & "中查找键" & str & "时，发现错误：" & err.description )
					else
						POP_MVC.error( classType & ".baseReplace_after:" & "输出 " & match.value & " 时发现错误：" & err.description )
					end if
				else
					call POP_MVC.tplPushTime( startTime , POP_MVC.String.encodeHtml( match.value )  )
				end if	
				
				if condition and CStr(condition) <> "0" then
					str = match.subMatches(3)	
					if match.subMatches(2) = "?" then
						str =  prefixReplace( str,l_d,r_d  , t_ )
						Execute( "str = " & str )
					else
						str =  delimiterReplace( str,l_d,r_d )
					end if
				else
					str = ""
				end if
				if isNull( str ) then str = ""			
		
				
				context = Replace(context,match.value,str)
				call POP_MVC.tplPushTime( startTime , POP_MVC.String.encodeHtml( match.value )  )
			next
			set matches = nothing
		end if
	
		'表达式输出
		'=1+2
		'output_exp
		if b_output_exp then			
			if l_d = left_delimiter then
				pattern = l_d & "\s*(\=\:?)([^\?}]+?)\s*" & r_d
			else	
				pattern = "(?:" & l_d & ")"  & "{1}" &  "(?!\[+)" &   "[ ]*(\=\:?)([^\?\]]+?)[ ]*" & "(?:" & r_d & ")" & "{1}"  &  "(?!\])"
			end if
			set matches = POP_MVC.String.reg_exec(context, pattern  ,"gi")	
						
			for each match in matches
				startTime = timer()
				str = match.subMatches(1)
				
				if match.subMatches(0) = "=" then
					str =  prefixReplace( str ,l_d,r_d  , t_ )
				elseif match.subMatches(0) = "=:" then
					'不做任何修改
				end if
				

				Execute( "str = " & str )
				if err.number <> 0 then
					POP_MVC.error( classType & ".baseReplace_after:" & "解析 " & match.value & "为" & POP_MVC.String.encodeHtml( str ) & "时不能被Execute,错误：" & err.description )
				end if
				if isNull( str ) then str = ""
				context = Replace(context,match.value, str)
				call POP_MVC.tplPushTime( startTime , POP_MVC.String.encodeHtml( match.value )  )
			next
			set matches = nothing
		end if
		
		'默认值输出
		'$varname|default="默认值"
		'$varname|default=:默认值
		'default_output
		if b_default_output then
			if l_d = left_delimiter then
				pattern = l_d & "[ ]*(\" & var_prefix & ".*?)\|default(=:?)(.*?)[ ]*" & r_d
			else		
				pattern = "(?:" & l_d & ")"  & "{1}" &  "(?!\[+)" & "[ ]*((\" & var_prefix & "|\$)([^\@\]]+?))\|default(=:?)(.*?)[ ]*" & "(?:" &  r_d & ")" & "{1}"  &  "(?!\])"
			end if		
			set matches = POP_MVC.String.reg_exec(context, pattern ,"gi")

			for each match in matches
				startTime = timer()
				str = match.subMatches(0)
				str =  prefixReplace( str,l_d,r_d  , t_ )
				Execute("condition = isNul(" & str & ")" )	
				if condition then
					str = match.subMatches(2)	
					if match.subMatches(1) = "=" then
						str =  delimiterReplace( str,l_d,r_d )
					else
						str =  prefixReplace( str,l_d,r_d  , t_ )
						Execute( "str = " & str )
					end if
				else
					Execute( "str = " & str )	
				end if
				if isNull( str ) then str = ""
				context = Replace(context,match.value,str)
				call POP_MVC.tplPushTime( startTime , POP_MVC.String.encodeHtml( match.value )  )
			next
			set matches = nothing
		end if
		
		
		
		'变量输出
		'that.d("site") = "http://www.popasp.com"  =>  {$site}
		'that.d("rs") = rs   => {$rs.content}	'说明rs为从数据库出使用find取出的一条记录
		'output_var		
			if l_d = left_delimiter then
				pattern = l_d & "[ ]*(\" & var_prefix & ".*?)[ ]*" & r_d
			else	
				pattern = "(?:" & l_d & ")"  & "{1}" &  "(?!\[+)" & "[ ]*((\" & var_prefix & "|\$)([^\@\]]+?))[ ]*" & "(?:" &  r_d & ")" & "{1}"  &  "(?!\])"
			end if
			set matches = POP_MVC.String.reg_exec(context, pattern ,"gi")	
			
			for each match in matches
				startTime = timer()
				str = prefixReplace( match.subMatches(0), l_d, r_d, t_ )	
				Execute( "str = " & str )	

				if isNull( str ) then str = ""
				context = Replace(context,match.value,str)
				if err.number <> 0 then
					if var_prefix = "@" then
						POP_MVC.error( classType & ".baseReplace_after:" & "在字符串 " & match.value & "中 " & typename(t_) & "中查找键" & str & "时，发现错误：" & err.description )
					else
						POP_MVC.error( classType & ".baseReplace_after:" & "输出 " & match.value & " 时发现错误：" & err.description )
					end if
				else
					call POP_MVC.tplPushTime( startTime , POP_MVC.String.encodeHtml( match.value )  )
				end if
			next
			set matches = nothing			
		baseReplace_after = context		
		Call L_( classType & ".baseReplace_after" )
	end function	
	
	'''''''''''''''''''''关键函数执行结束''''''''''''''''''''''''''''''''''''''
	
	public property get template( )
		template = s_template
	end property
	
	public property let template( value )
		s_template = value
		file_label = replace( value ,POP_MVC.config("TMPL_TEMPLATE_SUFFIX") , "" )
		file_label = POP_MVC.String.reg_replace( file_label, "_" , "\W" , "g" ) 
	end property
	
	Function MarkReplace( byval str , prefix, pattern,repStr )
		dim matches,match,num
		if inStr( str, prefix ) then
			set matches = POP_MVC.String.reg_exec( str, pattern , "g" )		
			for each match in matches
				num = match.subMatches(0)
				if num = "" then
					num = 1
				end if
				str = replace( str, match.value, POP_MVC.String.repeat( repStr , num ) )
			next
		end if
		MarkReplace = str
	End Function
	
	'替换代替字符
	'__LJ__ : {
	'__RJ__ : }
	'__LF__ : [
	'__RJ__ : ]
	'__WH__ : ?
	'__MH__ : :
	Function delimiterReplace( ByVal str ,l_d,r_d  )
		on error resume next
		if b_LJ then
			str = MarkReplace( str ,"__LJ" ,"__LJ(\d*)__" ,  left_delimiter )
		end if
		
		if b_RJ then
			str = MarkReplace( str ,"__RJ" ,"__RJ(\d*)__" ,  right_delimiter )
		end if
		
		if b_LF then
			str = MarkReplace( str ,"__LF" ,"__LF(\d*)__" ,  "[" )
		end if		
		
		if b_RF then
			str = MarkReplace( str ,"__RF" ,"__RF(\d*)__" ,  "]" )
		end if
		
		if b_WH then
			str = MarkReplace( str ,"__WH" ,"__WH(\d*)__" ,  "?" )
		end if	

		if b_MH then
			str = MarkReplace( str ,"__MH" ,"__MH(\d*)__" ,  ":" )
		end if	
		
		delimiterReplace = str
		Call L_( classType & "delimiterReplace" )
	End Function	
	
	Function prefixReplace( ByVal str ,l_d,r_d  , t_ )
		on error resume next
		dim pattern,repStr,matches,match,num
		str = delimiterReplace( str , l_d , r_d  )		
		if inStr( str, "$" ) > 0 then
			pattern = "\" & "$" & "([\w\u0391-\uFFE5]+(?:\.[\w\u0391-\uFFE5]+)?)"	
			repStr = "V_(""$1"")"
			str = POP_MVC.String.reg_replace( str,  repStr  ,pattern , "gim" ) 
		end if
		if not isNull(t_) then
			if inStr( str, "@" ) > 0 then
				if typename(t_) = "Recordset" then
					pattern = "@" & "(\d+)"	
					repStr = "t_($1)"
					repStr = repStr & ".Value"
					str = POP_MVC.String.reg_replace( str,  repStr  , pattern ,"gim" ) 
				end if
				if typename(t_) = "Dictionary" and is_loopdb_dict then
					pattern = "@" & "(\d+)"	
					repStr = "t_(dictKeys($1))"
					str = POP_MVC.String.reg_replace( str,  repStr  , pattern ,"gim" ) 
				end if	
				
				pattern = "@" & "([\w\u0391-\uFFE5]+(?:\.[\w\u0391-\uFFE5]+)?)"	
				if typename( t_ ) = "File" or  typename( t_ ) = "Folder" then
					repStr = "t_.$1"
				elseif typename(t_) = "Dictionary" and is_loopdb_dict then
					repStr = "t_(dictKeys(POP_MVC.Arr.iSearch( dictKeys, ""$1"")))"
				elseif typename(t_) = "IXMLDOMElement" then
					repStr = "repNull(t_.getAttribute(""$1""))"			
				else
					repStr = "t_(""$1"")"
				end if				
				if typename(t_) = "Recordset" then
					repStr =  repStr & ".Value"
				end if				
				str = POP_MVC.String.reg_replace( str,  repStr  , pattern ,"gim" ) 	
			end if
		end if
		prefixReplace = str
		Call L_( classType & "prefixReplace" )
	End Function

	Private sub loopcntReplace( l_d,r_d,loopCounter,dict_label )
		l_d = POP_MVC.String.repeat( l_d, loopCounter )
		r_d = POP_MVC.String.repeat( r_d, loopCounter )
		
		if not tplDict.exists(dict_label) then set tplDict(dict_label) = CreateDict	
	end sub
	
	Private sub varNameReplace( byref execStr , str, varName )
		on error resume next
		if inStr( varName , "(" ) then
			execStr =  "POP_MVC.tpl_vars(""" & replace( varName , "(" , """)(" ) & "=" & str
		elseif inStr( varName , "." ) then
			execStr = "(""" & Replace( varName, POP_MVC.config("TMPL_VARNAME_SEPARATOR") , """)(""" ) & """)"
			execStr = POP_MVC.String.reg_replace( execStr , "($1)" , "(""\d+"")" , "g" )
			execStr =  "POP_MVC.tpl_vars" & execStr & "=" & str
		else
			execStr =  "P_(""controller"").d(varName)=" & str
		end if	
		Call L_( classType & ".varNameReplace" )
	end sub
	
	Function compile_callback( ByRef nloopstr, byval repStr ,byval  l_d , byval loopCounter , byref collect ,Byval bool , byval dict_label )
		on error resume next
		dim stype
		stype = typename(collect)
		if  stype = "File" or stype = "Folder" then
			POP_MVC.tpl_vars("__level") = ubound( split( collect.path , "\" ) )			
			POP_MVC.tpl_vars("__index") = file_index
			file_index = file_index + 1
		end if
		if repStr <> "" then
			if  bool  then
				if l_d = left_delimiter then
					repStr = compile_(repStr , "\[" , "\]" ,"@", loopCounter , collect , dict_label )
				else
					repStr = compile_(repStr , "\[" , "\]" ,"@", loopCounter + 1, collect , dict_label )
				end if
			else
				if l_d = left_delimiter then
					repStr = compile_(repStr , "\[" , "\]" ,"$", 1, null , dict_label )
				else
					repStr = compile_(repStr , "\[" ,"\]" ,"$", loopCounter+1, null , dict_label )
				end if			
			end if
		end if
		nloopstr= nloopstr & repStr
		Call L_( classType & ".compile_callback" )
	End Function	
	
	
	Private sub parseCacheArgs( byval cacheStr, byref cacheBool,byref cache_label,byref fileBool,byref is_cached,byref cachePath,byref outputBool,byref needParse,byref match,byref content,byref dict_label,byVal startTime)
		on error resume next
		dim lifeTime,arr,bnd,temp
		cacheBool = false
		fileBool = false
		outputBool = true	'是否立即输出
		cache_label = ""	'缓存标识符
		lifeTime = 0		'页面缓存标识符
		is_cached = true	'是否文件缓存
		needParse = true	'是否需要解析
		'缓存字符串
		
		cacheStr = trim(cacheStr)
		if cacheStr <> "" then
			cacheStr = mid( cacheStr , 2 )
			cacheStr = trim( cacheStr )
			cacheStr = mid( cacheStr , 2 , len(cacheStr) - 2 )
			arr = split( cacheStr , ":" )
			cacheBool = true
			cache_label = arr(0)

			temp = cache_label
			cache_label = prefixReplace( cache_label,left_delimiter,right_delimiter  , null )
			
			Execute( "cache_label = " & cache_label )
			
			if cache_label = "" then cache_label = temp
			
			bnd = ubound(arr)
			if bnd > 0 then 
				if arr(1) = 0 then
					outputBool = false
				end if
				
				if arr(1) = "" then
					outputBool = true
				end if
			end if
			
			if bnd > 1 then '页面缓存
				if arr(2) = "" then
					lifeTime = cache_lifetime
				else					
					lifeTime = arr(2)
				end if
				
				lifeTime = CLng( lifeTime )
				if lifeTime > 0 then						
					fileBool = true					
					cachePath = POP_MVC.appPath & "/Runtime/Cache/"   & file_label & "#" & cache_label & ".txt"			
					if Not file_exists(cachePath)  Then
						is_cached = false
					ElseIf dateCompare(filemtime(template_file),filemtime(cachePath)) > 0 Then	
						is_cached = false
					ElseIf dateCompare(now(),filemtime(cachePath)) > lifeTime Then	
						is_cached = false
					End If	
				end if
			end if
		end if
		
		'如果缓存了，
		if cache_label <> "" and POP_MVC.tpl_vars.exists( cache_label ) then
			needParse = false
			if outputBool then
				content = replace( content,match.value, POP_MVC.tpl_vars( cache_label ),1,1,0 )					
			else
				content = replace( content,match.value, "",1,1,0 )
			end if	
			call POP_MVC.tplPushTime( startTime , "缓存中的" & POP_MVC.String.encodeHtml( dict_label )  )
		elseif cache_label <> "" and fileBool and is_cached then
			needParse = false
			temp = POP_MVC.file_get_contents( cachePath )
			POP_MVC.tpl_vars( cache_label ) = temp
			if outputBool then
				content = replace( content,match.value, temp,1,1,0 )					
			else
				content = replace( content,match.value, "",1,1,0 )
			end if
			call POP_MVC.tplPushTime( startTime , "缓存文件中的" & POP_MVC.String.encodeHtml( dict_label )  )
		end if
		
		Call L_( classType & ".parseCacheArgs" )
	End Sub	
	
	Private sub parseCacheOutput( byref cacheBool,byref cache_label,Byref repStr,byref fileBool,byref is_cached,byref cachePath,byref outputBool,byref match,byref content)
		on error resume next
		if cacheBool  then
			POP_MVC.tpl_vars( cache_label ) = repStr
		end if
		
		if fileBool and not is_cached then
			Call POP_MVC.file_put_contents_without_bom( cachePath , repStr )
		end if

		if outputBool then
			content = replace( content,match.value, repStr)
		else
			content = replace( content,match.value, "")
		end if
		Call L_( classType & ".parseCacheOutput" )
	End Sub
	
	Public Sub ParseKVloopstr(ByRef i , ByRef key, ByRef value, ByRef tpl_label, ByRef repStr, ByRef bool, ByVal temp, ByRef l_d, ByRef loopCounter, ByRef dict_label, ByRef nloopstr,ByRef rs  )
		on error resume next
		POP_MVC.tpl_vars( "__index" ) = i
		POP_MVC.tpl_vars( "__key" ) = key
		if isObject( value ) then
			set POP_MVC.tpl_vars( "__value" ) = value
		else
			POP_MVC.tpl_vars( "__value" ) = value
		end if
		if not isEmpty( tpl_label ) then
			POP_MVC.tpl_vars( tpl_label & "__key" ) = key
			if isObject( value ) then
				set POP_MVC.tpl_vars( tpl_label & "__value" ) = value
			else
				POP_MVC.tpl_vars( tpl_label & "__value" ) = value
			end if
			POP_MVC.tpl_vars( tpl_label & "__" & "index" ) = i
		end if	
		if bool then
			if temp <> "" then
				repStr = compile__( temp , l_d , loopCounter , rs , true , dict_label )
			end if
			if repStr = temp then
				bool = false
			else
				temp = repStr
			end if
		end if
		
		nloopstr = nloopstr & temp	
		call L_( classType & ".ParseKVloopstr" )
	end sub
	
	Function compile__( byval repStr , l_d , loopCounter , byref collect , bool , byval dict_label )
		on error resume next		
		if repStr <> "" then
			if  bool  then
				if l_d = left_delimiter then
					repStr = compile_(repStr , "\[" , "\]" ,"@", loopCounter , collect , dict_label )
				else
					repStr = compile_(repStr , "\[" , "\]" ,"@", loopCounter + 1, collect , dict_label )
				end if
			else
				if l_d = left_delimiter then
					repStr = compile_(repStr , "\[" , "\]" ,"$", 1, null , dict_label )
				else
					repStr = compile_(repStr , "\[" ,"\]" ,"$", loopCounter+1, null , dict_label )
				end if			
			end if
		end if
		compile__=repStr
		Call L_( classType & ".compile__" )
	End Function
  
	Public Sub init ( byRef l_d,byRef r_d)		
		left_delimiter	= l_d
		right_delimiter = r_d
	End Sub
	
	Private sub parseRuleFix( rulePrefix,ruleSuffix,l_d,r_d,loopCounter)
		rulePrefix = "<!-{2,}\s*" & POP_MVC.String.Repeat(l_d , loopCounter) & "(?!" & l_d & ")"
		ruleSuffix = POP_MVC.String.Repeat(r_d , loopCounter) & "(?!" & r_d & ")" & "\s*-{2,}>"		
	end sub
	
	Private Function trimContent( byval content )
		if POP_MVC.config("APP_DEBUG") then
			'trimContent = POP_MVC.String.reg_replace(content , "" , "^\s+$" , "gm" )
			trimContent = content
		else
			trimContent = POP_MVC.String.reg_replace(content , "" , "^\s+|\s+$" , "gm" )
		end if		
	End Function
	
	'如果date1>date2返回正，否则返回负数
	Private Function dateCompare( date1,date2 )
		dateCompare = DateDiff("s",date2,date1)
	End Function	
	
	'判断文件或目录是否存在
	Private Function file_exists( fileName )
		file_exists = POP_MVC.File.isExists(fileName)
	End Function	
	
	Private Function filemtime(filename)
		filemtime = POP_MVC.File.mtime(filename)
	End Function
	
	Private Sub Class_Terminate
	End Sub  
	
	Private Sub Class_Initialize
		' The left delimiter used for the template tags.
		left_delimiter  =  "{"

		' The right delimiter used for the template tags.
		right_delimiter =  "}"
		
		zuo_delimiter = "<!--"
		you_delimiter = "-->"
		
		
		'是否启用模板布局
		layout_on = false
		'模板布局文件
		layout_file		= "layout"
		
		layout_label = "{__CONTENT__}"
		
		nolayout_label = "{__NOLAYOUT__}"
		
		digui_count = 0
		
		If Not IsEmpty( POP_MVC.config("TMPL_LAYOUT_ON") ) 		Then layout_on = POP_MVC.config("TMPL_LAYOUT_ON")
		If Not IsEmpty( POP_MVC.config("TMPL_LAYOUT_FILE") ) 	Then layout_file = POP_MVC.config("TMPL_LAYOUT_FILE")
		If Not IsEmpty( POP_MVC.config("TMPL_LAYOUT_LABEL") ) 	Then layout_label	= POP_MVC.config("TMPL_LAYOUT_LABEL")
		If Not IsEmpty( POP_MVC.config("TMPL_NOLAYOUT_LABEL") ) Then nolayout_label	= POP_MVC.config("TMPL_NOLAYOUT_LABEL")	
		
		set tplDict = CreateDict
		
		set cacheDict = CreateDict
		
		set recurDict = CreateDict
		
		cache_lifetime	= 86400
		If Not IsEmpty( POP_MVC.config("TMPL_CACHE_LIFETIME") ) Then cache_lifetime	= POP_MVC.config("TMPL_CACHE_LIFETIME")	
		
		ruleVar = "[\w\(\)\.\""\u0391-\uFFE5]+"
		
		if isEmpty( POP_MVC.config("TMPL_PARSE_STRING") ) then
			set POP_MVC.config("TMPL_PARSE_STRING") = CreateDict
		end if
		
		if NOT POP_MVC.config("TMPL_PARSE_STRING").Exists("__ROOT__") then	POP_MVC.config("TMPL_PARSE_STRING")("__ROOT__") = ROOT__
		if NOT POP_MVC.config("TMPL_PARSE_STRING").Exists("__APP__") then	POP_MVC.config("TMPL_PARSE_STRING")("__APP__") = APP__
		if NOT POP_MVC.config("TMPL_PARSE_STRING").Exists("__URL__") then	POP_MVC.config("TMPL_PARSE_STRING")("__URL__") = URL__
		if NOT POP_MVC.config("TMPL_PARSE_STRING").Exists("__ACTION__") then	POP_MVC.config("TMPL_PARSE_STRING")("__ACTION__") = URL__ & POP_MVC.a
		if NOT POP_MVC.config("TMPL_PARSE_STRING").Exists("__SELF__") then	POP_MVC.config("TMPL_PARSE_STRING")("__SELF__") = SELF__
	 	
		classType = typename(Me)
	End Sub
	
	Private Function CreateDict
		set CreateDict = Server.CreateObject("Scripting.Dictionary")
	End Function
End Class
%>