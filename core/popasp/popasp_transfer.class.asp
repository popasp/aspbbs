<%
' +----------------------------------------------------------------------
' | POPASP [ ASP MVC ]
' +----------------------------------------------------------------------
' | Copyright (c) 2016 http://popasp.com All rights reserved.
' +----------------------------------------------------------------------
' | Author: popasp <1737025626@qq.com>
' +----------------------------------------------------------------------
' 自动完成代码类
Class POPASP_TRANSFER
	'获取客户端的ip地址
	Function getClientIp
		getClientIp = get_client_ip 
	End Function

	'获取popasp的版本号
	Function version
		version = POP_MVC.version
	End Function
	
	Function isAjax
		isAjax = POP_MVC.isAjax
	End Function
	
	Function md5_( str )
		md5_= md5(str)
	End Function
	
End Class
%>