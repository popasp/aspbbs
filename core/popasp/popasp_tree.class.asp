<%
' +----------------------------------------------------------------------
' | POPASP [ ASP MVC ]
' +----------------------------------------------------------------------
' | Copyright (c) 2016 http://popasp.com All rights reserved.
' +----------------------------------------------------------------------
' | Author: popasp <1737025626@qq.com>
' +----------------------------------------------------------------------
Class POPASP_TREE
	public isLockNav
	public dictLockNav
	private levelName,prikey2key
	
	private sPrikeyField,sParentField
	
	Private Sub Class_Initialize
		isLockNav = false	
		levelName = "popasp_level"
		prikey2key = false
	End Sub
	
	Function Tree( dict, id , prikey, parent)
		dim ret , level
		set ret = Server.CreateObject("Scripting.Dictionary")
		level = 0

		Call Tree_( ret , dict, id , level , prikey , parent)
		set Tree = ret
	End Function
	
	private sub Tree_(byref ret,byref dict,byref id, byval level,byref prikey, byref parent )
		dim key
		for each key in dict
			if CStr(dict(key)(parent)) = CStr(id) then
				dict(key)(levelName) = level
				if prikey2key then
					ret.Add key,dict(key)
				else
					ret.Add ret.count,dict(key)
				end if				
				call Tree_( ret,dict,dict(key)(prikey), level+1, prikey, parent )
			end if
		next
	end sub
	
	'通过多次查询获取某个栏目ID的所有父结点，返回数组，每个数组中的元素为Dictionry对象，比如
	'[ { "CMS_ID": 1, "CMS_NavName": "国内新闻", "CMS_Date": "2017/1/25 星期三 22:03:08", "CMS_Sid": 0, "CMS_Sort": 4 }, { "CMS_ID": 24, "CMS_NavName": "山西新闻", "CMS_Date": "2017/2/6 星期一 22:10:28", "CMS_Sid": 1, "CMS_Sort": 24 }, { "CMS_ID": 25, "CMS_NavName": "运城新闻", "CMS_Date": "2017/2/6 星期一 22:10:42", "CMS_Sid": 24, "CMS_Sort": 25 } ]
	Function NavParentNodes( table,  parentField , id )
		dim arr,i,j,item,node,ret
		ret = Array()
		arr = M_(table).db.getArr
		set node = M_(table).db.where(id).getRow
		do while node.count > 0
			set node = M_(table).db.where( node(parentField) ).getRow
			if node.count > 0 then
				POP_MVC.Arr.unshift ret , node
			end if
		loop
		NavParentNodes = ret 
	End Function
	
	'获取某个栏目ID的所有父结点的ID，返回数组
	Function NavParentIDs( table, parentField , selectId )
		dim arr,pid,ret,limit,i,prikeyField
		prikeyField = M_(table).db.getPK()
		limit = 10
		ret = Array()
		pid = M_(table).db.field(parentField).where(selectId).getOne		
		do while pid > 0
			if i = limit then
				exit do
			end if
			POP_MVC.Arr.unshift ret , CStr(pid)
			pid = M_(table).db.field(parentField).where( prikeyField & " = " & pid ).getOne
			pid = clng(pid)
			i = i +1
		loop
		NavParentIDs = ret 
	End Function
	
	'获取某个栏目ID的所有父结点的ID，包含自身，返回数组
	Function NavUpIds( table,  parentField , sortField , selectId )
		on error resume next
		dim prikeyField,ret,key,dict,bool,ret2,level
		prikeyField = M_(table).db.getPK()
				
		if isLockNav then
			if isEmpty( dictLockNav ) then
				set dict = M_(table).db.field( array( prikeyField,parentField,sortField ) ).order( sortField & " ASC" ).getAll
				set ret = Tree( dict, 0 , prikeyField, parentField )
				set dictLockNav = ret
			else
				set ret = dictLockNav
			end if
		else
			set dict = M_(table).db.field( array( prikeyField,parentField,sortField ) ).order( sortField & " ASC" ).getAll			
			set ret = Tree( dict, 0 , prikeyField, parentField  )
		end if
		ret2 = Array()
		bool = false
		selectID = CLng( selectID )
		POP_MVC.Arr.push ret2,selectID

		for key = ret.count-1 to 0 step -1			
			if ret(key)(prikeyField) = selectID then
				if ret(key)(parentField) = 0 then
					exit for
				end if
				level = ret(key)(levelName)	
				bool = true					
			else			
				if bool then					
					if level < 0 then
						exit for
					end if
					if ret(key)(levelName) = level - 1 then
						level = level - 1
						POP_MVC.Arr.unshift ret2,ret(key)(prikeyField)
					end if
				end if			
			end if			
		next
		NavUpIds = ret2
	End Function
	
	'获取某个栏目ID的所有的子节点的ID,包含自身id
	'返回数组
	Function NavSonIds( table,  parentField , sortField , selectId )
		on error resume next
		dim prikeyField,ret,key,dict,bool,ret2,level
		prikeyField = M_(table).db.getPK()
		if isLockNav then
			if isEmpty( dictLockNav ) then
				set dict = M_(table).db.field( array( prikeyField,parentField,sortField ) ).order( sortField & " ASC" ).getAll
				
				set ret = Tree( dict, 0 , prikeyField, parentField )
				set dictLockNav = ret
			else
				set ret = dictLockNav
			end if
			
		else
			set dict = M_(table).db.field( array( prikeyField,parentField,sortField ) ).order( sortField & " ASC" ).getAll	
			
			set ret = Tree( dict, 0 , prikeyField, parentField )
		end if
		
		ret2 = Array()
		bool = false
		selectID = CLng( selectID )
		POP_MVC.Arr.push ret2,selectID
		for each key in ret
			if ret(key)(prikeyField) = selectID then
				level = ret(key)(levelName)	
				bool = true						
			else			
				if bool then	
					if ret(key)(levelName) = level then
						exit for
					end if
					POP_MVC.Arr.push ret2,ret(key)(prikeyField)
				end if			
			end if			
		next
		NavSonIds = ret2
	End Function
	
	'使用<select name=parentField><option></option>...</select>显示无限级分类栏目
	Function NavSelect( table, parentField , navField , sortField , selectId,selectName)
		dim prikeyField,html,ret,key,dict
		
		prikeyField = M_(table).db.getPK()
		
		set dict = B_(table).field( array( prikeyField,parentField,navField,sortField ) ).order( sortField & " ASC" ).getAll

		set ret = Tree( dict, 0 , prikeyField, parentField  )
		if selectName = "" then
			html = "<select name='" & parentField & "' class='popasp_select'>" & VbCrLf
		else
			html = "<select name='" & selectName & "' class='popasp_select'>" & VbCrLf
		end if
		
		html = html & "<option value='0'>--顶级菜单--</option>" & VbCrLf
		for each key in ret
			set item = ret(key)
			html = html & "<option value='" & item(prikeyField) & "' " & iif( CStr(item(prikeyField)) = CStr(selectId) , "selected" , "" ) & ">" & POP_MVC.String.repeat( "　　" , item(levelName) ) & iif( item(levelName) > 0 , "|-- " , "" ) & item(navField) & "</option>" & VbCrLf
		next
		html = html & "</select>"
		NavSelect = html
	End Function
	
	'使用<select name=parentField><option></option>...</select>显示无限级分类栏目
	Function NavOption( table, parentField , navField , sortField , selectId)
		dim prikeyField,html,ret,key,dict
		
		prikeyField = M_(table).db.getPK()
		
		set dict = B_(table).field( array( prikeyField,parentField,navField,sortField ) ).order( sortField & " ASC" ).getAll

		set ret = Tree( dict, 0 , prikeyField, parentField  )
		
		html = ""
		for each key in ret
			set item = ret(key)
			html = html & "<option value='" & item(prikeyField) & "' " & iif( CStr(item(prikeyField)) = CStr(selectId) , "selected" , "" ) & ">" & POP_MVC.String.repeat( "　　" , item(levelName) ) & iif( item(levelName) > 0 , "|-- " , "" ) & item(navField) & "</option>" & VbCrLf
		next
		NavOption = html
	End Function	
	
	'使用<table class='popasp_table'></table>显示无限级分类栏目
	Function NavList( table, parentField , navField, sortField )
		dim prikeyField,html,ret,key,item,dict,prev,nxt,j,sortAction,delAction,add_asp,save_asp,parentID
		sortAction = "do_sort"
		delAction = "do_remove"
		if not isEmpty(POP_MVC.config( "URL_MODE" )) and  POP_MVC.config( "URL_MODE" ) <> 0 then
			add_asp = URL__ & "add&"
			save_asp =  URL__ & "edit&"
		else
			add_asp = "nav_add.asp?"
			save_asp = "nav_edit.asp?"
		end if

		parentID = 0
		prikeyField = M_(table).db.getPK()
		set dict = M_(table).db.field( array( prikeyField,parentField,navField,sortField ) ).order( sortField & " ASC" ).getAll
		set ret = Tree( dict, 0 , prikeyField, parentField )
		html = "<TABLE cellSpacing=0 class='popasp_table' cellPadding=0 width=""100%"" align=center border=0 id=tab"& ParentID &">" & VbCrLf
		html = html & "<tr><th>栏目ID</th><th>栏目名称</th><th>栏目排序</th><th>操作</th></tr>" & VbCrLf
		for each key in ret
			set item = ret(key)
			
			set prev = Server.CreateObject("Scripting.Dictionary")
			j = key - 1
			do while j >= 0
				set prev = ret( j )
				if prev( levelName ) <= item( levelName ) then
					exit do
				end if
				j = j - 1					
			loop
			
			set nxt = Server.CreateObject("Scripting.Dictionary")
			j = key + 1
			do while j < ret.count
				set nxt = ret( j )
				if nxt( levelName ) <= item( levelName ) then
					exit do
				end if
				j = j + 1					
			loop
			
			if key mod 2 = 1 then
				html = html & "<tr onmouseover=""this.bgColor='#CDE6FF'"" onmouseout=""this.bgColor='#f6fbff'"" bgColor='#f6fbff'>"
			else
				html = html & "<tr onmouseover=""this.bgColor='#CDE6FF'"" onmouseout=""this.bgColor='#FFFFFF'"" bgColor='#FFFFFF'>"
			end if
			
			html = html & "<td style='text-align:center;'>" & item(prikeyField) &  "</td>" & VbCrLf
			html = html & "<td>"
			html = html & POP_MVC.String.repeat( "　　" , item(levelName) ) 
			html = html & iif( item(levelName) > 0 , "|-- " , "" ) & item(navField)
			html = html & iif( ret.count > 1 AND key > 0 AND prev( levelName ) = item( levelName ) , "　　<a href='" & URL__ & sortAction & "&ids=" & item(prikeyField) & "," & prev(prikeyField) & "' style='text-decoration:none' title='排序上移'>&uarr;</a>" , "" )  
			html = html & iif( ret.count > 1 AND key < ret.count -1 AND nxt( levelName ) = item( levelName ) , "　　<a href='" & URL__ & sortAction & "&ids=" & item(prikeyField) & "," & nxt(prikeyField) & "' style='text-decoration:none' title='排序下移'>&darr;</a>" , "" )  & "</td>" & VbCrLf
			html = html & "<td style='text-align:center;'>" & item(sortField) & "</td>" & VbCrLf
			html = html & "<td style='text-align:center;'>" & " <a href='" & add_asp & "parent=" & item(prikeyField) & "'>添加</a> | <a href='" & save_asp & "id="& item(prikeyField) &"'>编辑</a> | <a href='javascript:if(confirm(""你确定要删除吗？"")){location.href=""" & URL__ & delAction & "&id=" & item(prikeyField) & """;}'>删除</a> " & "</td></tr>" & VbCrLf
		next
		html = html & "</table>"
		NavList = html
		set dict = nothing
		set prev = nothing
		set nxt = nothing
	End Function	

End Class
%>