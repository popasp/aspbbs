<%
' +----------------------------------------------------------------------
' | POPASP [ ASP MVC ]
' +----------------------------------------------------------------------
' | Copyright (c) 2016 http://popasp.com All rights reserved.
' +----------------------------------------------------------------------
' | Author: popasp <1737025626@qq.com>
' +----------------------------------------------------------------------

'使用注意事项
'文本文件的编码必须是ANSI
'由于文本数据库的限制，不能进行update与delete操作
Class POPASP_TXT
	'为了减轻数据库操作类的重量，特将相同代码写到了另外一个文件
	'POPASP_REPLACE_CLASS_CODE_POPASP'
	
	' 获取表的所有字段与字段类型
	Function getTableFields( ByVal table_name )
		table_name = getTrueTableName(table_name)
		set getTableFields = tool.getTableFields("SELECT TOP 1 * FROM [" & table_name & "]" )
	End Function
	
	Function InsertByTable( table,data,pk )
		on error resume next
		dim rs,sql,key,start,dict
		if not is_empty( POP_MVC.config("SHOW_PAGE_TRACE") ) Then
			start = timer()	
		end if
		
		'剔除不存在的字段
		call handlerData( table,data , 0 )
		call set_increment_id( table, data )
		set rs = tool.CreateRS() 
		sql = "SELECT * FROM " & parsedOptions("table")
		rs.open sql,tool.conn,1,2
		Application.lock
		rs.AddNew
		for each key in data
			rs(key)= data(key)			
		next
		
		rs.Update '调用Update方法立即将内存中数据写入数据库中,下面这句是关键的
		Application.unlock
		
		if err.number = 0 then
			if pk<>"" then
				If data.Exists( pk ) Then
					InsertByTable = data( pk )
				End If
			else
				InsertByTable = True
			end if

			if not is_empty( POP_MVC.config("SHOW_PAGE_TRACE") ) Then
				set dict = Server.CreateObject("Scripting.Dictionary")
				dict("time") = round((timer() - start) * 1000,0)
				dict("sql") = "使用 Recordset.Update 插入数据(没有使用SQL)"
				set POP_MVC.dSql(POP_MVC.dSql.count + 1) = dict
				set dict = nothing
				POP_MVC.num_db_write = POP_MVC.num_db_write + 1
			end if	
		end if
		
		Call L_( Me.db_type & " InsertByTable" )
	End Function
	
	' 向数据表中插入数据，data为Dictionary对象，其键名与字段名相对应，如果data含主键，须手动删除
	Public Function Insert(ByVal table,ByVal data)		
		table = getTableFromInput(table)		
		Insert = InsertByTable( table , data , Me.getPrikey( table )  )
	End Function
	
	' 向数据表中添加数据
	Public Property Get Add()
		on error resume next
		call parseOptions	'解析参数		
		if is_empty(parsedOptions("table")) then 
			Call setPoptsTable( parsedOptions )	
		end if
		
		if isEmpty( parsedOptions("data") ) Then
			set parsedOptions("data") = POP_MVC.Form2Dict()
		end If
		
		if ( not is_empty( parsedOptions("data") ) ) Then
			Add = InsertByTable( parsedOptions("table") ,parsedOptions("data") , Me.getPK  )
		else 
			POP_MVC.error( "POPASP_TXT.Add" )
		end If		
		call ResumeOpts
		call L_( Me.db_type & " Add" )
	End Property
	

	' 得到数据库中的所有表名，返回一个对象
	Public Function getTables()
		on error resume next
		dim shm,arr,content
		dim filename	
		' 如果已经保存了数据，直接返回
		if not isEmpty(dTables) Then
			set getTables = dTables
			Exit Function
		End If
		'如果对应文件不存在，则须先将数据生成json保存到文件中
		filename = tool.getDataFileName("_tables_")
		if Not is_empty( POP_MVC.config("APP_DEBUG") ) OR Not tool.file_exists( filename ) Then
			
			call tool.initConn		
			Set shm = tool.conn.OpenSchema(20)
			lastSql = "Access OpenSchema"
			POP_MVC.num_db_query = POP_MVC.num_db_query + 1
			shm.MoveFirst 
			Do While Not shm.EOF 
				If shm("TABLE_TYPE") = "TABLE" Then	
					POP_MVC.Arr.Push arr, shm("TABLE_NAME")
				End If
				shm.MoveNext 
			Loop
			tool.closeRS shm
			call tool.file_put_contents(filename, join(arr,",") )	
		else	'从文件中取出数据
			content = POP_MVC.file_get_contents(tool.getFilePath(filename))
			content = mid(content,2)
			arr = split(content,",")
		End If
		set dTables = POP_MVC.Arr.toDict(arr)
		set getTables = dTables		
		Call L_("POPASP_ACCESS.getTables")
	End Function	
	
	'获取数据表结构
	Public Function getTableStructure( ByVal tableName )
		call getTables
		tableName = getTrueTableName(tableName)
		
		if dTables.count = 0 then
			Call getTables()
		end if
		
		set getTableStructure = tool.getTableStructure( tableName,dTables,dTS,"SELECT TOP 1 * FROM [" & tableName & "]")		
	End Function
	
	Function set_increment_id( ByVal tableName , ByRef data )
		dim rs,sql
		call getPK()
		if not is_empty(prikey) Then
			sql = "SELECT max(" & prikey & ") as popasp_result FROM [" & tableName & "]" & " WHERE NOT ISNULL([" & prikey & "])"
			set rs = getRS(sql)			
			if Not rs.eof then
				data(prikey) = CInt( rs( "popasp_result" ) ) + 1
			else
				data(prikey) = 1
			end if
			set dict = nothing
			tool.closeRS rs
		End if
	End Function	
	
End Class
%>