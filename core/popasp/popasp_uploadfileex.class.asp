<%
'=========================================================
 '类名: AnUpLoad(艾恩无组件上传类)
 '作者: Anlige
 '版本: 艾恩ASP无组件上传类V11.03.25
 '开发日期: 2008-4-12
 '修改日期: 2011-03025
 '主页: http://dev.mo.cn
 'Email: zhanghuiguoanlige@126.com
 'QQ: 1034555083
'=========================================================
Class POPASP_UPLOADFILEEX
	Private mvarFormName , mvarNewName , mvarLocalName , mvarFileName , mvarUserSetName , mvarContentType ,mException,mvarPosition
	Private mvarSize , mvarValue , mvarPath , mvarExtend ,mvarWidth, mvarHeight
	Public StreamT
	
	Public Property Let Extend(ByVal vData )
		mvarExtend = vData
	End Property
	Public Property Get Extend() 
		Extend = mvarExtend
	End Property

	Public Property Get Width() 
		Width = mvarWidth
	End Property
	
	Public Property Get Height() 
		Height = mvarHeight
	End Property
	
		
	Public Property Let Path(ByVal vData )
		mvarPath = vData
	End Property
	Public Property Get Path() 
		Path = mvarPath
	End Property
	
	Public Property Get Exception() 
		Exception = mException
	End Property
	
	Public Property Let Value(ByVal vData )
		mvarValue = vData
	End Property
	
	Public Property Get Value() 
		Value = mvarValue
	End Property
	
	Public Property Let Size(ByVal vData )
		mvarSize = vData
	End Property
	Public Property Get Size() 
		Size = mvarSize
	End Property

	Public Property Let Position(ByVal vData )
		mvarPosition = vData
	End Property
	Public Property Get Position() 
		Size = mvarPosition
	End Property
		
	Public Property Let ContentType(ByVal vData )
		mvarContentType = vData
	End Property
	Public Property Get ContentType() 
		ContentType = mvarContentType
	End Property
	
	Public Property Let UserSetName(ByVal vData )
		mvarUserSetName = vData
	End Property
	Public Property Get UserSetName() 
		UserSetName = mvarUserSetName
	End Property
	
	Public Property Let FileName(ByVal vData )
		mvarFileName = vData
	End Property
	Public Property Get FileName() 
		FileName = mvarFileName
	End Property
	
	Public Property Let LocalName(ByVal vData )
		mvarLocalName = vData
	End Property
	Public Property Get LocalName() 
		LocalName = mvarLocalName
	End Property
	
	Public Property Let NewName(ByVal vData )
		mvarNewName = vData
	End Property
	Public Property Get NewName() 
		NewName = mvarNewName
	End Property
	
	Public Property Let FormName(ByVal vData )
		mvarFormName = vData
	End Property
	Public Property Get FormName() 
		FormName = mvarFormName
	End Property
	
	Private Sub Class_Initialize()
		mvarSize =0
		mvarWidth = 0
		mvarHeight = 0
	End Sub
	
	Public Function SaveToFile(ByVal Path , byval tOption, byval OverWrite)
		On Error Resume Next
		Dim IsP 
		IsP = (InStr(Path, ":") = 2)
		If Not IsP Then Path = Server.MapPath(Path)
		Path = Replace(Path, "/", "\")
		If Mid(Path, Len(Path) - 1) <> "\" Then Path = Path + "\"
		CreateFolder Path
		mvarPath = Path
		If tOption = 1 Then
			Path = Path & mvarLocalName: mvarFileName = mvarLocalName
		Else
			If tOption = -1 And mvarUserSetName <> "" Then
				Path = Path & mvarUserSetName & "." & mvarExtend: mvarFileName = mvarUserSetName & "." & mvarExtend
			Else
				Path = Path & mvarNewName: mvarFileName = mvarNewName
			End If
		End If
		If Not OverWrite Then
			Path = GetFilePath()
		End If
		Dim tmpStrm
		Set tmpStrm =server.CreateObject("ADODB.Stream")
		tmpStrm.Mode = 3
		tmpStrm.Type = 1
		tmpStrm.Open
		StreamT.Position = mvarPosition
		StreamT.copyto tmpStrm,mvarSize
		tmpStrm.SaveToFile Path, 2
		tmpStrm.Close
		Set tmpStrm = Nothing
		SaveToFile 
		If Not Err Then
			SaveToFile = Path
		Else
			SaveToFile = false
		End If
	End Function
	
	Public Function GetBytes()
		StreamT.Position = mvarPosition
		GetBytes = StreamT.read(mvarSize)
	End Function
	Private Function CreateFolder(ByVal folderPath )
		Dim oFSO
		Set oFSO = POP_MVC.fso
		Dim sParent 
		sParent = oFSO.GetParentFolderName(folderPath)
		If sParent = "" Then Exit Function
		If Not oFSO.FolderExists(sParent) Then CreateFolder (sParent)
		If Not oFSO.FolderExists(folderPath) Then oFSO.CreateFolder (folderPath)
		Set oFSO = Nothing
	End Function
	
	Private Function GetFilePath() 
		Dim oFSO, Fname , FNameL , i 
		i = 0
		Set oFSO = POP_MVC.fso
		Fname = mvarPath & mvarFileName
		FNameL = Mid(mvarFileName, 1, InStr(mvarFileName, ".") - 1)
		Do While oFSO.FileExists(Fname)
			Fname = mvarPath & FNameL & "(" & i & ")." & mvarExtend
			mvarFileName = FNameL & "(" & i & ")." & mvarExtend
			i = i + 1
		Loop
		Set oFSO = Nothing
		GetFilePath = Fname
	End Function
End Class
%>