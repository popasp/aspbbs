<%
' +----------------------------------------------------------------------
' | POPASP [ ASP MVC ]
' +----------------------------------------------------------------------
' | Copyright (c) 2016 http://popasp.com All rights reserved.
' +----------------------------------------------------------------------
' | Author: popasp <1737025626@qq.com>
' +----------------------------------------------------------------------
Class POPASP_URL
	private ctrlName
	private actionName
	
	Public Sub setConfig( attr,value )
		if isNumeric(value) then
			Execute attr & " = " & value
		else
			Execute attr & " = """ & value & """"
		end if
	end sub

	'获取控制器名称
	Function get_ctrl_name ( )
		on error resume next
		dim c		
		if isEmpty( ctrlName ) then		
			c = POP_MVC.Get( POP_MVC.config("VAR_MODULE") )	
			if isEmpty(c) Then
			  c = POP_MVC.config("DEFAULT_MODULE")
			else
			  c = POP_MVC.String.UCFirst(LCase(c))
			end if
			get_ctrl_name = c
			ctrlName = c
		else
			get_ctrl_name = ctrlName			
		end if	
	End Function
	
	'获取操作方法名称
	Function get_action_name ( )		
		dim a	
		if isEmpty( actionName ) then			
			a = POP_MVC.get( POP_MVC.config("VAR_ACTION")  )
			if isEmpty(a) Then
			  a = POP_MVC.config("DEFAULT_ACTION")
			else
			  a = POP_MVC.String.UCFirst(LCase(a))
			end if					
			get_action_name = a
			actionName = a
		else
			get_action_name = actionName
		end if
		
	End Function	
	
	Sub set_tmpl_config	
		on error resume next
		dim script_name,self_
		dim c,a , str

		c = get_ctrl_name()
		a = get_action_name()
		str = Request.ServerVariables("QUERY_STRING")
		str = Replace(str , chr(34) , chr(34) & chr(34) )
		'设置模板替换变量
		script_name = Request.ServerVariables("SCRIPT_NAME")
		Execute("const ROOT__ = """ & POP_MVC.root_path & """")
		Execute("const APP__ = """ & script_name & """")		
		Execute("const QUERY__ = str")

		self_= APP__
		if Request.ServerVariables("QUERY_STRING") <> "" then
			self_ = self_ & "?" &  Request.ServerVariables("QUERY_STRING")
		end if
		str = Replace(self_ , chr(34) , chr(34) & chr(34) )
		Execute("const SELF__ = """ & str & """")	
	end sub
	
End Class
%>