<%
' +----------------------------------------------------------------------
' | POPASP [ ASP MVC ]
' +----------------------------------------------------------------------
' | Copyright (c) 2016 http://popasp.com All rights reserved.
' +----------------------------------------------------------------------
' | Author: popasp <1737025626@qq.com>
' +----------------------------------------------------------------------

'处理 版本 的类
Class POPASP_VERSION
	'检测是否为升级文件
	'升级文件名特点 CMS名称_update_新版号_旧版号
	'如 aspblog_update_1.2_1.1.data
	Function isUpdateFile( upfile )
		dim baseName,arr
		baseName = POP_MVC.file.baseName( upfile )
		arr = split( baseName, "_" )
		isUpdateFile = false
		if ubound(arr) <> 3  then
			exit Function
		end if
		
		if LCase(arr(0)) <> LCase( C_("CMS_NAME") ) then
			exit Function
		end if
		
		if LCase(arr(1)) <> "update" then
			exit Function
		end if
		
		if not POP_MVC.String.reg_test( arr(2) , "\d+(\.\d+){1,3}" , "" ) then
			exit Function
		end if
		
		if not POP_MVC.String.reg_test( arr(3) , "\d+(\.\d+){1,3}" , "" ) then
			exit Function
		end if
		isUpdateFile = true
	End Function
	
	'根据升级文件，判断当前版本是否需要升级
	'在这个升级 aspblog_update_1.2_1.1.data 中 [1.1,1.2) 范围的版本均可使用，否则不需要
	Function isNeedUpdate( upfile )
		dim highVer,lowVer,arr
		baseName = POP_MVC.file.baseName( upfile )
		arr = split( baseName, "_" )
		highVer = arr(2)
		lowVer = POP_MVC.file.basename( Array(arr(3),true) )
		isNeedUpdate = POP_MVC.String.verBetween(  C_("CMS_VERSION") , lowVer , highVer )
	End Function

	'获取旧版本版本号
	'从网站某个配置文件中获取
	'configPath含版本号的配置文件路径
	'配置文件中应含有 POP_MVC.config("CMS_VERSION") = 1.0 类似这样的配置
	Function getVersion( ByVal configPath)
		if POP_MVC.file.isFile( configPath ) then
			getVersion = POP_MVC.String.reg_find( POP_MVC.file_get_contents( configPath ) , "POP_MVC\.config\(\s*""CMS_VERSION""\s*\)\s*=\s*""([\w.\s]+)""" , 1, "i" )
		end if
	End Function
	
	'文件是否属于过滤文件夹
	'sNoSkipFolder为不可忽略过滤的文件夹，sSkipFolder为忽略过滤的文件夹
	Function BelongSkipFolder( path , sNoSkipFolder , sSkipFolder )
		dim i

		BelongSkipFolder = false
		if not isArray( sNoSkipFolder ) then
			exit function
		end if
		for i = 0 to ubound( sNoSkipFolder )
			if POP_MVC.file.belong( sNoSkipFolder( i ) , path ) then
				exit function
			end if
		next
		for i = 0 to ubound( sSkipFolder )
			if POP_MVC.file.belong( sSkipFolder( i ) , path ) then
				BelongSkipFolder = true
				exit function
			end if
		next
	End Function
	
	'得到打包文件名
	Function getPackName( ByVal lowVer ,ByVal highVer )
		if isNul(highVer) then
			highVer = C_("CMS_VERSION")
		end if
		getPackName =  LCase( C_("CMS_NAME") ) & "_update_" & highVer & "_" & lowVer
	End Function
	
	'将stream流存入文件
	Sub stream2file( stream , path)
		on error resume next
		dim objStream
		Set objStream = Server.CreateObject("ADODB.Stream")
			With objStream
				.Open
				.Type = 1
				 .SetEos()
				.Write stream
				.SaveToFile  POP_MVC.realPath(path), 2
				.Close
			End With
			
		if err.number <> 0 then
			var_export POP_MVC.realPath(path)
			
			response.end
		end if
		objStream.close
		Set objStream = Nothing

	End sub
End Class
%>