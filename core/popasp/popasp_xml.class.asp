<%
' +----------------------------------------------------------------------
' | POPASP [ ASP MVC ]
' +----------------------------------------------------------------------
' | Copyright (c) 2016 http://popasp.com All rights reserved.
' +----------------------------------------------------------------------
' | Author: popasp <1737025626@qq.com>
' +----------------------------------------------------------------------
Class POPASP_XML
	Public xmlDocument,xmlPath,xmlDomObj,xmlstr
	Private xmlDomVer,xmlFileSavePath

	Private Sub Class_Initialize()
		xmlDomVer=getXmlDomVer()
		createXmlDomObj
	End Sub

	Private Sub Class_Terminate()
		If IsObject(xmlDomObj) Then Set xmlDomObj=Nothing
	End Sub

	'获取系统所用XML处理组件名
	Public Function getXmlDomVer()
		dim i,xmldomVersions,xmlDomVersion
		getXmlDomVer=false
		xmldomVersions=Array("Microsoft.2MLDOM","MSXML2.DOMDocument","MSXML2.DOMDocument.3.0","MSXML2.DOMDocument.4.0","MSXML2.DOMDocument.5.0")
		for i=0 to ubound(xmldomVersions)
			xmlDomVersion=xmldomVersions(i)
			if POP_MVC.isInstall(xmlDomVersion) then getXmlDomVer=xmlDomVersion : Exit Function
		next
	End Function
	
	'构造函数
	'使用: P_("xml")(xmlfile)，此时相当于 P_("xml").load(xmlfile)
	Public Default Function Constructor(byref xmlFilePath)
		Call Me.load( xmlFilePath, "xmlfile" )
		set Constructor = xmlDomObj
	End Function

	'创建XmlDom对象
	Private Sub createXmlDomObj
		set xmlDomObj=server.CreateObject(xmlDomVer)
		xmlDomObj.validateonparse=true 
		xmlDomObj.async=false 
	End Sub

	'加载xml文档
	'nodeName 节点名
	'xmlType有3种类型，xmlfile时xml为文档路径，xmldocument时xml为dom字符串，transfer时为url路径
	Public Function load(Byval xml,Byval xmlType)
		dim xmlUrl,xmlfilePath
		select case xmlType 
			case "xmlfile"
				xmlfilePath=POP_MVC.realPath(xml)   
		 		xmlDomObj.load(xmlfilePath) 
				xmlFileSavePath = xmlfilePath
			case "xmldocument"
				xmlUrl=xml
				xmlstr=getRemoteContent(xmlUrl,"text")
				If left(xmlstr, 5) <> "<?xml" then POP_MVC.exit(err.description) else xmlDomObj.loadXML(xmlstr)
			case "transfer"
				xmlUrl=xml
				xmlstr= P_("http").get(xmlUrl) 'bytesToStr(getRemoteContent(xmlUrl,"body"),"utf-8")
				If left(xmlstr, 5) <> "<?xml" then POP_MVC.exit(err.description) else xmlDomObj.loadXML(xmlstr)
		end select
		set load = Me
	End Function

	'判断是否存在某个节点
	'nodeName 节点名	
	Public Function isExistNode(nodename)
        dim node
        isExistNode=True
        set node=xmlDomObj.getElementsByTagName(nodename)
        If node.Length=0 Then isExistNode=False : set node=nothing
    End Function

	'获取节点值
	'nodeName 节点名
	'itemId  节点索引	
	Public Function getNodeValue(nodename, itemId)
		if isNul(itemId) then  itemId=0
		getNodeValue=xmlDomObj.getElementsByTagName(nodename).Item(itemId).Text
	End Function

	'获取节点长度
	'nodeName 节点名	
	Public Function getNodeLen(nodename)
		getNodeLen=xmlDomObj.getElementsByTagName(nodename).Length
	End Function
	
	'获取一组节点
	'IXMLDOMSelection
	Public Function getNodes(nodename)
		Set getNodes=xmlDomObj.getElementsByTagName(nodename)
	End Function
	
	
	'获取一组节点值的拼接字符串
	Public Function getNodesValue(nodename)
		dim nodes,node		
		if typename( nodename ) = "IXMLDOMSelection" then
			Set nodes=nodename
		else
			Set nodes=xmlDomObj.getElementsByTagName(nodename)
		end if
		for each node in nodes
			getNodesValue = getNodesValue & node.text
		next
	End Function
	
	'获取节点对象
	'nodeName 节点名
	'itemId  节点索引	
	Public Function getNode(nodename, itemId)
		Set getNode=xmlDomObj.getElementsByTagName(nodename).Item(itemId)
	End Function

	'获取节点的某个属性值
	'nodeName 节点名
	'attrName 属性名
	'itemId  节点索引
	Public Function getAttributes(nodeName, attrName, itemId)
	dim xmlAttributes, i
		if isNul(itemId) then  itemId=0
		err.clear:on error resume next
		getAttributes=xmlDomObj.getElementsByTagName(nodeName).Item(itemId).getAttributeNode(attrName).nodevalue
		if err then getAttributes="":err.clear
	End Function

	'获取节点的某个属性值
	'node 节点对象
	'attrName 属性名
	Public Function getAttributesByNode(node, attrName)
		err.clear:on error resume next
		getAttributesByNode=node.getAttributeNode(attrName).nodevalue
		if err then getAttributesByNode="":err.clear
	End Function

	'设置节点值
	'nodename 节点名
	'itemId  节点索引
	'str 节点文本值
	'savePath 保存路径
	Public Function setXmlNodeValue(Byval nodename, Byval itemId, Byval str,Byval savePath)
	dim node
		xmlFileSavePath=savePath
		Set node=xmlDomObj.getElementsByTagName(nodename).Item(itemId)		
		node.innerXML=str
		xmlDomObj.save POP_MVC.realPath(xmlFileSavePath)
		set node=nothing
	End Function
	
	Public Function setXmlNodeCDATA( ByRef node,ByRef cdataValue )
		dim cdata
        set cdata = xmlDomObj.CreateCDataSection(cdataValue)
		node.text = ""
		node.AppendChild(cdata)
	End Function
	
	'加载xml文件后，根据路径，将dictionary对象的值替换到xml文件中
	'path为节点路径，该路径中的节点对应于Dict中的键名，键名为节点名，值为节点值
	Public Function setXmlNodesValue(Byval path, ByRef Dict,Byval savePath)
		on error resume next
		dim nodes,i	, textNode , cdataNode , newNode , arr , key , pNode
		
		set  nodes = Me.getNodes(path)
		
		if nodes.Length < 1 then
			if not isNul( savePath ) then
				POP_MVC.warning( savePath & "的路径" & path & "未找到一个节点" )
			else
				POP_MVC.warning( xmlFileSavePath & "的路径" & path & "未找到一个节点" )
			end if			
		end if
		arr = Array()
		for i = 0 to nodes.Length - 1	
			if isEmpty( pNode ) then
				set pNode = nodes.item(i).parentNode
			end if
			POP_MVC.Arr.push arr, nodes.item(i).BaseName
			if Dict.Exists( nodes.item(i).BaseName ) then
				nodes.item(i).text=""
				set cdataNode = xmlDomObj.createCDATASection( Dict( nodes.item(i).BaseName ) )
				nodes.item(i).AppendChild(cdataNode)
				set cdataNode = nothing
			end if
		next
		

		if not isEmpty(pNode) then
			for each key in Dict
				if NOT POP_MVC.Arr.Exists( arr , key  ) then
					set newNode = xmlDomObj.createElement( key )
					set cdataNode = xmlDomObj.createCDATASection( Dict( key ) )
					newNode.AppendChild(cdataNode)
					pNode.AppendChild(newNode)					
					set cdataNode = nothing
					set newNode = nothing
				end if
			next
		end if
		
		if isNul( savePath ) then
			xmlDomObj.save POP_MVC.realPath(xmlFileSavePath)
		elseif POP_MVC.file.isFile( savePath ) then
			xmlDomObj.save POP_MVC.realPath(savePath)
		end if
		set nodes = nothing
		Call L_("POPASP_XML.setXmlNodesValue")
	End Function
	
	'将dom保存到savePath路径，如果savePath为空，则保存到load时的文件路径
	Public Property Get Save( savePath )
		if isNul(savePath) then
			savePath = xmlFileSavePath
		end if
		xmlDomObj.save POP_MVC.realPath(savePath)
	End Property
End Class
%>