<%
' +----------------------------------------------------------------------
' | POPASP [ ASP MVC ]
' +----------------------------------------------------------------------
' | Copyright (c) 2016 http://popasp.com All rights reserved.
' +----------------------------------------------------------------------
' | Author: popasp <1737025626@qq.com>
' +----------------------------------------------------------------------

'处理RECORDSET的类
Class POPASP_XMLDB
	public db_path,db_type,xmlobj,curPage,pagesize,curNodes
	public auto_,validate_
	Private options 		'存储分配过来的参数，Dictionary对象
	Private parsedOptions,isSave	'存储解析后的参数，Dictionary对象
	
	Public  patchValidate	'是否批处理验证
	public  [error]			'最近错误消息
	
	Private Sub Class_Initialize()
		db_path = POP_MVC.config("XML_PATH")
		db_type = POP_MVC.config("XML_TYPE")
		pagesize = 10
		patchValidate = False
		isSave = true
		Call ResumeOpts
	End Sub
	
	Private Sub Class_Terminate()
		If IsObject(xmlobj) Then Set xmlobj=Nothing
	End Sub
	
	'指定数据库
	Public Property Get db( ByVal arg )
		dim s_path,s_type
		if not isArray( arg ) then
			arg = split(arg,":")
		end if
		
		if ubound( arg ) > 0 then
			s_path = arg(1)
			s_type = arg(0)
		elseif ubound( arg ) = 0 then
			s_path = arg(0)
			s_type = "xmlfile"
		end if
		if isNul( s_path ) then s_path = POP_MVC.config("XML_PATH")
		if isNul( s_type ) then s_type = POP_MVC.config("XML_TYPE")
		db_path = s_path
		db_type = s_type
		set db = me
	End Property
	
	'初始化
	Public Property Get Init
		set  xmlobj = P_("xml")	
		
		if db_type = "" then
			db_type = "xmlfile"
		end if
		
		if db_type = "xmlfile" then
			if not POP_MVC.file.isFile( db_path ) then
				POP_MVC.error( POP_MVC.File.baseName( db_path ) & "文件不存在" )
			end if
		end if
		Call xmlobj.load( db_path ,db_type )		
		set Init = Me
	End Property
	
	Public Property Get SaveXml
		if isSave then
			xmlobj.xmlDomObj.save POP_MVC.realPath(db_path)
		end if
	End Property
	
	Public Property Get OpenSave
		isSave = true
	End Property
	
	Public Property Get CloseSave
		isSave = false
	End Property
	
	' mode为1时新增，2时修改，3时新增/修改 
	Public Property Get Create(  mode )
		On Error Resume Next
		dim key,data
		call parseOptions	'解析参数
		Create = false
		
		if isEmpty( parsedOptions("data") ) Then
			set parsedOptions("data") = POP_MVC.Form2Dict()
		end If
		
		set data = parsedOptions("data")	
		
		if not AutoValidate( data , mode ) Then
			Create = False
			exit Property
		End If

		call AutoComplete( data , mode )
		
		set parsedOptions("data") = data
		Create = true
		call L_( typename(Me) & " Create" )
	End Property
	
	'获取parsedOptions("data")
	'如果使用了db.create，则可以使用getData获取最终过滤或添加后的数据，该数据为一维Dictionary对象
	Public Function getData()
		if isObject( parsedOptions("data") ) then
			set getData = parsedOptions("data")
		else
			getData = parsedOptions("data")
		end if
	end Function
	
	'自动完成
	Public Sub AutoComplete( ByRef data, ByRef mode )
		on error resume next
		dim s_auto
		
		if Not isEmpty( parsedOptions("auto") ) Then
			s_auto = parsedOptions("auto")
		ElseIf Not isEmpty( auto_ ) Then
			s_auto = auto_
		End If

		P_( Array("POPASP_AUTOCOMPLETE","xml") ).modelType = "xml"
		call P_( Array("POPASP_AUTOCOMPLETE","xml") ).handle(s_auto,data,mode)
		call L_( typename(Me) & ".AutoComplete" )
	End Sub
	
	'自动验证
	Public Function AutoValidate ( ByVal data,ByRef mode )

		on error resume next
		dim s_valid
		
		if Not isEmpty( parsedOptions("validate") ) Then
			s_valid = parsedOptions("validate")
		ElseIf Not isEmpty( validate_ ) Then
			s_valid = validate_
		End If
		
	
		P_( Array("POPASP_AUTOVALIDATE" , "xml") ).tableName = ""
		P_( Array("POPASP_AUTOVALIDATE" , "xml") ).modelType = "xml"
		P_( Array("POPASP_AUTOVALIDATE" , "xml") ).patchValidate = patchValidate
		AutoValidate = P_( Array("POPASP_AUTOVALIDATE" , "xml") ).handle(s_valid,data,mode)	
		
		
		[error] = P_( Array("POPASP_AUTOVALIDATE" , "xml") ).error
		Call L_( typename(Me) & ".AutoValidate" )
	End Function
	
	'获取rs的字段名，返回数组
	Function getFields( ByRef nodes )
		dim arr,i,obj
		arr = array()
		for i = 0 to nodes.Length - 1
			POP_MVC.arr.push nodes.item(i).BaseName
		next
		getFields = arr
	End Function
	
	'获取数组
	Function getArr()
		dim nodes,arr,i
		Call init
		Call parseOptions
		arr = Array()
		parsedOptions("path") = parsedOptions("path") & "/text()"
		
		'getArr = xmlobj.xmlDomObj.getElementsByTagName( parsedOptions("path") )
		set nodes = xmlobj.xmlDomObj.getElementsByTagName( parsedOptions("path") )

		for i = 0 to nodes.length -1 
			POP_MVC.Arr.push arr,nodes.item(i).nodeValue
		next
		getArr = arr
		Call ResumeOpts 
	End Function
	
	Function getArrStr( sep )
		dim arr
		arr = getArr()
		getArrStr = join( arr, sep )
	End Function
	
	'连贯操作取N条记录，返回二维Dictionary对象
	Function [select]( )
		dim nodes
		Call init
		Call parseOptions
		
		'取节点，类型 IXMLDOMSelection
		'取个数 nodes.length
		set nodes = getNodes( parsedOptions("path") )
		
		if parsedOptions.exists("where") then	
			Call filterNodes( nodes,parsedOptions("where") )
		end if

		if nodes.length<1 then
			set [select] = D_
		else
			Call setNodesPageSize(nodes)
			set [select] = nodes2dict( nodes,parsedOptions("field"),parsedOptions("page")(0),parsedOptions("page")(1) , parsedOptions("order") )
		end if
		set curNodes = nodes

		Call ResumeOpts 
	End Function
	
	'连贯操作，设置位置，从1开始
	Public Property Get setRPosition( pos )
		dim nodes,i,item,bool,total,node,pnode
		Call init
		Call parseOptions
		
		'取节点，类型 IXMLDOMSelection
		'取个数 nodes.length
		set nodes = getNodes( parsedOptions("path") )
		
		if parsedOptions.exists("where") then	
			Call filterNodes( nodes,parsedOptions("where") )
		end if
		
		setRPosition = nodes.length

		if nodes.length > 0 then
			for i = nodes.length-1 to 0 step -1
				set item = nodes.item(i)
				if isEmpty( pnode ) then
					set pnode =item.parentNode
				end if				
				if pos > pnode.childNodes.length then
					pnode.appendChild( item )
				else					
					set node = pnode.childNodes.item(pos-1)					
					Call pnode.insertBefore( item, node)
				end if
			next
		end if
		Call SaveXml
	End Property
	
	
	'连贯操作，设置位置，从1开始
	Public Property Get setPosition( pos )
		dim nodes,i,item,bool,total,node,pnode
		Call init
		Call parseOptions
		
		'取节点，类型 IXMLDOMSelection
		'取个数 nodes.length
		set nodes = getNodes( parsedOptions("path") )
		
		if parsedOptions.exists("where") then	
			Call filterNodes( nodes,parsedOptions("where") )
		end if
		
		setPosition = nodes.length

		if nodes.length > 0 then
			for i = 0 to nodes.length-1
				set item = nodes.item(i)
				if isEmpty( pnode ) then
					set pnode =item.parentNode
				end if				
				if pos > pnode.childNodes.length then
					pnode.appendChild( item )
				else
					
					set node = pnode.childNodes.item(pos-1)					
					Call pnode.insertBefore( item, node)
				end if
			next
		end if
		Call SaveXml
	End Property
	
	Public Property Get setTop()
		setTop = setPosition(1)
	End Property
	
	'连贯操作，设置逆序
	Public Property Get setDesc( TagName )
		dim nodes,i,item,bool,total,node,pnode,j,itemValue,nodeValue,itemNode,curNode,flag
		Call init
		Call parseOptions
		
		'取节点，类型 IXMLDOMSelection
		'取个数 nodes.length
		set nodes = getNodes( parsedOptions("path") )
		
	
		if parsedOptions.exists("where") then	
			Call filterNodes( nodes,parsedOptions("where") )
		end if
		
		setDesc = nodes.length

		if nodes.length > 0 then
			for i = 0 to nodes.length-1
				set item = nodes.item(i)
				if isEmpty( pnode ) then
					set pnode =item.parentNode
				end if
				
				set itemNode = item.selectSingleNode( TagName )
				
				'找不到TagName节点，则移到尾部，退出循环
				if itemNode is nothing then
					pnode.appendChild( item )
					exit for
				end if
				'如果节点值为空，则移到尾部，退出循环
				if itemNode.text = "" then
					pnode.appendChild( item )
					exit for
				end if
				
				if err.number <> 0 then
					POP_MVC.ajax( typename(pnode) )
					response.end
				end if

				'从第1个比较到最后一个，如果当前值
				for j = pnode.childNodes.length - 1 to 0 step -1
					bool = true
					flag = false
					set node = pnode.childNodes.item(j)
					set curNode = node.selectSingleNode(TagName)
					if curNode is nothing then
						bool = false
					elseif curNode.text = "" then
						bool = false
					end if
					if bool then
						'从所有节点curNode中寻找比itemNode的值小的，找到后插入之前
						if POP_MVC.String.cmp( itemNode.text, curNode.text ) <= 0 then
							flag = true
							exit for							
						end if
					end if
				next
						
				'找到后，插入之前
				if flag then
					if j = pnode.childNodes.length - 1 then
						pnode.appendChild( item )
					else
						call pnode.insertBefore( item ,  pnode.childNodes.item(j-1) )
					end if
				end if
			next
		end if
		Call SaveXml
	End Property
	
	'连贯操作，设置正序
	Public Property Get setAsc( TagName )
		dim nodes,i,item,bool,total,node,pnode,j,itemValue,nodeValue,itemNode,curNode,flag
		Call init
		Call parseOptions
		
		'取节点，类型 IXMLDOMSelection
		'取个数 nodes.length
		set nodes = getNodes( parsedOptions("path") )
		
		if parsedOptions.exists("where") then	
			Call filterNodes( nodes,parsedOptions("where") )
		end if
		
		setPosition = nodes.length

		if nodes.length > 0 then
			for i = 0 to nodes.length-1
				if isEmpty( pnode ) then
					set pnode =item.parentNode
				end if
				set item = nodes.item(i)
				set itemNode = item.selectSingleNode( TagName )
				'找不到TagName节点，则移到尾部，退出循环
				if itemNode is nothing then
					pnode.appendChild( item )
					exit for
				end if
				'如果节点值为空，则移到尾部，退出循环
				if itemNode.text = "" then
					pnode.appendChild( item )
					exit for
				end if

				'从第1个比较到最后一个，如果当前值
				for j = 0 to pnode.childNodes.length - 1
					bool = true
					flag = false
					set node = pnode.childNodes.item(j)
					set curNode = node.selectSingleNode(TagName)
					if curNode is nothing then
						bool = false
					elseif curNode.text = "" then
						bool = false
					end if
					if bool then
						'从所有节点curNode中寻找比itemNode的值小的，找到后插入之前
						if POP_MVC.String.cmp( itemNode.text, curNode.text ) >= 0 then
							flag = true
							exit for							
						end if
					end if
				next
				
				'找到后，插入之前
				if flag then
					call pnode.insertBefore( item ,  node )
				end if
			next
		end if
		Call SaveXml
	End Property	

	'使用同select，返回二维Dictionary对象，键名为主键值
	'keyName主键名
	Public Function getPKAll( keyName )
		dim dict,item
		set dict = [select]

		for each key in dict
			set item = dict(key)
			dict.key( key ) = item(keyName)
		next
		set getPKAll = dict
	End Function
	
	'连贯操作取N条记录，返回二维Dictionary对象
	Function search( q,fields )
		on error resume next
		dim nodes,item,i,qNode,qField,totalCount,bool,arr,j
		Call init
		Call parseOptions
		
		'取节点，类型 IXMLDOMSelection
		'取个数 nodes.length
		set nodes = getNodes( parsedOptions("path") )
		
		totalCount = 1
		

		arr = split(fields,",")
		for i = 0 to nodes.length-1
			if totalCount > POP_MVC.config("RS_2DICT_LIMIT") then
				exit for
			end if
			set item = nodes.item(i)
			
			for j = 0 to ubound(arr)
				bool = true
				set qNode = item.selectSingleNode( arr(j) )
				if IsEmpty( qNode ) then
					bool = false
				elseif qNode is nothing then	
					bool = false
				elseif inStr( qNode.text,q )< 1 then	
					bool = false
				end if
				if bool then
					exit for
				end if
			next
			
			if not bool then
				if i = 0 then
					nodes.reset()
					nodes.removenext
					i = i - 1
				else
					set item = nodes.item(i-1)
					nodes.removenext
					i = i -1
				end if
			end if
			
			totalCount = totalCount + 1
		next
		if nodes.length<1 then
			set search = D_
		else
			Call setNodesPageSize(nodes)
			set search = nodes2dict( nodes,parsedOptions("field"),parsedOptions("page")(0),parsedOptions("page")(1) , parsedOptions("order") )
		end if
		set curNodes = nodes
		Call ResumeOpts 
	End Function
	
	'连贯操作取N条记录，返回二维Dictionary对象
	Function soCount( q,fields )
		on error resume next
		dim nodes,item,i,qNode
		Call init
		Call parseOptions
		
		'取节点，类型 IXMLDOMSelection
		'取个数 nodes.length
		set nodes = getNodes( parsedOptions("path") )
		for i = 0 to nodes.length-1
			set item = nodes.item(i)
			
			set qNode = item.selectSingleNode(fields)
			if qNode is nothing then				
				nodes.removenext
			elseif inStr( qNode.text,q )< 1 then				
				if i = 0 then
					nodes.reset()
					nodes.removenext
				else
					set item = nodes.item(i-1)
					nodes.removenext
				end if
				i = i - 1
			end if
		next
		soCount = nodes.length
	End Function
	
	'取键值对
	Function getKeyValue
		dim nodes,arr,keyName,valName,i,item,keyObj,valObj,dict
		Call init
		Call parseOptions
		
		'取节点，类型 IXMLDOMSelection
		'取个数 nodes.length
		set nodes = getNodes( parsedOptions("path") )
		set curNodes = nodes

		if parsedOptions.exists("where") then	
			Call filterNodes( nodes,parsedOptions("where") )
		end if
		
		if nodes.length<1 then
			Call ResumeOpts
			set getKeyValue = D_
			exit function
		end if
		
		if not isArray(  parsedOptions("field") ) then
			parsedOptions("field") = split( parsedOptions("field") , "," )
		end if
		
		if ubound(  parsedOptions("field") ) < 1 then
			POP_MVC.exit( parsedOptions("path") & " 使用getKeyValue方法时指定的字段至少得有两个" )
		end if
		
		keyName = parsedOptions("field")(0)
		valName = parsedOptions("field")(1)
		set dict = D_
		for i = 0 to nodes.length - 1
			set item = nodes.item(i)
			set keyObj = item.selectSingleNode( keyName )
			set valObj = item.selectSingleNode( valName )
			if not keyObj is nothing and not valObj is nothing then
				dict( keyObj.text ) = valObj.text
			end if
		next
		set getKeyValue = dict
		Call ResumeOpts 
	End Function
	
	'排序后取前N个的位置
	Function getSortPosition( sortType , num )
		dim dict,arr
		set dict = getKeyValue
		Execute("Call POP_MVC.dict." & sortType & "( dict )" )
		arr = dict.keys
		getSortPosition = POP_MVC.Arr.slice( arr, 0, num )
	End Function
	
	'连续操作取1条记录，返回一维Dictionary对象
	Function find( )
		dim data
		set data = [select]
		if data.count > 0 then
			set find = data(0)
		else
			set find = D_
		end if
	End Function
	
	' 定位查询，获取第N条记录，返回Dictionary对象
	' num正数从1开始计，负数则-1表示最后1条
	Function getN( num )
		dim data
		set data = [select]
		
		if data.count = 0 then
			set getN = D_
			Exit Function
		end if
		
		if num < 0 then
			num = data.count + num + 1
		end if
		if num < 1 then num = 1	
		if num > data.count then num = data.count
		
		set getN = data( num - 1 )
		POP_MVC.unset(data)
	End Function
	
	'得到前面一条
	Function prevRow( TagName, TagValue )
		dim data,i,item
		set data = [select]
		keys = data.keys
		for i = 0 to data.count-1
			set item = data( keys(i) )
			if item( TagName ) = TagValue then
				exit for
			end if
		next
		if i = 0 then
			set prevRow = D_
		else
			set prevRow = data(i-1)
		end if
	End Function
	
	'得到前面一条
	Function nextRow( TagName, TagValue )
		dim data,i,item
		set data = [select]
		keys = data.keys
		for i = 0 to data.count-1
			set item = data( keys(i) )
			if item( TagName ) = TagValue then
				exit for
			end if
		next
		if i = data.count-1 then
			set nextRow = D_
		else
			set nextRow = data(i+1)
		end if
	End Function
	
	Function getOne( )
		dim dict,fields,keys,arr
		fields = ParseField( options )
		set dict = find
		if dict.count > 0 then
			keys = dict.keys
			if fields = "" or fields = "*" then
				getOne = dict( keys(0) )
			else
				arr = split( fields , "," )
				getOne = dict( arr(0) )
			end if
		end if
	End Function
	
	'连贯操作取个数
	Function sum( TagName )
		dim nodes,i,ret,node,item
		Call init
		Call parseOptions
		'取节点
		set nodes = getNodes( parsedOptions("path") )
		
		if parsedOptions.exists("where") then	
			Call filterNodes( nodes,parsedOptions("where") )
		end if
		
		ret = 0
		for i = 0 to nodes.length - 1
			set item = nodes.item(i)
			if not item is nothing then
				set node = item.selectSingleNode(TagName)				
				if not node is nothing then
					if isNumeric( node.text ) then
						ret = ret + node.text
					end if
				end if
			end if
		next
		set curNodes = nodes
		Call ResumeOpts 
		sum = ret
	End Function
	
	'连贯操作取个数
	Function count( )
		dim nodes
		Call init
		Call parseOptions
		'取节点
		set nodes = getNodes( parsedOptions("path") )
		
		if parsedOptions.exists("where") then	
			Call filterNodes( nodes,parsedOptions("where") )
		end if

		set curNodes = nodes
		count = nodes.length
		Call ResumeOpts 
	End Function
	
	'是否存在
	Function exists( )
		if count() = 0 then
			exists = false
		else
			exists = true
		end if		
	End Function
	
	Function eof()
		eof = not exists()
	end function
	
	
	'连贯操作取一个属性
	Function attr( attrName )
		dim nodes
		Call init
		Call parseOptions
		
		'取节点，类型 IXMLDOMSelection
		'取个数 nodes.length
		set nodes = getNodes( parsedOptions("path") )

		if nodes.length > 0 then
			attr = nodes.item(0).getAttributeNode(attrName).nodevalue
		end if
		set curNodes = nodes
		Call ResumeOpts 
	End Function
	
	'根据节点路径，返回自定义对象
	'nodePath 节点路径
	'keyName　作为键名的节点名
	'valName  作为值的节点名
	Function getObject( )
		dim nodes , keyName , valName , arr
		Call init
		Call parseOptions
		
		set nodes = getNodes( parsedOptions("path") )
		
		'取节点
		arr = parsedOptions("field")
		if not isArray( arr ) then
			arr = split( arr , "," )
		end if
		if ubound(arr) < 1 then
			POP_MVC.exit( parsedOptions("path") & " 使用方法getObject时，指定的字段至少得有两个" )
		end if
		keyName = arr(0)
		valName = arr(1)
		
		
		set getObject =  nodes2object(  nodes , keyName , valName )	
		set curNodes = nodes
		Call ResumeOpts
	End Function
	
	'向xml中设置健值对，如果不存在则追加
	Function mergeField( fieldName, fieldValue )
		dim nodes,i,item,node,bool
		Call init
		Call parseOptions
		'取节点
		set nodes = getNodes( parsedOptions("path") )

		if nodes.length > 0 then
			mergeField = true		
		else
			mergeField = false
		end if
		for i = 0 to nodes.length-1
			set item = nodes.item(i)
			if typename( fieldName ) = "Dictionary" then
				Call MergeNode( item , fieldName, 1 )
			else
				bool = false
				for each node in item.ChildNodes			
					if LCase(node.BaseName) = LCase(fieldName) then
						Call setNodeValue( node,fieldValue )
						bool = true
						exit for
					end if
				next
				if not bool then
					item.appendChild( createNode(fieldName , fieldValue) )
				end if
			end if
		next

		Call SaveXml
		set curNodes = nodes
		Call ResumeOpts
	End Function	
	
	'向xml中设置健值对，如果不存在则追加
	Public Property Get setNot( fieldName )
		dim nodes,i,item,node,bool
		Call init
		Call parseOptions
		'取节点
		set nodes = getNodes( parsedOptions("path") )

		setNot = nodes.length
		for i = 0 to nodes.length-1
			set item = nodes.item(i)
			if typename( fieldName ) = "Dictionary" then
				Call MergeNode( item , fieldName, 1 )
			else
				bool = false
				for each node in item.ChildNodes	
					if LCase(node.BaseName) = LCase(fieldName) then
						if node.text = 1 or LCase(node.text) = "true" then
							Call setNodeValue( node,0 )
						elseif node.text = 0 or LCase(node.text) = "false" or LCase( node.text ) = "null" then
							Call setNodeValue( node,1 )
						end if						
						bool = true
						exit for
					end if
				next
				if not bool then
					item.appendChild( createNode(fieldName , 1) )
				end if
			end if
		next
		Call SaveXml
		set curNodes = nodes
		Call ResumeOpts
	End Property

	
	'向xml中设置健值对，不存在也不追加
	'fieldName 节点名
	'fieldValue 节点值
	'如果要设置一组，fieldName设为Dictionary，此时fieldValue值无效
	Function setField( fieldName, fieldValue )
		dim nodes,i,item,node,keys
		Call init
		Call parseOptions
		'取节点
		set nodes = getNodes( parsedOptions("path") )
		setField = nodes.length
		if typename( fieldName ) = "Dictionary" then
			keys = fieldName.keys			
		end if
		for i = 0 to nodes.length-1
			set item = nodes.item(i)
			if typename( fieldName ) = "Dictionary" then
				for each node in item.ChildNodes
					pos = POP_MVC.arr.iSearch( keys,node.BaseName )	
					if pos > -1 then						
						Call setNodeValue( node,fieldName( keys(pos) ) )
					end if
				next
			else
				for each node in item.ChildNodes			
					if LCase(node.BaseName) = LCase(fieldName) then
						Call setNodeValue( node,fieldValue )
						exit for
					end if
				next
			end if
		next
		Call SaveXml
		set curNodes = nodes
		Call ResumeOpts
	End Function
	
	'向尾部添加一条记录
	'如果packeageName为空，使用MergeNode，有则修改，无则追加
	'如果packeageName不为空，则相当于数据库中的追加一条记录	
	Function append( packageName , idName )
		dim nodes,i,item,dict
		Call init
		Call parseOptions
		'取节点
		set nodes = getNodes( parsedOptions("path") )
		set dict = parsedOptions("data")
		for i = 0 to nodes.length-1
			set item = nodes.item(i)
			if isNul(packageName) then
				Call MergeNode( item , dict , 1 )
			else
				item.appendChild( createNodes( packageName , dict ) )
			end if
		next
		
		Call SaveXml
		set curNodes = nodes
		if not isNul(idName) then
			append = parsedOptions("data")(idName)
		else
			append= true
		end if
		Call ResumeOpts
	End Function
	
	'向尾部添加键值对，适用于配置
	'如果packeageName为空，使用MergeNode，有则修改，无则追加
	'如果packeageName不为空，则相当于数据库中的追加一条记录	
	Function appendData( packageName , idName )
		dim nodes,i,item,dict,data,key
		Call init
		Call parseOptions
		'取节点
		set nodes = getNodes( parsedOptions("path") )
		set data = parsedOptions("data")
		for i = 0 to nodes.length-1
			set item = nodes.item(i)
			if isNul(packageName) then
				for each key in data
					set dict = data(key)
					Call MergeNode( item , dict , 1 )
				next
			else
				for each key in data
					set dict = data(key)
					item.appendChild( createNodes( packageName , dict ) )
				next
			end if
		next
		Call SaveXml
		set curNodes = nodes
		if not isNul(idName) then
			appendData = parsedOptions("data")(idName)
		else
			appendData= true
		end if
		Call ResumeOpts
	End Function	
	
	'向头添加一条记录
	'如果packeageName为空，使用MergeNode，有则修改，无则追加
	'如果packeageName不为空，则相当于数据库中的追加一条记录	
	Function prepend( packageName , idName )
		dim nodes,i,item,dict,node
		Call init
		Call parseOptions
		'取节点
		set nodes = getNodes( parsedOptions("path") )
		set dict = parsedOptions("data")

		for i = 0 to nodes.length-1
			set item = nodes.item(i)
			if isNul(packageName) then
				Call MergeNode( item , dict , 0 )
			else
				if item.ChildNodes.length > 0 then
					set node = item.ChildNodes.item(0)
					Call item.insertBefore( createNodes( packageName , dict ) , node )
				else
					item.appendChild( createNodes( packageName , dict ) )
				end if
			end if
		next

		Call SaveXml	
		set curNodes = nodes
		if not isNul(idName) then
			prepend = parsedOptions("data")(idName)
		else
			prepend= true
		end if
		Call ResumeOpts		
	End Function
	
	'删除记录
	Public Property Get remove()
		dim nodes,i,item,dict,node
		Call init
		Call parseOptions
		'取节点
		set nodes = getNodes( parsedOptions("path") )
		remove = nodes.length
		for i = 0 to nodes.length-1
			set item = nodes.item(i)
			item.parentNode.removeChild(item)
		next
		Call SaveXml
		Call ResumeOpts	
	End Property
	
	'创建一个CDATASection对象
	Function getCDataNode( str )
		set getCDataNode = xmlobj.xmlDomObj.createCDATASection( str )
	End Function
	
	'设置节点值
	Function setNodeValue( node,str )
		if inStr( str, "<" ) > 0 then
			node.text = ""
			node.appendChild( getCDataNode( str ) )			
		else
			node.text = str
		end if
	End Function
	
	'创建一个元素节点
	Function createNode( nodeName , nodeValue )
		dim node
		set node = xmlobj.xmlDomObj.createElement( nodeName )
		Call setNodeValue( node, nodeValue )
		set createNode = node
	end Function
	
	sub setNodeAttr( node , attrName , attrValue )
		 node.setAttribute attrName, attrValue
	End sub
	
	'创建一条记录节点
	Function createNodes( packageName , dict )
		dim parentNode	'父节点
		set parentNode = createNode( packageName , "" )
		for each key in dict
			parentNode.appendChild( createNode( key , dict(key) ) )
		next
		set createNodes = parentNode
	End Function
	
	'修改一条记录
	Function save( )
		dim nodes,i,item,node,keys
		Call init
		Call parseOptions
		set nodes = getNodes( parsedOptions("path") )
		set dict = parsedOptions("data")
		if typename( dict ) <> "Dictionary" then
			Save = false : exit function
		end if
		keys = dict.keys
		for i = 0 to nodes.length-1
			set item = nodes.item(i)
			for each node in item.ChildNodes
				pos = POP_MVC.arr.iSearch( keys,node.BaseName )					
				if pos > -1 then
					Call setNodeValue( node,dict( keys(pos) ) )
				end if
			next
			exit for
		next
		Save = true
		Call SaveXml
		set curNodes = nodes
		Call ResumeOpts
	End Function
	
	'自增1
	'若自增值为其它，需要使用数组
	'setInc("Visits")  setInc( array( "Price" , 2 ) )
	Public Property Get setInc( arg )
		setInc = setIncOrDec( arg, 1 )
	End Property
	
	'修改一条记录
	Public Property Get setDec( arg )
		setDec = setIncOrDec( arg, 0 )
	End Property
	
	
	'获取当前页
	Function getCurPage( )
		if is_numeric( POP_MVC.req( POP_MVC.config("VAR_PAGE")  ) ) then
			getCurPage = CInt( POP_MVC.req( POP_MVC.config("VAR_PAGE") ) )
		end if
		if getCurPage < 1 then
			getCurPage = 1
		end if
	End Function
	
	Function getPerPage( )		
		if is_numeric( POP_MVC.req( POP_MVC.config("VAR_PAGESIZE") ) ) then
			getPerPage = CInt( POP_MVC.req( POP_MVC.config("VAR_PAGESIZE") ) )
		end if
		if getPerPage < 1 then
			getPerPage = pagesize
		end if
	End Function
	
	Public Sub pushDebug( thePath , start , info ) 
		dim sql
		if not is_empty( POP_MVC.config("SHOW_PAGE_TRACE") ) Then
			set sql = Server.CreateObject("Scripting.Dictionary")
			sql("time") = round((timer() - start) * 1000,0)
			sql("sql") = thePath & " ; -- " & info
			set POP_MVC.dSql(POP_MVC.dSql.count + 1) = sql
			set sql = nothing
		end if	
	End Sub
	
	Public sub filterNodes(nodes,filters)
		dim key,item,line,oneFilter,stype,nodevalue,nodeName,opt,node,bool,arr

		
		if typename(filters) = "String" then
			arr = split( filters, "=" ,2 )
			set filters = POP_MVC.SCD
			filters( trim(arr(0)) ) = trim(arr(1))
		end if

		for each key in filters
			oneFilter = filters(key)
		
			for i = 0 to nodes.length - 1
				bool = false
				set item = nodes.item(i)	
				
				if item is nothing then
					exit for
				end if
				
				nodeName = key
				set node = item.selectSingleNode(nodeName)
				
				if node is nothing then
					bool = true					
				else
					if isArray( oneFilter ) then
						stype = oneFilter(0)
						opt = oneFilter(1)
					else
						stype = "eq"
						opt = oneFilter
					end if				

					nodeValue = node.text
					stype = LCase(stype)
					select case stype
						case "eq"
							if CStr(opt) <> nodeValue then
								bool = true
							end if
						case "gt"
							if is_numeric(opt) and is_numeric(nodeValue) then
								if opt - nodeValue > 0 then
									bool = true
								end if
							end if
						case "in"
							if isArray(opt) then
								bool = not POP_MVC.Arr.Exists( opt, nodeValue )
							else
								if inStr( "," & opt & "," , "," & nodeValue & "," ) < 1 then
									bool = true
								end if
							end if
						case "position"
							if isArray(opt) then
								bool = not POP_MVC.Arr.Exists( opt, i )
							else
								if inStr( "," & i & "," , "," & opt & "," ) < 1 then
									bool = true
								end if
							end if
						case "instr"
							if inStr( LCase(nodeValue) , lCase(opt) ) < 1 then
								bool = true
							end if
						case "list"
							if inStr( "," & nodeValue & "," , "," & opt & "," ) < 1 then
								bool = true
							end if
						case "daydiff"
							if not isDate(nodeValue) then
								bool = true
							else								
								if DATEDIFF( "d" , nodeValue , now ) - opt > 0 then
									bool = true
								end if
							end if
						case "between"
							if not isNumeric( nodeValue ) then
								bool = true
							else
								if not isArray( opt ) then
									opt = split( opt, "," )
								end if
								if not ( nodeValue - opt(0) >= 0 and nodeValue - opt(1) <=0 ) then
									bool = true
								end if
							end if
						case "notbetween"
							if not isNumeric( nodeValue ) then
								bool = true
							else
								if not isArray( opt ) then
									opt = split( opt, "," )
								end if
								if ( nodeValue - opt(0) >= 0 and nodeValue - opt(1) <=0 ) then
									bool = true
								end if
							end if	
						case "date"
							if not isDate(nodeValue) then
								bool = true
							else
								if DATEDIFF( "d" , nodeValue , opt ) <>0 then
									bool = true
								end if
							end if
						case "month"
							if not isDate(nodeValue) then
								bool = true
							else
								if isDate(opt) then
									if DATEDIFF( "m" , nodeValue , opt ) <>0 then
										bool = true
									end if								
								else
									if left(nodeValue,7) = left(opt,7) then
										bool = true
									end if
								end if
							end if
						case "year"
							if not isDate(nodeValue) then
								bool = true
							else
								if isDate(opt) then
									if DATEDIFF( "yyyy" , nodeValue , opt ) <>0 then
										bool = true
									end if								
								else
									if year(nodeValue) - opt <> 0 then
										bool = true
									end if
								end if
							end if
					end select
				end if
				
				if bool then
					if i = 0 then
						nodes.reset()
						nodes.removenext
						i = i -1
					else
						set item = nodes.item(i-1)
						nodes.removenext
						i = i -1
					end if	
				end if					
			next
		next		
	End Sub
	
	Public Property Get page( opts )
		Call setOption("page",opts) : set page = Me
	End Property
	
	Public Property Get filter( opts )
		Call setOption("filter",opts) : set filter = Me
	End Property
	
	Public Property Get path( opts )
		Call setOption("path",opts) : set path = Me
	End Property
	
	Public Property Get field( opts )
		Call setOption("field",opts) : set field = Me
	End Property
	
	Public Property Get order( opts )
		Call setOption("order",opts) : set order = Me
	End Property
	
	Public Property Get where( opts )
		Call setOption("where",opts) : set where = Me
	End Property
	
	Public Property Get data( opts )
		dim bool
		if C_("DB_TYPE") = "access" then
			bool = true
		else
			bool = false
		end if
		if typename( opts ) = "Dictionary" then	
			Call setOption("data", POP_MVC.Dict.Clone(opts) ) 
		elseif typename( opts ) = "Recordset" then
			Call setOption("data", P_("POPASP_RECORDSET").rs2data(opts,"",bool,0) ) 
		else
			Call setOption("data", opts ) 
		end if
		set data = Me	
	End Property
	
	private Sub setOption(key,opts)
		POP_MVC.dict.edit options,key,opts
	End Sub	
	
	Public Property Get [auto]( opts )
		Call setOption("auto",opts) : set [auto] = Me
	End Property
	
	Public Property Get validate( opts )
		Call setOption("validate",opts) : set validate = Me
	End Property
	
	Public Property Get top( opts )
		Call setOption("page",array(1,opts)) 
		set top = Me
	End Property
	
	Function ParseField( opts )
		dim key,arr,str,keys,pos
		if opts.Exists("field") then
			if TypeName( opts("field") ) = "String" Then
				ParseField = opts("field")
			Elseif isArray( opts("field") ) Then
				ParseField = Join( opts("field") , "," )
			End If
		end if
	End Function
	
	Public Function ParsePage( opts )
		on error resume next
		dim optsType : optsType = TypeName( opts )
		dim arr,i,bound	
		if optsType = "Integer"  OR optsType = "Long" OR optsType = "Null" Then
			ParsePage = array( opts )
		elseif optsType = "String"  Then 				
			arr = split(opts,",")	
			bound = ubound(arr)
			for i = 0 to bound
				arr(i) = trim( arr(i) )
			next
			ParsePage = arr
		Elseif isArray( opts ) Then
			ParsePage = opts
		Else 
			POP_MVC.error( "page 参数不正确" )
		End if
	End Function
	
	' 解析order
	' 可以传入字符串: id 或者 id desc 或者 name desc,time asc
	' 也可以传入dict对象： {name:"desc","time":asc}
	Public Function ParseOrder(opts)
		ParseOrder = opts
	End Function
	
	Public Function ParsePath(opts)
		ParsePath = opts
	End Function
	
	Public Sub setNodesPageSize( ByRef nodes )
		dim curpage,startNum,cnt,totalPage
		
		if typename( nodes ) <> "IXMLDOMSelection" then
			exit sub
		end if
		
		if nodes.length < 1 then
			exit sub
		end if
		
		if not parsedOptions.exists("order") then
			parsedOptions("order") = 1
		end if
		
		if not parsedOptions.exists("page") then
			parsedOptions("page") = array(null,null) : 	exit sub
		elseif isNull(parsedOptions("page")) then
			parsedOptions("page") = array(null,null) : 	exit sub
		end if		
		
		curPage = parsedOptions("page")(0)
		if ubound( parsedOptions("page") ) >0 Then		'如果有两个元素			
			perpage = parsedOptions("page")(1)
		elseif POP_MVC.String.reg_test( POP_MVC.req( POP_MVC.config("VAR_PAGESIZE") ) , "^[1-9]\d*$" , "" ) then
			perpage =  POP_MVC.req( POP_MVC.config("VAR_PAGESIZE") )
		End If

		if typename(curPage) = "String" then
			if LCase( curPage ) = "null" then
				curPage =  POP_MVC.req( POP_MVC.config("VAR_PAGE") )
				curPage = CInt( curPage )
			end if
		end if

		if isNul( perpage ) then
			perpage = getPerPage()
			cnt = perpage
		elseif perpage = 0 then
			cnt = pagesize
		else
			cnt = perpage
		end if		
			
		if curPage < 0 then
			if parsedOptions("order") = 1 then
				curPage = 1
			else
				curPage = totalPage + curPage
			end if
		elseif curPage = 0 then
			curPage = 1
		end if
		
		totalPage = CLng(Abs(Int(- nodes.length / perpage)))
		
		if curPage > totalPage then
			curPage = totalPage
		end if
		
		if isNul( curPage ) then
			curPage = getCurPage
			startNum = (curPage - 1) * perpage
		else
			startNum = (curPage - 1) * perpage
		end if	

		parsedOptions("page") = Array( Cint(startNum) , Cint(cnt) )	
	End sub
	
	'解析查询条件
	Private Sub parseOptions
		on error resume next
		dim key	,ts , pk

		for each key in options
			if key<>"auto" AND key<>"validate" Then	
				select case key	
					case "data"
						POP_MVC.Dict.edit parsedOptions , key , options(key)
					case "order","page","path"
						Execute( "parsedOptions(key) = Parse" & key & "( options(key) )" )
					case "field","fieldRev"	
						parsedOptions(key) = ParseField( options )
					case "where"
						if isObject( options(key) ) then
							set parsedOptions(key) = options(key)
						else
							parsedOptions(key) = options(key)
						end if						
				End Select
			End If
		next
	End sub
	
	'向记录中设置健值对，如果不存在则追加
	Function mergeNode( recordNode, dict , action )
		dim keys,arr,node,pos
		arr = Array()
		keys = dict.keys
		for each node in recordNode.ChildNodes
			pos = POP_MVC.arr.iSearch( keys,node.BaseName )					
			if pos > -1 then
				POP_MVC.arr.push arr,node.BaseName
				Call setNodeValue( node,dict( keys(pos) ) )
			end if
		next
		for each key in dict
			if not POP_MVC.arr.iExists( arr , key ) then
				'尾部添加
				if action = 1 then
					recordNode.appendChild( createNode(key , dict(key) ) )
				elseif action = 0 then
					if recordNode.ChildNodes.length > 0 then
						set node = recordNode.ChildNodes.item(0)
						Call recordNode.insertBefore( createNode(key , dict(key) ) , node )
					else
						recordNode.appendChild( createNode(key , dict(key) ) )
					end if
				end if					
			end if
		next
	End Function

	
	'自增或自减
	Private Function setIncOrDec( arg ,action )
		dim nodes,i,item,node,keys,fieldName,incValue,bool,tempName
		Call init
		Call parseOptions
		set nodes = getNodes( parsedOptions("path") )
		if nodes.length < 1 then
			setIncOrDec = 0 : exit function
		end if
		incValue = 1
		if isArray( arg ) then
			fieldName = arg(0)
			if ubound( arg ) > 0 then
				incValue = arg(1)
			end if
		else
			fieldName = arg
		end if
		
		if action = 0 then
			incValue = -1 * incValue
		end if
		tempName = fieldName
		fieldName = LCase( fieldName )
		for i = 0 to nodes.length-1
			set item = nodes.item(i)
			bool = false
			for each node in item.ChildNodes
				if LCase( node.BaseName ) = fieldName then
					bool = true
					if not isNumeric( node.text ) then
						node.text = incValue
					else
						node.text = node.text + incValue
					end if
					exit for
				end if
			next
			
			if not bool then
				item.appendChild( createNode( tempName , incValue ) )
			end if
		next
		setIncOrDec = nodes.length
		Call SaveXml
		set curNodes = nodes
		Call ResumeOpts
	End Function
	
	'将节点转成Dictionary对象
	'nodePath 节点路径
	'fields   需要取的节点名，取所有时，为空或*
	'startNum 从第几条开始，第1条0，若取所有，取null
	'cnt      取几条，取所有时为null即可
	'action   取数据方向，1正向，0逆向
	Function nodes2dict( ByRef nodes ,ByRef fields, ByVal startNum, ByRef cnt ,action )
		dim dict,ret,node,bnd,counter,start_,end_,step_		
		if nodes.length < 1 then
			set nodes2dict = D_
			exit function
		end if
		
		set ret = D_

		'将节点名转成数组
		if not isArray( fields ) then
			if isNul( fields ) or fields = "*" then
				fields = Array()
			else			
				fields = split( fields,"," )
			end if
		end if

		bnd = ubound( fields )
		counter = 0
		
		if action =1 then
			if isNull( startNum ) then
				start_ = 0
			else
				start_ = startNum
			end if
			
			if isNull( cnt ) then
				end_ = nodes.length -1
			else
				end_   = start_ + cnt -1
				if end_ > nodes.length -1 then
					end_ = nodes.length -1
				end if
			end if
			step_ = 1
		elseif action = 0 then
			if isNull( startNum ) then
				start_ = nodes.length - 1
			elseif startNum > 0 then
				start_ = startNum
			elseif startNum < 0 then
				start_ = nodes.length + startNum
			end if
			
			if isNull( cnt ) then
				end_ = 0
			else
				end_   = start_ - cnt + 1
				if end_ < 0 then
					end_ = 0
				end if
			end if
			step_ = -1
		end if


		for i = start_ to end_ step step_
			set dict = D_
			set item = nodes.item(i)
			for each node in item.ChildNodes
				'节点位置，从0开始
				dict( "_NODE_INDEX_" ) = i
				if bnd < 0 then
					dict( node.BaseName ) = node.text
				elseif POP_MVC.Arr.iExists( fields,node.BaseName  ) then
					dict( node.BaseName ) = node.text
				end if
			next
			POP_MVC.dict.push ret , ret.count, dict
		next
		set xmlobj = nothing
		set nodes2dict = ret
	End Function
	
	'将一对字段中的第1个作为键名，将第2个字段的值作为单一值形成的object对象
	Function nodes2object( ByRef nodes , ByRef keyName, ByRef valName)	
		on error resume next
		dim start,str,str2,temp,className,i,temp2,arr,str3,obj
		dim item
		
		

		if nodes.length<0 then
			set nodes2object = Nothing
			exit Function
		end if
		
		if not is_empty( POP_MVC.config("SHOW_PAGE_TRACE") ) Then
			start = timer()	
		end if	

		className = POP_MVC.config("POPASP_SELF_OBJECT_PREFIX") & POP_MVC.dClass.count
		str = "Class " & className & vbcrlf	
	
		if nodes.length>0 then
			str2 = ""
			temp = ""
			temp2 = ""
			str = str & "Public vars__" & vbcrlf
			str = str & "Public "
			for i = 0 to nodes.length - 1
				set item = nodes.item(i)
				set obj = item.selectSingleNode(keyName)
				if not obj is nothing then
					temp = temp & "[" & obj.text & "],"
					temp2 =  temp2 & """" & obj.text & ""","
					str2 = str2 & vbTab & "if nodes.item(" & i & ").selectSingleNode(""" & valName &""") is Nothing then" & vbcrlf
					str2 = str2 & vbTab & vbTab & "[" & obj.text & "]=null" & vbcrlf
					str2 = str2 & vbTab & "else" & vbcrlf
					str2 = str2 & vbTab & vbTab & "[" & obj.text & "]=" & "nodes.item(" & i & ").selectSingleNode(""" & valName &""").text" & vbcrlf
					str2 = str2 & vbTab & "end if" & vbcrlf
				end if
			next

			temp = left( temp , len(temp) - 1 )
			temp2 = left( temp2 , len(temp2) - 1 )
			str = str & temp & vbcrlf
			str = str & "Public Sub Init__(ByRef nodes)" & vbcrlf
			str = str & vbTab & "vars__ = Array(" & temp2 & ")" & vbcrlf
			str = str & str2
			str = str & "End Sub" & vbcrlf
			str = str & "Property Get Exists__(ByRef key__)" & vbcrlf
			str = str & vbTab & "Exists__=POP_MVC.Arr.iExists(vars__,key__)"  & vbcrlf
			str = str & "End Property" & vbcrlf
			str = str & "Property Get Get__(ByRef key__)" & vbcrlf
			str = str & vbTab & "Ex" & Chr("101") & "cute(""Get__ = "" &  key__ )"  & vbcrlf
			str = str & vbTab & "if isNull(Get__) Then Get__ = """""  & vbcrlf
			str = str & "End Property" & vbcrlf
		end if	
		str = str & "End Class" & vbcrlf
		
		
		

		
		on error resume next	

		execute( str )
		str3 = "set POP_MVC.dClass(className) = NEW " & className
		

		execute( str3 )
		POP_MVC.dClass(className).init__(nodes)
		set nodes2object = POP_MVC.dClass(className)

		Call pushDebug( "nodes转对象", start ,  "nodes to " & className )
	End Function
	
	'根据路径获取一组节点
	Function getNodes( ByRef nodePath )
		Call init
		set  getNodes = xmlobj.getNodes(nodePath)
	End Function
	
	'根据路径获取某个节点
	Function getNode( ByRef nodePath , pos )
		dim nodes
		Call init
		set  nodes = getNodes(nodePath)
		set getNode = nodes.item(pos)
	End Function
	
	Public Sub ResumeOptions()
		set options = nothing  : set options = POP_MVC.dict.Create()
	End Sub
	
	Public Sub ResumeParsedOptions()
		set parsedOptions = nothing	 : set parsedOptions = POP_MVC.dict.Create()
	End Sub
	
	' 重置链接操作的参数，可链式操作
	Public Function ResumeOpts()
		call ResumeOptions : call ResumeParsedOptions : set ResumeOpts = Me
	End Function
End Class
%>