<%
' +----------------------------------------------------------------------
' | POPASP [ ASP MVC ]
' +----------------------------------------------------------------------
' | Copyright (c) 2016 http://popasp.com All rights reserved.
' +----------------------------------------------------------------------
' | Author: popasp <1737025626@qq.com>
' +----------------------------------------------------------------------

'处理RECORDSET的类
Class POPASP_XMLDB4BBS
	public db_path,db_type,xmlobj,curPage,curNodes
	public auto_,validate_
	Private options 		'存储分配过来的参数，Dictionary对象
	
	public  [error]			'最近错误消息
	
	Private Sub Class_Initialize()
		db_path = POP_MVC.config("XML_PATH")
		db_type = POP_MVC.config("XML_TYPE")
	End Sub
	
	Private Sub Class_Terminate()
		If IsObject(xmlobj) Then Set xmlobj=Nothing
	End Sub
	
	
	'初始化
	Public Property Get Init
		set  xmlobj = new POPASP_XML
		
		if db_type = "" then
			db_type = "xmlfile"
		end if
		
		if db_type = "xmlfile" then
			if not POP_MVC.file.isFile( db_path ) then
				POP_MVC.error( POP_MVC.File.baseName( db_path ) & "文件不存在" )
			end if
		end if
		

		Call xmlobj.load( db_path ,db_type )		
		set Init = Me
	End Property	
	
	
	
	'根据节点路径，返回自定义对象
	'nodePath 节点路径
	'keyName　作为键名的节点名
	'valName  作为值的节点名
	Function getObject( path , field )
		dim nodes , keyName , valName , arr
		Call init
		
		set nodes = getNodes( path )
		

		
		'取节点
		arr = field
		if not isArray( arr ) then
			arr = split( arr , "," )
		end if
		if ubound(arr) < 1 then
			POP_MVC.exit( path & " 使用方法getObject时，指定的字段至少得有两个" )
		end if
		keyName = arr(0)
		valName = arr(1)
		

		set getObject =  nodes2object(  nodes , keyName , valName )	
		set curNodes = nodes
	End Function
	
	'根据路径获取一组节点
	Function getNodes( ByRef nodePath )
		Call init	
		set  getNodes = xmlobj.getNodes(nodePath)
	End Function
	
	'将一对字段中的第1个作为键名，将第2个字段的值作为单一值形成的object对象
	Function nodes2object( ByRef nodes , ByRef keyName, ByRef valName)	
		on error resume next
		dim start,str,str2,temp,className,i,temp2,arr,str3,obj
		dim item

		if nodes.length<0 then
			set nodes2object = Nothing
			exit Function
		end if
		start = timer()	

		className = POP_MVC.config("POPASP_SELF_OBJECT_PREFIX") & POP_MVC.dClass.count
		str = "Class " & className & vbcrlf	
					
		if nodes.length>0 then
			str2 = ""
			temp = ""
			temp2 = ""
			str = str & "Public vars__" & vbcrlf
			str = str & "Public "
			
			
			for i = 0 to nodes.length - 1
				set item = nodes.item(i)
				
				set obj = item.selectSingleNode(keyName)
				if not obj is nothing then
					temp = temp & "[" & obj.text & "],"
					temp2 =  temp2 & """" & obj.text & ""","
					str2 = str2 & vbTab & "if nodes.item(" & i & ").selectSingleNode(""" & valName &""") is Nothing then" & vbcrlf
					str2 = str2 & vbTab & vbTab & "[" & obj.text & "]=null" & vbcrlf
					str2 = str2 & vbTab & "else" & vbcrlf
					str2 = str2 & vbTab & vbTab & "[" & obj.text & "]=" & "nodes.item(" & i & ").selectSingleNode(""" & valName &""").text" & vbcrlf
					str2 = str2 & vbTab & "end if" & vbcrlf
				end if
			next

			temp = left( temp , len(temp) - 1 )
			temp2 = left( temp2 , len(temp2) - 1 )
			str = str & temp & vbcrlf
			str = str & "Public Sub Init__(ByRef nodes)" & vbcrlf
			str = str & vbTab & "vars__ = Array(" & temp2 & ")" & vbcrlf
			str = str & str2
			str = str & "End Sub" & vbcrlf
			str = str & "Property Get Exists__(ByRef key__)" & vbcrlf
			str = str & vbTab & "Exists__=POP_MVC.Arr.iExists(vars__,key__)"  & vbcrlf
			str = str & "End Property" & vbcrlf
			str = str & "Property Get Get__(ByRef key__)" & vbcrlf
			str = str & vbTab & "Ex" & Chr("101") & "cute(""Get__ = "" &  key__ )"  & vbcrlf
			str = str & vbTab & "if isNull(Get__) Then Get__ = """""  & vbcrlf
			str = str & "End Property" & vbcrlf
		end if	
		str = str & "End Class" & vbcrlf
	
		execute( str )
		str3 = "set POP_MVC.dClass(className) = NEW " & className
		



		execute( str3 )
		POP_MVC.dClass(className).init__(nodes)
		set nodes2object = POP_MVC.dClass(className)

		Call pushDebug( "nodes转对象", start ,  "nodes to " & className )
	End Function
End Class
%>