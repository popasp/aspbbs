<%
' +----------------------------------------------------------------------
' | POPASP [ ASP MVC ]
' +----------------------------------------------------------------------
' | Copyright (c) 2016 http://popasp.com All rights reserved.
' +----------------------------------------------------------------------
' | Author: popasp <1737025626@qq.com>
' +----------------------------------------------------------------------
'节点操作
Class POPASP_XMLNODE
  Private o_node
  '析构
  Private Sub Class_Terminate()
    Set o_node = Nothing
  End Sub
  '建立新Node对象
  Private Function [New](ByVal o)
    Set [New] = New POPASP_XMLNODE
    [New].Dom = o
  End Function
  
  '源对象
  Public Property Let Dom(ByVal o)
    If Not o Is Nothing Then
      Set o_node = o
    Else
      If Not is_empty( POP_MVC.config("APP_DEBUG") ) Then
		POP_MVC.error( dError( "error-xml-notdom" ) )
      End If
      Set o_node = Nothing
    End If
  End Property
  Public Property Get Dom
    Set Dom = o_node
  End Property
  
  '取集合中的某一项
  Public Default Function Item(ByVal n)
    '如果是集合就取其中下标对应子项
    If IsNodes Then
      Set Item = [New](o_node(n))
    '如果是节点且下标为0就取节点本身
    ElseIf IsNode And n = 0 Then
      Set Item = [New](o_node)
    Else
      If Not is_empty( POP_MVC.config("APP_DEBUG") ) Then
		POP_MVC.exit( dError("error-xml-notnode") )
      End If
      Set Item = Nothing
    End If
  End Function
  
  '=======Xml元素属性（自身属性）======

  '是否是元素节点
  Public Function IsNode
    IsNode = TypeName(o_node) = "IXMLDOMElement"
  End Function
  '是否是元素集合
  Public Function IsNodes
    IsNodes = TypeName(o_node) = "IXMLDOMSelection"
  End Function
  '属性设置(可读可写)
  Public Property Let Attr(ByVal s, ByVal v)
    '如果值为 Null 相当于删除属性
    If IsNull(v) Then RemoveAttr s : Exit Property
    '如果是节点
    If IsNode Then
      o_node.setAttribute s, v
    '如果是集合则设置每个子节点的属性
    ElseIf IsNodes Then
      Dim i
      For i = 0 To Length - 1
        o_node(i).setAttribute s, v
      Next
    End If
  End Property
  Public Property Get Attr(ByVal s)
    If Not IsNode Then Exit Property
    Attr = o_node.getAttribute(s)
  End Property
  
  '文本设置
  Public Property Let [Text](ByVal v)
    If IsNode Then
      If not is_empty(v) Then o_node.Text = v
    '如果是集合则设置每个子节点的文本
    ElseIf IsNodes Then
      Dim i
      For i = 0 To Length - 1
        If not is_empty(v) Then o_node(i).Text = v
      Next
    End If
  End Property
  Public Property Get [Text]
    If IsNode Then
      Text = o_node.Text
    ElseIf IsNodes Then
      Dim i
      For i = 0 To Length - 1
        Text = Text & o_node(i).Text
      Next
    End If
  End Property
  
  '文本设置
  Public Property Let Value(ByVal v)
    If IsNode Then
      o_node.ChildNodes(0).NodeValue = v
    ElseIf IsNodes Then
      Dim i
      For i = 0 To Length - 1
        o_node(i).ChildNodes(0).NodeValue = v
      Next
    End If
  End Property
  Public Property Get Value
    If Not IsNode Then Exit Property
    Value = o_node.ChildNodes(0).NodeValue
  End Property
  
  '获取XML(只读)
  Public Property Get Xml
    If IsNode Then
      Xml = o_node.Xml
    ElseIf IsNodes Then
      Dim i
      For i = 0 To Length - 1
        If i>0 Then Xml = Xml & vbCrLf
        Xml = Xml & o_node(i).Xml
      Next
    End If
  End Property
  '取元素名
  Public Property Get Name
    If Not IsNode Then Exit Property
    Name = o_node.BaseName
  End Property
  '取元素类型
  Public Property Get [Type]
    If IsNodes Then
      [Type] = 0
    Else
      [Type] = o_node.NodeType
    End If
  End Property
  '取元素类型名称
  Public Property Get TypeString
  If IsNodes Then
      TypeString = "selection"
    Else
      TypeString = o_node.NodeTypeString
    End If
  End Property
  '取元素长度
  Public Property Get Length
    If IsNode Then 
      Length = o_node.ChildNodes.Length
    Else
      Length = o_node.Length
    End If
  End Property
  
  '=======Xml元素属性（返回新节点元素）======
  
  '获取根元素
  Public Function Root
    If IsNode Then
      Set Root = [New](o_node.OwnerDocument)
    Else
      Set Root = [New](o_node(0).OwnerDocument)
    End If
  End Function
  '获取父元素
  Public Function Parent
    If Not IsNode Then Exit Function
    Set Parent = [New](o_node.parentNode)
  End Function
  '获取子元素
  Public Function Child(ByVal n)
    If Not IsNode Then Exit Function
    Set Child = [New](o_node.ChildNodes(n))
  End Function
  '获取上一同级元素
  Public Function Prev
    If Not IsNode Then Exit Function
    Dim o
    Set o = o_node.PreviousSibling
    Do While True
      If TypeName(o) = "Nothing" Or TypeName(o) = "IXMLDOMElement" Then Exit Do
      Set o = o.PreviousSibling
    Loop
    If TypeName(o) = "IXMLDOMElement" Then
      Set Prev = [New](o)
    Else
      If Not is_empty( POP_MVC.config("APP_DEBUG") ) Then
		POP_MVC.exit( dError( "error-xml-prevel" ) )
      End If
      Set Prev = Nothing
    End If
    Set o = Nothing
  End Function
  '获取下一同级元素
  Public Function [Next]
    If Not IsNode Then Exit Function
    Dim o
    Set o = o_node.NextSibling
    Do While True
      If TypeName(o) = "Nothing" Or TypeName(o) = "IXMLDOMElement" Then Exit Do
      Set o = o.NextSibling
    Loop
    If TypeName(o) = "IXMLDOMElement" Then
      Set [Next] = [New](o)
    Else
      If Not is_empty( POP_MVC.config("APP_DEBUG") ) Then
		POP_MVC.exit( dError( "error-xml-nextel" ) )
      End If
      Set [Next] = Nothing
    End If
    Set o = Nothing
  End Function
  '获取第一个元素
  Public Function First
    If Not IsNode Then Exit Function
    Set First = [New](o_node.FirstChild)
  End Function
  '获取最后一个元素
  Public Function Last
    If Not IsNode Then Exit Function
    Set Last = [New](o_node.LastChild)
  End Function
  
  '=======Xml元素方法======

  '查找是否有某属性
  Public Function HasAttr(ByVal s)
    If Not IsNode Then HasAttr = False : Exit Function
    Dim oattr
    Set oattr = o_node.Attributes.GetNamedItem(s)
    HasAttr = Not oattr Is Nothing
    Set oattr = Nothing
  End Function
  '是否有子节点
  Public Function HasChild()
    If Not IsNode Then HasChild = False : Exit Function
    HasChild = o_node.hasChildNodes()
  End Function
  '查找子元素
  Public Function Find(ByVal t)
    If Not IsNode Then Exit Function
    Dim o
    If POP_MVC.String.reg_test(t, "[, >\[@:]","i") Then
      '按简单表达式取元素
      Set o = o_node.selectNodes(Popasp_Xml_TransToXpath(t))
    Else
      '从标签取元素
      Set o = o_node.GetElementsByTagName(t)
    End If
    If o.Length = 0 Then
      If Not is_empty( POP_MVC.config("APP_DEBUG") ) Then
		POP_MVC.exit( dError("error-xml-findel") )
      End If
      Set Find = Nothing
    ElseIf o.Length = 1 Then
      Set Find = [New](o(0))
    Else
      Set Find = [New](o)
    End If
  End Function
  'XPath取对象集合
  Public Function [Select](ByVal p)
    If Not IsNode Then Exit Function
    Set [Select] = [New](o_node.selectNodes(p))
  End Function
  'XPath取单个对象
  Public Function Sel(ByVal p)
    If Not IsNode Then Exit Function
    Set Sel = [New](o_node.selectSingleNode(p))
  End Function

  '建立克隆节点
  Public Function Clone(ByVal b)
    If Not IsNode Then Exit Function
    If is_empty(b) Then b = True
    Set Clone = [New](o_node.CloneNode(b))
  End Function
  '统一对象为Dom节点
  Private Function GetNodeDom(ByVal o)
    Select Case TypeName(o)
      Case "IXMLDOMElement" Set GetNodeDom = o
      Case "POPASP_XMLNODE" Set GetNodeDom = o.Dom
    End Select
  End Function
  '添加子节点
  Public Function Append(ByVal o)
    If Not IsNode Then Exit Function
    o_node.AppendChild(GetNodeDom(o))
    Set Append = [New](o_node)
  End Function
  '替换节点
  Public Function ReplaceWith(ByVal o)
    If IsNode Then
      '如果是节点则直接替换（是Dom内节点会直接移动），返回被替换的节点
      Call o_node.ParentNode.replaceChild(GetNodeDom(o), o_node)
    ElseIf IsNodes Then
      '如果是集合则依次替换，是Dom内的节点不会移动而是复制
      Dim i,n
      For i = 0 To Length - 1
        Set n = GetNodeDom(o).CloneNode(True)
        Call o_node(i).ParentNode.replaceChild(n, o_node(i))
      Next
    End If
    Set ReplaceWith = [New](o_node)
  End Function
  '在节点前加入另一个节点
  Public Function Before(ByVal o)
    If IsNode Then
      Call o_node.ParentNode.InsertBefore(GetNodeDom(o), o_node)
    ElseIf IsNodes Then
      Dim i,n
      For i = 0 To Length - 1
        Set n = GetNodeDom(o).CloneNode(True)
        Call o_node(i).ParentNode.InsertBefore(n, o_node(i))
      Next
    End If
    Set Before = [New](o_node)
  End Function
  '在节点后加入另一个节点
  Public Function After(ByVal o)
    If IsNode Then
      Call InsertAfter(GetNodeDom(o), o_node)
    ElseIf IsNodes Then
      Dim i,n
      For i = 0 To Length - 1
        Set n = GetNodeDom(o).CloneNode(True)
        Call InsertAfter(n, o_node(i))
      Next
    End If
    Set After = [New](o_node)
  End Function
  Private Sub InsertAfter(ByVal n, Byval o)
    Dim p
    Set p = o.ParentNode
    If p.LastChild Is o Then
      p.AppendChild(n)
    Else
      Call p.InsertBefore(n, o.nextSibling)
    End If
  End Sub

  '删除节点某属性
  Public Function RemoveAttr(ByVal s)
    If IsNode Then
      o_node.removeAttribute(s)
    ElseIf IsNodes Then
      Dim i
      For i = 0 To Length - 1
        o_node(i).removeAttribute(s)
      Next
    End If
    Set RemoveAttr = [New](o_node)
  End Function
  '清空所有子节点
  Public Function [Empty]
    If IsNode Then
      o_node.Text = ""
    ElseIf IsNodes Then
      Dim i
      For i = 0 To Length - 1
        o_node(i).Text = ""
      Next
    End If
    Set [Empty] = [New](o_node)
  End Function
  '清除所有子节点，包括空文本节点
  Public Function Clear
    If IsNode Then
      o_node.Text = ""
      o_node.removeChild(o_node.FirstChild)
    ElseIf IsNodes Then
      Dim i
      For i = 0 To Length - 1
        o_node(i).Text = ""
        o_node(i).removeChild(o_node(i).FirstChild)
      Next
    End If
    Set Clear = [New](o_node)
  End Function
  '合并相邻的Text节点并删除空的Text节点
  Public Function Normalize
    If IsNode Then
      o_node.normalize()
    ElseIf IsNodes Then
      Dim i
      For i = 0 To Length - 1
        o_node(i).normalize()
      Next
    End If
    Set Normalize = [New](o_node)
  End Function
  '删除自身
  Public Sub Remove
    If IsNode Then
      o_node.ParentNode.RemoveChild(o_node)
    ElseIf IsNodes Then
      Dim i
      For i = 0 To Length - 1
        o_node(i).ParentNode.RemoveChild(o_node(i))
      Next
    End If
  End Sub
Function Popasp_Xml_TransToXpath(ByVal s)
  s = POP_MVC.String.reg_replace(s, "\s*,\s*", "|//","igm")
  s = POP_MVC.String.reg_replace(s, "\s*>\s*", "/","igm")
  s = POP_MVC.String.reg_replace(s, "\s+", "//","igm")
  s = POP_MVC.String.reg_replace(s, "(\[)([a-zA-Z]+\])", "$1@$2","igm")
  s = POP_MVC.String.reg_replace(s, "(\[)([a-zA-Z]+[!]?=[^\]]+\])", "$1@$2","igm")
  s = POP_MVC.String.reg_replace(s, "(?!\[\d)\]\[", " and ","igm")
  s = Replace(s, "|", " | ")
  Popasp_Xml_TransToXpath = "//" & s
End Function
End Class
%>