<%
' +----------------------------------------------------------------------
' | POPASP [ ASP MVC ]
' +----------------------------------------------------------------------
' | Copyright (c) 2016 http://popasp.com All rights reserved.
' +----------------------------------------------------------------------
' | Author: popasp <1737025626@qq.com>
' +----------------------------------------------------------------------

'借用XML文件打包和解包
Class POPASP_XMLPACk
	Private s_PackPath,s_RootPath,s_RootPathLen
	Private s_XmlDoc
	Private Conn,rs,stream
	
	'类的销毁
	Private Sub Class_Terminate
		if isObject( s_XmlDoc ) then
			set s_XmlDoc = nothing
		end if
	End Sub

	'赋值数据库路径
	Public Property Let PackPath( value )
		s_PackPath = value
		s_PackPath = POP_MVC.rtrim( s_PackPath , "\" )
		s_PackPath = POP_MVC.rtrim( s_PackPath , "/" )
		s_PackPath = POP_MVC.realPath( s_PackPath )
	End Property
	
	'得到数据库路径
	Public Property Get PackPath(  )
		PackPath = s_PackPath		
	End Property
	
	'赋值打包文件的根路径
	Public Property Let RootPath( value )
		s_RootPath = value
		s_RootPath = POP_MVC.rtrim( s_RootPath , "\" )
		s_RootPath = POP_MVC.rtrim( s_RootPath , "/" )
		s_RootPath = POP_MVC.realPath( s_RootPath )
		s_RootPathLen = Len( s_RootPath ) + 1
	End Property
	
	'得到打包文件的根路径
	Public Property Get RootPath(  )
		RootPath = s_RootPath		
	End Property
	
	'创建一个空的XML文件，为写入文件作准备
	Public Property Get CreateXml(FilePath)
		'程序开始执行时间
		PackPath = FilePath
		startime=timer()
		dim XmlDoc,Root
		Set XmlDoc = Server.CreateObject("Microsoft.XMLDOM")
			XmlDoc.async = False
			Set Root = XmlDoc.createProcessingInstruction("xml","version='1.0' encoding='UTF-8'")
			XmlDoc.appendChild(Root)
			XmlDoc.appendChild(XmlDoc.CreateElement("root"))
			XmlDoc.Save(POP_MVC.realPath(FilePath))
			Set Root = Nothing
		Set XmlDoc = Nothing
		'程序结束时间
		endtime=timer()
		'response.Write("页面执行时间：" & FormatNumber((endtime-startime),3) & "秒")
	end Property
	
	Public Property Get SaveXml
		s_XmlDoc.Save(POP_MVC.realPath(s_PackPath))
		SaveXml = s_PackPath
	end Property
	
	'打包
	'添加就会打包
	Public Property Get Add(ByVal thePath)
		on error resume next
		
		Server.ScriptTimeOut = 1440	
		if isEmpty( s_XmlDoc ) then			
			Set s_XmlDoc = Server.CreateObject("Microsoft.XMLDOM")
			s_XmlDoc.load(s_PackPath)
		end if
		Set stream = Server.CreateObject("ADODB.Stream")
		stream.Open
		stream.Type = 1
		thePath = POP_MVC.realPath( thePath )
		fsoTreeForXml thePath, stream
		stream.Close
		Set stream = Nothing
	End Property
	
	Function CreateNode( nodeName )
		set CreateNode = s_XmlDoc.CreateElement(nodeName)
	End Function

	'节点 <file><path></path><editTime></editTime><stream></stream></file>
	Sub fsoTreeForXml( ByVal thePath,stream)
		dim Xpath,Xstream,Xdate,xfile
		if POP_MVC.file.isFile( thePath ) then
			if LCase(thePath) <> LCase(s_PackPath) then
				set xfile  = s_XmlDoc.SelectSingleNode("//root").AppendChild(CreateNode("file"))
				Set Xpath = Xfile.AppendChild(CreateNode("path"))
				Xpath.text = mid(thePath,s_RootPathLen)		
				Set Xdate = Xfile.AppendChild(CreateNode("editTime"))
				Xdate.text = POP_MVC.file.mtime( thePath )
				
				Set Xstream = Xfile.AppendChild(CreateNode("stream"))
				Xstream.SetAttribute "xmlns:dt","urn:schemas-microsoft-com:datatypes"
				'文件内容采用二制方式存放
				Xstream.dataType = "bin.base64"
				
				stream.LoadFromFile(thePath)
				Xstream.nodeTypedValue = stream.Read()
				set Xpath = nothing
				set Xdate = nothing
				set Xstream = nothing
				Set Xfile = Nothing
			end if
		elseif POP_MVC.file.isFolder( thePath ) then
			dim objFolder,objFiles,i,fileList
			set objFolder=POP_MVC.fso.GetFolder( thePath )
			set objFiles=objFolder.Files
			for each file in objFiles
				call fsoTreeForXml( file.path,stream )
			next 
			set objFiles=nothing				
			Set objFiles = objFolder.SubFolders	  
			For Each file In objFiles		
				call fsoTreeForXml( file.path,stream )
			Next
			set objFiles=nothing	
			set objFolder=nothing
		end if
	End Sub

	Function getNodeValue( node , TagName )
		dim o
		set o = node.GetElementsByTagName(TagName)
		If o.Length = 0 Then
		  getNodeValue = null
		Else
		  getNodeValue = o(0).text
		End If
	End Function
	
	'解包
	'thePath为解包数据库路径
	Public Function unPack(ByVal thePath , ByVal DirPrefix)
		Server.ScriptTimeOut = 1440

		if isNul( thePath ) then
			thePath = s_PackPath
		end if
		Dim XmlDoc, stream, theFolder,objStream
		dim i
		Set stream = Server.CreateObject("ADODB.Stream")
		stream.Open
		stream.Type = 1
		
		Set XmlDoc = Server.CreateObject("Microsoft.XMLDOM")
		XmlDoc.load POP_MVC.realPath(thePath)
		
		DirPrefix = POP_MVC.rtrim( POP_MVC.rtrim( DirPrefix , "/" ) , "\" )

		If XmlDoc.readyState=4 Then
			If XmlDoc.parseError.errorCode = 0 Then		
				Set objNodeList = XmlDoc.documentElement.selectNodes("//root/file")
				
				For i=0 To objNodeList.length-1
					set node = objNodeList.item(i)
					Call stream2file( node.getElementsByTagName("stream")(0).nodeTypedvalue , POP_MVC.RealPath( DirPrefix & getNodeValue(node,"path")) )
					set node = nothing
				Next
				Set objNodeList = Nothing
			end if
		end if

		stream.Close
		Set stream = Nothing
	End Function
	
	'解包
	'thePath为解包数据库路径
	Public Function cmsUnPack(ByVal thePath , ByVal DirPrefix , ByRef callback )
		Server.ScriptTimeOut = 1440
		
		if isNul( thePath ) then
			thePath = s_PackPath
		end if
		Dim XmlDoc, stream, theFolder,objStream
		dim savePath,theLen,adminName,theAdmin,bool
		dim i
		Set stream = Server.CreateObject("ADODB.Stream")
		stream.Open
		stream.Type = 1
		
		Set XmlDoc = Server.CreateObject("Microsoft.XMLDOM")
		XmlDoc.load POP_MVC.realPath(thePath)
		

		
		adminName = "core"
		theAdmin  = coreName
		theLen = len(adminName) + 2
		if isNul( DirPrefix ) then
			DirPrefix = POP_MVC.config("sitePath")
		end if
		
		DirPrefix = POP_MVC.rtrim( POP_MVC.rtrim( DirPrefix , "/" ) , "\" )
		
		If XmlDoc.readyState=4 Then
			If XmlDoc.parseError.errorCode = 0 Then		
				Set objNodeList = XmlDoc.documentElement.selectNodes("//root/file")
				
				For i=0 To objNodeList.length - 1
					set node = objNodeList.item(i)
					
					savePath = getNodeValue(node,"path")	
					
					'获取路径
					theFolder = POP_MVC.file.dir( DirPrefix & savePath )
					
					if left( savePath , theLen ) = "\" & adminName & "\" then
						savePath = theAdmin & "\" & mid( savePath , theLen + 1 )
					end if

					Execute "bool = " & callback & "( savePath , node )"
					
					if bool then
						response.write "<center>" & (i+1) & ". 正在更新 " & DirPrefix & savePath & "</center>"
						if POP_MVC.CreateFolder(theFolder) then
							Call P_("VERSION").stream2file( node.getElementsByTagName("stream")(0).nodeTypedvalue , POP_MVC.RealPath( DirPrefix & getNodeValue(node,"path")) )
						end if
					end if
					set node = nothing
				Next
				Set objNodeList = Nothing
			end if
		end if

		stream.Close
		Set stream = Nothing
	End Function
End Class
%>