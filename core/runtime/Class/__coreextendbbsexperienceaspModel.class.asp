﻿<%
Class [__coreextendbbsexperienceaspModel]
	private SignExpirenceDays,SignExpirenceArr
	Private Sub Class_Initialize()
		SignExpirenceDays = Array(30,15,5 ,0)
		SignExpirenceArr  = Array(20,15,10,5)
	end sub
	
	'获取下次签到的成长值
	Function getNextSignExperience( ByRef obj )
		dim i,ret
		dim SignTime , SignDays
		
		if not obj.eof then
			SignTime = obj("SignTime")
			SignDays = obj("SignDays")
		end if
		
		'连续签到天数
		if isDate( SignTime ) and isNumeric( SignDays ) then
			if Datediff( "d"  , SignTime, now() ) <= 1 then
				SignDays = SignDays + 1
			else
				SignDays = 1
			end if
		else
			SignDays = 1
		end if
		
		for i = 0 to ubound( SignExpirenceDays )
			if SignDays > SignExpirenceDays(i) then
				ret = SignExpirenceArr( i )
				exit for
			end if
		next
		getNextSignExperience = ret
	End Function
	
	'获取本次签到的成长值
	Function getSignExperience( ByRef obj )
		dim i,ret
		dim SignTime , SignDays
		
		if not obj.eof then 
			SignTime = obj("SignTime")
			SignDays = obj("SignDays")
		end if
		
		'连续签到天数
		if isDate( SignTime ) and isNumeric( SignDays ) then
			if Datediff( "d"  , SignTime, now() ) = 1 then
				SignDays = SignDays + 1
			else
				SignDays = 1
			end if
		else
			SignDays = 1
		end if
		
		for i = 0 to ubound( SignExpirenceDays )
			if SignDays > SignExpirenceDays(i) then
				ret = SignExpirenceArr( i )
				exit for
			end if
		next
		getSignExperience = ret
	End Function
	
	'签到
	Function SignIn( ByRef obj )
		dim i,ret
		dim SignTime , SignDays, SignCount, SignExperience
		
		if not obj.eof then
			SignTime = obj("SignTime")
			SignDays = obj("SignDays")
			SignCount = obj("SignCount")
			SignExperience = obj("SignExperience")
		end if
		
		'连续签到天数
		if isDate( SignTime ) and isNumeric( SignDays ) then
			if Datediff( "d"  , SignTime, now() ) = 1 then
				SignDays = SignDays + 1
			else
				SignDays = 1
			end if
		else
			SignDays = 1
		end if
		
		'总签到次数
		if isNul( SignCount ) then
			SignCount = 1
		else
			SignCount = SignCount + 1
		end if
		
		for i = 0 to ubound( SignExpirenceDays )
			if SignDays > SignExpirenceDays(i) then
				SignExperience = SignExpirenceArr( i )
				exit for
			end if
		next		

		set ret = D_
		
		ret("SignTime") = Now()
		ret("SignDays") = SignDays
		ret("SignCount") = SignCount
		ret("SignExperience") = SignExperience
		set SignIn = ret
	end Function
End Class
%>