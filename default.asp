<!--#include file="./core/popasp/popasp.asp"-->
<%
'为了安全考虑，核心路径最好修改成别人不容易猜出的名称
'将上面两行include包含语句中的 core 与下面的coreName 同时进行修改
'比如将 core 改成 core123
private coreName,sitePath,templateName_,objXML
coreName = "core" 'coreName
'coreName = "core123"

'如果给该文件重命名，系统将无法正常运行

'指定框架路径
POP_MVC.mvc_dir = coreName & "/popasp/"
POP_MVC.appPath = coreName
POP_MVC.config("EXTEND_PATH") = coreName & "/Extend"
POP_MVC.config("PLUGIN_PATH") = coreName & "/"
POP_MVC.config("CORE_NAME") = coreName

POP_MVC.config("XML_CONFIG_PATH") = coreName & "/runtime/Xml/Config.xml"

'默认模块
POP_MVC.config("DEFAULT_MODULE") = "bbs--index"

'内存缓存
'application.contents.removeAll

'POP_MVC.applicationOn = 1

'额外配置
if POP_MVC.config("EXT_CONFIG") = "" then POP_MVC.config("EXT_CONFIG") = "./#config.asp"
POP_MVC.init


dim config,objCMS,that,guestConfig

sitePath = POP_MVC.get_site_path
POP_MVC.config("sitePath") = sitePath

if sitePath <> "" then
	POP_MVC.config("COOKIE_PATH") = sitePath 
end if


'得到配置对象guestConfig
if POP_MVC.file.isFile( C_("XML_CONFIG_PATH") ) then	
	set objXML = new POPASP_XMLDB4BBS
	objXML.db_path = POP_MVC.config("XML_CONFIG_PATH")
	set guestConfig = objXML.init.getObject( "Line" , "K,V" )
	set objXML = nothing
else
	set guestConfig = B_( "User" ).from("self_GuestConfig").field("ConfigName as K,ConfigValue as V").getObject2
	Call B_( "User" ).from("self_GuestConfig").field("ConfigName as K,ConfigValue as V").SaveXmlFile( "Line" , C_("XML_CONFIG_PATH") )
end if

'如果网站关闭，则退出
if guestConfig.SiteStatus <> "1" then
	if NOT ( POP_MVC.get("c") = "Site" and POP_MVC.get("MailAlert") <> "" )  and POP_MVC.file.isFile( POP_MVC.config("CORE_NAME") & "/closebbs.txt"  ) then
		K_("bbs--public").CloseBBS
	end if
end if

if guestConfig.TemplateSwitch - 0 = 1 then
	if Request.Cookies("SystemTemplate") <> "" and inStr( "," & guestConfig.SiteTplArr & "," , Request.Cookies("SystemTemplate") ) > 0 then
		templateName_= Request.Cookies("SystemTemplate")
		guestConfig.DefaultTemplate = templateName_
		Execute POP_MVC.file_get_contents( "./Templates/" & guestConfig.DefaultTemplate & "/#data/config.txt" )
	end if
end if

set that = new POPASP_CONTROLLER

'水印
POP_MVC.config("WATERMARK_PATH") = "ASPBBS"

'set objCMS = P_( C_("CMS_CLASS_NAME") )	
set objCMS = new POPASP_ASPBBS
objCMS.baseTable = "User"
POP_MVC.isExecuteExtends = false
POP_MVC.config("TMPL_CACHE_LIFETIME") = -1
POP_MVC.tpl_vars("AddSortID") = ""

''''''''''''''''''''''''''''''''''''''''''下面是一些共用方法
private SortNameArr,SortIdArr
private HonorTitleArr,HonorLevelArr,HonorExperienceArr
SortNameArr = guestConfig.SortUserName & "," & guestConfig.SortAdminName & "," & guestConfig.SortHideName
SortIdArr   = guestConfig.SortUserID & "," & guestConfig.SortAdminID & "," & guestConfig.SortHideID

SortNameArr = split( SortNameArr, "," )
SortIdArr   = split( SortIdArr, "," )

Function getLoginUrl()
	getLoginUrl = "?Login" & guestConfig.PageSuffix
End Function

Function getSortName( SortID )
	dim pos,a,page,c
	

	
	c = LCase(POP_MVC.get("c"))
	a = LCase(POP_MVC.get("a"))
	page = POP_MVC.get("page")


	if isnul(SortID) or SortID = 0 then
				'如果控制器分配了页面名称，则使用之
		if c = "thread" & POP_MVC.tpl_vars.exists("PageName") then
			getSortName = POP_MVC.tpl_vars("PageName")
			exit Function
		else		
			if a = "index" then
				getSortName = "首页"
			else	
				getSortName = "全部帖子列表"
			end if

			exit Function		
		end if
	end if
	
	pos = POP_MVC.Arr.iSearch( SortIdArr, SortID )
	
	

	if pos > -1 then
		getSortName = SortNameArr( pos )
	else
		getSortName = "未分类"
	end if
End Function


HonorTitleArr 		= guestConfig.HonorTitle
HonorLevelArr   	= guestConfig.HonorLevel
HonorExperienceArr	= guestConfig.HonorExperience

HonorTitleArr		= split( HonorTitleArr, "," )
HonorLevelArr		= split( HonorLevelArr, "," )
HonorExperienceArr	= split( HonorExperienceArr, "," )

'得到头衔名称
Function getHonorTitle( val )
	dim pos,i
	if val = "" then
		exit Function
	end if
	for i = 0 to ubound( HonorExperienceArr )
		if val - HonorExperienceArr(i) > 0 then
			pos = i
			exit for
		end if
	next	
	if pos = "" then pos = i
	getHonorTitle = HonorTitleArr( pos )	
End Function

'得到头衔级别
Function getHonorLevel( val )
	dim pos,i
	if val = "" then
		exit Function
	end if
	for i = 0 to ubound( HonorExperienceArr )
		if val - HonorExperienceArr(i) > 0 then
			pos = i
			exit for
		end if
	next
	if pos = "" then pos = i
	getHonorLevel = HonorLevelArr( pos )
End Function

'获取分页头
Function getPageHeader()
	dim ctrl,action,id
	ctrl = LCase( POP_MVC.c )
	action = LCase( POP_MVC.a )
	id = POP_MVC.get("id")
	if id= "" then
		id = 0
	end if
	
	select case ctrl
		case "bbs--index"
			getPageHeader = objCMS.linkPrefix & "index"
		case "bbs--thread"
			if action = "index" then
				if id = 0 then
					getPageHeader = objCMS.linkPrefix & "index"
				else
					getPageHeader = objCMS.linkPrefix & "list_" & POP_MVC.get("id")
				end if
			elseif action = "unsolved" or action = "solved" or action = "feature" or action = "orderid" or action = "orderreply" then	
				if id = 0 then
					getPageHeader = objCMS.linkPrefix & action
				else
					getPageHeader = objCMS.linkPrefix & "thread_" & action & "_" & id
				end if
			elseif action = "recycle" then	
				getPageHeader = objCMS.linkPrefix & action
			elseif action = "toplist" then	
				getPageHeader = objCMS.linkPrefix & action
			elseif  action = "detail" then
				if isNul(guestConfig.ThreadUrlMode) then
					getPageHeader = objCMS.linkPrefix
				else
					getPageHeader = objCMS.linkPrefix & guestConfig.ThreadUrlMode & "_"
				end if
			else
				getPageHeader = objCMS.linkPrefix & "thread_" & action
			end if
		case "bbs--case"
			if POP_MVC.a = "index" then
				getPageHeader = objCMS.linkPrefix & "case"
			else
				getPageHeader = objCMS.linkPrefix & "case" & "_" & POP_MVC.a
			end if
		case else
			getPageHeader = objCMS.linkPrefix & POP_MVC.c & "_" & POP_MVC.a
	end select
End Function

'获取分页头
Function getFilterHeader( action )
	dim ctrl,id
	ctrl = LCase( POP_MVC.c )
	id = POP_MVC.get("id")
	if id= "" then
		id = 0
	end if
	
	select case ctrl
		case "bbs--index"
			getFilterHeader = objCMS.linkPrefix & action
		case "bbs--thread"
			if id = 0 then
				getFilterHeader = objCMS.linkPrefix & action
			else
				getFilterHeader = objCMS.linkPrefix & "thread_" & action & "_" & id
			end if
		case else
			getFilterHeader = objCMS.linkPrefix & POP_MVC.c & "_" & POP_MVC.a
	end select
	getFilterHeader = getFilterHeader & guestConfig.PageSuffix
End Function

'帖子链接
Function getThreadLink( id )
	if isNul(guestConfig.ThreadUrlMode)then
		getThreadLink = objCMS.linkPrefix & id & guestConfig.PageSuffix
	else
		getThreadLink = objCMS.linkPrefix & guestConfig.ThreadUrlMode & "_" & id & guestConfig.PageSuffix
	end if
End Function

'用户主页链接
Function getUserULink( id )
	getUserULink = objCMS.linkPrefix & "u_" & id
End Function



'登陆页面的链接
Function getLoginLink()
	getLoginLink = objCMS.linkPrefix & "login" & guestConfig.PageSuffix
End Function

'登陆链接
Function getLogoutLink
	getLogoutLink = objCMS.linkPrefix & "logout" & guestConfig.PageSuffix
End Function

'注册页面的链接
Function getRegLink()
	getRegLink = objCMS.linkPrefix & "reg" & guestConfig.PageSuffix
End Function

'发表新帖链接
Function getPostLink()	
	getPostLink	= objCMS.linkPrefix & "Topic_Add"
	if POP_MVC.tpl_vars("AddSortID") <> "" then
		getPostLink	= getPostLink & "_" & POP_MVC.tpl_vars("AddSortID")
	end if
	getPostLink = getPostLink & guestConfig.PageSuffix
End Function

'栏目链接
Function getSortsLink( id )
	getSortsLink = objCMS.linkPrefix & "list_" & id & guestConfig.PageSuffix
End Function
''''''''''''''''''''''''''''''''''''''''''上面是一些共用方法




if guestConfig.DefaultTemplate = "xiunobbs" then '操作设置
	that.AjaxArgs("info") = "message"
	that.AjaxArgs("status") = "code"
	that.ajaxSuccessStatus = 0
	that.ajaxErrorStatus = -1
else
	that.AjaxArgs("info") = "msg"
	that.ajaxSuccessStatus = 0
	that.ajaxErrorStatus = 1
end if


'上传目录设置
POP_MVC.config("UPLOAD_SAVE_PATH") =  POP_MVC.config("CMS_UPLOAD_PATH") & "/" & S_("adminId") & "/"

'案例的分类ID值
'POP_MVC.config("BBS_CASE_SORTID") = 58

POP_MVC.config("APPLICATION_PREFIX") = POP_MVC.get_site_path()

POP_MVC.config("LoginNameTip") = "用户名须由 " & guestConfig.RegNameLenMin & "到" & guestConfig.RegNameLenMax & "个" & guestConfig.RegNameType & " 组成，且不能包含敏感字词!"
POP_MVC.config("PasswordTip")  = "密码应为 " & guestConfig.RegPassLenMin & "到" & guestConfig.RegPassLenMax & " 位，" & guestConfig.RegPassType

POP_MVC.config("THREAD_FIELD") = "a.TopicID,a.ZanCount,a.SortID,a.AddTime,a.IndexImage,a.DownURL,a.Title,a.IpAddr,a.EditIpAddr,a.ContentSource,a.ContentStatus,a.IsTop,a.Isrecommend,a.IsHeadline,a.IsFeatured,a.IsClosed,a.IsReplyView,a.Visits,a.IsNoComment,a.Content,a.ReplyCount,a.AnswerReward,a.EditTime,a.EditCount,a.IsSolved,a.PageDesc,a.PageKeywords,c.LoginName,c.Sign,c.AccountCash,c.Nickname,c.GroupID,c.Avatar,c.UserID,c.Experience,c.ThreadReplyView,b.GroupName"

POP_MVC.config("REPLY_FIELD") = "a.ReplyID,a.FloorStep,a.TopicID,a.IpAddr,a.ContentStatus,a.Content,a.IsAdopted,a.Reward,a.ZanCount,a.CaiCount,a.AddTime,b.UserID,b.Online,b.Avatar,b.Gender,b.Experience,b.LoginName,b.Nickname,b.UserStatus,c.GroupID,c.IsAdmin"

if POP_MVC.config("DB_TYPE") = "access" then
	POP_MVC.config("THREAD_FIELD1") =  "UserID as uid,SignDays as days,SignTime as [time]"
	POP_MVC.config("THREAD_FIELD2") =  "UserID as uid,SignCount as days,SignTime as [time]"
	POP_MVC.config("SIGN_FIELD1")   =   ""
else
	POP_MVC.config("THREAD_FIELD1") =  "UserID as uid,SignDays as days,SignTime as [time]"
	POP_MVC.config("THREAD_FIELD2") =  "UserID as uid,SignCount as days,SignTime as `time`"
end if

POP_MVC.config("THREAD_TABLE") = "{prefix}self_GuestTopic as a,{prefix}UserGroup as b,{prefix}User as c"
POP_MVC.config("REPLY_TABLE") = "{prefix}self_GuestReply as a,{prefix}User as b,{prefix}UserGroup as c"


that.d("ExtendLinks") = split( repNull(guestConfig.ExtendLinks) , ";" )

if guestConfig.DefaultTemplate <> "bbs" then
	POP_MVC.config("TMPL_ACTION_SUCCESS") = "./Templates/" & templateName_ & "/html/" & templateName_ & "/success.html"
	POP_MVC.config("TMPL_ACTION_ERROR") = "./Templates/" & templateName_ & "/html/" & templateName_ & "/error.html"
end if
	
POP_MVC.start
%>