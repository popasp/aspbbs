

//随机词汇表
var arr=["新春快乐","阖家快乐","恭贺新禧","万事如意","张灯结彩","恭喜发财","假期愉快","今晚吃鸡","初中资料","教育盘网","网课资源"];
var dic = "一乙二十丁厂七卜人入八九几儿了力乃刀又三于干亏士工土才寸下大丈与万上小口巾山千乞川亿个勺久凡及夕丸么广亡门义之尸弓己已子卫也女飞刃习叉马乡丰王井开夫天无元专云扎艺木五支厅不太犬区历尤友匹车巨牙屯比互切瓦止少日中冈贝内水见午牛手毛气升长仁什片仆化仇币仍仅斤爪反介父从今凶分乏公仓月氏勿欠风丹匀乌凤勾文六方火为斗忆订计户认心尺引丑巴孔队办以允予劝双书幻玉刊示末未击打巧正扑扒功扔去甘世古节本术可丙左厉右石布龙平灭轧东卡北占业旧帅归且旦目叶甲申叮电号田由史只央兄叼叫另叨叹四生失禾丘付仗代仙们仪白仔他斥瓜乎丛令用甩印乐句匆册犯外处冬鸟务包饥主市立闪兰半汁汇头汉宁穴它讨写让礼训必议讯记永司尼民出辽奶奴加召皮边发孕圣对台矛纠母幼丝式刑动扛寺吉扣考托老执巩圾扩扫地扬场耳共芒亚芝朽朴机权过臣再协西压厌在有百存而页匠夸夺灰达列死成夹轨邪划迈毕至此贞师尘尖劣光当早吐吓虫曲团同吊吃因吸吗屿帆岁回岂刚则肉网年朱先丢舌竹迁乔伟传乒乓休伍伏优伐延件任伤价份华仰限妹姑姐姓始驾参艰线练组细驶织终驻驼绍经贯奏春帮珍玻毒型挂封帘实试郎诗肩房诚衬衫视话诞询该详建肃录隶居届刷屈弦承孟孤陕降河沾泪油泊沿泡注泻泳泥沸波泼泽治怖性怕怜怪学宝宗定宜审宙官空京享店夜庙府底剂郊废净盲放刻育闸闹郑券卷单炒炊炕炎炉沫浅法泄斧爸采受乳贪念贫肤肺肢肿胀朋股肥服胁周昏鱼兔狐忽狗备饰饱饲变"
//创建随机成语
function start(){
	$('#box').html('<div class="bg-blur"></div>')
	$('#minbox').html("");
	var math=arr[Math.floor(Math.random()*(arr.length))];

$('#minbox').html(`请依次点击: <span>${math}</span>`)
var timer; 

//创建一个位置数组
var place=[{left:'130px',top:'0px'},{left:'170px',top:'20px'},{left:'110px',top:'200px'},{left:'220px',top:'280px'},{left:'40px',top:'260px'},{left:'120px',top:'50px'},{left:'140px',top:'150px'},{left:'40px',top:'110px'},{left:'160px',top:'120px'}]

// var left=Math.floor(Math.random()*(boxs[0].offsetWidth-51)) ;
// 			var top=Math.floor(Math.random()*(boxs[0].offsetHeight-51));

//随机打乱位置数组
place.sort(()=>{
	return Math.random()-0.5
})
console.log(place)

//创建一个数组用于与最终结果验证
var verify=[];
var extra_str="";
for(var i = 0; i<5;i++){
	extra_str += dic[Math.floor(Math.random()*(dic.length))]
}
extra_str = math + extra_str;
//遍历随机成语并创建标签
for(i in extra_str){
	
	verify.push(i)
	//创建随机左边位置
	var left=Math.floor(Math.random()*(i*20))
	var top=Math.floor(Math.random()*(i*5));
	
	//创建存放span的div对象
	divs=$('<div class="fl"></div>')
	//给div定位
	divs.css({
		left:place[i].left,
		top:place[i].top
	})
	
	
	//创建储存文字的span标签
	span=$(`<span>${extra_str[i]}</span>`)
	//随机span的位置
	span.css({
		left:left+'px',
		top:top+'px'
	})
	
	span.data('index',i);
	span.data('judge','true');
	divs.append(span);
	$('#box').append(divs)
	
}

//span点击事件
var cspan=[]
$('#box .fl span').click(function(){
	if($(this).data('judge')=='true'){
		cspan.push($(this).data('index'))
		$(this).data('judge','false')
		console.log(cspan)
		
	}else{
		console.log('重复点击')
	}
	
	
})

var a=0
//大盒子点击事件,用于生成小绿点
$('#box').unbind("click");
$('#box').click(function(event){
	a++
	var rad=$(`<div class='radio'>${a}</div>`)
	rad.css({
		left:event.pageX-$(this).offset().left-15+'px',
		top:event.pageY-$(this).offset().top-15+'px'
	})
	
	$(this).append(rad)
	if(a==4){
		if(cspan.join()==verify.slice(0,4).join()){
			$('#minbox').addClass('size');
			$('#minbox').html('验证成功');
			$('.fl').css('display','none');
			$('.radio').css('opacity','0');
			// setInterval(()=>{
			// 	location.reload()
			// },2000)
		}else{
			$('.fl').css('display','none');
			$('.radio').css('opacity','0');
			$('#minbox').html('验证失败');
			$('#minbox').css('color','red');
			clearInterval(timer);
			timer =setTimeout(() => {
				start()
			},1000)
		}
		a=0
	}
	
	
	
	
})
}
start()