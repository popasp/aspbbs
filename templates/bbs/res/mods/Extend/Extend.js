
/*!
 * 扩展组件平台
 */
 
layui.define(['fly', 'element', 'carousel','dropdown'], function(exports){

  var $ = layui.jquery;
  var layer = layui.layer;
  var util = layui.util;
  var laytpl = layui.laytpl;
  var form = layui.form;
  var laypage = layui.laypage;
  var fly = layui.fly;
  
  var element = layui.element;
  var upload = layui.upload;

  var carousel = layui.carousel;
  var dropdown = layui.dropdown;

  var THIS = 'layui-this';
  
  var gather = {};


  $('.jie-admin').on('click', 'span', function(){
    var othis = $(this), type = othis.attr('type');
    gather.jieAdmin[type] && gather.jieAdmin[type].call(this, othis.parent());
  });
  
  
  //求解管理
  gather.jieAdmin = {
    //查看
    view: function(div){
      var id=div.data('id');
	  location.href= "?extend--User_View_" + id;
    }	  
	  
	  
    //删除
    ,del: function(div){
      layer.confirm('确认删除吗？', function(index){
        layer.close(index);
        fly.json('?extend--Admin_Remove', {
          id: div.data('id')
        }, function(res){
          if(res.status === 0){
            location.reload();
          } else {
            layer.msg(res.msg);
          }
        });
      });
    }
	
    //审核
    ,accept: function(div){
        fly.json('?extend--Admin_Accept', {
          id: div.data('id')
        }, function(res){
          if(res.status === 0){
            location.reload();
          } else {
            layer.msg(res.msg);
          }
        });
    }
	
    //拒绝
    ,refuse: function(div){
      layer.confirm('确认要拒绝？', function(index){
        layer.close(index);
        fly.json('?extend--Admin_Refuse', {
          id: div.data('id')
        }, function(res){
          if(res.status === 0){
            location.reload();
          } else {
            layer.msg(res.msg);
          }
        });
      });
    }
	
    //标记为未审
    ,uncheck: function(div){
      layer.confirm('确认要标记为未审吗？', function(index){
        layer.close(index);
        fly.json('?extend--Admin_Uncheck', {
          id: div.data('id')
        }, function(res){
          if(res.status === 0){
            location.reload();
          } else {
            layer.msg(res.msg);
          }
        });
      });
    }
	
    //彻底删除
    ,remove: function(div){
      layer.confirm('确认要永久删除吗？', function(index){
        layer.close(index);
        fly.json('?extend--Admin_Remove2', {
          id: div.data('id')
        }, function(res){
          if(res.status === 0){
            location.reload();
          } else {
            layer.msg(res.msg);
          }
        });
      });
    }
    
    //设置置顶、状态
    ,set: function(div){
      var othis = $(this);
      fly.json('?Topic_setField', {
        id: div.data('id')
        ,rank: othis.attr('rank')
        ,field: othis.attr('field')
      }, function(res){
        if(res.status === 0){
          location.reload();
        }
      });
    }

    //收藏
    ,collect: function(div){
      var othis = $(this), type = othis.data('type');
      fly.json('?Digg_' + type + 'Collection', {
        cid: div.data('id')
      }, function(res){
        if(type === 'add'){
          layer.msg('收藏成功');
          othis.data('type', 'remove').html('取消收藏').addClass('layui-btn-primary');
        } else if(type === 'remove'){
          layer.msg('取消收藏成功');
          othis.data('type', 'add').html('收藏').removeClass('layui-btn-primary');
        }
      });
    }
	
	,zan: function(div){
      var othis = $(this), ok = othis.hasClass('zanok');
      fly.json('?Digg_doZanTopic', {
        ok: ok
        ,cid: div.data('id')
      }, function(res){
        if(res.status === 0){
          var zans = othis.next().find('em').html()|0;
          othis[ok ? 'removeClass' : 'addClass']('zanok');
           othis.next().find('em').html((++zans));
          //othis.find('em').html(ok ? (++zans) : (--zans));
        } else {
          layer.msg(res.msg);
        }
      });
		
		
	}
	
    //查看点赞用户
    ,showPraise: function(div){
      var othis=$(this);
      if(othis.find("em").html() == 0) return layer.tips('该贴子还没有收到赞，您来赞一个吧！', othis, {
        tips: 1
      });
      fly.json('?Digg_listTopicZan', {
        id: div.data('id')
      }, function(res){
        var html = '';
        layer.open({
          type: 1
          ,title: '该贴子获得的赞'
          ,id: 'LAY_showPraise'
          ,shade: 0.8
          ,shadeClose: true
          ,area: '305px'
          ,skin: 'layer-ext-case'
          ,content: function(){
            layui.each(res.data, function(_, item){
              html += '<li><a href="?u_'+ item.id +'" target="_blank" title="'+ item.username +'"><img src="'+ item.avatar +'"></a></li>'
            });
            return '<ul class="layer-ext-ul">' + html + '</ul>';
          }()
        })
      });
    }	

  };

  //banner 轮播
  var elemBanner = $('#FLY-extend-banner');
  carousel.render({
    elem: elemBanner
    ,width: '100%' //设置容器宽度
    ,height: elemBanner.data('height')
    ,arrow: 'none' //始终显示箭头
    ,anim: 'fade' //切换动画方式
    ,interval: 5000
  });


  //验证规则
  form.verify({
    resRequired: [
      /[\S]+/
      ,'请上传组件资源包'
    ]
  });

  //上传组件
  var elemRes = $('#FLY-extend-res')
  upload.render({
    elem: '#FLY-extend-upload'
    ,url: '/api/upload/file'
    ,accept: 'file'
    ,exts: 'zip|rar|7z'
    ,size: 3*1000*2014
    ,done: function(res){
      if(res.status == 0){
        elemRes.val(res.url);
        this.elem.find('p').html(res.filename);
        layer.msg('文件上传成功', {icon: 1});
      } else {
        layer.msg(res.msg, {icon: 5});
      }
    }
  });

  //提交成功后的回调
  fly.form['extendRelease'] = function(field, elem, res){
    layer.alert(res.msg, {
      icon: 1
      ,btnAlign: 'c'
      ,btn: ['朕已知晓']
      ,end: function(){
        location.href = '/user/extend/';
      }
    });
  };


  //文档导航切换
  element.tab({
    headerElem: '.fly-extend-doc-nav>li' //指定tab头元素项
    ,bodyElem: '.fly-extend-doc' //指定tab主体元素项
  });

  //获取hash来切换选项卡，假设当前地址的hash为lay-id对应的值
  var layid = location.hash.replace(/^#/, '');
  layid && $('.fly-extend-doc-nav>li[lay-id="'+ layid +'"]').trigger('click')

  //监听文档导航
  element.on('tab(extend-doc-nav)', function(){
    location.hash = this.getAttribute('lay-id');
  });
  
    if( $('.fly-shortcut li  a.DbSet')[0] ) {
		$('.fly-shortcut li  a.DbSet').on('click' ,function(){
			var url = $(this).data('href') ;
			var tip = $(this).find('cite').html() ;
			layer.confirm('确定要' + tip + '?', function(){
				fly.json( url ,{} , function(res){
					
				  if(res.status === 0){
					layer.alert(res.msg, {
					  icon: 1,
					  time: 0.5 *1000
					})  
				  } else {
					layer.alert(res.msg, {
					  icon: 5,
					  time: 0.5 *1000
					}) 
					  
				  }

				} );
			});
		});
	}
  

  util.fixbar({
    //bar1: '&#xe642;',
    click: function(type){
      if(type === 'bar1'){
        //location.href = '/jie/add/';
      }
    }
  });

  exports('extends', {});
  
});