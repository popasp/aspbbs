/**

 @Name: 案例

 */
 
layui.define(['fly','dropdown'], function(exports){

  var $ = layui.jquery;
  var layer = layui.layer;
  var util = layui.util;
  var form = layui.form;
  var laypage = layui.laypage;
  var upload = layui.upload;
  var fly = layui.fly;
  var device = layui.device();
  var flow = layui.flow;
  var timeoutID = null;  
  var dropdown = layui.dropdown;
  
  flow.lazyimg();
  //案例管理
  var active = {
     //点赞
    praise: function(othis){
      var li = othis.parents('li')
      ,PRIMARY = 'layui-btn-primary'
      ,unpraise = !othis.hasClass(PRIMARY)
      ,numElem = li.find('.fly-case-nums')

      fly.json('?UploadLog--User_doZan', {
        id: li.data('id')
        ,unpraise: unpraise ? true : null
      }, function(res){
        numElem.html(res.praise);
        if(unpraise){
          othis.addClass(PRIMARY).html('点赞');
          layer.tips('少了个赞囖', numElem, {
            tips: 1
          });
        } else {
          othis.removeClass(PRIMARY).html('已赞');
          layer.tips('成功获得个赞', numElem, {
            tips: [1, '#FF5722']
          });
        }
      });
    }

    //查看点赞用户
    ,showPraise: function(othis){
      var li = othis.parents('li');
      if(othis.html() == 0) return layer.tips('该文件还没有收到赞', othis, {
        tips: 1
      });
      fly.json('?UploadLog--User_listZan', {
        id: li.data('id')
      }, function(res){
        var html = '';
        layer.open({
          type: 1
          ,title: '上传文件【'+ res.title + '】获得的赞'
          ,id: 'LAY_showPraise'
          ,shade: 0.8
          ,shadeClose: true
          ,area: '305px'
          ,skin: 'layer-ext-case'
          ,content: function(){
            layui.each(res.data, function(_, item){
              html += '<li><a href="?u_'+ item.id +'" target="_blank" title="'+ item.username +'"><img src="'+ item.avatar +'"></a></li>'
            });
            return '<ul class="layer-ext-ul">' + html + '</ul>';
          }()
        })
      });
    }
	
	,del:function(){
      var othis = $(this)
		  ,li = othis.parents('li');
		layer.confirm('确认删除该文件么？', function(){
			fly.json('?UploadLog--Admin_Remove', {
			  id: li.data('id')
			}, function(res){
			  if(res.status === 0){
				location.reload();
			  } else {
				layer.msg(res.msg);
			  }
			});
		});
	}	
	
  };
  
  
    if( $('.fly-shortcut li  a.DbSet')[0] ) {
		$('.fly-shortcut li  a.DbSet').on('click' ,function(){
			var url = $(this).data('href') ;
			var tip = $(this).find('cite').html() ;
			layer.confirm('确定要' + tip + '?', function(){
				fly.json( url ,{} , function(res){
					
				  if(res.status === 0){
					layer.alert(res.msg, {
					  icon: 1,
					  time: 0.5 *1000
					})  
				  } else {
					layer.alert(res.msg, {
					  icon: 5,
					  time: 0.5 *1000
					}) 
					  
				  }

				} );
			});
		});
	}

	
  if( $('.aspbbs-recycle-dropdown')[0] ) {
	  //自定义事件
	  dropdown.render({
		elem: '.aspbbs-recycle-dropdown'
		,data: [{
		  title: '查看'
		  ,id: 'viewRecycle'
		},{
		  title: '删除'
		  ,id: 'del'
		},{
		  title: '还原'
		  ,id: 'restore'
		}]
		,click: function(data, othis){
		  var id = $(this.elem).parents("li").data("id");
		  //根据 id 做出不同操作
		  if(data.id === 'del'){
			layer.confirm('确定要彻底删除吗？', function(index){
				fly.json('?UploadLog--Admin_Remove2', {
				  id: id
				}, function(res){
				  if(res.status === 0){
					location.reload();
				  } else {
					layer.msg(res.msg);
				  }
				});
			});
		  } else if ( data.id === 'restore' ) {
			layer.confirm('确定要还原该文件吗？', function(index){
				fly.json('?UploadLog--Admin_restore', {
				  id: id
				}, function(res){
				  if(res.status === 0){
					location.reload();
				  } else {
					layer.msg(res.msg);
				  }
				});
			});
		  } else if ( data.id === 'viewRecycle' ) {
			location.href = '?UploadLog--Admin_viewRecycle_' + id;
		  }
		}
	  });
  }

	
	
$("#LAY_tips li .aspbbs-tip")
  .on("mouseenter", function () {
	if ($(this).find('img').length>0) {
		//clearTimeout( timeoutID );
		var othis = $(this).find('img');
		this.index = layer.tips(
		  '<p><img style="width:100%;max-width:600px;max-height:500px;" src="' + othis.attr('src') + '" /></p>',
		  othis,
		  { time: -1, maxWidth: '200px', maxHeight:'400px', tips: [1, "#3A3D49"] }
		);	
	}
  })
  .on("mouseleave", function () {	
	var othis = this;
	timeoutID = setTimeout( function(){
		layer.close(othis.index);
	} , 2000 );
  });	
  

  $('body').on('click', '.fly-case-active', function(){
    var othis = $(this), type = othis.data('type');
    active[type] && active[type].call(this, othis);
  });

  exports('case', {});
});