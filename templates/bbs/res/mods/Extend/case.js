/**

 @Name: 案例

 */
 
layui.define(['fly','dropdown'], function(exports){

  var $ = layui.jquery;
  var layer = layui.layer;
  var util = layui.util;
  var form = layui.form;
  var laypage = layui.laypage;
  var upload = layui.upload;
  var fly = layui.fly;
  var device = layui.device();
   var dropdown = layui.dropdown;
  

  //案例管理
  var active = {
    //提交案例
    push: function(div){
      layer.open({
        type: 1
        ,id: 'LAY_pushcase'
        ,title: '提交案例'
        ,area: (device.ios || device.android) ? ($(window).width() + 'px') : '660px'
        ,content: ['<ul class="layui-form" style="margin: 20px;">'
          ,'<li class="layui-form-item">'
            ,'<label class="layui-form-label">案例名称</label>'
            ,'<div class="layui-input-block">'
              ,'<input required name="title" lay-verify="required" placeholder="一般为网站名称" value="" class="layui-input">'
            ,'</div>'
          ,'</li>'
          ,'<li class="layui-form-item">'
            ,'<label class="layui-form-label">案例网址</label>'
            ,'<div class="layui-input-block">'
              ,'<input required name="link" lay-verify="url" placeholder="必须是自己或自己参与过的项目" value="" class="layui-input">'
            ,'</div>'
          ,'</li>'
          ,'<li class="layui-form-item layui-form-text">'
            ,'<label class="layui-form-label">案例描述</label>'
            ,'<div class="layui-input-block layui-form-text">'
              ,'<textarea required name="desc" lay-verify="required" autocomplete="off" placeholder="大致介绍你的项目，也可以阐述你在该项目中使用 iAspCms 的感受\n10-60个字" class="layui-textarea"></textarea>'
            ,'</div>'
          ,'</li>'
          ,'<li class="layui-form-item">'
            ,'<label class="layui-form-label">案例封面</label>'
            ,'<div class="layui-input-inline" style="width:auto;">'
              ,'<input type="hidden" name="cover" lay-verify="required" class="layui-input fly-case-image">'
              ,'<button type="button" class="layui-btn layui-btn-primary" id="caseUpload">'
                ,'<i class="layui-icon">&#xe67c;</i>上传图片'
              ,'</button>'
            ,'</div>'
            ,'<div class="layui-form-mid layui-word-aux" id="preview">推荐尺寸：478*300，大小不能超过 100kb</div>'
          ,'</li>'
          ,'<li class="layui-form-item">'
            ,'<label class="layui-form-label"> </label>'
            ,'<div class="layui-input-block">'
              ,'<input type="checkbox" checked="checked" name="agree" id="agree" title="我同意（如果你进行了刷赞行为，你的案例将被立马剔除）" lay-skin="primary">'
            ,'</div>'
          ,'</li>'
          ,'<li class="layui-form-item">'
            ,'<div class="layui-input-block">'
              ,'<button type="button" lay-submit lay-filter="pushCase" class="layui-btn">提交案例</button>'
           ,'</div>'
          ,'</li>'
        ,'</ul>'].join('')
        ,success: function(layero, index){
          var image = layero.find('.fly-case-image')
          ,preview = $('#preview');
 
          upload.render({
            url: '?Cases--User_upload'
            ,elem: '#caseUpload'
            ,size: 100
            ,done: function(res){
              if(res.status == 0){
                image.val(res.url);
                preview.html('<a href="'+ res.url +'" target="_blank" style="color: #5FB878;">封面已上传，点击可预览</a>');
              } else {
                layer.msg(res.msg, {icon: 5});
              }
            }
          });

          form.render('checkbox').on('submit(pushCase)', function(data){
            if(!data.field.agree){
              return layer.tips('你需要同意才能提交', $('#agree').next(), {tips: 1});
            }

            fly.json('?cases--User_doAddCase', data.field, function(res){
              layer.close(index);
              layer.alert(res.msg, {
                icon: 1
              })
            });
          });
        }
      });
    }
    
	
    //点赞
    ,praise: function(othis){
      var li = othis.parents('li')
      ,PRIMARY = 'layui-btn-primary'
      ,unpraise = !othis.hasClass(PRIMARY)
      ,numElem = li.find('.fly-case-nums')

      fly.json('?cases--User_doZan', {
        id: li.data('id')
        ,unpraise: unpraise ? true : null
      }, function(res){
        numElem.html(res.praise);
        if(unpraise){
          othis.addClass(PRIMARY).html('点赞');
          layer.tips('少了个赞啰', numElem, {
            tips: 1
          });
        } else {
          othis.removeClass(PRIMARY).html('已赞');
          layer.tips('成功获得个赞', numElem, {
            tips: [1, '#FF5722']
          });
        }
      });
    }

    //查看点赞用户
    ,showPraise: function(othis){
      var li = othis.parents('li');
      if(othis.html() == 0) return layer.tips('该项目还没有收到赞', othis, {
        tips: 1
      });
      fly.json('?cases--User_listCaseZan', {
        id: li.data('id')
      }, function(res){
        var html = '';
        layer.open({
          type: 1
          ,title: '项目【'+ res.title + '】获得的赞'
          ,id: 'LAY_showPraise'
          ,shade: 0.8
          ,shadeClose: true
          ,area: '305px'
          ,skin: 'layer-ext-case'
          ,content: function(){
            layui.each(res.data, function(_, item){
              html += '<li><a href="?u_'+ item.id +'" target="_blank" title="'+ item.username +'"><img src="'+ item.avatar +'"></a></li>'
            });
            return '<ul class="layer-ext-ul">' + html + '</ul>';
          }()
        })
      });
    }
  };
  
  
    if( $('.fly-shortcut li  a.DbSet')[0] ) {
		$('.fly-shortcut li  a.DbSet').on('click' ,function(){
			var url = $(this).data('href') ;
			var tip = $(this).find('cite').html() ;
			layer.confirm('确定要' + tip + '?', function(){
				fly.json( url ,{} , function(res){
					
				  if(res.status === 0){
					layer.alert(res.msg, {
					  icon: 1,
					  time: 0.5 *1000
					})  
				  } else {
					layer.alert(res.msg, {
					  icon: 5,
					  time: 0.5 *1000
					}) 
					  
				  }

				} );
			});
		});
	}
	
  if( $('.aspbbs-audit-dropdown')[0] ) {
	  //自定义事件
	  dropdown.render({
		elem: '.aspbbs-audit-dropdown'
		,data: aspbbsDropdownData
		,click: function(data, othis){
		  var id = $(this.elem).parents("li").data("id");
		  //根据 id 做出不同操作
		  if(data.id === 'edit'){
			editThread( this.elem );
		  } else if ( data.id === 'del' ) {
			delThread( this.elem )
		  } else if ( data.id === 'refuse' ) {
			setStatus( this.elem , 2 )
		  } else if ( data.id === 'pass' ) {
			setStatus( this.elem , 1 )
		  }
		}
	  });
  }
  
	//设置状态
	function setStatus( btn , rank){
      var othis = btn
		  ,li = othis.parents('li')
		  ,leftBtn = li.find('.case-left-btn')
		  ,rightBtn = li.find('.case-right-btn')
		  ,numElem = li.find('.fly-case-nums');
      fly.json('?Cases--Admin_setField', {
        id: li.data('id')
        ,'rank': rank
        ,field: 'ContentStatus'
      }, function(res){
		  if( rank == 1 ) {
			   numElem.html( "已通过" );
			   leftBtn.removeClass("layui-btn-primary");			   
			   rightBtn.removeClass("layui-btn-danger");			   
			   rightBtn.addClass("layui-btn-primary");			   
		  } else if( rank == 2 ) {
			   numElem.html( "已拒绝" ); 
			   leftBtn.addClass("layui-btn-primary");			   
			   rightBtn.addClass("layui-btn-danger");			   
			   rightBtn.removeClass("layui-btn-primary");			  
		  }
		  layer.msg( res.msg );
      });
	}
  
	//删除操作
	function delThread( btn ){
      var othis = btn
		  ,li = othis.parents('li');
		layer.confirm('确认删除该案例么？', function(){
			fly.json('?Cases--Admin_Remove', {
			  id: li.data('id')
			}, function(res){
			  if(res.status === 0){
				location.reload();
			  } else {
				layer.msg(res.msg);
			  }
			});
		});	
	}
  
	//修改操作
	function editThread( btn ){
		 var othis = btn
			  ,li = othis.parents('li');
			fly.json('?Cases--User_find', {
			  id: li.data('id')
			}, function(res){			
					
			  if(res.status === 0){
				  layer.open({
					type: 1
					,id: 'LAY_pushcase'
					,title: '修改案例'
					,area: (device.ios || device.android) ? ($(window).width() + 'px') : '660px'
					,content: ['<ul class="layui-form" style="margin: 20px;">'
					  ,'<li class="layui-form-item">'
						,'<label class="layui-form-label">案例名称</label>'
						,'<div class="layui-input-block">'
						  ,'<input required name="title" lay-verify="required" placeholder="一般为网站名称" value="' + res.Title + '" class="layui-input">'
						,'</div>'
					  ,'</li>'
					  ,'<li class="layui-form-item">'
						,'<label class="layui-form-label">案例网址</label>'
						,'<div class="layui-input-block">'
						  ,'<input required name="link" lay-verify="url" placeholder="必须是自己或自己参与过的项目" value="' + res.ContentSource + '" class="layui-input">'
						,'</div>'
					  ,'</li>'
					  ,'<li class="layui-form-item layui-form-text">'
						,'<label class="layui-form-label">案例描述</label>'
						,'<div class="layui-input-block layui-form-text">'
						  ,'<textarea required name="desc" lay-verify="required" autocomplete="off" placeholder="大致介绍你的项目，也可以阐述你在该项目中使用 iAspCms 的感受\n10-60个字" class="layui-textarea">' + res.Content + '</textarea>'
						,'</div>'
					  ,'</li>'
					  ,'<li class="layui-form-item">'
						,'<label class="layui-form-label">案例封面</label>'
						,'<div class="layui-input-inline" style="width:auto;">'
						  ,'<input hidden name="id" value="' + res.TopicID + '" />'
						  ,'<input type="hidden" name="cover" value="' + res.IndexImage + '" lay-verify="required" class="layui-input fly-case-image">'
						  ,'<button type="button" class="layui-btn layui-btn-primary" id="caseUpload">'
							,'<i class="layui-icon">&#xe67c;</i>上传图片'
						  ,'</button>'
						,'</div>'
						,'<div class="layui-form-mid layui-word-aux" id="preview"><a href="'+ res.IndexImage +'" target="_blank" style="color: #5FB878;">点击预览</a></div>'
					  ,'</li>'
					  ,'<li class="layui-form-item">'
						,'<label class="layui-form-label"> </label>'
						,'<div class="layui-input-block">'
						  ,'<input type="checkbox" checked="checked" name="agree" id="agree" title="我同意（如果你进行了刷赞行为，你的案例将被立马剔除）" lay-skin="primary">'
						,'</div>'
					  ,'</li>'
					  ,'<li class="layui-form-item">'
						,'<div class="layui-input-block">'
						  ,'<button type="button" lay-submit lay-filter="pushCase" class="layui-btn">修改案例</button>'
					   ,'</div>'
					  ,'</li>'
					,'</ul>'].join('')
					,success: function(layero, index){
					  var image = layero.find('.fly-case-image')
					  ,preview = $('#preview');
			 
					  upload.render({
						url: '?Cases--User_upload'
						,elem: '#caseUpload'
						,size: 100
						,done: function(res){
						  if(res.status == 0){
							image.val(res.url);
							preview.html('<a href="'+ res.url +'" target="_blank" style="color: #5FB878;">封面已上传，点击可预览</a>');
						  } else {
							layer.msg(res.msg, {icon: 5});
						  }
						}
					  });

					  form.render('checkbox').on('submit(pushCase)', function(data){
						if(!data.field.agree){
						  return layer.tips('你需要同意才能提交', $('#agree').next(), {tips: 1});
						}

						fly.json('?cases--User_doEditCase', data.field, function(res){
						  layer.close(index);
						  layer.alert(res.msg, {
							icon: 1
						  });
						  location.reload();
						});
					  });
					}
				  }); 
				  
				//location.reload();
			  } else {
				layer.msg(res.msg);
				
			  }
		});
		
	}
  

  $('body').on('click', '.fly-case-active', function(){
    var othis = $(this), type = othis.data('type');
    active[type] && active[type].call(this, othis);
  });

  exports('case', {});
});