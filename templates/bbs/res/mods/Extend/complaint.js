/**

 @Name: 用户模块

 */
 
layui.define(['laypage', 'fly', 'element', 'flow','table','form'], function(exports){

  var $ = layui.jquery;
  var layer = layui.layer;
  var util = layui.util;
  var laytpl = layui.laytpl;
  var form = layui.form;
  var laypage = layui.laypage;
  var fly = layui.fly;
  var flow = layui.flow;
  var element = layui.element;
  var upload = layui.upload;
  var table = layui.table;
  var ids = [];
  
  var gather = {}, dom = {
    mine: $('#LAY_mine')
    ,mineview: $('.mine-view')
    ,minemsg: $('#LAY_minemsg')
    ,infobtn: $('#LAY_btninfo')
  };
 
  	function quickSetField( data ){
		
		var oForm = $(data.elem).parents("form"),
		    setField = oForm.data("setfield"),
			action = oForm.attr("action"),
			fieldName = $(data.elem).attr("name"),
			fieldValue = data.value,
			label = $(data.elem).parents("div.layui-form-item").find("label").html(),
			dict = {},
			arr = [];
		if ( setField.toLowerCase() == "setting"  ) {
			dict[ fieldName ] = fieldValue;
			fly.json( action , dict, function(res){
				  layer.tips(label +' 快捷设置成功',data.othis, {
					 tips: [1, '#3595CC'],
					time: 2000
				  });
			} );
		} else {
			arr = setField.split(":");
			action = "?" + arr[0] + "_doQuickSet";
			dict[ "id" ] = $( "[name='" + arr[1] + "']" ).val();
			dict[ "field" ] = fieldName;
			dict[ fieldName ] = fieldValue
			fly.json( action , dict, function(res){
				  layer.tips(label +' 快捷设置成功',data.othis, {
					 tips: [1, '#3595CC'],
					time: 2000
				  });
			} );
		}
	}
	
	if ( $("form.bbs-quickset").data("setfield") != undefined ) {
		
		form.on('radio', function(data){
			quickSetField( data );
		});  
		
		form.on('select', function(data){
			quickSetField( data );
		}); 
		
		form.on('switch', function(data){
			quickSetField( data );
		}); 
	}


  //显示当前tab
  if(location.hash){
    element.tabChange('user', location.hash.replace(/^#/, ''));
  }

  element.on('tab(user)', function(){
    var othis = $(this), layid = othis.attr('lay-id');
    if(layid){
      location.hash = layid;
    }
  });
  
	$('#chatBtn').on('click', function(){
		var othis = $(this);
		if(layui.cache.user.uid == '-1') {
			layer.alert('请先登陆！', {icon: 7});			
		} else{
			layer.prompt({
				title:'发送小纸条'
			},function(val, index){
				fly.json('?user_SendMessage', {
				  FromUser: layui.cache.user.uid,
				  ToUser: othis.data("touser"),
				  Message:val
				}, function(res){					
				  if(res.status === 0){
					layer.alert('发送成功', {icon: 6});
					layer.close(index);
				  }
				});
				//layer.msg('得到了'+val);
				//layer.close(index);
			});			
		}
		
	});
	
	$('.IsClosed').on('click', function(){
		var othis = $(this);

			fly.json('?complaint--Admin_setStatus4Cpl', {
			  'id': othis.data("touser")
			  ,'CplID' : othis.parents("li").data("id")
			  ,'FieldValue': othis.data("type")
			  ,'StatusField' : othis.data("status")
			}, function(res){					
			  if(res.status === 0){
				if ( othis.data("type") == '1' ) {
					othis.removeClass("layui-btn-danger") ;
					othis.data("type",'0') ;
					othis.html("未忽略") ;
				} else {
					othis.addClass("layui-btn-danger") ;
					othis.data("type",'1') ;
					othis.html("已忽略") ;
				}
				layer.msg("设置成功");
			  }
			});	
	});
	
	
	$('.UserStatus,.ThreadStatus,.ReplyStatus').on('click', function(){
		var othis = $(this);

		if(layui.cache.user.uid == '-1') {
			layer.alert('请先登陆！', {icon: 7});			
		} else{
			setStatusField( othis );
		}
	});
	
	$('#SelfStatus').on('click', function(){
		var othis = $(this);
		if(layui.cache.user.uid == '-1') {
			layer.alert('请先登陆！', {icon: 7});			
		} else{
			layer.confirm('确认要' + othis.html() + '？', function(){
				fly.json('?User_setStatusField', {
				  'id': othis.data("touser")
				  ,'FieldValue': othis.data("type")
				  ,'StatusField' : "UserStatus"
				}, function(res){					
				  if(res.status === 0){
					layer.msg("注销成功！");
					 location.href = '/';
				  }
				});	
			});
		}
	});
	
	function setStatusField( othis ) {
		fly.json('?complaint--Admin_setStatusField', {
		  'id': othis.data("touser")
		  ,'CplID' : othis.parents("li").data("id")
		  ,'FieldValue': othis.data("type")
		  ,'StatusField' : othis.data("status")
		}, function(res){					
		  if(res.status === 0){
			if ( othis.data("type") == '0' ) {
				othis.removeClass("layui-btn-danger") ;
				othis.data("type",'1') ;
				othis.html( '禁' + othis.html().slice(1) );
			} else {
				othis.addClass("layui-btn-danger") ;
				othis.data("type",'0') ;
				othis.html( '允' + othis.html().slice(1) );
			}
			layer.msg("设置成功");
		  }
		});	
	}
	
  dom.minemsg[0] && gather.minemsg();

  $(".ThreadDel").on( 'click' , function(){
	  var othis = $(this);
	  var li = othis.parents("li");
	  var id = li.data("id");
	  
      layer.confirm('确认要删除么？', function(index){
        layer.close(index);
        fly.json('?Complaint--Admin_remove', {
          id: id
		  ,'type' : othis.attr( 'type' )
        }, function(res){
			console.log(res);
          if(res.status === 0){
            li.hide();
          } else {
            layer.msg(res.msg);
          }
        });
      });	  
  } )

	
	$('.fly-shortcut li  a.DbSet').on('click' ,function(){
		var url = $(this).data('href') ;
		var tip = $(this).find('cite').html() ;
		layer.confirm('确定要' + tip + '?', function(){
			fly.json( url ,{} , function(res){
				
			  if(res.status === 0){
				layer.alert(res.msg, {
				  icon: 1,
				  time: 0.5 *1000
				})  
			  } else {
				layer.alert(res.msg, {
				  icon: 5,
				  time: 0.5 *1000
				}) 
				  
			  }

			} );
		});
	});
	
	$('.fly-shortcut li a.MsgSet').on('click' ,function(){
		var url = $(this).data('href') ;
		var type = $(this).data('type');
			layer.open({
				title: '修改',
				type: 2,
				shade: 0.2,
				shadeClose: true,
				maxWidth: '90',
				area: ['720px', '450px'],
				content: url + '_' + type
			});
	});	
  
  exports('user', null);
  
});