
/*!
 * 扩展组件平台
 */
 
layui.define(['fly', 'element', 'carousel','dropdown'], function(exports){

  var $ = layui.jquery;
  var layer = layui.layer;
  var util = layui.util;
  var laytpl = layui.laytpl;
  var form = layui.form;
  var laypage = layui.laypage;
  var fly = layui.fly;
  
  var element = layui.element;
  var upload = layui.upload;

  var carousel = layui.carousel;
  var dropdown = layui.dropdown;

  var THIS = 'layui-this';
  
  var gather = {};


  $('.jie-admin').on('click', 'span', function(){
    var othis = $(this), type = othis.attr('type');
    gather.jieAdmin[type] && gather.jieAdmin[type].call(this, othis.parent());
  });
  
  
  //求解管理
  gather.jieAdmin = {
	  
    //删除
    remove: function(div){
      layer.confirm('确认删除吗？', function(index){
        layer.close(index);
        fly.json('?slide--Admin_Remove2', {
          id: div.data('id')
        }, function(res){
          if(res.status === 0){
            location.reload();
          } else {
            layer.msg(res.msg);
          }
        });
      });
    }
	
	
    //彻底删除
    ,removesort: function(div){
      layer.confirm('确认要永久删除吗？', function(index){
        layer.close(index);
        fly.json('?slide--Admin_RemoveSort', {
          id: div.data('id')
        }, function(res){
          if(res.status === 0){
            location.reload();
          } else {
            layer.msg(res.msg);
          }
        });
      });
    }
	

  };

  //banner 轮播
  var elemBanner = $('#FLY-extend-banner');
  carousel.render({
    elem: elemBanner
    ,width: '100%' //设置容器宽度
    ,height: elemBanner.data('height')
    ,arrow: 'none' //始终显示箭头
    ,anim: 'fade' //切换动画方式
    ,interval: 5000
  });


  //验证规则
  form.verify({
    resRequired: [
      /[\S]+/
      ,'请上传组件资源包'
    ]
  });

  //上传组件
  var elemRes = $('#FLY-extend-res')
  upload.render({
    elem: '#FLY-extend-upload'
    ,url: '/api/upload/file'
    ,accept: 'file'
    ,exts: 'zip|rar|7z'
    ,size: 3*1000*2014
    ,done: function(res){
      if(res.status == 0){
        elemRes.val(res.url);
        this.elem.find('p').html(res.filename);
        layer.msg('文件上传成功', {icon: 1});
      } else {
        layer.msg(res.msg, {icon: 5});
      }
    }
  });

  //提交成功后的回调
  fly.form['extendRelease'] = function(field, elem, res){
    layer.alert(res.msg, {
      icon: 1
      ,btnAlign: 'c'
      ,btn: ['朕已知晓']
      ,end: function(){
        location.href = '/user/extend/';
      }
    });
  };


  //文档导航切换
  element.tab({
    headerElem: '.fly-extend-doc-nav>li' //指定tab头元素项
    ,bodyElem: '.fly-extend-doc' //指定tab主体元素项
  });

  //获取hash来切换选项卡，假设当前地址的hash为lay-id对应的值
  var layid = location.hash.replace(/^#/, '');
  layid && $('.fly-extend-doc-nav>li[lay-id="'+ layid +'"]').trigger('click')

  //监听文档导航
  element.on('tab(extend-doc-nav)', function(){
    location.hash = this.getAttribute('lay-id');
  });
  
    if( $('.fly-shortcut li  a.DbSet')[0] ) {
		$('.fly-shortcut li  a.DbSet').on('click' ,function(){
			var url = $(this).data('href') ;
			var tip = $(this).find('cite').html() ;
			layer.confirm('确定要' + tip + '?', function(){
				fly.json( url ,{} , function(res){
					
				  if(res.status === 0){
					layer.alert(res.msg, {
					  icon: 1,
					  time: 0.5 *1000
					})  
				  } else {
					layer.alert(res.msg, {
					  icon: 5,
					  time: 0.5 *1000
					}) 
					  
				  }

				} );
			});
		});
	}
  

  util.fixbar({
    //bar1: '&#xe642;',
    click: function(type){
      if(type === 'bar1'){
        //location.href = '/jie/add/';
      }
    }
  });

  exports('extends', {});
  
});