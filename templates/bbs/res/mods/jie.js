/**

 @Name: 求解板块

 */
 
layui.define('fly', function(exports){
  var $ = layui.jquery;
  var layer = layui.layer;
  var util = layui.util;
  var laytpl = layui.laytpl;
  var form = layui.form;
  var fly = layui.fly;
  
  var gather = {}, dom = {
    jieda: $('#jieda')
    ,content: $('#L_content').summernote?$('#L_content').summernote('code'):''
    ,jiedaCount: $('#jiedaCount')
  };

  //监听专栏选择
  form.on('select(column)', function(obj){
    var value = obj.value
    ,elemQuiz = $('#LAY_quiz')
    ,tips = {
      tips: 1
      ,maxWidth: 250
      ,time: 10000
    };
    elemQuiz.addClass('layui-hide');
    if(value === '0'){
      layer.tips('下面的信息将便于您获得更好的答案', obj.othis, tips);
      elemQuiz.removeClass('layui-hide');
    } else if(value === '99'){
      layer.tips('系统会对【分享】类型的帖子予以' + ASPBBS.ExperienceName + '奖励，但我们需要审核，通过后方可展示', obj.othis, tips);
    }
  });

  //提交回答
  fly.form['add-reply'] = function(data, required){
    var tpl = '<li>\
      <div class="detail-about detail-about-reply">\
        <a class="fly-avatar" href="?u_{{ layui.cache.user.uid }}" target="_blank">\
          <img src="{{= d.user.avatar}}" alt="{{= d.user.username}}">\
        </a>\
        <div class="fly-detail-user">\
          <a href="/u/{{ layui.cache.user.uid }}" target="_blank" class="fly-link">\
            <cite>{{d.user.username}}</cite>\
          </a>\
        </div>\
        <div class="detail-hits">\
          <span>刚刚</span>\
        </div>\
      </div>\
      <div class="detail-body jieda-body photos">\
        {{ d.content}}\
      </div>\
    </li>'
    //data.content = fly.content(data.content);
    data.Content = $('#L_content').summernote?$('#L_content').summernote('code'):'';
    laytpl(tpl).render($.extend(data, {
      user: layui.cache.user
    }), function(html){
      required[0].value = '';
      dom.jieda.find('.fly-none').remove();
      dom.jieda.append(html);
      
      var count = dom.jiedaCount.text()|0;
      dom.jiedaCount.html(++count);
    });
  };

  //求解管理
  gather.jieAdmin = {
    //删求解
    del: function(div){
      layer.confirm('确认删除该求解么？', function(index){
        layer.close(index);
        fly.json('?Topic_Remove', {
          id: div.data('id')
        }, function(res){
          if(res.status === 0){
            location.href = res.action;
          } else {
            layer.msg(res.msg);
          }
        });
      });
    }
    
    //设置置顶、状态
    ,set: function(div){
      var othis = $(this);
      fly.json('?Topic_setField', {
        id: div.data('id')
        ,rank: othis.attr('rank')
        ,field: othis.attr('field')
      }, function(res){
        if(res.status === 0){
          location.reload();
        }
      });
    }

    //收藏
    ,collect: function(div){
      var othis = $(this), type = othis.data('type');
      fly.json('?Digg_' + type + 'Collection', {
        cid: div.data('id')
      }, function(res){
        if(type === 'add'){
          layer.msg('收藏成功');
          othis.data('type', 'remove').html('取消收藏').addClass('layui-btn-primary');
        } else if(type === 'remove'){
          layer.msg('取消收藏成功');
          othis.data('type', 'add').html('收藏').removeClass('layui-btn-primary');
        }
      });
    }
	
	,zan: function(div){
		if ( layui.cache.user.uid == -1 ) {
			layer.msg( '请先登陆后再点赞！' , {icon: 5} );	
			return false;
		}
      var othis = $(this), ok = othis.hasClass('zanok');
      fly.json('?Digg_doZanTopic', {
        ok: ok
        ,cid: div.data('id')
      }, function(res){
        if(res.status === 0){
          var zans = othis.next().find('em').html()|0;
          othis[ok ? 'removeClass' : 'addClass']('zanok');
           othis.next().find('em').html((++zans));
          //othis.find('em').html(ok ? (++zans) : (--zans));
        } else {
          layer.msg(res.msg);
        }
      });
		
		
	}

	
    //查看点赞用户
    ,showPraise: function(div){
      var othis=$(this);
      if(othis.find("em").html() == 0) return layer.tips('该贴子还没有收到赞，您来赞一个吧！', othis, {
        tips: 1
      });
      fly.json('?Digg_listTopicZan', {
        id: div.data('id')
      }, function(res){
        var html = '';
        layer.open({
          type: 1
          ,title: '该贴子获得的赞'
          ,id: 'LAY_showPraise'
          ,shade: 0.8
          ,shadeClose: true
          ,area: '305px'
          ,skin: 'layer-ext-case'
          ,content: function(){
            layui.each(res.data, function(_, item){
              html += '<li><a href="?u_'+ item.id +'" target="_blank" title="'+ item.username +'"><img src="'+ item.avatar +'"></a></li>'
            });
            return '<ul class="layer-ext-ul">' + html + '</ul>';
          }()
        })
      });
    }	
	,quote: function(li){
		
	}
  };

  $('body').on('click', '.jie-admin', function(){
    var othis = $(this), type = othis.attr('type');
    gather.jieAdmin[type] && gather.jieAdmin[type].call(this, othis.parent());
  });
	  	
  //异步渲染
  var asyncRender = function(){
   var jieAdmin = $('#LAY_jieAdmin'), elemAdminCollect = $('.jie-admin-collect'),elemAdminZan = $('.jie-admin-zan');
    //查询帖子是否收藏
    if(jieAdmin[0] && layui.cache.user.uid != -1){
      fly.json('?Digg_findDigg', {
        cid: jieAdmin.data('id')
      }, function(res){
        if(res.data.Collection){
          elemAdminCollect.data('type', 'remove').html('取消收藏').addClass('layui-btn-primary');
        }
        if(res.data.TopicZan){
          elemAdminZan.data('type', 'remove').html('点过赞 <em>' + res.data.praise + '</em>').addClass('layui-btn-primary');
        }
        elemAdminCollect.css('visibility', 'visible')
        elemAdminZan.css('visibility', 'visible')
      });
    }

  }();

  //解答操作
  gather.jiedaActive = {
    zan: function(li){ //赞
		if ( layui.cache.user.uid == -1 ) {
			layer.msg( '请先登陆后再点赞！' , {icon: 5} );
			return false;
		}
      var othis = $(this), ok = othis.hasClass('zanok');
      fly.json('?Digg_doZanReply', {
        ok: ok
        ,cid: li.data('id')
      }, function(res){
        if(res.status === 0){
          var zans = othis.next("span").find('em').html()|0;
          othis[ok ? 'removeClass' : 'addClass']('zanok');
          othis.next("span").find('em').html((++zans));
          //othis.find('em').html(ok ? (++zans) : (--zans));
        } else {
          layer.msg(res.msg);
        }
      });
    }
	
	
	,faqZan: function(div){
		if ( layui.cache.user.uid == -1 ) {
			layer.msg( '请先登陆后再点赞！' , {icon: 5} );	
			return false;
		}
      var othis = $(this), ok = othis.hasClass('zanok');
      fly.json('?Faq_doZan', {
        ok: ok
        ,cid: div.data('id')
      }, function(res){
        if(res.status === 0){
          var zans = othis.next().find('em').html()|0;
          othis[ok ? 'removeClass' : 'addClass']('zanok');
           othis.next().find('em').html((++zans));
          //othis.find('em').html(ok ? (++zans) : (--zans));
        } else {
          layer.msg(res.msg);
        }
      });
		
		
	}	
	
	
    ,reply: function(li){ //回复
      //var val = dom.content.val();
      var val =  $('#L_content').summernote?$('#L_content').summernote('code'):'';
	  var UserID = li.data('user');
	  var FloorNum = li.find('.layui-bg-cyan').eq(0).text();
	  FloorNum = '<span class="layui-badge layui-bg-cyan">' + FloorNum + '</span>';
      var aite = '<a href="?u_'+ UserID +'">@'+ li.find('.fly-detail-user cite').text().replace(/\s/g, '') + ' ' + FloorNum + '</a> <br />';
      //dom.content.focus()
      if(val.indexOf(aite) !== -1) return;
      //dom.content.val(aite +' ' + val);
	  if ($('#L_content').summernote) {
		  $('#L_content').summernote('code', aite +' ' + val);
	  }
	  
    }
    ,faq: function(li){ //回答留言
      //var val = dom.content.val();
      var val =  $('#L_content').summernote?$('#L_content').summernote('code'):'';
	  var UserID = li.data('user');
	  var FloorNum = li.find('.layui-bg-cyan').eq(0).text();
	  FloorNum = '<span class="layui-badge layui-bg-cyan">' + FloorNum + '</span>';
      var aite = li.find('.fly-detail-user cite').text().replace(/\s/g, '') + ' ' + FloorNum + '<br /><p><br /></p>';
      //dom.content.focus()
      if(val.indexOf(aite) !== -1) return;
      //dom.content.val(aite +' ' + val);
	  if ($('#L_content').summernote) {
		  $('#L_content').summernote('code', aite +' ' + val);
		  $("input[name='ParentID']").val( li.data("id") );
		  $("input[name='FaqType']").val( 5 );
	  }
	  
    }
    ,accept: function(li){ //采纳
      var othis = $(this);
      layer.confirm('是否采纳该回答为最佳答案？', function(index){
        layer.close(index);
        fly.json('?Reply_Accept', {
          id: li.data('id')
        }, function(res){
          if(res.status === 0){
            $('.jieda-accept').remove();
            li.addClass('jieda-daan');
            li.find('.detail-about').append('<i class="iconfont icon-caina" title="最佳答案"></i>');
          } else {
            layer.msg(res.msg);
          }
        });
      });
    }
    ,edit: function(li){ //编辑
      fly.json('?Reply_AjaxEdit', {
        id: li.data('id')
      }, function(res){
        var data = res;
        layer.prompt({
          formType: 2
          ,value: data.Content
          ,maxlength: 100000
          ,title: '编辑回帖'
          ,area: ['728px', '300px']
          ,success: function(layero){
            fly.layEditor({
              elem: layero.find('textarea')
            });
          }
        }, function(value, index){
          fly.json('/jie/updateDa/', {
            id: li.data('id')
            ,content: value
          }, function(res){
            layer.close(index);
            li.find('.detail-body').html(fly.content(value));
          });
        });
      });
    }
    ,restore: function(li){ //恢复
      layer.confirm('确认恢复该回答么？', function(index){
        layer.close(index);
        fly.json('?Reply_Restore', {
          id: li.data('id')
        }, function(res){
          if(res.status === 0){
			  console.log(li.find(".fly-remove"));
			li.find(".jieda-admin").append("<span type='del'>删除</span>" );
            li.find("div.fly-remove").remove();
            li.find(".jieda-admin span[type='restore']").remove();
          } else {
            layer.msg(res.msg);
          }
        });
      }); 	  
    }
    ,faqAudit: function(li){ //恢复
      layer.confirm('确认要通过该留言吗？', function(index){
        layer.close(index);
        fly.json('?Faq_Audit', {
          id: li.data('id')
        }, function(res){
          if(res.status === 0){
            li.find("div.fly-remove").remove();
            li.find(".jieda-admin span[type='faqAudit']").remove();
            li.find(".bbs-faq-status").addClass("layui-bg-green").html("已公开")
          } else {
            layer.msg(res.msg);
          }
        });
      }); 	  
    }
    ,faqDel: function(li){ //恢复
      layer.confirm('确认要删除该留言吗？', function(index){
        layer.close(index);
        fly.json('?Faq_Remove', {
          id: li.data('id')
        }, function(res){
          if(res.status === 0){
			location.reload();
          } else {
            layer.msg(res.msg);
          }
        });
      }); 	  
    }
    ,del: function(li){ //删除
      layer.confirm('确认删除该回答么？', function(index){
        layer.close(index);
        fly.json('?Reply_Remove', {
          id: li.data('id')
        }, function(res){
          if(res.status === 0){
            //var count = dom.jiedaCount.text()|0;
            //dom.jiedaCount.html(--count);
			//li.find(".jieda-admin").append("<span type='restore'>恢复</span>" );
            //li.find(".jieda-admin span[type='del']").fadeOut().remove();
			location.reload();
            //如果删除了最佳答案
            if(li.hasClass('jieda-daan')){
              $('.jie-status').removeClass('jie-status-ok').text('求解中');
            }
          } else {
            layer.msg(res.msg);
          }
        });
      });    
    }
    //删提问
    ,threaddel: function(div){
      layer.confirm('确认删除该提问么？', function(index){
        layer.close(index);
        fly.json('?Topic_Remove', {
          id: div.data('id')
        }, function(res){
          if(res.status === 0){
            //var count = dom.jiedaCount.text()|0;
            //dom.jiedaCount.html(--count);
            //li.remove();
            //如果删除了最佳答案
            if(li.hasClass('jieda-daan')){
              $('.jie-status').removeClass('jie-status-ok').text('求解中');
            }
          } else {
            layer.msg(res.msg);
          }
        });
      });
    }
    //查看点赞用户
    ,showPraise: function(li){
      var othis=$(this);
      if(othis.find("em").html() == 0) return layer.tips('该回复还没有收到赞，您来赞一个吧！', othis, {
        tips: 1
      });
      fly.json('?Digg_listReplyZan', {
        id: li.data('id')
      }, function(res){
        var html = '';
        layer.open({
          type: 1
          ,title: '该回复获得的赞'
          ,id: 'LAY_showPraise'
          ,shade: 0.8
          ,shadeClose: true
          ,area: '305px'
          ,skin: 'layer-ext-case'
          ,content: function(){
            layui.each(res.data, function(_, item){
              html += '<li><a href="?u_'+ item.id +'" target="_blank" title="'+ item.username +'"><img src="'+ item.avatar +'"></a></li>'
            });
            return '<ul class="layer-ext-ul">' + html + '</ul>';
          }()
        })
      });
    }
	
    //查看点赞用户
    ,showFaqPraise: function(li){
      var othis=$(this);
      if(othis.find("em").html() == 0) return layer.tips('该留言还没有收到赞，您来赞一个吧！', othis, {
        tips: 1
      });
      fly.json('?Faq_listZan', {
        id: li.data('id')
      }, function(res){
        var html = '';
        layer.open({
          type: 1
          ,title: '该留言获得的赞'
          ,id: 'LAY_showPraise'
          ,shade: 0.8
          ,shadeClose: true
          ,area: '305px'
          ,skin: 'layer-ext-case'
          ,content: function(){
            layui.each(res.data, function(_, item){
              html += '<li><a href="?u_'+ item.id +'" target="_blank" title="'+ item.username +'"><img src="'+ item.avatar +'"></a></li>'
            });
            return '<ul class="layer-ext-ul">' + html + '</ul>';
          }()
        })
      });
    }
  };
  
  $('.faq-all-delete').on('click', function(){
      layer.confirm('确认要全部移除吗？删除之后不能恢复！', function(index){
        layer.close(index);
        fly.json('?Faq_Clear', {}, function(res){
          if(res.status === 0){
			layer.msg(res.msg);
            location.reload();
          } else {
            layer.msg(res.msg);
          }
        });
      });
  });
  
  $('.faq-recycle-delete').on('click', function(){
      layer.confirm('确认要清空留言回收站吗？删除之后不能恢复！', function(index){
        layer.close(index);
        fly.json('?Faq_ClearRecycle', {}, function(res){
          if(res.status === 0){
            layer.msg(res.msg)
          } else {
            layer.msg(res.msg);
          }
        });
      });
  });
  
  $('.jie-all-delete').on('click', function(){
      layer.confirm('确认要全部移除吗？', function(index){
        layer.close(index);
        fly.json('?Topic_Delete', {}, function(res){
          if(res.status === 0){
            location.reload();
          } else {
            layer.msg(res.msg);
          }
        });
      });
  });
  
  $('.jie-all-restore').on('click', function(){
      layer.confirm('确认要全部恢复吗？', function(index){
        layer.close(index);
        fly.json('?Topic_Restore', {}, function(res){
          if(res.status === 0){
            location.reload();
          } else {
            layer.msg(res.msg);
          }
        });
      });
  });
  
  $('.detail-hits span.quote').on('click', function(){
      //var val = dom.content.val();
	  var othis = $(this);
	  var li = othis.parents('li');
      var val =  $('#L_content').summernote?$('#L_content').summernote('code'):'';
	  var userID = li.data("user");
	  var userName = li.find(".fly-link cite").text();
	  var replyTime = li.find(".detail-hits .ReplyTime").attr('title');
	  var content = li.find('.jieda-body').text().replace(/\s+/g,'');
	  var quoteText = '';
	  var FloorNum = li.find('.layui-bg-cyan').text();
	  FloorNum = '<span class="layui-badge layui-bg-cyan">' + FloorNum + '</span>';
	  if(val.indexOf('<div class="layui-elem-quote"') !== -1){
		  layer.alert('一次只能引用一个回复！', {icon: 7,time:1.5*1000});
		  return false;
	  };
	  
      var quoteText = '<div class="layui-elem-quote" data-user="' + userID + '"><p>' + content.substr(0,50) + ( content.length > 50 ? '...' : '' ) + '</p><p class="fly-grey"><a href="?u_' + userID + '">' + userName + ' ' + FloorNum + '</a> 发表于 ' + replyTime + '</p></div>';
      //dom.content.val(aite +' ' + val);
	  if ($('#L_content').summernote) {
		  $('#L_content').summernote('editor.pasteHTML', quoteText );
	  }
  });

  $('.jieda-reply').on('click','span',function(){
    var othis = $(this), type = othis.attr('type');
	if ( type != undefined ) {
		gather.jiedaActive[type].call(this, othis.parents('li'));		
	}    
  });
  

  //定位分页
  if(/\/page\//.test(location.href) && !location.hash){
    var replyTop = $('#flyReply').offset().top - 80;
    $('html,body').scrollTop(replyTop);
  }
  
 
  
	$('.quick-faq').on('click', function(){
		var othis = $(this).parents("li");
		var that = $(this);
		var title = that.attr("title");

		layer.prompt({
			title:(title.length <= 15 ? title : title.substr( 0,15 ) + '...' )
		},function(val, index){
			fly.json('?Faq_Add', {
			  page:0
			  ,Contact: ''
			  ,ParentID: othis.data("id")
			  ,FaqType: 1
			  ,Content:val
			}, function(res){		
			  if(res.status === 0){					
				layer.alert('追问成功', {icon: 6,time: 1.5 *1000});
				layer.close(index);
				if ( res.action !== '' ) {
					location.reload();
				}
			  }
			});
			//layer.msg('得到了'+val);
			//layer.close(index);
		});			
	}); 

  exports('jie', null);
});