/**

 @Name: 用户模块

 */
 
layui.define(['laypage', 'fly', 'element', 'flow'], function(exports){

  var $ = layui.jquery;
  var layer = layui.layer;
  var util = layui.util;
  var laytpl = layui.laytpl;
  var form = layui.form;
  var laypage = layui.laypage;
  var fly = layui.fly;
  var flow = layui.flow;
  var element = layui.element;
  var upload = layui.upload;

  var gather = {}, dom = {
    mine: $('#LAY_mine')
    ,mineview: $('.mine-view')
    ,minemsg: $('#LAY_minemsg')
    ,infobtn: $('#LAY_btninfo')
  };

  //我的相关数据
  var elemUC = $('#LAY_uc'), elemUCM = $('#LAY_ucm');
  gather.minelog = {};
  gather.mine = function(index, type, url){
    var tpl = [
      //求解
      '{{# for(var i = 0; i < d.rows.length; i++){ }}\
      <li>\
        {{# if(d.rows[i].collection_time){ }}\
          <a class="jie-title" href="/jie/{{d.rows[i].id}}/" target="_blank">{{= d.rows[i].title}}</a>\
          <i>{{ d.rows[i].collection_time }} 收藏</i>\
        {{# } else { }}\
          {{# if(d.rows[i].status == 1){ }}\
          <span class="fly-jing layui-hide-xs">精</span>\
          {{# } }}\
          {{# if(d.rows[i].accept >= 0){ }}\
            <span class="jie-status jie-status-ok">已结</span>\
          {{# } else { }}\
            <span class="jie-status">未结</span>\
          {{# } }}\
          {{# if(d.rows[i].status == -1){ }}\
            <span class="jie-status">审核中</span>\
          {{# } }}\
          <a class="jie-title" href="/jie/{{d.rows[i].id}}/" target="_blank">{{= d.rows[i].title}}</a>\
          <i class="layui-hide-xs">{{ layui.util.timeAgo(d.rows[i].time, 1) }}</i>\
          {{# if(d.rows[i].accept == -1){ }}\
          <a class="mine-edit layui-hide-xs" href="/jie/edit/{{d.rows[i].id}}" target="_blank">编辑</a>\
          {{# } }}\
          <em class="layui-hide-xs">{{d.rows[i].hits}}阅/{{d.rows[i].comment}}答</em>\
        {{# } }}\
      </li>\
      {{# } }}'
    ];

    var view = function(res){
      var html = laytpl(tpl[0]).render(res);
      dom.mine.children().eq(index).find('span').html(res.count);
      elemUCM.children().eq(index).find('ul').html(res.rows.length === 0 ? '<div class="fly-msg">没有相关数据</div>' : html);
    };

    var page = function(now){
      var curr = now || 1;
      if(gather.minelog[type + '-page-' + curr]){
        view(gather.minelog[type + '-page-' + curr]);
      } else {
        //我收藏的帖
        if(type === 'collection'){
          var nums = 10; //每页出现的数据量
          fly.json(url, {}, function(res){
            res.count = res.rows.length;

            var rows = layui.sort(res.rows, 'collection_timestamp', 'desc')
            ,render = function(curr){
              var data = []
              ,start = curr*nums - nums
              ,last = start + nums - 1;

              if(last >= rows.length){
                last = curr > 1 ? start + (rows.length - start - 1) : rows.length - 1;
              }

              for(var i = start; i <= last; i++){
                data.push(rows[i]);
              }

              res.rows = data;
              
              view(res);
            };

            render(curr)
            gather.minelog['collect-page-' + curr] = res;

            now || laypage.render({
              elem: 'LAY_page1'
              ,count: rows.length
              ,curr: curr
              ,jump: function(e, first){
                if(!first){
                  render(e.curr);
                }
              }
            });
          });
        } else {
          fly.json('/api/'+ type +'/', {
            page: curr
          }, function(res){
            view(res);
            gather.minelog['mine-jie-page-' + curr] = res;
            now || laypage.render({
              elem: 'LAY_page'
              ,count: res.count
              ,curr: curr
              ,jump: function(e, first){
                if(!first){
                  page(e.curr);
                }
              }
            });
          });
        }
      }
    };

    if(!gather.minelog[type]){
      page();
    }
  };

  if(elemUC[0]){
    layui.each(dom.mine.children(), function(index, item){
      var othis = $(item)
      gather.mine(index, othis.data('type'), othis.data('url'));
    });
  }

  //显示当前tab
  if(location.hash){
    element.tabChange('user', location.hash.replace(/^#/, ''));
  }

  element.on('tab(user)', function(){
    var othis = $(this), layid = othis.attr('lay-id');
    if(layid){
      location.hash = layid;
    }
  });

  //根据ip获取城市
  if($('#L_city').val() === ''){
    $.getScript('http://int.dpool.sina.com.cn/iplookup/iplookup.php?format=js', function(){
      $('#L_city').val(remote_ip_info.city||'');
    });
  }

  //上传图片
  if($('.upload-img')[0]){
    layui.use('upload', function(upload){
      var avatarAdd = $('.avatar-add');

      upload.render({
        elem: '.upload-img'
        ,url: '?User_Upload'
        ,size: 200
        ,before: function(){
          avatarAdd.find('.loading').show();
        }
        ,done: function(res){
          if(res.status == 0){
            $.post('?User_Settings', {
              avatar: res.url
            }, function(res){
              location.reload();
            });
          } else {
            layer.msg(res.msg, {icon: 5});
          }
          avatarAdd.find('.loading').hide();
        }
        ,error: function(){
          avatarAdd.find('.loading').hide();
        }
      });
    });
  }

  //合作平台
  if($('#LAY_coop')[0]){

    //资源上传
    $('#LAY_coop .uploadRes').each(function(index, item){
      var othis = $(this);
      upload.render({
        elem: item
        ,url: '/api/upload/cooperation/?filename='+ othis.data('filename')
        ,accept: 'file'
        ,exts: 'zip'
        ,size: 30*1024
        ,before: function(){
          layer.msg('正在上传', {
            icon: 16
            ,time: -1
            ,shade: 0.7
          });
        }
        ,done: function(res){
          if(res.code == 0){
            layer.msg(res.msg, {icon: 6})
          } else {
            layer.msg(res.msg)
          }
        }
      });
    });

    //成效展示
    var effectTpl = ['{{# layui.each(d.data, function(index, item){ }}'
    ,'<tr>'
      ,'<td><a href="?u_{{ item.uid }}" target="_blank" style="color: #01AAED;">{{ item.uid }}</a></td>'
      ,'<td>{{ item.authProduct }}</td>'
      ,'<td>￥{{ item.rmb }}</td>'
      ,'<td>{{ item.create_time }}</td>'
      ,'</tr>'
    ,'{{# }); }}'].join('');

    var effectView = function(res){
      var html = laytpl(effectTpl).render(res);
      $('#LAY_coop_effect').html(html);
      $('#LAY_effect_count').html('你共有 <strong style="color: #FF5722;">'+ (res.count||0) +'</strong> 笔合作授权订单');
    };

    var effectShow = function(page){
      fly.json('/cooperation/effect', {
        page: page||1
      }, function(res){
        effectView(res);
        laypage.render({
          elem: 'LAY_effect_page'
          ,count: res.count
          ,curr: page
          ,jump: function(e, first){
            if(!first){
              effectShow(e.curr);
            }
          }
        });
      });
    };

    effectShow();

  }

  //提交成功后刷新
  fly.form['set-mine'] = function(data, required){
    layer.msg('修改成功', {
      icon: 1
      ,time: 1000
      ,shade: 0.1
    }, function(){
      location.reload();
    });
  }

  //初始化数据库
  $('.acc-InitDataBase').on('click', function(){
    var othis = $(this), type = othis.attr('type');
    layer.confirm('确定要初始化'+ type + '数据库吗？', {icon: 7}, function(){
      fly.json('?System_InitDataBase', {
        type: type
      }, function(res){
        if(res.status === 0){
          layer.alert('已成功初始化。', {
            icon: 1
            ,end: function(){
              //location.reload();
            }
          });
        } else {
          layer.msg(res.msg);
        }
      });
    });
  });
  
  //从旧版导入
  $('.acc-ImportDataBase').on('click', function(){
    var othis = $(this), type = othis.attr('type'),OldDb = othis.attr('dbName');
    layer.confirm('确定要从'+ type + '旧版数据库导入吗？', {icon: 7}, function(){
		 layer.closeAll();
		 layer.prompt({
			 value: OldDb
		 },function(val, index){
			OldDb = val;
			layer.closeAll();
			importDataBase( type ,OldDb );
		}); 		  
    });
  });
 
  
  //数据库旧版导入
  function importDataBase( type ,OldDb ){
	  fly.json('?System_importDataBase', {
		'type': type
		,'OldDb' : OldDb
	  }, function(res){
		if(res.status === 0){
		  layer.alert('导入完成', {
			icon: 1
			,end: function(){
			  //location.reload();
			}
		  });
		} else {
		  layer.msg(res.msg);
		}
	  });  
  }
  
  //删除数据库
  $('.acc-RemoveDataBase').on('click', function(){
    var othis = $(this), type = othis.attr('type'),OldDb = othis.attr('dbName');
    layer.confirm('确定要删除'+ type + '备份数据库' + OldDb + '吗？', {icon: 7}, function(){
		 layer.closeAll();
		  fly.json('?System_RemoveDataBase', {
			'type': type
			,'OldDb' : OldDb
		  }, function(res){
			if(res.status === 0){
			  layer.alert('删除完成', {
				icon: 1
				,end: function(){
				  location.reload();
				}
			  });
			} else {
			  layer.msg(res.msg);
			}
		  }); 		  
    });
  });
  
  //数据库转换
  $('.acc-db2db').on('click', function(){
    var othis = $(this), type = othis.attr('type') , dst=othis.attr('dst');
    layer.confirm('确定要将数据库'+ type + '转换到' + dst + '吗？', {icon: 7}, function(){
	  layer.closeAll();
      fly.json('?System_db2db', {
        'scrType': type
		,'dstType': dst
      }, function(res){
        if(res.status === 0){
          layer.alert('数据库'+ type + '转换到' + dst + '完成!', {
            icon: 1
            ,end: function(){
              //location.reload();
            }
          });
        } else {
          layer.msg(res.msg);
        }
      });
    });
  });
  
  //access数据库压缩
  $('.acc-CompressAccess').on('click', function(){
    var othis = $(this), type = othis.attr('type') ;
    layer.confirm('确定要压缩 '+ type + ' 数据库吗？', {icon: 7}, function(){
      fly.json('?System_CompressAccess', {
		'type': type
      }, function(res){
        if(res.status === 0){
          layer.alert(res.msg, {
            icon: 1
            ,end: function(){
              location.reload();
            }
          });
        } else {
          layer.msg(res.msg);
        }
      });
    });
  });
  
  //数据库备份
  $('.acc-DbBackup').on('click', function(){
    var othis = $(this), type = othis.attr('type') ;
    layer.confirm('确定要备份 '+ type + ' 数据库吗？', {icon: 7}, function(){
      fly.json('?System_DbBackup', {
		'type': type
      }, function(res){
        if(res.status === 0){
          layer.alert(res.msg, {
            icon: 1
            ,end: function(){
              location.reload();
            }
          });
        } else {
          layer.msg(res.msg);
        }
      });
    });
  });
  
  //数据库切换
  $('.acc-switchDb').on('click', function(){
    var othis = $(this), type = othis.attr('type') ;
    layer.confirm('确定要将数据库切换到'+ type + '吗？', {icon: 7}, function(){
      fly.json('?System_switchDb', {
		'type': type
      }, function(res){
        if(res.status === 0){
          layer.alert(res.msg, {
            icon: 1
            ,end: function(){
              location.reload();
            }
          });
        } else {
          layer.msg(res.msg);
        }
      });
    });
  });
  
  //核心文件夹改名
  $('.acc-EditCore').on('click', function(){
	 var othis = $(this), type = othis.attr('type') , d = new Date();
	 if ( type == "" ) {
		 type = '#' + d.getYear() + (d.getMonth()+1) + d.getDate() + d.getHours() + d.getMinutes()+ d.getSeconds() + Math.ceil((1000+9000*Math.random()));		 
	 }
    layer.confirm('修改核心文件夹名吗？', {icon: 7}, function(){
		 layer.closeAll();
		 layer.prompt({
			  value: type,
			  title: '请输入核心文件夹名',
		 },function(val, index){
		   layer.closeAll();
		  fly.json('?System_EditCorePath', {
			  'folder' : val
		  }, function(res){
			if(res.status === 0){
			  layer.alert('核心文件夹名修改完成!', {
				icon: 1
				,end: function(){
				  location.reload();
				}
			  });
			} else {
			  layer.msg(res.msg);
			}
		  });
		}); 
    });
  });
  
  //模板文件夹改名
  $('.acc-EditHtml').on('click', function(){
	 var othis = $(this), type = othis.attr('type') , d = new Date();
	 if ( type == "" ) {
		 type = '#' + d.getYear() + (d.getMonth()+1) + d.getDate() + d.getHours() + d.getMinutes()+ d.getSeconds() + Math.ceil((1000+9000*Math.random()));		 
	 }
    layer.confirm('修改模板文件夹名？', {icon: 7}, function(){
		 layer.closeAll();
		 layer.prompt({
			  value: type,
			  title: '请输入修改模板文件夹名',
		 },function(val, index){
		   layer.closeAll();
		  fly.json('?System_EditHtmlPath', {
			  'configValue' : val
		  }, function(res){
			if(res.status === 0){
			  layer.alert('模板文件名修改完成!', {
				icon: 1
				,end: function(){
				  location.reload();
				}
			  });
			} else {
			  layer.msg(res.msg);
			}
		  });
		}); 
    });
  });  
  
  
  //使用某个邮件发送方式
  $('.acc-UseMail').on('click', function(){
    var othis = $(this), type = othis.attr('type'),stype = othis.attr('stype');
    layer.confirm('确定要使用 '+ stype  + ' 吗？', {icon: 7}, function(){
		 layer.closeAll();
		   layer.closeAll();
		  fly.json('?Mail_SwitchType', {
			  'type' : type,
			  'typestr' : stype
		  }, function(res){
			location.reload();
		  });
    });
  });
  
  //测试某个邮件发送方式
  $('.acc-MailTest').on('click', function(){
    var othis = $(this), type = othis.attr('type'),stype = othis.attr('stype');
    layer.confirm('确定要测试 '+ stype  + ' 吗？', {icon: 7}, function(){
		 layer.closeAll();
		   layer.closeAll();
		  fly.json('?Mail_MailTest', {
			  'type' : type,
			  'typestr' : stype
		  }, function(res){
			layer.msg("测试邮件已发送，若收不到邮件，说明 " + stype +" 不可用");
			return false;
		  });
    });
  });
  
  
  //我的消息
  gather.minemsg = function(){
    var delAll = $('#LAY_delallmsg')
    ,tpl = '{{# var len = d.rows.length;\
    if(len === 0){ }}\
      <div class="fly-none">您暂时没有最新消息</div>\
    {{# } else { }}\
      <ul class="mine-msg">\
      {{# for(var i = 0; i < len; i++){ }}\
        <li data-id="{{d.rows[i].id}}">\
          <blockquote class="layui-elem-quote">{{ d.rows[i].content}}</blockquote>\
          <p><span>{{d.rows[i].time}}</span><a href="javascript:;" class="layui-btn layui-btn-sm layui-btn-danger fly-delete">删除</a></p>\
        </li>\
      {{# } }}\
      </ul>\
    {{# } }}'
    ,delEnd = function(clear){
      if(clear || dom.minemsg.find('.mine-msg li').length === 0){
        dom.minemsg.html('<div class="fly-none">您暂时没有最新消息</div>');
      }
    }
    
    
    /*
    fly.json('/message/find/', {}, function(res){
      var html = laytpl(tpl).render(res);
      dom.minemsg.html(html);
      if(res.rows.length > 0){
        delAll.removeClass('layui-hide');
      }
    });
    */
    
    //阅读后删除
    dom.minemsg.on('click', '.mine-msg li .fly-delete', function(){
      var othis = $(this).parents('li'), id = othis.data('id');
      fly.json('?user_removeMessage', {
        id: id
      }, function(res){
        if(res.status === 0){
          othis.remove();
          delEnd();
        }
      });
    });

    //删除全部
    $('#LAY_delallmsg').on('click', function(){
      var othis = $(this);
      layer.confirm('确定清空吗？', function(index){
        fly.json('?user_removeMessage', {
          all: true
        }, function(res){
          if(res.status === 0){
            layer.close(index);
            othis.addClass('layui-hide');
            delEnd(true);
          }
        });
      });
    });
	


  };
  
	$('#chatBtn').on('click', function(){
		var othis = $(this);
		console.log(othis);
		if(layui.cache.user.uid == '-1') {
			layer.alert('请先登陆！', {icon: 7});			
		} else{
			layer.prompt(function(val, index){
				layer.closeAll();
				fly.json('?user_SendMessage', {
				  FromUser: layui.cache.user.uid,
				  ToUser: othis.data("touser"),
				  Message:val
				}, function(res){					
				  if(res.status === 0){
					layer.alert('发送成功', {icon: 6});
					layer.close(index);
				  }
				});
				//layer.msg('得到了'+val);
				//layer.close(index);
			});			
		}
		
	});
  
  dom.minemsg[0] && gather.minemsg();

  exports('user', null);
  
});