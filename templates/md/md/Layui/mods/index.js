﻿/**

 @Name: Fly社区主入口

 */
 

layui.define(['layer','form', 'element' ,'util'], function(exports){
  
  var $ = layui.jquery
  ,layer = layui.layer
  ,form = layui.form
  ,util = layui.util
  ,element=layui.element
  ,device = layui.device();
  
 element.init();
  
  
  //阻止IE7以下访问
  if(device.ie && device.ie < 8){
    layer.alert('如果您非得使用 IE 浏览器访问Fly社区，那么请使用 IE8+');
  }
  
  layui.focusInsert = function(obj, str){
    var result, val = obj.value;
    obj.focus();
    if(document.selection){ //ie
      result = document.selection.createRange(); 
      document.selection.empty(); 
      result.text = str; 
    } else {
      result = [val.substring(0, obj.selectionStart), str, val.substr(obj.selectionEnd)];
      obj.focus();
      obj.value = result.join('');
    }
  };
  
  var fly = {

    //Ajax
    json: function(url, data, success, options){
      var that = this, type = typeof data === 'function';
      
      if(type){
        options = success
        success = data;
        data = {};
      }

      options = options || {};

      return $.ajax({
        type: options.type || 'post',
        dataType: options.dataType || 'json',
        data: data,
        url: url,
        success: function(res){
          if(res.status === 0) {
            success && success(res);
          } else {
            layer.msg(res.msg || res.code, {shift: 6});
            options.error && options.error();
          }
        }, error: function(e){
          layer.msg('请求异常，请重试', {shift: 6});
          options.error && options.error(e);
        }
      });
    }

    //计算字符长度
    ,charLen: function(val){
      var arr = val.split(''), len = 0;
      for(var i = 0; i <  val.length ; i++){
        arr[i].charCodeAt(0) < 299 ? len++ : len += 2;
      }
      return len;
    }
    
    ,form: {}

    ,escape: function(html){
      return String(html||'').replace(/&(?!#?[a-zA-Z0-9]+;)/g, '&amp;')
      .replace(/</g, '&lt;').replace(/>/g, '&gt;').replace(/'/g, '&#39;').replace(/"/g, '&quot;');
    }
    
  };
 //头条轮播
  if($('#FLY_topline')[0]){
    layui.use('carousel', function(){
      var carousel = layui.carousel;
      
      var ins = carousel.render({
        elem: '#FLY_topline'
        ,width: '100%'
        ,height: '172px'
        ,anim: 'fade'
      });

      var resizeTopline = function(){
        var width = $(this).prop('innerWidth');
        if(width >= 1200){
          ins.reload({
            height: '172px'
          });
        } else if(width >= 992){
          ins.reload({
            height: '141px'
          });
        } else if(width >= 768){
          ins.reload({
            height: '166px'
          });
        }
      };

      resizeTopline()

      $(window).on('resize', resizeTopline);

    });
  }


  //相册
  if($(window).width() > 750){
    layer.photos({
      photos: '.photos'
      ,zIndex: 9999999999
      ,anim: -1
    });
  } else {
    $('body').on('click', '.photos img', function(){
      window.open(this.src);
    });
  }


  //搜索
  $('.fly-search').on('click', function(){
    layer.open({
      type: 1
      ,title: false
      ,closeBtn: false
      //,shade: [0.1, '#fff']
      ,shadeClose: true
      ,maxWidth: 10000
      ,skin: 'fly-layer-search'
      //,content: ['<form action="http://cn.bing.com/search">'
      ,content: ['<form action="' + ASPBBS.searchUrl + '" method="post">'
        ,'<input autocomplete="off" placeholder="搜索内容，回车跳转" type="text" name="keys">'
      ,'</form>'].join('')
      ,success: function(layero){
        var input = layero.find('input');
        input.focus();

        layero.find('form').submit(function(){
          var val = input.val();
          if(val.replace(/\s/g, '') === ''){
            return false;
          }
          //input.val('site:iaspcms.cn '+ input.val());
      });
      }
    })
  });
  
  //表单提交
  form.on('submit(search)', function(data){
	  return true;
  });
  

  //表单提交
  form.on('submit(*)', function(data){
    var action = $(data.form).attr('action'), button = $(data.elem);
	var isClose = $(data.form).data("close")
    fly.json(action, data.field, function(res){
      var end = function(){
        if(res.action){
		
          location.href = res.action;
		  
        } else {
          fly.form[action||button.attr('key')](data.field, data.form, res);
        }
		if ( isClose == 1 ) {
			var index = parent.layer.getFrameIndex(window.name); //先得到当前iframe层的索引
			parent.layer.close(index);
			parent.location.reload();
			return false;
		}
      };

      if(res.status == 0){
        button.attr('alert') ? layer.alert(res.msg, {
          icon: 1,
          time: 1.5 *1000,
          end: end
        }) : end();
      };
    });
    return false;
  });


  //加载特定模块
  if(layui.cache.page && layui.cache.page !== 'index'){
    var extend = {};
    extend[layui.cache.page] = layui.cache.page;
    layui.extend(extend);
    layui.use(layui.cache.page);
  }
  
  //加载IM
  if(!device.android && !device.ios){
    //layui.use('im');
  }

  //手机设备的简单适配
  var treeMobile = $('.site-tree-mobile')
  ,shadeMobile = $('.site-mobile-shade')

  treeMobile.on('click', function(){
    $('body').addClass('site-mobile');
  });

  shadeMobile.on('click', function(){
    $('body').removeClass('site-mobile');
  });

  


  
  //固定Bar
  (function(){
    if($('.layui-header')[0] || $('#LAY_brain')[0]){
      return;
    }
    util.fixbar({
      bar1: '&#xe642;'
      ,bgcolor: '#009688'
      ,click: function(type){
        if(type === 'bar1'){
          location.href = '?Topic_Add';
        }
      }
  })});
	
  exports('fly', fly);

});;